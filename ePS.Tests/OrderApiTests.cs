﻿using System;
using BusinessObjectsLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using DataServiceLibrary;
using ePS.Models.OrderApi;
using BusinessObjectServicesUnitTestLibrary;
using ePS.Types;
using BusinessObjectServices;

namespace ePS.Tests
{
    [TestClass]
    public class OrderApiTests : TestBase
    {
        [TestMethod]
        public void OrderApiPutPlaceOrderTest()
        {
            GrowerOrder placedOrder = null;

            try
            {
                placedOrder = CreatePlacedGrowerOrder(UserGuidScott, UserCodeScott, 100);

                var request = new PutRequestModel()
                    {
                        GrowerOrderGuid = placedOrder.OrderGuid,
                        OrderTransitionTypeLookupCode = "PlaceOrder",
                        AgreeToTerms = false
                    };

                var response = new PutResponseModel(UserGuidScott, request);

                CheckResponse(response, requireSuccess: false);

                response.Messages[0].Should().Be("You must agree to the Terms and Conditions.");
                response.Messages.Length.Should().Be(1);
               // response.Success.Should().BeFalse();

                request.AgreeToTerms = true;

                response = new PutResponseModel(UserGuidScott, request);

                CheckResponse(response, requireSuccess: true);

              //  response.OrderLinesChanged.Should().BeGreaterThan(0);
                //TODO: this should cause it to fail if the order has already been placed, but it's not.
                response.PrecartOrderLinesDeleted.Should().Be(0);
                response.IsAuthenticated.Should().BeTrue();
                response.ProductsOrdered.Should().Be(0);

              //  System.Console.WriteLine(response.SupplierOrderStatuses.ToString());
            }
            finally
            {
                if (placedOrder != null)
                    DeleteAnyOrder(placedOrder);
            }
        }
       
        [TestMethod]
        public void OrderApiPutSupplierNotifiedTest()
        {
            GrowerOrder placedOrder = null;

            try
            {
                placedOrder = CreatePlacedGrowerOrder(UserGuidScott, UserCodeScott, 100);
                var supplierOrder = GetTheOnlySupplierOrder(placedOrder);

                var request = new PutRequestModel()
                {
                    SupplierOrderGuid = supplierOrder.SupplierOrderGuid,
                    OrderTransitionTypeLookupCode = "SupplierNotified",
                    AgreeToTerms = false
                };

                var response = new PutResponseModel(UserGuidScott, request);

                CheckResponse(response);

                response.Messages.Length.Should().Be(0);
                response.Success.Should().BeTrue();
                //response.OrderLinesChanged.Should().BeGreaterThan(0);
                response.PrecartOrderLinesDeleted.Should().Be(0);
                response.IsAuthenticated.Should().BeTrue();
                response.ProductsOrdered.Should().Be(0);
            }
            finally
            {
                if (placedOrder != null)
                {
                    DeleteAnyOrder(placedOrder);
                }
            }
        }

        [TestMethod]
        public void OrderApiPutSupplierNotifiedUsingOrderLineGuidTest()
        {
            GrowerOrder placedOrder = null;

            try
            {
                placedOrder = CreatePlacedGrowerOrder(UserGuidScott, UserCodeScott, 100);
                var orderLine = GetTheOnlyOrderOrderLine(placedOrder);

                var request = new PutRequestModel()
                    {
                        OrderLineGuid = orderLine.Guid,
                        OrderTransitionTypeLookupCode = "SupplierNotified",
                        AgreeToTerms = false
                    };

                var response = new PutResponseModel(UserGuidScott, request);

                CheckResponse(response);

                response.Messages.Length.Should().Be(0);
                response.Success.Should().BeTrue();
               // response.OrderLinesChanged.Should().BeGreaterThan(0);
                response.PrecartOrderLinesDeleted.Should().Be(0);
                response.IsAuthenticated.Should().BeTrue();
                response.ProductsOrdered.Should().Be(0);

                placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
                orderLine = GetTheOnlyOrderOrderLine(placedOrder);
                orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.SupplierNotified.Guid);
            }
            finally
            {
                if (placedOrder != null)
                {
                    DeleteAnyOrder(placedOrder);
                }
            }

        }

        [TestMethod]
        public void OrderApiPutSupplierConfirmedUsingOrderLineGuidTest()
        {
            GrowerOrder placedOrder = null;
            try
            {


                var growerOrderBusinessService = new GrowerOrderService();

                //placedOrder = growerOrderBusinessService.GetGrowerOrderDetail(new Guid("F2F1EE11-93DC-4674-B9D2-E4062C2CE26A"), false);
                placedOrder = CreatePlacedGrowerOrder(UserGuidScott, UserCodeScott, 100);
                var orderLine = GetTheOnlyOrderOrderLine(placedOrder);
                var request = new PutRequestModel()
                {
                    OrderLineGuid = orderLine.Guid,
                    OrderTransitionTypeLookupCode = "SupplierConfirmed",
                    AgreeToTerms = false
                };
                var response = new PutResponseModel(UserGuidScott, request);
                CheckResponse(response, requireSuccess: false);
           //    response.Messages.Length.Should().Be(1);
            //    response.Messages[0].Should().Be("No updates were made.");

                //var growerOrderService = new BusinessObjectServices.GrowerOrderService();
                //growerOrderService.SupplierNotified(UserGuidScott, Guid.Empty, orderLine.Guid);

                //placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
                //orderLine = GetTheOnlyOrderOrderLine(placedOrder);
                //orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.SupplierNotified.Guid);

                //response = new PutResponseModel(UserGuidScott, request);

                //CheckResponse(response);

              //  response.Messages.Length.Should().Be(0);
              //  response.Success.Should().BeTrue();
              ////  response.OrderLinesChanged.Should().BeGreaterThan(0);
              //  response.PrecartOrderLinesDeleted.Should().Be(0);
              //  response.IsAuthenticated.Should().BeTrue();
              //  response.ProductsOrdered.Should().Be(0);

                placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
                orderLine = GetTheOnlyOrderOrderLine(placedOrder);
                orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.SupplierConfirmed.Guid);
            }
            finally
            {
                if (placedOrder != null)
                {
                    DeleteAnyOrder(placedOrder);
                }
            }

        }

        [TestMethod]
        public void OrderApiPutSupplierNotifiedUsingOrderLineGuidFromScottTest()
        {
            GrowerOrder placedOrder = null;

            try
            {
                placedOrder = CreatePlacedGrowerOrder(UserGuidScott, UserCodeScott, 100);
                var orderLine = GetTheOnlyOrderOrderLine(placedOrder);

                var request = new PutRequestModel()
                {
                    OrderLineGuid = orderLine.Guid,
                    OrderTransitionTypeLookupCode = "SupplierNotified",
                    AgreeToTerms = false
                };

                var response = new PutResponseModel(UserGuidScott, request);

                CheckResponse(response);

                response.Messages.Length.Should().Be(0);
                response.Success.Should().BeTrue();
               // response.OrderLinesChanged.Should().BeGreaterThan(0);
                response.PrecartOrderLinesDeleted.Should().Be(0);
                response.IsAuthenticated.Should().BeTrue();
                response.ProductsOrdered.Should().Be(0);
            }
            finally
            {
                if (placedOrder != null)
                {
                    DeleteAnyOrder(placedOrder);
                }
            }
        }

        [TestMethod]
        public void OrderApiPutSupplierFeeTest()
        {
            GrowerOrder placedOrder = null;

            try
            {
                placedOrder = CreatePlacedGrowerOrder(UserGuidScott, UserCodeScott, 100);
                var orderLine = GetTheOnlyOrderOrderLine(placedOrder);

                var request = new ePS.Models.SupplierOrderFeeApi.PostRequestModel();
                request.SupplierOrderGuid = new Guid("24332B9E-EF0C-46C9-999F-F3981856CE81");
                request.SupplierOrderFeeTypeCode = LookupTableValues.Code.SupplierOrderFeeType.Freight.Code;

                Guid userGuid = new Guid("29E12974-4E99-4A93-B05F-E1847D98F01E");
                var response = new ePS.Models.SupplierOrderFeeApi.PostResponseModel(userGuid, request);

                CheckResponse(response);

                response.IsAuthenticated.Should().BeTrue();

                response.Success.Should().BeTrue();
                response.Messages.Length.Should().Be(0);

                response.SupplierOrderFeeType.Guid.Should().NotBeEmpty();
            }
            finally
            {
                if (placedOrder != null)
                    DeleteAnyOrder(placedOrder);
            }
        }

        [TestMethod]
        public void OrderApiGrowerOrderLogPostInternalCommentTest()
        {
            var request = new ePS.Models.OrderApi.PostGrowerOrderLogRequestModel()
                {
                    UserCode = "Test",
                    GrowerOrderGuid = Guid.NewGuid(),
                    LogTypeLookupCode = DataServiceLibrary.LookupTableValues.Logging.LogType.Grower.CommentInternal.Code,
                    Text = "Test GrowerOrder Log Internal Comment."
                };

            Guid userGuid = new Guid("7714B7E5-7148-404B-9E2E-93EE8B7E3B63");

            var response = new PostGrowerOrderLogResponseModel(userGuid, request,"Add");

            CheckResponse(response);

            response.Messages.Should().BeEmpty();
            response.EventLogGuid.Should().NotBeEmpty();
            response.Success.Should().BeTrue();
        }

        [TestMethod]
        public void OrderApiGrowerOrderLogPostExternalCommentTest()
        {
            var request = new ePS.Models.OrderApi.PostGrowerOrderLogRequestModel()
            {
                UserCode = "Test",
                GrowerOrderGuid = Guid.NewGuid(),
                LogTypeLookupCode = DataServiceLibrary.LookupTableValues.Logging.LogType.Grower.CommentExternal.Code,
                Text = "Test GrowerOrder Log External Comment."
            };

            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

            var response = new PostGrowerOrderLogResponseModel(userGuid, request,"Add");

            CheckResponse(response);

            response.Messages.Should().BeEmpty();
            response.EventLogGuid.Should().NotBeEmpty();
            response.Success.Should().BeTrue();
        }

        [TestMethod]
        public void OrderApiGrowerOrderSubstituteTest()
        {
            GrowerOrder placedOrder = null;

            try
            {
                const int quantity = 100;

                placedOrder = CreatePlacedGrowerOrder(UserGuidScott, UserCodeScott, quantity);
                var substituteProduct = GetAvailableProduct(UserGuidScott, UserCodeScott, placedOrder.ShipWeek,
                                                            placedOrder.ProgramTypeCode,
                                                            placedOrder.ProductFormCategoryCode,
                                                            quantity, productsToSkip: 37);
                var orderLine = GetTheOnlyOrderOrderLine(placedOrder);
                decimal savedPrice = orderLine.Price;
                Guid savedPriceUsedGuid = orderLine.PriceUsedGuid ?? Guid.Empty;

                substituteProduct.Guid.Should().NotBe(orderLine.Product.Guid);

                var request = new ePS.Models.OrderApi.SubstituteRequestModel()
                    {
                        UserCode = UserCodeScott,
                        OrderLineGuid = orderLine.Guid,
                        ProductGuid = substituteProduct.Guid,
                        ShipWeek =placedOrder.ShipWeek.ShipWeekString
                    };

                var response = new ePS.Models.OrderApi.SubstituteResponseModel(UserGuidScott, request);

                CheckResponse(response);

                if (response.Messages.Length > 0)
                {
                    System.Console.WriteLine("Messages:");
                    foreach (var message in response.Messages)
                    {
                        System.Console.WriteLine("\t" + message);
                    }
                }

                response.Messages.Length.Should().Be(0);
                response.Success.Should().BeTrue();

                placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
                orderLine = GetTheOnlyOrderOrderLine(placedOrder);
                orderLine.Product.Guid.Should().Be(substituteProduct.Guid);

                // Price should have been updated because of the substitution.
                orderLine.PriceHasBeenSet.Should().BeTrue();
                orderLine.PriceUsedGuid.Should().NotBe(savedPriceUsedGuid);
                orderLine.Price.Should().NotBe(savedPrice);
            }
            finally
            {
                if (placedOrder != null)
                {
                    DeleteAnyOrder(placedOrder);
                }
            }
        }

        [TestMethod]
        public void OrderApiGrowerApiRepriceTest()
        {
            GrowerOrder placedOrder = null;

            try
            {
                const int quantity = 100;

                placedOrder = CreatePlacedGrowerOrder(UserGuidScott, UserCodeScott, quantity);
                var substituteProduct = GetAvailableProduct(UserGuidScott, UserCodeScott, placedOrder.ShipWeek,
                                                            placedOrder.ProgramTypeCode,
                                                            placedOrder.ProductFormCategoryCode,
                                                            quantity, productsToSkip: 37);
                var orderLine = GetTheOnlyOrderOrderLine(placedOrder);

                decimal savedPrice = orderLine.Price;
                decimal savedCost = orderLine.Cost;
                orderLine.PriceUsedGuid.Should().NotBeEmpty();
                Guid savedPriceUsedGuid = orderLine.PriceUsedGuid ?? Guid.Empty;
                orderLine.PriceHasBeenSet.Should().BeTrue();

                substituteProduct.Guid.Should().NotBe(orderLine.Product.Guid);

                var orderService = new BusinessObjectServices.GrowerOrderService();
                orderService.SubstituteProductOnOrderLine(UserGuidScott, UserCodeScott, orderLine.Guid,
                                                          substituteProduct.Guid);

                placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
                orderLine = GetTheOnlyOrderOrderLine(placedOrder);
                orderLine.Product.Guid.Should().Be(substituteProduct.Guid);

                 // However, the GetGrowerOrderDetail logic gets the proper price data.
                orderLine.Product.Guid.Should().Be(substituteProduct.Guid);
                orderLine.PriceUsedGuid.Should().NotBe(savedPriceUsedGuid);
                orderLine.Price.Should().NotBe(savedPrice);
                orderLine.Cost.Should().NotBe(savedCost);

                var request = new PutPriceUpdateRequestModel()
                    {
                        UserCode = UserCodeScott,
                        GrowerOrderGuid = placedOrder.OrderGuid,
                        OrderLineGuid = orderLine.Guid
                    };

                var response = new PutPriceUpdateResponseModel(UserGuidScott, request);

                CheckResponse(response);
                
                placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
                orderLine = GetTheOnlyOrderOrderLine(placedOrder);

                orderLine.PriceHasBeenSet.Should().BeTrue();
                orderLine.Product.Guid.Should().Be(substituteProduct.Guid);
                orderLine.PriceUsedGuid.Should().NotBe(savedPriceUsedGuid);
                orderLine.Price.Should().NotBe(savedPrice);
                orderLine.Cost.Should().NotBe(savedCost);

                savedPrice = orderLine.Price;
                savedCost = orderLine.Cost;
                savedPriceUsedGuid = orderLine.PriceUsedGuid ?? Guid.Empty;

                placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
                orderLine = GetTheOnlyOrderOrderLine(placedOrder);

                orderLine.PriceHasBeenSet.Should().BeTrue();
                orderLine.Product.Guid.Should().Be(substituteProduct.Guid);
                orderLine.PriceUsedGuid.Should().Be(savedPriceUsedGuid);
                orderLine.Price.Should().Be(savedPrice);
                orderLine.Cost.Should().Be(savedCost);

                var orderLineTableService = new OrderLineTableService();
                var orderLineTableObject = orderLineTableService.GetByGuid(orderLine.Guid);
                orderLineTableObject.ActualPrice += 1;
                orderLineTableObject.ActualCost += 1;
                orderLineTableService.Update(orderLineTableObject);

                placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
                orderLine = GetTheOnlyOrderOrderLine(placedOrder);

                orderLine.PriceHasBeenSet.Should().BeTrue();
                orderLine.Product.Guid.Should().Be(substituteProduct.Guid);
                orderLine.PriceUsedGuid.Should().Be(savedPriceUsedGuid);
                orderLine.Price.Should().Be(savedPrice + 1);
                orderLine.Cost.Should().Be(savedCost + 1);

                response = new PutPriceUpdateResponseModel(UserGuidScott, request);
                response.PricesUpdated.Should().Be(1);

                placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
                orderLine = GetTheOnlyOrderOrderLine(placedOrder);

                orderLine.PriceHasBeenSet.Should().BeTrue();
                orderLine.Product.Guid.Should().Be(substituteProduct.Guid);
                orderLine.PriceUsedGuid.Should().Be(savedPriceUsedGuid);
                orderLine.Price.Should().Be(savedPrice);
                orderLine.Cost.Should().Be(savedCost);
            }
            finally
            {
                if (placedOrder != null)
                {
                    DeleteAnyOrder(placedOrder);
                }
            }
        }
        [TestMethod]
        public void SupplierFeeGetTest()
        {


            try
            {
                var request = new Models.OrderApi.GetRequestSupplierFeeModel();
                var supplierOrderGuid = request.SupplierOrderGuid = new Guid("4DA9D0C2-0919-43FF-A234-28D7F9343B9D");

                var response = new Models.OrderApi.GetResponseSupplierFeeModel(new Guid(), request);
                response.SupplierOrderFees.Count.Should().BeGreaterThan(0);
            }
            finally
            {

            }
        }
        [TestMethod]
        public void GrowerFeeGetTest()
        {


            try
            {
                var request = new Models.OrderApi.GetRequestGrowerFeeModel();
                var growerOrderGuid = request.GrowerOrderGuid = new Guid("3FBF5BB5-B560-4987-BD7D-253465DE9559");

                var response = new Models.OrderApi.GetResponseGrowerFeeModel(new Guid(), request);
                response.GrowerOrderFees.Count.Should().BeGreaterThan(0);
            }
            finally
            {

            }
        }

       
        [TestMethod]
        public void OrderApiGetGrowerOrderDetailSortTest()
        {
            GrowerOrder placedOrder = null;

            try
            {
                placedOrder = CreatePlacedGrowerOrder(UserGuidScott, UserCodeScott, 100, 5);

                var requestModel = new ePS.Models.OrderApi.GetDetailRequestModel()
                    {
                        UserCode = UserCodeScott,
                        OrderGuid = placedOrder.OrderGuid
                    };

                var responseModel = new ePS.Models.OrderApi.GetDetailResponseModel(UserGuidScott, requestModel);

                CheckResponse(responseModel, requireSuccess: false);

                System.Console.WriteLine( responseModel.GrowerOrder.ToString());

                foreach (var supplierOrder in responseModel.GrowerOrder.SupplierOrders)
                {
                    System.Console.WriteLine(supplierOrder.ToString());

                    OrderLineType previousOrderLine = null;
                    foreach (var orderLine in supplierOrder.OrderLines)
                    {
                        System.Console.WriteLine(orderLine.ToString() + " (" + orderLine.SpeciesName + " " + orderLine.ProductDescription + ")");

                        if (previousOrderLine != null)
                        {
                            orderLine.CompareTo(previousOrderLine).Should().BeGreaterThan(0);
                        }

                        previousOrderLine = orderLine;
                    }
                }
            }
            finally
            {
                if (placedOrder != null)
                {
                    DeleteAnyOrder(placedOrder);
                }
            }
        }
    }
}

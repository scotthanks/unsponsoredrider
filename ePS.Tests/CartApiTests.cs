﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using DataServiceLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ePS.Models.CartApi;
using FluentAssertions;
using ePS.Types;

namespace ePS.Tests
{
    [TestClass]
    public class CartApiTests : TestBase
    {
        [TestMethod]
        public void CartApiPostAddOrUpdateOrderLineBadRequestTest()
        {
            Guid orderLineGuid = Guid.NewGuid();
            int quantity = 10;

            var request = new PostRequestModel()
                {
                    UserCode = UserCodeScott,
                    CartOrderLineUpdates = new List<CartOrderLineUpdate>()
                        {
                            new CartOrderLineUpdate()
                                {
                                    Guid = orderLineGuid,
                                    Quantity = quantity
                                }
                        }
                };

            var response = new PostResponseModel(UserGuidScott, request);

            CheckResponse(response, requireSuccess: false);

            response.Success.Should().BeFalse();

            foreach (var orderLineStatus in response.OrderLineStatuses)
            {
                orderLineStatus.Success.Should().BeFalse();
                orderLineStatus.Message.Should().NotBeEmpty();
                System.Console.WriteLine(orderLineStatus.Message);
                orderLineStatus.Message.Should().Be("A system error has occurred. Your request was not processed.");
            }
        }

        [TestMethod]
        public void CartApiPostUpdateOrderLineTest()
        {
            GrowerOrder cartOrder = null;
            try
            {
                int quantity = 100;
                int newQuantity = 1000;

                cartOrder = CreateCartOrder(UserGuidScott, UserCodeScott, quantity);
                var orderLine = GetTheOnlyOrderOrderLine(cartOrder);
                quantity = orderLine.QuantityOrdered;

                var request = new PostRequestModel()
                    {
                        UserCode = UserCodeScott,
                        CartOrderLineUpdates = new List<CartOrderLineUpdate>()
                            {
                                new CartOrderLineUpdate()
                                    {
                                        Guid = orderLine.Guid,
                                        Quantity = newQuantity
                                    }
                            }
                    };

                var response = new PostResponseModel(UserGuidScott, request);

                Guid orderLineGuid = Guid.NewGuid();

                foreach (var orderLineStatus in response.OrderLineStatuses)
                {
                    System.Console.WriteLine(orderLineStatus.Message);

                    orderLineStatus.Success.Should().BeTrue();

                    orderLineGuid = orderLineStatus.OrderLineGuid;
                    orderLineStatus.Quantity.Should().BeGreaterThan(quantity);
                    quantity = orderLineStatus.Quantity;

                    orderLineGuid.Should().NotBeEmpty();
                }

                CheckResponse(response, requireSuccess: false);

                orderLineGuid.Should().NotBeEmpty();
                orderLineGuid.Should().Be(orderLine.Guid);
                var orderlineService = new OrderLineTableService();
                var newOrderLine =orderlineService.TryGetByGuid(orderLineGuid);


                newOrderLine.QtyOrdered.Should().Be(newQuantity);
             

                DeleteAnyOrder(cartOrder);
                cartOrder = null;
            }
            finally
            {
                if (cartOrder != null)
                {
                    DeleteAnyOrder(cartOrder);                    
                }
            }
        }


      
        public void CartApiDeleteTest()
        {
            GrowerOrder cartOrder = null;

            try
            {
                cartOrder = CreateCartOrder(UserGuidScott, UserCodeScott, 100);

                var growerOrderTableService = new GrowerOrderTableService();
                growerOrderTableService.Exists(cartOrder.OrderGuid).Should().BeTrue();

                var response = new Models.CartApi.DeleteResponseModel(UserGuidScott, UserCodeScott, cartOrder.OrderGuid);

                CheckResponse(response);

                growerOrderTableService = new GrowerOrderTableService();
                growerOrderTableService.Exists(cartOrder.OrderGuid).Should().BeFalse();

                // It's deleted now, so don't try to delete it again in the finally clause.
                cartOrder = null;
            }
            finally
            {
                if (cartOrder != null)
                {
                    DeleteAnyOrder(cartOrder);
                }
            }
        }

        [TestMethod]
        public void CartApiGetGrowerOrderDetailTest()
        {
            GrowerOrder cartOrder = null;

            try
            {
                const string programTypeCode = "VA";
                const string productFormCategoryCode = "RC";

                cartOrder = CreateCartOrder(UserGuidScott, UserCodeScott, 100, 1, programTypeCode, productFormCategoryCode);

                int supplierOrders = 0;
                int orderLines = 0;

                CountSupplierOrdersAndOrderLines(cartOrder, out supplierOrders, out orderLines);
                supplierOrders.Should().Be(1);
                orderLines.Should().Be(1);

                var additionalProductList = 
                    GetAvailableProducts
                    (
                        UserGuidScott,
                        UserCodeScott, 
                        cartOrder.ShipWeek,
                        programTypeCode, 
                        productFormCategoryCode,
                        200, 
                        productsToSkip: 1,
                        maxProductsToReturn: 100
                    );

                var orderLine = GetTheOnlyOrderOrderLine(cartOrder);
                string originalSupplierCode = orderLine.Product.SupplierCode;

                Product availableProductForSameSupplier = null;
                Product availableProductForDifferentSupplier = null;

                foreach (var product in additionalProductList)
                {
                    if (product.SupplierCode == originalSupplierCode && availableProductForSameSupplier == null)
                    {
                        availableProductForSameSupplier = product;
                    }
                    else if (product.SupplierCode != originalSupplierCode &&
                             availableProductForDifferentSupplier == null)
                    {
                        availableProductForDifferentSupplier = product;
                    }

                    if (availableProductForSameSupplier != null && availableProductForDifferentSupplier != null)
                        break;
                }

                availableProductForSameSupplier.Should().NotBeNull();
                availableProductForDifferentSupplier.Should().NotBeNull();

                if (availableProductForSameSupplier == null)
                    throw new ApplicationException("availableProductforSameSupplier should not be null.");

                if (availableProductForDifferentSupplier == null)
                    throw new ApplicationException("availableProductForDifferentSupplier should not be null.");

                var cartLine = GetTheOnlyOrderOrderLine(cartOrder);

                // Selecting additional items without adding them to the cart shouldn't affect the order detail.
                UpdateCartQuantity(UserGuidScott, UserCodeScott, cartOrder.ShipWeek, availableProductForSameSupplier, 200, availableProductForSameSupplier.Price);
                UpdateCartQuantity(UserGuidScott, UserCodeScott, cartOrder.ShipWeek, availableProductForDifferentSupplier, 300, availableProductForDifferentSupplier.Price);

                cartOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, cartOrder.OrderGuid, includePriceData: false);

                CountSupplierOrdersAndOrderLines(cartOrder, out supplierOrders, out orderLines);
                supplierOrders.Should().Be(1);
                orderLines.Should().Be(1);
            }
            finally
            {
                if (cartOrder != null)
                {
                    DeleteAnyOrder(cartOrder);
                }
            }
        }

        [TestMethod]
        public void CartApiDeleteVersusCancelTest()
        {
            GrowerOrder cartOrder = null;

            try
            {
                cartOrder = CreateCartOrder(UserGuidScott, UserCodeScott, 100, 1);

                int supplierOrders = 0;
                int orderLines = 0;

                CountSupplierOrdersAndOrderLines(cartOrder, out supplierOrders, out orderLines);
                supplierOrders.Should().Be(1);
                orderLines.Should().Be(1);

                var orderLine = GetTheOnlyOrderOrderLine(cartOrder);
                string originalSupplierCode = orderLine.Product.SupplierCode;

                var growerOrderTableService = new GrowerOrderTableService();
                growerOrderTableService.Exists(cartOrder.OrderGuid).Should().BeTrue();

                UpdateCartQuantity(UserGuidScott, UserCodeScott, cartOrder.ShipWeek, orderLine.Product, 0, orderLine.Product.Price);

                growerOrderTableService = new GrowerOrderTableService();
                growerOrderTableService.Exists(cartOrder.OrderGuid).Should().BeFalse();
                cartOrder = null;
            }
            finally
            {
                if (cartOrder != null)
                {
                    DeleteAnyOrder(cartOrder);
                }
            }
        }

        [TestMethod]
        public void CartApiGetOrderDetailSortTest()
        {
            const int numberOfLines = 10;
            GrowerOrder cartOrder = null;

            try
            {
                cartOrder = CreateCartOrder(UserGuidScott, UserCodeScott, 100, numberOfLines);

                var request = new GetRequestDetailModel()
                    {
                        OrderGuid = cartOrder.OrderGuid,
                        RequestType = "DetailData"
                    };

                var response = new ePS.Models.CartApi.GetResponseDetailModel(UserGuidScott, request);

                CheckResponse(response, requireSuccess: false);

                response.GrowerOrder.Should().NotBeNull();
                System.Console.WriteLine( response.GrowerOrder.ToString());

                response.GrowerOrder.SupplierOrders.Length.Should().BeGreaterOrEqualTo(1);
                int orderLineCount = 0;
                foreach (var supplierOrder in response.GrowerOrder.SupplierOrders)
                {
                    System.Console.WriteLine( supplierOrder.ToString());

                    OrderLineType previousOrderLine = null;
                    bool isSortedProperly = true;
                    int badSortCount = 0;
                    supplierOrder.OrderLines.Length.Should().BeGreaterOrEqualTo(1);
                    supplierOrder.OrderLines.Length.Should().BeLessOrEqualTo(numberOfLines);
                    foreach (var orderLine in supplierOrder.OrderLines)
                    {
                        if (previousOrderLine != null)
                        {
                            if (orderLine.CompareTo(previousOrderLine) <= 0)
                            {
                                isSortedProperly = false;
                            }
                        }

                        string asterisk = isSortedProperly ? "" : "*";
                        System.Console.WriteLine(orderLine.ToString() + " (" + orderLine.SpeciesName + " - " + orderLine.ProductDescription + ")" + asterisk);

                        previousOrderLine = orderLine;

                        orderLineCount++;

                        if (!isSortedProperly)
                        {
                            badSortCount++;
                        }
                    }

                    if (badSortCount > 0)
                    {
                        System.Console.Write("* - Is not sorted properly.");
                    }

                    badSortCount.Should().Be(0);
                }

                orderLineCount.Should().Be(numberOfLines);
            }
            finally
            {
                if (cartOrder != null)
                {
                    DeleteAnyOrder(cartOrder);
                }
            }
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using DataServiceLibrary;
using ePS.Models.OrderApi;

namespace ePS.Tests
{
    [TestClass]
    public class OrderApiPutTests
    {
        [TestMethod]
        public void OrderApiPutTest()
        {
            Guid growerOrderGuid = new Guid("d94e087a-f992-4bda-8229-3c9b8f7c5a5d");
            Guid userGuid = new Guid("7714B7E5-7148-404B-9E2E-93EE8B7E3B63");

            var request = new PutRequestModel()
                {
                    GrowerOrderGuid = growerOrderGuid,
                    OrderTransitionTypeLookupCode = "PlaceOrder",
                    AgreeToTerms = false
                };

            var response = new PutResponseModel(userGuid, request);

            response.Message.Should().Be("You must agree to the Terms and Conditions.");
            response.Success.Should().BeFalse();

            request.AgreeToTerms = true;

            response = new PutResponseModel(userGuid, request);

            response.Message.Should().Be("");
            response.Success.Should().BeTrue();
            response.OrderLinesChanged.Should().BeGreaterThan(0);
            response.PrecartOrderLinesDeleted.Should().Be(0);
            response.IsAuthenticated.Should().BeTrue();
            response.ProductsOrdered.Should().Be(0);

            System.Console.WriteLine( response.SupplierOrderStatuses.ToString());
        }

    }
}

﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ePS.Models.CardOnFile;
using BusinessObjectServices;
using FluentAssertions;

namespace ePS.Tests
{
    [TestClass]
    public class CardOnFileApiTests
    {
        private static Random _random;

        //[TestMethod]
        //public void CardOnFileApiPutTest()
        //{
        //    string cardNumber = GetValidCardNumber();

        //    var request = new PutRequestModel()
        //        {
        //            GrowerGuid = new Guid(new LookupService().GetConfigValue("SCOTT_GROWER_GUID")),
        //            Description = "NewCard",
        //            CardNumber = cardNumber,
        //            CardType = "Visa",
        //            ExpirationDate = "0315",
        //            Code = "1215",
        //            EmailAddress = "scotth@eplantsource.com",
        //            StreetAddress1 = "123 Pleasant",
        //            StreetAddress2 = "",
        //            City = "Pleasant Town",
        //            StateCode = "CA",
        //            Country = "USA",
        //            ZipCode = "12345",
        //            PhoneNumber = "123-123-1234",
        //            FirstName = "Scott",
        //            LastName = "Hanks"
        //        };

        //    var userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

        //    var response = new PutResponseModel(userGuid, request);

        //    response.IsAuthenticated.Should().BeTrue();
        //    response.Message.Should().BeEmpty();
        //    response.Success.Should().BeTrue();
        //    response.CardNumber.Should().Be("XXXX" + request.CardNumber.Substring(request.CardNumber.Length - 4, 4));
        //    response.CardExpiration.Should().Be("XXXX");
        //    response.Description.Should().Be(request.Description);
        //    response.ProfileID.Should().NotBeNullOrEmpty();
        //    response.ProfileID.Length.Should().Be(8);
        //}

        private string GetValidCardNumber()
        {
            string cardNumber;
            int retryCount = 0;
            do
            {
                cardNumber = GetRandomCardNumber();
                retryCount++;
            } while (!IsValidCardNumber(cardNumber));

            System.Console.WriteLine("Retry Count: " + retryCount.ToString(CultureInfo.InvariantCulture));

            return cardNumber;
        }

        private string GetRandomCardNumber()
        {
            if (_random == null)
            {
                _random = new Random();
            }

            long cardNumber = 0;
            for (int i = 0; i < 16; i++)
            {
                cardNumber = cardNumber * 10 + _random.Next() % 10;
            }

            return Math.Abs(cardNumber).ToString("0000000000000000");
        }

        private const string CreditCardPattern =
            @"^((4\d{3})|(5[1-5]\d{2})|(6011)|(34\d{1})|(37\d{1}))-?\d{4}-?\d{4}-?\d{4}|3[4,7][\d\s-]{15}$";

        private static Regex Regex
        {
            get
            {
                return new System.Text.RegularExpressions.Regex(CreditCardPattern);
            }
        }

        private static bool IsValidCardNumber(string number)
        {
            return Regex.IsMatch(number);
        }


        [TestMethod]
        public void CardOnFileApiGetTest()
        {
            Guid userGuid = new Guid("7714B7E5-7148-404B-9E2E-93EE8B7E3B63");

            var request = new ePS.Models.CardOnFile.GetRequestModel()
            {
                GrowerGuid = Guid.Empty,
                GrowerCreditCardGuid = Guid.Empty,
                GrowerOrderGuid = Guid.Empty
            };

            var response = new ePS.Models.CardOnFile.GetResponseModel(userGuid, request);

            response.IsAuthenticated.Should().BeTrue();
            response.Message.Should().Be("");

            int count = 0;

            bool selectedCard = false;
            foreach (var creditCard in response.CreditCards)
            {
                creditCard.Guid.Should().NotBeEmpty();
                //creditCard.CardDescription.Should().NotBeNullOrEmpty();
                //creditCard.CardType.Should().NotBeNull();
                //creditCard.CardTypeImageUrl.Should().NotBeNull();
                //creditCard.CardNumber.Should().NotBeNullOrEmpty();
                //creditCard.ExpirationDate.Should().NotBeNullOrEmpty();
                //creditCard.ProfileId.Should().NotBeNullOrEmpty();

                if (creditCard.IsSelected)
                {
                    selectedCard = true;
                }
                count++;
            }
            selectedCard.Should().BeFalse();

            count.Should().BeGreaterOrEqualTo(1);
        }

        [TestMethod]
        public void CardOnFileGetWithGrowerOrderTest()
        {
            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

            var request = new ePS.Models.CardOnFile.GetRequestModel()
            {
                GrowerGuid = Guid.Empty,
                GrowerCreditCardGuid = Guid.Empty,
                GrowerOrderGuid = new Guid("94604EC3-260A-49D2-971C-0DC9039622EC")
            };

            var response = new ePS.Models.CardOnFile.GetResponseModel(userGuid, request);

            response.IsAuthenticated.Should().BeTrue();
            response.Message.Should().Be("");

            int count = 0;
            bool selectedCard = false;
            foreach (var creditCard in response.CreditCards)
            {
                creditCard.Guid.Should().NotBeEmpty();
                //creditCard.CardDescription.Should().NotBeNullOrEmpty();
                //creditCard.CardType.Should().NotBeNull();
                //creditCard.CardTypeImageUrl.Should().NotBeNull();
                //creditCard.CardNumber.Should().NotBeNullOrEmpty();
                //creditCard.ExpirationDate.Should().NotBeNullOrEmpty();
                //creditCard.ProfileId.Should().NotBeNullOrEmpty();

                if (creditCard.IsSelected)
                {
                    selectedCard = true;
                }
                count++;
            }
            //selectedCard.Should().BeTrue();
            count.Should().BeGreaterOrEqualTo(1);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using BusinessObjectsLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using DataServiceLibrary;
using BusinessObjectServices;
using ePS.Models.AvailabilityApi;

namespace ePS.Tests
{
    [TestClass]
    public class AvailabilityApiTests : TestBase
    {
        [TestMethod]
        public void GetAddableOrderLines()
        {
            Guid SupplierOrderGuid = new Guid("4fbea27f-3101-4025-b308-7fe0a391e4d3");
            //Guid SupplierOrderGuid = new Guid("999f9b23-942a-414e-8ed9-36b00d4cde7e");

            SupplierOrderTableService supplierOrderService = new SupplierOrderTableService();
            DataObjectLibrary.SupplierOrder supplierOrder = supplierOrderService.GetByGuid(SupplierOrderGuid);

            SupplierTableService supplierService = new SupplierTableService();
            DataObjectLibrary.Supplier supplier = supplierService.GetByGuid(supplierOrder.SupplierGuid);

            GrowerOrderTableService growerOrderService = new GrowerOrderTableService();
            DataObjectLibrary.GrowerOrder growerOrder = growerOrderService.GetByGuid(supplierOrder.GrowerOrderGuid);

            Guid UserGuidGib = new Guid(new LookupService().GetConfigValue("GIB_USER_GUID"));

            ProgramTypeTableService programTypeService = new ProgramTypeTableService();
            DataObjectLibrary.ProgramType programType = programTypeService.GetByGuid(growerOrder.ProgramTypeGuid);

            ProductFormCategoryTableService productFormService = new ProductFormCategoryTableService();
            DataObjectLibrary.ProductFormCategory productFormCategory = productFormService.GetByGuid(growerOrder.ProductFormCategoryGuid);

            ShipWeekTableService shipWeekService = new ShipWeekTableService();
            DataObjectLibrary.ShipWeek shipWeek = shipWeekService.GetByGuid(growerOrder.ShipWeekGuid);

            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            AvailabilityService availService = new AvailabilityService(status);

            Availability availability = availService.GetAvailability(
                userCode: "Gib",
                shipWeek: new ShipWeek(shipWeek),
                weeksBefore: 0,
                weeksAfter: 0,
                includeCartData: false,
                includePrices: true,
                programTypeCode: programType.Code,
                productFormCategoryCode: productFormCategory.Code, 
                supplierCodeList: new string[] { supplier.Code }, 
                geneticOwnerCodeList: new string[] { "*" }, 
                varietyCodeList: new string[] { "*" },
                varietyMatchOptional: true);

        }

        [TestMethod]
        public void GetSpecitesTest()
        {
            Guid userGuid = new Guid(new LookupService().GetConfigValue("GIB_USER_GUID"));
            var status = new StatusObject(userGuid, true);
            var service = new AvailabilityDataService(status);
           
            var shipWeekService = new ShipWeekService();
            var selectedShipWeek = shipWeekService.ParseShipWeekString("15|2014");

            GetSubstitutesRequestModel request = new GetSubstitutesRequestModel();
            request.ShipWeek = "15|2014";
            //request.SpeciesCode = "AST";
            request.SpeciesCode = null;
            request.UserCode = "GIB";
            request.OrderLineGuid = new Guid("1152e66a-59c5-44be-9063-014694c30848");

            var model = new GetSubstitutesResponseModel(UserGuidGib, request);

            List<string> speciesCodes = new List<string>();

            foreach (var item in model.RowData)
            {
                if(!speciesCodes.Contains(item.SpeciesCode))
                {
                    speciesCodes.Add(item.SpeciesCode);
                }
            }


        }

    
        [TestMethod]
        public void AvailabilityApiGetTest()
        {
            var request = new Models.AvailabilityApi.GetAvailabilityRequestModel()
                {
                    ProgramTypeCode = "VA",
                    ProductFormCategoryCode = "RC",
                    ShipWeek = "30|2014",
                    Suppliers = "BUR,DUW,HDS,LIN,MLM,PLU,RAK",
                    Species = "ABU,ACA,ACH,AGA,AGE,AJA,ALT,ANA,ANG,ARG,ART,ASP,BAC,BEA,BEP,BEG,BID,BRY,BRA,CLN,CAL,CPT,CAN,CAR,CAS,CEL,CER,CHL,CHR,CIN,CLE,CLR,COL,CLC,CVL,COP,CRD,COR,COS,CRO,CUP,DAH,DIA,DIS,DIC,DIP,DOR,DRA,DUR,ERY,EUP,EUR,EVO,FEL,FCH,GAL,GAU,GAZ,GER,GMI,GEU,GLE,GRA,GYP,HEL,HTP,HMZ,HEU,HEC,HOU,HYP,IBE,IMP,NGI,IMW,IPO,IRE,IVY,KWK,LAM,LAN,LVT,LAV,LEU,LIT,LOL,LOB,LOP,LOT,LYS,MAN,MXS,MUE,NAS,NEM,NEP,NIE,OEN,OST,OTO,OXA,PAC,PEN,PAS,PER,PSK,PRS,PTC,PET,PHL,PLE,PLU,POR,PSE,RAN,RHO,RUD,RUE,SAL,SAN,SCB,SCA,SED,SET,SNA,STE,STR,THU,TOR,WNJ,TRI,TUR,VER,VRN,VIN,VIV,VIO"
                };

            request.Species =
                "ABU,ACA,ACH,AGA,AGE,AJA,ALT,ANA,ANG,ARG,ART,ASP,BAC,BEA,BEP,BEG,BID,BRY,BRA,CLN,CAL,CPT,CAN,CAR,CAS,CEL,CER,CHL,CHR,CIN,CLE,CLR,COL,CLC,CVL,COP,CRD,COR,COS,CRO,CUP,DAH,DIA,DIS,DIC,DIP,DOR,DRA,DUR,ERY,EUP,EUR,EVO,FEL,FCH,GAL,GAU,GAZ,GER,GMI,GEU,GLE,GRA,GYP,HEL,HTP,HMZ,HEU,HEC,HOU,HYP,IBE,IMP,NGI,IMW,IPO,IRE,IVY,KWK,LAM,LAN,LVT,LAV,LEU,LIT,LOL,LOB,LOP,LOT,LYS,MAN,MXS,MUE,NAS,NEM,NEP,NIE,OEN,OST,OTO,OXA,PAC,PEN,PAS,PER,PSK,PRS,PTC,PET,PHL,PLE,PLU,POR,PSE,RAN,RHO,RUD,RUE,SAL,SAN,SCB,SCA,SET,SNA,STE,STR,THU,TOR,WNJ,TRI,TUR,VER,VRN,VIN,VIV,VIO";

            var response =
                new Models.AvailabilityApi.GetAvailabilityResponseModel(
                    new Guid("7714B7E5-7148-404B-9E2E-93EE8B7E3B63"), request);

            int count = 0;
            int vioCount = 0;
            var speciesDict = new Dictionary<string, int>();
            foreach (var row in response.RowData)
            {
                int speciesCount;
                bool found = speciesDict.TryGetValue(row.SpeciesCode, out speciesCount);

                if (!found)
                {
                    speciesDict.Add(row.SpeciesCode, 1);                    
                }
                else
                {
                    speciesDict.Remove(row.SpeciesCode);
                    speciesDict.Add(row.SpeciesCode, speciesCount + 1);
                }

                if (row.SpeciesCode == "VIO")
                {
                    vioCount++;
                }
                count++;
            }

            foreach (var speciesCount in speciesDict)
            {
                System.Console.WriteLine("{0}={1}", speciesCount.Key, speciesCount.Value);                
            }

            System.Console.WriteLine(string.Format("count:{0}", count));
            System.Console.WriteLine(string.Format("vioCount:{0}", vioCount));

            //vioCount.Should().NotBe(0);

            request.Species =
                "ABU,ACA,ACH,AGA,AGE,AJA,ALT,ANA,ANG,ARG,ART,ASP,BAC,BEA,BEP,BEG,BID,BRY,BRA,CLN,CAL,CPT,CAN,CAR,CAS,CEL,CER,CHL,CHR,CIN,CLE,CLR,COL,CLC,CVL,COP,CRD,COR,COS,CRO,CUP,DAH,DIA,DIS,DIC,DIP,DOR,DRA,DUR,ERY,EUP,EUR,EVO,FEL,FCH,GAL,GAU,GAZ,GER,GMI,GEU,GLE,GRA,GYP,HEL,HTP,HMZ,HEU,HEC,HOU,HYP,IBE,IMP,NGI,IMW,IPO,IRE,IVY,KWK,LAM,LAN,LVT,LAV,LEU,LIT,LOL,LOB,LOP,LOT,LYS,MAN,MXS,MUE,NAS,NEM,NEP,NIE,OEN,OST,OTO,OXA,PAC,PEN,PAS,PER,PSK,PRS,PTC,PET,PHL,PLE,PLU,POR,PSE,RAN,RHO,RUD,RUE,SAL,SAN,SCB,SCA,SET,SNA,STE,STR,THU,TOR,WNJ,TRI,TUR,VER,VRN,VIN,VIV";// ",VIO";

            var response2 =
                new Models.AvailabilityApi.GetAvailabilityResponseModel(
                    new Guid("7714B7E5-7148-404B-9E2E-93EE8B7E3B63"), request);

            var count2 = 0;
            var vioCount2 = 0;
            var speciesDict2 = new Dictionary<string, int>();
            foreach (var row in response2.RowData)
            {
                int speciesCount;
                bool found = speciesDict2.TryGetValue(row.SpeciesCode, out speciesCount);

                if (!found)
                {
                    speciesDict2.Add(row.SpeciesCode, 1);
                }
                else
                {
                    speciesDict2.Remove(row.SpeciesCode);
                    speciesDict2.Add(row.SpeciesCode, speciesCount + 1);
                }

                if (row.SpeciesCode == "VIO")
                {
                    vioCount2++;
                }
                count2++;
            }

            foreach (var speciesCount in speciesDict2)
            {
                System.Console.WriteLine("{0}={1}", speciesCount.Key, speciesCount.Value);
            }

            System.Console.WriteLine( string.Format("count2:{0}", count2));
            System.Console.WriteLine(string.Format("vioCount2:{0}", vioCount2));

            count2.Should().BeLessThan(count);
            count2.Should().Be(count - vioCount);
            vioCount2.Should().Be(0);
        }

        public void AvailabilityApiGetFirstShipWeekTest()
        {
            var shipWeek = GetFirstShipWeek();
            shipWeek.Should().NotBeNull();
            shipWeek.MondayDate.Should().BeBefore(DateTime.Today);
        }

        [TestMethod]
        public void AvailabilityApiResultsBoundsTest()
        {
            var shipWeekService = new BusinessObjectServices.ShipWeekService();

            var shipWeek = GetShipWeek(DateTime.Today);
            TestAvailabilityForOneShipWeek(shipWeek);

            shipWeek = shipWeekService.OffsetByWeeks(shipWeek, 1);
            TestAvailabilityForOneShipWeek(shipWeek);
        }

        private void TestAvailabilityForOneShipWeek(ShipWeek shipWeek)
        {
            //var product = GetAvailableProduct(UserGuidRick, UserCodeRick, shipWeek, "VA", "URC", 100);

            var request = new Models.AvailabilityApi.GetAvailabilityRequestModel()
            {
                UserCode = UserCodeScott,
                ProgramTypeCode = "VA",
                ProductFormCategoryCode = "LIN",
                ShipWeek = shipWeek.Code,
                Suppliers = "PLU,RAK",
                Species = "ALY"
            };

            var response = new Models.AvailabilityApi.GetAvailabilityResponseModel(UserGuidScott, request);

            CheckResponse(response, requireSuccess: true);

            response.ShipWeekHeadings.Length.Should().Be(5);

            int expectedHeadingValue = 0;
            if (shipWeek.Week == 1)
            { expectedHeadingValue = 52; }
            else
            {
                expectedHeadingValue = shipWeek.Week - 1;
            }
            foreach (var shipWeekHeading in response.ShipWeekHeadings)
            {
                System.Console.WriteLine(shipWeekHeading);
                Convert.ToDouble(shipWeekHeading).Should().Be(expectedHeadingValue);
                
                expectedHeadingValue++;
                if (expectedHeadingValue == 53) { expectedHeadingValue = 1; }
            }

            int rowCount = 0;
            foreach (var row in response.RowData)
            {
                row.Availabilities.Length.Should().Be(5);
                foreach (var availability in row.Availabilities)
                {
                    System.Console.Write(availability.ToString(CultureInfo.InvariantCulture) + ",");
                }
                System.Console.WriteLine();
                rowCount++;
            }

            rowCount.Should().BeGreaterOrEqualTo(1);
        }
    }
}

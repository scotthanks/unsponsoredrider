﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using DataServiceLibrary;
using BusinessObjectServices;
using ePS.Models.OrderApi;

namespace ePS.Tests
{
    [TestClass]
    public class OrderApiGetSummaryTests
    {
        [TestMethod]
        public void OrderApiGetMyShoppingCartOrdersSummaryTest()
        {
            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

            var request = new GetSummaryRequestModel()
                {
                    RequestType = "MyShoppingCartOrders"
                };

            var response = new GetResponseSummaryModel(userGuid, request);

            response.IsAuthenticated.Should().BeTrue();

            response.SummaryData.Should().NotBeNull();

            int count = 0;
            foreach (var orderSummaryLine in response.SummaryData)
            {
                System.Console.WriteLine(string.Format( "OrderGuid:{0}", orderSummaryLine.OrderGuid));
                count++;
                if (count > 10)
                {
                    break;
                }
            }
        }

        [TestMethod]
        public void OrderApiGetMyPlacedOrdersSummaryTest()
        {
            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

            var request = new GetSummaryRequestModel()
            {
                RequestType = "MyOpenOrders"
            };

            var response = new GetResponseSummaryModel(userGuid, request);

            response.IsAuthenticated.Should().BeTrue();

            response.SummaryData.Should().NotBeNull();

            int count = 0;
            foreach (var orderSummaryLine in response.SummaryData)
            {
                System.Console.WriteLine(string.Format("OrderGuid:{0}", orderSummaryLine.OrderGuid));
                count++;
                if (count > 10)
                {
                    break;
                }
            }
        }

        //[TestMethod]
        //public void OrderApiGetMyShipppedOrdersSummaryTest()
        //{
        //    Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

        //    var request = new GetSummaryRequestModel()
        //    {
        //        RequestType = "MyShippedOrders"
        //    };

        //    var response = new GetResponseSummaryModel(userGuid, request);

        //    response.IsAuthenticated.Should().BeTrue();

        //    response.SummaryData.Should().NotBeNull();

        //    int count = 0;
        //    foreach (var orderSummaryLine in response.SummaryData)
        //    {
        //        System.Console.WriteLine(string.Format("OrderGuid:{0}", orderSummaryLine.OrderGuid));
        //        count++;
        //        if (count > 10)
        //        {
        //            break;
        //        }
        //    }

            
        //}

        //public void OrderApiGetMyCancelledOrdersSummaryTest()
        //{
        //    Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

        //    var request = new GetSummaryRequestModel()
        //    {
        //        RequestType = "MyCancelledOrders"
        //    };

        //    var response = new GetResponseSummaryModel(userGuid, request);

        //    response.IsAuthenticated.Should().BeTrue();

        //    response.SummaryData.Should().NotBeNull();

        //    int count = 0;
        //    foreach (var orderSummaryLine in response.SummaryData)
        //    {
        //        System.Console.WriteLine(string.Format("OrderGuid:{0}", orderSummaryLine.OrderGuid));
        //        count++;
        //        if (count > 10)
        //        {
        //            break;
        //        }
        //    }

        //    count.Should().Be(0);
        //}

        //[TestMethod]
        //public void OrderApiGetMyOrdersSummaryTest()
        //{
        //    Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

        //    var request = new GetSummaryRequestModel()
        //    {
        //        RequestType = "MyOrders"
        //    };

        //    var response = new GetResponseSummaryModel(userGuid, request);

        //    response.IsAuthenticated.Should().BeTrue();

        //    response.SummaryData.Should().NotBeNull();

        //    int count = 0;
        //    foreach (var orderSummaryLine in response.SummaryData)
        //    {
        //        System.Console.WriteLine(string.Format("OrderGuid:{0}", orderSummaryLine.OrderGuid));
        //        count++;
        //        if (count > 10)
        //        {
        //            break;
        //        }
        //    }
        //}

        //[TestMethod]
        //public void OrderApiGetAllShoppingCartOrdersSummaryTest()
        //{
        //    Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

        //    var request = new GetSummaryRequestModel()
        //    {
        //        RequestType = "AllShoppingCartOrders"
        //    };

        //    var response = new GetResponseSummaryModel(userGuid, request);

        //    response.IsAuthenticated.Should().BeTrue();

        //    response.SummaryData.Should().NotBeNull();

        //    int count = 0;
        //    foreach (var orderSummaryLine in response.SummaryData)
        //    {
        //        System.Console.WriteLine(string.Format("OrderGuid:{0}", orderSummaryLine.OrderGuid));
        //        count++;
        //        if (count > 10)
        //        {
        //            break;
        //        }
        //    }
        //}

        //[TestMethod]
        //public void OrderApiGetAllPlacedOrdersSummaryTest()
        //{
        //    Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

        //    var request = new GetSummaryRequestModel()
        //    {
        //        RequestType = "AllOpenOrders"
        //    };

        //    var response = new GetResponseSummaryModel(userGuid, request);

        //    response.IsAuthenticated.Should().BeTrue();

        //    response.SummaryData.Should().NotBeNull();

        //    int count = 0;
        //    foreach (var orderSummaryLine in response.SummaryData)
        //    {
        //        System.Console.WriteLine(string.Format("OrderGuid:{0}", orderSummaryLine.OrderGuid));
        //        count++;
        //        if (count > 10)
        //        {
        //            break;
        //        }
        //    }
        //}

        [TestMethod]
        public void OrderApiGetAllShipppedOrdersSummaryTest()
        {
            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

            var request = new GetSummaryRequestModel()
            {
                RequestType = "MyShippedOrders"
            };

            var response = new GetResponseSummaryModel(userGuid, request);

            response.IsAuthenticated.Should().BeTrue();

            response.SummaryData.Should().NotBeNull();

            int count = 0;
            foreach (var orderSummaryLine in response.SummaryData)
            {
                System.Console.WriteLine(string.Format("OrderGuid:{0}", orderSummaryLine.OrderGuid));
                count++;
                if (count > 10)
                {
                    break;
                }
            }

            
        }

        public void OrderApiGetAllCancelledOrdersSummaryTest()
        {
            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

            var request = new GetSummaryRequestModel()
            {
                RequestType = "AllCancelledOrders"
            };

            var response = new GetResponseSummaryModel(userGuid, request);

            response.IsAuthenticated.Should().BeTrue();

            response.SummaryData.Should().NotBeNull();

            int count = 0;
            foreach (var orderSummaryLine in response.SummaryData)
            {
                System.Console.WriteLine(string.Format("OrderGuid:{0}", orderSummaryLine.OrderGuid));
                count++;
                if (count > 10)
                {
                    break;
                }
            }

            count.Should().Be(0);
        }

        [TestMethod]
        public void OrderApiGetAllOrdersSummaryTest()
        {
            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

            var request = new GetSummaryRequestModel()
            {
                RequestType = "AllOrders"
            };

            var response = new GetResponseSummaryModel(userGuid, request);

            response.IsAuthenticated.Should().BeTrue();

            response.SummaryData.Should().NotBeNull();

            int count = 0;
            foreach (var orderSummaryLine in response.SummaryData)
            {
                System.Console.WriteLine(string.Format("OrderGuid:{0}", orderSummaryLine.OrderGuid));
                count++;
                if (count > 10)
                {
                    break;
                }
            }
        }
    }
}

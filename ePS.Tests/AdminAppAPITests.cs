﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using DataServiceLibrary;
using ePS.Models.AdminAppOrderDetailApi;
using AdminAppApiLibrary;
using BusinessObjectServices;
using DataObjectLibrary;
using ePS.Models.AdminAppAccountingApi;

namespace ePS.Tests
{
    [TestClass]
    public class AdminAppApiTests
    {
        [TestMethod]
        public void GetSpeciesTest()
        {

            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));
            var request = new GetSpeciesListForSupplierOrderRequest ();

            request.SupplierOrderGuid = new Guid( "4178BAF9-AB9B-492A-9821-17628959934A");
         
            var response = new ePS.Models.AdminAppOrderDetailApi.GetSpeciesListResponseModel (userGuid, request);
            response.Response.Success.Should().Be(true);
            response.Response.SpeciesList.Count.Should().BeGreaterThan(0);

         
        }

        [TestMethod]
        public void AddOrderLineTest()
        {

            try
            {
                Guid supplierOrderGuid = new Guid("016E5BEF-C216-44E4-A938-0015BD98E8E3"); //supplier order
                Guid productGuid = new Guid("33E8EDA4-7C2E-4A04-91FA-0003316F0B17");   //product

                OrderLineTableService orderLineService = new OrderLineTableService();
                OrderLine orderLine = new OrderLine();

                orderLine.Guid = Guid.NewGuid();
                orderLine.DateDeactivated = null;
                orderLine.SupplierOrderGuid = supplierOrderGuid;
                orderLine.ProductGuid = productGuid;
                orderLine.QtyOrdered = 10;
                orderLine.QtyShipped = 0;
                orderLine.ActualPrice = (decimal)55.44;
                orderLine.OrderLineStatusLookupGuid = LookupTableValues.Code.OrderLineStatus.Ordered.Guid;
                orderLine.ChangeTypeLookupGuid = LookupTableValues.Code.ChangeType.NoChange.Guid;
                orderLine.DateEntered = DateTime.Now;

                orderLineService.Insert(orderLine);

                orderLineService.Delete(orderLine);
            }
            catch (Exception ex)
            {
            }
        }
        //[TestMethod]
        //public void SupplierOrderKnoxTest()
        //{
        //  //  var gSupplerGuid = new Guid("2c49b890-fbde-4985-82d3-1af5b7fead51");  //knox
        //    var gSupplerGuid = new Guid(" 99BB2CC3-4E2B-495E-A51E-1E1EEC91CCAD");  //Plug Connection
           
        // //   SupplierAccountingService supplierService = new SupplierAccountingService();
        // //   List<SupplierOrderDataType> orderList = supplierService.GetSupplierOrders(gSupplerGuid);
        ////   var orderList = supplierService.GetSupplierOrders(gSupplerGuid);

        //    var model = new SupplierOrdersResponseModel(gSupplerGuid, gSupplerGuid);
        //    var theResponse = model.Response;
        //}
    }
}

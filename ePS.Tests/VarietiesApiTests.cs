﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace ePS.Tests
{
    [TestClass]
    public class VarietiesApiTests
    {
        [TestMethod]
        public void VarietiesApiGetTest()
        {
            const string supplierCodes = "BUR,DUW,HDS,LIN,MLM,PLU,RAK";
            const string speciesCodes = "ABU,ACA,ACH,AGA,AGE,AJA,ALT,ANA,ANG,ARG,ART,ASP,BAC,BEA,BEP,BEG,BID,BRY,BRA,CLN,CAL,CPT,CAN,CAR,CAS,CEL,CER,CHL,CHR,CIN,CLE,CLR,COL,CLC,CVL,COP,CRD,COR,COS,CRO,CUP,DAH,DIA,DIS,DIC,DIP,DOR,DRA,DUR,ERY,EUP,EUR,EVO,FEL,FCH,GAL,GAU,GAZ,GER,GMI,GEU,GLE,GRA,GYP,HEL,HTP,HMZ,HEU,HEC,HOU,HYP,IBE,IMP,NGI,IMW,IPO,IRE,IVY,KWK,LAM,LAN,LVT,LAV,LEU,LIT,LOL,LOB,LOP,LOT,LYS,MAN,MXS,MUE,NAS,NEM,NEP,NIE,OEN,OST,OTO,OXA,PAC,PEN,PAS,PER,PSK,PRS,PTC,PET,PHL,PLE,PLU,POR,PSE,RAN,RHO,RUD,RUE,SAL,SAN,SCB,SCA,SED,SET,SNA,STE,STR,THU,TOR,WNJ,TRI,TUR,VER,VRN,VIN,VIV,VIO";

            var list = new List<Species>();

            string.IsNullOrEmpty(supplierCodes).Should().BeFalse();
            string.IsNullOrEmpty(speciesCodes).Should().BeFalse();

            string[] supplierCodesArray = supplierCodes.Split(',');
            string[] speciesCodesArray = speciesCodes.Split(',');

            var service = new VarietyService();
            var userGuid = new Guid();
            var result = service.SelectVarietiesGroupedBySpecies(userGuid:userGuid,plantCategoryCode: "VA", productFormCode: "RC", supplierCodeList: supplierCodesArray, speciesCodeList: speciesCodesArray);

            foreach (var varietySet in result)
            {
                System.Console.WriteLine("{0} {1}", varietySet.Code, varietySet.Name);

                foreach (var selectedVariety in varietySet.SelectedVarieties)
                {
                    System.Console.WriteLine("    {0} {1}", selectedVariety.Code, selectedVariety.Name);
                }
            }
        }
    }
}

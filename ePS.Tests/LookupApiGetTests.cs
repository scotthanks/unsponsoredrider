﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ePS.Models.LookupApi;
using BusinessObjectServices;
using FluentAssertions;

namespace ePS.Tests
{
    [TestClass]
    public class LookupApiGetTests
    {
        [TestMethod]
        public void LookupApiGetTest()
        {
            var request = new GetRequestModel();

            var userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));
            var response = new GetResponseModel(userGuid, request);

            response.IsAuthenticated.Should().BeTrue();
            response.LookupList.Count.Should().BeGreaterThan(100);

            int count = 0;
            foreach (var lookup in response.LookupList)
            {
                System.Console.WriteLine("Guid={0}, Code={1}, DisplayName={2}, IsDefault={3}, IsSelected={4}, ParentPath={5}", lookup.Guid, lookup.Code, lookup.DisplayName, lookup.IsDefault, lookup.IsSelected, lookup.ParentPath);

                lookup.Guid.Should().NotBeEmpty();
                lookup.Code.Should().NotBeNullOrEmpty();
                lookup.DisplayName.Should().NotBeNullOrEmpty();
                lookup.IsDefault.Should().BeFalse();
                lookup.IsSelected.Should().BeFalse();

                if (count++ >= 10)
                    break;
            }
        }
    }
}

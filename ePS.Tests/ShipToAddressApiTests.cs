﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using DataServiceLibrary;

namespace ePS.Tests
{
    [TestClass]
    public class ShipToAddressApiTests
    {
        //[TestMethod]
        //public void ShipToAddressApiAddTest()
        //{
        //    Guid userGuid = new Guid("7714B7E5-7148-404B-9E2E-93EE8B7E3B63");
        //    const string name = "Test Name";
        //    const string streetAddress1 = "123 Pleasant Street";
        //    const string streetAddress2 = "Apartment #1";
        //    const string city = "New Peace";
        //    const string stateCode = "CA";
        //    const string zipCode = "12345";
        //    const string country = "USA";
        //    const string phoneNumber = "415-321-9950";
        //    const string specialInstructions = "Use back door.";

        //    var request = new ePS.Models.ShipToAddressApi.PostRequestModel()
        //        {
        //            Name = name,
        //            StreetAddress1 = streetAddress1,
        //            City = city,
        //            StateCode = stateCode,
        //            ZipCode = zipCode,
        //            PhoneNumber = phoneNumber,
        //            SpecialInstructions = specialInstructions
        //        };

        //    var response = new ePS.Models.ShipToAddressApi.PostResponseModel(userGuid, request);

        //    response.IsAuthenticated.Should().BeTrue();
        //    response.Message.Should().Be("");

        //    response.GrowerAddressGuid.Should().NotBeEmpty();
        //    response.AddressGuid.Should().NotBeEmpty();

        //    var growerAddressGuid = response.GrowerAddressGuid;

        //    var growerAddressTableService = new GrowerAddressTableService();
        //    var growerAddress = growerAddressTableService.GetByGuid(growerAddressGuid);
        //    growerAddress.Should().NotBeNull();
        //    growerAddress.GrowerGuid.Should().NotBeEmpty();
        //    var address = growerAddress.Address;
        //    address.Should().NotBeNull();
        //    address.Name.Should().Be(name);
        //    address.StreetAddress1.Should().Be(streetAddress1);
        //    address.StreetAddress2.Should().BeEmpty();
        //    address.City.Should().Be(city);
        //    address.State.Code.Should().Be(stateCode);
        //    address.ZipCode.Should().Be(zipCode);

        //    growerAddressTableService.Delete(growerAddress);

        //    var addressTableService = new AddressTableService();
        //    addressTableService.Delete(address);
        //}

        //[TestMethod]
        //public void ShipToAddressApiAddWithNullPhoneNumberTest()
        //{
        //    Guid userGuid = new Guid("7714B7E5-7148-404B-9E2E-93EE8B7E3B63");
        //    const string name = "Test Name";
        //    const string streetAddress1 = "123 Pleasant Street";
        //    const string streetAddress2 = "Apartment #1";
        //    const string city = "New Peace";
        //    const string stateCode = "CA";
        //    const string zipCode = "12345";
        //    const string country = "USA";
        //    const string phoneNumber = null;
        //    const string specialInstructions = "Use back door.";

        //    var request = new ePS.Models.ShipToAddressApi.PostRequestModel()
        //    {
        //        Name = name,
        //        StreetAddress1 = streetAddress1,
        //        City = city,
        //        StateCode = stateCode,
        //        ZipCode = zipCode,
        //        PhoneNumber = phoneNumber,
        //        SpecialInstructions = specialInstructions
        //    };

        //    var response = new ePS.Models.ShipToAddressApi.PostResponseModel(userGuid, request);

        //    response.IsAuthenticated.Should().BeTrue();
        //    response.Message.Should().Be("");

        //    response.GrowerAddressGuid.Should().NotBeEmpty();
        //    response.AddressGuid.Should().NotBeEmpty();

        //    var growerAddressGuid = response.GrowerAddressGuid;

        //    var growerAddressTableService = new GrowerAddressTableService();
        //    var growerAddress = growerAddressTableService.GetByGuid(growerAddressGuid);
        //    growerAddress.Should().NotBeNull();
        //    growerAddress.GrowerGuid.Should().NotBeEmpty();
        //    var address = growerAddress.Address;
        //    address.Should().NotBeNull();
        //    address.Name.Should().Be(name);
        //    address.StreetAddress1.Should().Be(streetAddress1);
        //    address.StreetAddress2.Should().BeEmpty();
        //    address.City.Should().Be(city);
        //    address.State.Code.Should().Be(stateCode);
        //    address.ZipCode.Should().Be(zipCode);

        //    growerAddressTableService.Delete(growerAddress);

        //    var addressTableService = new AddressTableService();
        //    addressTableService.Delete(address);
        //}

        //[TestMethod]
        //public void ShipToAddressApiGetTest()
        //{
        //    Guid userGuid = new Guid("7714B7E5-7148-404B-9E2E-93EE8B7E3B63");

        //    var request = new ePS.Models.ShipToAddressApi.GetRequestModel()
        //    {
        //        GrowerGuid = Guid.Empty,
        //        GrowerAddressGuid = Guid.Empty,
        //        GrowerOrderGuid = Guid.Empty
        //    };

        //    var response = new ePS.Models.ShipToAddressApi.GetResponseModel(userGuid, request);

        //    response.IsAuthenticated.Should().BeTrue();
        //    response.Message.Should().Be("");

        //    response.ShipToAddressList.Length.Should().BeGreaterOrEqualTo(1);

        //    bool selectedAddressFound = false;
        //    foreach (var shipToAddress in response.ShipToAddressList)
        //    {
        //        shipToAddress.GrowerAddressGuid.Should().NotBeEmpty();
        //        shipToAddress.AddressGuid.Should().NotBeEmpty();
        //        shipToAddress.Name.Should().NotBeNullOrEmpty();
        //        //shipToAddress.Name.Should().Be(name);
        //        shipToAddress.StreetAddress1.Should().NotBeNullOrEmpty();
        //        shipToAddress.StreetAddress2.Should().NotBeNull();
        //        shipToAddress.City.Should().NotBeNullOrEmpty();
        //        shipToAddress.StateCode.Should().NotBeNullOrEmpty();

        //        if (shipToAddress.IsSelected)
        //        {
        //            selectedAddressFound = true;
        //        }
        //    }
        //    selectedAddressFound.Should().BeFalse();
        //}

        //[TestMethod]
        //public void ShipToAddressApiGetWithGrowerOrderTest()
        //{
        //    Guid userGuid = new Guid("7714B7E5-7148-404B-9E2E-93EE8B7E3B63");

        //    var request = new ePS.Models.ShipToAddressApi.GetRequestModel()
        //    {
        //        GrowerGuid = Guid.Empty,
        //        GrowerAddressGuid = Guid.Empty,
        //        GrowerOrderGuid = new Guid("B98C1AF9-906B-4F76-AF1D-00429372F0D9")
        //    };

        //    var response = new ePS.Models.ShipToAddressApi.GetResponseModel(userGuid, request);

        //    response.IsAuthenticated.Should().BeTrue();
        //    response.Message.Should().Be("");

        //    response.ShipToAddressList.Length.Should().BeGreaterOrEqualTo(1);

        //    bool selectedAddressFound = false;
        //    foreach (var shipToAddress in response.ShipToAddressList)
        //    {
        //        shipToAddress.GrowerAddressGuid.Should().NotBeEmpty();
        //        shipToAddress.AddressGuid.Should().NotBeEmpty();
        //        shipToAddress.Name.Should().NotBeNullOrEmpty();
        //        //shipToAddress.Name.Should().Be(name);
        //        shipToAddress.StreetAddress1.Should().NotBeNullOrEmpty();
        //        shipToAddress.StreetAddress2.Should().NotBeNull();
        //        shipToAddress.City.Should().NotBeNullOrEmpty();
        //        shipToAddress.StateCode.Should().NotBeNullOrEmpty();

        //        if (shipToAddress.IsSelected)
        //        {
        //            selectedAddressFound = true;
        //        }
        //    }
        //    selectedAddressFound.Should().BeTrue();
        //}
    }
}

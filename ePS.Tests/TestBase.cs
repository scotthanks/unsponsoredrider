﻿using System;
using BusinessObjectsLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace ePS.Tests
{
    [TestClass]
    public abstract class TestBase : BusinessObjectServicesUnitTestLibrary.TestBase
    {
        public void CheckResponse(ePS.Models.ResponseBase response, bool requireSuccess = true)
        {
            if (response.Messages.Length > 0)
            {
                System.Console.WriteLine("Response Messages:");
                foreach (var message in response.Messages)
                {
                    System.Console.WriteLine("\t" + message);
                }
            }

            System.Console.WriteLine("response.Success: " + response.Success.ToString());
            if (!response.Success && requireSuccess)
            {
                response.Success.Should().BeTrue();
            }
            System.Console.WriteLine();
        }

        public ShipWeek GetFirstShipWeek()
        {
            var sqlDataServices = new DataAccessLibrary.SqlQueryServices();
            var shipWeekService = new DataServiceLibrary.ShipWeekTableService();

            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            var reader = sqlDataServices.ReaderFromSqlQuery(connection, "SELECT TOP 1 * FROM ShipWeek ORDER BY MondayDate");
            var shipWeekList = shipWeekService.GetNFromReader(reader, 1);
            connection.Close();

            DataObjectLibrary.ShipWeek shipWeekTableObject = shipWeekList[0];

            return new ShipWeek(shipWeekTableObject);
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using DataServiceLibrary;
using BusinessObjectServices;


namespace ePS.Tests
{
    [TestClass]
    public class CartUpdateApiTests
    {
        [TestMethod]
        public void CartUpdateApiCustomerPoNoTest()
        {
            Guid growerOrderGuid = new Guid(new LookupService().GetConfigValue("CART_ORDER_GUID"));
            var request = new ePS.Models.CartUpdateApi.PostRequestModel
                {
                    FieldName = "CustomerPoNo",
                    GrowerOrderGuid = growerOrderGuid,
                    CustomerPoNo = "PO#: " + DateTime.Now.ToString("dd-MMM-yy hh:mm")
                };

            var controller = new ePS.Controllers.api.CartUpdateController();
            var response = new ePS.Models.CartUpdateApi.PostResponseModel(new Guid("7714B7E5-7148-404B-9E2E-93EE8B7E3B63"), request);

            response.Success.Should().Be(true);
            response.Message.Should().Be("");

            var growerOrderService = new DataServiceLibrary.GrowerOrderTableService();
            var growerOrder = growerOrderService.GetByGuid(growerOrderGuid);
            growerOrder.Should().NotBeNull();
            growerOrder.CustomerPoNo.Should().Be(request.CustomerPoNo);
        }

        [TestMethod]
        public void CartUpdateApiOrderDescriptionTest()
        {
            Guid growerOrderGuid = new Guid(new LookupService().GetConfigValue("CART_ORDER_GUID"));
            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

            var request = new ePS.Models.CartUpdateApi.PostRequestModel
            {
                FieldName = "OrderDescription",
                GrowerOrderGuid = growerOrderGuid,
                OrderDescription = "Desc is " + DateTime.Now.ToString("dd-MMM-yy hh:mm"),
            };

            var controller = new ePS.Controllers.api.CartUpdateController();
            var response = new ePS.Models.CartUpdateApi.PostResponseModel(userGuid, request);

            response.Success.Should().Be(true);
            response.Message.Should().Be("");

            var growerOrderService = new DataServiceLibrary.GrowerOrderTableService();
            var growerOrder = growerOrderService.GetByGuid(growerOrderGuid);
            growerOrder.Should().NotBeNull();
            growerOrder.Description.Should().Be(request.OrderDescription);
        }

        [TestMethod]
        public void CartUpdateApiOrderPaymentTypeLookupCodeTest()
        {
            var growerOrderService = new DataServiceLibrary.GrowerOrderTableService();
            Guid growerOrderGuid = new Guid(new LookupService().GetConfigValue("CART_ORDER_GUID"));
            var growerOrder = growerOrderService.GetByGuid(growerOrderGuid);
            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

            var lookupService = new DataServiceLibrary.LookupTableService();
            var creditCardPaymentTypeLookup = LookupTableValues.Code.PaymentType.Credit;
            
            growerOrder.Should().NotBeNull();
            growerOrder.PaymentTypeLookupGuid = creditCardPaymentTypeLookup.Guid;
            growerOrderService.Update(growerOrder);

            growerOrder = growerOrderService.GetByGuid(growerOrderGuid);
            growerOrder.PaymentTypeLookup.Code.Should().Be("Credit");

            var request = new ePS.Models.CartUpdateApi.PostRequestModel
            {
                FieldName = "PaymentTypeLookupCode",
                GrowerOrderGuid = growerOrderGuid,
                PaymentTypeLookupCode = "Invoice",
            };

            var controller = new ePS.Controllers.api.CartUpdateController();
            var response = new ePS.Models.CartUpdateApi.PostResponseModel(userGuid, request);

            response.Success.Should().Be(true);
            response.Message.Should().Be("");

            growerOrderService.ClearCache();

            growerOrder = growerOrderService.GetByGuid(growerOrderGuid);
            growerOrder.Should().NotBeNull();
            growerOrder.PaymentTypeLookup.Code.Should().Be("Invoice");
        }

        [TestMethod]
        public void CartUpdateApiCardOnFileGuidTest()
        {

            Guid growerOrderGuid = new Guid(new LookupService().GetConfigValue("CART_ORDER_GUID"));
            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

            Guid creditCardGuid1 = new Guid("C07CCC0B-B0AD-4301-9449-CD845ABBE922");
            Guid creditCardGuid2 = new Guid("6E0C0AF2-FEF8-4B8A-9005-17D8806BD8FB");

            var growerOrderService = new DataServiceLibrary.GrowerOrderTableService();
            var growerOrder = growerOrderService.GetByGuid(growerOrderGuid);
            growerOrder.GrowerCreditCardGuid = creditCardGuid2;
            growerOrderService.Update(growerOrder);
            growerOrder = growerOrderService.GetByGuid(growerOrderGuid);
            growerOrder.GrowerCreditCardGuid.Should().Be(creditCardGuid2);            

            var request = new ePS.Models.CartUpdateApi.PostRequestModel
            {
                FieldName = "CardOnFileGuid",
                GrowerOrderGuid = growerOrderGuid,
                CardOnFileGuid = creditCardGuid1,
            };

            var controller = new ePS.Controllers.api.CartUpdateController();
            var response = new ePS.Models.CartUpdateApi.PostResponseModel(userGuid, request);

            response.Success.Should().Be(true);
            response.Message.Should().Be("");

            growerOrderService.ClearCache();
            growerOrder = growerOrderService.GetByGuid(growerOrderGuid);
            growerOrder.Should().NotBeNull();
            growerOrder.GrowerCreditCardGuid.Should().Be(request.CardOnFileGuid);
            growerOrder.GrowerCreditCardGuid.Should().Be(creditCardGuid1);
        }

        [TestMethod]
        public void CartUpdateApiAgreeToTermsTest()
        {
            Guid growerOrderGuid = new Guid(new LookupService().GetConfigValue("CART_ORDER_GUID"));
            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));
            
            var growerOrderService = new DataServiceLibrary.GrowerOrderTableService();
            var growerOrder = growerOrderService.GetByGuid(growerOrderGuid);
            growerOrderService.Update(growerOrder);
            growerOrder = growerOrderService.GetByGuid(growerOrderGuid);

            var request = new ePS.Models.CartUpdateApi.PostRequestModel
            {
                FieldName = "AgreeToTerms",
                GrowerOrderGuid = growerOrderGuid,
                AgreeToTerms = false
            };

            var controller = new ePS.Controllers.api.CartUpdateController();
            var response = new ePS.Models.CartUpdateApi.PostResponseModel(userGuid, request);

            response.Success.Should().Be(false);
            response.Message.Should().Be("You must agree to the Terms and Conditions.");

            request.AgreeToTerms = true;
            response = new ePS.Models.CartUpdateApi.PostResponseModel(userGuid, request);

            response.Success.Should().Be(true);
            response.Message.Should().Be("");

            growerOrderService.ClearCache();
            growerOrder = growerOrderService.GetByGuid(growerOrderGuid);
            growerOrder.Should().NotBeNull();
        }

        [TestMethod]
        public void CartUpdateApiShipMethodLookupTest()
        {
            Guid supplierOrderGuid = new Guid(new LookupService().GetConfigValue("CART_SUPPLIERORDER_GUID"));
            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));

            var lookupService = new DataServiceLibrary.LookupTableService();
            var shipMethodLookupTruck = LookupTableValues.Code.ShipMethod.Truck;
            var shipMethodLookupUps = LookupTableValues.Code.ShipMethod.Fedex;

            var supplierOrderService = new DataServiceLibrary.SupplierOrderTableService();
            var supplierOrder = supplierOrderService.GetByGuid(supplierOrderGuid);
            supplierOrder.ShipMethodLookupGuid = shipMethodLookupTruck.Guid;
            supplierOrder = supplierOrderService.Update(supplierOrder);
            supplierOrder.ShipMethodLookupGuid.Should().Be(shipMethodLookupTruck.Guid);
            supplierOrder = supplierOrderService.GetByGuid(supplierOrderGuid);
            supplierOrder.ShipMethodLookupGuid.Should().Be(shipMethodLookupTruck.Guid);

            var request = new ePS.Models.CartUpdateApi.PostRequestModel
            {
                FieldName = "ShipMethodLookupCode",
                SupplierOrderGuid = supplierOrderGuid,
                ShipMethodLookupCode = "FEDEX",
            };

            var controller = new ePS.Controllers.api.CartUpdateController();
            var response = new ePS.Models.CartUpdateApi.PostResponseModel(userGuid, request);

            response.Message.Should().Be("");
            response.Success.Should().Be(true);

            supplierOrderService.ClearCache();
            supplierOrder = supplierOrderService.GetByGuid(supplierOrderGuid);
            supplierOrder.ShipMethodLookupGuid.Should().Be(shipMethodLookupUps.Guid);
        }

        [TestMethod]
        public void CartUpdateApiRequestInvalidTest()
        {
            Guid personGuid = Guid.NewGuid();
            Guid growerOrderGuid = Guid.NewGuid();
            Guid supplierOrderGuid = Guid.NewGuid();

            // Invalid field name.
            var request = new ePS.Models.CartUpdateApi.PostRequestModel
            {
                FieldName = "NotAValidField",
                GrowerOrderGuid = growerOrderGuid,
                ShipMethodLookupCode = "FEDEX",
            };

            var controller = new ePS.Controllers.api.CartUpdateController();
            var response = new ePS.Models.CartUpdateApi.PostResponseModel(personGuid, request);

            System.Console.WriteLine( string.Format( "Message = \"{0}\"", response.Message));
            response.Success.Should().BeFalse();
            response.Message.Should().NotBeBlank();
            response.Message.Should().Be("The request is not valid.");

            // Wrong order guid set.
            request = new ePS.Models.CartUpdateApi.PostRequestModel
            {
                FieldName = "CustomerPoNo",
                SupplierOrderGuid = supplierOrderGuid,
                CustomerPoNo = "A PO Number",
            };

            response = new ePS.Models.CartUpdateApi.PostResponseModel(personGuid, request);

            System.Console.WriteLine( string.Format( "Message = \"{0}\"", response.Message));
            response.Success.Should().BeFalse();
            response.Message.Should().NotBeBlank();
            response.Message.Should().Be("The request is not valid.");

            // Wrong field value set.
            request = new ePS.Models.CartUpdateApi.PostRequestModel
            {
                FieldName = "CustomerPoNo",
                GrowerOrderGuid = growerOrderGuid,
                OrderDescription = "Wrong field value set.",
            };

            response = new ePS.Models.CartUpdateApi.PostResponseModel(personGuid, request);

            System.Console.WriteLine(string.Format("Message = \"{0}\"", response.Message));
            response.Success.Should().BeFalse();
            response.Message.Should().NotBeBlank();
            response.Message.Should().Be("The request is not valid.");            
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void CartUpdateApiExceptionThrownTest()
        {
            Guid personGuid = Guid.NewGuid();
            Guid growerOrderGuid = Guid.NewGuid();
            Guid supplierOrderGuid = Guid.NewGuid();

            var request = new ePS.Models.CartUpdateApi.PostRequestModel
            {
                FieldName = "OrderDescription",
                GrowerOrderGuid = growerOrderGuid,
                OrderDescription = "Appears Valid with bad guids.",
            };

            var controller = new ePS.Controllers.api.CartUpdateController();
            var response = new ePS.Models.CartUpdateApi.PostResponseModel(personGuid, request);
        }
    }
}

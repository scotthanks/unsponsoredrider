﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessObjectsLibrary;
using ePSAdmin.Models.OrderDetailApi;
using ePS.Models.OrderApi;
using DataServiceLibrary;
using ePS.Models.AdminAppOrderDetailApi;
using AdminAppApiLibrary;
using ePS.Models.CartApi;

namespace ePS.Tests
{
    [TestClass]
    public class GrowerAckEmailTests : TestBase
    {
        [TestMethod]
        public void CreatEmailNoFeesTest()
        {
          //  CreatEmailTest(false);
        }

        [TestMethod]
        public void CreatEmailFeesTest()
        {
          //  CreatEmailTest(true);
        }

        [TestMethod]
        public void CreatEmailTestA0072980()
        {

            //A0072980  
            var gGuid = new Guid("A46D9592-1C16-42A5-9615-8680D2330B43");
            try
            {
                // C:\Latitude40\ePlantMain-8-13-clone\ePS\Views\CustomerEmail
                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;

                path = path.Substring(0, path.IndexOf("ePS.Tests"));
                //  path += "ePS\\Views\\CustomerEmail\\GrowerAckEmail.html";
                path += "ePS\\Views\\CustomerEmail\\GrowerOrderEmail.html";



                var service = new BusinessObjectServices.GrowerOrderService();
                var growerOrder = service.GetGrowerOrderDetail(gGuid, includePriceData: false);
                var responseModel = new GetGrowerOrderResponseModel(growerOrder);

                EmailModel emailModel = new EmailModel(-1, growerOrder.ShipWeek.ShipWeekString, path, responseModel.GrowerOrder, new OrderStatusNameService(),"hi","there");
                string emailText = emailModel.ApplyTemplate();

            }
            catch (Exception ex)
            {
                Assert.IsTrue(false);
            }

        }


        ////[TestMethod]
        //private void CreatEmailTest(bool withFees)
        //{
        //    GrowerOrder placedOrder = null;

        //    try
        //    {
        //        // C:\Latitude40\ePlantMain-8-13-clone\ePS\Views\CustomerEmail
        //        string path = System.Reflection.Assembly.GetExecutingAssembly().Location;

        //        path = path.Substring(0, path.IndexOf("ePS.Tests"));
        //      //  path += "ePS\\Views\\CustomerEmail\\GrowerAckEmail.html";
        //        path += "ePS\\Views\\CustomerEmail\\GrowerOrderEmail.html";
        //        placedOrder = CreatePlacedGrowerOrder(UserGuidGib, UserCodeScott, 100, 5);

        //        if (withFees)
        //        {
        //            CreateOrderFeeRequest orderFeeRequest = new CreateOrderFeeRequest();
        //            orderFeeRequest.Guid = Guid.NewGuid();
        //            orderFeeRequest.GrowerOrderGuid = placedOrder.OrderGuid;
        //            orderFeeRequest.Amount = (decimal)25.0;
        //            orderFeeRequest.UserCode = "Gib";
        //            orderFeeRequest.GrowerOrderFeeTypeCode = "Carrying                                          ";

        //            var orderFeeModel = new CreateOrderFeeResponseModel(UserGuidGib, orderFeeRequest);
        //            Assert.IsTrue(orderFeeModel.Response.Success);

        //            orderFeeRequest.Guid = Guid.NewGuid();
        //            orderFeeRequest.Amount = (decimal)-25.0;
        //            orderFeeRequest.UserCode = "Gib";
        //            orderFeeRequest.GrowerOrderFeeTypeCode = "EPS Discount                                      ";
        //            orderFeeModel = new CreateOrderFeeResponseModel(UserGuidGib, orderFeeRequest);
        //            Assert.IsTrue(orderFeeModel.Response.Success);

        //            BusinessObjectServices.GrowerOrderService growerOrderService = new BusinessObjectServices.GrowerOrderService();
        //            foreach (var order in placedOrder.SupplierOrders)
        //            {
        //                growerOrderService.AddSupplierFee(order.SupplierOrderGuid, "Freight", (decimal)55.0, "Freight Fee", "Actual");
        //                growerOrderService.AddSupplierFee(order.SupplierOrderGuid, "RestockingFee", (decimal)45.0, "Stocking Fee", "TBD");
        //            }
        //        }

        //        var service = new BusinessObjectServices.GrowerOrderService();
        //        var growerOrder = service.GetGrowerOrderDetail(placedOrder.OrderGuid, includePriceData: false);
        //        var responseModel = new GetGrowerOrderResponseModel(growerOrder);

        //        EmailModel emailModel = new EmailModel(-1, placedOrder.ShipWeek.ShipWeekString, path, responseModel.GrowerOrder, new OrderStatusNameService());
        //        string emailText = emailModel.ApplyTemplate();

        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.IsTrue(false);
        //    }
        //    finally
        //    {
        //        if (placedOrder != null)
        //            DeleteAnyOrder(placedOrder);
        //    }
        //}

        

    }
}

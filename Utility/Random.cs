﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class Random
    {
        private static System.Random _random;

        public Random()
        {
            if (_random == null)
            {
                _random = new System.Random();
            }
        }

        public bool GetRandomBool()
        {
            return _random.Next(2) == 1;
        }

        public int GetRandomIntLessThan(int maxValue = int.MaxValue)
        {
            return _random.Next(maxValue);
        }

        public static bool RandomBool
        {
            get
            {
                return new Random().GetRandomBool();
            }
        }

        public static int RandomInt
        {
            get
            {
                return new Random().GetRandomIntLessThan();
            }
        }
    }
}

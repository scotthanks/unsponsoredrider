﻿using System.Text;
using System.Text.RegularExpressions;

namespace Utility
{
    public class PhoneNumber
    {
        private const bool RequireRegExMatchDefault = true;
        private const string PhoneNumberPattern = @"^(?:\+?1[-. ]?)?\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$";

        private const bool AllowEmptyDefault = true;
        public bool AllowEmpty { get; set; }
        public bool RequireRegExMatch { get; set; }

        public bool IsValid { get; private set; }

        private PhoneNumber(bool allowEmpty = AllowEmptyDefault, bool requireRegExMatch = RequireRegExMatchDefault)
        {
            AllowEmpty = allowEmpty;
            RequireRegExMatch = requireRegExMatch;
        }

        public PhoneNumber(string phoneNumberString, bool allowEmpty = AllowEmptyDefault, bool requireRegExMatch = RequireRegExMatchDefault)
            : this(allowEmpty, requireRegExMatch)
        {
            IsValid = false;

            if (!RequireRegExMatch || MatchesRegEx(phoneNumberString))
            {
                var digitsOnly = new StringBuilder();

                foreach (char digit in phoneNumberString)
                {
                    if (char.IsDigit(digit))
                    {
                        digitsOnly.Append(digit);
                    }
                }

                phoneNumberString = digitsOnly.ToString();

                if (phoneNumberString.Length == 0 && AllowEmpty)
                {
                    IsValid = true;
                }
                else if (phoneNumberString.Length == 10)
                {
                    decimal areaCode = 0;
                    decimal localPhoneNumber = 0;

                    decimal.TryParse(phoneNumberString.Substring(0, 3), out areaCode);
                    decimal.TryParse(phoneNumberString.Substring(3, 7), out localPhoneNumber);

                    if (areaCode == 0 && localPhoneNumber == 0 && AllowEmpty)
                    {
                        IsValid = true;
                    }
                    else if (!RequireRegExMatch || MatchesRegEx(FormatPhoneNumber(areaCode, localPhoneNumber)))
                    {
                        AreaCode = areaCode;
                        LocalPhoneNumber = localPhoneNumber;

                        if (IsInRange)
                        {
                            IsValid = true;
                        }
                        else
                        {
                            Clear();
                        }
                    }
                }
            }
        }

        public PhoneNumber(decimal areaCode, decimal phone, bool allowEmpty = AllowEmptyDefault, bool requireRegExMatch = RequireRegExMatchDefault)
            : this(allowEmpty, requireRegExMatch)
        {
            AreaCode = areaCode;
            LocalPhoneNumber = phone;

            if (RequireRegExMatch)
            {
                IsValid = MatchesRegEx(FormattedPhoneNumber);
            }
            if (!IsInRange)
            {
                Clear();
            }
            else
            {
                if (!AllowEmpty)
                {
                    IsValid = !IsEmpty;
                }
                else if (IsInRange)
                {
                    IsValid = true;
                }                
            }
        }

        public decimal AreaCode { get; private set; }
        public decimal LocalPhoneNumber { get; private set; }

        public string FormattedPhoneNumber
        {
            get { return FormatPhoneNumber(AreaCode, LocalPhoneNumber); }
        }

        public string UnformattedPhoneNumber
        {
            get { return UnformatPhoneNumber(AreaCode, LocalPhoneNumber); }
        }

        public bool IsEmpty
        {
            get { return AreaCode == 0 && LocalPhoneNumber == 0; }
        }

        private bool IsInRange
        {
            get { return AreaCode >= 0 && AreaCode <= 999 && LocalPhoneNumber >= 0 && LocalPhoneNumber <= 9999999; }
        }

        public void Clear()
        {
            AreaCode = 0;
            LocalPhoneNumber = 0;
        }

        public static bool MatchesRegEx(string phoneNumberString)
        {
            var regex = new Regex(PhoneNumberPattern);
            return regex.IsMatch(phoneNumberString ?? "");            
        }

        public static string FormatPhoneNumber(decimal areaCode, decimal phoneNumber)
        {
            if (areaCode == 0 && phoneNumber == 0)
                return "";
            else
                return string.Format
                    (
                        "({0:000}) {1:000}-{2:0000}",
                        areaCode,
                        decimal.Truncate(phoneNumber/10000),
                        phoneNumber - (decimal.Truncate(phoneNumber/10000)*10000)
                    );
        }

        public static string UnformatPhoneNumber(decimal areaCode, decimal phoneNumber)
        {
            if (areaCode == 0 && phoneNumber == 0)
                return "";
            else
                return string.Format
                    (
                        "{0:000}{1:0000000}",
                        areaCode,
                        phoneNumber
                    );            
        }

        public static bool IsValidPhoneNumber(string phoneNumberString, bool allowEmpty = false, bool requireRegExMatch = RequireRegExMatchDefault)
        {
            var phoneNumber = new PhoneNumber(phoneNumberString, allowEmpty, requireRegExMatch);
            return phoneNumber.IsValid;
        }
    }
}
﻿using System;
using System.Text;

namespace Utility
{
    public static class Logger
    {
        public enum LogType
        {
            Debug,
            ServerCode
        }

        public enum Severity
        {
            Failure,
            Error,
            Warning,
            Performance,
            Info,
            Debug
        }

        public enum Priority
        {
            High,
            Medium,
            Low
        }

        public enum Verbosity
        {
            Minimal,
            Terse,
            Normal,
            Verbose
        }

        public delegate void LogDelegate
        (
            Guid userGuid,
            string userCode,
            string processCode,
            string comment,
            LogType logType,
            Severity severity,
            Priority priority,
            Verbosity verbosity,
            string objectTypeCode,
            Guid? objectGuid = null,
            long? intValue = null,
            long? processId = 0,
            float? floatValue = null,
            Guid? guidValue = null,
            string xmlData = null
        );

        private static LogDelegate _logDelegate;

        public static void SetLogDelegate(LogDelegate logDelegate)
        {
            _logDelegate = logDelegate;
        }

        public static void Log
        (
            Guid userGuid,
            string userCode,
            string processCode,
            string comment,
            LogType logType,
            Severity severity,
            Priority priority,
            Verbosity verbosity,
            string objectTypeCode = null,
            Guid? objectGuid = null,
            long? intValue = null,
            long? processId = 0,
            float? floatValue = null,
            Guid? guidValue = null,
            string xmlData = null
        )
        {
            if (_logDelegate != null)
            {
                _logDelegate
                    (
                        userGuid,
                        userCode,
                        processCode,
                        comment,
                        logType,
                        severity,
                        priority,
                        verbosity,
                        objectTypeCode,
                        objectGuid,
                        intValue,
                        processId,
                        floatValue,
                        guidValue,
                        xmlData
                    );
            }
            else
            {
                var stringBuilder = new StringBuilder();
                stringBuilder.AppendFormat("userGuid: {0}", userGuid);
                stringBuilder.Append(",");
                stringBuilder.AppendFormat("userCode: {0}", userCode);
                stringBuilder.Append(",");
                stringBuilder.AppendFormat("comment: {0}", comment);
                stringBuilder.Append(",");
                stringBuilder.AppendFormat("processCode: {0}", processCode);
                stringBuilder.Append(",");
                stringBuilder.AppendFormat("logTypeCode: {0}", logType);
                stringBuilder.Append(",");
                stringBuilder.AppendFormat("verbosity: {0}", verbosity.ToString());
                stringBuilder.Append(",");
                stringBuilder.AppendFormat("priority: {0}", priority.ToString());
                stringBuilder.Append(",");
                stringBuilder.AppendFormat("severity: {0}", severity.ToString());
                stringBuilder.Append(",");
                stringBuilder.AppendFormat("objectTypeCode: {0}", objectTypeCode);
                stringBuilder.Append(",");
                stringBuilder.AppendFormat("objectGuid: {0}", objectGuid);
                stringBuilder.Append(",");
                stringBuilder.AppendFormat("intValid: {0}", intValue);
                stringBuilder.Append(",");
                stringBuilder.AppendFormat("processId: {0}", processId);
                stringBuilder.Append(",");
                stringBuilder.AppendFormat("floatValid: {0}", floatValue);
                stringBuilder.Append(",");
                stringBuilder.AppendFormat("guidValue: {0}", guidValue);
                stringBuilder.Append(",");
                stringBuilder.AppendFormat("xmlData: {0}", xmlData);

                Console.WriteLine(stringBuilder.ToString());
            }
        }
    }
}
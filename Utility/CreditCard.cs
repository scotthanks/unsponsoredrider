﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Utility
{
    public class CreditCard
    {
        private const string CreditCardPattern =
            @"^((4\d{3})|(5[1-5]\d{2})|(6011)|(34\d{1})|(37\d{1}))-?\d{4}-?\d{4}-?\d{4}|3[4,7][\d\s-]{15}$";

        private static Regex Regex
        {
            get
            {
                return new System.Text.RegularExpressions.Regex(CreditCardPattern);
            }
        }

        public static bool IsValidCardNumber(string number)
        {
            return Regex.IsMatch(number);
        }

        public static bool IsValidExpirationDate(string expirationDate)
        {
            bool isValid = false;

            if (expirationDate == null)
                expirationDate = "";

            expirationDate = expirationDate.Trim();

            if (expirationDate.Length == 4)
            {
                int month = GetExpirationMonth(expirationDate);
                int year = GetExpirationYear(expirationDate);

                if (month > 0 && year > 0)
                {
                    var today = DateTime.Today;

                    if (year >= today.Year)
                    {
                        if (year > today.Year || month >= today.Month)
                        {
                            isValid = true;
                        }
                    }
                }
            }

            return isValid;
        }

        public static int GetExpirationMonth(string expirationDate)
        {
            int month = 0;
            string monthString = expirationDate.Substring(0, 2);
            month = GetExpirationDatePart(monthString);

            if (month > 12)
                month = 0;                

            return month;
        }

        public static int GetExpirationYear(string expirationDate)
        {
            int year = 0;
            string yearString = expirationDate.Substring(2, 2);
            year = GetExpirationDatePart(yearString);

            if (year > DateTime.Today.Year + 10)
            {
                year = 0;
            }

            if (year != 0)
            {
                year += 2000;
            }

            return year;
        }

        private static int GetExpirationDatePart(string expirationDatePart)
        {
            int part = 0;
            if (expirationDatePart.Length == 2 && char.IsDigit(expirationDatePart[0]) && char.IsDigit(expirationDatePart[1]))
            {
                if (!int.TryParse(expirationDatePart.Substring(0, 2), out part))
                    part = 0;
            }

            return part;
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessObjectServices;

namespace BusinessObjectServicesUnitTestLibrary
{
    [TestClass]
    public class ProductFormCategoryServiceUnitTest
    {
        [TestMethod]
        public void BusinessObjectServicesProductFormServiceTest()
        {
            var service = new ProductFormService();

            var productFormList = service.GetProductForms();

            foreach (var productForm in productFormList)
            {
                System.Console.WriteLine(productForm.ToString());
            }
        }
    }
}

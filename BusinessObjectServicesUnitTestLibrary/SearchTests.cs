﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;




namespace BusinessObjectServicesUnitTestLibrary
{
    [TestClass]
    public class SearchTests
    {
        [TestMethod]
        public void RunSearchTest()
        {
             Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));
             var status = new StatusObject(userGuid, true);
             var service = new SearchService(status);
            
            string theSearch = "Perfectunia";

            var returnData = service.RunSearch( theSearch, false);
            
            returnData.SearchString.Should().Be(theSearch);
        }

    }
}

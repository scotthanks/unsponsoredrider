﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using DataServiceLibrary;
using FluentAssertions;

namespace BusinessObjectServicesUnitTestLibrary
{
    [TestClass]
    public abstract class TestBase
    {
        private const string DefaultProgramTypeCode = "VA";
        private const string DefaultProductFormCategory = "URC";
        public readonly Guid UserGuidScott = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));
        public readonly Guid UserGuidGib = new Guid(new LookupService().GetConfigValue("GIB_USER_GUID"));
        public readonly string UserCodeScott = new LookupService().GetConfigValue("USER_CODE");
        
        public GrowerOrder CreatePlacedGrowerOrder(Guid userGuid, string userCode, int quantity, int numberOfOrderLinesToAdd = 1, string programTypeCode = DefaultProgramTypeCode, string productFormCategory = DefaultProductFormCategory)
        {
            var cartOrder = CreateCartOrder(userGuid, userCode, quantity, numberOfOrderLinesToAdd, programTypeCode, productFormCategory);
            PlaceOrder(userGuid, userCode, cartOrder.OrderGuid);
            var placedOrder = GetGrowerOrderDetail(userGuid, userCode, cartOrder.OrderGuid, includePriceData: false);
            placedOrder.OrderGuid.Should().Be(cartOrder.OrderGuid);
            return placedOrder;
        }

        public GrowerOrder CreateCartOrder(Guid userGuid, string userCode, int quantity, int numberOfOrderLinesToAdd = 1, string programTypeCode = DefaultProgramTypeCode, string productFormCategory = DefaultProductFormCategory)
        {
            var shipWeek = GetShipWeek(DateTime.Now.AddDays(30));
            var productList = GetAvailableProducts(userGuid, userCode, shipWeek, programTypeCode, productFormCategory, 100, 0, numberOfOrderLinesToAdd);

            foreach (var product in productList)
            {
                UpdateCartQuantity(userGuid, userCode, shipWeek, product, quantity, product.Price);
                AddToCart(userGuid, userCode, shipWeek, product.ProgramTypeCode, product.ProductFormCategoryCode);                
            }

            Guid growerOrderGuid = FindCartOrder(userGuid, userCode, shipWeek, productList[0].ProgramTypeCode, productList[0].ProductFormCategoryCode);

            var cartOrder = GetGrowerOrderDetail(userGuid, userCode, growerOrderGuid, includePriceData: false);

            return cartOrder;
        }

        public Product GetAvailableProduct(Guid userGuid, string userCode, ShipWeek shipWeek, string programTypeCode, string productFormCategoryCode, int quantity, int productsToSkip = 0, string supplierCode = null)
        {
            var productList = GetAvailableProducts(UserGuidScott, UserCodeScott, shipWeek, programTypeCode,
                                                   productFormCategoryCode, quantity, productsToSkip, supplierCode: supplierCode);

            return productList[0];
        }

        public List<Product> GetAvailableProducts(Guid userGuid, string userCode, ShipWeek shipWeek, string programTypeCode, string productFormCategoryCode, int quantityRequired, int productsToSkip = 0, int maxProductsToReturn = -1, string supplierCode = null)
        {
            var availableProductList = new List<Product>();
            var status = new StatusObject(userGuid, true);
            var availabilityService = new BusinessObjectServices.AvailabilityService(status);
            var availability = 
                availabilityService.GetAvailability
                (
                    userCode, 
                    shipWeek, 
                    0, 
                    0,
                    includeCartData: false, includePrices: true,
                    programTypeCode: programTypeCode,
                    productFormCategoryCode: productFormCategoryCode,
                    supplierCodeList: supplierCode == null ? null : new string[] {supplierCode}
                );

            int productsSkipped = 0;
            foreach (var product in availability.ProductList)
            {
                if (product.HasAvailableQtyOfAtLeast(quantityRequired))
                {
                    if (productsSkipped < productsToSkip)
                    {
                        productsSkipped++;
                    }
                    else
                    {
                        availableProductList.Add(product);
                        if (maxProductsToReturn != -1 && availableProductList.Count >= maxProductsToReturn)
                            break;
                    }
                }
            }

            if (maxProductsToReturn != -1)
            {
                availableProductList.Count.Should().BeLessOrEqualTo(maxProductsToReturn);
            }

            return availableProductList;
        }

        public ShipWeek GetShipWeek(DateTime dateTime)
        {
            var shipweekService = new ShipWeekService();
            var shipWeek = shipweekService.ReturnShipWeekTypeFromDate(dateTime);
            return shipWeek;
        }

        public void UpdateCartQuantity(Guid userGuid, string userCode, ShipWeek shipWeek, Product product, int quantity, decimal price)
        {
            var growerOrderBusinessService = new BusinessObjectServices.GrowerOrderService();

            growerOrderBusinessService.UpdateCartQuantity(userGuid, userCode, shipWeek.Code, product.Guid, quantity, price);
        }

        public void AddToCart(Guid userGuid, string userCode, ShipWeek shipWeek, string programTypeCode, string productFormCategoryCode)
        {
            var growerOrderBusinessService = new BusinessObjectServices.GrowerOrderService();
            growerOrderBusinessService.AddToCart(userGuid, userCode, shipWeek.Code, programTypeCode,
                                                 productFormCategoryCode);
        }

        public Guid FindCartOrder(Guid userGuid, string userCode, ShipWeek shipWeek, string programTypeCode,
                                     string productFormCategoryCode)
        {
            var growerOrderBusinessService = new BusinessObjectServices.GrowerOrderService();

            var orderSummaryService = new OrderSummaryService();
            var cartOrders = orderSummaryService.GetAllShoppingCartOrders(userGuid);
            OrderSummary cartOrderSummary = null;
            foreach (var orderSummary in cartOrders)
            {
                if (orderSummary.ProgramTypeCode == programTypeCode && orderSummary.ProductFormCategoryCode == productFormCategoryCode)
                {
                    cartOrderSummary = orderSummary;
                    break;
                }
            }

            cartOrderSummary.Should().NotBeNull();
            if (cartOrderSummary == null)
                return Guid.Empty;

            return cartOrderSummary.OrderGuid;
        }

        public void PlaceOrder(Guid userGuid, string userCode, Guid growerOrderGuid)
        {
            var growerOrderBusinessService = new BusinessObjectServices.GrowerOrderService();

            growerOrderBusinessService.PlaceOrder(userGuid, userCode, growerOrderGuid, updatePricesOnNewThread: false);
        }

    


        public GrowerOrder GetGrowerOrderDetail(Guid userGuid, string userCode, Guid growerOrderGuid, bool includePriceData)
        {
            var growerOrderBusinessService = new BusinessObjectServices.GrowerOrderService();

            var growerOrder = growerOrderBusinessService.GetGrowerOrderDetail(growerOrderGuid, includePriceData);

            return growerOrder;
        }

        public void DeleteGrowerOrder(Guid userGuid, string userCode, GrowerOrder growerOrder)
        {
            var growerOrderBusinessService = new BusinessObjectServices.GrowerOrderService();
            growerOrderBusinessService.DeletePlacedOrderFORTESTINGPURPOSESONLY(userGuid, userCode, growerOrder);
        }

        public bool GrowerOrderExists(Guid growerOrderGuid)
        {
            var growerOrderTableService = new GrowerOrderTableService();
            return growerOrderTableService.Exists(growerOrderGuid);
        }

        public SupplierOrder GetTheOnlySupplierOrder(GrowerOrder growerOrder)
        {
            SupplierOrder onlySupplierOrder = null;

            growerOrder.SupplierOrders.Count.Should().Be(1);
            foreach (var supplierOrder in growerOrder.SupplierOrders)
            {
                onlySupplierOrder = supplierOrder;
                break;
            }

            return onlySupplierOrder;
        }

        public SupplierOrder GetSupplierOrder(GrowerOrder growerOrder, int index)
        {
            SupplierOrder supplierOrderFound = null;
            OrderLine orderLine = null;

            int i = 0;
            foreach (var supplierOrder in growerOrder.SupplierOrders)
            {
                if (i == index)
                {
                    supplierOrderFound = supplierOrder;
                }

                i++;
            }

            supplierOrderFound.Should().NotBeNull();

            return supplierOrderFound;
        }

        public OrderLine GetTheOnlyOrderOrderLine(GrowerOrder growerOrder)
        {
            return GetOrderLine(growerOrder, 0);
        }

        public OrderLine GetOrderLine(GrowerOrder growerOrder, int index)
        {
            OrderLine orderLine = null;

            int i = 0;
            foreach (var supplierOrder in growerOrder.SupplierOrders)
            {
                foreach (var line in supplierOrder.OrderLines)
                {
                    if (i == index)
                    {
                        orderLine = line;
                    }

                    i++;
                }
            }

            orderLine.Should().NotBeNull();

            return orderLine;
        }

        public void NotifySupplierOfOrderLine(Guid userGuid, string userCode, OrderLine orderLine)
        {
            var growerOrderBusinessService = new BusinessObjectServices.GrowerOrderService();

            growerOrderBusinessService.SupplierNotified(UserGuidScott, Guid.Empty, orderLine.Guid);
        }

        public void NotifySupplierOfOrder(Guid userGuid, string userCode, SupplierOrder supplierOrder)
        {
            var growerOrderBusinessService = new BusinessObjectServices.GrowerOrderService();

            var results = growerOrderBusinessService.SupplierNotified(UserGuidScott, supplierOrder.SupplierOrderGuid, Guid.Empty);

            results.OrderLinesChanged.Should().BeGreaterOrEqualTo(1);
            results.OrderLinesDeleted.Should().Be(0);
        }

        public void NotifySupplierOfOrderLine(Guid userGuid, string userCode, Guid orderLineGuid)
        {
            var growerOrderBusinessService = new BusinessObjectServices.GrowerOrderService();

            var results = growerOrderBusinessService.SupplierNotified(UserGuidScott, Guid.Empty, orderLineGuid);

            results.OrderLinesChanged.Should().Be(1);
            results.OrderLinesDeleted.Should().Be(0);
        }

        public void ConfirmSupplierOrder(Guid userGuid, string userCode, SupplierOrder supplierOrder)
        {
            var growerOrderBusinessService = new BusinessObjectServices.GrowerOrderService();

            var results = growerOrderBusinessService.SupplierConfirmed(UserGuidScott, supplierOrder.SupplierOrderGuid, Guid.Empty);

            results.OrderLinesChanged.Should().BeGreaterOrEqualTo(1);
            results.OrderLinesDeleted.Should().Be(0);
        }

        public void ConfirmSupplierOrderLine(Guid userGuid, string userCode, OrderLine orderLine)
        {
            var growerOrderBusinessService = new BusinessObjectServices.GrowerOrderService();

            var results = growerOrderBusinessService.SupplierConfirmed(UserGuidScott, Guid.Empty, orderLine.Guid);
            results.OrderLinesChanged.Should().Be(1);
            results.OrderLinesDeleted.Should().Be(0);
        }

        public int DisplayAllEventLogs(GrowerOrder growerOrder)
        {
            int eventLogCount = 0;

            System.Console.WriteLine(string.Format("GrowerOrder {0} EventLogs:", growerOrder.OrderGuid));
            foreach (var orderEventLog in growerOrder.GrowerOrderHistoryEvents)
            {
                System.Console.WriteLine(orderEventLog.ToFormattedString(1));
                eventLogCount++;
            }

            foreach (var supplierOrder in growerOrder.SupplierOrders)
            {
                System.Console.WriteLine(string.Format("\tSupplierOrder {0} EventLogs:", supplierOrder.SupplierOrderGuid));
                foreach (var supplierOrderEventLog in supplierOrder.GrowerOrderHistoryEvents)
                {
                    System.Console.WriteLine(supplierOrderEventLog.ToFormattedString(2));
                    eventLogCount++;
                }

                foreach (var orderLine in supplierOrder.OrderLines)
                {
                    System.Console.WriteLine(string.Format("\t\tOrderLine {0} EventLogs:", orderLine.Guid));
                    foreach (var orderLineEventLog in orderLine.GrowerOrderHistoryEvents)
                    {
                        System.Console.WriteLine(orderLineEventLog.ToFormattedString(3));
                        eventLogCount++;
                    }
                }
            }

            return eventLogCount;
        }

        public void DeleteAnyOrder(GrowerOrder growerOrder)
        {
            var growerOrderTableService = new GrowerOrderTableService();
            var supplierOrderTableService = new SupplierOrderTableService();
            var orderLineTableService = new OrderLineTableService();

            var growerOrderTableObject = growerOrderTableService.GetByGuid(growerOrder.OrderGuid);

            foreach (var growerFee in growerOrderTableObject.GrowerOrderFeeList)
            {
                var growerFeeService = new GrowerOrderFeeTableService();
                DataObjectLibrary.GrowerOrderFee fee = growerFeeService.GetByGuid(growerFee.Guid);
                growerFeeService.Delete(fee);
            }

            foreach (var supplierOrder in growerOrderTableObject.SupplierOrderList)
            {
                foreach (var orderLine in supplierOrder.OrderLineList)
                {
                    orderLineTableService.Delete(orderLine);
                }
                foreach (var supplierFee in supplierOrder.SupplierOrderFeeList)
                {
                    SupplierOrderFeeTableService feeService = new SupplierOrderFeeTableService();
                    DataObjectLibrary.SupplierOrderFee fee = feeService.GetByGuid(supplierFee.Guid);
                    feeService.Delete(fee);
                }

                supplierOrderTableService.Delete(supplierOrder);
            }
            growerOrderTableService.Delete(growerOrderTableObject);

            growerOrderTableService.Exists(growerOrder.OrderGuid).Should().BeFalse();
        }

        public void CountSupplierOrdersAndOrderLines(GrowerOrder growerOrder, out int supplierOrders, out int orderLines)
        {
            supplierOrders = 0;
            orderLines = 0;

            foreach (var supplierOrder in growerOrder.SupplierOrders)
            {
                supplierOrders++;
                foreach (var orderLine in supplierOrder.OrderLines)
                {
                    orderLines++;
                }
            }

            System.Console.WriteLine( string.Format( "SupplierOrders: {0}, OrderLines: {1}", supplierOrders, orderLines));
        }
    }
}

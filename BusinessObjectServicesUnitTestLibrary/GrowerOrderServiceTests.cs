﻿using System;
using System.Globalization;
using DataServiceLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessObjectsLibrary;
using BusinessObjectServices;
using FluentAssertions;
using System.Collections.Generic;

namespace BusinessObjectServicesUnitTestLibrary
{
    [TestClass]
    public class GrowerOrderServiceTests : TestBase
    {
        //[TestMethod]
        //public void BusinessObjectServicesGrowerOrderChangeOrderLineQuantityTest()
        //{
        //    GrowerOrder placedOrder = null;
        //    try
        //    {
        //        int originalQuantity = 100;

        //        placedOrder = CreatePlacedGrowerOrder(UserGuidScott, UserCodeScott, originalQuantity);
        //        var newProduct =
        //            GetAvailableProduct
        //                (
        //                    UserGuidScott,
        //                    UserCodeScott,
        //                    placedOrder.ShipWeek,
        //                    placedOrder.ProgramTypeCode,
        //                    placedOrder.ProductFormCategoryCode,
        //                    originalQuantity,
        //                    productsToSkip: 1
        //                );

        //        var orderLine = GetTheOnlyOrderOrderLine(placedOrder);
        //       // orderLine.PriceUsedGuid.Should().HaveValue();
        //       // orderLine.PriceUsedGuid.Should().NotBeEmpty();
        //       // if (orderLine.PriceUsedGuid == null)
        //       //     throw new ApplicationException("This should have been caught by the previous two lines.");

        //      //  Guid priceUsedGuid = (Guid) orderLine.PriceUsedGuid;
        //      //  priceUsedGuid.Should().NotBeEmpty();


        //        var growerOrderService = new GrowerOrderService();
        //        originalQuantity = orderLine.QuantityOrdered;

        //        int newQuantity = 1000;
        //        GrowerOrderService.RoundTripToDatabaseHasOccurredFORDEBUGGINGPURPOSESONLY = false;
        //        growerOrderService.UpdatePlacedOrderLineQuantity(UserGuidScott,UserCodeScott, orderLine.Guid, newQuantity);
        //        GrowerOrderService.RoundTripToDatabaseHasOccurredFORDEBUGGINGPURPOSESONLY.Should().BeTrue();

        //        growerOrderService = new GrowerOrderService();
        //        placedOrder = growerOrderService.GetGrowerOrderDetail(placedOrder.OrderGuid, includePriceData: false);
        //        orderLine = GetTheOnlyOrderOrderLine(placedOrder);
        //        orderLine.QuantityOrdered.Should().BeGreaterThan(originalQuantity);
        //        newQuantity = orderLine.QuantityOrdered;

        //        System.Console.WriteLine(string.Format("GrowerOrder Logs for {0}:", placedOrder.OrderGuid));
        //        foreach (var eventLog in placedOrder.GrowerOrderHistoryEvents)
        //        {
        //            System.Console.WriteLine(eventLog.ToFormattedString(1));
        //        }

        //        var supplierOrder = GetTheOnlySupplierOrder(placedOrder);
        //        System.Console.WriteLine(string.Format("SupplierOrder Logs for {0}:", placedOrder.OrderGuid));
        //        foreach (var eventLog in supplierOrder.GrowerOrderHistoryEvents)
        //        {
        //            System.Console.WriteLine(eventLog.ToFormattedString(1));
        //        }

                
        //    }
        //    finally
        //    {
        //        if (placedOrder != null)
        //            DeleteAnyOrder(placedOrder);
        //    }
        //}

        //[TestMethod]
        //public void BusinessObjectServicesGrowerOrderChangeOrderLineQuantityWithPerformanceEnhancementTest()
        //{
        //    GrowerOrder placedOrder = null;
        //    try
        //    {
        //        int originalQuantity = 100;

        //        placedOrder = CreatePlacedGrowerOrder(UserGuidScott, UserCodeScott, originalQuantity);
        //        var newProduct =
        //            GetAvailableProduct
        //                (
        //                    UserGuidScott,
        //                    UserCodeScott,
        //                    placedOrder.ShipWeek,
        //                    placedOrder.ProgramTypeCode,
        //                    placedOrder.ProductFormCategoryCode,
        //                    originalQuantity,
        //                    productsToSkip: 1
        //                );

        //        var orderLine = GetTheOnlyOrderOrderLine(placedOrder);
        //       // orderLine.PriceUsedGuid.Should().HaveValue();
        //       // orderLine.PriceUsedGuid.Should().NotBeEmpty();
        //       // if (orderLine.PriceUsedGuid == null)
        //       //     throw new ApplicationException("This should have been caught by the previous two lines.");

        //      //  Guid priceUsedGuid = (Guid)orderLine.PriceUsedGuid;
        //       // priceUsedGuid.Should().NotBeEmpty();

        //        var growerOrderService = new GrowerOrderService();
        //        originalQuantity = orderLine.QuantityOrdered;

        //        int newQuantity = 1000;
        //        GrowerOrderService.RoundTripToDatabaseHasOccurredFORDEBUGGINGPURPOSESONLY = false;

        //        const int quantityPassedIn = 234234;
        //        const string productDescriptionPassedIn = "Product Desc.";
        //        growerOrderService.UpdatePlacedOrderLineQuantity(UserGuidScott, UserCodeScott, orderLine.Guid, newQuantity, growerOrderGuid: placedOrder.OrderGuid, originalQuantity: quantityPassedIn, productDescription: productDescriptionPassedIn);
        //        GrowerOrderService.RoundTripToDatabaseHasOccurredFORDEBUGGINGPURPOSESONLY.Should().BeFalse();

        //        placedOrder = growerOrderService.GetGrowerOrderDetail(placedOrder.OrderGuid, includePriceData: false);
        //        orderLine = GetTheOnlyOrderOrderLine(placedOrder);
        //        orderLine.QuantityOrdered.Should().BeGreaterThan(originalQuantity);
        //        newQuantity = orderLine.QuantityOrdered;

        //        System.Console.WriteLine(string.Format("GrowerOrder Logs for {0}:", placedOrder.OrderGuid));
        //        foreach (var eventLog in placedOrder.GrowerOrderHistoryEvents)
        //        {
        //            System.Console.WriteLine(eventLog.ToFormattedString(1));
        //        }

        //        var supplierOrder = GetTheOnlySupplierOrder(placedOrder);
        //        System.Console.WriteLine(string.Format("SupplierOrder Logs for {0}:", placedOrder.OrderGuid));
        //        foreach (var eventLog in supplierOrder.GrowerOrderHistoryEvents)
        //        {
        //            System.Console.WriteLine(eventLog.ToFormattedString(1));
        //        }

               
        //    }
        //    finally
        //    {
        //        if (placedOrder != null)
        //            DeleteAnyOrder(placedOrder);
        //    }
        //}

             
            
        [TestMethod]
        public void BusinessObjectServicesRepriceOneLineTest()
        {
           

            try
            {
               int icount  = 0;
               var userGuid = new Guid();
               Guid orderGuid = new Guid("9A9EC7FF-34C7-4FD6-9D7C-C8B13D97EA7B");
               Guid orderLinGuid = new Guid("069CE91D-0170-49D2-B1D3-647D5175BCE9");


               var growerOrderService = new GrowerOrderService();



                icount = growerOrderService.UpdatePrice(userGuid,orderGuid, orderLinGuid);

               

            }
            finally
            {
                             
            }
        }

        [TestMethod]
        public void GetGrowerOrderPublicHistoryTest()
        {
           

            try
            {


                Guid orderGuid = new Guid("93ED4CA4-DC5A-4012-B0B4-1ABD11FC9213");



                var growerOrderService = new GrowerOrderService();



                List<GrowerOrderHistoryEvent> theEventList = growerOrderService.GetGrowerOrderHistory(orderGuid, true);

              theEventList.Count.Should().BeGreaterThan(2);

            }
            finally
            {

            }
        }
        [TestMethod]
        public void BusinessObjectServicesGrowerOrderSubstituteTest()
        {
            GrowerOrder placedOrder = null;

            try
            {
                int quantity = 123;
               
                placedOrder = CreatePlacedGrowerOrder(UserGuidScott, UserCodeScott, quantity);
                var newProduct =
                    GetAvailableProduct
                        (
                            UserGuidScott,
                            UserCodeScott,
                            placedOrder.ShipWeek,
                            placedOrder.ProgramTypeCode,
                            placedOrder.ProductFormCategoryCode,
                            quantity,
                            productsToSkip: 37
                        );

                var orderLine = GetTheOnlyOrderOrderLine(placedOrder);
            //    orderLine.PriceUsedGuid.Should().HaveValue();
            //    orderLine.PriceUsedGuid.Should().NotBeEmpty();
            //    if (orderLine.PriceUsedGuid == null)
            //        throw new ApplicationException("This should have been caught by the previous two lines.");

            //    Guid priceUsedGuid = (Guid)orderLine.PriceUsedGuid;
            //    priceUsedGuid.Should().NotBeEmpty();

                bool substituteLogFound = false;
                var growerOrderService = new GrowerOrderService();
                quantity = orderLine.QuantityOrdered;
                growerOrderService.SubstituteProductOnOrderLine(UserGuidScott, UserCodeScott, orderLine.Guid,
                                                                newProduct.Guid);

                placedOrder = growerOrderService.GetGrowerOrderDetail(placedOrder.OrderGuid, includePriceData: false);
                var newOrderLine = GetTheOnlyOrderOrderLine(placedOrder);
                newOrderLine = GetTheOnlyOrderOrderLine(placedOrder);
                newOrderLine.QuantityOrdered.Should().Be(quantity);
                newOrderLine.Product.Guid.Should().Be(newProduct.Guid);
              //  newOrderLine.PriceUsedGuid.Should().HaveValue();
               // newOrderLine.PriceUsedGuid.Should().NotBe(priceUsedGuid);

                System.Console.WriteLine(string.Format("GrowerOrder Logs for {0}:", placedOrder.OrderGuid));
                foreach (var eventLog in placedOrder.GrowerOrderHistoryEvents)
                {
                    System.Console.WriteLine(eventLog.ToFormattedString(1));
                }

                var supplierOrder = GetTheOnlySupplierOrder(placedOrder);
                System.Console.WriteLine(string.Format("SupplierOrder Logs for {0}:", placedOrder.OrderGuid));
                foreach (var eventLog in supplierOrder.GrowerOrderHistoryEvents)
                {
                    System.Console.WriteLine(eventLog.ToFormattedString(1));
                }

                System.Console.WriteLine(string.Format("OrderLine Logs for {0}:", orderLine.Guid));
                foreach (var eventLog in orderLine.GrowerOrderHistoryEvents)
                {
                    if (eventLog.Comment.Contains("substitute"))
                        substituteLogFound = true;

                    System.Console.WriteLine(eventLog.ToFormattedString(1));
                }

            }
            finally
            {
                if (placedOrder != null)
                    DeleteAnyOrder(placedOrder);                
            }
        }

        //[TestMethod]
        //public void BusinessObjectServicesGrowerOrderCancelTest()
        //{
        //    GrowerOrder placedOrder = null;

        //    try
        //    {
        //        const int originalQuantity = 100;
        //        int quantity = originalQuantity;

        //        placedOrder = CreatePlacedGrowerOrder(UserGuidScott, UserCodeScott, quantity);
        //        var newProduct =
        //            GetAvailableProduct
        //                (
        //                    UserGuidScott,
        //                    UserCodeScott,
        //                    placedOrder.ShipWeek,
        //                    placedOrder.ProgramTypeCode,
        //                    placedOrder.ProductFormCategoryCode,
        //                    quantity,
        //                    productsToSkip: 1
        //                );

        //        var orderLine = GetTheOnlyOrderOrderLine(placedOrder);

        //        bool cancelLogFound = false;
        //        var growerOrderService = new GrowerOrderService();
        //        quantity = orderLine.QuantityOrdered;

        //        GrowerOrderService.RoundTripToDatabaseHasOccurredFORDEBUGGINGPURPOSESONLY = false;
        //        growerOrderService.UpdatePlacedOrderLineQuantity(UserGuidScott, UserCodeScott, orderLine.Guid, 0);
        //        GrowerOrderService.RoundTripToDatabaseHasOccurredFORDEBUGGINGPURPOSESONLY.Should().BeTrue();

        //        placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
        //        orderLine = GetTheOnlyOrderOrderLine(placedOrder);
        //        orderLine.QuantityOrdered.Should().Be(0);

        //        System.Console.WriteLine(string.Format("GrowerOrder Logs for {0}:", placedOrder.OrderGuid));
        //        foreach (var eventLog in placedOrder.GrowerOrderHistoryEvents)
        //        {
        //            if
        //            (
        //                eventLog.Comment.Contains("cancelled") &&
        //                eventLog.Comment.Contains(originalQuantity.ToString(CultureInfo.InvariantCulture)) &&
        //                eventLog.Comment.Contains(orderLine.Product.ProductDescription)
        //            )
        //            {
        //                cancelLogFound = true;
        //            }

        //            System.Console.WriteLine(eventLog.ToFormattedString(1));
        //        }

        //        var supplierOrder = GetTheOnlySupplierOrder(placedOrder);
        //        System.Console.WriteLine(string.Format("SupplierOrder Logs for {0}:", placedOrder.OrderGuid));
        //        foreach (var eventLog in supplierOrder.GrowerOrderHistoryEvents)
        //        {
        //            System.Console.WriteLine(eventLog.ToFormattedString(1));
        //        }

        //        System.Console.WriteLine(string.Format("OrderLine Logs for {0}:", orderLine.Guid));
        //        foreach (var eventLog in orderLine.GrowerOrderHistoryEvents)
        //        {
        //            System.Console.WriteLine(eventLog.ToFormattedString(1));
        //        }

        //        cancelLogFound.Should().BeTrue();
        //    }
        //    finally
        //    {
        //        if (placedOrder != null)
        //        {
        //            DeleteAnyOrder(placedOrder);                                    
        //        }
        //    }

        //}

        //[TestMethod]
        //public void BusinessObjectServicesGrowerOrderCancelWithPerformanceEnhancementTest()
        //{
        //    GrowerOrder placedOrder = null;

        //    try
        //    {
        //        const int originalQuantity = 100;
        //        int quantity = originalQuantity;

        //        placedOrder = CreatePlacedGrowerOrder(UserGuidScott, UserCodeScott, quantity);
        //        var newProduct =
        //            GetAvailableProduct
        //                (
        //                    UserGuidScott,
        //                    UserCodeScott,
        //                    placedOrder.ShipWeek,
        //                    placedOrder.ProgramTypeCode,
        //                    placedOrder.ProductFormCategoryCode,
        //                    quantity,
        //                    productsToSkip: 1
        //                );

        //        var orderLine = GetTheOnlyOrderOrderLine(placedOrder);

        //        bool cancelLogFound = false;
        //        var growerOrderService = new GrowerOrderService();
        //        quantity = orderLine.QuantityOrdered;

        //        GrowerOrderService.RoundTripToDatabaseHasOccurredFORDEBUGGINGPURPOSESONLY = false;

        //        // Values for growerOrderGuid, originalQuantity and productDescription are included to prevent a round trip to look them up.
        //        const int quantityPassedInAsOriginalQuantity = 234;
        //        const string productDescriptionPassedIn = "This would be the product description.";
        //        growerOrderService.UpdatePlacedOrderLineQuantity(UserGuidScott, UserCodeScott, orderLine.Guid, 0,
        //                                                         placedOrder.OrderGuid,
        //                                                         originalQuantity: quantityPassedInAsOriginalQuantity,
        //                                                         productDescription: productDescriptionPassedIn);

        //        GrowerOrderService.RoundTripToDatabaseHasOccurredFORDEBUGGINGPURPOSESONLY.Should().BeFalse();

        //       placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
        //        orderLine = GetTheOnlyOrderOrderLine(placedOrder);
        //        orderLine.QuantityOrdered.Should().Be(0);

        //        System.Console.WriteLine(string.Format("GrowerOrder Logs for {0}:", placedOrder.OrderGuid));
        //        foreach (var eventLog in placedOrder.GrowerOrderHistoryEvents)
        //        {
        //            if
        //            (
        //                eventLog.Comment.Contains("cancelled") &&
        //                eventLog.Comment.Contains(
        //                    quantityPassedInAsOriginalQuantity.ToString(CultureInfo.InvariantCulture)) &&
        //                eventLog.Comment.Contains(productDescriptionPassedIn) &&
        //                !eventLog.Comment.Contains(orderLine.Product.ProductDescription)
        //            )
        //            {
        //                cancelLogFound = true;
        //            }

        //            System.Console.WriteLine(eventLog.ToFormattedString(1));
        //        }

        //        var supplierOrder = GetTheOnlySupplierOrder(placedOrder);
        //        System.Console.WriteLine(string.Format("SupplierOrder Logs for {0}:", placedOrder.OrderGuid));
        //        foreach (var eventLog in supplierOrder.GrowerOrderHistoryEvents)
        //        {
        //            System.Console.WriteLine(eventLog.ToFormattedString(1));
        //        }

        //        System.Console.WriteLine(string.Format("OrderLine Logs for {0}:", orderLine.Guid));
        //        foreach (var eventLog in orderLine.GrowerOrderHistoryEvents)
        //        {
        //            System.Console.WriteLine(eventLog.ToFormattedString(1));
        //        }

        //        cancelLogFound.Should().BeTrue();
        //    }
        //    finally
        //    {
        //        if (placedOrder != null)
        //        {
        //            DeleteAnyOrder(placedOrder);
        //        }
        //    }
        //}

        //[TestMethod]
        //public void BusinessObjectServicesGrowerOrderLifeCycleTest()
        //{
        //    GrowerOrder cartOrder = null;

        //    try
        //    {
        //        int quantity = 100;

        //        cartOrder = CreateCartOrder(UserGuidScott, UserCodeScott, quantity, 4);

        //        var cartLine = GetOrderLine(cartOrder, 0);
        //        Guid? priceUsedGuid = cartLine.PriceUsedGuid;
        //        priceUsedGuid.Should().NotHaveValue();
        //        cartLine.Price.Should().NotBe(0);
        //        UpdateCartQuantity(UserGuidScott, UserCodeScott, cartOrder.ShipWeek, cartLine.Product, quantity * 4, cartLine.Product.Price);
        //        cartOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, cartOrder.OrderGuid, includePriceData: false);
        //        var orderLine = GetOrderLine(cartOrder, 0);
        //        orderLine.QuantityOrdered.Should().BeGreaterThan(quantity);
        //        quantity = orderLine.QuantityOrdered;

        //        var orderLineTableService = new OrderLineTableService();
        //        var orderLineTableRow = orderLineTableService.GetByGuid(orderLine.Guid);
        //        orderLineTableRow.QtyOrdered.Should().Be(quantity);

        //        orderLine.PriceHasBeenSet.Should().BeFalse();

        //        PlaceOrder(UserGuidScott, UserCodeScott, cartOrder.OrderGuid);
        //        var placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, cartOrder.OrderGuid, includePriceData: false);
        //        orderLine = GetOrderLine(placedOrder, 0);
        //        orderLine.OrderLineStatus.Code.Should().Be(LookupTableValues.Code.OrderLineStatus.GrowerNotified.Code);
        //        orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.GrowerNotified.Guid);
        //        orderLine = GetOrderLine(placedOrder, 1);
        //        orderLine.OrderLineStatus.Code.Should().Be(LookupTableValues.Code.OrderLineStatus.GrowerNotified.Code);
        //        orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.GrowerNotified.Guid);
        //        orderLine = GetOrderLine(placedOrder, 2);
        //        orderLine.OrderLineStatus.Code.Should().Be(LookupTableValues.Code.OrderLineStatus.GrowerNotified.Code);
        //        orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.GrowerNotified.Guid);
        //        orderLine = GetOrderLine(placedOrder, 3);
        //        orderLine.OrderLineStatus.Code.Should().Be(LookupTableValues.Code.OrderLineStatus.GrowerNotified.Code);
        //        orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.GrowerNotified.Guid);

        //        orderLine = GetOrderLine(placedOrder, 0);

        //      //  orderLine.PriceHasBeenSet.Should().BeTrue();
        //        orderLine.Price.Should().NotBe(0);
        //      //  orderLine.PriceUsedGuid.Should().NotBeEmpty();

        //        placedOrder.OrderGuid.Should().Be(cartOrder.OrderGuid);
        //        placedOrder.SupplierOrders.Count.Should().Be(1);

        //        orderLine.QuantityOrdered.Should().Be(quantity);
        //       // orderLine.PriceUsedGuid.Should().HaveValue();
        //       // orderLine.PriceUsedGuid.Should().NotBeEmpty();
        //       // if (orderLine.PriceUsedGuid == null)
        //       //     throw new ApplicationException("The previous two lines should have caught this.");
        //        orderLine.OrderLineStatus.Code.Should().Be(LookupTableValues.Code.OrderLineStatus.GrowerNotified.Code);
        //        orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.GrowerNotified.Guid);

        //      //  Guid priceUsed = (Guid) orderLine.PriceUsedGuid;

        //        // By passing in a null userCode, we are emulating an update by the customer, which should result in a status of GrowerEdit
        //        UpdatePlacedOrderLineQuantity(UserGuidScott, null, Guid.Empty, orderLine.Guid, quantity * 4);
        //        placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
        //        orderLine = GetOrderLine(placedOrder, 0);
        //        orderLine.QuantityOrdered.Should().BeGreaterThan(quantity);
        //      //  orderLine.PriceUsedGuid.Should().Be(priceUsed);
        //        orderLine.OrderLineStatus.Code.Should().Be(LookupTableValues.Code.OrderLineStatus.GrowerEdit.Code);
        //        orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.GrowerEdit.Guid);

        //        // By passing in a non-null userCode, we are emulating an update from the admin app, which should result in a status of SupplierEdit
        //        UpdatePlacedOrderLineQuantity(UserGuidScott, UserCodeScott, Guid.Empty, orderLine.Guid, quantity * 2);
        //        placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
        //        orderLine = GetOrderLine(placedOrder, 0);
        //        orderLine.QuantityOrdered.Should().BeGreaterThan(quantity);
        //       // orderLine.PriceUsedGuid.Should().Be(priceUsed);
        //        orderLine.OrderLineStatus.Code.Should().Be(LookupTableValues.Code.OrderLineStatus.SupplierEdit.Code);
        //        orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.SupplierEdit.Guid);

        //        // Cancel the next order line.
        //        orderLine = GetOrderLine(placedOrder, 1);
        //        // By passing in a null UserCode, we are emulating an update by the customer.
        //        UpdatePlacedOrderLineQuantity(UserGuidScott, null, Guid.Empty, orderLine.Guid, 0);
        //        placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
        //        orderLine = GetOrderLine(placedOrder, 1);
        //       // orderLine.OrderLineStatus.Code.Should().Be(LookupTableValues.Code.OrderLineStatus.GrowerCancelled.Code);
        //       // orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.GrowerCancelled.Guid);

        //        // Cancel the next order line.
        //        orderLine = GetOrderLine(placedOrder, 2);
        //        // By passing in a non-null UserCode, we are emulating an update by the supplier.
        //        UpdatePlacedOrderLineQuantity(UserGuidScott, UserCodeScott, Guid.Empty, orderLine.Guid, 0);
        //        placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
        //        orderLine = GetOrderLine(placedOrder, 2);
        //      //  orderLine.OrderLineStatus.Code.Should().Be(LookupTableValues.Code.OrderLineStatus.SupplierCancelled.Code);
        //     //   orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.SupplierCancelled.Guid);

        //        // Sub the next order line.
        //        var newProduct =
        //            GetAvailableProduct
        //                (
        //                    UserGuidScott,
        //                    UserCodeScott,
        //                    placedOrder.ShipWeek,
        //                    placedOrder.ProgramTypeCode,
        //                    placedOrder.ProductFormCategoryCode,
        //                    quantity,
        //                    productsToSkip: 5,
        //                    supplierCode: orderLine.Product.SupplierCode
        //                );

        //        var growerOrderService = new GrowerOrderService();
        //        orderLine = GetOrderLine(placedOrder, 3);
        //        newProduct.Guid.Should().NotBe(orderLine.Product.Guid);
        //        growerOrderService.SubstituteProductOnOrderLine(UserGuidScott, UserCodeScott, orderLine.Guid,
        //                                                        newProduct.Guid);

        //        var supplierOrder = GetSupplierOrder(placedOrder, 0);

        //        placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);

        //        //System.Console.WriteLine("Before Notifying Supplier:");
        //        //int eventLogCountBefore = DisplayAllEventLogs(placedOrder);

        //        //NotifySupplierOfOrder(UserGuidScott, UserCodeScott, supplierOrder);

        //        //placedOrder = GetGrowerOrderDetail(UserGuidScott, UserCodeScott, placedOrder.OrderGuid, includePriceData: false);
        //        //orderLine = GetOrderLine(placedOrder, 0);
        //        //orderLine.OrderLineStatus.Code.Should().Be(LookupTableValues.Code.OrderLineStatus.SupplierNotified.Code);
        //        //orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.SupplierNotified.Guid);
        //        //orderLine = GetOrderLine(placedOrder, 1);
        //        //orderLine.OrderLineStatus.Code.Should().Be(LookupTableValues.Code.OrderLineStatus.Cancelled.Code);
        //        //orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.Cancelled.Guid);
        //        //orderLine = GetOrderLine(placedOrder, 2);
        //        //orderLine.OrderLineStatus.Code.Should().Be(LookupTableValues.Code.OrderLineStatus.Cancelled.Code);
        //        //orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.Cancelled.Guid);
        //        //orderLine = GetOrderLine(placedOrder, 3);
        //        //orderLine.OrderLineStatus.Code.Should().Be(LookupTableValues.Code.OrderLineStatus.SupplierNotified.Code);
        //        //orderLine.OrderLineStatus.Guid.Should().Be(LookupTableValues.Code.OrderLineStatus.SupplierNotified.Guid);

        //        //System.Console.WriteLine("After Notifying Supplier:");
        //        //int eventLogCountAfter = DisplayAllEventLogs(placedOrder);

        //        //eventLogCountAfter.Should().Be(eventLogCountBefore);
        //    }
        //    finally
        //    {
        //        if (cartOrder != null)
        //        {
        //            DeleteAnyOrder(cartOrder);
        //        }
        //    }
        //}

        [TestMethod]
        public void BusinessObjectServicesGrowerOrderGetOrderTotalsTest()
        {
            GrowerOrder cartOrder = null;
            try
            {
                int originalQuantity = 500;

                cartOrder = CreateCartOrder(UserGuidScott, UserCodeScott, originalQuantity);
                

                var service = new BusinessObjectServices.GrowerOrderService();

              //  int iCart = service.GetOrderTotals(cartOrder, true);
             //   int iUnCart = service.GetOrderTotals(cartOrder, false);

          //    iCart.Should().Be(500);
           //   iUnCart.Should().Be(0);

            }
            finally
            {
                if (cartOrder != null)
                    DeleteAnyOrder(cartOrder);
            }
        }


    }
}

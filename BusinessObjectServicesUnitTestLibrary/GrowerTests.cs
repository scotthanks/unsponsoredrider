﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;




namespace BusinessObjectServicesUnitTestLibrary
{
    [TestClass]
    public class GrowerTests
    {
    //    [TestMethod]
    //    public void GetGrowerOrderSummariesTest()
    //    {
    //        Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));
    //        string shipWeekCode = "201512";
    //        var service = new GrowerService();
    //        var theresult = service.GetGrowerOrderSummaries(userGuid,shipWeekCode);
    //        theresult.Count().Should().Be(1);
    //    }
        [TestMethod]
        public void RequestLineOfCreditTest()
        {
            var service = new GrowerService();
            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));
           
            
            service.RequestLineOfCredit(userGuid, 7000);
            var oGrower = new Grower();
            oGrower = service.TryGetGrowerForUser(userGuid);
            oGrower.RequestedLineOfCredit.Should().Be(7000);
            oGrower.CreditAppStatus.Code.Should().Be("Pending");
        }

        [TestMethod]
        public void AllowSubstitutionsTest()
        {
            var service = new GrowerService();
            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));


            bool bReturn = service.UpdateGrowerField(userGuid,"AllowSubstitutions" ,"false");
            var oGrower = new Grower();
            oGrower = service.TryGetGrowerForUser(userGuid);
            oGrower.AllowSubstitutions.Should().Be(false);

            bReturn = service.UpdateGrowerField(userGuid, "AllowSubstitutions", "true");
            var oGrower2 = new Grower();
            oGrower2 = service.TryGetGrowerForUser(userGuid);
            oGrower2.AllowSubstitutions.Should().Be(true);



        }

    }
}

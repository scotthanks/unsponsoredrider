﻿using System;
using System.Globalization;
using BusinessObjectsLibrary;
using DataServiceLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessObjectServices;
using FluentAssertions;
using System.Linq;

namespace BusinessObjectServicesUnitTestLibrary
{
    [TestClass]
    public class CatalogServiceTest : TestBase
    {
        [TestMethod]
        public void SelectionsGetTest_EDB_RC()
        {

            var status = new StatusObject(UserGuidScott, false);
            var service = new CatalogService(status);
            var categoryCode = "EDB";
            var productFormCode = "RC";
            var selections = service.SelectionsGet( categoryCode, productFormCode);
            var supplierList = selections.Suppliers.ToList();
            var speciesList = selections.Species.ToList();
            var varietyList = selections.Varieties.ToList();
            var ssList = selections.SpeciesToSuppliers.ToList();

            Assert.AreEqual(10, supplierList.Count());
            Assert.AreEqual(48, speciesList.Count());
            Assert.AreEqual(272, varietyList.Count());
            Assert.AreEqual(160, ssList.Count());

        }

        [TestMethod]
        public void SelectionsGetTest_VA_LIN()
        {

            var status = new StatusObject(UserGuidScott, true);
            var service = new CatalogService(status);
            var categoryCode = "VA";
            var productFormCode = "LIN";
            var selections = service.SelectionsGet(categoryCode, productFormCode);
            var supplierList = selections.Suppliers.ToList();
            var speciesList = selections.Species.ToList();
            var varietyList = selections.Varieties.ToList();
            var ssList = selections.SpeciesToSuppliers.ToList();



            Assert.AreEqual(10, supplierList.Count());
            Assert.AreEqual(94, speciesList.Count());
            Assert.AreEqual(3195, varietyList.Count());
            Assert.AreEqual(352, ssList.Count());

        }
        [TestMethod]
        public void SelectionsGetTest_VA_RC()
        {

            var status = new StatusObject(UserGuidScott, true);
            var service = new CatalogService(status);
            var categoryCode = "VA";
            var productFormCode = "RC";
            var selections = service.SelectionsGet(categoryCode, productFormCode);
            var supplierList = selections.Suppliers.ToList();
            var speciesList = selections.Species.ToList();
            var varietyList = selections.Varieties.ToList();
            var ssList = selections.SpeciesToSuppliers.ToList();



            Assert.AreEqual(22, supplierList.Count());
            Assert.AreEqual(170, speciesList.Count());
            Assert.AreEqual(4009, varietyList.Count());
            Assert.AreEqual(1008, ssList.Count());

        }
    }
}

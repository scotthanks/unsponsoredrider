﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessObjectsLibrary;
using BusinessObjectServices;

namespace BusinessObjectServicesUnitTestLibrary
{
    [TestClass]
    public class ShipWeekTests : TestBase
    {
        [TestMethod]
        public void BusinessObjectServicesShipWeeksGetTest()
        {
            var shipWeekService = new ShipWeekService();
            
            string shipWeekCode = DateTime.Today.Year.ToString("0000") + "04";
            var shipWeekList = shipWeekService.GetShipWeeks(UserGuidScott, UserCodeScott, shipWeekCode, 52);

            foreach (var shipWeek in shipWeekList)
            {
                System.Console.WriteLine( string.Format( "{0}", shipWeek.Code));
            }

            shipWeekCode = (DateTime.Today.Year + 1).ToString("0000") + "04";
            shipWeekList = shipWeekService.GetShipWeeks(UserGuidScott, UserCodeScott, shipWeekCode, 13);

            foreach (var shipWeek in shipWeekList)
            {
                System.Console.WriteLine(string.Format("{0}", shipWeek.Code));
            }
        }
    }
}

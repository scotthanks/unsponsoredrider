﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;




namespace BusinessObjectServicesUnitTestLibrary
{
    [TestClass]
    public class LookupTests
    {
        [TestMethod]
        public void GetConfigValueTest()
        {
            var service = new LookupService();
            string sValue = service.GetConfigValue("ProdDataServer");



            sValue.Should().NotBe("");
        }
    }
}

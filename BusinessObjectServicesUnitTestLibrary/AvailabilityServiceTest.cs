﻿using System;
using System.Globalization;
using BusinessObjectsLibrary;
using DataServiceLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessObjectServices;
using FluentAssertions;

namespace BusinessObjectServicesUnitTestLibrary
{
    [TestClass]
    public class AvailabilityServiceTest
    {
        [TestMethod]
        public void BusinessObjectServicesGetAvailabilityTest()
        {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var service = new AvailabilityService(status);

            var availability = service.GetAvailability
                (
                    
                    null, 
                    new BusinessObjectsLibrary.ShipWeek(20,2014), 0, 4, includeCartData: true, includePrices: true, programTypeCode:"VA", productFormCategoryCode:"RC", geneticOwnerCodeList: new string[] { "*" }, supplierCodeList: new string[] { "DUW", "LIN", "MLM", "SYN", "WEL" }, speciesCodeList: new string[] { "*" }, varietyCodeList: new string[] { "Z1007" }, varietyMatchOptional: true);

            Console.WriteLine("ShipWeeks:"); 
            foreach (var shipWeek in availability.ShipWeekList)
            {
                Console.WriteLine( string.Format( "\t{0:00} : {1:dd-MMM-yy}", shipWeek.Week, shipWeek.MondayDate));
            }

            System.Console.WriteLine();

            int cartOrdersFound = 0;

            System.Console.WriteLine("Products:");
            foreach (var productData in availability.ProductList)
            {
                System.Console.WriteLine( string.Format( "\t{0}", productData.VarietyName));

                System.Console.WriteLine("\t\tAvailability:");
                foreach (var availabilityData in productData.SelectedAvailability)
                {
                    System.Console.WriteLine( string.Format( "\t\t\t{0:00} : {1:#,###}", availabilityData.ShipWeek.Week, availabilityData.Quantity));
                }

                if (productData.OrderLine != null && productData.OrderLine.QuantityOrdered > 0)
                    cartOrdersFound++;
            }

            Assert.AreEqual(0, cartOrdersFound);
        }

        [TestMethod]
        public void BusinessObjectServicesGetAvailabilityForPricingTest()
        {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var service = new AvailabilityService(status);        

            var availability =
                service.GetAvailability
                    (
                        userCode: null,
                        growerGuid: new Guid(new LookupService().GetConfigValue("SCOTT_GROWER_GUID")),
                        shipWeek: new BusinessObjectsLibrary.ShipWeek(15,2014),
                        productGuid: new Guid("E3451608-2B90-401C-A630-F1FBC84E2D72")
                    );

            availability.ProductList.Count.Should().Be(1);
            availability.ProductList[0].Price.Should().Be((decimal) 0.5568);
        }

         [TestMethod]
        public void BusinessObjectServicesGetAvailabilityValidFormandCategory()
        {
            bool bCheck = false;
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var service = new AvailabilityService(status);
            string sName  = "";
            service.IsValidCategoryCode("ZZZ", out sName).Should().Be(false);
            service.IsValidCategoryCode("VA" , out sName).Should().Be(true);

            service.IsValidFormCode("ZZZ", out sName).Should().Be(false);
            service.IsValidFormCode("RC", out sName).Should().Be(true);

        }
        

        [TestMethod]
        public void BusinessObjectServicesDefaultShipWeekGetOddWeekNumberTest()
        {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var availabilityService = new AvailabilityService(status);

            availabilityService.GetOddWeekNumberBetweenWeekNumbers(5, 7, 11).Should().Be(7);
            availabilityService.GetOddWeekNumberBetweenWeekNumbers(6, 7, 11).Should().Be(7);
            availabilityService.GetOddWeekNumberBetweenWeekNumbers(7, 7, 11).Should().Be(7);
            availabilityService.GetOddWeekNumberBetweenWeekNumbers(8, 7, 11).Should().Be(9);
            availabilityService.GetOddWeekNumberBetweenWeekNumbers(9, 7, 11).Should().Be(9);
            availabilityService.GetOddWeekNumberBetweenWeekNumbers(10, 7, 11).Should().Be(11);
            availabilityService.GetOddWeekNumberBetweenWeekNumbers(11, 7, 11).Should().Be(11);
            availabilityService.GetOddWeekNumberBetweenWeekNumbers(12, 7, 11).Should().Be(7);
            availabilityService.GetOddWeekNumberBetweenWeekNumbers(13, 7, 11).Should().Be(7);
        }

        [TestMethod]
        public void BusinessObjectServicesDefaultShipWeekTest()
        {
            TestYear(DateTime.Today.Year - 1);
            TestYear(DateTime.Today.Year);
            TestYear(DateTime.Today.Year + 1);
            TestYear(DateTime.Today.Year + 2);
            TestYear(DateTime.Today.Year + 3);
        }

        public void TestYear(int year)
        {
            System.Console.WriteLine("Testing year " + year.ToString(CultureInfo.InvariantCulture));

            var shipWeekService = new ShipWeekService();

            var shipWeek = shipWeekService.ShipWeek(1, year);
            shipWeek.MondayDate.DayOfWeek.Should().Be(System.DayOfWeek.Monday);
            System.Console.WriteLine( "Week 1 Monday date is {0:dd-MMM-yy}", shipWeek.MondayDate);

            DateTime monday = shipWeek.MondayDate;
            DateTime tuesday = monday.AddDays(1);
            DateTime wednesday = tuesday.AddDays(1);
            DateTime thursday = wednesday.AddDays(1);
            DateTime friday = thursday.AddDays(1);
            DateTime saturday = friday.AddDays(1);
            DateTime sunday = saturday.AddDays(1);

            int thisYear = sunday.Year;
            int nextYear = sunday.Year + 1;

            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var service = new AvailabilityService(status);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeek.Code).Week.Should().Be(1);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeek.Code).Week.Should().Be(1);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeek.Code).Week.Should().Be(2);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeek.Code).Week.Should().Be(2);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeek.Code).Week.Should().Be(2);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeek.Code).Week.Should().Be(2);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeek.Code).Week.Should().Be(2);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeek.Code).MondayDate.Year.Should().Be(monday.Year);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeek.Code).MondayDate.Year.Should().Be(sunday.Year);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus1.Code).Week.Should().Be(2);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus1.Code).Week.Should().Be(2);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus1.Code).Week.Should().Be(2);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus1.Code).Week.Should().Be(3);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus1.Code).Week.Should().Be(3);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus1.Code).Week.Should().Be(3);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus1.Code).Week.Should().Be(3);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus1.Code).MondayDate.Year.Should().Be(thisYear);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus3.Code).Week.Should().Be(4);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus3.Code).Week.Should().Be(4);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus3.Code).Week.Should().Be(4);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus3.Code).Week.Should().Be(4);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus3.Code).Week.Should().Be(4);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus3.Code).Week.Should().Be(4);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus3.Code).Week.Should().Be(4);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus3.Code).MondayDate.Year.Should().Be(thisYear);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).MondayDate.Year.Should().Be(thisYear);

            shipWeek = shipWeekService.ShipWeek(10, year);
            monday = shipWeek.MondayDate;
            tuesday = monday.AddDays(1);
            wednesday = tuesday.AddDays(1);
            thursday = wednesday.AddDays(1);
            friday = thursday.AddDays(1);
            saturday = friday.AddDays(1);
            sunday = saturday.AddDays(1);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).MondayDate.Year.Should().Be(thisYear);

            shipWeek = shipWeekService.ShipWeek(11, year);
            monday = shipWeek.MondayDate;
            tuesday = monday.AddDays(1);
            wednesday = tuesday.AddDays(1);
            thursday = wednesday.AddDays(1);
            friday = thursday.AddDays(1);
            saturday = friday.AddDays(1);
            sunday = saturday.AddDays(1);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);

            service.GetDefaultShipWeek(monday, "",LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).MondayDate.Year.Should().Be(thisYear);

            shipWeek = shipWeekService.ShipWeek(12, year);
            monday = shipWeek.MondayDate;
            tuesday = monday.AddDays(1);
            wednesday = tuesday.AddDays(1);
            thursday = wednesday.AddDays(1);
            friday = thursday.AddDays(1);
            saturday = friday.AddDays(1);
            sunday = saturday.AddDays(1);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(13);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(13);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(13);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(13);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(13);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(13);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(13);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).MondayDate.Year.Should().Be(thisYear);

            shipWeek = shipWeekService.ShipWeek(13, year);
            monday = shipWeek.MondayDate;
            tuesday = monday.AddDays(1);
            wednesday = tuesday.AddDays(1);
            thursday = wednesday.AddDays(1);
            friday = thursday.AddDays(1);
            saturday = friday.AddDays(1);
            sunday = saturday.AddDays(1);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(13);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(13);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(13);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(13);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(13);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(13);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(13);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).MondayDate.Year.Should().Be(thisYear);

            shipWeek = shipWeekService.ShipWeek(14, year);
            monday = shipWeek.MondayDate;
            tuesday = monday.AddDays(1);
            wednesday = tuesday.AddDays(1);
            thursday = wednesday.AddDays(1);
            friday = thursday.AddDays(1);
            saturday = friday.AddDays(1);
            sunday = saturday.AddDays(1);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(15);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(15);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(15);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(15);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(15);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(15);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(15);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).MondayDate.Year.Should().Be(thisYear);

            shipWeek = shipWeekService.ShipWeek(15, year);
            monday = shipWeek.MondayDate;
            tuesday = monday.AddDays(1);
            wednesday = tuesday.AddDays(1);
            thursday = wednesday.AddDays(1);
            friday = thursday.AddDays(1);
            saturday = friday.AddDays(1);
            sunday = saturday.AddDays(1);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(15);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(15);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(15);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(15);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(15);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(15);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(15);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).MondayDate.Year.Should().Be(thisYear);

            shipWeek = shipWeekService.ShipWeek(16, year);
            monday = shipWeek.MondayDate;
            tuesday = monday.AddDays(1);
            wednesday = tuesday.AddDays(1);
            thursday = wednesday.AddDays(1);
            friday = thursday.AddDays(1);
            saturday = friday.AddDays(1);
            sunday = saturday.AddDays(1);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(17);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(17);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(17);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(17);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(17);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(17);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(17);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).MondayDate.Year.Should().Be(thisYear);

            shipWeek = shipWeekService.ShipWeek(17, year);
            monday = shipWeek.MondayDate;
            tuesday = monday.AddDays(1);
            wednesday = tuesday.AddDays(1);
            thursday = wednesday.AddDays(1);
            friday = thursday.AddDays(1);
            saturday = friday.AddDays(1);
            sunday = saturday.AddDays(1);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(17);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(17);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(17);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(17);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(17);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(17);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(17);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).MondayDate.Year.Should().Be(thisYear);

            shipWeek = shipWeekService.ShipWeek(18, year);
            monday = shipWeek.MondayDate;
            tuesday = monday.AddDays(1);
            wednesday = tuesday.AddDays(1);
            thursday = wednesday.AddDays(1);
            friday = thursday.AddDays(1);
            saturday = friday.AddDays(1);
            sunday = saturday.AddDays(1);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).MondayDate.Year.Should().Be(nextYear);

            shipWeek = shipWeekService.ShipWeek(50, year);
            monday = shipWeek.MondayDate;
            tuesday = monday.AddDays(1);
            wednesday = tuesday.AddDays(1);
            thursday = wednesday.AddDays(1);
            friday = thursday.AddDays(1);
            saturday = friday.AddDays(1);
            sunday = saturday.AddDays(1);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).MondayDate.Year.Should().Be(nextYear);

            shipWeek = shipWeekService.ShipWeek(51, year);
            monday = shipWeek.MondayDate;
            tuesday = monday.AddDays(1);
            wednesday = tuesday.AddDays(1);
            thursday = wednesday.AddDays(1);
            friday = thursday.AddDays(1);
            saturday = friday.AddDays(1);
            sunday = saturday.AddDays(1);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).MondayDate.Year.Should().Be(nextYear);

            shipWeek = shipWeekService.ShipWeek(52, year);

            if (shipWeek.Year != year)
                return;

            monday = shipWeek.MondayDate;
            tuesday = monday.AddDays(1);
            wednesday = tuesday.AddDays(1);
            thursday = wednesday.AddDays(1);
            friday = thursday.AddDays(1);
            saturday = friday.AddDays(1);
            sunday = saturday.AddDays(1);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).MondayDate.Year.Should().Be(nextYear);

            shipWeek = shipWeekService.ShipWeek(53, year);

            if (shipWeek.Year != year)
                return;

            monday = shipWeek.MondayDate;
            tuesday = monday.AddDays(1);
            wednesday = tuesday.AddDays(1);
            thursday = wednesday.AddDays(1);
            friday = thursday.AddDays(1);
            saturday = friday.AddDays(1);
            sunday = saturday.AddDays(1);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(tuesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(wednesday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(thursday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(friday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(saturday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);
            service.GetDefaultShipWeek(sunday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).Week.Should().Be(11);

            service.GetDefaultShipWeek(monday, "", LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code).MondayDate.Year.Should().Be(nextYear);
        }
    }
}

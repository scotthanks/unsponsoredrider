﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;




namespace BusinessObjectServicesUnitTestLibrary
{
    [TestClass]
    public class EmailQueueTests
    {
        [TestMethod]
        public void EmailAddToQueueTest()
        {
           
            var sEmptyguid = new Guid("00000000-0000-0000-0000-000000000000");
            Guid userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));
            var status = new StatusObject(userGuid, false);
            var service = new EmailQueueService(status);
            var bRet = service.AddEmailtoQueue("Service Test", "This is a service test", 1, "Test", "almam@eplantsource.com", "scotth@eplantsource.com", "", "", sEmptyguid, 0, 0, 0);
            status.Success.Should().BeTrue();
 
        }
       
    }
}

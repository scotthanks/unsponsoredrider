﻿using System;

namespace DataAccessLibrary
{
    public class ProcessResult
    {
        public Object ObjectProcessed { get; set; }
        public Exception Exception { get; set; }
        public string Result { get; set; }
    }
}

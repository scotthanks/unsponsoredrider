﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DataAccessLibrary;

namespace DataAccessLibrary
{
    public abstract class Procedure<T> : ProcedureBase where T : ProcedureParameterBase, new()
    {
        public T Parameters
        {
            get
            {
                return (T)GetParameters();
            }
            set
            {
                SetParameters(value);
            }
        }

        public System.Data.SqlClient.SqlDataReader GetReader(SqlConnection connection)
        {
            var storedProcedureServices = new StoredProcedureServices();

            return storedProcedureServices.ReaderFromSproc(connection, this);
        }

        public override ProcedureParameterBase CreateParameters()
        {
            return new T();
        }

        protected string ToCommaSeparatedList(string[] codes)
        {
            if (codes == null)
            {
                return "*";
            }
            else
            {
                var list = new StringBuilder();
                foreach (string code in codes)
                {
                    if (list.Length > 0)
                        list.Append(',');

                    list.Append(code);
                }

                return list.ToString();
            }
        }

        //TODO: Better Way?
        protected string ToCommaSeparatedList(int[] intValues)
        {
            if (intValues == null)
            {
                return "*";
            }
            else
            {
                var list = new StringBuilder();
                foreach (int value in intValues)
                {
                    if (list.Length > 0)
                        list.Append(',');

                    list.Append(value.ToString(CultureInfo.InvariantCulture));
                }

                return list.ToString();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DataAccessLibrary
{
    public class StoredProcedureServices
    {
        private SqlCommand _command;
        private SqlDataReader _reader;
        private ProcedureParameterBase _parameters;

        public SqlDataReader ReaderFromSproc<T>(SqlConnection connection, T procedure, int timeout = 0, SqlTransaction transaction = null) where T : ProcedureBase 
        {
            return ReaderFromSproc(connection, procedure.ProcedureName, procedure.GetParameters(), timeout, transaction: transaction);
        }

        public SqlDataReader ReaderFromSproc(SqlConnection connection, string procedureName, ProcedureParameterBase parameters = null, int timeout = 0, SqlTransaction transaction = null)
        {
            //ToDo: Make this primitive private and create a public class that returns reader as property along with details about the execution: elapsed time, errorMsg, rows, columns

            _command = new SqlCommand(procedureName, connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            if (transaction != null)
            {
                _command.Transaction = transaction;
            }

            try
            {
                if (parameters != null)
                {
                    _parameters = parameters;
                    parameters.AddTo(_command);
                }

                if (timeout != 0)
                {
                    _command.CommandTimeout = timeout;
                }

                _reader = _command.ExecuteReader();
            }
            catch( Exception e)
            {
                string parameterList = Utility.GetParameterList(_command.Parameters);

                if (parameterList.Length > 0)
                    parameterList = " with parameters: " + parameterList;
                else
                    parameterList = " with no parameters";

                throw new ApplicationException(string.Format("Exception occurred while trying to execute the procedure \"{0}\" against the database{1}. Inner Exception Message: \"{2}\".", _command.CommandText, parameterList, e.Message));
            }

            return _reader;
        }

        public void GetOutputParameterValues()
        {
            if (!_reader.IsClosed)
            {
                _reader.Close();
            }

            if (_parameters != null)
            {
                _parameters.GetOutputValuesFrom(_command);
            }
        }
    }
}
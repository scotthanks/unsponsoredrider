﻿using System;
using System.Configuration;
using System.Data.SqlClient;



namespace DataAccessLibrary
{
    public static class ConnectionServices
    {
        private static int _connectionCount = 0;

        public enum ConnectionStringNameEnum
        {
            DataDb,
            MembershipDb,
            SearchDb
        }

        public static string GetConnectionString( ConnectionStringNameEnum connectionStringNameEnum = ConnectionStringNameEnum.DataDb)
        {
            var connectionStringObject = ConfigurationManager.ConnectionStrings[connectionStringNameEnum.ToString()];
            if (connectionStringObject == null)
            {
                throw new ApplicationException(string.Format( "There is no connection string with a name of {0} in the config file.", connectionStringNameEnum.ToString()));
            }
            return connectionStringObject.ToString();
        }

        public static string MembershipConnectionString
        {
            get { return GetConnectionString(ConnectionStringNameEnum.MembershipDb); }
        }

        public static string DataConnectionString
        {
            get { return GetConnectionString(ConnectionStringNameEnum.DataDb); }
        }

     
        public static string SearchConnectionString
        {
            get { return GetConnectionString(ConnectionStringNameEnum.SearchDb); }
        }

        public static SqlConnection GetConnection(ConnectionStringNameEnum connectionStringNameEnum = ConnectionStringNameEnum.DataDb)
        {
            _connectionCount++;
            return new SqlConnection(GetConnectionString(connectionStringNameEnum));
        }

        public static int ConnectionCount
        {
            get { return _connectionCount; }
        }

    

        public static SqlConnection ConnectionToData
        {
            get
            {
                return GetConnection(ConnectionStringNameEnum.DataDb);
            }
        }
        public static SqlConnection ConnectionToSearch
        {
            get
            {
                return GetConnection(ConnectionStringNameEnum.SearchDb);
            }
        }

        public static SqlConnection ConnectionToMembership
        {
            get
            {
                return GetConnection(ConnectionStringNameEnum.MembershipDb);
            }
        }

        public delegate void ProcessDelegate(SqlConnection connection);

        public static void ProcessWithContentConnection(ProcessDelegate processDelegate)
        {
            using (var connection = ConnectionToData)
            {
                try
                {
                    processDelegate(connection);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary
{
    public class Constants
    {
        public const string TestPersonTableName = "Persons";
        public const string TestPersonGuid = "654164E9-90F4-467E-94FC-988FB4415DB1";
        public const int TestPersonId = 8;
    }
}

﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net.Mail;

namespace DataAccessLibrary
{
    public class MailService
    {
        public MailService()
        {
            string smtpServerUrl = GetSetting("SmtpServerUrl", typeof(string));
            string smtpServerPortString = System.Configuration.ConfigurationManager.AppSettings["SmtpServerPort"];
            string smtpUserName = System.Configuration.ConfigurationManager.AppSettings["SmtpServerUserName"];
            string smtpPassword = System.Configuration.ConfigurationManager.AppSettings["SmtpServerPassword"];

            
        }

        private string GetSetting(string settingKey, Type type)
        {
            var nameValueCollection = System.Configuration.ConfigurationManager.AppSettings[settingKey];

            if (nameValueCollection.Length == 0)
            {
                throw new ApplicationException(string.Format( "The AppSetting \"{0}\" does not exist in the configuration file.", settingKey));
            }
            else if (nameValueCollection.Length > 1)
            {
                throw new ApplicationException(string.Format("There are {0} AppSettings with a key of \"{1}\" in the Web.config file.", settingKey));
            }

            return "";
        }

        public static bool SendMail(string To, string From, string Subject, string Body, string LogPath)
        {
            try
            {
                SmtpClient oSmtp = new SmtpClient();
                oSmtp.Host = ConfigurationSettings.AppSettings["MailServer"];
                oSmtp.UseDefaultCredentials = true;

                MailAddress oAddTo = new MailAddress(To);
                MailAddress oAddFrom = new MailAddress(From);

                System.Net.Mail.MailMessage Msg = new System.Net.Mail.MailMessage(From, To);
                Msg.IsBodyHtml = true;
                Msg.From = oAddFrom;
                Msg.Subject = Subject;
                Msg.Body = Body;

                oSmtp.Send(Msg);
                return (true);
            }
            catch (Exception Exc)
            {
                string sEmail = "To:" + To + Environment.NewLine +
                    "From:" + From + Environment.NewLine +
                    "Subj:" + Subject + Environment.NewLine +
                    "Body:" + Body;

                Log.LogErrNoEmail("Mail.SendMail", sEmail + Environment.NewLine +
                    Exc.Message + Environment.NewLine + Exc.StackTrace, LogPath);
                //Log Error
                FileStream fs = new FileStream(LogPath + "ErrorLog.txt", FileMode.Append, FileAccess.Write);
                StreamWriter wr = new StreamWriter(fs);
                string sMsg = "ERROR IN Mail.SendMail on " + DateTime.Now.ToShortDateString() + " at " +
                    DateTime.Now.ToShortTimeString() + sEmail + ":  " + Exc.Message + Environment.NewLine;
                wr.Write(sMsg);
                wr.Close();

                return (false);
            }
        }

        public static bool SendMailWithAttachment(string To, string From, string Subject,
                 string Body, string FileName, string LogPath)
        {

            try
            {

                SmtpClient oSmtp = new SmtpClient();
                oSmtp.Host = ConfigurationSettings.AppSettings["MailServer"];
                oSmtp.UseDefaultCredentials = true;

                MailAddress oAddTo = new MailAddress(To);
                MailAddress oAddFrom = new MailAddress(From);

                System.Net.Mail.MailMessage Msg = new System.Net.Mail.MailMessage(From, To);
                Msg.IsBodyHtml = true;
                Msg.From = oAddFrom;
                Msg.Subject = Subject;
                Msg.Body = Body;

                System.Net.Mail.Attachment att = new Attachment(FileName);
                Msg.Attachments.Add(att);

                oSmtp.Send(Msg);

                return (true);
            }
            catch (Exception Exc)
            {
                string sEmail = "To:" + To + "\r\nFrom:" + From + "\r\nSubj:" +
                    Subject + "\r\nBody:" + Body;
                Log.LogErrNoEmail("Mail.SendMailWith", sEmail + Environment.NewLine +
                    Exc.Message + Environment.NewLine + Exc.StackTrace, LogPath);
                return (false);
            }
        }

        public static bool MailSpecial(string To, string From, string Subject,
             string sFileName, string sLogPath)
        {
            try
            {
                StreamReader sr = File.OpenText(sFileName);
                string sBody = sr.ReadToEnd();
                sr.Close();
                bool bMailOK = SendMail(To, From, Subject, sBody, sLogPath);
                return (bMailOK);
            }
            catch (Exception Exc)
            {
                Log.LogErr("Mail.MailSpecial", Exc.Message, sLogPath);
                return (false);
            }
        }

    }
   
}

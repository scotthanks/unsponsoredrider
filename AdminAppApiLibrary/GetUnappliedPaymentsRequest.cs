﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class GetUnappliedPaymentsRequest
    {
        public Guid OrderGuid { get; set; }
        public Guid GrowerGuid { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ePS.Types
{
    [DataContract]
    public class CreditCardType
    {
        [DataMember]
        public Guid Guid { get; set; }

        [DataMember]
        public string CardDescription { get; set; }

        [DataMember]
        public string CardType { get; set; }

        [DataMember]
        public string CardTypeImageUrl { get; set; }

        [DataMember]
        public string CardNumber { get; set; }

        [DataMember]
        public string ExpirationDate { get; set; }

        [DataMember]
        public string ProfileId { get; set; }

        [DataMember]
        public bool IsDefault { get; set; }

        [DataMember]
        public bool IsSelected { get; set; }
    }
}
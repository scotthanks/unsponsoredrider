﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Types
{
    public class AdminAppResponseBase
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
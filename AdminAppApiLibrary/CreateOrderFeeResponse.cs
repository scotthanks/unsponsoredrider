﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class CreateOrderFeeResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}

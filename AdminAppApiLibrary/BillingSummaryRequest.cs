﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Types
{
    public class BillingSummaryRequest
    {
        public string PaymentTypeCode { get; set; }
        public string OrderStatusCode { get; set; }
    }
}
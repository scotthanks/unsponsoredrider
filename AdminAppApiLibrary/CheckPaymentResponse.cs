﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class CheckPaymentResponse
    {
        public Guid TransactionGuid { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}

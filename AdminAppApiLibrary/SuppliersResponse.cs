﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class SupplierType
    {
        public Guid SupplierGuid { get; set; }
        public string SupplierName { get; set; }
        public string SupplierEmail { get; set; }
        public string AddressName { get; set; }
        public string StreetAddress1 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string ZipCode { get; set; }
    }

    public class SuppliersResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<SupplierType> SupplierList { get; set; }
    }
}

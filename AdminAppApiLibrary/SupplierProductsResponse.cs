﻿using ePS.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class SupplierProduct
    {
        public Guid Guid { get; set; }
        public string Code { get; set; }
        public string SpeciesCode { get; set; }
        public string SpeciesName { get; set; }
        public string VarietyCode { get; set; }
        public string VarietyName { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string GeneticOwnerName { get; set; }

        public Guid ProgramGuid { get; set; }
        public string ProgramTypeCode { get; set; }

        public string ProductDescription { get; set; }
        public string SupplierProductIdentifier { get; set; }
        public string SupplierProductDescription { get; set; }
        public string ProductFormCategoryCode { get; set; }
        public string ProductFormName { get; set; }

        public Guid ProductFormGuid { get; set; }

        public int SalesUnitQty { get; set; }
        public int LineItemMinumumQty { get; set; }
        public decimal Price { get; set; }
        public bool IncludesDelivery { get; set; }

        //public string ImageUrl { get; set; }
        public string ColorName { get; set; }
        public string HabitName { get; set; }
        public string VigorName { get; set; }

        public bool IsOrganic { get; set; }

        //public bool VarietyWasSelectedByUser { get; set; }

        public int ProductionLeadTimeWeeks { get; set; }
    }

    public class SupplierProductsResponse : AdminAppResponseBase
    {
        public List<SupplierProduct> SupplierProductsList { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Types
{
    public class BillingSummaryResponse : AdminAppResponseBase
    {
        public List<BillingSummary> SummaryList { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class  SupplierInvoiceType
    {
        public Guid SupplierGuid { get; set; }
        public Guid SupplierOrderGuid { get; set; }
        public Guid SupplierOrderNo { get; set; }
        public string EpsInvoiceNumber { get; set; }
        public string SupplierInvoiceNumber { get; set; }
        public string SupplierName { get; set; }
        public string InvoiceDateStr { get; set; }
        public string ExpectedDeliveryDateStr { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal BalanceDue { get; set; } 
    }
    public class SupplierInvoicesResponse
    {
        public bool Success { get; set; }
        public bool Message { get; set; }
        public List<SupplierInvoiceType> SupplierInvoiceList { get; set; }
    }
}

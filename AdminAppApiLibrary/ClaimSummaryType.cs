using System;
using System.Runtime.Serialization;

namespace ePS.Types
{
    [DataContract]
    public class ClaimSummaryType
    {
        [DataMember]
        public Guid ClaimGuid { get; set; }

        [DataMember]
        public string ClaimNo { get; set; }
        
        [DataMember]
        public string OrderNo { get; set; }

        [DataMember]
        public decimal? ClaimAmount { get; set; }

        [DataMember]
        public string ShipWeekString { get; set; }

        [DataMember]
        public string ProgramTypeName { get; set; }

        [DataMember]
        public string ProductFormCategoryName { get; set; }
        
        [DataMember]
        public string ClaimStatusName { get; set; }

       
    }
}
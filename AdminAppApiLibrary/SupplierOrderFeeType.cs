﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace AdminAppApiLibrary
{
    [DataContract]
    public class SupplierOrderFeeType
    {

        [DataMember]
        public Guid Guid { get; set; }

        [DataMember]
        public Guid SupplierOrderGuid { get; set; }

        [DataMember]
        public String SupplierOrderFeeTypeCode { get; set; }

        [DataMember]
        public String SupplierOrderFeeTypeDescription { get; set; }
        
        [DataMember]
        public String SupplierOrderFeeStatusCode { get; set; }

        [DataMember]
        public String SupplierOrderFeeStatusDescription { get; set; }

        [DataMember]
        public Decimal Amount { get; set; }
        
        [DataMember]
        public String FeeDescription { get; set; }
    }
}
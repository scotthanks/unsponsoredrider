﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLibrary;


namespace AdminAppApiLibrary
{
    public class GetSpeciesListForSupplierOrderResponse
    {
        public bool Success { get; set; }
        public List<Species> SpeciesList { get; set; }
        public string Message { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class UpdateOrderLineRequest
    {
        public string UserCode { get; set; }
        public Guid OrderLineGuid { get; set; }
        public decimal Cost { get; set; }
        public decimal Price { get; set; }
        public decimal QauntityShipped { get; set; }
    }
}

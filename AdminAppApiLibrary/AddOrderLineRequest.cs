﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class AddOrderLineRequest
    {
        public string UserCode { get; set; }
        public Guid SupplierOrderGuid { get; set; }
        public Guid ProductGuid { get; set; }
        public decimal Price { get; set; }
        public int QtyOrdered { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Types
{
    public class BillingSummary
    {
        public Guid OrderGuid { get; set; }
        public Guid GrowerGuid { get; set; }
        public string Name { get; set; } 
	    public decimal CreditLimit {get; set;}
        public decimal CreditLimitTemporaryIncrease { get; set; } 
	    public string CreditLimitTemporaryIncreaseExpiration {get; set;} 
	    public string SalesTaxNumber {get; set;} 
	    public string EMail {get; set;} 
	    public string PhoneAreaCode {get; set;} 
	    public string Phone {get; set;}
        public decimal FeeTotal { get; set; }
        public decimal ProductTotal { get; set; }
        public decimal OrderTotal { get; set; } 	 
	    public string CustomerPoNo {get; set;} 
	    public string OrderNumber {get; set;} 
	    public string Description {get; set;} 
	    public string Week {get; set;}  
	    public string Year {get; set;} 
	    public string OrderStatus {get; set;} 
	    public string AccountType {get; set;} 
    }
}
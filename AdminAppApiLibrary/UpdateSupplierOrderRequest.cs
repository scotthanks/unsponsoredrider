﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class UpdateSupplierOrderRequest
    {
        public string UserCode { get; set; }
        public Guid SupplierOrderGuid { get; set; }
        public string ExpectedDeliveryDate { get; set; }
        public string TrackingNumber { get; set; }
        public string TrackingComment { get; set; }

    }
}

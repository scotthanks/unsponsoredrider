﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class LedgerTransactionsResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<LedgerEntryType> LedgerEntryList { get; set; }
    }
}

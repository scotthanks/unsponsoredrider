﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web;

namespace ePS.Types
{
    [DataContract]
    public class LogType
    {
        [DataMember]
        public Guid Guid { get; set; }

        //[DataMember]
        //public DateTime LogTime { get; set; }

        [DataMember]
        public string LogTimeString { get; set; }

        [DataMember]
        public string LogTypeLookupCode { get; set; }

        [DataMember]
        public bool IsInternal { get; set; }

        [DataMember]
        public string Text { get; set; }

        [DataMember]
        public string PersonName { get; set; }

        [DataMember]
        public string UserCode { get; set; }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("LogTimeString:{0}", LogTimeString);
            stringBuilder.AppendFormat(", ");
            stringBuilder.AppendFormat("LogTypeLookupCode:{0}", LogTypeLookupCode);
            stringBuilder.AppendFormat(", ");
            stringBuilder.AppendFormat("IsInternal:{0}", IsInternal);
            stringBuilder.AppendFormat(", ");
            stringBuilder.AppendFormat("Text:\"{0}\"", Text);
            stringBuilder.AppendFormat(", ");
            stringBuilder.AppendFormat("PersonName:", PersonName);
            stringBuilder.AppendFormat(", ");
            stringBuilder.AppendFormat("UserCode:", UserCode);

            return stringBuilder.ToString();
        }
    }
}
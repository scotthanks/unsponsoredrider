﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ePS.Types
{
    [DataContract]
    public class OrderLineType : IComparable<OrderLineType>
    {
        [DataMember]
        public Guid Guid { get; set; }

        [DataMember]
        public string SpeciesCode { get; set; }

        [DataMember]
        public string SpeciesName { get; set; }

        [DataMember]
        public string VarietyCode { get; set; }

        [DataMember]
        public string VarietyName { get; set; }

        [DataMember]
        public string ProductCode { get; set; }

        [DataMember]
        public string ProductDescription { get; set; }

        [DataMember]
        public string SupplierProductIdentifier { get; set; }

        [DataMember]
        public string SupplierProductDescription { get; set; }

        [DataMember]
        public string ProductFormName { get; set; }

        [DataMember]
        public int Multiple { get; set; }

        [DataMember]
        public int Minumum { get; set; }

        [DataMember]
        public int QuantityOrdered { get; set; }

        [DataMember]
        public int QuantityShipped { get; set; }

        [DataMember]
        public bool? IsCarted { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public decimal Cost { get; set; }

        [DataMember]
        public string PriceBasisCode { get; set; }

        [DataMember]
        public int AvailableQty { get; set; }

        [DataMember]
        public string AvailabilityTypeCode { get; set; }

        [DataMember]
        public string OrderLineStatusCode { get; set; }

        [DataMember]
        public string OrderLineStatusName { get; set; }

        [DataMember]
        public bool IsLockedForReduction { get; set; }

        [DataMember]
        public bool IsLockedForAllChanges { get; set; }

        [DataMember]
        public LogType[] Logs { get; set; }

        [DataMember]
        public string DateEnteredString { get; set; }

        [DataMember]
        public string DateChangedString { get; set; }

        [DataMember]
        public string DateQtyChangedString { get; set; }

        [DataMember]
        public int LineNumber { get; set; }

        [DataMember]
        public string LineComment { get; set; }

        [DataMember]
        public long OrderLineID { get; set; }

        [DataMember]
        public long ProductID { get; set; }


        public int CompareTo(OrderLineType other)
        {
            int result = 0;

            result = System.String.Compare(this.SpeciesName, other.SpeciesName, System.StringComparison.Ordinal);

            if (result == 0)
            {
                result = System.String.Compare(this.ProductDescription, other.ProductDescription,
                                               System.StringComparison.Ordinal);
            }

            return result;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class SupplierOrderCostType
    {
        public decimal ProductOrderedCost { get; set; }
        public decimal ProductShippedCost { get; set; }
        public decimal ProductCostTotal { get; set; }
        public decimal FeeTotal { get; set; }
        public Guid SupplierOrderGuid { get; set; }
        public Guid SupplierGuid { get; set; }
        public string OrderNumber { get; set; }
        public string GrowerName { get; set; }
        public string GrowerOrderDescription { get; set; }
        public string CustomerPoNo { get; set; }
        public string PromotionalCode { get; set; }
        public string ShipWeekCode { get; set; }
        public string GrowerOrderStatusLookupCode { get; set; }
        public string SupplierName { get; set; }
        public string SupplierPoNo { get; set; }
        public string SupplierInvoiceNo { get; set; }
        public string ePsAssingedInvoiceNo { get; set; }
        public string InvoiceReceivedDate { get; set; }
        public string ePsCheckNumber { get; set; }
        public decimal ePsCheckAmount { get; set; }
        public string InvoicePaidDate { get; set; }
        public decimal SupplierInvoiceAmount { get; set; }

    }

    public class SupplierOrdersResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<SupplierOrderCostType> SupplierOrderList { get; set; }
    }
}

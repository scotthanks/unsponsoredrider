﻿using System;
using System.Runtime.Serialization;
using System.Text;

namespace ePS.Types
{
    [DataContract]
    public class ProductAvailabilityDataType : IComparable<ProductAvailabilityDataType>
    {
        [DataMember]
        public Guid ProductGuid { get; set; }

        [DataMember]
        public string Supplier { get; set; }

        [DataMember]
        public string Species { get; set; }

        [DataMember]
        public string SpeciesCode { get; set; }

        [DataMember]
        public string Product { get; set; }
        
        [DataMember]
        public string ProductCode { get; set; }
        
        [DataMember]
        public string VarietyCode { get; set; }

        [DataMember]
        public string Form { get; set; }

        [DataMember]
        public int Min { get; set; }

        [DataMember]
        public int Mult { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public bool IsOrganic { get; set; }
        
        [DataMember]
        public bool IsPW { get; set; }

        [DataMember]
        public bool IncludesDelivery { get; set; }
         
        [DataMember]
        public int OrderQuantity { get; set; }

        [DataMember]
        public bool IsCarted { get; set; }

        // For when multiple weeks of availabilities are being provided (such as on the availability page).

        [DataMember]
        public int[] OrderQuantities { get; set; }

        [DataMember]
        public bool[] IsCarteds { get; set; }

        [DataMember]
        public int[] Availabilities { get; set; }

        [DataMember]
        public string[] DisplayCodes { get; set; }

        // For when a single week of availability are being provided (such as when getting a list of substitutions).

        [DataMember]
        public int Availability { 
            get {
                if (Availabilities != null)
                {
                    return Availabilities[0];
                }
                else
                {
                    return 0;
                }
            } 
            set { } }

        [DataMember]
        public string DisplayCode { 
            get {
                if (DisplayCodes != null)
                {
                    return DisplayCodes[0];
                }
                else
                    return "OPEN";
                }
                
            set { } }

        public int CompareTo(ProductAvailabilityDataType other)
        {
            int result = 0;

            result = System.String.Compare(this.Species, other.Species, System.StringComparison.Ordinal);

            if (result == 0)
            {
                result = System.String.Compare(this.Product, other.Product, System.StringComparison.Ordinal);
            }

            return result;
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            //TODO: Finish this.
            stringBuilder.AppendFormat("ProductGuid={0}", ProductGuid);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("Product={0}", Product);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("Supplier={0}", Supplier);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("Form={0}", Form);

            return stringBuilder.ToString();
        }
        [DataMember]
        public int WeekOffset { get; set; }

    }
}
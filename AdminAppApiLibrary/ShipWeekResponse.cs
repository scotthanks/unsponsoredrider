﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePS.Types
{
    public class ShipWeekResponse : AdminAppResponseBase
    {
        public List<ShipWeekType> ShipWeekList { get; set; }
    }
}

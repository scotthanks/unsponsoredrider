﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public enum LedgerTransactionType
    {
        PayInvoiceFromUnappliedPayments,
        PayInvoiceFromUnappliedCredits,
        PaySupplierInvoiceFromEpsCheck,
        UnappliedCreditFromCreditMemo,
        UnappliedPaymentFromGrowerCheck,
        UnappliedPaymentFromEpsCheck,
        RecordSupplierInvoice
    }

    public class LedgerTransactionRequest
    {
        public string LedgerTransactionType { get; set; }
        public Guid AssetParentGuid { get; set; }
        public Guid LiabilityParentGuid { get; set; }
        public Guid GrowerGuid { get; set; }
        public Guid SupplerGuid { get; set; }
        public decimal Amount { get; set; }
        public string InternalIdNumber { get; set; }
        public string ExternalIdNumber { get; set; }
        public string UserCode { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;


namespace ePS.Types
{
    [DataContract]
    public class ShipWeekType
    {
        [DataMember]
        public Guid ShipWeekGuid { get; set; }

        [DataMember]
        public int ShipWeekId { get; set; }

        [DataMember]
        public int Week { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public string Monday { get; set; }

    }
}

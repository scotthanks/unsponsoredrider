﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class LedgerEntryType
    {
        public string TransactionDate { get; set; }

        public decimal Amount { get; set; }
        public Guid TransactionGuid { get; set; }
        public Guid AssetEntryTypeLookupGuid { get; set; }
        public string AssetInternalIdNumber { get; set; }
        public string AssetExternalIdNumber { get; set; }
        public Guid AssetParentGuid { get; set; }
        public Guid LiablityLedgerEntryTypeLookupGuid { get; set; }
        public string LiablityLedgerInternalIdNumber { get; set; }
        public string LiablityLedgerExternalIdNumber { get; set; }
        public Guid LiablityParentGuid { get; set; }
    }
}

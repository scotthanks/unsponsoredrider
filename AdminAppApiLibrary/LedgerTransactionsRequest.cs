﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public enum TransactionRequestType
    {
        InvoicePaymentsFromUnappliedPayments,
        UnappliedPaymentsFromGrowerCheck,
        UnappliedCreditsFromCreditMemo,
        InvoicePaymentFromUnappliedCredits
    }

    public class LedgerTransactionsRequest
    {
        public string TransactionRequestType { get; set; }
        public Guid OrderGuid { get; set; }
        public Guid GrowerGuid { get; set; }
    }
}

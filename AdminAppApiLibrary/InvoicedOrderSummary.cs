﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class InvoicedOrderSummary
    {
        public Guid GrowerGuid { get; set; }
        public Guid GrowerOrderGuid { get; set; }
        public decimal BillTotal { get; set; }
        public decimal PayTotal { get; set; }
        public decimal BalDue { get; set; }
        public string OrderNo { get; set; }
        public string CustomerPoNo { get; set; }
        public string PromotionalCode { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string Name { get; set; }
    }
}

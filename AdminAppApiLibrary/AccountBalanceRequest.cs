﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public enum AccountTypes
    {
        UnappliedPayments,
        UnappliedCredits,
        AccountsRecievable
    }
    public class AccountBalanceRequest
    {
        public Guid GrowerGuid { get; set; }
        public string AccountType { get; set; }
    }
}

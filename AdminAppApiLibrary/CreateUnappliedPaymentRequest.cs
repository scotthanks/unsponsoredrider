﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class CreateUnappliedPaymentRequest
    {
        public string UserCode { get; set; }
        public decimal Amount { get; set; }
        public Guid GrowerGuid { get; set; }
        public string CheckNumber { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;


namespace ePS.Types
{
    [DataContract]
    public class ProgramType
    {

        [DataMember]
        public Guid ProgramGuid { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public Guid ProgramTypeGuid { get; set; }
        [DataMember]
        public Guid SupplierGuid { get; set; }
    }
}

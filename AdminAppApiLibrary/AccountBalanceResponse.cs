﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class AccountBalanceResponse
    {
        public decimal AccountBalance { get; set; }
        public decimal DebitTotal { get; set; }
        public decimal CreditTotal { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}

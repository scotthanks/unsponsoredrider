﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class InvoicedSummaryRequest
    {
        public bool BalanceDueOrders { get; set; }
        public Guid GrowerGuid { get; set; }
    }
}

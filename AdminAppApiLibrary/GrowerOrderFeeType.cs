﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;


namespace AdminAppApiLibrary
{
    [DataContract]
    public class GrowerOrderFeeType
    {
        [DataMember]
        public Guid Guid { get; set; }

        [DataMember]
        public Guid GrowerOrderGuid { get; set; }

        [DataMember]
        public String GrowerOrderFeeTypeCode { get; set; }

        [DataMember]
        public String GrowerOrderFeeTypeDescription { get; set; }

        [DataMember]
        public String GrowerOrderFeeStatusCode { get; set; }

        [DataMember]
        public String GrowerOrderFeeStatusDescription { get; set; }

        [DataMember]
        public Decimal Amount { get; set; }

        [DataMember]
        public String FeeDescription { get; set; }
    }
}

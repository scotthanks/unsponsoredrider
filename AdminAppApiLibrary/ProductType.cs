﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;


namespace ePS.Types
{
    [DataContract]
    public class ProductType
    {

         [DataMember]
        public Guid ProductGuid { get; set; }

         [DataMember]
        public string SupplierProductIdentifier { get; set; }

         [DataMember]
        public string SupplierProductDescription { get; set; }

        [DataMember]
         public Guid ProgramGuid { get; set; }

    }
}

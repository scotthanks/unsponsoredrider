﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class CreateOrderFeeRequest
    {
        public Guid Guid { get; set; }
        public String UserCode { get; set; }
        public Guid GrowerOrderGuid { get; set; }
        public String GrowerOrderFeeTypeCode { get; set; }
        public Decimal Amount { get; set; }
    }
}

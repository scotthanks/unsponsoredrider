﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class SaveInvoiceDocumentResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }

    public class SaveInvoiceDocumentRequest
    {
        public byte[] InvoicePdf { get; set; }
        public string InvoiceNumber { get; set; }
        public Guid OrderGuid { get; set; }
    }
}

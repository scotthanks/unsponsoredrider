﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class GetSpeciesRequest
    {
        public Guid OrderLineGuid { get; set; }
        public string ShipWeek { get; set; }
    }
}

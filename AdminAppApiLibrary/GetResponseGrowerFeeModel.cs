﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace ePS.Types
{
    [DataContract]
    public class GetResponseGrowerFeeModel
    {
         [DataMember]
        public List<AdminAppApiLibrary.GrowerOrderFeeType> GrowerOrderFees { get; private set; }
    }
}
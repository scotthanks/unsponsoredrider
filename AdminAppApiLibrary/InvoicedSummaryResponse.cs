﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class InvoicedSummaryResponse
    {
        public List<InvoicedOrderSummary> SummaryList { get; set; }
    }
}

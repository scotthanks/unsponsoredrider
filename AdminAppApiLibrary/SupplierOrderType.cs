﻿using AdminAppApiLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ePS.Types
{
    [DataContract]
    public class SupplierOrderType
    {
        [DataMember]
        public Guid SupplierOrderGuid { get; set; }

        [DataMember]
        public string SupplierCode { get; set; }

        [DataMember]
        public string SupplierName { get; set; }

        [DataMember]
        public string SupplierEmail { get; set; }

        [DataMember]
        public string SupplierPhone { get; set; }

        [DataMember]
        public string SupplierOrderNo { get; set; }

        [DataMember]
        public string SupplierOrderStatusCode { get; set; }

        [DataMember]
        public SupplierOrderFeeType[] SupplierOrderFeeList { get; set; }

        [DataMember]
        public SelectListItemType[] TagRatioList { get; set; }

        [DataMember]
        public SelectListItemType[] ShipMethodList { get; set; }

        [DataMember]
        public OrderLineType[] OrderLines { get; set; }

        [DataMember]
        public LogType[] Logs { get; set; }

        [DataMember]
        public string TrackingNumber { get; set; }
        
        [DataMember]
        public string TrackingComment { get; set; }

        [DataMember]
        public string ExpectedDeliveryDate { get; set; }
        
        [DataMember]
        public string LastSentToSupplierDate { get; set; }


    }
}
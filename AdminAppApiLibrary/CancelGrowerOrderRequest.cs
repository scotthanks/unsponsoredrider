﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class CancelGrowerOrderRequest
    {
        public string UserCode { get; set; }
        public Guid GrowerOrderGuid { get; set; }
    }
}

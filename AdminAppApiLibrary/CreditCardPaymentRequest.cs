﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class CreditCardPaymentRequest
    {
        public Guid OrderGuid { get; set; }
        public Guid GrowerGuid { get; set; }
        public decimal Amount { get; set; }
        public string UserCode { get; set; }
        public string InvoiceNumber { get; set; }
        public string ReferenceNumber { get; set; }
    }

    public class CreditCardPaymentResponse
    {
        public Guid TransactionGuid { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}

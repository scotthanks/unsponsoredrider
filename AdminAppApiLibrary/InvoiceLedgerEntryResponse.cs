﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class InvoiceEntryRequest
    {
        public string OrderGuid { get; set; }
    }

    public class InvoiceLedgerEntryResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string InvoiceDate { get; set; }
        public decimal Amount { get; set; }
        public string InvoiceNumber { get; set; }
    }
}

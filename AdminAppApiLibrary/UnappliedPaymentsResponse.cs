﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminAppApiLibrary
{
    public class UnappliedPaymentsResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<LedgerEntryType> PaymentList { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace ePS.Types
{
    [DataContract]
    public class GetResponseSupplierFeeModel
    {
         [DataMember]
        public List<AdminAppApiLibrary.SupplierOrderFeeType> SupplierOrderFees { get; private set; }
    }
}
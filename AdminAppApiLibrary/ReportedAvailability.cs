﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ePS.Types
{
    [DataContract]
    public class ReportedAvailabilityType
    {
        [DataMember]
        public Guid Guid { get; set; }

        [DataMember]
        public Guid ProductGuid { get; set; }

        [DataMember]
        public Guid ShipWeekGuid { get; set; }

        [DataMember]
        public int Qty { get; set; }

    }
}

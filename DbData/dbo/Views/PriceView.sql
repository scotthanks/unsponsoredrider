﻿CREATE VIEW [dbo].[PriceView]
AS
	SELECT
		Price.RegularCost,
		Price.EODCost,
		Price.RegularMUPercent,
		Price.EODMUPercent,
		PriceGroupView.PriceGroupCode,
		PriceGroupView.PriceGroupName,
		PriceGroupView.TagCost,
		VolumeLevelView.VolumeLevelCode,
		VolumeLevelView.VolumeLevelName,
		VolumeLevelView.VolumeLow,
		VolumeLevelView.VolumeHigh,
		VolumeLevelView.ProgramSeasonCode,
		VolumeLevelView.ProgramSeasonName,
		VolumeLevelView.SeasonStartDate,
		VolumeLevelView.SeasonEndDate,
		VolumeLevelView.EODDate,
		VolumeLevelView.ProgramCode,
		VolumeLevelView.ProgramTypeCode,
		VolumeLevelView.SupplierCode,
		VolumeLevelView.SupplierName,
		VolumeLevelView.ProductFormCategoryCode,
		Price.Id AS PriceId,
		Price.Guid AS PriceGuid,
		PriceGroupView.PriceGroupId,
		PriceGroupView.PriceGroupGuid,
		VolumeLevelView.VolumeLevelId,
		VolumeLevelView.VolumeLevelGuid,
		VolumeLevelView.ProgramSeasonId,
		VolumeLevelView.ProgramSeasonGuid,
		VolumeLevelView.ProgramGuid,
		VolumeLevelView.ProgramTypeGuid,
		VolumeLevelView.DefaultPeakWeekNumber,
		VolumeLevelView.DefaultSeasonEndWeekNumber,
		VolumeLevelView.SupplierGuid,
		VolumeLevelView.ProductFormCategoryGuid
	FROM Price
		INNER JOIN PriceGroupView
			ON Price.PriceGroupGuid = PriceGroupView.PriceGroupGuid
		INNER JOIN VolumeLevelView
			ON Price.VolumeLevelGuid = VolumeLevelView.VolumeLevelGuid
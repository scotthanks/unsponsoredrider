﻿
CREATE VIEW [dbo].[GrowerOrderLineStatusPendingCountView]
AS
	SELECT
		OrderLineView.GrowerOrderGuid,
		COUNT(DISTINCT OrderLineView.OrderLineGuid) AS CountOfPendingStatusLines
	FROM OrderLineView
	WHERE OrderLineView.OrderLineStatusLookupCode='Pending'
	GROUP BY OrderLineView.GrowerOrderGuid
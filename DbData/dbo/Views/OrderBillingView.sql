﻿


CREATE VIEW [dbo].[OrderBillingView]
AS
Select prodTbl.pguid as OrderGuid, 
		prodTbl.OrderNumber,
		prodTbl.Description,
		prodTbl.CustomerPoNo, 
		prodTbl.ShipWeekGuid, 
		prodTbl.PaymentTypeLookupGuid,
		prodTbl.GrowerGuid, 
		prodTbl.GrowerOrderStatusLookupGuid,
		prodTbl.ShipToAddressGuid,
		prodTbl.DateEntered,
		prodTbl.GrowerCreditCardGuid,
		prodTbl.PersonGuid,
		feeTable.FeeTotal, 
		growerFeeTable.GrowerFeeTotal,
		prodTbl.ProductTotal,
		( ISNULL(prodTbl.ProductTotal, 0) + ISNULL(feeTable.FeeTotal, 0) +  ISNULL(growerFeeTable.GrowerFeeTotal, 0)) as OrderTotal
		,prodTbl.SellerCode
		 from
(Select GrowerOrder.Guid as pguid, 
	GrowerOrder.OrderNo as OrderNumber,
	GrowerOrder.Description as Description,
	GrowerOrder.CustomerPoNo as CustomerPoNo, 
	GrowerOrder.ShipWeekGuid as ShipWeekGuid, 
	GrowerOrder.PaymentTypeLookupGuid as PaymentTypeLookupGuid,
	GrowerOrder.GrowerGuid as GrowerGuid,
	GrowerOrder.ShipToAddressGuid as ShipToAddressGuid,
	GrowerOrder.DateEntered as DateEntered,
	GrowerOrder.GrowerCreditCardGuid as GrowerCreditCardGuid,
	GrowerOrder.GrowerOrderStatusLookupGuid as GrowerOrderStatusLookupGuid,
	GrowerOrder.PersonGuid as PersonGuid,
--	Sum(OrderLine.ActualPrice*OrderLine.QtyOrdered) as ProductTotal  
	Sum(OrderLine.ActualPrice*OrderLine.QtyShipped) as ProductTotal,
	Max(Seller.Code) as SellerCode  
	from GrowerOrder
    join SupplierOrder on SupplierOrder.GrowerOrderGuid = GrowerOrder.Guid
	Join Supplier on SupplierOrder.SupplierGuid = Supplier.Guid
	join Seller on Supplier.SellerGuid = Seller.guid
	join OrderLine on OrderLine.SupplierOrderGuid = SupplierOrder.Guid
    Group by 
		GrowerOrder.OrderNo,
		GrowerOrder.Description,
		GrowerOrder.Guid, 
		GrowerOrder.CustomerPoNo, 
		GrowerOrder.ShipWeekGuid, 
		GrowerOrder.PaymentTypeLookupGuid, 
		GrowerOrder.GrowerGuid,
		GrowerOrder.ShipToAddressGuid,
		GrowerOrder.GrowerOrderStatusLookupGuid,
		GrowerOrder.DateEntered,
		GrowerOrder.GrowerCreditCardGuid,
		GrowerOrder.PersonGuid,
		GrowerOrderStatusLookupGuid
		) prodTbl
left join
(Select GrowerOrder.Guid as fguid, 
	Sum(SupplierOrderFee.Amount) as FeeTotal from
	GrowerOrder
	join SupplierOrder on SupplierOrder.GrowerOrderGuid = GrowerOrder.Guid
	join SupplierOrderFee on SupplierOrderFee.SupplierOrderGuid = SupplierOrder.Guid
    Group by GrowerOrder.Guid) feeTable
on prodTbl.pguid = feeTable.fguid
left join
(Select GrowerOrder.Guid as fguid, 
	Sum(GrowerOrderFee.Amount) as GrowerFeeTotal from
	GrowerOrder
	join GrowerOrderFee on GrowerOrderFee.GrowerOrderGuid = GrowerOrder.Guid
    Group by GrowerOrder.Guid) growerFeeTable
on prodTbl.pguid = growerFeeTable.fguid
﻿CREATE VIEW [dbo].[GrowerVolumeView]
AS
	SELECT
		GrowerView.GrowerCode,
		GrowerView.GrowerName,
		GrowerVolumeBaseView.VolumeLevelType,
		VolumeLevelView.VolumeLow,
		VolumeLevelView.VolumeHigh,
		VolumeLevelView.ProgramSeasonCode,
		VolumeLevelView.ProgramSeasonName,
		VolumeLevelView.SeasonStartDate,
		VolumeLevelView.SeasonEndDate,
		VolumeLevelView.EODDate,
		VolumeLevelView.ProgramCode,
		VolumeLevelView.ProgramTypeCode,
		VolumeLevelView.SupplierCode,
		VolumeLevelView.SupplierName,
		VolumeLevelView.ProductFormCategoryCode,
		GrowerVolumeBaseView.GrowerVolumeId,
		GrowerVolumeBaseView.GrowerVolumeGuid,
		GrowerView.GrowerId,
		GrowerView.GrowerGuid,
		GrowerView.AddressId,
		GrowerView.AddressGuid,
		GrowerView.StateGuid,
		GrowerView.StateId,
		GrowerView.CountryGuid,
		GrowerView.CountryId,
		GrowerView.GrowerTypeLookupId,
		GrowerView.GrowerTypeLookupGuid,
		GrowerView.DefaultPaymentTypeLookupId,
		GrowerView.DefaultPaymentTypeLookupGuid,
		VolumeLevelView.VolumeLevelId,
		VolumeLevelView.VolumeLevelGuid,
		VolumeLevelView.ProgramSeasonId,
		VolumeLevelView.ProgramSeasonGuid,
		VolumeLevelView.ProgramGuid,
		VolumeLevelView.ProgramTypeGuid,
		VolumeLevelView.DefaultPeakWeekNumber,
		VolumeLevelView.DefaultSeasonEndWeekNumber,
		VolumeLevelView.SupplierGuid,
		VolumeLevelView.ProductFormCategoryGuid
	FROM GrowerVolumeBaseView
		INNER JOIN GrowerView
			ON GrowerVolumeBaseView.GrowerGuid=GrowerView.GrowerGuid
		LEFT OUTER JOIN VolumeLevelView AS VolumeLevelView
			ON GrowerVolumeBaseView.VolumeLevelGuid=VolumeLevelView.VolumeLevelGuid
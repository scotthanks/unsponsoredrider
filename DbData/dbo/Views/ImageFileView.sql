﻿

CREATE VIEW [dbo].[ImageFileView]
AS
	SELECT
		ImageFile.Guid AS ImageFileGuid,
		ImageFile.Id AS ImageFileId,
		ImageFile.Code AS ImageFileCode,
		ImageFile.Name AS ImageFileName,
		ImageFile.Height,
		ImageFile.Width,
		ImageFile.Size,
		ImageFile.Notes,
		ImageFile.ImageSetGuid,
		FileExtensionLookup.Guid AS FileExtensionLookupGuid,
		FileExtensionLookup.Id AS FileExtensionLookupId,
		FileExtensionLookup.Code AS FileExtensionLookupCode,
		FileExtensionLookup.Name AS FileExtensionLookupName,
		ImageFileTypeLookup.Guid AS ImageFileTypeLookupGuid,
		ImageFileTypeLookup.Id AS ImageFileTypeLookupId,
		ImageFileTypeLookup.Code AS ImageFileTypeLookupCode,
		ImageFileTypeLookup.Name AS ImageFileTypeLookupName,
		TripleFileExtensionMimeTypeView.MimeTypeLookupId,
		TripleFileExtensionMimeTypeView.MimeTypeLookupGuid,
		TripleFileExtensionMimeTypeView.MimeTypeLookupCode,
		TripleFileExtensionMimeTypeView.MimeTypeLookupName
	FROM ImageFile
		INNER JOIN Lookup AS FileExtensionLookup
			ON ImageFile.FileExtensionLookupGuid = FileExtensionLookup.Guid
		INNER JOIN Lookup AS ImageFileTypeLookup
			ON ImageFile.ImageFileTypeLookupGuid = ImageFileTypeLookup.Guid
		LEFT OUTER JOIN TripleFileExtensionMimeTypeView
			ON ImageFile.FileExtensionLookupGuid = TripleFileExtensionMimeTypeView.FileExtensionLookupGuid
	WHERE
		ImageFile.DateDeactivated IS NULL AND
		ImageFileTypeLookup.DateDeactivated IS NULL
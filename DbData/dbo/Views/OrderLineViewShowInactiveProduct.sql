﻿





CREATE view [dbo].[OrderLineViewShowInactiveProduct]
AS
	SELECT
		SupplierOrderView.GrowerOrderNo,
		SupplierOrderView.SupplierPoNo,
		SupplierOrderView.GrowerOrderDescription,
		SupplierOrderView.ProductFormCategoryCode AS GrowerOrderProductFormCategoryCode,
		SupplierOrderView.ProductFormCategoryName AS GrowerOrderProductFormCategoryName,
		SupplierOrderView.ProgramTypeCode AS GrowerOrderProgramTypeCode,
		SupplierOrderView.ProgramTypeName AS GrowerOrderProgramTypeName,
		SupplierOrderView.PersonGuid,
		OrderLineStatusLookupView.LookupCode AS OrderLineStatusLookupCode,
		OrderLineStatusLookupView.LookupName AS OrderLineStatusLookupName,
		OrderLineStatusLookupView.SortSequence AS OrderLineStatusLookupSortSequence,
		SupplierOrderView.SupplierOrderStatusLookupCode,
		SupplierOrderView.SupplierOrderStatusLookupName,
		OrderLine.Id AS OrderLineId,
		OrderLine.Guid AS OrderLineGuid,
		OrderLine.QtyOrdered,
		OrderLine.QtyShipped,
		OrderLine.ActualPriceUsedGuid,
		OrderLine.LineNumber,
		Variety.Code as VarietyCode,
		Species.Code as SpeciesCode,
		GeneticOwner.Code as GeneticOwnerCode,
		ProductForm.Code as ProductFormCode,
		ProductFormCategory.Code as ProductFormCategoryCode,
		Product.Code as ProductCode,
		SupplierOrderView.SupplierOrderId,
		SupplierOrderView.SupplierOrderGuid,
		SupplierOrderView.GrowerOrderGuid,
		SupplierOrderView.GrowerOrderId,
		SupplierOrderView.CustomerPoNo,
		SupplierOrderView.GrowerGuid,
		SupplierOrderView.GrowerId,
		SupplierOrderView.GrowerCode,
		SupplierOrderView.GrowerName,
		SupplierOrderView.ShipWeekGuid,
		SupplierOrderView.ShipWeekId,
		SupplierOrderView.ShipWeekCode,
		SupplierOrderView.ProductFormCategoryGuid AS GrowerOrderProductFormCategoryGuid,
		SupplierOrderView.ProductFormCategoryId AS GrowerOrderProductFormCategoryId,
		SupplierOrderView.IsRooted,
		SupplierOrderView.ProgramTypeGuid AS GrowerOrderProgramTypeGuid,
		SupplierOrderView.ProgramTypeId AS GrowerOrderProgramTypeId,
		SupplierOrderView.OrderTypeLookupGuid,
		SupplierOrderView.OrderTypeLookupCode,
		SupplierOrderView.GrowerOrderStatusLookupGuid,
		SupplierOrderView.GrowerOrderStatusLookupCode,
		SupplierOrderView.GrowerOrderStatusLookupName,
		SupplierOrderView.SupplierId,
		SupplierOrderView.SupplierGuid,
		SupplierOrderView.SupplierCode,
		SupplierOrderView.SupplierName,
		SupplierOrderView.AddressGuid,
		SupplierOrderView.AddressId,
		SupplierOrderView.AddressName,
		SupplierOrderView.StreetAddress1,
		SupplierOrderView.StreetAddress2,
		SupplierOrderView.City,
		SupplierOrderView.StateGuid,
		SupplierOrderView.StateId,
		SupplierOrderView.StateCode,
		SupplierOrderView.CountryGuid,
		SupplierOrderView.CountryId,
		SupplierOrderView.CountryCode,
		SupplierOrderView.CountryName,
		SupplierOrderView.ZipCode,
		SupplierOrderView.SupplierOrderStatusLookupGuid,
		SupplierOrderView.SupplierOrderStatusLookupId,
		SupplierOrderView.SupplierOrderStatusLookupSortSequence,
		SupplierOrderView.UserGuid,
		Product.Guid as ProductGuid,
		Program.Guid as ProgramGuid,
		Program.Code as ProgramCode,
		ProgramType.Guid as ProgramTypeGuid,
		ProgramType.Code as ProgramTypeCode,
		Program.SupplierGuid AS ProductSupplierGuid,
		Product.VarietyGuid,
		Variety.SpeciesGuid,
		GeneticOwner.Guid as GeneticOwnerGuid,
		Variety.Name as VarietyName,
		Variety.ActiveDate as VarietyActiveDate,
		Variety.DeactiveDate as VarietyDeactiveDate,
		--ProductView.VarietyColorDescription,
		--ProductView.VarietyColorLookupGuid,
		--ProductView.VarietyColorLookupCode,
		--ProductView.VarietyHabitLookupGuid,
		--ProductView.VarietyHabitLookupCode,
		--ProductView.VarietyVigorLookupGuid,
		--ProductView.VarietyVigorLookupCode,
		--ProductView.VarietyImageSetGuid,
		--ProductView.VarietyThumbnailImageFileGuid,
		--ProductView.VarietyThumbnailImageFileFileExtensionLookupGuid,
		Product.ProductFormGuid,
		ProductForm.ProductFormCategoryGuid,
		OrderLineStatusLookupView.LookupGuid AS OrderLineStatusLookupGuid,
		CASE WHEN SupplierOrderView.SupplierGuid = Program.SupplierGuid THEN 1 ELSE 0 END AS SupplierGuidsMatch,
		CASE WHEN SupplierOrderView.ProductFormCategoryGuid = ProductForm.ProductFormCategoryGuid THEN 1 ELSE 0 END AS ProductFormCategoryGuidsMatch,
		CASE WHEN SupplierOrderView.ProgramTypeGuid = Program.ProgramTypeGuid THEN 1 ELSE 0 END AS ProgramTypeGuidsMatch,
		SupplierOrderView.SellerGuid,
		SupplierOrderView.SellerCode
	FROM OrderLine
		INNER JOIN SupplierOrderView
			ON OrderLine.SupplierOrderGuid = SupplierOrderView.SupplierOrderGuid
		INNER JOIN Product
			ON OrderLine.ProductGuid = Product.Guid
		INNER JOIN ProductForm
			ON Product.ProductFormGuid = ProductForm.Guid
		INNER JOIN ProductFormCategory
			ON ProductForm.ProductFormCategoryGuid = ProductFormCategory.Guid
		INNER JOIN Variety
			ON Product.VarietyGuid = Variety.Guid
		INNER JOIN GeneticOwner
			ON Variety.GeneticOwnerGuid = GeneticOwner.Guid
		INNER JOIN Species
			ON Variety.SpeciesGuid = Species.Guid
		INNER JOIN Program 
			ON Product.ProgramGuid = Program.Guid
		INNER JOIN ProgramType
			ON Program.ProgramTypeGuid = ProgramType.Guid
		INNER JOIN LookupView AS OrderLineStatusLookupView
			ON OrderLIne.OrderLineStatusLookupGuid = OrderLineStatusLookupView.LookupGuid
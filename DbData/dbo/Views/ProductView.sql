﻿




CREATE VIEW [dbo].[ProductView]
AS
	SELECT
		Product.Code AS ProductCode,
		Product.SupplierIdentifier AS ProductSupplierIdentifier,
		Product.SupplierDescription AS ProductSupplierDescription,
		Product.ProductDescription,
		Product.ProductionLeadTimeWeeks,
		Product.IsOrganic,
		Product.IsExclusive,
		ProgramView.ProgramCode,
		ProgramView.ProgramTypeCode,
		ProgramView.SupplierCode,
		ProgramView.SupplierName,
		ProgramView.SellerCode,
		VarietyView.VarietyCode,
		VarietyView.GeneticOwnerCode,
		VarietyView.Name AS VarietyName,
		VarietyView.SpeciesCode,
		VarietyView.SpeciesName,
		VarietyView.ColorLookupCode AS VarietyColorLookupCode,
		VarietyView.HabitLookupCode AS VarietyHabitLookupCode,
		VarietyView.VigorLookupCode AS VarietyVigorLookupCode,
		ProductFormView.ProductFormCode,
		ProductFormView.ProductFormCategoryCode,
		ProductFormView.SalesUnitQty,
		ProductFormView.LineItemMinimumQty,
		PriceGroupView.PriceGroupCode,
		PriceGroupView.PriceGroupName,
		PriceGroupView.TagCost,
		ProductFormView.ProductFormCategoryGuid,
		Product.Guid AS ProductGuid,
		ProgramView.ProgramGuid,
		ProgramView.ProgramTypeGuid,
		ProgramView.SupplierGuid,
		ProgramView.ProductFormCategoryGuid AS ProductFormCategoryOfProgramGuid,
		ProgramView.ProductFormCategoryCode AS ProductFormCategoryOfProgramCode,
		VarietyView.VarietyGuid,
		VarietyView.SpeciesGuid,
		VarietyView.GeneticOwnerGuid,
		VarietyView.ActiveDate AS VarietyActiveDate,
		VarietyView.DeactiveDate AS VarietyDeactiveDate,
		VarietyView.ColorDescription AS VarietyColorDescription,
		VarietyView.ColorLookupGuid AS VarietyColorLookupGuid,
		VarietyView.HabitLookupGuid AS VarietyHabitLookupGuid,
		VarietyView.VigorLookupGuid AS VarietyVigorLookupGuid,
		VarietyView.ImageSetGuid AS VarietyImageSetGuid,
		VarietyView.ThumbnailImageFileGuid AS VarietyThumbnailImageFileGuid,
		VarietyView.ThumbnailImageFileFileExtensionLookupGuid AS VarietyThumbnailImageFileFileExtensionLookupGuid,
		ProductFormView.ProductFormGuid,
		CASE WHEN (ProductFormView.ProductFormCategoryGuid = ProgramView.ProductFormCategoryGuid) THEN 1 ELSE 0 END AS ProductFormCategoriesMatch,
		PriceGroupView.PriceGroupId,
		PriceGroupView.PriceGroupGuid
	FROM Product with (nolock)
		INNER JOIN ProgramView with (nolock)
			ON Product.ProgramGuid = ProgramView.ProgramGuid
		INNER JOIN VarietyView with (nolock)
			ON Product.VarietyGuid = VarietyView.VarietyGuid
		INNER JOIN ProductFormView with (nolock)
			ON Product.ProductFormGuid = ProductFormView.ProductFormGuid
		INNER JOIN PriceGroupView with (nolock)
			ON Product.PriceGroupGuid = PriceGroupView.PriceGroupGuid
	WHERE
		Product.DateDeactivated IS NULL
﻿
--SELECT * FROM ProductGrowerSeasonVolumeLevelPriceViewX WHERE GrowerVolumeAndPriceHaveSameSeason = 0
--SELECT * FROM ProductGrowerSeasonVolumeLevelPriceViewX WHERE ProductPriceGroupProgramSeasonAndPriceHaveSameSeason = 0

CREATE VIEW [dbo].[ProductGrowerSeasonVolumeLevelPriceViewX] WITH SCHEMABINDING
AS
	SELECT
		dbo.Product.Guid as ProductGuid,
		dbo.GrowerVolume.GrowerGuid,
		dbo.GrowerVolume.ProgramSeasonGuid,
		dbo.ProgramSeason.StartDate as SeasonStartDate,
		dbo.ProgramSeason.EndDate as SeasonEndDate,
		dbo.Price.guid as PriceGuid,
		CASE WHEN dbo.GrowerVolume.ProgramSeasonGuid=dbo.ProductPriceGroupProgramSeason.ProgramSeasonGuid THEN
			CAST(1 AS BIT)
		ELSE
			CAST(0 AS BIT)
		END AS GrowerVolumeAndPriceHaveSameSeason,
		CASE WHEN dbo.ProductPriceGroupProgramSeason.ProgramSeasonGuid=dbo.VolumeLevel.ProgramSeasonGuid THEN
			CAST(1 AS BIT)
		ELSE
			CAST(0 AS BIT)
		END AS ProductPriceGroupProgramSeasonAndPriceHaveSameSeason
	FROM dbo.GrowerVolume
		INNER JOIN dbo.VolumeLevel on Coalesce(dbo.GrowerVolume.QuotedVolumeLevelGuid,dbo.GrowerVolume.ActualVolumeLevelGuid,dbo.GrowerVolume.EstimatedVolumeLevelGuid) = dbo.VolumeLevel.Guid
		INNER JOIN dbo.ProgramSeason on dbo.GrowerVolume.ProgramSeasonGuid = dbo.ProgramSeason.Guid
		INNER JOIN dbo.Program on dbo.ProgramSeason.ProgramGuid = dbo.Program.Guid
		INNER JOIN dbo.Product ON dbo.Program.Guid=dbo.Product.ProgramGuid
		INNER JOIN dbo.PriceGroup on dbo.Product.PriceGroupGuid = dbo.PriceGroup.Guid
		INNER JOIN dbo.ProductPriceGroupProgramSeason ON
				dbo.Product.Guid = dbo.ProductPriceGroupProgramSeason.ProductGuid AND
				dbo.GrowerVolume.ProgramSeasonGuid = dbo.ProductPriceGroupProgramSeason.ProgramSeasonGuid
		INNER JOIN dbo.Price
			ON
				dbo.VolumeLevel.Guid = dbo.Price.VolumeLevelGuid AND
				dbo.ProductPriceGroupProgramSeason.PriceGroupGuid = dbo.Price.PriceGroupGuid
GO
CREATE UNIQUE CLUSTERED INDEX [ixProductGrowerSeasonVolumeLevelPriceViewX_GrowerProductSeason]
    ON [dbo].[ProductGrowerSeasonVolumeLevelPriceViewX]([GrowerGuid] ASC, [ProductGuid] ASC, [ProgramSeasonGuid] ASC, [SeasonStartDate] ASC);


﻿
CREATE VIEW [dbo].[AddressView]
AS
	SELECT
		[Id] AS AddressId,
		[Guid] AS AddressGuid,
		[Name] AS AddressName,
		[StreetAddress1],
		[StreetAddress2],
		[City],
		[ZipCode],
		StateView.StateGuid,
		StateView.StateId,
		StateView.StateCode,
		StateView.StateName,
		StateView.CountryGuid,
		StateView.CountryId,
		StateView.CountryCode,
		StateView.CountryName
	FROM Address
		INNER JOIN StateView
			ON Address.StateGuid = StateView.StateGuid
	WHERE DateDeactivated IS NULL
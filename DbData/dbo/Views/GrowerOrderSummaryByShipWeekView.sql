﻿



CREATE view [dbo].[GrowerOrderSummaryByShipWeekView]
AS  
	SELECT
		OrderLineView.GrowerOrderGuid,
		OrderLineView.GrowerGuid,
		OrderLineView.OrderTypeLookupGuid,
		OrderLineView.OrderTypeLookupCode,
		OrderLineView.ProgramTypeGuid,
		OrderLineView.ProgramTypeCode,
		OrderLineView.ProductFormCategoryGuid,
		OrderLineView.ProductFormCategoryCode,
		OrderLineView.ShipWeekGuid,
		OrderLineView.ShipWeekCode,
		ISNULL(COUNT(DISTINCT OrderLineView.SupplierOrderGuid),0) AS SupplierOrderCount,
		ISNULL(COUNT(DISTINCT OrderLineView.SupplierOrderGuid),0) AS SupplierOrderWithOrderLinesCount,
		ISNULL(COUNT(DISTINCT OrderLineView.ProductGuid),0) AS ProductCount,
		ISNULL(COUNT(DISTINCT(CASE WHEN OrderLineView.QtyOrdered > 0 THEN ProductGuid ELSE NULL END)),0) AS ProductWithNonZeroOrderQtyCount,
		ISNULL(COUNT(DISTINCT OrderLineView.OrderLineGuid),0) AS OrderLineCount,
		SUM(CASE WHEN OrderLineView.QtyOrdered > 0 THEN 1 ELSE 0 END) AS OrderLineWithNonZeroOrderQtyCount,
		ISNULL(SUM(OrderLineView.QtyOrdered),0) AS QtyOrderedCount,
		OrderLineView.GrowerOrderStatusLookupGuid,
		OrderLineView.GrowerOrderStatusLookupCode,
		GrowerOrderLowestSupplierOrderStatusLookupView.LookupGuid as LowestSupplierOrderStatusLookupGuid,
		GrowerOrderLowestSupplierOrderStatusLookupView.LookupCode as LowestSupplierOrderStatusLookupCode,
		GrowerOrderLowestOrderLineStatusLookupView.LookupGuid as LowestOrderLineStatusLookupGuid,
		GrowerOrderLowestOrderLineStatusLookupView.LookupCode as LowestOrderLineStatusLookupCode,
		OrderLineView.PersonGuid,
		CASE
			WHEN GrowerOrderLineStatusPendingCountView.GrowerOrderGuid IS NULL THEN 
				0 
			ELSE 
				GrowerOrderLineStatusPendingCountView.CountOfPendingStatusLines 
			END 
		AS CountOfPendingStatusLines,
		'' as InvoiceNo
	FROM OrderLineViewShowInactiveProduct OrderLineView
		INNER JOIN GrowerOrderLowestOrderLineStatusView
			ON OrderLineView.GrowerOrderGuid = GrowerOrderLowestOrderLineStatusView.GrowerOrderGuid
		INNER JOIN LookupView GrowerOrderLowestOrderLineStatusLookupView 
			ON GrowerOrderLowestOrderLineStatusView.OrderLineStatusLookupGuid = GrowerOrderLowestOrderLineStatusLookupView.LookupGuid
		INNER JOIN GrowerOrderLowestSupplierOrderStatusView
			ON OrderLineView.GrowerOrderGuid = GrowerOrderLowestSupplierOrderStatusView.GrowerOrderGuid
		INNER JOIN LookupView GrowerOrderLowestSupplierOrderStatusLookupView 
			ON GrowerOrderLowestSupplierOrderStatusView.SupplierOrderStatusLookupGuid = GrowerOrderLowestSupplierOrderStatusLookupView.LookupGuid
		LEFT OUTER JOIN GrowerOrderLineStatusPendingCountView
			ON OrderLineView.GrowerOrderGuid = GrowerOrderLineStatusPendingCountView.GrowerOrderGuid
	WHERE OrderLineView.OrderLineStatusLookupCode != 'PreCart'
	GROUP BY
		OrderLineView.GrowerOrderGuid,
		OrderLineView.GrowerGuid,
		OrderLineView.OrderTypeLookupGuid,
		OrderLineView.OrderTypeLookupCode,
		OrderLineView.ProgramTypeGuid,
		OrderLineView.ProgramTypeCode,
		OrderLineView.ProductFormCategoryGuid,
		OrderLineView.ProductFormCategoryCode,
		OrderLineView.SupplierOrderStatusLookupGuid,
		OrderLineView.SupplierOrderStatusLookupCode,
		OrderLineView.ShipWeekGuid,
		OrderLineView.SHipWeekCode,
		OrderLineView.GrowerOrderStatusLookupGuid,
		OrderLineView.GrowerOrderStatusLookupCode,
		GrowerOrderLowestSupplierOrderStatusLookupView.LookupGuid,
		GrowerOrderLowestSupplierOrderStatusLookupView.LookupCode,
		GrowerOrderLowestOrderLineStatusLookupView.LookupGuid,
		GrowerOrderLowestOrderLineStatusLookupView.LookupCode,
		OrderLineView.PersonGuid,
		GrowerOrderLineStatusPendingCountView.GrowerOrderGuid,
		GrowerOrderLineStatusPendingCountView.CountOfPendingStatusLines
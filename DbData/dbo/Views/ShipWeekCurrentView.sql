﻿CREATE VIEW [dbo].[ShipWeekCurrentView]
AS
	SELECT TOP 1 * 
	FROM ShipWeekView 
	WHERE ShipWeekMondayDate > DATEADD(WEEK, -1, CURRENT_TIMESTAMP)
	ORDER BY ShipWeekMondayDate
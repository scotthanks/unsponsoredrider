﻿	create view dbo.ProductFormCategoryProgramTypeCombinationView
	AS
		SELECT
			ProductView.ProductFormCategoryGuid,
			ProductView.ProductFormCategoryCode,
			ProductView.ProgramTypeGuid,
			ProductView.ProgramTypeCode
		FROM ProductView
		GROUP BY
			ProductView.ProductFormCategoryGuid,
			ProductView.ProductFormCategoryCode,
			ProductView.ProgramTypeGuid,
			ProductView.ProgramTypeCode
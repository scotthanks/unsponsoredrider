﻿-- Should be empty

CREATE VIEW dbo.ProductSeasonBasePriceCountView
AS
	SELECT
		ProductGuid,
		ProgramSeasonGuid,
		COUNT(PriceGuid) AS PriceCount
	FROM ProductSeasonBasePriceView
	GROUP BY
		ProductGuid,
		ProgramSeasonGuid
	HAVING COUNT(PriceGuid) > 1
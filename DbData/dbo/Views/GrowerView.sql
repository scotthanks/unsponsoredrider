﻿CREATE view [dbo].[GrowerView]
AS
	SELECT
		Grower.Code AS GrowerCode,
		Grower.Name AS GrowerName,
		Grower.IsActivated AS GrowerIsActivated,
		Grower.AllowSubstitutions,
		Grower.CreditLimit,
		Grower.CreditLimitTemporaryIncrease,
		Grower.Email,
		Grower.PhoneAreaCode,
		Grower.Phone,
		Grower.SalesTaxNumber,
		AddressView.AddressName,
		AddressView.StreetAddress1,
		AddressView.StreetAddress2,
		AddressView.City,
		AddressView.ZipCode,
		AddressView.StateCode,
		AddressView.StateName,
		AddressView.CountryCode,
		AddressView.CountryName,
		GrowerTypeLookupView.LookupCode AS GrowerTypeLookupCode,
		GrowerTypeLookupView.LookupName AS GrowerTypeLookupName,
		DefaultPaymentTypeLookupView.LookupCode AS DefaultPaymentTypeLookupCode,
		DefaultPaymentTypeLookupView.LookupName AS DefaultPaymentTypeLookupName,
		Grower.Id AS GrowerId,
		Grower.Guid AS GrowerGuid,
		AddressView.AddressId,
		AddressView.AddressGuid,
		AddressView.StateGuid,
		AddressView.StateId,
		AddressView.CountryGuid,
		AddressView.CountryId,
		GrowerTypeLookupView.LookupId AS GrowerTypeLookupId,
		GrowerTypeLookupView.LookupGuid AS GrowerTypeLookupGuid,
		DefaultPaymentTypeLookupView.LookupId AS DefaultPaymentTypeLookupId,
		DefaultPaymentTypeLookupView.LookupGuid AS DefaultPaymentTypeLookupGuid
	FROM Grower
		INNER JOIN AddressView
			ON Grower.AddressGuid = AddressView.AddressGuid
		INNER JOIN LookupView AS GrowerTypeLookupView
			ON Grower.GrowerTypeLookupGuid = GrowerTypeLookupView.LookupGuid
		INNER JOIN LookupView AS DefaultPaymentTypeLookupView
			ON Grower.DefaultPaymentTypeLookupGuid = DefaultPaymentTypeLookupView.LookupGuid
	WHERE Grower.DateDeactivated IS NULL
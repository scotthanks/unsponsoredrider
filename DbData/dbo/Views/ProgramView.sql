﻿


CREATE VIEW [dbo].[ProgramView]
AS
	SELECT
		Program.Code AS ProgramCode,
		ProgramTypeView.ProgramTypeCode,
		SupplierView.SupplierCode,
		SupplierView.SupplierName,
		ProductFormCategoryView.ProductFormCategoryCode,
		Program.Guid AS ProgramGuid,
		ProgramTypeView.ProgramTypeGuid,
		ProgramTypeView.DefaultPeakWeekNumber,
		ProgramTypeView.DefaultSeasonEndWeekNumber,
		SupplierView.SupplierGuid,
		ProductFormCategoryView.ProductFormCategoryGuid,
		Seller.Code as SellerCode
	FROM Program with (nolock)
		INNER JOIN ProgramTypeView with (nolock)
			ON Program.ProgramTypeGuid = ProgramTypeView.ProgramTypeGuid
		INNER JOIN SupplierView with (nolock)
			ON Program.SupplierGuid = SupplierView.SupplierGuid
		INNER JOIN ProductFormCategoryView with (nolock)
			ON Program.ProductFormCategoryGuid = ProductFormCategoryView.ProductFormCategoryGuid
		INNER JOIN Seller with (nolock)
			ON Program.SellerGuid = Seller.Guid
	WHERE
		Program.DateDeactivated IS NULL
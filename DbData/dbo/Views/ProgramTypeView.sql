﻿
CREATE VIEW [dbo].[ProgramTypeView]
AS
	SELECT
		Id AS ProgramTypeId,
		Guid AS ProgramTypeGuid,
		Code AS ProgramTypeCode,
		Name AS ProgramTypeName,
		DefaultPeakWeekNumber,
		DefaultSeasonEndWeekNumber
	FROM ProgramType
	WHERE DateDeactivated IS NULL
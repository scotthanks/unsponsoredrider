﻿
CREATE VIEW [dbo].[PriceGroupView]
AS
	SELECT
		PriceGroup.Id AS PriceGroupId,
		PriceGroup.Guid AS PriceGroupGuid,
		PriceGroup.Code AS PriceGroupCode,
		PriceGroup.Name AS PriceGroupName,
		PriceGroup.TagCost
	FROM PriceGroup with (nolock)
		WHERE PriceGroup.DateDeactivated IS NULL
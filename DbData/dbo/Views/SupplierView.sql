﻿
CREATE VIEW [dbo].[SupplierView]
AS
	SELECT
		Supplier.Id AS SupplierId,
		Supplier.Guid AS SupplierGuid,
		Supplier.Code AS SupplierCode,
		Supplier.Name AS SupplierName,
		AddressView.AddressGuid,
		AddressView.AddressId,
		AddressView.AddressName,
		AddressView.StreetAddress1,
		AddressView.StreetAddress2,
		AddressView.City,
		AddressView.StateGuid,
		AddressView.StateId,
		AddressView.StateCode,
		AddressView.CountryGuid,
		AddressView.CountryId,
		AddressView.CountryCode,
		AddressView.CountryName,
		AddressView.ZipCode,
		Seller.Guid as SellerGuid,
		Seller.Code as SellerCode
	FROM Supplier
		JOIN AddressView
			ON Supplier.AddressGuid = AddressView.AddressGuid
		JOIN Seller on Supplier.SellerGuid = Seller.Guid
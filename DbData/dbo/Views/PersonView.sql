﻿
CREATE view [dbo].[PersonView]
AS
	SELECT
		Id AS PersonId,
		Guid AS PersonGuid,
		FirstName,
		LastName,
		PersonTypeLookupView.LookupGuid as PersonTypeLookupGuid,
		PersonTypeLookupView.LookupCode as PersonTypeLookupCode,
		PersonTypeLookupView.LookupName as PersonTypeLookupName,
		UserGuid,
		UserId,
		UserCode,
		Email,
		PhoneAreaCode,
		Phone,
		GrowerGuid
	FROM Person
		INNER JOIN LookupView AS PersonTypeLookupView
			ON Person.PersonTypeLookupGuid = PersonTypeLookupView.LookupGuid
	WHERE DateDeactivated IS NULL
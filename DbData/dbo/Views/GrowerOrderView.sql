﻿


CREATE view [dbo].[GrowerOrderView]
AS
	SELECT
		GrowerOrder.Id AS GrowerOrderId,
		GrowerOrder.Guid AS GrowerOrderGuid,
		GrowerOrder.OrderNo,
		GrowerOrder.Description,
		GrowerOrder.CustomerPoNo,
		GrowerOrder.PromotionalCode,
		GrowerView.GrowerGuid,
		GrowerView.GrowerId,
		GrowerView.GrowerCode,
		GrowerView.GrowerName,
		ShipWeekView.ShipWeekGuid,
		ShipWeekView.ShipWeekId,
		ShipWeekView.ShipWeekCode,
		ProductFormCategoryView.ProductFormCategoryGuid,
		ProductFormCategoryView.ProductFormCategoryId,
		ProductFormCategoryView.ProductFormCategoryCode,
		ProductFormCategoryView.Name as ProductFormCategoryName,
		ProductFormCategoryView.IsRooted,
		ProgramTypeView.ProgramTypeGuid,
		ProgramTypeView.ProgramTypeId,
		ProgramTypeView.ProgramTypeCode,
		ProgramTypeView.ProgramTypeName,
		OrderTypeLookupView.LookupGuid AS OrderTypeLookupGuid,
		OrderTypeLookupView.LookupCode AS OrderTypeLookupCode,
		GrowerOrderStatusLookupView.LookupGuid AS GrowerOrderStatusLookupGuid,
		GrowerOrderStatusLookupView.LookupCode AS GrowerOrderStatusLookupCode,
		GrowerOrderStatusLookupView.LookupName AS GrowerOrderStatusLookupName,
		Person.Guid as PersonGuid,
		Person.UserGuid
	FROM GrowerOrder
		INNER JOIN GrowerView
			ON GrowerOrder.GrowerGuid = GrowerView.GrowerGuid
		INNER JOIN ShipWeekView
			ON GrowerOrder.ShipWeekGuid = ShipWeekView.ShipWeekGuid
		INNER JOIN ProductFormCategoryView
			ON GrowerOrder.ProductFormCategoryGuid = ProductFormCategoryView.ProductFormCategoryGuid
		INNER JOIN ProgramTypeView
			ON GrowerOrder.ProgramTypeGuid = ProgramTypeView.ProgramTypeGuid
		INNER JOIN LookupView AS OrderTypeLookupView
			ON GrowerOrder.OrderTypeLookupGuid = OrderTypeLookupView.LookupGuid
		INNER JOIN LookupView AS GrowerOrderStatusLookupView
			ON GrowerOrder.GrowerOrderStatusLookupGuid = GrowerOrderStatusLookupView.LookupGuid
		INNER JOIN Person
			ON GrowerOrder.PersonGuid = Person.Guid
	WHERE GrowerOrder.DateDeactivated IS NULL
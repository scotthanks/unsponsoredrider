﻿



CREATE VIEW [dbo].[VarietyView]
AS
	SELECT
		Variety.Guid AS VarietyGuid,
		Variety.Code AS VarietyCode,
		Variety.Name,
		Variety.ActiveDate,
		Variety.DeactiveDate,
		Variety.ColorDescription,
		Species.Guid AS SpeciesGuid,
		Species.Code AS SpeciesCode,
		Species.Name AS SpeciesName,
		GeneticOwner.Guid as GeneticOwnerGuid,
		GeneticOwner.Code as GeneticOwnerCode,
		Variety.ColorLookupGuid,
		ColorLookupView.LookupCode AS ColorLookupCode,
		Variety.HabitLookupGuid,
		HabitLookupView.LookupCode AS HabitLookupCode,
		Variety.VigorLookupGuid,
		VigorLookupView.LookupCode AS VigorLookupCode,
		ImageSetView.ImageSetGuid,
		ImageSetView.ThumbnailImageFileGuid,
		ImageSetView.ThumbnailImageFileFileExtensionLookupGuid
	FROM Variety with (nolock)
	INNER JOIN Species with (nolock) ON Variety.SpeciesGuid = Species.Guid
	INNER JOIN GeneticOwner with (nolock) ON Variety.GeneticOwnerGuid = GeneticOwner.Guid
	LEFT OUTER JOIN LookupView AS ColorLookupView with (nolock) ON Variety.ColorLookupGuid = ColorLookupView.LookupGuid
	LEFT OUTER JOIN LookupView AS HabitLookupView with (nolock)	ON Variety.HabitLookupGuid = HabitLookupView.LookupGuid
	LEFT OUTER JOIN LookupView AS VigorLookupView with (nolock) ON Variety.VigorLookupGuid = VigorLookupView.LookupGuid
	LEFT OUTER JOIN ImageSetView with (nolock) ON Variety.ImageSetGuid = ImageSetView.ImageSetGuid
	WHERE
		Variety.DateDeactivated IS NULL 
		AND	Species.DateDeactivated IS NULL 
		AND	GeneticOwner.DateDeactivated IS NULL
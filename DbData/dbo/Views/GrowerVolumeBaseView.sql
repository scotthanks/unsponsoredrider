﻿

CREATE VIEW [dbo].[GrowerVolumeBaseView]
AS
	SELECT
		GrowerVolume.Id AS GrowerVolumeId,
		GrowerVolume.Guid AS GrowerVolumeGuid,
		GrowerVolume.GrowerGuid,
		GrowerVolume.ProgramSeasonGuid,
		CASE
			WHEN GrowerVolume.QuotedVolumeLevelGuid IS NOT NULL THEN
				'QUOTED'
			WHEN GrowerVolume.ActualVolumeLevelGuid IS NOT NULL THEN
				'ACTUAL'
			WHEN GrowerVolume.EstimatedVolumeLevelGuid IS NOT NULL THEN
				'ESTIMATED'
			ELSE
				'NONE'
		END AS VolumeLevelType,
		CASE
			WHEN GrowerVolume.QuotedVolumeLevelGuid IS NOT NULL THEN
				GrowerVolume.QuotedVolumeLevelGuid
			WHEN GrowerVolume.ActualVolumeLevelGuid IS NOT NULL THEN
				GrowerVolume.ActualVolumeLevelGuid
			WHEN GrowerVolume.EstimatedVolumeLevelGuid IS NOT NULL THEN
				GrowerVolume.EstimatedVolumeLevelGuid
			ELSE
				NULL
		END AS VolumeLevelGuid
	FROM GrowerVolume
	WHERE DateDeactivated IS NULL
﻿CREATE view [dbo].[TripleProgramShipMethodLookupView]
AS
	SELECT
		PredicateLookup.Code AS PredicateLookupCode,
		ProgramView.ProgramCode AS ProgramCode,
		ShipMethodLookupView.LookupCode AS ShipMethodLookupCode,
		PredicateLookup.Name AS PredicateLookupName,
		ShipMethodLookupView.LookupName AS ShipMethodLookupName,
		TripleView.TripleId,
		TripleView.TripleGuid,
		TripleView.SubjectGuid AS ProgramGuid,
		TripleView.PredicateLookupGuid,
		TripleView.ObjectGuid AS LookupGuid,
		PredicateLookup.Id AS PredicateLookupId,
		ShipMethodLookupView.LookupId AS ShipMethodLookupId,
		ShipMethodLookupView.LookupGuid AS ShipMethodLookupGuid
	FROM TripleView
		INNER JOIN ProgramView
			ON TripleView.SubjectGuid = ProgramView.ProgramGuid
		INNER JOIN Lookup AS PredicateLookup
			ON TripleView.PredicateLookupGuid = PredicateLookup.Guid
		INNER JOIN LookupView AS ShipMethodLookupView
			ON
				TripleView.ObjectGuid = ShipMethodLookupView.LookupGuid AND
				ShipMethodLookupView.Path LIKE 'Code/ShipMethod/%'
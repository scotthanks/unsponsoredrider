﻿CREATE VIEW [dbo].[ProgramSeasonView]
AS
	SELECT
		ProgramSeason.Code AS ProgramSeasonCode,
		ProgramSeason.Name AS ProgramSeasonName,
		ProgramSeason.StartDate,
		ProgramSeason.EndDate,
		ProgramSeason.EODDate,
		ProgramView.ProgramCode,
		ProgramView.ProgramTypeCode,
		ProgramView.SupplierCode,
		ProgramView.SupplierName,
		ProgramView.ProductFormCategoryCode,
		ProgramSeason.Id AS ProgramSeasonId,
		ProgramSeason.Guid AS ProgramSeasonGuid,
		ProgramView.ProgramGuid,
		ProgramView.ProgramTypeGuid,
		ProgramView.DefaultPeakWeekNumber,
		ProgramView.DefaultSeasonEndWeekNumber,
		ProgramView.SupplierGuid,
		ProgramView.ProductFormCategoryGuid
	FROM ProgramSeason
		INNER JOIN ProgramView
			ON ProgramSeason.ProgramGuid = ProgramView.ProgramGuid
	WHERE
		ProgramSeason.DateDeactivated IS NULL
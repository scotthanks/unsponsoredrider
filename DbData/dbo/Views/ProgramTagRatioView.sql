﻿CREATE view [dbo].[ProgramTagRatioView]
AS
    SELECT
		ProgramView.ProgramGuid,
		TripleProgramTagRatioLookupView.TagRatioLookupGuid,
		CASE
			WHEN TripleProgramTagRatioDefaultLookupView.TagRatioLookupGuid IS NOT NULL THEN 
				CAST(1 AS BIT) 
			ELSE 
				CAST(0 AS BIT) 
		END AS IsDefault
	FROM ProgramView
        INNER JOIN TripleProgramTagRatioLookupView
                ON ProgramView.ProgramGuid=TripleProgramTagRatioLookupView.ProgramGuid
        LEFT OUTER JOIN TripleProgramTagRatioDefaultLookupView
                ON ProgramView.ProgramGuid=TripleProgramTagRatioDefaultLookupView.ProgramGuid AND
					TripleProgramTagRatioDefaultLookupView.LookupGuid = TripleProgramTagRatioLookupView.LookupGuid
	GROUP BY  
		ProgramView.ProgramGuid,
		TripleProgramTagRatioLookupView.TagRatioLookupGuid,
		TripleProgramTagRatioDefaultLookupView.TagRatioLookupGuid
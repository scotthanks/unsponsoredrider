﻿
CREATE view [dbo].[TripleProgramTagRatioDefaultLookupView]
AS
	SELECT
		PredicateLookup.Code AS PredicateLookupCode,
		ProgramView.ProgramCode AS ProgramCode,
		ProgramView.SupplierGuid AS SupplierGuid,
		ProgramView.ProgramTypeCode AS ProgramTypeCode,
		ProgramView.ProductFormCategoryCode AS ProductFormCategoryCode,
		TagRatioLookupView.LookupCode AS TagRatioLookupCode,
		PredicateLookup.Name AS PredicateLookupName,
		TagRatioLookupView.LookupName AS TagRatioLookupName,
		TripleView.TripleId,
		TripleView.TripleGuid,
		TripleView.SubjectGuid AS ProgramGuid,
		TripleView.PredicateLookupGuid,
		TripleView.ObjectGuid AS LookupGuid,
		PredicateLookup.Id AS PredicateLookupId,
		TagRatioLookupView.LookupId AS TagRatioLookupId,
		TagRatioLookupView.LookupGuid AS TagRatioLookupGuid
	FROM TripleView
		INNER JOIN ProgramView
			ON TripleView.SubjectGuid = ProgramView.ProgramGuid
		INNER JOIN Lookup AS PredicateLookup
			ON TripleView.PredicateLookupGuid = PredicateLookup.Guid
		INNER JOIN LookupView AS TagRatioLookupView
			ON
				TripleView.ObjectGuid = TagRatioLookupView.LookupGuid AND
				TagRatioLookupView.Path LIKE 'Code/TagRatio/%'
	WHERE PredicateLookup.Code = 'ProgramTagRatioDefault'
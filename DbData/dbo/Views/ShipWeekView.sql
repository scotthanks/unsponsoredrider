﻿




CREATE VIEW [dbo].[ShipWeekView]
AS
	SELECT
		ShipWeek.Id AS ShipWeekId,
		ShipWeek.Guid AS ShipWeekGuid,
		CAST (ShipWeek.Year * 100 + ShipWeek.Week AS NVARCHAR(6)) AS ShipWeekCode,
		Week as ShipWeekWeek,
		Year as ShipWeekYear,
		MondayDate as ShipWeekMondayDate,
		ContinuousWeekNumber as ShipWeekContinuousWeekNumber
	FROM ShipWeek with (nolock)
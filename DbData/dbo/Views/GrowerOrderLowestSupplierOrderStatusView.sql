﻿CREATE view [dbo].[GrowerOrderLowestSupplierOrderStatusView]
AS
	SELECT
		GrowerOrderView.GrowerOrderGuid,
		MIN(SupplierOrderStatusLookupView.LookupGuid) AS SupplierOrderStatusLookupGuid
	FROM GrowerOrderView
		INNER JOIN
			(
				SELECT
					GrowerOrderView.GrowerOrderGuid,
					MIN(SortSequence) AS MinSortSequence
				FROM GrowerOrderView
					INNER JOIN SupplierOrderView
						ON GrowerOrderView.GrowerOrderGuid = SupplierOrderView.GrowerOrderGuid
					INNER JOIN LookupView AS SupplierOrderStatusView
						ON SupplierOrderView.SupplierOrderStatusLookupGuid = SupplierOrderStatusView.LookupGuid
				GROUP BY
					GrowerOrderView.GrowerOrderGuid
			) GrowerOrderLowestSupplierOrderSequenceNumberView
				ON GrowerOrderView.GrowerOrderGuid = GrowerOrderLowestSupplierOrderSequenceNumberView.GrowerOrderGuid
		INNER JOIN LookupView SupplierOrderStatusLookupView
			ON
				GrowerOrderLowestSupplierOrderSequenceNumberView.MinSortSequence = SupplierOrderStatusLookupView.SortSequence AND
				SupplierOrderStatusLookupView.Path LIKE 'Code/SupplierOrderStatus/%'
	GROUP BY GrowerOrderView.GrowerOrderGuid
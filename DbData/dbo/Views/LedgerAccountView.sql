﻿
CREATE VIEW [dbo].[LedgerAccountView]
AS
select 
BillTotal, 
PayTotal, 
(BillTotal - PayTotal) as BalDue, 
GrowerOrderGuid, 
InvoiceNumber,
InvoiceDate  
from
(select 
sum(Amount) as BillTotal, 
ParentGuid as GrowerOrderGuid, 
InternalIdNumber as InvoiceNumber,
TransactionDate as InvoiceDate  
from Ledger 
where 
GLAccountLookupGuid = 'D7739B18-1906-47B2-9FD8-8FFAC63A8B9A' AND
EntryTypeLookupGuid = '07D9C62F-73D5-4905-91BC-662E0269A217'
group by 
ParentGuid,
GLAccountLookupGuid,
EntryTypeLookupGuid,
InternalIdNumber,
TransactionDate
)
Debits
left join
(select 
sum(Amount) as PayTotal, 
ParentGuid 
from Ledger 
where 
GLAccountLookupGuid = 'D7739B18-1906-47B2-9FD8-8FFAC63A8B9A' AND
EntryTypeLookupGuid = '9B7335B3-4D1E-4EA7-99F6-D3266D18D536'
group by 
ParentGuid,
GLAccountLookupGuid,
EntryTypeLookupGuid
)
Credits 
on Credits.ParentGuid = Debits.GrowerOrderGuid
﻿




CREATE view [dbo].[GrowerOrderSummaryByShipWeekGroupByOrderView]
AS
	SELECT
		OrderLineView.GrowerOrderGuid,
		max(OrderLineView.GrowerGuid) as GrowerGuid,
		max(OrderLineView.OrderTypeLookupGuid) as OrderTypeLookupGuid,
		max(OrderLineView.OrderTypeLookupCode) as OrderTypeLookupCode,
		max(OrderLineView.ProgramTypeGuid)  as ProgramTypeGuid,
		max(OrderLineView.ProgramTypeCode) as ProgramTypeCode,
		max(OrderLineView.ProductFormCategoryGuid) as ProductFormCategoryGuid,
		max(OrderLineView.ProductFormCategoryCode) as ProductFormCategoryCode,
		max(OrderLineView.ShipWeekGuid) as ShipWeekGuid,
		max(OrderLineView.ShipWeekCode) as ShipWeekCode,
		ISNULL(COUNT(DISTINCT OrderLineView.SupplierOrderGuid),0) AS SupplierOrderCount,
		ISNULL(COUNT(DISTINCT OrderLineView.SupplierOrderGuid),0) AS SupplierOrderWithOrderLinesCount,
		ISNULL(COUNT(DISTINCT OrderLineView.ProductGuid),0) AS ProductCount,
		ISNULL(COUNT(DISTINCT(CASE WHEN OrderLineView.QtyOrdered > 0 THEN ProductGuid ELSE NULL END)),0) AS ProductWithNonZeroOrderQtyCount,
		ISNULL(COUNT(DISTINCT OrderLineView.OrderLineGuid),0) AS OrderLineCount,
		SUM(CASE WHEN OrderLineView.QtyOrdered > 0 THEN 1 ELSE 0 END) AS OrderLineWithNonZeroOrderQtyCount,
		ISNULL(SUM(OrderLineView.QtyOrdered),0) AS QtyOrderedCount,
		max(OrderLineView.GrowerOrderStatusLookupGuid) as GrowerOrderStatusLookupGuid,
		max(OrderLineView.GrowerOrderStatusLookupCode) as GrowerOrderStatusLookupCode,
		max(GrowerOrderLowestSupplierOrderStatusLookupView.LookupGuid) as LowestSupplierOrderStatusLookupGuid,
		max(GrowerOrderLowestSupplierOrderStatusLookupView.LookupCode) as LowestSupplierOrderStatusLookupCode,
		max(GrowerOrderLowestOrderLineStatusLookupView.LookupGuid) as LowestOrderLineStatusLookupGuid,
		max(GrowerOrderLowestOrderLineStatusLookupView.LookupCode) as LowestOrderLineStatusLookupCode,
		max(OrderLineView.PersonGuid) as PersonGuid,
		CASE
			WHEN max(GrowerOrderLineStatusPendingCountView.GrowerOrderGuid) IS NULL THEN 
				0 
			ELSE 
				max(GrowerOrderLineStatusPendingCountView.CountOfPendingStatusLines) 
			END 
		AS CountOfPendingStatusLines,
		max(GrowerOrderLowestOrderLineStatusLookupView.LookupName) as LowestOrderLineStatusLookupName,
		max(OrderLineView.GrowerOrderStatusLookupName) as GrowerOrderStatusLookupName,
		ISNULL(max(LedgerAccountView.InvoiceNumber),'') as InvoiceNo
	FROM OrderLineViewShowInactiveProduct OrderLineView
		INNER JOIN GrowerOrderLowestOrderLineStatusView
			ON OrderLineView.GrowerOrderGuid = GrowerOrderLowestOrderLineStatusView.GrowerOrderGuid
		INNER JOIN LookupView GrowerOrderLowestOrderLineStatusLookupView 
			ON GrowerOrderLowestOrderLineStatusView.OrderLineStatusLookupGuid = GrowerOrderLowestOrderLineStatusLookupView.LookupGuid
		INNER JOIN GrowerOrderLowestSupplierOrderStatusView
			ON OrderLineView.GrowerOrderGuid = GrowerOrderLowestSupplierOrderStatusView.GrowerOrderGuid
		INNER JOIN LookupView GrowerOrderLowestSupplierOrderStatusLookupView 
			ON GrowerOrderLowestSupplierOrderStatusView.SupplierOrderStatusLookupGuid = GrowerOrderLowestSupplierOrderStatusLookupView.LookupGuid
		LEFT OUTER JOIN GrowerOrderLineStatusPendingCountView
			ON OrderLineView.GrowerOrderGuid = GrowerOrderLineStatusPendingCountView.GrowerOrderGuid
		LEFT OUTER JOIN	LedgerAccountView
			ON OrderLineView.GrowerOrderGuid = LedgerAccountView.GrowerOrderGuid
	WHERE OrderLineView.OrderLineStatusLookupCode != 'PreCart'
	--and OrderLineView.GrowerOrderNo = 'A0072850'
	GROUP BY
		OrderLineView.GrowerOrderGuid
﻿CREATE TABLE [dbo].[FreightProgramZoneBoxCost] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated] DATETIME         NULL,
    [ProgramGuid]     UNIQUEIDENTIFIER NOT NULL,
    [ZoneLookupGuid]  UNIQUEIDENTIFIER NOT NULL,
    [CostPerBox]      DECIMAL (8, 2)   NOT NULL,
    [SupplierBoxGuid] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK__FreightS__A2B5777C7C7D1C87] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_FreightProgramZoneBoxCost_Program] FOREIGN KEY ([ProgramGuid]) REFERENCES [dbo].[Program] ([Guid]),
    CONSTRAINT [FK_FreightProgramZoneBoxCost_SupplierBox] FOREIGN KEY ([SupplierBoxGuid]) REFERENCES [dbo].[SupplierBox] ([Guid]),
    CONSTRAINT [FK_FreightProgramZoneBoxCost_Zone] FOREIGN KEY ([ZoneLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [UC_Program_Zone_Box] UNIQUE NONCLUSTERED ([ProgramGuid] ASC, [ZoneLookupGuid] ASC, [SupplierBoxGuid] ASC)
);




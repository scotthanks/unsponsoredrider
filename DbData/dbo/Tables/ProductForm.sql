﻿CREATE TABLE [dbo].[ProductForm] (
    [Id]                       BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                     UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]          DATETIME         NULL,
    [Code]                     NVARCHAR (30)    NOT NULL,
    [Name]                     NVARCHAR (50)    NOT NULL,
    [ProductFormCategoryGuid]  UNIQUEIDENTIFIER NOT NULL,
    [SalesUnitQty]             INT              NOT NULL,
    [TagBundleQty]             INT              NOT NULL,
    [LineItemMinimumQty]       INT              NOT NULL,
    [SalesUnitsPerPackingUnit] INT              CONSTRAINT [DF__ProductFo__Sales__06CD04F7] DEFAULT ((0)) NOT NULL,
    [SupplierGuid]             UNIQUEIDENTIFIER NULL,
    [IncludesDelivery]         BIT              CONSTRAINT [DF__ProductFo__Inclu__15A53433] DEFAULT ((0)) NOT NULL,
    [TagBundlesPerSalesUnit]   INT              CONSTRAINT [DF__ProductFo__Bundl__5911273F] DEFAULT ((0)) NOT NULL,
    [SalesUnitsPerPallet]      INT              DEFAULT ((0)) NOT NULL,
    [SalesUnitsPerRack]        INT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__ProductF__A2B5777C13CAD41C] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ProductForm_ProductFormCategory] FOREIGN KEY ([ProductFormCategoryGuid]) REFERENCES [dbo].[ProductFormCategory] ([Guid]),
    CONSTRAINT [FK_ProductForm_SupplierGUID] FOREIGN KEY ([SupplierGuid]) REFERENCES [dbo].[Supplier] ([Guid]),
    CONSTRAINT [UC_ProductForm_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);












﻿CREATE TABLE [dbo].[GeneticOwner] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated] DATETIME         NULL,
    [Name]            NVARCHAR (50)    NOT NULL,
    [Code]            NVARCHAR (10)    NOT NULL,
    CONSTRAINT [PK__GeneticO__A2B5777CEC9148A5] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [UC_GeneticOwner_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);






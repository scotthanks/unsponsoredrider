﻿CREATE TABLE [dbo].[Lookup] (
    [Id]                 BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]               UNIQUEIDENTIFIER NOT NULL,
    [ParentLookupGuid]   UNIQUEIDENTIFIER NULL,
    [DateDeactivated]    DATETIME         NULL,
    [Code]               NVARCHAR (50)    NOT NULL,
    [Name]               NVARCHAR (100)   NOT NULL,
    [SortSequence]       FLOAT (53)       NOT NULL,
    [Path]               NVARCHAR (200)   NULL,
    [SequenceChildren]   BIT              NOT NULL,
    [CategoryLookupGuid] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Code] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_Lookup_Category] FOREIGN KEY ([CategoryLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_Lookup_Parent] FOREIGN KEY ([ParentLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [UC_Lookup_ParentLookupGuid] UNIQUE NONCLUSTERED ([ParentLookupGuid] ASC, [Guid] ASC)
);




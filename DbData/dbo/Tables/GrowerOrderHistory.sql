﻿CREATE TABLE [dbo].[GrowerOrderHistory] (
    [Id]                BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]              UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]   DATETIME         NULL,
    [GrowerOrderGuid]   UNIQUEIDENTIFIER NOT NULL,
    [Comment]           NVARCHAR (2000)  NULL,
    [IsInternal]        BIT              CONSTRAINT [DF__GrowerOrd__IsInt__4EC8A2F6] DEFAULT ((0)) NOT NULL,
    [PersonGuid]        UNIQUEIDENTIFIER NOT NULL,
    [EventDate]         DATETIME         NOT NULL,
    [LogTypeLookupGuid] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK__GrowerOr__A2B5777C76687BC4] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_GrowerOrderHistory_GrowerOrder] FOREIGN KEY ([GrowerOrderGuid]) REFERENCES [dbo].[GrowerOrder] ([Guid]),
    CONSTRAINT [FK_GrowerOrderHistory_LogTypeLookup] FOREIGN KEY ([LogTypeLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_GrowerOrderHistory_Person] FOREIGN KEY ([PersonGuid]) REFERENCES [dbo].[Person] ([Guid])
);


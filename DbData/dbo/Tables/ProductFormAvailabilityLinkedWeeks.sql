﻿CREATE TABLE [dbo].[ProductFormAvailabilityLinkedWeeks] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated] DATETIME         NULL,
    [GroupingCode]    INT              NOT NULL,
    [ProductFormGuid] UNIQUEIDENTIFIER NOT NULL,
    [ShipWeekGuid]    UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK__ProductF__A2B5777CA6F00608] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ProductFormAvailabilityLinkedWeeks_ProductForm] FOREIGN KEY ([ProductFormGuid]) REFERENCES [dbo].[ProductForm] ([Guid]),
    CONSTRAINT [FK_ProductFormAvailabilityLinkedWeeks_ShipWeek] FOREIGN KEY ([ShipWeekGuid]) REFERENCES [dbo].[ShipWeek] ([Guid])
);




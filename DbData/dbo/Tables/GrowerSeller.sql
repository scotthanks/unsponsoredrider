﻿CREATE TABLE [dbo].[GrowerSeller] (
    [Id]         BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]       UNIQUEIDENTIFIER NOT NULL,
    [GrowerGuid] UNIQUEIDENTIFIER NOT NULL,
    [SellerGuid] UNIQUEIDENTIFIER NOT NULL,
    [IsDefault]  BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__GrowerSeller__Guid] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_GrowerSeller_Grower] FOREIGN KEY ([GrowerGuid]) REFERENCES [dbo].[Grower] ([Guid]),
    CONSTRAINT [FK_GrowerSeller_Seller] FOREIGN KEY ([SellerGuid]) REFERENCES [dbo].[Seller] ([Guid]),
    CONSTRAINT [uc_GrowerSeller] UNIQUE NONCLUSTERED ([GrowerGuid] ASC, [SellerGuid] ASC)
);




﻿CREATE TABLE [dbo].[Address] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated] DATETIME         NULL,
    [Name]            NVARCHAR (50)    NOT NULL,
    [StreetAddress1]  NVARCHAR (50)    NOT NULL,
    [StreetAddress2]  NVARCHAR (50)    NOT NULL,
    [City]            NVARCHAR (50)    NOT NULL,
    [StateGuid]       UNIQUEIDENTIFIER NOT NULL,
    [ZipCode]         NVARCHAR (12)    NOT NULL,
    CONSTRAINT [PK__Address__A2B5777C09F0EB35] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_Address_State] FOREIGN KEY ([StateGuid]) REFERENCES [dbo].[State] ([Guid])
);






﻿CREATE TABLE [dbo].[Price] (
    [Id]               BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]             UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]  DATETIME         NULL,
    [CountryGuid]      UNIQUEIDENTIFIER NOT NULL,
    [PriceGroupGuid]   UNIQUEIDENTIFIER NOT NULL,
    [VolumeLevelGuid]  UNIQUEIDENTIFIER NOT NULL,
    [RegularCost]      DECIMAL (8, 4)   NOT NULL,
    [EODCost]          DECIMAL (8, 4)   NOT NULL,
    [RegularMUPercent] FLOAT (53)       NOT NULL,
    [EODMUPercent]     FLOAT (53)       CONSTRAINT [DF__Price__EODMUPerc__68487DD7] DEFAULT ((0)) NOT NULL,
    [CuttingCost]      DECIMAL (8, 4)   DEFAULT ((0)) NOT NULL,
    [RoyaltyCost]      DECIMAL (8, 4)   DEFAULT ((0)) NOT NULL,
    [FreightCost]      DECIMAL (8, 4)   DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__Price__A2B5777CC087A8B9] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_Price_Country] FOREIGN KEY ([CountryGuid]) REFERENCES [dbo].[Country] ([Guid]),
    CONSTRAINT [FK_Price_PriceGroup] FOREIGN KEY ([PriceGroupGuid]) REFERENCES [dbo].[PriceGroup] ([Guid]),
    CONSTRAINT [FK_Price_VolumeLevel] FOREIGN KEY ([VolumeLevelGuid]) REFERENCES [dbo].[VolumeLevel] ([Guid]),
    CONSTRAINT [UC_Price_Guid] UNIQUE NONCLUSTERED ([Guid] ASC),
    CONSTRAINT [UC_PriceGroupVolumeLevel] UNIQUE NONCLUSTERED ([PriceGroupGuid] ASC, [VolumeLevelGuid] ASC)
);










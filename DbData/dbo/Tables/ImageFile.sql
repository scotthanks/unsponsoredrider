﻿CREATE TABLE [dbo].[ImageFile] (
    [Guid]                    UNIQUEIDENTIFIER NOT NULL,
    [Id]                      BIGINT           IDENTITY (1, 1) NOT NULL,
    [Code]                    NVARCHAR (50)    NOT NULL,
    [Name]                    NVARCHAR (100)   NOT NULL,
    [FileExtensionLookupGuid] UNIQUEIDENTIFIER NOT NULL,
    [ImageFileTypeLookupGuid] UNIQUEIDENTIFIER NOT NULL,
    [Height]                  INT              NOT NULL,
    [Width]                   INT              NOT NULL,
    [Size]                    BIGINT           NOT NULL,
    [Notes]                   NVARCHAR (500)   NOT NULL,
    [ImageSetGuid]            UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]         DATETIME         NULL,
    CONSTRAINT [PK__ImageFil__A2B5777C00A55F7E] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ImageFile_FileExtensionLookup] FOREIGN KEY ([FileExtensionLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_ImageFile_ImageFileTypeLookup] FOREIGN KEY ([ImageFileTypeLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_ImageFile_ImageSet] FOREIGN KEY ([ImageSetGuid]) REFERENCES [dbo].[ImageSet] ([Guid]),
    CONSTRAINT [UC_ImageFile_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);






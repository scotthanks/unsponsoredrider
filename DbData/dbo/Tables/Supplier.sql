﻿CREATE TABLE [dbo].[Supplier] (
    [Id]                      BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                    UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]         DATETIME         NULL,
    [AddressGuid]             UNIQUEIDENTIFIER NOT NULL,
    [Name]                    NVARCHAR (50)    NOT NULL,
    [Code]                    NVARCHAR (10)    NOT NULL,
    [Email]                   NVARCHAR (70)    CONSTRAINT [DF__Supplier__EMail__2C3393D0] DEFAULT ('') NOT NULL,
    [PhoneAreaCode]           DECIMAL (3)      CONSTRAINT [DF__Supplier__PhoneA__2D27B809] DEFAULT ((0)) NOT NULL,
    [Phone]                   DECIMAL (7)      CONSTRAINT [DF__Supplier__Phone__2E1BDC42] DEFAULT ((0)) NOT NULL,
    [AvailabilityInSalesUnit] BIT              DEFAULT ((0)) NOT NULL,
    [FreightCalcStandard]     BIT              DEFAULT ((0)) NOT NULL,
    [TagsCalcStandard]        BIT              DEFAULT ((0)) NOT NULL,
    [BelowMinCharge]          DECIMAL (14, 2)  DEFAULT ((0)) NOT NULL,
    [BelowMinLevel]           INT              DEFAULT ((0)) NOT NULL,
    [BelowMinLevelTypeGuid]   UNIQUEIDENTIFIER NULL,
    [FreightCalcFromPrice]    BIT              DEFAULT ((0)) NOT NULL,
    [IsB2BOrdering]           BIT              DEFAULT ((0)) NOT NULL,
    [SellerGuid]              UNIQUEIDENTIFIER DEFAULT ('CDF5C0F2-D183-4AB4-9D99-59C97E7424C8') NOT NULL,
    [DefaultB2BLocationCode]  NVARCHAR (20)    DEFAULT ('') NOT NULL,
    CONSTRAINT [PK__Supplier__A2B5777C5AB374BD] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_Supplier_Address] FOREIGN KEY ([AddressGuid]) REFERENCES [dbo].[Address] ([Guid]),
    CONSTRAINT [UC_Supplier_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);










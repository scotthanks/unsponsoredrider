﻿CREATE TABLE [dbo].[ReportedAvailabilityUploadJobMessage] (
    [Id]                                BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                              UNIQUEIDENTIFIER NOT NULL,
    [ReportedAvailabilityUploadJobGuid] UNIQUEIDENTIFIER NOT NULL,
    [MessageTime]                       DATETIME         NOT NULL,
    [MessageType]                       NVARCHAR (20)    NOT NULL,
    [MessageState]                      NVARCHAR (20)    NOT NULL,
    [Message]                           NVARCHAR (200)   NOT NULL,
    CONSTRAINT [PK__ReportedAvailabilityUploadMessage] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ReportedAvailabilityUploadMessage_ReportedAvailabilityUploadJob] FOREIGN KEY ([ReportedAvailabilityUploadJobGuid]) REFERENCES [dbo].[ReportedAvailabilityUploadJob] ([Guid])
);


﻿CREATE TABLE [dbo].[ClaimOrderLine] (
    [Id]                    BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                  UNIQUEIDENTIFIER NOT NULL,
    [ClaimGuid]             UNIQUEIDENTIFIER NOT NULL,
    [ClaimReasonLookupGUID] UNIQUEIDENTIFIER NOT NULL,
    [Comment]               NVARCHAR (1000)  NOT NULL,
    [QtyClaimed]            INT              NOT NULL,
    [QtyApproved]           INT              CONSTRAINT [DF__ClaimOrde__QtyAp__05A3D694] DEFAULT ((0)) NOT NULL,
    [ClaimStatusLookupGUID] UNIQUEIDENTIFIER NULL,
    [OrderLineGuid]         UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK__ClaimOrd__A2B5777CC15B3329] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ClaimOrderLine_Claim] FOREIGN KEY ([ClaimGuid]) REFERENCES [dbo].[Claim] ([Guid]),
    CONSTRAINT [FK_ClaimOrderLine_ClaimReasonLookup] FOREIGN KEY ([ClaimReasonLookupGUID]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_ClaimOrderLine_ClaimStatusLookup] FOREIGN KEY ([ClaimStatusLookupGUID]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_ClaimOrderLine_OrderLine] FOREIGN KEY ([OrderLineGuid]) REFERENCES [dbo].[OrderLine] ([Guid])
);






﻿CREATE TABLE [dbo].[ProgramType] (
    [Id]                         BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                       UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]            DATETIME         NULL,
    [Code]                       NVARCHAR (10)    NOT NULL,
    [Name]                       NVARCHAR (50)    NOT NULL,
    [TagMultiple]                INT              NOT NULL,
    [ImageSetGuid]               UNIQUEIDENTIFIER NULL,
    [DefaultPeakWeekNumber]      INT              CONSTRAINT [DF__ProgramTy__Defau__34C8D9D1] DEFAULT ((0)) NULL,
    [DefaultSeasonEndWeekNumber] INT              CONSTRAINT [DF__ProgramTy__Defau__35BCFE0A] DEFAULT ((0)) NULL,
    CONSTRAINT [PK__ProgramT__A2B5777CA5382373] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ProgramType_ImageSet] FOREIGN KEY ([ImageSetGuid]) REFERENCES [dbo].[ImageSet] ([Guid]),
    CONSTRAINT [UC_ProgramType_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);






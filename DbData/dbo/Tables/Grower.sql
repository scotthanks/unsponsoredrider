﻿CREATE TABLE [dbo].[Grower] (
    [Id]                                     BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                                   UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]                        DATETIME         NULL,
    [Name]                                   NVARCHAR (50)    NOT NULL,
    [Code]                                   NVARCHAR (20)    NOT NULL,
    [IsActivated]                            BIT              NOT NULL,
    [AddressGuid]                            UNIQUEIDENTIFIER NOT NULL,
    [AllowSubstitutions]                     BIT              CONSTRAINT [DF__Grower__AllowSub__4BAC3F29] DEFAULT ((1)) NOT NULL,
    [CreditLimit]                            INT              CONSTRAINT [DF__Grower__CreditLi__4CA06362] DEFAULT ((0)) NOT NULL,
    [CreditLimitTemporaryIncrease]           INT              CONSTRAINT [DF__Grower__CreditLi__4D94879B] DEFAULT ((0)) NOT NULL,
    [CreditLimitTemporaryIncreaseExpiration] DATETIME         NULL,
    [Email]                                  NVARCHAR (70)    CONSTRAINT [DF__Grower__EMail__4E88ABD4] DEFAULT ('') NOT NULL,
    [PhoneAreaCode]                          DECIMAL (3)      CONSTRAINT [DF__Grower__PhoneAre__4F7CD00D] DEFAULT ((0)) NOT NULL,
    [Phone]                                  DECIMAL (7)      CONSTRAINT [DF__Grower__Phone__5070F446] DEFAULT ((0)) NOT NULL,
    [GrowerTypeLookupGuid]                   UNIQUEIDENTIFIER CONSTRAINT [DF__Grower__GrowerTy__5165187F] DEFAULT ('307936FC-C0F6-4D61-8CA5-C8874011A5D9') NOT NULL,
    [DefaultPaymentTypeLookupGuid]           UNIQUEIDENTIFIER CONSTRAINT [DF__Grower__DefaultP__52593CB8] DEFAULT ('E1E73B39-E4F0-488D-AC93-87AFE75025A7') NOT NULL,
    [SalesTaxNumber]                         NVARCHAR (20)    CONSTRAINT [DF__Grower__SalesTax__534D60F1] DEFAULT ('') NOT NULL,
    [CreditAppOnFile]                        BIT              CONSTRAINT [DF__Grower__CreditAp__77DFC722] DEFAULT ((0)) NOT NULL,
    [RequestedLineOfCredit]                  INT              CONSTRAINT [DF__Grower__Requeste__78D3EB5B] DEFAULT ((0)) NOT NULL,
    [RequestedLineOfCreditStatusLookupGuid]  UNIQUEIDENTIFIER NULL,
    [GrowerSizeLookupGuid]                   UNIQUEIDENTIFIER CONSTRAINT [DF__Grower__GrowerSi__1FA39FB9] DEFAULT ('7E09EBF5-BB19-47D9-8CEB-86F459ACE9CD') NOT NULL,
    [AdditionalAccountingEmail]              NVARCHAR (500)   DEFAULT ('') NOT NULL,
    [AdditionalOrderEmail]                   NVARCHAR (500)   DEFAULT ('') NOT NULL,
    [SellerGuid]                             UNIQUEIDENTIFIER DEFAULT ('CDF5C0F2-D183-4AB4-9D99-59C97E7424C8') NOT NULL,
    [OnlySpeciesIncluded]                    BIT              DEFAULT ((0)) NOT NULL,
    [UseDummenPriceMethod]                   BIT              DEFAULT ((0)) NOT NULL,
    [B2BOrderURL]                            NVARCHAR (200)   DEFAULT ('') NOT NULL,
    [B2BAvailabilityURL]                     NVARCHAR (200)   DEFAULT ('') NOT NULL,
    [IsInternal]                             BIT              DEFAULT ((0)) NOT NULL,
    [IsBadGrower]                            BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__Grower__A2B5777C20721771] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_Grower_Address] FOREIGN KEY ([AddressGuid]) REFERENCES [dbo].[Address] ([Guid]),
    CONSTRAINT [FK_Grower_RequestedLineStatusLookupGuid] FOREIGN KEY ([RequestedLineOfCreditStatusLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_Grower_TypeLookupGuid] FOREIGN KEY ([GrowerTypeLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [UC_Grower_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);
















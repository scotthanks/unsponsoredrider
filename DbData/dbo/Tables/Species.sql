﻿CREATE TABLE [dbo].[Species] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated] DATETIME         NULL,
    [Name]            NVARCHAR (50)    NOT NULL,
    [Code]            NVARCHAR (30)    NOT NULL,
    CONSTRAINT [PK__Species__A2B5777C1539BD63] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [UC_Species_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);






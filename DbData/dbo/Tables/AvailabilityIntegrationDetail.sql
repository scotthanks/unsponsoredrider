﻿CREATE TABLE [dbo].[AvailabilityIntegrationDetail] (
    [Id]                        BIGINT           IDENTITY (1, 1) NOT NULL,
    [AvailabilityIntegrationID] INT              NOT NULL,
    [DateReported]              DATETIME         NOT NULL,
    [SupplierIdentifier]        NVARCHAR (50)    NOT NULL,
    [ProductGuid]               UNIQUEIDENTIFIER NULL,
    [MondayDate]                DATETIME         NOT NULL,
    [ShipWeekGuid]              UNIQUEIDENTIFIER NULL,
    [Qty]                       INT              NOT NULL,
    CONSTRAINT [PK__AvailabilityIntegrationDetail] PRIMARY KEY CLUSTERED ([Id] ASC)
);




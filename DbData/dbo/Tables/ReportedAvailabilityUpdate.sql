﻿CREATE TABLE [dbo].[ReportedAvailabilityUpdate] (
    [Id]                         BIGINT           IDENTITY (1, 1) NOT NULL,
    [DateReported]               DATETIME         NOT NULL,
    [SupplierCode]               NVARCHAR (10)    NOT NULL,
    [SupplierGuid]               UNIQUEIDENTIFIER NULL,
    [SupplierIdentifier]         NVARCHAR (50)    NOT NULL,
    [SupplierDescription]        NVARCHAR (100)   NOT NULL,
    [ProductGuid]                UNIQUEIDENTIFIER NULL,
    [Year]                       INT              NOT NULL,
    [Week]                       INT              NOT NULL,
    [ShipWeekGuid]               UNIQUEIDENTIFIER NULL,
    [Qty]                        INT              NOT NULL,
    [AvailabiltiyType]           NVARCHAR (10)    NOT NULL,
    [AvailabilityTypeLookupGuid] UNIQUEIDENTIFIER NULL,
    [RunNumber]                  INT              NOT NULL,
    [AvailabilityUpdated]        BIT              NOT NULL,
    [MultiplyByUnit]             BIT              CONSTRAINT [DF__ReportedA__Multi__1975C517] DEFAULT ((0)) NOT NULL,
    [SellingUnit]                INT              NULL,
    CONSTRAINT [PK__Reported__3214EC0756D705F9] PRIMARY KEY CLUSTERED ([Id] ASC)
);






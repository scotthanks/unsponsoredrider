﻿CREATE TABLE [dbo].[Country] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated] DATETIME         NULL,
    [Code]            NVARCHAR (10)    NOT NULL,
    [Name]            NVARCHAR (50)    NOT NULL,
    CONSTRAINT [PK__Country__A2B5777C95C52057] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [UC_Country_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);






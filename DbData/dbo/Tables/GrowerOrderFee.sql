﻿CREATE TABLE [dbo].[GrowerOrderFee] (
    [Id]                 BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]               UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]    DATETIME         NULL,
    [GrowerOrderGuid]    UNIQUEIDENTIFIER NOT NULL,
    [FeeTypeLookupGuid]  UNIQUEIDENTIFIER NOT NULL,
    [Amount]             DECIMAL (8, 2)   NOT NULL,
    [FeeStatXLookupGuid] UNIQUEIDENTIFIER NULL,
    [FeeDescription]     NVARCHAR (500)   CONSTRAINT [DF__GrowerOrd__FeeDe__3BEAD8AC] DEFAULT ('') NOT NULL,
    [IsManual]           BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__GrowerOr__A2B5777CCB8CE1C8] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_GrowerOrderFee_FeeStatXLookupGuid] FOREIGN KEY ([FeeStatXLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_GrowerOrderFee_FeeTypeLookupGuid] FOREIGN KEY ([FeeTypeLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_GrowerOrderFee_GrowerOrder] FOREIGN KEY ([GrowerOrderGuid]) REFERENCES [dbo].[GrowerOrder] ([Guid])
);
















﻿CREATE TABLE [dbo].[GrowerCreditCard] (
    [Id]                       BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                     UNIQUEIDENTIFIER NOT NULL,
    [GrowerGuid]               UNIQUEIDENTIFIER NOT NULL,
    [AuthorizeNetSerialNumber] NVARCHAR (100)   NULL,
    [CardDescription]          NVARCHAR (100)   NULL,
    [DateDeactivated]          DATETIME         NULL,
    [DefaultCard]              BIT              CONSTRAINT [DF__GrowerCre__Defau__55009F39] DEFAULT ((0)) NOT NULL,
    [LastFourDigits]           NVARCHAR (8)     NULL,
    [CardType]                 NVARCHAR (20)    NULL,
    CONSTRAINT [PK__GrowerCr__A2B5777CC57410A8] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_GrowerCreditCard_Grower] FOREIGN KEY ([GrowerGuid]) REFERENCES [dbo].[Grower] ([Guid])
);




﻿CREATE TABLE [dbo].[DummenPricingUploadJobMessage] (
    [Id]                         BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                       UNIQUEIDENTIFIER NOT NULL,
    [DummenPricingUploadJobGuid] UNIQUEIDENTIFIER NOT NULL,
    [MessageTime]                DATETIME         NOT NULL,
    [MessageType]                NVARCHAR (20)    NOT NULL,
    [MessageState]               NVARCHAR (20)    NOT NULL,
    [Message]                    NVARCHAR (600)   NOT NULL,
    CONSTRAINT [PK__DummenPricingUploadMessage] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_DummenPricingUploadMessage_DummenPricingUploadJob] FOREIGN KEY ([DummenPricingUploadJobGuid]) REFERENCES [dbo].[DummenPricingUploadJob] ([Guid])
);


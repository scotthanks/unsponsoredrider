﻿CREATE TABLE [dbo].[VolumeLevel] (
    [Id]                        BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                      UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]           DATETIME         NULL,
    [Name]                      NVARCHAR (50)    NOT NULL,
    [Code]                      NVARCHAR (20)    NOT NULL,
    [VolumeLow]                 INT              NOT NULL,
    [VolumeHigh]                INT              NOT NULL,
    [ProgramSeasonGuid]         UNIQUEIDENTIFIER NOT NULL,
    [LevelNumber]               INT              CONSTRAINT [DF__VolumeLev__Level__5C37ACAD] DEFAULT ((0)) NOT NULL,
    [VolumeLevelTypeLookupGuid] UNIQUEIDENTIFIER CONSTRAINT [DF__VolumeLev__Volum__2D32A501] DEFAULT ('F4A6A9CE-2975-44E9-BE23-4ADF0082A2B9') NOT NULL,
    CONSTRAINT [PK__VolumeLe__A2B5777C3FD8E333] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_VolumeLevel_ProgramSeason] FOREIGN KEY ([ProgramSeasonGuid]) REFERENCES [dbo].[ProgramSeason] ([Guid]),
    CONSTRAINT [UC_VolumeLevel_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);










﻿CREATE TABLE [dbo].[Challenge] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated] DATETIME         NULL,
    [Code]            NVARCHAR (10)    NOT NULL,
    [Name]            NVARCHAR (50)    NOT NULL,
    [Points]          INT              NOT NULL,
    CONSTRAINT [PK__Challeng__A2B5777CE5C83989] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [UC_Challenge_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);




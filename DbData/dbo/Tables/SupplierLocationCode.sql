﻿CREATE TABLE [dbo].[SupplierLocationCode] (
    [Id]           BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]         UNIQUEIDENTIFIER NOT NULL,
    [SupplierGuid] UNIQUEIDENTIFIER NOT NULL,
    [LocationCode] NVARCHAR (30)    NOT NULL,
    CONSTRAINT [PK__SupplierLocationCode] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_SupplierLocationCode_Supplier] FOREIGN KEY ([SupplierGuid]) REFERENCES [dbo].[Supplier] ([Guid]),
    CONSTRAINT [UC_SupplierGuid_LocationCode] UNIQUE NONCLUSTERED ([SupplierGuid] ASC, [LocationCode] ASC)
);


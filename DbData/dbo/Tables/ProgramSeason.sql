﻿CREATE TABLE [dbo].[ProgramSeason] (
    [Id]                    BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                  UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]       DATETIME         NULL,
    [ProgramGuid]           UNIQUEIDENTIFIER NOT NULL,
    [StartDate]             DATETIME         NOT NULL,
    [EndDate]               DATETIME         NOT NULL,
    [EODDate]               DATETIME         NOT NULL,
    [Code]                  NVARCHAR (20)    NOT NULL,
    [Name]                  NVARCHAR (50)    NOT NULL,
    [RollingWeeksEOD]       INT              CONSTRAINT [DF__ProgramSe__Rolli__4222D4EF] DEFAULT ((0)) NOT NULL,
    [OpenWeeksDesc]         NVARCHAR (30)    CONSTRAINT [DF__ProgramSe__OpenW__79C80F94] DEFAULT ('') NOT NULL,
    [NAWeeksDesc]           NVARCHAR (30)    CONSTRAINT [DF__ProgramSe__NAWee__7ABC33CD] DEFAULT ('') NOT NULL,
    [SpecialRulesByForm]    NVARCHAR (500)   CONSTRAINT [DF__ProgramSe__Speci__7BB05806] DEFAULT ('') NOT NULL,
    [SpecialRulesBySpecies] NVARCHAR (500)   CONSTRAINT [DF__ProgramSe__Speci__7CA47C3F] DEFAULT ('') NOT NULL,
    [SpecialRulesByProduct] NVARCHAR (MAX)   NOT NULL,
    [RunAvailabilityUpdate] BIT              CONSTRAINT [DF__ProgramSe__RunAv__0169315C] DEFAULT ((0)) NOT NULL,
    [InternalStatus]        NVARCHAR (20)    CONSTRAINT [DF__ProgramSe__Inter__30242045] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK__ProgramS__A2B5777C6B73FDFC] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ProgramSeason_Program] FOREIGN KEY ([ProgramGuid]) REFERENCES [dbo].[Program] ([Guid]),
    CONSTRAINT [UC_ProgramSeason_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);










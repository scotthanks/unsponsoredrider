﻿CREATE TABLE [dbo].[EventLog] (
    [Id]                   BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                 UNIQUEIDENTIFIER NOT NULL,
    [LogTypeLookupGuid]    UNIQUEIDENTIFIER NOT NULL,
    [ObjectTypeLookupGuid] UNIQUEIDENTIFIER NOT NULL,
    [ObjectGuid]           UNIQUEIDENTIFIER NULL,
    [ProcessLookupGuid]    UNIQUEIDENTIFIER NOT NULL,
    [ProcessId]            BIGINT           NOT NULL,
    [PersonGuid]           UNIQUEIDENTIFIER NULL,
    [EventTime]            DATETIME         NOT NULL,
    [SyncTime]             DATETIME         NULL,
    [IntValue]             BIGINT           NULL,
    [FloatValue]           FLOAT (53)       NULL,
    [Comment]              NVARCHAR (2000)  NULL,
    [XmlData]              XML              NULL,
    [GuidValue]            UNIQUEIDENTIFIER NULL,
    [UserCode]             NVARCHAR (56)    NULL,
    [VerbosityLookupGuid]  UNIQUEIDENTIFIER NULL,
    [PriorityLookupGuid]   UNIQUEIDENTIFIER NULL,
    [SeverityLookupGuid]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK__EventLog__A2B5777CB0122E33] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_EventLog_LogTypeLookup] FOREIGN KEY ([LogTypeLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_EventLog_ObjectTypeLookup] FOREIGN KEY ([ObjectTypeLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_EventLog_PriorityLookup] FOREIGN KEY ([PriorityLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_EventLog_ProcessLookup] FOREIGN KEY ([ProcessLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_EventLog_SeverityLookup] FOREIGN KEY ([SeverityLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_EventLog_VerbosityLookup] FOREIGN KEY ([VerbosityLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [UC_EventLog_Guid] UNIQUE NONCLUSTERED ([Guid] ASC)
);








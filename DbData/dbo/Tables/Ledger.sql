﻿CREATE TABLE [dbo].[Ledger] (
    [Id]                  BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                UNIQUEIDENTIFIER NOT NULL,
    [ParentGuid]          UNIQUEIDENTIFIER NULL,
    [Amount]              DECIMAL (8, 2)   NOT NULL,
    [TransactionDate]     DATETIME         NOT NULL,
    [GrowerGuid]          UNIQUEIDENTIFIER NULL,
    [SupplierGuid]        UNIQUEIDENTIFIER NULL,
    [EntryTypeLookupGuid] UNIQUEIDENTIFIER NULL,
    [GLAccountLookupGuid] UNIQUEIDENTIFIER NULL,
    [TransactionGuid]     UNIQUEIDENTIFIER NULL,
    [InternalIdNumber]    NVARCHAR (50)    NULL,
    [ExternalIdNumber]    NVARCHAR (50)    NULL,
    CONSTRAINT [PK__Ledger__A2B5777C00ACE7F4] PRIMARY KEY CLUSTERED ([Guid] ASC)
);








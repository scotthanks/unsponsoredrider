﻿CREATE TABLE [dbo].[SearchTermSearchResult] (
    [Id]                BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]              UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]   DATETIME         NULL,
    [SearchTermGuid]    UNIQUEIDENTIFIER NOT NULL,
    [SearchResultGuid]  UNIQUEIDENTIFIER NOT NULL,
    [SortOrder]         INT              NULL,
    [RelevanceCategory] INT              NULL,
    CONSTRAINT [PK__SearchTe__A2B5777C1EB5EFA7] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [UC_SearchTermSearchResult] UNIQUE NONCLUSTERED ([SearchTermGuid] ASC, [SearchResultGuid] ASC)
);


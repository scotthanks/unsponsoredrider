﻿CREATE TABLE [dbo].[State] (
    [Id]          BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]        UNIQUEIDENTIFIER NOT NULL,
    [Code]        NVARCHAR (2)     NOT NULL,
    [Name]        NVARCHAR (30)    NOT NULL,
    [CountryGuid] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK__State__A2B5777C93A94FB7] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_State_Country] FOREIGN KEY ([CountryGuid]) REFERENCES [dbo].[Country] ([Guid]),
    CONSTRAINT [UC_State_CodeCountryGuid] UNIQUE NONCLUSTERED ([Code] ASC, [CountryGuid] ASC)
);






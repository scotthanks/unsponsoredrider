﻿CREATE TABLE [dbo].[Product] (
    [Guid]                    UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]         DATETIME         NULL,
    [ProgramGuid]             UNIQUEIDENTIFIER NOT NULL,
    [VarietyGuid]             UNIQUEIDENTIFIER NOT NULL,
    [Code]                    NVARCHAR (50)    NOT NULL,
    [SupplierIdentifier]      NVARCHAR (50)    NOT NULL,
    [ProductFormGuid]         UNIQUEIDENTIFIER NOT NULL,
    [Id]                      BIGINT           IDENTITY (220001, 1) NOT NULL,
    [SupplierDescription]     NVARCHAR (100)   NOT NULL,
    [ProductDescription]      NVARCHAR (100)   NOT NULL,
    [PriceGroupGuid]          UNIQUEIDENTIFIER NOT NULL,
    [ProductionLeadTimeWeeks] INT              CONSTRAINT [DF__Product__Product__0C85DE4D] DEFAULT ((0)) NOT NULL,
    [IsOrganic]               BIT              CONSTRAINT [DF__Product__IsOrgan__14B10FFA] DEFAULT ((0)) NOT NULL,
    [LastUpdate]              DATETIME         CONSTRAINT [DF__Product__LastUpd__218BE82B] DEFAULT ('9/1/2014') NOT NULL,
    [IsExclusive]             BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__Product__A2B5777CD0B060F9] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_Product_PriceGroup] FOREIGN KEY ([PriceGroupGuid]) REFERENCES [dbo].[PriceGroup] ([Guid]),
    CONSTRAINT [FK_Product_ProductForm] FOREIGN KEY ([ProductFormGuid]) REFERENCES [dbo].[ProductForm] ([Guid]),
    CONSTRAINT [FK_Product_Program] FOREIGN KEY ([ProgramGuid]) REFERENCES [dbo].[Program] ([Guid]),
    CONSTRAINT [FK_Product_Variety] FOREIGN KEY ([VarietyGuid]) REFERENCES [dbo].[Variety] ([Guid]),
    CONSTRAINT [UC_Product_Code] UNIQUE NONCLUSTERED ([Code] ASC),
    CONSTRAINT [UC_Product_ProgramVariety] UNIQUE NONCLUSTERED ([ProgramGuid] ASC, [VarietyGuid] ASC, [ProductFormGuid] ASC)
);










GO



--Alter table Product add LastUpdate datetime default '9/1/2014' not null

create trigger [dbo].[ProductUpdate]
on [dbo].[Product]
after update
as
begin

update p
set LastUpdate = getdate()
from Product as p
join inserted as b 
on p.Guid = b.Guid; 
end
﻿CREATE TABLE [dbo].[Variety] (
    [Id]                     BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                   UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]        DATETIME         NULL,
    [Code]                   NVARCHAR (30)    NOT NULL,
    [SpeciesGuid]            UNIQUEIDENTIFIER NOT NULL,
    [GeneticOwnerGuid]       UNIQUEIDENTIFIER NOT NULL,
    [Name]                   NVARCHAR (100)   NOT NULL,
    [ActiveDate]             DATETIME         NOT NULL,
    [DeactiveDate]           DATETIME         NULL,
    [ColorDescription]       NVARCHAR (30)    NOT NULL,
    [ImageSetGuid]           UNIQUEIDENTIFIER NULL,
    [ColorLookupGuid]        UNIQUEIDENTIFIER NULL,
    [HabitLookupGuid]        UNIQUEIDENTIFIER NULL,
    [VigorLookupGuid]        UNIQUEIDENTIFIER NULL,
    [TimingLookupGuid]       UNIQUEIDENTIFIER CONSTRAINT [DF__Variety__TimingL__02084FDA] DEFAULT ('E1ABB23E-FC52-4B06-8A40-6EDC7D3B8B78') NOT NULL,
    [SeriesGuid]             UNIQUEIDENTIFIER NULL,
    [DescriptionHTMLGUID]    UNIQUEIDENTIFIER NULL,
    [ResearchStatus]         NVARCHAR (15)    NOT NULL,
    [CultureLibraryLink]     NVARCHAR (200)   CONSTRAINT [DF__Variety__Culture__08012052] DEFAULT ('') NOT NULL,
    [LastUpdate]             DATETIME         CONSTRAINT [DF__Variety__LastUpd__292D09F3] DEFAULT ('9/1/14') NOT NULL,
    [NameStripped]           NVARCHAR (100)   CONSTRAINT [DF__Variety__NameStr__349EBC9F] DEFAULT ('') NOT NULL,
    [SpeciesAndNameStripped] NVARCHAR (150)   NULL,
    [ZoneLookupGuid]         UNIQUEIDENTIFIER NULL,
    [BrandLookupGuid]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK__Variety__A2B5777CEB632F40] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_Variety_BrandLookup] FOREIGN KEY ([BrandLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_Variety_ColorLookup] FOREIGN KEY ([ColorLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_Variety_GeneticOwner] FOREIGN KEY ([GeneticOwnerGuid]) REFERENCES [dbo].[GeneticOwner] ([Guid]),
    CONSTRAINT [FK_Variety_HabitLookup] FOREIGN KEY ([HabitLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_Variety_ImageSet] FOREIGN KEY ([ImageSetGuid]) REFERENCES [dbo].[ImageSet] ([Guid]),
    CONSTRAINT [FK_Variety_Series] FOREIGN KEY ([SeriesGuid]) REFERENCES [dbo].[Series] ([Guid]),
    CONSTRAINT [FK_Variety_Species] FOREIGN KEY ([SpeciesGuid]) REFERENCES [dbo].[Species] ([Guid]),
    CONSTRAINT [FK_Variety_VigorLookup] FOREIGN KEY ([VigorLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_Variety_ZoneLookup] FOREIGN KEY ([ZoneLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [UC_Variety_Code] UNIQUE NONCLUSTERED ([Code] ASC),
    CONSTRAINT [UC_VarietyNameSpeices] UNIQUE NONCLUSTERED ([Name] ASC, [SpeciesGuid] ASC)
);


















GO



--select * from SynchControl



--Alter table Variety add LastUpdate datetime not null default '9/1/14'

create trigger [dbo].[VarietyUpdate]
on [dbo].[Variety]
after update
as
begin

update v
set LastUpdate = getdate()
from Variety as v
join inserted as b 
on v.Guid = b.Guid; 
end
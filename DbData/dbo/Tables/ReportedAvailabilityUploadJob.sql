﻿CREATE TABLE [dbo].[ReportedAvailabilityUploadJob] (
    [Id]           BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]         UNIQUEIDENTIFIER NOT NULL,
    [UserGuid]     UNIQUEIDENTIFIER NOT NULL,
    [UploadDate]   DATETIME         NOT NULL,
    [SupplierCode] NVARCHAR (10)    NOT NULL,
    CONSTRAINT [PK__ReportedAvailabilityUploadJob] PRIMARY KEY CLUSTERED ([Guid] ASC)
);


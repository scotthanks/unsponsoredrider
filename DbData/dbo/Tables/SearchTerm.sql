﻿CREATE TABLE [dbo].[SearchTerm] (
    [Id]                BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]              UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]   DATETIME         NULL,
    [SearchString]      NVARCHAR (200)   NOT NULL,
    [RelatedSearches]   NVARCHAR (400)   NOT NULL,
    [ResultCount]       INT              NOT NULL,
    [IncludeInAutoFill] BIT              CONSTRAINT [DF__SearchTer__Inclu__5AA469F6] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__SearchTe__A2B5777C8E139487] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [UC_SearchTerm] UNIQUE NONCLUSTERED ([SearchString] ASC),
    CONSTRAINT [UC_SearchTerm_SearchString] UNIQUE NONCLUSTERED ([SearchString] ASC)
);








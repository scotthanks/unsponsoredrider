﻿CREATE TABLE [dbo].[GrowerVolume] (
    [Id]                       BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                     UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]          DATETIME         NULL,
    [GrowerGuid]               UNIQUEIDENTIFIER NOT NULL,
    [ProgramSeasonGuid]        UNIQUEIDENTIFIER NOT NULL,
    [ActualVolumeLevelGuid]    UNIQUEIDENTIFIER NULL,
    [EstimatedVolumeLevelGuid] UNIQUEIDENTIFIER NULL,
    [QuotedVolumeLevelGuid]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK__GrowerVo__A2B5777CBA54E6DC] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_GrowerVolume_ActualVolumeLevel] FOREIGN KEY ([ActualVolumeLevelGuid]) REFERENCES [dbo].[VolumeLevel] ([Guid]),
    CONSTRAINT [FK_GrowerVolume_EstimatedVolumeLevel] FOREIGN KEY ([EstimatedVolumeLevelGuid]) REFERENCES [dbo].[VolumeLevel] ([Guid]),
    CONSTRAINT [FK_GrowerVolume_Grower] FOREIGN KEY ([GrowerGuid]) REFERENCES [dbo].[Grower] ([Guid]),
    CONSTRAINT [FK_GrowerVolume_ProgramSeason] FOREIGN KEY ([ProgramSeasonGuid]) REFERENCES [dbo].[ProgramSeason] ([Guid]),
    CONSTRAINT [FK_GrowerVolume_QuotedVolumeLevel] FOREIGN KEY ([QuotedVolumeLevelGuid]) REFERENCES [dbo].[VolumeLevel] ([Guid]),
    CONSTRAINT [UC_Grower_ProgramSeason] UNIQUE NONCLUSTERED ([GrowerGuid] ASC, [ProgramSeasonGuid] ASC)
);






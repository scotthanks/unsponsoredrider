﻿--EXAMPLE:
--PRINT dbo.IsUniqueIdentifier('3848ABC5-B97E-4AF4-A2CF-0EE1B9D7EAE6')
--PRINT dbo.IsUniqueIdentifier('3848ABC5-B97E-4AF4-A2CF-0EE1B9D7EAEX')
--PRINT dbo.IsUniqueIdentifier('X848ABC5-B97E-4AF4-A2CF-0EE1B9D7EAE6')
--PRINT dbo.IsUniqueIdentifier('Hello')

CREATE FUNCTION [dbo].[IsUniqueIdentifier]
	(
		@String VARCHAR(56)
	) 
	RETURNS BIT
AS 
	BEGIN
		DECLARE @NullIdentifier as UNIQUEIDENTIFIER
		SET @NullIdentifier = CAST('00000000-0000-0000-0000-000000000000' AS UNIQUEIDENTIFIER)

		DECLARE @UniqueIdentifier as UNIQUEIDENTIFIER
		SET @UniqueIdentifier = dbo.ExtractUniqueIdentifier(@String)

		DECLARE @ReturnValue AS BIT

		IF @UniqueIdentifier = @NullIdentifier
			SET @ReturnValue = 0
		ELSE
			SET @ReturnValue = 1

		RETURN @ReturnValue
	END
﻿
--EXAMPLE:
--PRINT dbo.xmlsegment
--(
--	'Parameters',
--	dbo.xmlsegment('name','Rick',1) +
--	dbo.xmlsegment('rowcount',10,1) +
--	dbo.xmlsegment('id',newid(),1),
--	0
--)

CREATE FUNCTION [dbo].[XmlSegment] 
	(
		@Name VARCHAR(50), 
		@Value VARCHAR(MAX),
		@IndentLevel INT = 0
	) 
	RETURNS VARCHAR(MAX)
AS 
	BEGIN
		DECLARE @XmlSegment AS VARCHAR(MAX)

		--SET @Value = REPLACE( @Value, '<', '(lt)')
		--SET @Value = REPLACE( @Value, '>', '(gt)')

		SET @Value = REPLACE( @Value, '&', '(amp)')
		SET @Value = REPLACE( @Value, '''', '(sq)')
		SET @Value = REPLACE( @Value, '"', '(dq)')

		IF @Value IS NULL
			SET @Value='null'

		SET @XmlSegment =
			'<' +
			LTRIM(RTRIM(@Name)) +
			'>' +
			LTRIM(RTRIM(@Value)) +
			'</' +
			LTRIM(RTRIM(@Name)) +
			'>'

		RETURN @XmlSegment
	END
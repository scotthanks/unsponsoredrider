﻿--EXAMPLE:
--PRINT 'Parameters:'
--PRINT dbo.NameAndValue('Name','Rick',1)
--PRINT dbo.NameAndValue('RowCount',10,1)
--PRINT dbo.NameAndValue('Id',newid(),1)

CREATE FUNCTION [dbo].[NameAndValue] 
	(
		@Name VARCHAR(50), 
		@Value VARCHAR(MAX),
		@IndentLevel INT
	) 
	RETURNS VARCHAR(MAX)
AS 
	BEGIN
		DECLARE @String AS VARCHAR(MAX)
		DECLARE @Indent AS VARCHAR(30)

		SET @Indent = ''
		DECLARE @Count AS INT
		SET @Count = 0
		WHILE @Count < @IndentLevel
			BEGIN
				SET @Indent = @Indent + '    '
				SET @Count = @Count + 1
			END

		SET @String =
			@Indent +
			LTRIM(RTRIM(@Name)) +
			'=' +
			LTRIM(RTRIM(@Value))

		RETURN @String
	END
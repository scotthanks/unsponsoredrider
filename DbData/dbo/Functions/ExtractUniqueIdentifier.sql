﻿
--EXAMPLE:
--PRINT dbo.ExtractUniqueIdentifier('3848ABC5-B97E-4AF4-A2CF-0EE1B9D7EAE6')
--PRINT dbo.ExtractUniqueIdentifier('3848ABC5-B97E-4AF4-A2CF-0EE1B9D7EAEX')
--PRINT dbo.ExtractUniqueIdentifier('X848ABC5-B97E-4AF4-A2CF-0EE1B9D7EAE6')
--PRINT dbo.ExtractUniqueIdentifier('Hello')

CREATE FUNCTION [dbo].[ExtractUniqueIdentifier]
	(
		@String VARCHAR(56)
	) 
	RETURNS UNIQUEIDENTIFIER
AS 
	BEGIN
		DECLARE @UniqueIdentifier as UNIQUEIDENTIFIER
		SET @UniqueIdentifier = CAST('00000000-0000-0000-0000-000000000000' AS UNIQUEIDENTIFIER)

		DECLARE @i AS INTEGER
		SET @i = 1

		DECLARE @c AS VARCHAR(1)
		DECLARE @IsValidUniqueIdentifier AS BIT
		SET @IsValidUniqueIdentifier = 1
		WHILE(@i <= 36)
			BEGIN
				SET @c = SUBSTRING(@String,@i,1)

				IF @i IN (9,14,19,24)
					BEGIN
						IF @c != '-'
							SET @IsValidUniqueIdentifier = 0
					END
				ELSE
					BEGIN
						IF (@c < '0' OR @c > '9') AND (@c < 'A' OR @c > 'F')
							SET @IsValidUniqueIdentifier = 0
					END

				SET @i = @i + 1
			END

		IF @IsValidUniqueIdentifier = 0
			SET @String = '00000000-0000-0000-0000-000000000000'

		SET @UniqueIdentifier = CAST(@String AS UNIQUEIDENTIFIER)

		return @UniqueIdentifier
	END
﻿

--EXAMPLE:

---- Delete all Rick Greenhouse orders.
--DELETE OrderLine WHERE Guid IN (SELECT OrderLineGuid FROM OrderLineView WHERE GrowerName = 'Rick Greenhouse')
--DELETE SupplierOrder WHERE Guid IN (SELECT SupplierOrderGuid FROM SupplierOrderView WHERE GrowerName = 'Rick Greenhouse')
--DELETE GrowerOrder WHERE Guid IN (SELECT GrowerOrderGuid FROM GrowerOrderView WHERE GrowerName = 'Rick Greenhouse')

--DECLARE @OrderLineTrxStatusLookupCode AS NCHAR(20)
--DECLARE @ActualQuantity AS INTEGER
--DECLARE @GrowerOrderGuid AS UNIQUEIDENTIFIER
--DECLARE @SupplierOrderGuid AS UNIQUEIDENTIFIER
--DECLARE @OrderLineGuid AS UNIQUEIDENTIFIER

---- Add a new item, but with a zero quantity, resulting in no change.
--EXECUTE GrowerOrderProcessTransaction
--	@UserCode = 'rick.harrison',
--	@ShipWeekCode='201320',
--	@ProductGuid='42E77AE2-375B-4EF7-ABCA-004DD9A6F33A',
--	@Quantity='0',
--	@OrderTypeLookupCode='Order',
--	@OrderLineTrxStatusLookupCode=@OrderLineTrxStatusLookupCode OUT,
--	@ActualQuantity=@ActualQuantity OUT,
--	@GrowerOrderGuid=@GrowerOrderGuid OUT,
--	@SupplierOrderGuid=@SupplierOrderGuid OUT,
--	@OrderLineGuid=@OrderLineGuid OUT

--SELECT
--	@OrderLineTrxStatusLookupCode AS OrderLineTrxStatusLookupCode,
--	@ActualQuantity AS ActualQuantity,
--	@GrowerOrderGuid AS GrowerOrderGuid,
--	@SupplierOrderGuid AS SupplierOrderGuid,
--	@OrderLineGuid AS OrderLineGuid

--SELECT * FROM GrowerOrder WHERE Guid IN (SELECT GrowerOrderGuid FROM GrowerOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM SupplierOrder WHERE Guid IN (SELECT SupplierOrderGuid FROM SupplierOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM OrderLine WHERE Guid IN (SELECT OrderLineGuid FROM OrderLineView WHERE GrowerName = 'Rick Greenhouse')

---- Add a new item.
--EXECUTE GrowerOrderProcessTransaction
--	@UserCode = 'rick.harrison',
--	@ShipWeekCode='201320',
--	@ProductGuid='42E77AE2-375B-4EF7-ABCA-004DD9A6F33A',
--	@Quantity='3010',
--	@OrderTypeLookupCode='Order',
--	@OrderLineTrxStatusLookupCode=@OrderLineTrxStatusLookupCode OUT,
--	@ActualQuantity=@ActualQuantity OUT,
--	@GrowerOrderGuid=@GrowerOrderGuid OUT,
--	@SupplierOrderGuid=@SupplierOrderGuid OUT,
--	@OrderLineGuid=@OrderLineGuid OUT

--SELECT
--	@OrderLineTrxStatusLookupCode AS OrderLineTrxStatusLookupCode,
--	@ActualQuantity AS ActualQuantity,
--	@GrowerOrderGuid AS GrowerOrderGuid,
--	@SupplierOrderGuid AS SupplierOrderGuid,
--	@OrderLineGuid AS OrderLineGuid

--SELECT * FROM GrowerOrder WHERE Guid IN (SELECT GrowerOrderGuid FROM GrowerOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM SupplierOrder WHERE Guid IN (SELECT SupplierOrderGuid FROM SupplierOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM OrderLine WHERE Guid IN (SELECT OrderLineGuid FROM OrderLineView WHERE GrowerName = 'Rick Greenhouse')

---- Add a new item that goes on the same supplier order.
--EXECUTE GrowerOrderProcessTransaction
--	@UserCode = 'rick.harrison',
--	@ShipWeekCode='201320',
--	@ProductGuid='BCBF7300-6F68-4D01-A5C5-DEA275269DBF',
--	@Quantity='200',
--	@OrderTypeLookupCode='Order',
--	@OrderLineTrxStatusLookupCode=@OrderLineTrxStatusLookupCode OUT,
--	@ActualQuantity=@ActualQuantity OUT,
--	@GrowerOrderGuid=@GrowerOrderGuid OUT,
--	@SupplierOrderGuid=@SupplierOrderGuid OUT,
--	@OrderLineGuid=@OrderLineGuid OUT

--SELECT
--	@OrderLineTrxStatusLookupCode AS OrderLineTrxStatusLookupCode,
--	@ActualQuantity AS ActualQuantity,
--	@GrowerOrderGuid AS GrowerOrderGuid,
--	@SupplierOrderGuid AS SupplierOrderGuid,
--	@OrderLineGuid AS OrderLineGuid

--SELECT * FROM GrowerOrder WHERE Guid IN (SELECT GrowerOrderGuid FROM GrowerOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM SupplierOrder WHERE Guid IN (SELECT SupplierOrderGuid FROM SupplierOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM OrderLine WHERE Guid IN (SELECT OrderLineGuid FROM OrderLineView WHERE GrowerName = 'Rick Greenhouse')

---- Update the first item by changing the quantity
--EXECUTE GrowerOrderProcessTransaction
--	@UserCode = 'rick.harrison',
--	@ShipWeekCode='201320',
--	@ProductGuid='42E77AE2-375B-4EF7-ABCA-004DD9A6F33A',
--	@Quantity='500',
--	@OrderTypeLookupCode='Order',
--	@OrderLineTrxStatusLookupCode=@OrderLineTrxStatusLookupCode OUT,
--	@ActualQuantity=@ActualQuantity OUT,
--	@GrowerOrderGuid=@GrowerOrderGuid OUT,
--	@SupplierOrderGuid=@SupplierOrderGuid OUT,
--	@OrderLineGuid=@OrderLineGuid OUT

--SELECT
--	@OrderLineTrxStatusLookupCode AS OrderLineTrxStatusLookupCode,
--	@ActualQuantity AS ActualQuantity,
--	@GrowerOrderGuid AS GrowerOrderGuid,
--	@SupplierOrderGuid AS SupplierOrderGuid,
--	@OrderLineGuid AS OrderLineGuid

--SELECT * FROM GrowerOrder WHERE Guid IN (SELECT GrowerOrderGuid FROM GrowerOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM SupplierOrder WHERE Guid IN (SELECT SupplierOrderGuid FROM SupplierOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM OrderLine WHERE Guid IN (SELECT OrderLineGuid FROM OrderLineView WHERE GrowerName = 'Rick Greenhouse')

---- Cause no change to the first item by attempting to change the quantity by less than the order multiple.
--EXECUTE GrowerOrderProcessTransaction
--	@UserCode = 'rick.harrison',
--	@ShipWeekCode='201320',
--	@ProductGuid='42E77AE2-375B-4EF7-ABCA-004DD9A6F33A',
--	@Quantity='501',
--	@OrderTypeLookupCode='Order',
--	@OrderLineTrxStatusLookupCode=@OrderLineTrxStatusLookupCode OUT,
--	@ActualQuantity=@ActualQuantity OUT,
--	@GrowerOrderGuid=@GrowerOrderGuid OUT,
--	@SupplierOrderGuid=@SupplierOrderGuid OUT,
--	@OrderLineGuid=@OrderLineGuid OUT

--SELECT
--	@OrderLineTrxStatusLookupCode AS OrderLineTrxStatusLookupCode,
--	@ActualQuantity AS ActualQuantity,
--	@GrowerOrderGuid AS GrowerOrderGuid,
--	@SupplierOrderGuid AS SupplierOrderGuid,
--	@OrderLineGuid AS OrderLineGuid

--SELECT * FROM GrowerOrder WHERE Guid IN (SELECT GrowerOrderGuid FROM GrowerOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM SupplierOrder WHERE Guid IN (SELECT SupplierOrderGuid FROM SupplierOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM OrderLine WHERE Guid IN (SELECT OrderLineGuid FROM OrderLineView WHERE GrowerName = 'Rick Greenhouse')

---- Add a new item that goes on a new supplier order, but with a zero quantity, resulting in no change.
--EXECUTE GrowerOrderProcessTransaction
--	@UserCode = 'rick.harrison',
--	@ShipWeekCode='201320',
--	@ProductGuid='D2FD1E28-D6C0-4DA8-BF67-C37BAABDD03D',
--	@Quantity='0',
--	@OrderTypeLookupCode='Order',
--	@OrderLineTrxStatusLookupCode=@OrderLineTrxStatusLookupCode OUT,
--	@ActualQuantity=@ActualQuantity OUT,
--	@GrowerOrderGuid=@GrowerOrderGuid OUT,
--	@SupplierOrderGuid=@SupplierOrderGuid OUT,
--	@OrderLineGuid=@OrderLineGuid OUT

--SELECT
--	@OrderLineTrxStatusLookupCode AS OrderLineTrxStatusLookupCode,
--	@ActualQuantity AS ActualQuantity,
--	@GrowerOrderGuid AS GrowerOrderGuid,
--	@SupplierOrderGuid AS SupplierOrderGuid,
--	@OrderLineGuid AS OrderLineGuid

--SELECT * FROM GrowerOrder WHERE Guid IN (SELECT GrowerOrderGuid FROM GrowerOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM SupplierOrder WHERE Guid IN (SELECT SupplierOrderGuid FROM SupplierOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM OrderLine WHERE Guid IN (SELECT OrderLineGuid FROM OrderLineView WHERE GrowerName = 'Rick Greenhouse')

---- Add a new item that goes on a new supplier order.
--EXECUTE GrowerOrderProcessTransaction
--	@UserCode = 'rick.harrison',
--	@ShipWeekCode='201320',
--	@ProductGuid='D2FD1E28-D6C0-4DA8-BF67-C37BAABDD03D',
--	@Quantity='500',
--	@OrderTypeLookupCode='Order',
--	@OrderLineTrxStatusLookupCode=@OrderLineTrxStatusLookupCode OUT,
--	@ActualQuantity=@ActualQuantity OUT,
--	@GrowerOrderGuid=@GrowerOrderGuid OUT,
--	@SupplierOrderGuid=@SupplierOrderGuid OUT,
--	@OrderLineGuid=@OrderLineGuid OUT

--SELECT
--	@OrderLineTrxStatusLookupCode AS OrderLineTrxStatusLookupCode,
--	@ActualQuantity AS ActualQuantity,
--	@GrowerOrderGuid AS GrowerOrderGuid,
--	@SupplierOrderGuid AS SupplierOrderGuid,
--	@OrderLineGuid AS OrderLineGuid

--SELECT * FROM GrowerOrder WHERE Guid IN (SELECT GrowerOrderGuid FROM GrowerOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM SupplierOrder WHERE Guid IN (SELECT SupplierOrderGuid FROM SupplierOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM OrderLine WHERE Guid IN (SELECT OrderLineGuid FROM OrderLineView WHERE GrowerName = 'Rick Greenhouse')

---- Set quantity on the line to zero, causing the line and supplier order to be deleted.
--EXECUTE GrowerOrderProcessTransaction
--	@UserCode = 'rick.harrison',
--	@ShipWeekCode='201320',
--	@ProductGuid='D2FD1E28-D6C0-4DA8-BF67-C37BAABDD03D',
--	@Quantity='0',
--	@OrderTypeLookupCode='Order',
--	@OrderLineTrxStatusLookupCode=@OrderLineTrxStatusLookupCode OUT,
--	@ActualQuantity=@ActualQuantity OUT,
--	@GrowerOrderGuid=@GrowerOrderGuid OUT,
--	@SupplierOrderGuid=@SupplierOrderGuid OUT,
--	@OrderLineGuid=@OrderLineGuid OUT

--SELECT
--	@OrderLineTrxStatusLookupCode AS OrderLineTrxStatusLookupCode,
--	@ActualQuantity AS ActualQuantity,
--	@GrowerOrderGuid AS GrowerOrderGuid,
--	@SupplierOrderGuid AS SupplierOrderGuid,
--	@OrderLineGuid AS OrderLineGuid

--SELECT * FROM GrowerOrder WHERE Guid IN (SELECT GrowerOrderGuid FROM GrowerOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM SupplierOrder WHERE Guid IN (SELECT SupplierOrderGuid FROM SupplierOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM OrderLine WHERE Guid IN (SELECT OrderLineGuid FROM OrderLineView WHERE GrowerName = 'Rick Greenhouse')

---- Set quantity on the line to zero, causing the line to be deleted.
--EXECUTE GrowerOrderProcessTransaction
--	@UserCode = 'rick.harrison',
--	@ShipWeekCode='201320',
--	@ProductGuid='BCBF7300-6F68-4D01-A5C5-DEA275269DBF',
--	@Quantity='0',
--	@OrderTypeLookupCode='Order',
--	@OrderLineTrxStatusLookupCode=@OrderLineTrxStatusLookupCode OUT,
--	@ActualQuantity=@ActualQuantity OUT,
--	@GrowerOrderGuid=@GrowerOrderGuid OUT,
--	@SupplierOrderGuid=@SupplierOrderGuid OUT,
--	@OrderLineGuid=@OrderLineGuid OUT

--SELECT
--	@OrderLineTrxStatusLookupCode AS OrderLineTrxStatusLookupCode,
--	@ActualQuantity AS ActualQuantity,
--	@GrowerOrderGuid AS GrowerOrderGuid,
--	@SupplierOrderGuid AS SupplierOrderGuid,
--	@OrderLineGuid AS OrderLineGuid

--SELECT * FROM GrowerOrder WHERE Guid IN (SELECT GrowerOrderGuid FROM GrowerOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM SupplierOrder WHERE Guid IN (SELECT SupplierOrderGuid FROM SupplierOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM OrderLine WHERE Guid IN (SELECT OrderLineGuid FROM OrderLineView WHERE GrowerName = 'Rick Greenhouse')

---- Set quantity on the line to zero, causing the line and supplier order and grower order to be deleted.
--EXECUTE GrowerOrderProcessTransaction
--	@UserCode = 'rick.harrison',
--	@ShipWeekCode='201320',
--	@ProductGuid='42E77AE2-375B-4EF7-ABCA-004DD9A6F33A',
--	@Quantity='0',
--	@OrderTypeLookupCode='Order',
--	@OrderLineTrxStatusLookupCode=@OrderLineTrxStatusLookupCode OUT,
--	@ActualQuantity=@ActualQuantity OUT,
--	@GrowerOrderGuid=@GrowerOrderGuid OUT,
--	@SupplierOrderGuid=@SupplierOrderGuid OUT,
--	@OrderLineGuid=@OrderLineGuid OUT

--SELECT
--	@OrderLineTrxStatusLookupCode AS OrderLineTrxStatusLookupCode,
--	@ActualQuantity AS ActualQuantity,
--	@GrowerOrderGuid AS GrowerOrderGuid,
--	@SupplierOrderGuid AS SupplierOrderGuid,
--	@OrderLineGuid AS OrderLineGuid

--SELECT * FROM GrowerOrder WHERE Guid IN (SELECT GrowerOrderGuid FROM GrowerOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM SupplierOrder WHERE Guid IN (SELECT SupplierOrderGuid FROM SupplierOrderView WHERE GrowerName = 'Rick Greenhouse')
--SELECT * FROM OrderLine WHERE Guid IN (SELECT OrderLineGuid FROM OrderLineView WHERE GrowerName = 'Rick Greenhouse')

CREATE PROCEDURE [dbo].[GrowerOrderProcessTransaction]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NCHAR(56) = NULL,
	@ShipWeekCode AS NCHAR(6),
	@ProductGuid AS UNIQUEIDENTIFIER,
	@Quantity AS INTEGER,
	@OrderTypeLookupCode AS NCHAR(10) = 'Order',
	@Price AS DECIMAL(8,4) = NULL,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER = NULL OUT,
	@SupplierOrderGuid AS UNIQUEIDENTIFIER = NULL OUT,
	@OrderLineGuid AS UNIQUEIDENTIFIER = NULL OUT,
	@OrderLineTrxStatusLookupCode AS NVARCHAR(20) = NULL OUT,
	@ActualQuantity AS INTEGER = NULL OUT
AS
	-- DEPRICATED OBSOLETE
	IF dbo.IsUniqueIdentifier(@UserCode) = 1 AND @UserGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
		BEGIN
			SET @UserGuid = dbo.ExtractUniqueIdentifier(@UserCode)

			SELECT @UserCode = UserCode
			FROM Person
			WHERE UserGuid = @UserGuid
		END

	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('UserGuid',LEFT(@UserGuid,4)+'...',1) +
		dbo.XmlSegment('ShipWeekCode',@ShipWeekCode,1) +
		dbo.XmlSegment('ProductGuid',@ProductGuid,1) +
		dbo.XmlSegment('Quantity',@Quantity,1) +
		dbo.XmlSegment('OrderTypeLookupCode',@OrderTypeLookupCode,1) +
		dbo.XmlSegment('Price',@Price,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerOrderProcessTransaction',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	SET @OrderLineTrxStatusLookupCode = 'Failure'

	DECLARE @PersonGuid AS UNIQUEIDENTIFIER
	EXECUTE PersonGetGuid
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@ProcessLookupCode='GrowerOrderProcessTransaction',
		@PersonGuid=@PersonGuid OUT

	DECLARE @GrowerGuid AS UNIQUEIDENTIFIER
	EXECUTE GrowerGetGuid
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@PersonGuid=@PersonGuid,
		@ProcessLookupCode='GrowerOrderProcessTransaction',
		@GrowerGuid=@GrowerGuid OUT

	DECLARE @ShipWeekGuid AS UNIQUEIDENTIFIER
	EXECUTE ShipWeekGetGuid
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@ShipWeekCode=@ShipWeekCode,
		@ProcessLookupCode='GrowerOrderProcessTransaction',
		@ShipWeekGuid=@ShipWeekGuid OUT

	DECLARE @OrderTypeLookupGuid AS UNIQUEIDENTIFIER
	EXECUTE LookupGetGuid
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@Path='Code/OrderType',
		@LookupCode=@OrderTypeLookupCode,
		@ProcessLookupCode='GrowerOrderProcessTransaction',
		@LookupGuid=@OrderTypeLookupGuid OUT

	SET @GrowerOrderGuid = NULL
	SET @SupplierOrderGuid = NULL

	DECLARE @ProgramTypeCode AS NCHAR(20)
	DECLARE @ProductFormCategoryCode AS NCHAR(20)

	SELECT 
		@ProgramTypeCode = pt.Code,
		@ProductFormCategoryCode = pfc.Code
	FROM Product p
	Join Program pr on p.ProgramGuid = pr.Guid
	Join ProgramType pt on pr.ProgramTypeGuid = pt.Guid
	Join ProductFormCategory pfc on pr.ProductFormCategoryGuid = pfc.Guid
	WHERE p.Guid=@ProductGuid

	IF @ProgramTypeCode IS NULL OR @ProductFormCategoryCode IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid,
				@UserCode=@UserCode,
				@LogTypeLookupCode='TransactionFail',
				@ProcessLookupCode='GrowerOrderProcessTransaction',
				@ObjectTypeLookupCode='EventLog',
				@ObjectGuid=@OriginalEventLogGuid,
				@GuidValue=@ProductGuid,
				@Comment='The Product (see GuidValue) is not valid.'		

			RETURN
		END

	IF
		@PersonGuid IS NOT NULL AND
		@GrowerGuid IS NOT NULL AND
		@ShipWeekGuid IS NOT NULL AND
		@OrderTypeLookupGuid IS NOT NULL
			BEGIN
				BEGIN TRANSACTION

				-- This is a "lock hint" that tells SQL Server to lock the Grower row during the transaciton. Without it, we get multiple orders when items are ordered rapidly in succession.
				SELECT @GrowerGuid=Guid from Grower WITH (ROWLOCK,XLOCK) WHERE Guid=@GrowerGuid

				EXECUTE GrowerOrderGetOrAdd
					@UserGuid=@UserGuid,
					@UserCode = @UserCode,
					@GrowerGuid = @GrowerGuid,
					@ShipWeekCode = @ShipWeekCode,
					@ProductFormCategoryCode = @ProductFormCategoryCode,
					@ProgramTypeCode = @ProgramTypeCode,
					@OrderTypeLookupCode = @OrderTypeLookupCode,
					@OrderLineStatusLookupCodeList = 'PreCart,Pending',
					@GrowerOrderGuid = @GrowerOrderGuid OUT

				IF @GrowerOrderGuid IS NOT NULL
					BEGIN
						DECLARE @SupplierGuid UNIQUEIDENTIFIER
						SELECT @SupplierGuid = pr.SupplierGuid 
							FROM Product p
							Join Program pr on p.ProgramGuid = pr.Guid
							 WHERE p.Guid = @ProductGuid

						EXECUTE SupplierOrderGetOrAdd
							@UserGuid=@UserGuid,
							@UserCode = @UserCode,
							@GrowerOrderGuid = @GrowerOrderGuid,
							@SupplierGuid = @SupplierGuid,
							@ProgramTypeCode = @ProgramTypeCode,
							@ProductFormCategoryCode = @ProductFormCategoryCode,
							@SupplierOrderGuid = @SupplierOrderGuid OUT

						IF @SupplierOrderGuid IS NULL
							BEGIN
								EXECUTE GrowerOrderCleanupOrphans
									@GrowerGuid=@GrowerGuid,
									@GrowerOrderGuid=@GrowerOrderGuid

								SET @GrowerOrderGuid = NULL
							END
						ELSE
							BEGIN
								EXECUTE OrderLineProcessTransaction
									@UserGuid = @UserGuid,
									@UserCode = @UserCode,
									@SupplierOrderGuid = @SupplierOrderGuid,
									@ProductGuid = @ProductGuid,
									@QtyOrdered = @Quantity,
									@Price = @Price,
									@SupplierOrGrower = 'Grower',
									@OrderLineTrxStatusLookupCode = @OrderLineTrxStatusLookupCode OUT,
									@ActualQuantity = @ActualQuantity OUT,
									@OrderLineGuid = @OrderLineGuid OUT

								IF @OrderLineGuid IS NULL OR @OrderLineTrxStatusLookupCode = 'Deleted'
									BEGIN
										EXECUTE GrowerOrderCleanupOrphans
											@GrowerGuid=@GrowerGuid,
											@GrowerOrderGuid=@GrowerOrderGuid

										SET @SupplierOrderGuid = NULL
										SET @GrowerOrderGuid = NULL
									END
							END
					END

					COMMIT
			END

	--EXECUTE AvailabilityDataGet
	--	@UserGuid=@UserGuid,
	--	@UserCode=@UserCode,
	--	@ShipWeekCode=@ShipWeekCode,
	--	@WeeksBefore=1, 
	--	@weeksAfter=3, 
	--	@ProductGuid=@ProductGuid

	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('GrowerOrderGuid',@GrowerOrderGuid,1) +
		dbo.XmlSegment('SupplierOrderGuid',@SupplierOrderGuid,1) +
		dbo.XmlSegment('OrderLineGuid',@OrderLineGuid,1) +
		dbo.XmlSegment('OrderLineTrxStatusLookupCode',@OrderLineTrxStatusLookupCode,1) +
		dbo.XmlSegment('ActualQuantity',@ActualQuantity,1),
		0
	)

	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerOrderProcessTransaction',
		@ObjectTypeLookupCode='GrowerOrder',
		@ObjectGuid=@GrowerOrderGuid,
		@XmlData=@ReturnValues
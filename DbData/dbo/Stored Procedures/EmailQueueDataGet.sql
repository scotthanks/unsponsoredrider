﻿



CREATE PROCEDURE [dbo].[EmailQueueDataGet]
	
AS
	Update EmailQueue
	set SendStatus ='Processing'
	WHERE SendStatus = 'Not Sent'
	and DateToSend < getdate()

	SELECT *
	FROM EmailQueue
	WHERE SendStatus = 'Processing'
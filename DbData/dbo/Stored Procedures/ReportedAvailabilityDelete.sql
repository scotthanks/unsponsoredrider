﻿--EXAMPLE:

--EXECUTE ReportedAvailabilityDelete
--	@ShipWeekCode='201334'
--	@SupplierCode='LIN'
--	@SupplierIdentifier='623 '

CREATE procedure [dbo].[ReportedAvailabilityDelete]
	@ShipWeekCode AS NCHAR(6),
	@SupplierCode AS NCHAR(10),
	@SupplierIdentifier AS NCHAR(50)
AS
	DELETE ReportedAvailability
	FROM ReportedAvailability
		INNER JOIN ReportedAvailabilityView
			ON ReportedAvailability.Guid = ReportedAvailabilityView.ReportedAvailabilityGuid
	WHERE
		ReportedAvailabilityView.ShipWeekCode = @ShipWeekCode AND
		ReportedAvailabilityView.SupplierCode = @SupplierCode AND
		ReportedAvailabilityView.ProductSupplierIdentifier = @SupplierIdentifier
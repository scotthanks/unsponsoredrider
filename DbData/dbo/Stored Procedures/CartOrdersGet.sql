﻿


CREATE PROCEDURE [dbo].[CartOrdersGet]
	@UserGuid AS uniqueidentifier
AS

SELECT 
o.GrowerOrderGuid,
right(o.ShipWeekCode,2) + '|' + left(o.ShipWeekCode,4) as ShipWeekString,
pfc.Name as ProductFormName,
pt.Name as ProgramCategoryName,
o.QtyOrderedCount as OrderQty,
og.ID as OrderID
 
FROM Person p

JOIN GrowerOrderSummaryByShipWeekGroupByOrderView o on p.GrowerGuid = o.GrowerGuid
JOIN GrowerOrder og on o.growerorderguid = og.guid
JOIN shipweek sw on o.shipweekguid = sw.guid
JOIN ProductFormCategory pfc on og.ProductFormCategoryGuid = pfc.Guid
JOIN ProgramType pt on og.ProgramTypeGuid = pt.Guid
Where
p.UserGuid = @UserGuid
and (o.LowestOrderLineStatusLookupCode ='Pending' 
	OR
	 o.CountOfPendingStatusLines > 0 )                                        
Order By 
	sw.ContinuousWeekNumber
﻿
CREATE PROCEDURE [dbo].[catalog_SpeciesGet]
	@categoryCode AS NCHAR(50) = null,
	@productFormCode AS NCHAR(50) = null,
	@supplierCodes AS NVARCHAR(500) = null
AS

--NOTE: MRR on 2015-06-05Should SupplierCodeList be limited to 500 chars, is that enough
	declare @ProgramTypeCode AS NCHAR(50) = @categoryCode
	declare @ProductFormCategoryCode AS NCHAR(50) = @productFormCode
	declare @GeneticOwnerCodeList AS NVARCHAR(500) = null
	declare @SupplierCodeList AS NVARCHAR(500) = @supplierCodes


	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('ProgramTypeCode',@ProgramTypeCode,1) +
		dbo.XmlSegment('ProductFormCategoryCode',@ProductFormCategoryCode,1) +
		dbo.XmlSegment('GeneticOwnerCodeList',@GeneticOwnerCodeList,1) +
		dbo.XmlSegment('SupplierCodeList',@SupplierCodeList,1),
		0
	)

	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER	
	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='SpeciesDataGet',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	IF @ProgramTypeCode Like '%*%'
		SET @ProgramTypeCode = NULL

	IF LEN(RTRIM(@ProgramTypeCode)) > 3
		BEGIN
			SELECT @ProgramTypeCode = Code FROM ProgramType WHERE Name LIKE RTRIM(@ProgramTypeCode) + '%'

			IF @ProgramTypeCode IS NULL
				SELECT @ProgramTypeCode = Code FROM ProgramType WHERE Name LIKE '%' + RTRIM(@ProgramTypeCode) + '%'

			IF @ProgramTypeCode IS NULL
				SET @ProgramTypeCode = '*'
		END

	PRINT '@ProgramTypeCode=' + @ProgramTypeCode

	IF @ProductFormCategoryCode Like '%*%'
		SET @ProductFormCategoryCode = NULL

	IF LEN(RTRIM(@ProductFormCategoryCode)) > 3
		BEGIN
			SELECT @ProductFormCategoryCode = Code FROM ProductFormCategory WHERE Name LIKE RTRIM(@ProductFormCategoryCode) + '%'

			IF @ProductFormCategoryCode IS NULL
				SELECT @ProductFormCategoryCode = Code FROM ProductFormCategory WHERE Name LIKE '%' + RTRIM(@ProductFormCategoryCode) + '%'

			IF @ProductFormCategoryCode IS NULL
				SET @ProductFormCategoryCode = '*'
		END

	PRINT '@ProductFormCategoryCode=' + @ProductFormCategoryCode

	IF @GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%*%'
		SET @GeneticOwnerCodeList = NULL
	ELSE
		SET @GeneticOwnerCodeList = ',' + @GeneticOwnerCodeList + ','

	PRINT '@GeneticOwnerCodeList=' + @GeneticOwnerCodeList

	IF @SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%*%'
		SET @SupplierCodeList = NULL
	ELSE
		SET @SupplierCodeList = ',' + @SupplierCodeList + ','

	PRINT '@SupplierCodeList=' + @SupplierCodeList

	DECLARE @LocalTempSpeciesData TABLE
	(
		SpeciesGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalTempSpeciesData
	SELECT
		SpeciesGuid
	FROM ProductView
	WHERE
		(@ProgramTypeCode IS NULL OR ProgramTypeCode=@ProgramTypeCode) AND
		(@ProductFormCategoryCode IS NULL OR ProductFormCategoryCode=@ProductFormCategoryCode) AND
		(@GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%,' + RTRIM(GeneticOwnerCode) + ',%') AND
		(@SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%,' + RTRIM(SupplierCode) + ',%')
	GROUP BY
		SpeciesGuid

	PRINT CAST(@@ROWCOUNT AS NVARCHAR(10)) + ' Species found.'

	SELECT Species.*
	FROM Species
	WHERE Species.Guid IN
	(SELECT DISTINCT(SpeciesGuid) FROM @LocalTempSpeciesData)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='SpeciesDataGet',
		@ObjectGuid=@OriginalEventLogGuid
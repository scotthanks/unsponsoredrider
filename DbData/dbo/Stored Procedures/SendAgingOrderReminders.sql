﻿






CREATE PROCEDURE [dbo].[SendAgingOrderReminders]
	
AS
	
	Declare @nowDate DateTime = GetDate()
	Declare @EmailMessage nVarchar(max) =  ''
	Declare @EmailMessage2 nVarchar(max) =  ''
	Declare @2DayDetail nVarchar(max) = ''
	Declare @7DayDetail nVarchar(max) = ''
 	Declare @bSend bit
           
            
    exec GrowerOrderReminderCheck
    
	DECLARE @GrowerName VARCHAR(50)   
	DECLARE @OrderNo VARCHAR(50) 
	DECLARE @VarietyName VARCHAR(200) 
	DECLARE @DateEntered DateTime
	DECLARE @Reminder2Day bit
	DECLARE @Reminder7Day bit
	DECLARE @OrderLineGuid UniqueIdentifier

	set @EmailMessage = 'These are the 2 day cart items:<BR>'  + CHAR(13) 
	set @EmailMessage2 = 'These are the 7 day cart items:<BR>'  + CHAR(13) 

	DECLARE db_Order CURSOR FOR  
		select g.Name as GrowerName,o.OrderNo,v.Name as VarietyName,ol.DateEntered,
		gor.Reminder2Day,gor.Reminder7Day,ol.Guid as OrderLineGuid
		from GrowerOrderReminder gor
		Join OrderLine ol on ol.Guid = gor.OrderLineGuid
		Join Product p on ol.ProductGuid =p.Guid
		Join variety v on p.VarietyGuid = v.Guid
		Join supplierOrder so on ol.SupplierOrderGuid = so.Guid
		join GrowerOrder o on so.GrowerOrderGuid = o.Guid
		Join Grower g on o.GrowerGuid = g.Guid
		Join person pe on o.PersonGuid = pe.Guid
		Order by g.Name, o.OrderNo,v.Name

	OPEN db_Order   
	FETCH NEXT FROM db_Order INTO @GrowerName, @OrderNo ,@VarietyName,@DateEntered,@Reminder2Day,@Reminder7Day,@OrderLineGuid

	WHILE @@FETCH_STATUS = 0   
	BEGIN   
		
		IF @Reminder2Day = 0
		BEGIN
			If (DATEDIFF(second, @DateEntered, @nowDate) / 3600.0) > 48 
			BEGIN
				set @bSend = 1
				set @2DayDetail = @2DayDetail + @GrowerName + ' - ' + @OrderNo + ' - ' + @VarietyName + ' <BR> ' 
				exec MarkGrowerOrderReminder @OrderLineGuid,2
			END
		END
		IF @Reminder7Day = 0
		BEGIN
			If (DATEDIFF(second, @DateEntered, @nowDate) / 3600.0) > 168 
			BEGIN
				set @bSend = 1
				set @7DayDetail = @7DayDetail + @GrowerName + ' - ' + @OrderNo + ' - ' + @VarietyName + ' <BR> ' 
				exec MarkGrowerOrderReminder @OrderLineGuid,7
			END
		END
		FETCH NEXT FROM db_Order INTO @GrowerName, @OrderNo ,@VarietyName,@DateEntered,@Reminder2Day,@Reminder7Day,@OrderLineGuid
	END   

	CLOSE db_Order   
	DEALLOCATE db_Order       

    
	IF @bSend = 1
	BEGIN
		set @EmailMessage = @EmailMessage +  @2DayDetail + @EmailMessage2 +  @7DayDetail      
	
		Execute EmailQueueAdd 'Cart - Email Notification of Cart Items needing email'	  
		  ,@EmailMessage
		  ,0
		  ,'Cart Notice'
		  ,'scotth@eplantsource.com'
		  ,'suzannem@eplantsource.com'
		  ,''
		  ,''
		  ,'00000000-0000-0000-0000-000000000000'
		  ,0
		  ,0
		  ,0



	END
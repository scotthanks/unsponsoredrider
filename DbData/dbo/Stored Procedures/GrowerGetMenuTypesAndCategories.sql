﻿






CREATE PROCEDURE [dbo].[GrowerGetMenuTypesAndCategories]
	@EMail AS nvarchar(70),
	@SellerCode AS nvarchar(10)
AS
	
	Declare @UseDummenPriceMethod bit

	Set @UseDummenPriceMethod  = (select UseDummenPriceMethod from Person p
									Join Grower g on p.GrowerGuid = g.Guid
									where p.Email = @EMail)

	IF @SellerCode = 'EPS'
	BEGIN
	
		SELECT
			pv.ProductFormCategoryGuid,
			pv.ProductFormCategoryCode,
			pfc.Name as ProductFormCategoryName,
			pv.ProgramTypeGuid,
			pv.ProgramTypeCode,
			pt.Name as ProgramTypeName
		FROM ProductView pv
		Join ProductFormCategory pfc on pv.ProductFormCategoryGuid = pfc.Guid
		Join ProgramType pt on pv.ProgramTypeGuid = pt.Guid
		Join Program pr on pv.ProgramGuid = pr.Guid
		Join Seller s on pr.SellerGuid = s.Guid
		WHERE 
		s.Code = @SellerCode
		GROUP BY
			pv.ProductFormCategoryGuid,
			pv.ProductFormCategoryCode,
			pfc.Name,
			pv.ProgramTypeGuid,
			pv.ProgramTypeCode,
			pt.Name
		ORDER BY 
			pt.Name,
			pfc.Name
	END
	ELSE 
	BEGIN
		
		SELECT
			pv.ProductFormCategoryGuid,
			pv.ProductFormCategoryCode,
			pfc.Name,
			pv.ProgramTypeGuid,
			pv.ProgramTypeCode,
			pt.Name
		FROM GrowerProductProgramSeasonPrice gps
		Join ProductView pv on gps.ProductGuid = pv.ProductGuid
		Join ProductFormCategory pfc on pv.ProductFormCategoryGuid = pfc.Guid
		Join ProgramType pt on pv.ProgramTypeGuid = pt.Guid
		Join Program pr on pv.ProgramGuid = pr.Guid
		Join Seller s on pr.SellerGuid = s.Guid
		--Join GrowerProgram gp on gp.ProgramGuid = pr.Guid
		Join Grower g on gps.GrowerGuid = g.Guid
		Join Person p on g.Guid = p.GrowerGuid
		WHERE 
			s.Code = @SellerCode
			AND p.Email = @EMail
		GROUP BY
			pv.ProductFormCategoryGuid,
			pv.ProductFormCategoryCode,
			pfc.Name,
			pv.ProgramTypeGuid,
			pv.ProgramTypeCode,
			pt.Name
		ORDER BY 
			pt.Name,
			pfc.Name
	END
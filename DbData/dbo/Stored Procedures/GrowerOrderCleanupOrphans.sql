﻿





CREATE procedure [dbo].[GrowerOrderCleanupOrphans]
	@GrowerGuid AS UNIQUEIDENTIFIER = NULL,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER = NULL
AS
	DELETE SupplierOrderFee
	FROM SupplierOrderFee
		INNER JOIN SupplierOrder so ON SupplierOrderFee.SupplierOrderGuid = so.Guid
		INNER JOIN GrowerOrder o on so.GrowerOrderGuid = o.Guid
	WHERE
		ISNULL(@GrowerGuid, o.GrowerGuid) = o.GrowerGuid AND
		ISNULL(@GrowerOrderGuid, so.GrowerOrderGuid) = so.GrowerOrderGuid AND
		so.Guid NOT IN
		(
			SELECT DISTINCT(SupplierOrderGuid) 
			FROM OrderLine ol
			Join SupplierOrder so on ol.SupplierOrderGuid= so.Guid
			Join GrowerOrder o on so.GrowerOrderGuid= o.Guid
			WHERE
				ISNULL(@GrowerGuid, o.GrowerGuid) = o.GrowerGuid AND
				ISNULL(@GrowerOrderGuid, so.GrowerOrderGuid) = so.GrowerOrderGuid
		)

	DELETE SupplierOrder
	FROM SupplierOrder
		INNER JOIN SupplierOrder so ON SupplierOrder.Guid = so.Guid
		INNER JOIN GrowerOrder o on so.GrowerOrderGuid = o.Guid
	WHERE
		ISNULL(@GrowerGuid, o.GrowerGuid) = o.GrowerGuid AND
		ISNULL(@GrowerOrderGuid, so.GrowerOrderGuid) = so.GrowerOrderGuid AND
		so.Guid NOT IN
		(
			SELECT DISTINCT(SupplierOrderGuid) 
			FROM OrderLine ol
			Join SupplierOrder so on ol.SupplierOrderGuid= so.Guid
			Join GrowerOrder o on so.GrowerOrderGuid= o.Guid
			WHERE
				ISNULL(@GrowerGuid, o.GrowerGuid) = o.GrowerGuid AND
				ISNULL(@GrowerOrderGuid, so.GrowerOrderGuid) = so.GrowerOrderGuid
		)


	DELETE GrowerOrderFee
	FROM GrowerOrderFee
		INNER JOIN GrowerOrder o
			ON GrowerOrderFee.GrowerOrderGuid = o.Guid
	WHERE
		ISNULL(@GrowerGuid, o.GrowerGuid) = o.GrowerGuid AND
		ISNULL(@GrowerOrderGuid, o.Guid) = o.Guid AND
		o.Guid NOT IN
		(
			SELECT DISTINCT(GrowerOrderGuid) 
			FROM SupplierOrder so
			Join GrowerOrder o on so.GrowerOrderGuid= o.Guid
			WHERE
				ISNULL(@GrowerGuid, o.GrowerGuid) = o.GrowerGuid AND
				ISNULL(@GrowerOrderGuid, so.GrowerOrderGuid) = so.GrowerOrderGuid
		)
	
	DELETE GrowerOrderHistory
	FROM GrowerOrderHistory
		INNER JOIN GrowerOrder o
			ON GrowerOrderHistory.GrowerOrderGuid = o.Guid
	WHERE
		ISNULL(@GrowerGuid, o.GrowerGuid) = o.GrowerGuid AND
		ISNULL(@GrowerOrderGuid, o.Guid) = o.Guid AND
		o.Guid NOT IN
		(
			SELECT DISTINCT(GrowerOrderGuid) 
			FROM SupplierOrder so
			Join GrowerOrder o on so.GrowerOrderGuid= o.Guid
			WHERE
				ISNULL(@GrowerGuid, o.GrowerGuid) = o.GrowerGuid AND
				ISNULL(@GrowerOrderGuid, so.GrowerOrderGuid) = so.GrowerOrderGuid
		)
	


	DELETE GrowerOrder
	FROM GrowerOrder
		INNER JOIN GrowerOrder o
			ON GrowerOrder.Guid = o.Guid
	WHERE
		ISNULL(@GrowerGuid, o.GrowerGuid) = o.GrowerGuid AND
		ISNULL(@GrowerOrderGuid, o.Guid) = o.Guid AND
		o.Guid NOT IN
		(
			SELECT DISTINCT(GrowerOrderGuid) 
			FROM SupplierOrder so
			Join GrowerOrder o on so.GrowerOrderGuid= o.Guid
			WHERE
				ISNULL(@GrowerGuid, o.GrowerGuid) = o.GrowerGuid AND
				ISNULL(@GrowerOrderGuid, so.GrowerOrderGuid) = so.GrowerOrderGuid
		)
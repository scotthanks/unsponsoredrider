﻿




CREATE PROCEDURE [dbo].[ProgramsGet]
	
AS
		Select 
	
		p.Code,
		p.Name,
		p.DateDeactivated,
		pt.Name as ProgramType,
		s.Name as Supplier
		
		from 
		Program p
		Join Supplier s on p.SupplierGuid = s.Guid
		Join ProgramType pt on p.ProgramTypeGuid = pt.Guid

		ORDER BY p.Name
﻿


CREATE PROCEDURE [dbo].[AvailabilityUploadAddMessage]
	 @JobNumber as bigint,
	 @MessageType as nvarchar(20),
	 @MessageState as nvarchar(20),
	 @Message as nvarchar(200)
	 

AS
		Insert into ReportedAvailabilityUploadJobMessage values(
		NewID(),
		(select Guid from ReportedAvailabilityUploadJob where ID = @JobNumber),
		getdate(),
		@MessageType,
		@MessageState,
		@Message
		)
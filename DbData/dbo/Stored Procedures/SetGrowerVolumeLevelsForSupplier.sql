﻿

CREATE procedure [dbo].[SetGrowerVolumeLevelsForSupplier]
	@GrowerGuid AS UNIQUEIDENTIFIER,
	@SupplierGuid AS UNIQUEIDENTIFIER,
	@LevelType AS INT,  --1 = Estimated, 2 = Actual, 3 = Quoted --TO DO
	@LevelNumber AS INT
AS
	
	--First Update the existing Records
	Update GrowerVolume
	Set QuotedVolumeLevelGuid = vl.Guid
	FROM GrowerVolume gv 
	JOIN ProgramSeason ps on gv.ProgramSeasonGuid = ps.Guid
	Join Program p on ps.ProgramGuid = p.Guid
	Join VolumeLevel vl on vl.ProgramSeasonGuid = ps.guid 
	Where GrowerGUID = @GrowerGuid
		and vl.LevelNumber = @LevelNumber
		and p.Supplierguid = @SupplierGuid
		
	
	
	--Now insert the new records
	Insert into GrowerVolume
	Select newid(),null,@GrowerGuid,ps.Guid,NULL, NULL,vl.Guid
	--,ps.Name,vl.code
	From ProgramSeason ps
	Join Program p on ps.ProgramGuid = p.Guid
	Join VolumeLevel vl on vl.ProgramSeasonGuid = ps.guid  
	Where 
	p.SupplierGuid = @SupplierGuid AND
	vl.LevelNumber = @LevelNumber AND
	ps.Guid not in (select ProgramSeasonGuid 
					From GrowerVolume gv
					Where gv.GrowerGuid = @GrowerGuid )
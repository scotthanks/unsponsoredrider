﻿


CREATE PROCEDURE [dbo].[SupplierOrderDeleteFee]
	@SupplierOrderFeeGuid AS UNIQUEIDENTIFIER

AS

	DELETE SupplierOrderFee
    WHERE Guid = @SupplierOrderFeeGuid
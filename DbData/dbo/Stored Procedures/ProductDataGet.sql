﻿

CREATE PROCEDURE [dbo].[ProductDataGet]
	@ProgramTypeCode AS NCHAR(50) = null,
	@ProductFormCategoryCode AS NCHAR(50) = null,
	@SupplierCodeList AS NVARCHAR(500) = null,
	@GeneticOwnerCodeList AS NVARCHAR(500) = null,
	@SpeciesCodeList AS NVARCHAR(500) = null
AS
	IF @ProgramTypeCode Like '%*%'
		SET @ProgramTypeCode = NULL

	IF @ProductFormCategoryCode Like '%*%'
		SET @ProductFormCategoryCode = NULL

	IF @SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%*%'
		SET @SupplierCodeList = NULL
	ELSE
		SET @SupplierCodeList = ',' + @SupplierCodeList + ','

	IF @GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%*%'
		SET @GeneticOwnerCodeList = NULL
	ELSE
		SET @GeneticOwnerCodeList = ',' + @GeneticOwnerCodeList + ','

	IF @SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%*%'
		SET @SpeciesCodeList = NULL
	ELSE
		SET @SpeciesCodeList = ',' + @SpeciesCodeList + ','

	DECLARE @LocalTempProductData TABLE
	(
		ProgramTypeGuid UNIQUEIDENTIFIER,
		ProductFormCategoryGuid UNIQUEIDENTIFIER,
		ProductFormGuid UNIQUEIDENTIFIER,
		SpeciesGuid UNIQUEIDENTIFIER,
		SupplierGuid UNIQUEIDENTIFIER,
		VarietyGuid UNIQUEIDENTIFIER,
		ProgramGuid UNIQUEIDENTIFIER,
		ProductGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalTempProductData
	SELECT
		ProgramTypeGuid,
		ProductFormCategoryGuid,
		ProductFormGuid,
		SpeciesGuid,
		SupplierGuid,
		VarietyGuid,
		ProgramGuid,
		ProductGuid
	FROM ProductView
	WHERE
		(@ProgramTypeCode IS NULL OR ProgramTypeCode=@ProgramTypeCode) AND
		(@ProductFormCategoryCode IS NULL OR ProductFormCategoryCode=@ProductFormCategoryCode) AND
		(@SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%,' + RTRIM(SupplierCode) + ',%') AND
		(@GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%,' + RTRIM(GeneticOwnerCode) + ',%') AND
		(@SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%,' + RTRIM(SpeciesCode) + ',%')
	GROUP BY
		ProgramTypeGuid,
		ProductFormCategoryGuid,
		ProductFormGuid,
		SpeciesGuid,
		SupplierGuid,
		VarietyGuid,
		ProgramGuid,
		ProductGuid

	SELECT ProgramType.*
	FROM ProgramType
	WHERE ProgramType.Guid IN
	(SELECT DISTINCT(ProgramTypeGuid) FROM @LocalTempProductData)

	SELECT ProductFormCategory.*
	FROM ProductFormCategory
	WHERE ProductFormCategory.Guid IN
	(SELECT DISTINCT(ProductFormCategoryGuid) FROM @LocalTempProductData)

	SELECT ProductForm.*
	FROM ProductForm
	WHERE ProductForm.Guid IN
	(SELECT DISTINCT(ProductFormGuid) FROM @LocalTempProductData)

	SELECT Species.*
	FROM Species
	WHERE Species.Guid IN
	(SELECT DISTINCT(SpeciesGuid) FROM @LocalTempProductData)

	SELECT Supplier.*
	FROM Supplier
	WHERE Supplier.Guid IN
	(SELECT DISTINCT(SupplierGuid) FROM @LocalTempProductData)

	SELECT Variety.*
	FROM Variety
	WHERE Variety.Guid IN
	(SELECT DISTINCT(VarietyGuid) FROM @LocalTempProductData)

	SELECT Program.*
	FROM Program
	WHERE Program.Guid IN
	(SELECT DISTINCT(ProgramGuid) FROM @LocalTempProductData)

	SELECT Product.*
	FROM Product
	WHERE Product.Guid IN
	(SELECT DISTINCT(ProductGuid) FROM @LocalTempProductData)
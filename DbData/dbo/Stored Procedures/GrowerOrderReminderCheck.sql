﻿






CREATE PROCEDURE [dbo].[GrowerOrderReminderCheck]
	
AS
	Delete from GrowerOrderReminder
	where orderlineGuid not in 
	(
	select ol.Guid
	from Orderline ol
	Join Lookup ols on ol.OrderLineStatusLookupGuid = ols.guid
	Where ols.path = 'Code/OrderLineStatus/Pending'
	
	)

	Insert into GrowerOrderReminder
	Select newid(),so.GrowerOrderGuid,ol.guid,0,0
	From Orderline ol
	Join SupplierOrder so on ol.SupplierOrderGuid = so.guid
	Join Lookup ols on ol.OrderLineStatusLookupGuid = ols.guid
	Where ols.path = 'Code/OrderLineStatus/Pending'
	and ol.Guid not in 
		(Select orderLineguid from GrowerOrderReminder)
	
	--select * from GrowerOrderReminder
	--select * from lookup where path like '%orderlinestatus%'
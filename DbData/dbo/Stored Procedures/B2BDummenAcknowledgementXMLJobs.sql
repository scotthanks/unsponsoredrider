﻿




CREATE PROCEDURE [dbo].[B2BDummenAcknowledgementXMLJobs]

AS


DECLARE @UploadID INT
DECLARE @DataProcessing CURSOR
	SET @DataProcessing = CURSOR FOR
		SELECT ID
		FROM epsb2bprod.dbo.B2BIntegrationXML 
		where B2BStatus = 'Data Received'
		and  B2BType = 'Acknowledgement'
		and CommunicationDirection = 'In'
		ORDER BY ID
	OPEN @DataProcessing
	
	FETCH NEXT
	FROM @DataProcessing INTO @UploadID
	
	WHILE @@FETCH_STATUS = 0
	
	BEGIN
		--print 'Starting job: ' + cast(@UploadID as varchar(10))
		exec B2BDummenAcknowledgementXMLJob @UploadID,0
	
		FETCH NEXT
		FROM @DataProcessing INTO @UploadID
	END

CLOSE @DataProcessing
DEALLOCATE @DataProcessing
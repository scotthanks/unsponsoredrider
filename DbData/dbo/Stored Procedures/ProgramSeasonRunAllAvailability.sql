﻿



CREATE PROCEDURE [dbo].[ProgramSeasonRunAllAvailability]
	
	
AS
	Declare @sText nvarchar(2000)
	Declare @RC integer

	set @sText = 'Start Running all Seasons'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'
	

	DECLARE @ProgramSeasonCode nVarchar(50)
	DECLARE @getSeasons CURSOR
	SET @getSeasons = CURSOR FOR
	SELECT Code
	FROM ProgramSeason
	WHERE RunAvailabilityUpdate  = 1
	Order by Code
	OPEN @getSeasons
	FETCH NEXT
	FROM @getSeasons INTO @ProgramSeasonCode
	WHILE @@FETCH_STATUS = 0
		BEGIN
			
			Exec ProgramSeasonRunAvailability @ProgramSeasonCode
			
			FETCH NEXT
			FROM @getSeasons INTO @ProgramSeasonCode
		END
	CLOSE @getSeasons
	DEALLOCATE @getSeasons


 
    set @sText = 'Done Running all Seasons'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'   

--Now clear out the temp table
	truncate table [dbo].[ReportedAvailabilityUpdate]
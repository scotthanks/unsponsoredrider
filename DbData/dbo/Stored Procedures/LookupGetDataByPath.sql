﻿

CREATE procedure [dbo].[LookupGetDataByPath]
	@Expression AS NVARCHAR(200),
	@Guid AS UNIQUEIDENTIFIER OUT,
	@Path AS NVARCHAR(200) = NULL OUT,
	@Count AS INTEGER = NULL OUT,
	@ReturnRecordset AS BIT = 1
AS
	IF @Expression IS NOT NULL
		SET @Expression = LTRIM(RTRIM(@Expression))

	IF @Expression IS NULL OR @Expression = ''
		SET @Expression = '*'

	SET @Expression = REPLACE( @Expression, '*', '%')

	SELECT @Count=COUNT(*),@Guid=Guid FROM Lookup
	WHERE Path LIKE @Expression
	GROUP BY Guid

	IF @ReturnRecordSet = 1
		SELECT TOP 1 @Guid=Guid, @Path=Path FROM Lookup
		WHERE Path LIKE @Expression
		ORDER BY Path
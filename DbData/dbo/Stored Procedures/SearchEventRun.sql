﻿





CREATE procedure [dbo].[SearchEventRun]
	@theSearchTerm AS nVarchar(200),
	@UserGuid AS uniqueidentifier,
	@GetOnlyPage1 AS bit
AS

	Declare @SearchTermGuid uniqueidentifier
	set @SearchTermGuid = (select guid from SearchTerm where SearchString = @theSearchTerm)

	If len(@theSearchTerm) >2
		BEGIN
			If @UserGuid = '00000000-0000-0000-0000-000000000000' 
				Insert into SearchEvent values (newid(),@theSearchTerm,getdate(),NULL)
			Else
				Insert into SearchEvent values (newid(),@theSearchTerm,getdate(),@UserGuid)

			IF @SearchTermGuid is null
			BEGIN
				Insert into SearchTerm values(newid(),null,@theSearchTerm,'',0,0)
				
				set @SearchTermGuid = (select guid from SearchTerm where SearchString = @theSearchTerm)
				
				exec SearchResultDataPopulate @SearchTermGuid,@GetOnlyPage1,0
			END
		
			select SearchString,RelatedSearches,ResultCount
			From SearchTerm
			Where SearchString = @theSearchTerm

			Select top 10 stsr.Guid,SearchTermGuid,SortOrder,RelevanceCategory,
			ResultType,ResultCode,ResultName,HeaderText,SubHeaderText,
			ImageURI,CatalogProductformCategoryCode,CatalogProgramTypeCode,
			Program1Name,Program1ProductformCategoryCode,Program1ProgramTypeCode,Program1ProductCount,
			Program2Name,Program2ProductformCategoryCode,Program2ProgramTypeCode,Program2ProductCount,
			Program3Name,Program3ProductformCategoryCode,Program3ProgramTypeCode,Program3ProductCount,
			Species1Code,Species1Name,Species1ProgramTypeCode,Species1ProductformCategoryCode,
			Species2Code,Species2Name,Species2ProgramTypeCode,Species2ProductformCategoryCode,
			Species3Code,Species3Name,Species3ProgramTypeCode,Species3ProductformCategoryCode

			From SearchTermSearchResult stsr
			JOIN SearchResult sr on stsr.SearchResultGuid = sr.Guid
			Where SearchTermGuid = @SearchTermGuid
			Order by RelevanceCategory,SortOrder,ResultName
		END
	ELSE
		Select 'Please enter more characters to search'
﻿





CREATE PROCEDURE [dbo].[DummenPricingUploadJobGetNotLoaded]
	 @JobNumber as bigint
	
	 

AS
	SELECT 
	growerName,programseasoncode,supplieridentifier
	From GrowerProductProgramSeasonPriceUpload
	WHERE RunNumber =@JobNumber
			and (GrowerGuid is null
			OR ProgramSeasonGuid is null
			OR ProductGuid is null)


	ORDER BY GrowerProductProgramSeasonPriceUpload.supplieridentifier
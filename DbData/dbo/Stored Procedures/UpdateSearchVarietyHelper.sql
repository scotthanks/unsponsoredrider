﻿








CREATE procedure [dbo].[UpdateSearchVarietyHelper]
	
AS
	Insert into SearchVarietyHelper
	select newid(),z.VarietyGuid,'17|2014',z.ProductFormCategoryGuid
	From (
		SELECT DISTINCT v.Guid as VarietyGuid,pf.ProductFormCategoryGuid
		from Variety v
		Join Product p on v.Guid = p.VarietyGuid
		Join ProductForm pf on pf.Guid = p.ProductFormGuid
		where Cast(v.Guid as varchar(36)) + Cast(pf.ProductFormCategoryGuid as varchar(36))  
			not in (select Cast(VarietyGuid as varchar(36)) + Cast(ProductFormCategoryGuid as varchar(36)) 
					 from SearchVarietyHelper
					 )
		and v.DateDeactivated is null
		and p.DateDeactivated is null
		) z

	Update SearchVarietyHelper
	set 
	--select 
	ShipWeekString = right ('00'+ltrim(str( w.Week )),2 ) + '|' + cast(w.year as varchar(4))
	from SearchVarietyHelper h
	Join Variety v on h.VarietyGuid = v.Guid
	
	--Join Product p1 on v.Guid = p1.VarietyGuid
	--Join ProductForm pf1 on pf1.Guid = p1.ProductFormGuid
	Join (
		Select v2.Guid as VarietyGuid,pf2.ProductFormCategoryGuid,Min(w2.ContinuousWeekNumber) as ContinuousWeekNumber
		From Variety v2
		Join Product p2 on p2.VarietyGuid = v2.Guid
		Join ProductForm pf2 on p2.ProductFormGuid = pf2.Guid
		Join Program pr on p2.ProgramGuid = pr.Guid
		Join reportedavailability r2 on p2.Guid = r2.ProductGuid
		Join Lookup r2l on r2.AvailabilityTypeLookupGuid = r2l.Guid
		Join shipweek w2 on r2.shipweekguid =w2.guid
		where ((r2l.code ='OPEN') or ( r2l.code ='AVAIL' AND r2.Qty > 0 ))
		and pr.DateDeactivated is Null
		and p2.DateDeactivated is Null
		and v2.DateDeactivated is Null
		--and v2.guid = '7ACEA2A2-BFD4-4EFF-B8D7-DDBFC39AB642'
		Group By v2.guid,pf2.ProductFormCategoryGuid
	) v3 on v.Guid = v3.VarietyGuid AND v3.ProductFormCategoryGuid = h.ProductFormCategoryGuid
	join shipweek w on w.ContinuousWeekNumber = v3.ContinuousWeekNumber
	--where v3.varietyguid = '7ACEA2A2-BFD4-4EFF-B8D7-DDBFC39AB642'


	--select * from SearchVarietyHelper where varietyguid = '7ACEA2A2-BFD4-4EFF-B8D7-DDBFC39AB642'
﻿






CREATE PROCEDURE [dbo].[SupplierOrderAddUpdateFee]
	@SupplierOrderFeeGuid AS UNIQUEIDENTIFIER,
	@SupplierOrderGuid AS UNIQUEIDENTIFIER,
	@FeeTypeCode AS NVARCHAR(200) = '',
	@DiscountPercent AS DECIMAL(8,3),
	@FeeAmount AS	DECIMAL(8,2),
	@FeeStatusCode AS NVARCHAR(200) = '',
	@FeeDescription AS NVARCHAR(500) = '',
	@IsManual AS BIT

AS

	DECLARE @iCount INTEGER




	--PRINT 'hi'
	--PRINT 	'-' + CAST(@SupplierOrderFeeGuid AS NVARCHAR(100)) + '-'		
	IF @SupplierOrderFeeGuid = '00000000-0000-0000-0000-000000000000'
	BEGIN
		--PRINT 'insret here'
		INSERT INTO SupplierOrderfee VALUES(
		NEWID(),NULL,@SupplierOrderGuid,
		(SELECT guid FROM Lookup WHERE path = 'Code/SupplierOrderFeeType/' + @FeeTypeCode),
		COALESCE(@DiscountPercent * @FeeAmount,0),
		(SELECT guid FROM Lookup WHERE path = 'Code/SupplierOrderFeeStatus/' + @FeeStatusCode),
		@FeeDescription,
		@IsManual
		)	
		--PRINT 'done insret here'
		--select guid,code,name,path from Lookup where path like 'Code/GrowerOrderFeeType%'
		--Update lookup set code = 'EPSDiscount' where guid  = '7D9D0263-854C-40C4-91FE-D13EC065632C'
	END
	ELSE
	BEGIN
		UPDATE SupplierOrderfee
		SET 
			Amount = COALESCE(@DiscountPercent * @FeeAmount,0),
			FeeStatXLookupGuid = (SELECT guid FROM Lookup WHERE path = 'Code/SupplierOrderFeeStatus/' + @FeeStatusCode),
			FeeTypeLookupGuid = (SELECT guid FROM Lookup WHERE path = 'Code/SupplierOrderFeeType/' + @FeeTypeCode),
			FeeDescription = @FeeDescription,
			IsManual = @IsManual
		WHERE	Guid = @SupplierOrderFeeGuid
				 

	END
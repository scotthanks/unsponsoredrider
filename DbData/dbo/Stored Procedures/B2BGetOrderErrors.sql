﻿


CREATE PROCEDURE [dbo].[B2BGetOrderErrors]
	 

AS
	--select * from grower where name = 'Color Point'
	--select * from grower where guid = '341AAAC5-518F-492E-8C8B-FA21789A15E0'
   select B2BStatus,StatusMessage,
   g.Name as GrowerName,
   g.Email as GrowerEmail
   from epsb2bprod.dbo.B2BIntegrationXML b2b
   Join Grower g on b2b.GrowerGuid = g.Guid
   where b2bType  in ('Order Place', 'Order Update')
   and communicationdirection = 'in'
   and B2BStatus = 'Error'

	Update epsb2bprod.dbo.B2BIntegrationXML 
	set B2BStatus = 'Error Processed'
	where 
		b2bType  in ('Order Place', 'Order Update')
		and communicationdirection = 'in'
		and B2BStatus = 'Error'
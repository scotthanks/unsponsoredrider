﻿









CREATE PROCEDURE [dbo].[GrowerOrderResetProgramCategoryType]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER
AS
	
	
	
	Declare @ProgramTypeCount INT
	Declare @ProductFormCategoryCount INT
	
	--This was added to set the multiple types on the order
	set @ProgramTypeCount = 
		(
		Select count(Distinct pr.ProgramTypeGuid) 
		from SupplierOrder so
		Join OrderLine ol on so.Guid = ol.SupplierOrderGuid
		Join Lookup ols on ol.OrderLineStatusLookupGuid = ols.Guid
		Join Product p on ol.ProductGuid = p.Guid
		Join Program pr on p.ProgramGuid = pr.guid
		WHERE so.GrowerOrderGuid = @GrowerOrderGuid
		and ols.Name not in ('Not Yet In Cart','Cancelled'))
		--	and ols.Name not in ('Cancelled')
		
	If @ProgramTypeCount > 1
	BEGIN
		Update GrowerOrder 
		set ProgramTypeGuid = (select guid from ProgramType where code = 'MUL')
		WHERE Guid = @GrowerOrderGuid
			
	END
	ELSE
	BEGIN
	    If @ProgramTypeCount = 1
		BEGIN
			Update GrowerOrder 
			set ProgramTypeGuid = 
				(Select top 1 pr.ProgramTypeGuid
				from SupplierOrder so
				Join OrderLine ol on so.Guid = ol.SupplierOrderGuid
				Join Lookup ols on ol.OrderLineStatusLookupGuid = ols.Guid
				Join Product p on ol.ProductGuid = p.Guid
				Join Program pr on p.ProgramGuid = pr.guid
				WHERE so.GrowerOrderGuid = @GrowerOrderGuid
				and ols.Name not in ('Not Yet In Cart','Cancelled'))
				--	and ols.Name not in ('Cancelled')
			WHERE Guid = @GrowerOrderGuid
		END
	END

	--This was added to set the multiple forms on the order
	set @ProductFormCategoryCount = 
		(
		Select count(Distinct pr.ProductFormCategoryGuid) 
		from SupplierOrder so
		Join OrderLine ol on so.Guid = ol.SupplierOrderGuid
		Join Lookup ols on ol.OrderLineStatusLookupGuid = ols.Guid
		Join Product p on ol.ProductGuid = p.Guid
		Join Program pr on p.ProgramGuid = pr.guid
		WHERE so.GrowerOrderGuid = @GrowerOrderGuid
		and ols.Name not in ('Not Yet In Cart','Cancelled'))
		--	and ols.Name not in ('Cancelled')

		
	If @ProductFormCategoryCount > 1
	BEGIN
		Update GrowerOrder 
		set ProductFormCategoryGuid = (select guid from ProductFormCategory where code = 'MUL')
		WHERE Guid = @GrowerOrderGuid
			
	END
	ELSE
	BEGIN
		If @ProductFormCategoryCount > 1
		BEGIN
			Update GrowerOrder 
			set ProductFormCategoryGuid = 
				(Select Distinct pr.ProductFormCategoryGuid
				from SupplierOrder so
				Join OrderLine ol on so.Guid = ol.SupplierOrderGuid
				Join Lookup ols on ol.OrderLineStatusLookupGuid = ols.Guid
				Join Product p on ol.ProductGuid = p.Guid
				Join Program pr on p.ProgramGuid = pr.guid
				WHERE so.GrowerOrderGuid = @GrowerOrderGuid
				and ols.Name not in ('Not Yet In Cart','Cancelled'))
			--	and ols.Name not in ('Cancelled')
			WHERE Guid = @GrowerOrderGuid
		END
	END
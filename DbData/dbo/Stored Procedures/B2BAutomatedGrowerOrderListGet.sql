﻿















CREATE PROCEDURE [dbo].[B2BAutomatedGrowerOrderListGet]
	  @Category as nvarchar(20) =''
	 

AS

	declare @IsB2BOrdering bit
	IF @Category = 'B2BGrower'
		set @IsB2BOrdering = 1
	ELSE
		set  @IsB2BOrdering = 0

	--select * from lookup where path like 'Code/OrderLineStatus/%'
	;With  inc (GrowerOrderGuid)
	AS
	(
		Select Distinct olv.GrowerOrderGuid 
		From OrderLineViewShowInactiveProduct olv (nolock)
		Join growerOrder o (nolock) on olv.GrowerOrderGuid = o.Guid
		Join grower g (nolock) on o.GrowerGuid = g.Guid
		Where 
		OrderLineStatusLookupGuid in (select Guid from lookup where path in  ('Code/OrderLineStatus/SupplierEdit','Code/OrderLineStatus/SupplierCancelled','Code/OrderLineStatus/SupplierAdd','Code/OrderLineStatus/SupplierConfirmed'))
		AND o.CustomerPONO not like '%test%'
		AND o.Description not like '%test%'
		AND g.IsInternal = 0
		AND o.Guid not in (
			Select Distinct olv.GrowerOrderGuid 
			From OrderLineViewShowInactiveProduct olv (nolock)
			Join ProductForm pf on pf.Guid = olv.ProductFormGuid
			Where olv.QtyOrdered  % pf.SalesUnitQty  <> 0
		)
	)
	Select o.Guid as GrowerOrderGuid
	into #GrowerOrder
	From GrowerOrder o (nolock)
	Join inc on o.Guid = inc.GrowerOrderGuid
	Join GrowerOrderSummaryByShipWeekGroupByOrderView2 ov  (nolock) on o.Guid = ov.GrowerOrderGuid
	
	WHERE 
		1 = 1
		
		AND ov.LowestOrderLineStatusLookupCode not in ('PreCart','Pending') 
		AND o.Guid not in (
			Select olv.GrowerOrderGuid--,GrowerOrderno,linenumber,count(*) 
			From OrderLineViewShowInactiveProduct olv (nolock)
			Where LineNumber <> 0
			Group By GrowerOrderGuid,LineNumber
			Having  count(*) > 1
		)
		AND o.Guid not in (
			Select Distinct olv.GrowerOrderGuid 
			From OrderLineViewShowInactiveProduct olv (nolock)
			Where OrderLineStatusLookupGuid in (select Guid from lookup where path in  ('Code/OrderLineStatus/SupplierB2BError','Code/OrderLineStatus/GrowerB2BError'))
		)
		--select path from lookup where path like 'Code/OrderLineStatus/%'
		--SELECT 38 / 5 AS Integer, 38 % 5 AS Remainder ; 
	;With  zz(GrowerOrderGuid,B2BCount)
	AS
	(
	SELECT  o.Guid as GrowerOrderGuid,count(b2b.ID) as B2BCount
	FROM #GrowerOrder tgo
	Join GrowerOrder o (nolock) on tgo.GrowerOrderGuid = o.Guid
	left JOIN [epsB2BProd].[dbo].[B2BIntegrationXML] b2b (nolock) on b2b.OrderGuid = o.Guid 
	--and  b2b.CommunicationDirection = 'ToSeller'
	
	Group by o.Guid
	)

	Select top 1000
	o.Guid as OrderGuid
	,g.Code as GrowerCode
	,g.Name as GrowerName
	,o.CustomerPoNo
	,rtrim(ov.GrowerOrderStatusLookupCode) as GrowerOrderStatusCode
	,case when rtrim(ov.GrowerOrderStatusLookupCode) in ('Invoiced','Paid') 
		then ov.GrowerOrderStatusLookupName 
		else LowestOrderLineStatusLookupName end as GrowerOrderStatusName
	,ov.InvoiceNo
	,case when rtrim(ov.GrowerOrderStatusLookupCode) in ('Invoiced','Paid') 
		then ov.GrowerOrderStatusLookupCode else ov.LowestOrderLineStatusLookupCode 
		end as OrderStatus
	,ov.LowestOrderLineStatusLookupCode as LowestOrderLineStatusCode
	,o.[Description] as OrderDescription
	,o.OrderNo as OrderNo
	,ov.QtyOrderedCount as OrderQty
	,ov.QtyOrderedCount as OrderQty
	,p.FirstName + ' ' + p.LastName as PersonWhoPlacedOrder
	,pfc.Name as ProductFormCategoryName
	,pt.Name as ProgramTypeName
	,substring(ov.ShipWeekCode,5,2) + '|' + substring(ov.ShipWeekCode,1,4) as ShipWeekString
	,zz.B2BCount
	,g.IsBadGrower
	from GrowerOrderSummaryByShipWeekGroupByOrderView2 ov
	Join GrowerOrder o on ov.GrowerOrderGuid = o.Guid
	Join Grower g on ov.GrowerGuid = g.Guid
	Join Person p on ov.PersonGuid = p.Guid
	Join ProductFormCategory pfc on ov.ProductFormCategoryGuid = pfc.Guid
	Join ProgramType pt on ov.ProgramTypeGuid = pt.Guid
	Join ShipWeek sw on ov.ShipWeekGuid = sw.Guid
	Join zz on o.Guid = zz.GrowerOrderGuid

	WHERE 
		1 = 1
		AND case when @IsB2BOrdering = 1 and  g.B2BOrderURL != '' then 1 when @IsB2BOrdering =0 and g.B2BOrderURL = '' then 1 else 0 end = 1
		AND ov.LowestOrderLineStatusLookupCode not in ('PreCart','Pending') 
		

	ORDER BY o.OrderNo desc




	


		
	Drop Table #GrowerOrder
﻿



CREATE PROCEDURE [dbo].[ProgramValidationGet]
	@ProgramSeasonCode AS nvarchar(20)
AS
SELECT pr.code as ProgramCode
,pr.name as ProgramName
,ps.code as SeasonCode
,ps.name as SeasonName
,p.code as ProductCode
,rtrim(p.SupplierIdentifier) as SupplierID
,rtrim(p.supplierdescription) as SupplierDescription
,v.Code as VarietyCode
,v.Name as VarietyName
,s.Name as Species
,pf.Code as ProductFormCode
,pf.name as ProductFormName
,v.name + ' - ' + cast(pf.salesunitqty  as varchar(6)) as ProductDescription
,p.ProductionLeadTimeWeeks as LeadTime
,vl.LevelNumber as LevelNumber
,pg.code as PriceGroup
,pg.TagCost
,pri.CuttingCost
,pri.RoyaltyCost
,pri.FreightCost
,ISNULL(pri.RegularCost,0) as Cost
,cast(ISNULL(pri.RegularCost + (pri.RegularCost * pri.RegularMUPercent/100),0) as decimal(8,4)) as Price
,ISNULL(pri.EODCost,0) as EODCost
,cast(ISNULL(pri.EODCost + (pri.EODCost * pri.EODMUPercent/100),0) as decimal(8,4))  as EODPrice


FROM Product p
Join Program pr on p.Programguid = pr.guid
Join ProgramSeason ps on ps.Programguid = pr.guid
Join VolumeLevel vl on ps.Guid = vl.ProgramSeasonGuid
	--AND vl.LevelNumber = 1
Join ProductForm pf on p.productformguid = pf.guid
Join ProductPriceGroupProgramSeason ppg on ps.guid = ppg.ProgramSeasonGuid and p.Guid = ppg.ProductGuid
Join PriceGroup pg on ppg.pricegroupguid = pg.guid
Join variety v on p.varietyguid = v.guid
Join species s on v.speciesguid = s.guid
Join Price pri on pri.PriceGroupGuid = pg.Guid
	AND pri.VolumeLevelGuid = vl.Guid
WHERE ps.code =  @ProgramSeasonCode 
AND p.DateDeactivated is null
ORDER BY vl.LevelNumber,s.name,v.name,pf.Name
﻿CREATE procedure dbo.PersonDataGet
	@PersonGuid AS UNIQUEIDENTIFIER
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('PersonGuid',@PersonGuid,1),
		0
	)

	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER	
	EXECUTE EventLogAdd
		@UserCode=@PersonGuid,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='PersonDataGet',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	DECLARE @LocalPersonData TABLE
	(
		PersonGuid UNIQUEIDENTIFIER,
		GrowerGuid UNIQUEIDENTIFIER,
		AddressGuid UNIQUEIDENTIFIER,
		StateGuid UNIQUEIDENTIFIER,
		CountryGuid UNIQUEIDENTIFIER,
		PersonTypeLookupGuid UNIQUEIDENTIFIER,
		GrowerTypeLookupGuid UNIQUEIDENTIFIER,
		PaymentTypeLookupGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalPersonData
	SELECT
		PersonGuid,
		GrowerGuid,
		AddressGuid,
		StateGuid,
		CountryGuid,
		PersonTypeLookupGuid,
		GrowerTypeLookupGuid,
		DefaultPaymentTypeLookupGuid
	FROM PersonGrowerView
	WHERE
		PersonGuid = @PersonGuid

	SELECT * FROM Person
	WHERE Guid IN (SELECT PersonGuid FROM @LocalPersonData)

	SELECT * FROM Grower
	WHERE Guid IN (SELECT GrowerGuid FROM @LocalPersonData)

	SELECT * FROM GrowerAddress
	WHERE GrowerGuid IN (SELECT GrowerGuid FROM @LocalPersonData)

	SELECT * FROM Address
	WHERE Guid IN (SELECT AddressGuid FROM @LocalPersonData)

	SELECT * FROM State
	WHERE Guid IN (SELECT StateGuid FROM @LocalPersonData)

	SELECT * FROM Country
	WHERE Guid IN (SELECT CountryGuid FROM @LocalPersonData)

	SELECT * FROM Lookup
	WHERE Guid IN 
	(
		SELECT PersonTypeLookupGuid FROM @LocalPersonData
		UNION 
		SELECT GrowerTypeLookupGuid FROM @LocalPersonData 
		UNION 
		SELECT PaymentTypeLookupGuid FROM @LocalPersonData 
	)

	EXECUTE EventLogAdd
		@UserCode=@PersonGuid,
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='PersonDataGet',
		@ObjectGuid=@OriginalEventLogGuid
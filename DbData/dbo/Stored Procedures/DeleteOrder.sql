﻿




CREATE PROCEDURE [dbo].[DeleteOrder]
	@OrderNo AS varchar(50)
	
AS
	

declare @GrowerOrderID bigint
set @GrowerOrderID = (select ID from growerorder where orderno =@OrderNo) 
If @GrowerOrderID > 0
BEGIN

	delete from orderline where id in(select ol.id from Orderline ol  join supplierorder so
										on ol.supplierorderguid = so.guid join growerorder o on o.Guid = so.GrowerOrderguid
											where o.ID =  @GrowerOrderID)
	delete from supplierorderfee where supplierorderguid in (select so.guid from supplierorder so 
											join growerorder o on o.Guid = so.GrowerOrderguid
											where o.ID =  @GrowerOrderID)
	delete from supplierorder where guid in (select so.guid from supplierOrder so 
											join growerorder o on o.Guid = so.GrowerOrderguid
											where o.ID =  @GrowerOrderID)
	delete from growerorderfee where growerorderguid in (select o.guid from growerorder o where o.ID =  @GrowerOrderID)
	delete from growerorderHistory where growerorderguid in (select o.guid from growerorder o where o.ID =  @GrowerOrderID)
	delete from growerorder where id = @GrowerOrderID	
END
Else
	print 'Nothing Deleted'
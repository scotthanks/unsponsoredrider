﻿



CREATE PROCEDURE [dbo].[GrowerVolumeUpdateQuotes]
	
	
AS
	Declare @sText nvarchar(2000)
	Declare @RC integer

	set @sText = 'Start Running GrowerVolumeUpdateQuotes'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','GrowerVolumeUpdateQuotes','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'
	
--some are sneaking in with NULL and these are invalid
Delete from GrowerVolume where guid in ( 
	select Guid from GrowerVolume gv1
	where coalesce(gv1.quotedvolumelevelguid,gv1.ActualVolumeLevelGuid,gv1.EstimatedVolumeLevelGuid) is null
)


Insert into GrowerVolume 
Select newid(),null,GrowerSeason.GrowerGuid,GrowerSeason.ProgramSeasonGuid,NULL,NULL,
(select guid from volumelevel vl
 where levelnumber = GrowerSupplier.LevelNumber and ProgramSeasonGuid =GrowerSeason.ProgramSeasonGuid )
 --,GrowerSupplier.LevelNumber
 --,GrowerSeason.GrowerName
-- ,GrowerSeason.ProgramSeasonName
--Select GrowerSeason.GrowerName,GrowerSeason.SupplierName,GrowerSeason.ProgramSeasonName,GrowerSupplier.LevelNumber
From
 (
	Select g.Guid as GrowerGuid,g.Name as GrowerName,Season.SupplierGuid,Season.SupplierName,Season.ProgramSeasonGuid,Season.ProgramSeasonName
	From Grower g,
 
	(SELECT
	ps.Name as ProgramSeasonName,ps.Guid as ProgramSeasonGuid,s.name as SupplierName,s.Guid as SupplierGuid
	FROM programSeason ps 
	Join Program pr on ps.programGuid = pr.guid
	Join Supplier s on pr.SupplierGuid = s.Guid
	Where ps.StartDate > dateadd(yy,-1,getdate()) 
	AND pr.dateDeactivated is NULL
	) Season 
) GrowerSeason
Join 
	(
		select g1.Name as GrowerName,g1.Guid as GrowerGuid,s1.Name as SupplierName,s1.Guid as SupplierGuid,Max(v1.levelnumber) as LevelNumber
		--,ps.Name,gv.*
		from Grower g1
		Join GrowerVolume gv1 on g1.guid = gv1.growerguid
		Join programSeason ps1 on gv1.ProgramSeasonguid = ps1.Guid
		Join Program pr1 on ps1.programGuid = pr1.guid
		Join Supplier s1 on pr1.SupplierGuid = s1.Guid
		Join volumelevel v1 on v1.guid = coalesce(gv1.quotedvolumelevelguid,gv1.ActualVolumeLevelGuid,gv1.EstimatedVolumeLevelGuid)
		Where ps1.StartDate > dateadd(yy,-1,getdate())
		Group by g1.Name,g1.Guid,s1.Name,s1.Guid
	) GrowerSupplier 
		on GrowerSeason.GrowerGuid = GrowerSupplier.GrowerGuid AND GrowerSeason.SupplierGuid = GrowerSupplier.SupplierGuid
	

WHERE  
	cast(GrowerSeason.GrowerGuid as varchar(36)) + cast(GrowerSeason.ProgramSeasonGuid as varchar(36)) 
	NOT IN 
	(
		Select cast(g2.Guid as varchar(36)) + cast(gv2.ProgramSeasonGuid as varchar(36))
		from Grower g2
		Join GrowerVolume gv2 on g2.guid = gv2.growerguid 
	)

	

 
    set @sText = 'Done Running GrowerVolumeUpdateQuotes'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','GrowerVolumeUpdateQuotes','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'
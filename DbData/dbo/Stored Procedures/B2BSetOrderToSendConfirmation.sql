﻿




CREATE PROCEDURE [dbo].[B2BSetOrderToSendConfirmation]
	 
@OrderGuid uniqueidentifier

AS
Declare @jobnumber bigint

	select @jobnumber = Max(Id) 
	From epsb2bprod.dbo.B2BIntegrationXML 
	where
	OrderGuid = @OrderGuid
	and b2bType  in ('Order Place','Order Update')
	and communicationdirection = 'in'
	
	Update epsb2bprod.dbo.B2BIntegrationXML 
	set B2BStatus = 'Sending Confirmation'
	where 
		id = @jobnumber
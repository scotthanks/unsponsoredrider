﻿



CREATE PROCEDURE [dbo].[SupplierOrderReprice]
	@SupplierOrderGuid AS UNIQUEIDENTIFIER
	
AS
	

Declare @OrderNo varchar(50)
Declare @SeasonCode varchar(50)
Declare @EOD AS Bit
Declare @RollingWeeksEOD as int
Declare @EODDate as DateTime
Declare @DateEntered as DateTime
Declare @ShipWeekMondayDate as DateTime

select  
   @SeasonCode = Season.Code,
   @RollingWeeksEOD = Season.RollingWeeksEOD,
   @EODDate = Season.EODDate,
   @DateEntered = Season.DateEntered,
   @ShipWeekMondayDate =Season.MondayDate
From
  (select distinct ps.Code, ps.RollingWeeksEOD,ps.EODDate,o.DateEntered,sw.MondayDate
	  From SupplierOrder so
	  Join OrderLine ol on so.guid = ol.SupplierOrderguid
	  Join Product p on ol.Productguid = p.guid
	  Join Program pr on p.ProgramGuid = pr.guid
	  Join ProgramSeason ps on ps.Programguid = pr.Guid
	  Join GrowerOrder o on o.guid = so.GrowerOrderGuid
	  join ShipWeek sw on o.ShipweekGuid = sw.guid
		And sw.MondayDate Between ps.StartDate and ps.EndDate
	  Where so.guid = @SupplierOrderGuid
	  ) Season


If @RollingWeeksEOD > 0 
	BEGIN
		if datediff(day,@DateEntered,@ShipWeekMondayDate)/7 >= @RollingWeeksEOD
			set @EOD = 1
		Else
			set @EOD = 0
	END
ELSE
	BEGIN
		If @DateEntered <= @EODDate 
			set @EOD = 1
		Else
			set @EOD = 0
	END

--Print  @SeasonCode + '--' + cast(@EODDate as varchar(30)) + '--' + 	cast(@RollingWeeksEOD as varchar(2)) + '--' + cast(@EOD as varchar(2)) 	+ cast(@DateEntered as varchar(30))

Create Table #Reprice (
OrderNo nvarchar(30),
SupplierOrderID bigint,
OrderLineID bigint,
GrowerGuid uniqueidentifier,
ProductGuid uniqueidentifier,
SupplierDescription nvarchar(100),
RegularCost Decimal(8,4),
RegularPrice Decimal(8,4),
EODCost Decimal(8,4),
EODPrice Decimal(8,4),
ActualPriceUsedGuid uniqueidentifier,
CurrentCost Decimal(8,4),
CurrentPrice Decimal(8,4),
ProgramSeasonCode nvarchar(20),
LevelNumber int,
GrowerVolumeID bigint
 )

--Start with Base Price
Insert into #Reprice (
OrderNo,SupplierOrderID,OrderLineID,GrowerGuid,ProductGuid,SupplierDescription,RegularCost,RegularPrice,
EODCost,EODPrice,ActualPriceUsedGuid,
CurrentCost,CurrentPrice,ProgramSeasonCode,LevelNumber,GrowerVolumeID)
select 
o.orderno,so.ID as SupplierOrderID,ol.ID as OrderLineID,o.GrowerGuid,p.guid,p.supplierdescription,
pr.RegularCost,pr.RegularCost + (pr.regularCost * pr.RegularMUPercent/100) as RegularPrice,
pr.EODCost,pr.EODCost + (pr.EODCost * pr.EODMUPercent/100) as EODPrice,pr.guid,
ol.ActualCost as CurrentCost,ol.ActualPrice as CurrentPrice,
@SeasonCode as ProgramSeasonCode,1,0 as GrowerVolumeID

from growerorder o
Join supplierorder so on o.guid = so.growerorderguid
Join orderline ol on so.guid = ol.supplierorderguid
Join product p on ol.productguid = p.guid
Join ProductSeasonBasePriceViewX pvx on pvx.ProductGuid = p.Guid
    AND pvx.ProgramSeasonGuid  = (select guid from ProgramSeason where code = @SeasonCode)
Join Price pr on pvx.PriceGuid = pr.guid		
where so.Guid = @SupplierOrderGuid  


--Now update based on grower volume
Update #Reprice
set  RegularCost = pr.RegularCost,
RegularPrice =  pr.RegularCost + (pr.regularCost * pr.RegularMUPercent/100),
EODCost = pr.EODCost,
EODPrice =  pr.EODCost + (pr.EODCost * pr.EODMUPercent/100),
ActualPriceUsedGuid = pr.guid,
LevelNumber = vl.LevelNumber,
GrowerVolumeID = gv.ID
From #Reprice r
JOIN ProductPriceGroupProgramSeason  ppgs ON r.ProductGuid = ppgs.ProductGuid
JOIN ProgramSeason ps ON ppgs.ProgramSeasonGuid = ps.Guid and ps.code = r.ProgramSeasonCode
JOIN PriceGroup pg ON ppgs.PriceGroupGuid = pg.Guid
JOIN Price pr ON ppgs.PriceGroupGuid = pr.PriceGroupGuid 
JOIN VolumeLevel vl ON pr.VolumeLevelGuid = vl.Guid AND
			ppgs.ProgramSeasonGuid = vl.ProgramSeasonGuid
JOIN GrowerVolume gv on r.growerguid = gv.growerguid
	AND ps.Guid = gv.ProgramSeasonGuid
	AND coalesce(gv.ActualVolumeLevelGuid,gv.EstimatedVolumeLevelGuid,gv.QuotedVolumeLevelGuid) = vl.guid

--Select * from #Reprice  order by orderno,SupplierOrderID,supplierdescription

--Update the Actual Table
Update Orderline
Set ActualCost = Case when @EOD = 1 then EODCost else r.RegularCost end,
ActualPrice = Case when @EOD = 1 then EODPrice else r.RegularPrice end,
ActualPriceUsedGuid = r.ActualPriceUsedGuid,
ActualCostUsedGuid = r.ActualPriceUsedGuid
From #Reprice r
Join Orderline ol on r.OrderlineID = ol.ID


--Drop the Table
Drop Table #Reprice
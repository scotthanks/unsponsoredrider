﻿CREATE procedure [dbo].[TripleAdd]
	@PredicateLookupCode AS nchar(50),
	@SubjectGuid AS uniqueidentifier,
	@ObjectGuid AS uniqueidentifier,
	@ReturnRecordset AS bit = 1,
	@TripleGuid AS uniqueidentifier = NULL OUT,
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NCHAR(56) = NULL
AS
	DECLARE @PredicateLookupGuid AS uniqueidentifier
	DECLARE @PathExpression AS NVARCHAR(200)

	SET @PathExpression = 'Predicate/' + LTRIM(RTRIM(@PredicateLookupCode))

	EXECUTE LookupGetDataByPath 
		@Expression=@PathExpression, 
		@Guid=@PredicateLookupGuid OUT

	IF @PredicateLookupGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid ,
				@UserCode=@UserCode ,
				@Comment='PredicateLookupCode was not found.' ,
				@LogTypeLookupCode='InvalidParameters' ,
				@ProcessLookupCode='TripleAdd',
				@SeverityLookupCode='Error'

			RETURN
		END

	IF @SubjectGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid ,
				@UserCode=@UserCode ,
				@Comment='SubjectGuid is required.' ,
				@LogTypeLookupCode='InvalidParameters' ,
				@ProcessLookupCode='TripleAdd',
				@SeverityLookupCode='Error'

			RETURN
		END

	IF @ObjectGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid ,
				@UserCode=@UserCode ,
				@Comment='ObjectGuid is required.' ,
				@LogTypeLookupCode='InvalidParameters' ,
				@ProcessLookupCode='TripleAdd',
				@SeverityLookupCode='Error'

			RETURN
		END

	SET @TripleGuid = newid()

	INSERT INTO Triple
	(
		Guid,
		SubjectGuid,
		PredicateLookupGuid,
		ObjectGuid
	)
	VALUES
	(
		@TripleGuid,
		@SubjectGuid,
		@PredicateLookupGuid,
		@ObjectGuid
	)

	IF @@ROWCOUNT != 1
		SET @TripleGuid=NULL

	IF @ReturnRecordset = 1
		SELECT * From Triple WHERE Guid=@TripleGuid
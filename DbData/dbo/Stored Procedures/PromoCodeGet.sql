﻿



CREATE procedure [dbo].[PromoCodeGet]
	@UserGuid as uniqueidentifier,
	@PromoCode AS nvarchar(20),
	@PromoName AS nvarchar(50) OUTPUT
AS
	declare @SingleUse bit
	select @PromoName = coalesce(p.name,''),
	@SingleUse = coalesce(p.SingleUse,0)
	FROM PromoCode p
	WHERE p.Code = @PromoCode
	AND GetDate() > p.EffectiveDate 
	AND GetDate() < p.ExpirationDate
	
	If 	@PromoName <> '' AND @SingleUse = 1
	BEGIN
	   select @PromoName = coalesce('This Promo Code has already been used',isnull(@PromoName,''))
	   from 
	   GrowerOrderView gov
	   join GrowerOrderLowestOrderLineStatusView gol on gov.GrowerOrderGuid = gol.GrowerOrderGuid
	   Join Lookup l on gol.OrderLineStatusLookupGuid = l.guid
	   Where gov.userguid = @UserGuid
	   and l.SortSequence > 1
	   and gov.PromotionalCode = @PromoCode
	   
	   
	  
	  --select @PromoName = @PromoName
	END
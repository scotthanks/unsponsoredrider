﻿


















CREATE PROCEDURE [dbo].[B2BAutomatedSupplierOrderListGet]
	 @Category as nvarchar(20) =''
	 

AS
	 
	declare @IsB2BOrdering bit
	IF @Category = 'B2BSupplier'
		set @IsB2BOrdering = 1
	ELSE
		set  @IsB2BOrdering = 0

	
 
	
	;With  inc (GrowerOrderGuid)
	AS
	(
		Select Distinct olv.GrowerOrderGuid 
		From OrderLineViewShowInactiveProduct olv (nolock)
		Join growerOrder o (nolock) on olv.GrowerOrderGuid = o.Guid
		Join grower g (nolock) on o.GrowerGuid = g.Guid
		Where 
		OrderLineStatusLookupGuid in (select Guid from lookup where path in  ('Code/OrderLineStatus/GrowerEdit','Code/OrderLineStatus/GrowerNotified','Code/OrderLineStatus/Ordered','Code/OrderLineStatus/GrowerCancelled'))
		AND o.CustomerPONO not like '%test%'
		AND o.Description not like '%test%'
		AND g.IsInternal = 0
		AND o.Guid not in (
			Select Distinct olv.GrowerOrderGuid 
			From OrderLineViewShowInactiveProduct olv (nolock)
			Join ProductForm pf on pf.Guid = olv.ProductFormGuid
			Where olv.QtyOrdered  % pf.SalesUnitQty  <> 0
		)
	)

	Select o.Guid as GrowerOrderGuid,so.Guid as SupplierOrderGuid
	into #SupplierOrder
	From GrowerOrder o (nolock)
	Join inc on o.Guid = inc.GrowerOrderGuid
	Join GrowerOrderSummaryByShipWeekGroupByOrderView2 ov  (nolock) on o.Guid = ov.GrowerOrderGuid
	Join SupplierOrder  so  (nolock) on so.GrowerOrderGuid   = o.Guid
	Join Supplier s  (nolock) on s.Guid = so.SupplierGuid
	WHERE 
		1 = 1
		
		AND s.IsB2BOrdering = @IsB2BOrdering
		AND ov.LowestOrderLineStatusLookupCode not in ('PreCart','Pending') 
		AND o.Guid not in (
			Select olv.GrowerOrderGuid--,GrowerOrderno,linenumber,count(*) 
			From OrderLineViewShowInactiveProduct olv (nolock)
			Where LineNumber <> 0
			Group By GrowerOrderGuid,LineNumber
			Having  count(*) > 1
		)
		AND o.Guid not in (
			Select Distinct olv.GrowerOrderGuid 
			From OrderLineViewShowInactiveProduct olv (nolock)
			Where OrderLineStatusLookupGuid in (select Guid from lookup where path in  ('Code/OrderLineStatus/SupplierB2BError','Code/OrderLineStatus/GrowerB2BError'))
		)
		--select path from lookup where path like 'Code/OrderLineStatus/%'
		--SELECT 38 / 5 AS Integer, 38 % 5 AS Remainder ; 
		 
	;With  zz(GrowerOrderGuid,B2BCount)
	AS
	(
	SELECT  o.Guid as GrowerOrderGuid,count(b2b.ID) as B2BCount
	FROM #SupplierOrder tso
	Join GrowerOrder o (nolock) on tso.GrowerOrderGuid = o.Guid
	left JOIN [epsB2BProd].[dbo].[B2BIntegrationXML] b2b (nolock) on b2b.OrderGuid = o.Guid 
	--and  b2b.CommunicationDirection = 'ToSeller'
	
	Group by o.Guid
	)
	
	Select 
	o.Guid as OrderGuid
	,so.Guid as SupplierOrderGuid
	,g.Code as GrowerCode
	,g.Name as GrowerName
	,o.CustomerPoNo
	,rtrim(ov.GrowerOrderStatusLookupCode) as GrowerOrderStatusCode
	,case when rtrim(ov.GrowerOrderStatusLookupCode) in ('Invoiced','Paid') 
		then ov.GrowerOrderStatusLookupName 
		else LowestOrderLineStatusLookupName end as GrowerOrderStatusName
	,ov.InvoiceNo
	,case when rtrim(ov.GrowerOrderStatusLookupCode) in ('Invoiced','Paid') 
		then ov.GrowerOrderStatusLookupCode else ov.LowestOrderLineStatusLookupCode 
		end as OrderStatus
	,so.ID as SupplierOrderID
	,rtrim(s.Code) as SupplierCode
	,s.Name as SupplierName
	,rtrim(se.Code) as SellerCode
	,ov.LowestOrderLineStatusLookupCode as LowestOrderLineStatusCode
	,o.[Description] as OrderDescription
	,o.OrderNo as OrderNo
	,ov.QtyOrderedCount as OrderQty
	,ov.QtyOrderedCount as OrderQty
	,p.FirstName + ' ' + p.LastName as PersonWhoPlacedOrder
	,pfc.Name as ProductFormCategoryName
	,pt.Name as ProgramTypeName
	,s.Email as SupplierEmail
	,substring(ov.ShipWeekCode,5,2) + '|' + substring(ov.ShipWeekCode,1,4) as ShipWeekString
	,zz.B2BCount
	,g.IsBadGrower
	from zz 
	
	Join GrowerOrderSummaryByShipWeekGroupByOrderView2 ov (nolock) on ov.GrowerOrderGuid = zz.GrowerOrderGuid
	Join GrowerOrder o (nolock) on ov.GrowerOrderGuid = o.Guid
	Join SupplierOrder so (nolock) on so.GrowerOrderGuid = o.Guid
	Join Supplier s (nolock) on s.Guid = so.SupplierGuid
	Join Seller se (nolock) on s.SellerGuid = se.Guid
	Join Grower g (nolock) on ov.GrowerGuid = g.Guid
	Join Person p (nolock) on ov.PersonGuid = p.Guid
	Join ProductFormCategory pfc (nolock) on ov.ProductFormCategoryGuid = pfc.Guid
	Join ProgramType pt (nolock) on ov.ProgramTypeGuid = pt.Guid
	Join ShipWeek sw (nolock) on ov.ShipWeekGuid = sw.Guid
	

	ORDER BY o.OrderNo desc,s.Name 




		
	Drop Table #SupplierOrder
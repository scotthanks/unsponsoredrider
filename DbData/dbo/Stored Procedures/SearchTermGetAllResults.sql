﻿


CREATE PROCEDURE [dbo].[SearchTermGetAllResults]
	
	
AS
	Declare @sText nvarchar(2000)
	Declare @RC integer

	set @sText = 'Start Running all Search'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','SearchTermGetAllResults','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'
	

	DECLARE @SearchTerm nVarchar(200)
	DECLARE @SearchTermGuid uniqueidentifier
	DECLARE @getResults CURSOR
	SET @getResults = CURSOR FOR
	SELECT SearchString,Guid
	FROM SearchTerm
	WHERE DateDeactivated is  null
	Order by SearchString
	OPEN @getResults
	FETCH NEXT
	FROM @getResults INTO @SearchTerm,@SearchTermGuid
	WHILE @@FETCH_STATUS = 0
		BEGIN
			
			Exec SearchResultDataPopulate @SearchTermGuid,0,0
			
			FETCH NEXT
			FROM @getResults INTO @SearchTerm,@SearchTermGuid
		END
	CLOSE @getResults
	DEALLOCATE @getResults


 
 	set @sText = 'Done Running all Search'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','SearchTermGetAllResults','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'
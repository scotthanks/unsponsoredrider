﻿--EXAMPLE:

--EXECUTE GrowerAddressDataGet
--	@UserGuid='7714B7E5-7148-404B-9E2E-93EE8B7E3B63',
--	@GrowerGuid='92CFACCB-5DF3-4832-8328-384B3BED6E8F'

--EXECUTE GrowerAddressDataGet
--	@UserGuid='7714B7E5-7148-404B-9E2E-93EE8B7E3B63',
--	@GrowerGuid='92CFACCB-5DF3-4832-8328-384B3BED6E8F',
--	@GrowerAddressGuid='00000000-0000-0000-0000-000000000000',
--	@GrowerOrderGuid='00000000-0000-0000-0000-000000000000'

--EXECUTE GrowerAddressDataGet
--	@UserGuid='7714B7E5-7148-404B-9E2E-93EE8B7E3B63',
--	@GrowerGuid='92CFACCB-5DF3-4832-8328-384B3BED6E8F',
--	@GrowerAddressGuid='A562C6A9-C46C-49C4-9709-97335E76D1F3'

CREATE PROCEDURE [dbo].[GrowerAddressDataGet]
	@UserGuid AS UNIQUEIDENTIFIER,
	@GrowerGuid AS UNIQUEIDENTIFIER,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER = NULL,
	@GrowerAddressGuid AS UNIQUEIDENTIFIER = NULL
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1) +
		dbo.XmlSegment('GrowerOrderGuid',@GrowerOrderGuid,1) +
		dbo.XmlSegment('GrowerAddressGuid',@GrowerAddressGuid,1),
		0
	)

	IF @GrowerAddressGuid=CAST(CAST(0 AS binary) AS UNIQUEIDENTIFIER)
		SET @GrowerAddressGuid = NULL

	IF @GrowerOrderGuid=CAST(CAST(0 AS binary) AS UNIQUEIDENTIFIER)
		SET @GrowerOrderGuid = NULL
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerAddressDataGet',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	IF @GrowerGuid IS NOT NULL
		BEGIN
			SELECT GrowerAddress.*
			FROM GrowerAddress
			WHERE Guid IN
			(
				SELECT GrowerAddressGuid from GrowerAddressView
				WHERE
					GrowerAddressView.GrowerGuid=@GrowerGuid AND
					(@GrowerAddressGuid IS NULL OR GrowerAddressView.GrowerAddressGuid=@GrowerAddressGuid)
			)

			SELECT * FROM Address
			WHERE Guid IN
			(
				SELECT AddressGuid from GrowerAddressView
				WHERE
					GrowerAddressView.GrowerGuid=@GrowerGuid AND
					(@GrowerAddressGuid IS NULL OR GrowerAddressView.GrowerAddressGuid=@GrowerAddressGuid)
			)

			SELECT * FROM GrowerOrder
			WHERE Guid = @GrowerOrderGuid
		END

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerAddressDataGet',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@UserGuid=@UserGuid
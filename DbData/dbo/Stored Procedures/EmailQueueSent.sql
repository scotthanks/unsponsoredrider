﻿



CREATE PROCEDURE [dbo].[EmailQueueSent]
	 @iID as bigint
AS
	Update EmailQueue
	set SendStatus ='Sent',DateSent = getdate()
	WHERE ID = @iID
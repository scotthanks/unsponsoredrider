﻿CREATE procedure [dbo].[TripleFileExtensionMimeTypeAdd]
	@FileExtensionLookupCode AS NCHAR(50),
	@MimeTypeLookupCode AS NCHAR(50),
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NCHAR(56) = NULL
AS
	DECLARE @FileExtensionGuid AS uniqueidentifier
	DECLARE @MimeTypeGuid AS uniqueidentifier

	DECLARE @PathExpression AS NVARCHAR(200)

	SET @PathExpression = 'Code/FileExtension/' + LTRIM(RTRIM(@FileExtensionLookupCode))
	EXECUTE LookupGetDataByPath
		@Expression=@PathExpression, 
		@Guid=@FileExtensionGuid OUT
	
	IF @FileExtensionGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid ,
				@UserCode=@UserCode ,
				@Comment='@FileExtensionLookupCode not found.' ,
				@LogTypeLookupCode='InvalidParameters' ,
				@ProcessLookupCode='TripleFileExtensionMimeTypeAdd' ,
				@SeverityLookupCode='Error'

			RETURN
		END
	
	SET @PathExpression = 'Code/MimeType/' + LTRIM(RTRIM(@MimeTypeLookupCode))
	EXECUTE LookupGetDataByPath
		@Expression=@PathExpression, 
		@Guid=@MimeTypeGuid OUT
	
	IF @MimeTypeGuid IS NULL
		BEGIN
			PRINT 'MimeType not found.'
			RETURN
		END

	EXECUTE TripleAdd 
		@PredicateLookupCode='FileExtensionMimeType', 
		@SubjectGuid=@FileExtensionGuid, 
		@ObjectGuid=@MimeTypeGuid
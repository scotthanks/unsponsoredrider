﻿







CREATE PROCEDURE [dbo].[DummenPricingUploadAddItem]
	 @JobNumber as bigint,
	 @GrowerName as nvarchar(50),
	 @ProgramSeasonCode as nvarchar(20),
	 @SupplierIdentifier as nvarchar(50),
	 @CuttingCost as decimal(8,4),
	 @RoyaltyCost as decimal(8,4),
	 @FreightCost as decimal(8,4),
	 @TagCost as decimal(8,4),
	 @DeactivatedDate as datetime


	
AS
		Insert into GrowerProductProgramSeasonPriceUpload values(
			@JobNumber,
			getDate(),
			@GrowerName ,
			NULL,
			@ProgramSeasonCode,
			NULL,
			@SupplierIdentifier,
			NULL,
			@CuttingCost,
			@RoyaltyCost,
			@FreightCost,
			@TagCost,
			Case when (@DeactivatedDate IS NULL OR @DeactivatedDate = '2001-01-01 00:00:00.000') then null 
				else @DeactivatedDate 
				end
			
			
		)
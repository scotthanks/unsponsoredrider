﻿--EXAMPLE:

---- Delete all Rick Greenhouse orders.
--DELETE OrderLine WHERE Guid IN (SELECT OrderLineGuid FROM OrderLineView WHERE GrowerName = 'Rick Greenhouse')
--DELETE SupplierOrder WHERE Guid IN (SELECT SupplierOrderGuid FROM SupplierOrderView WHERE GrowerName = 'Rick Greenhouse')
--DELETE GrowerOrder WHERE Guid IN (SELECT GrowerOrderGuid FROM GrowerOrderView WHERE GrowerName = 'Rick Greenhouse')

--DECLARE @GrowerOrderGuid as UNIQUEIDENTIFIER
--DECLARE @SupplierOrderGuid as UNIQUEIDENTIFIER

--EXECUTE GrowerOrderGetOrAdd
--	@UserCode='rick.harrison',
--	@GrowerGuid='9CB1183C-20AC-4049-9206-25C558CA5BEF',
--	@ShipWeekCode='201320',
--	@ProgramTypeCode='MUM',
--	@ProductFormCategoryCode='RC',
--	@CustomerPoNo='TestPoNumber',
--	@Description='This is my order.',
--	@GrowerOrderGuid=@GrowerOrderGuid OUT

--SELECT
--  @GrowerOrderGuid as GrowerOrderGuid

--EXECUTE SupplierOrderGetOrAdd
--	@UserCode='rick.harrison',
--	@GrowerOrderGuid=@GrowerOrderGuid,
--	@SupplierGuid='A070F7DF-A563-453E-A558-0F7524FCE705',
--	@PoNo='TestSupplierPoNumber',
--	@SupplierOrderGuid=@SupplierOrderGuid OUT

--SELECT
--	@SupplierOrderGuid as SupplierOrderGuid

CREATE PROCEDURE [dbo].[SupplierOrderGetOrAdd]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NCHAR(56) = NULL,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER,
	@SupplierGuid AS UNIQUEIDENTIFIER,
	@ProgramTypeCode AS NCHAR(20),
	@ProductFormCategoryCode AS NCHAR(20),
	@PoNo AS NCHAR(30) = '',
	@SupplierOrderGuid AS UNIQUEIDENTIFIER = NULL OUT
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('GrowerOrderGuid',@GrowerOrderGuid,1) +
		dbo.XmlSegment('SupplierGuid',@SupplierGuid,1) +
		dbo.XmlSegment('ProgramTypeCode',@SupplierGuid,1) +
		dbo.XmlSegment('PoNo',@PoNo,1),
		0
	)

	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='SupplierOrderGetOrAdd',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	DECLARE @VerifiedGrowerOrderGuid AS UNIQUEIDENTIFIER
	SELECT
		@VerifiedGrowerOrderGuid = GrowerOrderGuid
	FROM GrowerOrderView 
	WHERE GrowerOrderGuid = @GrowerOrderGuid

	IF @VerifiedGrowerOrderGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid,
				@UserCode=@UserCode,
				@Comment='GrowerOrderGuid is not valid',
				@LogTypeLookupCode='ParameterError',
				@ProcessLookupCode='GrowerOrderGetOrAdd',
				@ObjectTypeLookupCode='GrowerOrder',
				@GuidValue=@GrowerOrderGuid

			RETURN
		END


	DECLARE @VerifiedSupplierGuid AS UNIQUEIDENTIFIER
	SELECT
		@VerifiedSupplierGuid = SupplierGuid
	FROM SupplierView 
	WHERE SupplierGuid = @SupplierGuid

	IF @VerifiedSupplierGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid,
				@UserCode=@UserCode,
				@Comment='SupplierGuid is not valid',
				@LogTypeLookupCode='ParameterError',
				@ProcessLookupCode='SupplierOrderGetOrAdd',
				@ObjectTypeLookupCode='Supplier',
				@GuidValue=@SupplierGuid

			RETURN
		END

	IF @PoNo IS NULL
		SET @PoNo = ''
	SET @PoNo = UPPER(LTRIM(RTRIM(@PoNo)))

	SET @PoNo = LTRIM(RTRIM(@PoNo))

	DECLARE @SupplierOrderNo AS NCHAR(30)
	SET @SupplierOrderNo = 'unassigned'

	SELECT @SupplierOrderGuid = SupplierOrderGuid
	FROM SupplierOrderView
	WHERE
		GrowerOrderGuid = @GrowerOrderGuid AND
		SupplierGuid = @SupplierGuid

	IF @SupplierOrderGuid IS NULL
		EXECUTE SupplierOrderAdd
			@UserGuid = @UserGuid,
			@UserCode = @UserCode,
			@GrowerOrderGuid = @GrowerOrderGuid,
			@SupplierGuid = @SupplierGuid,
			@ProgramTypeCode = @ProgramTypeCode,
			@ProductFormCategoryCode = @ProductFormCategoryCode,
			@PoNo = @PoNo,
			@SupplierOrderGuid = @SupplierOrderGuid OUT

	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('SupplierOrderGuid',@SupplierOrderGuid,1),
		0
	)

	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='SupplierOrderGetOrAdd',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@XmlData=@ReturnValues
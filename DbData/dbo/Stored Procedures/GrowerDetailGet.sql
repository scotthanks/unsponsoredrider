﻿






CREATE PROCEDURE [dbo].[GrowerDetailGet]
	@GrowerGuid UNIQUEIDENTIFIER
AS
	SELECT 
	g.ID
	,g.code
	,g.name
	,g.Guid,g.Email
	,CAST(g.PhoneAreaCode AS CHAR(3)) AS PhoneAreaCode
	,CAST(g.Phone AS CHAR(7))  AS Phone
	 ,gt.Name AS GrowerType
	 ,AdditionalAccountingEmail
	 ,AdditionalOrderEmail
	 ,g.B2BOrderURL
	 ,g.B2BAvailabilityURL
	 ,IsBadGrower
	 ,g.IsInternal
	 FROM Grower g
	 JOIN Lookup gt ON g.GrowerTypeLookupGuid = gt.Guid
	 WHERE g.Guid = @GrowerGuid
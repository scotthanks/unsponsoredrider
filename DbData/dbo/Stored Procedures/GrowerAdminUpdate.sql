﻿









CREATE PROCEDURE [dbo].[GrowerAdminUpdate]
	@GrowerGuid AS UNIQUEIDENTIFIER,
	@GrowerName AS NVARCHAR(50),
	@Email AS NVARCHAR(70),
	@PhoneAreaCode AS decimal(3,0),
	@Phone AS decimal(7,0),
	@AdditionalAccountingEmail AS NVARCHAR(500),
	@AdditionalOrderEmail AS NVARCHAR(500),
	@B2BOrderURL AS NVARCHAR(200),
	@B2BAvailabilityURL AS NVARCHAR(200),
	@IsInternal AS bit,
	@IsBadGrower AS bit

AS

	--Update Grower
	UPDATE Grower
	SET
		Name=@GrowerName
		,Email = @Email
		,PhoneAreaCode = @PhoneAreaCode
		,Phone = @Phone
		,AdditionalAccountingEmail=@AdditionalAccountingEmail
		,AdditionalOrderEmail=@AdditionalOrderEmail
		,B2BOrderURL = @B2BOrderURL
		,B2BAvailabilityURL = @B2BAvailabilityURL
		,IsInternal = @IsInternal
		,IsBadGrower = @IsBadGrower
		
	WHERE Guid=@GrowerGuid
﻿






CREATE PROCEDURE [dbo].[SupplierOrderProductListGet]
	@SupplierOrderGuid AS UNIQUEIDENTIFIER,
	@ProductLike AS NVARCHAR(50)
AS

	DECLARE @SupplierGuid UNIQUEIDENTIFIER
	DECLARE @SupplierCode NVARCHAR(10)

	select @SupplierGuid = s.Guid 
	,@SupplierCode = s.Code
	from SupplierOrder so
	Join Supplier s on s.Guid = so.SupplierGuid
	where so.Guid = @SupplierOrderGuid
	

	Select top 30
	
	v.ProductGuid 
	,v.SpeciesName 
	,v.VarietyName 
    ,pf.Name as ProductFormName
	,v.ProductSupplierDescription as SupplierDescription
                         
	
	FROM 
	ProductView v 
	Join ProductForm pf on v.ProductFormGuid = pf.guid

	WHERE
	v.SupplierCode = @SupplierCode
	AND
		(v.SpeciesName like '%' + @ProductLike+ '%'
		OR v.VarietyName like '%' + @ProductLike+ '%'
		OR v.ProductSupplierDescription like '%' + @ProductLike+ '%'
		)

	ORDER BY 
	v.SpeciesName 
   ,v.VarietyName 
   ,v.ProductSupplierDescription
   ,pf.Name
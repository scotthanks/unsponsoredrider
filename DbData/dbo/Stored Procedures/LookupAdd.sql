﻿

--EXAMPLE:

--DECLARE @LookupGuid AS UNIQUEIDENTIFIER
--EXECUTE LookupAdd 'TestData','TEST','(description)',@LookupGuid=@LookupGuid OUT
--DELETE Lookup WHERE Guid=@LookupGuid
--EXECUTE LookupAdd 'TestData',@Code=NULL,@LookupGuid=@LookupGuid OUT
--DELETE Lookup WHERE Guid=@LookupGuid

CREATE procedure [dbo].[LookupAdd]
	@ParentExpression AS NVARCHAR(200),
	@Code AS NCHAR(50),
	@Name AS NVARCHAR(100) = '',
	@SortSequence AS float = 0.0,
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NCHAR(56) = NULL,
	@LookupGuid AS UNIQUEIDENTIFIER = NULL OUT,
	@ReturnRecordset AS BIT = 1
AS
	SET @LookupGuid = NULL

	DECLARE @ParentGuid AS uniqueidentifier
	DECLARE @ParentPath AS nvarchar(200)
	DECLARE @Count AS integer

	IF @ParentExpression IS NULL OR LTRIM(RTRIM(@ParentExpression)) = ''
		BEGIN
			SET @ParentGuid = NULL
			SET @ParentPath = ''
		END
	ELSE
		BEGIN
			EXECUTE LookupGetDataByPath @Expression=@ParentExpression, @Guid=@ParentGuid out, @Path=@ParentPath out, @Count=@Count out

			IF @ParentGuid IS NULL OR @Count != 1
				BEGIN
					EXECUTE EventLogAdd
						@UserGuid=@UserGuid ,
						@UserCode=@UserCode ,
						@Comment='The @ParentExpression parameter value does not uniquely identify a parent.',
						@IntValue=@Count,
						@LogTypeLookupCode='InvalidParameters' ,
						@ProcessLookupCode='LookupAdd' ,
						@SeverityLookupCode='Error'

					RETURN
				END
		END

	SET @LookupGuid = newid()

	IF @Code IS NULL OR LTRIM(RTRIM(@Code)) = ''
		SET @Code = @LookupGuid

	IF @Name IS NULL OR LTRIM(RTRIM(@Name)) = ''
		SET @Name = ''

	IF @SortSequence IS NULL
		SET @SortSequence = 0.0

	IF @ParentPath != ''
		SET @ParentPath = @ParentPath + '/'

	INSERT INTO Lookup
	(
		ParentLookupGuid,
		[Guid],
		Code,
		Name,
		SortSequence,
		[Path],
		SequenceChildren
	)
	VALUES
	(
		@ParentGuid,
		@LookupGuid,
		@Code,
		@Name,
		@SortSequence,
		@ParentPath + LTRIM(RTRIM(CAST(@Code AS NVARCHAR(200)))),
		0
	)

	DECLARE @RowCount AS INT
	SET @RowCount = @@ROWCOUNT
	IF @RowCount != 1
		BEGIN
			SET @LookupGuid = NULL

			EXECUTE EventLogAdd
				@Comment='@@ROWCOUNT not 1 after attempted write (See IntValue).',
				@LogTypeLookupCode='DatabaseError',
				@ProcessLookupCode='LookupAdd',
				@ObjectTypeLookupCode='Lookup',
				@IntValue=@RowCount
		END
	ELSE
		BEGIN
			EXECUTE EventLogAdd
				@LogTypeLookupCode='RowAdd',
				@ProcessLookupCode='LookupAdd',
				@ObjectTypeLookupCode='Lookup',
				@ObjectGuid=@LookupGuid

			IF @SortSequence != 0.0
				EXECUTE LookupCheck

			IF @ReturnRecordset = 1
				SELECT * FROM Lookup WHERE Guid = @LookupGuid
		END
﻿





CREATE PROCEDURE [dbo].[SuppliersGet]
	@NameLike nvarchar(100) = ''
AS
	SELECT s.Guid,s.code,s.name,IsNull(max(ra.DateReported),'1/1/2010') as LatestUpdate
	 From supplier s
	left Join program pr on s.guid = pr.supplierguid
	left join product p on pr.guid = p.programguid
	Left join ReportedAvailability RA on p.guid = RA.productguid
		 and RA.AvailabilityTypeLookupGuid = (select guid from lookup where code = 'AVAIL')
	
	 where s.Name like @NameLike + '%'
	group by s.Guid,s.code,s.name
	Order by s.name asc
﻿





CREATE procedure [dbo].[GrowerSellersGet]
	@Email as nvarchar(70)
	
AS
	SELECT s.Guid,
	rtrim(s.Code) as Code,
	--s.Name + ' Program View' as Name,
	s.DisplayName as Name,
	gs.IsDefault
	FROM Seller s
	JOIN GrowerSeller gs on s.Guid = gs.SellerGuid
	JOIN Grower g on gs.GrowerGuid = g.Guid
	JOIN Person p on p.GrowerGuid = g.Guid
	Where p.Email = @Email
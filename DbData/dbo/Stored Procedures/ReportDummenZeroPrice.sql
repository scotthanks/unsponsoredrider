﻿













CREATE PROCEDURE [dbo].[ReportDummenZeroPrice]
	

AS
select 
	g.Name as GrowerName,su.Name as SellerName,o.OrderNo,sw.Year,sw.WEek,
	ol.LineNumber,ols.Name as Status,p.SupplierIdentifier,p.SupplierDescription,
	ol.QtyOrdered,ol.ActualPrice,ol.ActualCost
from OrderLine ol 
	join Product p on ol.ProductGuid = p.guid
	join Lookup ols on ol.OrderLineStatusLookupGuid = ols.guid
	join SupplierOrder so on so.Guid = ol.SupplierOrderguid
	join Supplier su on so.SupplierGuid = su.guid
	join growerOrder o on o.guid = so.GrowerOrderguid
	join shipweek sw on o.shipweekguid = sw.Guid
	join Grower g on g.Guid = o.GrowerGuid
	Join GrowerSeller gs on g.Guid = gs.GrowerGuid
	join Seller s on gs.SellerGuid = s.Guid
where s.Guid = (select guid from seller where code = 'DMO')
	and ol.ActualPrice = 0
	and QtyOrdered > 0
	--and ols.Name != 'Cancelled'
Order By sw.Year,sw.WEek,g.Name,o.OrderNo,ol.LineNumber,ol.ActualPrice,ol.ActualCost
﻿












CREATE procedure [dbo].[AvailabilityProcessXMLJob]
	@AvailabilityIntegrationXMLID as bigint

AS
	DECLARE @StartYear int
	DECLARE @EndYear int
	DECLARE @StartWeek int
	DECLARE @EndWeek int
	DECLARE @XML AS XML
	DECLARE @PairedXML AS XML
	DECLARE @RunID AS bigint
	DECLARE @PairedID AS bigint
	DECLARE @SupplierCodeExternal AS nvarchar(10)
	DECLARE @SupplierCodeInternal AS nvarchar(10)
	DECLARE @PairedSupplierCodeExternal AS nvarchar(10)
	DECLARE @PairedSupplierCodeInternal AS nvarchar(10)
	DECLARE @UpdateStatus AS nvarchar(50)
	DECLARE @StatusMessage AS nvarchar(Max)
	
	set @StatusMessage = cast(getdate() as nvarchar(30))
	
	Update  epsB2BProd.dbo.AvailabilityIntegrationXML 
	set uploadStatus= 'In Process',
	StatusMessage = @StatusMessage where ID = @AvailabilityIntegrationXMLID

	



	SELECT 
		@XML = XMLData, 
		@RunID = ID,
		@PairedID =PairedID,
		@SupplierCodeExternal = SupplierCodeExternal
	FROM epsB2BProd.dbo.AvailabilityIntegrationXML
	WHERE ID = @AvailabilityIntegrationXMLID

	--Get the External Codes
	SELECT  
		@SupplierCodeExternal = S.X.value('(PlantCode/text())[1]', 'nvarchar(100)')
	FROM @XML.nodes('/AvailabilityFeed/AvailabilityHeader') as S(X)
	Update epsB2BProd.dbo.AvailabilityIntegrationXML
	set SupplierCodeExternal = @SupplierCodeExternal
	WHERE ID   = @AvailabilityIntegrationXMLID

	If @SupplierCodeExternal in ('DORO','DECK')
	BEGIN
		set @StatusMessage = @StatusMessage + ' ' + 'not uploaded will be paired'
		
		Update  epsB2BProd.dbo.AvailabilityIntegrationXML 
		set uploadStatus= 'Paired',
		StatusMessage = @StatusMessage where ID = @AvailabilityIntegrationXMLID
	END
	ELSE
	BEGIN
		If @SupplierCodeExternal in ('DORB','DMNS')	AND @PairedID = 0
		BEGIN
			set @StatusMessage = @StatusMessage + ' ' + 'not uploaded will be run in pair'
		
			Update  epsB2BProd.dbo.AvailabilityIntegrationXML 
			set uploadStatus= 'Paired',
			StatusMessage = @StatusMessage where ID = @AvailabilityIntegrationXMLID

		END

		ELSE  --run the job
		BEGIN
			--Get the Internal Code
			Update epsB2BProd.dbo.AvailabilityIntegrationXML
			set SupplierCodeInternal = (
											SELECT Coalesce (
												(select rtrim(s.code)
												from SupplierLocationCode l
												Join Supplier s on l.SupplierGuid = s.guid
												Where l.LocationCode = @SupplierCodeExternal )
												,'UNK') 
										)
			WHERE ID  = @AvailabilityIntegrationXMLID

			if @PairedID > 0
			BEGIN
				SELECT 
					@PairedXML = XMLData
				FROM epsB2BProd.dbo.AvailabilityIntegrationXML
				WHERE ID = @PairedID

				SELECT  
				@PairedSupplierCodeExternal = S.X.value('(PlantCode/text())[1]', 'nvarchar(100)')
				FROM @PairedXML.nodes('/AvailabilityFeed/AvailabilityHeader') as S(X)

				Update epsB2BProd.dbo.AvailabilityIntegrationXML
				set SupplierCodeExternal = @PairedSupplierCodeExternal
				WHERE ID   = @PairedID

				--Get the Internal Code
				Update epsB2BProd.dbo.AvailabilityIntegrationXML
				set SupplierCodeInternal = (
											SELECT Coalesce (
												(select rtrim(s.code)
												from SupplierLocationCode l
												Join Supplier s on l.SupplierGuid = s.guid
												Where l.LocationCode = @PairedSupplierCodeExternal)
												,'UNK') 
										) 
				WHERE ID  = @PairedID

			END

	
		
	
	

			select @SupplierCodeInternal = SupplierCodeInternal from epsB2BProd.dbo.AvailabilityIntegrationXML 
			where ID = @AvailabilityIntegrationXMLID
			select @PairedSupplierCodeInternal = isnull(SupplierCodeInternal,'UNK') from epsB2BProd.dbo.AvailabilityIntegrationXML 
			where ID = @PairedID

			DELETE FROM AvailabilityIntegrationDetail 
			WHERE AvailabilityIntegrationID in (@RunID,@PairedID)

			--print @SupplierCodeInternal
			--print @PairedSupplierCodeInternal
			--The Main run 
			if @SupplierCodeInternal = 'UNK'
			BEGIN
				Update  epsB2BProd.dbo.AvailabilityIntegrationXML 
				set uploadStatus= 'Processed',
				StatusMessage = @StatusMessage + ' - Unknown Supplier' where ID = @AvailabilityIntegrationXMLID
			END

			ELSE
			BEGIN
				INSERT INTO AvailabilityIntegrationDetail
					SELECT @RunID, 
					getdate() as DateReported,
					S.X.value('(MaterialNumber/Number/text())[1]', 'nvarchar(100)') as SupplierIdentifier,
					NULL as ProductGuid,
					S.X.value('(AvailabilityDate/Date/text())[1]', 'nvarchar(100)') as MondayDate,
					NULL as ShipWeekGuid,
					S.X.value('(Units/Count/text())[1]', 'nvarchar(100)') as Qty
				FROM @XML.nodes('/AvailabilityFeed/AvailabilityItem') as S(X)
			END

			if @PairedSupplierCodeInternal = 'UNK'
			BEGIN
				Update  epsB2BProd.dbo.AvailabilityIntegrationXML 
				set uploadStatus= 'Processed',
				StatusMessage = @StatusMessage + ' - Unknown Supplier' where ID = @PairedID
			END
			if @PairedID > 0 and @PairedSupplierCodeInternal <> 'UNK'
			BEGIN
				SELECT 
					@XML = XMLData
				FROM epsB2BProd.dbo.AvailabilityIntegrationXML
				WHERE ID = @PairedID

				--The Paired run 
				INSERT INTO AvailabilityIntegrationDetail
				SELECT @PairedID, 
				getdate() as DateReported,
				S.X.value('(MaterialNumber/Number/text())[1]', 'nvarchar(100)') as SupplierIdentifier,
				NULL as ProductGuid,
				S.X.value('(AvailabilityDate/Date/text())[1]', 'nvarchar(100)') as MondayDate,
				NULL as ShipWeekGuid,
				S.X.value('(Units/Count/text())[1]', 'nvarchar(100)') as Qty
				FROM @XML.nodes('/AvailabilityFeed/AvailabilityItem') as S(X)
			END


			if @SupplierCodeInternal != 'UNK'
			BEGIN

				Update AvailabilityIntegrationDetail set MondayDate = dateadd(d,-4,MondayDate)
				Where AvailabilityIntegrationID in (@RunID,@PairedID)

				Update AvailabilityIntegrationDetail set ShipWeekGuid = sw.Guid
				from AvailabilityIntegrationDetail a
				left join shipweek sw on a.MondayDate = sw.MondayDate
				Where AvailabilityIntegrationID in (@RunID,@PairedID)

				Update AvailabilityIntegrationDetail 
				set ProductGuid = p.Guid
				from epsB2BProd.dbo.AvailabilityIntegrationXML x
				Join AvailabilityIntegrationDetail d on x.Id = d.AvailabilityIntegrationID
				left join Product p on d.SupplierIdentifier = p.SupplierIdentifier
					and p.Code like x.SupplierCodeInternal + '%'
					and p.Code not like '%URCHGT%'
				Where x.ID in (@RunID,@PairedID)
				and p.Guid is not null

				----clear out incoming programs and handle multi year case.
				-- not doing this any longer as of 4/28/16 -- converstation bewteen Scott and Gary
				-- Back to doing this as of 7/5/16 -- Conversation between Scott, Suzanne and GAry
				SELECT 
					@StartYear =  Min(sw.Year),
					@EndYear = 	Max(sw.Year),
					@StartWeek = Min(sw.Week),
					@EndWeek = Max(sw.Week)			
				FROM AvailabilityIntegrationDetail d
				Join ShipWeek sw on d.ShipWeekGuid = sw.guid
				WHERE d.AvailabilityIntegrationID in (@RunID,@PairedID)
	


				set @StatusMessage = @StatusMessage + '; ' +  'StartYear = ' + cast(@StartYear as varchar(4)) + ', End Year = ' + cast(@EndYear as varchar(4))
						+  ',Startweek = ' + cast(@StartWeek as varchar(4)) + ', End Week = ' + cast(@EndWeek as varchar(4))
				if @StartYear = @EndYear
				BEGIN
					UPDATE ReportedAvailability
					SET availabilitytypelookupguid = 
							(select guid 
							 from lookup 
							 where path = 'Code/AvailabilityType/NA'),
						 QTY=0,
						 SalesSinceDateReported=0,
						 DateReported=getdate()
					FROM ReportedAvailability RA 
					JOIN Product p on RA.ProductGuid = p.guid 
					JOIN Program pr on p.ProgramGuid = pr.guid 
					JOIN Supplier s on pr.supplierguid = s.guid 
					JOIN ShipWeek sw on RA.shipweekguid = sw.guid 
					WHERE sw.Year = @StartYear
						AND sw.Week BETWEEN  @StartWeek AND @EndWeek
						AND pr.Guid in (
							SELECT DISTINCT pr1.Guid 
							FROM Program pr1 
							JOIN Product p1 on pr1.Guid = p1.ProgramGuid 
							JOIN  AvailabilityIntegrationDetail d1 on p1.Guid = d1.ProductGuid
							WHERE d1.AvailabilityIntegrationID in (@RunID,@PairedID)
						)        

				END
				ELSE
				BEGIN
					UPDATE ReportedAvailability
					SET availabilitytypelookupguid = 
						(select guid from lookup where path = 'Code/AvailabilityType/NA'),
						QTY = 0,
						SalesSinceDateReported = 0,
						DateReported = getdate()
					from ReportedAvailability RA 
					JOIN Product p on RA.ProductGuid = p.guid
					JOIN Program pr on p.ProgramGuid = pr.guid
					JOIN Supplier s on pr.supplierguid = s.guid
					JOIN ShipWeek sw on RA.shipweekguid = sw.guid
					WHERE sw.Year = @StartYear
						AND sw.Week >=  @StartWeek
						AND pr.Guid in (
							SELECT DISTINCT pr1.Guid 
							FROM Program pr1
							JOIN Product p1 on pr1.Guid = p1.ProgramGuid 
							JOIN  AvailabilityIntegrationDetail d1 on p1.Guid = d1.ProductGuid
							WHERE d1.AvailabilityIntegrationID in (@RunID,@PairedID)
						)
                

					UPDATE ReportedAvailability
					SET availabilitytypelookupguid = (select guid from lookup where path = 'Code/AvailabilityType/NA'),QTY=0,SalesSinceDateReported=0,DateReported=getdate()
					from ReportedAvailability RA 
					JOIN Product p on RA.ProductGuid = p.guid 
					JOIN Program pr on p.ProgramGuid = pr.guid 
					JOIN Supplier s on pr.supplierguid = s.guid 
					JOIN ShipWeek sw on RA.shipweekguid = sw.guid 
					WHERE sw.Year = @EndYear
						AND sw.Week <=  @EndWeek
						AND pr.Guid in (
							SELECT DISTINCT pr1.Guid 
							FROM Program pr1 
							JOIN Product p1 on pr1.Guid = p1.ProgramGuid 
							JOIN  AvailabilityIntegrationDetail d1 on p1.Guid = d1.ProductGuid
							WHERE d1.AvailabilityIntegrationID in (@RunID,@PairedID)
						  )

		
             
				END

				;	--Now Update Quantities
				--This combines Records in both files based on the shipweek and product, adds them together
				With ra_Sum ( ReportedAvailabilityGuid,Qty)
				AS (
				SELECT ra.Guid,Sum(d.Qty)
				From AvailabilityIntegrationDetail d
				Join ReportedAvailability ra on d.ProductGuid  =ra.ProductGuid
					AND d.ShipWeekGuid = ra.ShipWeekGuid
				Where d.AvailabilityIntegrationID in (@RunID,@PairedID)
				Group By 
				ra.Guid
				)
	
				Update ReportedAvailability
				set QTY = ra_Sum.QTY,
				AvailabilityTypeLookupGuid =(select guid from lookup where path ='Code/AvailabilityType/AVAIL'),
				DateReported = getdate(),
				SalesSinceDateReported = 0
				From ReportedAvailability ra  
				Join ra_Sum  on ra.Guid  =ra_Sum.ReportedAvailabilityGuid

				set @StatusMessage = @StatusMessage + ';  ' + cast(@@Rowcount as nvarchar(30)) + ' Records Updated' 

				----Now hold OPEN the week 40 and beyond for Guatemala Perennials.
				--if @SupplierCodeInternal = 'DGU'
				--BEGIN
		
				--	update reportedavailability set qty = 0
				--	,availabilitytypelookupguid = (select guid from lookup where path = 'Code/AvailabilityType/OPEN')
				--	where guid in (
				--		select a.guid
				--		from product p
				--		Join Variety v on p.VarietyGuid = v.Guid
				--		Join Species sp on v.SpeciesGuid = sp.Guid
				--		Join productform pf on p.ProductFormGuid = pf.guid
				--		Join reportedavailability a on p.guid = a.ProductGuid
				--		Join ShipWeek sw on a.Shipweekguid = sw.guid

				--		where (p.code like 'DGUURCP%')
			
				--		and ( (sw.year = 2016 and sw.week > 39)
				--			OR
				--			(sw.year = 2017 and sw.week < 41)
				--			)
				--	)
		
				--END

				----Now hold OPEN the NGI between 23-47
				--if @SupplierCodeInternal in ('DGU','DES')
				--BEGIN
				--	update reportedavailability set qty = 0
				--	,availabilitytypelookupguid = (select guid from lookup where path = 'Code/AvailabilityType/OPEN')
				--	where guid in (
				--		select a.guid
				--		from product p
				--		Join Variety v on p.VarietyGuid = v.Guid
				--		Join Species sp on v.SpeciesGuid = sp.Guid
				--		Join productform pf on p.ProductFormGuid = pf.guid
				--		Join reportedavailability a on p.guid = a.ProductGuid
				--		Join ShipWeek sw on a.Shipweekguid = sw.guid

				--		where (p.code like 'DES%' OR p.code like 'DGU%')
				--		--and v.Name like 'Sunstand%'
				--		--and v.Name like 'Tamarinda%'
				--		and sp.Code = 'ngi'
				--		and sw.year = 2016 
				--		and sw.week > 22
				--		and sw.week < 48
				--	)
				--END


				-- Now update the HGTV items
				if @SupplierCodeInternal in ('DGU','DES','DFL','DET','DEA')
				BEGIN
			

				--	print 'In HGTV'

					With ra_Sum ( ReportedAvailabilityGuid,Qty)
					AS (
					SELECT ra2.Guid ,Sum(d.Qty)
					From AvailabilityIntegrationDetail d
					Join ReportedAvailability ra on d.ProductGuid  =ra.ProductGuid
						AND d.ShipWeekGuid = ra.ShipWeekGuid
					Join Product p on ra.ProductGuid = p.Guid
					Join Program pr on p.ProgramGuid = pr.Guid
					Join Product p2 on p.SupplierIdentifier = p2.SupplierIdentifier 
						and p2.Code like '%URCHGT%'
						and p2.Code like  @SupplierCodeInternal + '%'
					Join ReportedAvailability ra2 on p2.Guid = ra2.productguid and ra.Shipweekguid = ra2.Shipweekguid
			

					Where d.AvailabilityIntegrationID in (@RunID,@PairedID)
					and pr.Code in (
						'DESURCA','DETURCA','DEGURCA','DGUURCA','DEAURCA'
						)
					Group By 
					ra2.Guid
					)
	
					Update ReportedAvailability
					set QTY = ra_Sum.QTY,
					AvailabilityTypeLookupGuid =(select guid from lookup where path ='Code/AvailabilityType/AVAIL'),
					DateReported = getdate(),
					SalesSinceDateReported = 0
					From ReportedAvailability ra  
					Join ra_Sum  on ra.Guid  =ra_Sum.ReportedAvailabilityGuid

					set @StatusMessage = @StatusMessage + ';  ' + cast(@@Rowcount as nvarchar(30)) + ' HGTV Records Updated' 

			
				END


				Update  epsB2BProd.dbo.AvailabilityIntegrationXML 
				set uploadStatus= 'Processed',
				StatusMessage = @StatusMessage where ID = @RunID

				Update  epsB2BProd.dbo.AvailabilityIntegrationXML 
				set uploadStatus= 'Processed',
				StatusMessage = 'Paired with ID: ' + cast(@RunID as nvarchar(10))  where ID = @PairedID

			END		
		END	
	END
﻿--DECLARE @OrderLineTrxStatusLookupGuid UNIQUEIDENTIFIER

--EXECUTE LookupGetGuid
--	@UserCode='rick.harrison',
--	@Path='Code/OrderLineStatus',
--	@LookupCode='PreCart ',
--	@ProcessLookupCode='Test',
--	@LookupGuid=@OrderLineTrxStatusLookupGuid OUT

--SELECT @OrderLineTrxStatusLookupGuid AS OrderLineTrxStatusLookupGuid

CREATE PROCEDURE [dbo].[LookupGetGuid]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NVARCHAR(56) = NULL,
	@Path AS NVARCHAR(50) = NULL,
	@LookupCode AS NVARCHAR(50),
	@ProcessLookupCode AS NVARCHAR(50),
	@LookupGuid AS UNIQUEIDENTIFIER OUT
AS
	SET @Path = LTRIM(RTRIM(@Path))

	IF @Path NOT LIKE '%/'
		SET @Path = @Path + '/'

	SET @Path = @Path + '%'

	SET @LookupGuid = NULL

	SELECT @LookupGuid = LookupGuid
	FROM LookupView
	WHERE LookupCode=@LookupCode AND Path LIKE @Path

	IF @LookupGuid IS NULL
		BEGIN
			SET @Path = REPLACE(@Path,'%','')

			EXECUTE EventLogKeyNotFound
				@UserCode=@UserCode,
				@UserGuid=@UserGuid,
				@KeyName=@Path,
				@Key=@LookupCode,
				@ObjectTypeLookupCode='Lookup',
				@ProcessLookupCode=@ProcessLookupCode
		END
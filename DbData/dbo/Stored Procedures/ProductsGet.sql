﻿






CREATE PROCEDURE [dbo].[ProductsGet]
	@VarietyCode nVarChar(30)= ''
AS

Select 
	
p.Code,
p.SupplierIdentifier,
p.SupplierDescription,
s.Name as Supplier,
pr.Name as Program,
pf.Name as ProductForm,
case	when ISNULL(p.DateDeactivated,1) = 1 
		then 'Active' 
		else 'Deactivated' 
		end as Active


FROM 
Product p
Join Program pr on p.ProgramGuid = pr.Guid
Join Variety v on p.VarietyGuid = v.Guid
Join Supplier s on pr.SupplierGuid = s.Guid
Join ProductForm pf on p.ProductFormGuid = pf.Guid
		
WHERE rtrim(v.code) =  @VarietyCode
	

ORDER BY s.Name,p.SupplierDescription
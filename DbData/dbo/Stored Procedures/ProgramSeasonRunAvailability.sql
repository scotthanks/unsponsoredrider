﻿









CREATE PROCEDURE [dbo].[ProgramSeasonRunAvailability]
	@ProgramSeasonCode AS NVARCHAR(50)
	
AS
	Declare @dCurrentMondayDate  Datetime
	Declare @DayOfWeek  NVARCHAR(50)
	Declare @HourOfDay  int
	Declare @ProgramCode  NVARCHAR(50)
	Declare @SellerCode NVarChar(10)
	Declare @ProgramFormCode  NVARCHAR(50)
	Declare @ProgramTypeCode  NVARCHAR(10)
	Declare @NAWeeks  NVARCHAR(50)
	Declare @OPENWeeks  NVARCHAR(50)
	Declare @SpecialFormRules  NVARCHAR(300)
	Declare @SpecialSpeciesRules  NVARCHAR(300)
	Declare @SpecialProductRules  NVARCHAR(500)
	Declare @ProcessNAWeeks  NVARCHAR(50)
	Declare @ProcessOPENWeeks  NVARCHAR(50)
	Declare @ProcessSpecialFormRules  NVARCHAR(300)
	Declare @ProcessSpecialSpeciesRules  NVARCHAR(300)
	Declare @ProcessSpecialProductRules  NVARCHAR(500)
	Declare @iStartWeek  int
	Declare @iEndWeek  int
	Declare @sFormCode varchar(20)
	Declare @sSpeciesCode varchar(10)
	Declare @sProductList varchar(300)
	Declare @sStatusCode varchar(20)
	Declare @sWeekRange varchar(20)
	Declare @sql  nvarchar(2000)
	Declare @tempsql  nvarchar(2000)
	Declare @err bigint
	Declare @RC int
	Declare @sText  nvarchar(2000)
	
	set @ProgramSeasonCode = rtrim(@ProgramSeasonCode)

	--Get the correct Program Code
	SELECT  @ProgramCode = rtrim(pr.code) 
			,@SellerCode = se.Code
		FROM ProgramSeason ps 
			Join Program pr on ps.ProgramGuid = pr.guid  
			Join Seller se on pr.SellerGuid = se.guid  
		WHERE ps.Code = @ProgramSeasonCode	
	
	set @sText = @ProgramSeasonCode + ':  Program is ' +  @ProgramCode + ':  seller is ' +  @SellerCode
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'
	

	--Get the correct Program Form Code
	SELECT @ProgramFormCode = rtrim(pfc.Code)
		,@ProgramTypeCode = rtrim(pt.Code)
	FROM Program pr
	JOIN ProductFormCategory pfc on pr.ProductFormCategoryGuid = pfc.guid
	JOIN ProgramType pt on pr.ProgramTypeGuid = pt.guid
	WHERE pr.code = @ProgramCode	
	
	set @sText = @ProgramSeasonCode + ':  Product Form Code is ' +  @ProgramFormCode
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'

	--Get the correct week
	set @dCurrentMondayDate = (
		SELECT TOP 1 sw.MondayDate 
		FROM shipweek sw WHERE mondaydate < getdate()
		order by MondayDate desc	)
	
	set @sText = @ProgramSeasonCode + ':  Current Monday Date is ' +  cast(@dCurrentMondayDate as varchar(50))
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'


	set @DayOfWeek = (select datename(dw,getdate()))
	set @HourOfDay = (SELECT DATEPART(HOUR,getdate()));
	
	--////////	Delete all from 2 weeks before current week -- all programs across the board
    DELETE FROM ReportedAvailability
	WHERE guid in
		(
		SELECT ra.guid
		FROM reportedavailability ra
		JOIN Product p ON ra.productguid = p.guid
		JOIN Program pr ON p.programguid = pr.guid
		JOIN Shipweek sw ON ra.shipweekguid = sw.guid
		WHERE sw.MondayDate  < dateadd(d,-14,@dCurrentMondayDate)
		 )

 	--select * from programtype
 --	select s.code,p.Name from program p
	--join seller s on p.SellerGuid = s.Guid
	--where s.Code = 'DMO'
	IF @SellerCode = 'DMO'
	BEGIN  --Begin of Dummen rules
		IF (@DayOfWeek in( 'Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') AND @ProgramTypeCode in( 'VA','HGT','GER') AND @ProgramFormCode = 'URC')       
		BEGIN  --Dummen Orange	Annuals, Geraniums and HGT URC	Tuesday (one week prior to ship)	10:00 AM --4:00PM
			UPDATE ReportedAvailability
				SET availabilitytypelookupguid  = (select guid from lookup where path = 'Code/AvailabilityType/NA'), 
				Qty = 0,SalesSinceDateReported = 0, DateReported = getdate()
				FROM ReportedAvailability ra
				JOIN Lookup avl on ra.availabilitytypelookupguid = avl.guid
				JOIN shipweek sw on ra.shipweekguid = sw.guid
				JOIN product p on ra.productguid = p.guid
				JOIN program pr on p.programguid = pr.guid
				JOIN ProgramSeason ps on ps.programguid = pr.guid
				WHERE ps.code = @ProgramSeasonCode
					AND sw.MondayDate BETWEEN ps.StartDate and ps.EndDate
					AND sw.MondayDate <= dateadd(d,7,@dCurrentMondayDate)
		END
		
		IF (@DayOfWeek in('Friday','Saturday','Sunday') AND @ProgramTypeCode in( 'VA','HGT','GER') AND @ProgramFormCode = 'CC')       
		BEGIN  --Dummen Orange	Annuals, Geraniums and HGT CC	Friday (four week prior to ship)	2:00PM --8:00PM
			UPDATE ReportedAvailability
				SET availabilitytypelookupguid  = (select guid from lookup where path = 'Code/AvailabilityType/NA'), 
				Qty = 0,SalesSinceDateReported = 0, DateReported = getdate()
				FROM ReportedAvailability ra
				JOIN Lookup avl on ra.availabilitytypelookupguid = avl.guid
				JOIN shipweek sw on ra.shipweekguid = sw.guid
				JOIN product p on ra.productguid = p.guid
				JOIN program pr on p.programguid = pr.guid
				JOIN ProgramSeason ps on ps.programguid = pr.guid
				WHERE ps.code = @ProgramSeasonCode
					AND sw.MondayDate BETWEEN ps.StartDate and ps.EndDate
					AND sw.MondayDate <= dateadd(d,28,@dCurrentMondayDate)
		END

		IF (@DayOfWeek in( 'Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') AND @ProgramTypeCode in( 'POI','PER','MUM') AND @ProgramFormCode = 'URC')       
		BEGIN  --Dummen Orange	Perennials,Poinsettia URC	Tuesday (one week prior to ship)	12:00 PM  -- 6 PM
			UPDATE ReportedAvailability
				SET availabilitytypelookupguid  = (select guid from lookup where path = 'Code/AvailabilityType/NA'), 
				Qty = 0,SalesSinceDateReported = 0, DateReported = getdate()
				FROM ReportedAvailability ra
				JOIN Lookup avl on ra.availabilitytypelookupguid = avl.guid
				JOIN shipweek sw on ra.shipweekguid = sw.guid
				JOIN product p on ra.productguid = p.guid
				JOIN program pr on p.programguid = pr.guid
				JOIN ProgramSeason ps on ps.programguid = pr.guid
				WHERE ps.code = @ProgramSeasonCode
					AND sw.MondayDate BETWEEN ps.StartDate and ps.EndDate
					AND sw.MondayDate <= dateadd(d,7,@dCurrentMondayDate)
		END


	END  --End of Dummen rules

	--Only run EPS programs and only run if on the 10,11,12 PM schedule
	--this stops the EPS from cutting off on the earlier runs
	IF @SellerCode = 'EPS' and 	@HourOfDay >= 22 --10:00 PM
	BEGIN				   
		If @ProgramFormCode = 'URC' 
			Begin
				set @sText = @ProgramSeasonCode + ':  Program is URC'
				EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
						 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 

				UPDATE ReportedAvailability
				SET availabilitytypelookupguid  = (select guid from lookup where path = 'Code/AvailabilityType/NA'), 
				Qty = 0,SalesSinceDateReported = 0, DateReported = getdate()
				FROM ReportedAvailability ra
				JOIN Lookup avl on ra.availabilitytypelookupguid = avl.guid
				JOIN shipweek sw on ra.shipweekguid = sw.guid
				JOIN product p on ra.productguid = p.guid
				JOIN program pr on p.programguid = pr.guid
				JOIN ProgramSeason ps on ps.programguid = pr.guid
				WHERE pr.code = @ProgramCode
					AND sw.MondayDate BETWEEN ps.StartDate and ps.EndDate
					AND sw.MondayDate = @dCurrentMondayDate
                     
			 End

					   
		If @ProgramFormCode = 'URC' and @DayOfWeek in ('Thursday','Friday','Saturday','Sunday')
			Begin
				set @sText = @ProgramSeasonCode + ':  Program is URC and In Th-Sun'
				EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
						 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 

				UPDATE ReportedAvailability
				SET availabilitytypelookupguid  = (select guid from lookup where path = 'Code/AvailabilityType/NA'), 
				Qty = 0,SalesSinceDateReported = 0, DateReported = getdate()
				FROM ReportedAvailability ra
				JOIN Lookup avl on ra.availabilitytypelookupguid = avl.guid
				JOIN shipweek sw on ra.shipweekguid = sw.guid
				JOIN product p on ra.productguid = p.guid
				JOIN program pr on p.programguid = pr.guid
				JOIN ProgramSeason ps on ps.programguid = pr.guid
				WHERE ps.code = @ProgramSeasonCode
					AND sw.MondayDate BETWEEN ps.StartDate and ps.EndDate
					AND sw.MondayDate = dateadd(d,7,@dCurrentMondayDate)
                     
			 End
	--	set @sText = @ProgramSeasonCode + ':  End If URC, then NA next week if Thurs-Sun'
	--	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
	--					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'    

  

      
	 --/////////////////////	
	--	set @sText = @ProgramSeasonCode + ':  Starting If not URC, then NA current week if Wed-Sun'
	--	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
	--					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'
  
		set @DayOfWeek = (select datename(dw,getdate()))

	-- 	set @sText = @ProgramSeasonCode + ':  Day of Week is: ' + @DayOfWeek
	--	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
	--					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 
					   
		If @ProgramFormCode in('LIN','RC','GRA','PRE','CC') 
			and @DayOfWeek in ('Wednesday','Thursday','Friday','Saturday','Sunday')
			Begin
				set @sText = @ProgramSeasonCode + ':  Program Form is ' + @ProgramFormCode + ' and In Wed-Sun'
				EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
						 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 

				UPDATE ReportedAvailability
				SET availabilitytypelookupguid  = (select guid from lookup where path = 'Code/AvailabilityType/NA'), 
				Qty = 0,SalesSinceDateReported = 0, DateReported = getdate()
				FROM ReportedAvailability ra
				JOIN Lookup avl on ra.availabilitytypelookupguid = avl.guid
				JOIN shipweek sw on ra.shipweekguid = sw.guid
				JOIN product p on ra.productguid = p.guid
				JOIN program pr on p.programguid = pr.guid
				JOIN ProgramSeason ps on ps.programguid = pr.guid
				WHERE ps.code = @ProgramSeasonCode
					AND sw.MondayDate BETWEEN ps.StartDate and ps.EndDate
					AND sw.MondayDate = @dCurrentMondayDate
                     
			 End
   	 

	END  --End of EPS and Late run

		--////////  For all Programs update to NA based on OPEN And Lead times
	UPDATE ReportedAvailability
    SET availabilitytypelookupguid  = (select guid from lookup where path = 'Code/AvailabilityType/NA'), 
	Qty = 0,SalesSinceDateReported = 0, DateReported = getdate()
    FROM ReportedAvailability ra
    JOIN Lookup avl on ra.availabilitytypelookupguid = avl.guid
    JOIN shipweek sw on ra.shipweekguid = sw.guid
    JOIN product p on ra.productguid = p.guid
    JOIN program pr on p.programguid = pr.guid
    JOIN ProgramSeason ps on pr.guid = ps.ProgramGuid
    WHERE ps.code = @ProgramSeasonCode
		AND sw.MondayDate BETWEEN ps.StartDate and ps.EndDate
		AND sw.MondayDate < DATEADD(wk,p.ProductionLeadTimeWeeks,@dCurrentMondayDate)
		AND ra.availabilitytypelookupguid <>  (select guid from lookup where code = 'AVAIL')
﻿



--NOTE: Null value returns all Forms for all categories

CREATE PROCEDURE [dbo].[catalog_ProductFormsGet]
	@programTypeCode AS nchar(10) = null
AS
	
	SELECT comb.ProductFormCategoryGuid as FormGuid, comb.ProductFormCategoryCode as FormCode, frm.Name as FormName, comb.ProgramTypeGuid as CategoryGuid, comb.ProgramTypeCode as CategoryCode, cat.ProgramTypeName as CategoryName
	FROM ProductFormCategoryProgramTypeCombinationView comb
	JOIN ProductFormCategoryView frm on frm.ProductFormCategoryGuid  = comb.ProductFormCategoryGuid
	JOIN ProgramTypeView cat on cat.ProgramTypeGuid  = comb.ProgramTypeGuid
	WHERE 
	(
	@programTypeCode is null
	or 
	@programTypeCode = comb.ProgramTypeCode
	)
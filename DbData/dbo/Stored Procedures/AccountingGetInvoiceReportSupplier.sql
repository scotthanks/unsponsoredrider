﻿




CREATE PROCEDURE [dbo].[AccountingGetInvoiceReportSupplier]
	 @FiscalYear as nvarchar(50)
	
	 

AS
--select count(*) from SupplierOrder
	
;with olz AS (
	SELECT so.Guid as SupplierOrderGuid ,so.ID as SupplierOrderID,so.SupplierGuid,
	 Sum(isnull(ol.qtyshipped,0) * isnull(ol.actualprice,0)) as ProductAmount,
	 MIN(cast(ols.SortSequence as varchar(10)) + ' - ' + ols.name) as LineStatus
	 FROM SupplierOrder so
	 JOIN OrderLine ol on so.Guid = ol.SupplierOrderGuid
	 Join Lookup ols on ol.OrderLineStatusLookupGuid = ols.Guid
	 Group by   so.Guid,so.ID,so.SupplierGuid
),
 soz AS (
	 SELECT so.Guid as SupplierOrderGuid ,so.ID as SupplierOrderID,so.SupplierGuid,
	 Sum(isnull(sof.Amount,0)) as Amount
	 FROM SupplierOrder so
	 LEFT JOIN SupplierOrderFee sof on so.Guid = sof.SupplierOrderGuid	
	 Group by   so.Guid,so.ID,so.SupplierGuid
)  ,
 goz AS (
	 SELECT o.Guid as GrowerOrderGuid ,o.ID as GrowerOrderID,o.GrowerGuid,
	 Sum(isnull(gof.Amount,0)) as Amount
	 FROM GrowerOrder o
	 LEFT JOIN GrowerOrderFee gof on o.Guid = gof.GrowerOrderGuid	
	 Group by   o.Guid,o.ID,o.GrowerGuid
)  ,invz as (
	SELECT 
	o.Guid as GrowerOrderGuid
	,l.InternalIDNumber
	,l.Amount
	,l.TransactionDate
    from Ledger l
    Join GrowerOrder o on o.Guid = l.ParentGuid
    Join lookup gla on l.GLAccountLookupGuid = gla.guid
    Join lookup et on l.EntryTypeLookupGuid = et.guid
    WHERE 
		gla.code = 'AccountsRecievable'
		and et.Code = 'Debit'
 )
  ,gpz as (
	SELECT 
	o.Guid as GrowerOrderGuid
	,sum(IsNull(l.Amount,0)) as Amount
    from Ledger l
    Join GrowerOrder o on o.Guid = l.ParentGuid
    Join lookup gla on l.GLAccountLookupGuid = gla.guid
    Join lookup et on l.EntryTypeLookupGuid = et.guid
    WHERE
		gla.code = 'AccountsRecievable'
		and et.Code = 'Credit'
	GROUP By
		o.Guid
 )
  ,sinvz as (
	SELECT 
	so.Guid as SupplierOrderGuid
	,sum(IsNull(l.Amount,0)) as Amount
    from Ledger l
    Join SupplierOrder so on so.Guid = l.ParentGuid
    Join lookup gla on l.GLAccountLookupGuid = gla.guid
    Join lookup et on l.EntryTypeLookupGuid = et.guid
    WHERE 
		gla.code = 'AccountsPayable'
		and et.Code = 'Credit'
	GROUP By
		so.Guid
 ) ,spz as (
	SELECT 
	so.Guid as SupplierOrderGuid
	,l.TransactionDate
	,sum(IsNull(l.Amount,0)) as Amount
    from Ledger l
    Join SupplierOrder so on so.Guid = l.ParentGuid
    Join lookup gla on l.GLAccountLookupGuid = gla.guid
    Join lookup et on l.EntryTypeLookupGuid = et.guid
    WHERE 
		gla.code = 'AccountsPayable'
		and et.Code = 'Debit'
	GROUP By
		so.Guid,l.TransactionDate
 )



	SELECT 
	ISNull(invz.InternalIDNumber,'Not Invoiced') as InvoiceNo
	,invz.TransactionDate  as InvoiceDate	
	,s.Name as Supplier
	,g.name as Grower
	,rtrim(o.OrderNo) as OrderNo
	,so.ID as SupplierOrderID
	,case when os.Name = 'Paid' then os.name when invz.TransactionDate is not null then 'Invoiced' else os.Name end as OrderStatus	
	,olz.LineStatus 
	,cast(sw.Year as varchar(4)) + '/' + right(str(100+sw.week),2) as ShipWeek
	,pt.Code as PaymentType
	,Sum(isnull(olz.ProductAmount,0)) as ProductAmount
	,Sum(isnull(soz.Amount,0)) as SupplierOrderFee
	,Sum(isnull(goz.Amount,0)) as GrowerOrderFee
	,Sum(isnull(olz.ProductAmount,0)) + Sum(isnull(soz.Amount,0)) + Sum(isnull(goz.Amount,0)) as ExpectedInvoiceAmount
	,'to do' as InvoiceAmount
	,'to do' as  GrowerPaid
	,Sum(isnull(sinvz.Amount,0)) as  SupplierInvoice
	,Sum(isnull(spz.Amount,0)) as  PaidToSupplier
	,'to do' as GrossProfit
	,'to do' as  GrossProfitRealied
	,spz.TransactionDate as EPSPaidDate	
	,case when olz.LineStatus = '9 - Cancelled' then 6
		  when invz.TransactionDate is not null and os.Name = 'Paid' then 4
		  when invz.TransactionDate is not null and olz.LineStatus = '10 - Write Off' then 5
		  when invz.TransactionDate is null and olz.LineStatus = '8 - Shipped' then 2 
	      when invz.TransactionDate is not null then 1
		  when os.Name = 'Open' then 3 
	       
	       
		 -- when   invz.TransactionDate ='Cancelled' then 6
	      
		  else 10 
		  end  as StatusSort

    FROM GrowerOrder O
    JOIN goz ON o.Guid = goz.GrowerOrderGuid
    LEFT JOIN invz ON o.Guid = invz.GrowerOrderGuid
    LEFT JOIN gpz ON o.Guid = gpz.GrowerOrderGuid
    JOIN Grower g ON o.GrowerGuid = g.Guid
	JOIN SupplierOrder so on so.GrowerOrderGuid = o.Guid
	JOIN soz on so.Guid = soz.SupplierOrderGuid
	LEFT JOIN sinvz on so.Guid = sinvz.SupplierOrderGuid
	LEFT JOIN spz on so.Guid = spz.SupplierOrderGuid
	JOIN olz on so.Guid = olz.SupplierOrderGuid
    JOIN Shipweek sw ON o.ShipWeekGuid = sw.guid
    JOIN Supplier s ON so.SupplierGuid = s.guid
    JOIN Lookup pt ON o.PaymentTypeLookupGuid = pt.guid
    JOIN Lookup os ON o.GrowerOrderStatusLookupGuid = os.guid
    
	
    WHERE sw.FiscalYear > @FiscalYear 
		and o.growerguid not in (
			SELECT subjectguid 
			FROM Triple 
			WHERE predicatelookupguid = (SELECT guid FROM lookup WHERE path = 'Predicate/TestGrower'))
       -- and o.OrderNo = 'A00112748'
      

    GROUP BY 
	s.Name
	,g.name
	,o.OrderNo
	,so.ID
	,olz.LineStatus
	,os.Name
	,cast(sw.Year as varchar(4)) + '/' + right(str(100+sw.week),2)
	,pt.Code 
	,invz.TransactionDate
	,invz.InternalIdNumber
	,spz.TransactionDate
    ORDER BY StatusSort,InvoiceDate,g.name,o.OrderNo
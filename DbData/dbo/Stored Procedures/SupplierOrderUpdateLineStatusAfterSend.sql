﻿






CREATE PROCEDURE [dbo].[SupplierOrderUpdateLineStatusAfterSend]
	 @SupplierOrderGuid as uniqueidentifier
	 
	 

AS
	--select * from lookup where path like '%orderlines%'
	--Code/OrderLineStatus/Cancelled --
	--Code/OrderLineStatus/CustomerNotifiedAfterSupplierConfirmed  --confirmed
	Update OrderLine
	Set OrderLineStatusLookupGuid = (select Guid from Lookup where path = 'Code/OrderLineStatus/SupplierNotified')
	FROM OrderLine ol
	Join OrderLineView olv on ol.Guid = olv.OrderLineGuid

	WHERE 
		1 = 1
		AND olv.SupplierOrderGuid = @SupplierOrderGuid
		AND ol.OrderLineStatusLookupGuid != (SELECT Guid from Lookup where path = 'Code/OrderLineStatus/PreCart')
		AND ol.OrderLineStatusLookupGuid != (SELECT Guid from Lookup where path = 'Code/OrderLineStatus/Cancelled')
		AND ol.OrderLineStatusLookupGuid != (SELECT Guid from Lookup where path = 'Code/OrderLineStatus/Shipped')
		AND ol.OrderLineStatusLookupGuid != (SELECT Guid from Lookup where path = 'Code/OrderLineStatus/Write Off')
		AND ol.OrderLineStatusLookupGuid != (SELECT Guid from Lookup where path = 'Code/OrderLineStatus/Pending')
		AND ol.OrderLineStatusLookupGuid != (SELECT Guid from Lookup where path = 'Code/OrderLineStatus/CustomerNotifiedAfterSupplierConfirmed')
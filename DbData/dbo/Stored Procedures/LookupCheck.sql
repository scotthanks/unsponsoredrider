﻿--update lookup set SEQUENCECHILDREN = 0

CREATE procedure [dbo].[LookupCheck]
	@Corrections AS integer = NULL out,
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NCHAR(56) = NULL
as
	SET @Corrections = 0

	DECLARE @LocalTempSequenced TABLE
	(
		LookupGuid uniqueidentifier
	)

	INSERT INTO @LocalTempSequenced
	SELECT
		Lookup.ParentLookupGuid
	FROM Lookup
	WHERE SortSequence != 0.0
	GROUP BY Lookup.ParentLookupGuid

	UPDATE Lookup
		SET SequenceChildren = 0
	WHERE
		SequenceChildren != 0 AND
		Guid NOT IN (SELECT LookupGuid FROM @LocalTempSequenced)

	SET @Corrections = @Corrections + @@ROWCOUNT

	UPDATE Lookup
		SET SequenceChildren = 1
	WHERE
		SequenceChildren != 1 AND
		Guid IN (SELECT LookupGuid FROM @LocalTempSequenced)

	SET @Corrections = @Corrections + @@ROWCOUNT

	PRINT 'Corrections: ' + CAST(@Corrections AS NVARCHAR(10))

	EXECUTE EventLogAdd
		@UserGuid=@UserGuid ,
		@UserCode=@UserCode ,
		@Comment='Corrections were made.',
		@IntValue=@Corrections,
		@LogTypeLookupCode='InvalidParameters' ,
		@ProcessLookupCode='LookupCheck' ,
		@SeverityLookupCode='Info'
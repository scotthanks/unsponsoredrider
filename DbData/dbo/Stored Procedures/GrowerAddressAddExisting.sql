﻿CREATE PROCEDURE [dbo].[GrowerAddressAddExisting]
	@UserGuid AS UNIQUEIDENTIFIER,
	@GrowerGuid AS UNIQUEIDENTIFIER,
	@AddressGuid AS UNIQUEIDENTIFIER,
	@PhoneAreaCode AS DECIMAL(3),
	@Phone AS DECIMAL(7),
	@SpecialInstructions AS VARCHAR(200),
	@IsDefault AS BIT = 0,
	@GrowerAddressGuid AS UNIQUEIDENTIFIER OUT
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1) +
		dbo.XmlSegment('AddressGuid',@AddressGuid,1) +
		dbo.XmlSegment('PhoneAreaCode',@PhoneAreaCode,1) +
		dbo.XmlSegment('Phone',@Phone,1) +
		dbo.XmlSegment('SpecialInstructions',@SpecialInstructions,1) +
		dbo.XmlSegment('IsDefault',@IsDefault,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerAddressAddExisting',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	DECLARE @RowCount AS INTEGER

	SET @GrowerAddressGuid = NEWID()

	INSERT INTO GrowerAddress
	(
		Guid,
		GrowerGuid,
		AddressGuid,
		PhoneAreaCode,
		Phone,
		SpecialInstructions,
		DefaultAddress
	)
	VALUES
	(
		@GrowerAddressGuid,
		@GrowerGuid,
		@AddressGuid,
		@PhoneAreaCode,
		@Phone,
		@SpecialInstructions,
		@IsDefault
	)

	SET @RowCount=@@ROWCOUNT

	IF @RowCount != 1
		BEGIN
			EXECUTE EventLogAdd
				@Comment='RowCount after adding GrowerAddress was not 1',
				@IntValue=@RowCount,
				@LogTypeLookupCode='RowAddError',
				@ProcessLookupCode='GrowerAddressAddExisting',
				@UserGuid=@UserGuid,
				@GuidValue=@OriginalEventLogGuid

			RETURN
		END

	IF @IsDefault = 1 AND @GrowerAddressGuid IS NOT NULL
		UPDATE GrowerAddress
			SET DefaultAddress = CASE WHEN Guid = @GrowerAddressGuid THEN 1 ELSE 0 END
		WHERE GrowerGuid = @GrowerGuid

	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('GrowerAddressGuid',@AddressGuid,1),
		0
	)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerAddressAddExisting',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@UserGuid=@UserGuid,
		@XmlData=@ReturnValues
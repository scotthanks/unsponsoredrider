﻿



CREATE PROCEDURE [dbo].[ValidateSupplierCode]
	 @SupplierCode as nvarchar(10),
	 @SupplierCodeOut as nvarchar(10) Output

AS
	SELECT @SupplierCodeOut = rtrim(isnull(Code,''))
	FROM Supplier 
	WHERE Code = @SupplierCode
﻿




CREATE PROCEDURE [dbo].[AvailabilityDataCountGet]
	@UserGuid AS UNIQUEIDENTIFIER=NULL,
	@UserCode AS NVARCHAR(56)=NULL,
	@GrowerGuid AS UNIQUEIDENTIFIER=NULL,
	@ShipWeekCode AS NCHAR(6),
	@WeeksBefore AS INTEGER = 0,
	@WeeksAfter AS INTEGER = 0,
	@ProgramTypeCode AS NCHAR(50) = NULL,
	@ProductFormCategoryCode AS NCHAR(50) = NULL,
	@SupplierCodeList AS NVARCHAR(3000) = NULL,
	@GeneticOwnerCodeList AS NVARCHAR(3000) = NULL,
	@SpeciesCodeList AS NVARCHAR(3000) = NULL,
	@VarietyCodeList AS NVARCHAR(3000) = NULL,
	@ProductGuid AS UNIQUEIDENTIFIER = NULL,
	@OrganicOnly AS BIT = 0,
	@AvailableOnly AS BIT = 0,
	@SellerCode as NVARCHAR(30)
AS
	DECLARE @bDebug as BIT = 0
	If @bDebug = 1  print 'start' + + (CONVERT( VARCHAR(24), GETDATE(), 121))



	IF @ProductGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
		SET @ProductGuid = NULL

	IF @WeeksBefore IS NULL OR @WeeksBefore < 0
		Set @WeeksBefore = 0

	IF @WeeksAfter IS NULL OR @WeeksAfter < 0
		SET @WeeksAfter = 0

	IF @ProgramTypeCode Like '%*%'
		SET @ProgramTypeCode = NULL

	IF @ProductFormCategoryCode Like '%*%'
		SET @ProductFormCategoryCode = NULL

	IF @SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%*%'
		SET @SupplierCodeList = NULL
	ELSE
		SET @SupplierCodeList = ',' + @SupplierCodeList + ','

	IF @GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%*%'
		SET @GeneticOwnerCodeList = NULL
	ELSE
		SET @GeneticOwnerCodeList = ',' + @GeneticOwnerCodeList + ','

	IF @SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%*%'
		SET @SpeciesCodeList = NULL
	ELSE
		SET @SpeciesCodeList = ',' + @SpeciesCodeList + ','

	IF @VarietyCodeList IS NULL OR @VarietyCodeList LIKE '%*%'
		SET @VarietyCodeList = NULL
	ELSE
		SET @VarietyCodeList = ',' + @VarietyCodeList + ','

	DECLARE @ProductFormCode AS NCHAR(50)
	SET @ProductFormCode = NULL
	

	IF @GrowerGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
		SET @GrowerGuid = NULL

	IF @GrowerGuid IS NULL
		EXECUTE GrowerGetGuid
			@UserGuid=@UserGuid,
			@UserCode=@UserCode,
			@PersonGuid=NULL,
			@ProcessLookupCode='AvailabilityDataGet',
			@GrowerGuid=@GrowerGuid OUT

	-- KLUDGE!!! THIS MAKES THE PROCEDURE WORK CORRECTLY, BUT I DON'T LIKE IT.
	SELECT @SpeciesCodeList = SpeciesCode FROM VarietyView WHERE @VarietyCodeList LIKE '%,' + RTRIM(VarietyCode) + ',%'
	SET @SpeciesCodeList = ',' + RTRIM(@SpeciesCodeList) + ','

	DECLARE @LocalTempShipWeeks TABLE
	(
		ShipWeekGuid UNIQUEIDENTIFIER
	)
	
	If @bDebug = 1  print 'start LocalTempShipWeeks' + + (CONVERT( VARCHAR(24), GETDATE(), 121))

	INSERT INTO @LocalTempShipWeeks
	SELECT ShipWeekView.ShipWeekGuid
	FROM ShipWeekView
		CROSS JOIN ShipWeekView AS SelectedShipWeek
		CROSS JOIN ShipWeekView AS FirstShipWeek
		CROSS JOIN ShipWeekView AS LastShipWeek
	WHERE
		SelectedShipWeek.ShipWeekCode = @ShipWeekCode AND
		FirstShipWeek.ShipWeekContinuousWeekNumber = SelectedShipWeek.ShipWeekContinuousWeekNumber - @WeeksBefore AND
		LastShipWeek.ShipWeekContinuousWeekNumber = SelectedShipWeek.ShipWeekContinuousWeekNumber + @WeeksAfter AND
		ShipWeekView.ShipWeekContinuousWeekNumber >= FirstShipWeek.ShipWeekContinuousWeekNumber AND
		ShipWeekView.ShipWeekContinuousWeekNumber <= LastShipWeek.ShipWeekContinuousWeekNumber
	
	If @bDebug = 1  print 'end LocalTempShipWeeks' + + (CONVERT( VARCHAR(24), GETDATE(), 121))


	If @bDebug = 1  print 'start LocalTempAvailability' + + (CONVERT( VARCHAR(24), GETDATE(), 121))
		
	

	create table #LocalTempAvailability(
		ShipWeekGuid uniqueidentifier NOT NULL,
		ProgramTypeGuid uniqueidentifier NOT NULL,
		ProductFormCategoryGuid uniqueidentifier NOT NULL,
		ProductFormGuid uniqueidentifier NOT NULL,
		SpeciesGuid uniqueidentifier NOT NULL,
		SupplierGuid uniqueidentifier NOT NULL,
		VarietyGuid uniqueidentifier NOT NULL,
		ProgramGuid uniqueidentifier NOT NULL,
		ProductGuid uniqueidentifier NOT NULL,
		ReportedAvailabilityGuid uniqueidentifier NOT NULL,
		AvailabilityTypeLookupGuid uniqueidentifier NOT NULL,
		ReportedAvailabilityQty int
	)
	If @SellerCode = 'EPS'
		BEGIN
			INSERT INTO #LocalTempAvailability
			SELECT
				av.ShipWeekGuid,
				av.ProgramTypeGuid,
				av.ProductFormCategoryGuid,
				av.ProductFormGuid,
				av.SpeciesGuid,
				av.SupplierGuid,
				av.VarietyGuid,
				av.ProgramGuid,
				av.ProductGuid,
				av.ReportedAvailabilityGuid,
				av.AvailabilityTypeLookupGuid,
				ReportedAvailabilityQty
			--INTO #LocalTempAvailability
			FROM ReportedAvailabilityView av
			--left Join ProgramSeason ps on ps.Programguid = av.ProgramGuid
				--and av.ShipWeekMondayDate between ps.StartDate and ps.EndDate
			--left Join GrowerProductProgramSeasonPrice gp on av.ProductGuid = gp.ProductGuid
			--	and gp.GrowerGuid = @GrowerGuid and gp.ProgramSeasonGuid = ps.Guid
	
			WHERE
				av.ShipWeekGuid IN (SELECT ShipWeekGuid from @LocalTempShipWeeks) AND
				--(case	when 
				--			@SellerCode = 'EPS' and
							 --ISNULL(@ProductGuid, av.ProductGuid) = av.ProductGuid 
							--then 1
					--	when 
					--		@SellerCode = 'DMO' 
					--		AND gp.ProductGuid = av.ProductGuid 
					--		AND gp.DateDeactivated IS NULL 
					--		then 1
					--	else 0
					--end) = 1 
					--AND
				--(ISNULL(@ProductGuid, av.ProductGuid) = av.ProductGuid) AND
				(@ProgramTypeCode IS NULL OR av.ProgramTypeCode=@ProgramTypeCode) AND
				(@ProductFormCategoryCode IS NULL OR av.ProductFormCategoryCode=@ProductFormCategoryCode) AND
				(@ProductFormCode IS NULL OR av.ProductFormCode=@ProductFormCode) AND
				(@SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%,' + RTRIM(av.SupplierCode) + ',%') AND
				(@GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%,' + RTRIM(av.GeneticOwnerCode) + ',%') AND
				(@SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%,' + RTRIM(av.SpeciesCode) + ',%') AND
				(@VarietyCodeList IS NULL OR @VarietyCodeList LIKE '%,' + RTRIM(av.VarietyCode) + ',%') AND
				(@OrganicOnly = 0 OR av.IsOrganic = 1 )  --AND
				--(av.IsExclusive = 0 OR gp.ProductGuid is not null)
			GROUP BY
				av.ShipWeekGuid,
				av.ProgramTypeGuid,
				av.ProductFormCategoryGuid,
				av.ProductFormGuid,
				av.SpeciesGuid,
				av.SupplierGuid,
				av.VarietyGuid,
				av.VarietyCode,
				av.ProgramGuid,
				av.ProductGuid,
				av.ReportedAvailabilityGuid,
				av.AvailabilityTypeLookupGuid,
				av.ReportedAvailabilityQty
		END
	Else
	BEGIN
		INSERT INTO #LocalTempAvailability
		SELECT
			av.ShipWeekGuid,
			av.ProgramTypeGuid,
			av.ProductFormCategoryGuid,
			av.ProductFormGuid,
			av.SpeciesGuid,
			av.SupplierGuid,
			av.VarietyGuid,
			av.ProgramGuid,
			av.ProductGuid,
			av.ReportedAvailabilityGuid,
			av.AvailabilityTypeLookupGuid,
			av.ReportedAvailabilityQty
		--INTO #LocalTempAvailability2
		FROM ReportedAvailabilityView av
		Join ProgramSeason ps on ps.Programguid = av.ProgramGuid
			and av.ShipWeekMondayDate between ps.StartDate and ps.EndDate
		Join GrowerProductProgramSeasonPrice gp on av.ProductGuid = gp.ProductGuid
			and gp.GrowerGuid = @GrowerGuid and gp.ProgramSeasonGuid = ps.Guid
	
		WHERE
			av.ShipWeekGuid IN (SELECT ShipWeekGuid from @LocalTempShipWeeks) --AND
			--(case	when 
			--			@SellerCode = 'EPS' 
			--			and ISNULL(@ProductGuid, av.ProductGuid) = av.ProductGuid then 1
			--		when 
			--			@SellerCode = 'DMO' 
						AND gp.ProductGuid = av.ProductGuid 
						AND gp.DateDeactivated IS NULL 
				--		then 1
				--	else 0
			--	end) = 1 AND
			--(ISNULL(@ProductGuid, av.ProductGuid) = av.ProductGuid) 
			AND
			(@ProgramTypeCode IS NULL OR av.ProgramTypeCode=@ProgramTypeCode) AND
			(@ProductFormCategoryCode IS NULL OR av.ProductFormCategoryCode=@ProductFormCategoryCode) AND
			(@ProductFormCode IS NULL OR av.ProductFormCode=@ProductFormCode) AND
			(@SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%,' + RTRIM(av.SupplierCode) + ',%') AND
			(@GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%,' + RTRIM(av.GeneticOwnerCode) + ',%') AND
			(@SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%,' + RTRIM(av.SpeciesCode) + ',%') AND
			(@VarietyCodeList IS NULL OR @VarietyCodeList LIKE '%,' + RTRIM(av.VarietyCode) + ',%') AND
			(@OrganicOnly = 0 OR av.IsOrganic = 1 )  
			--AND
			--(av.IsExclusive = 0 OR gp.ProductGuid is not null)
		GROUP BY
			av.ShipWeekGuid,
			av.ProgramTypeGuid,
			av.ProductFormCategoryGuid,
			av.ProductFormGuid,
			av.SpeciesGuid,
			av.SupplierGuid,
			av.VarietyGuid,
			av.VarietyCode,
			av.ProgramGuid,
			av.ProductGuid,
			av.ReportedAvailabilityGuid,
			av.AvailabilityTypeLookupGuid,
			av.ReportedAvailabilityQty
	END

	--remove not avail products if checked
	IF @AvailableOnly = 1
	BEGIN
		delete from #LocalTempAvailability
		where ProductGuid in 
		(
			select a.ProductGuid 
			From #LocalTempAvailability a
			Join Lookup rat on a.AvailabilityTypeLookupGuid = rat.Guid
			Group by a.ProductGuid
			Having 
				sum(a.ReportedAvailabilityQty) <= 0
				AND
				rtrim(Max(rat.Path))!='Code/AvailabilityType/OPEN'
			)
	END
	
	SELECT cast(count(DISTINCT(ProductGuid)) as bigint) as theCount FROM #LocalTempAvailability

	


	Drop Table #LocalTempAvailability
	
	If @bDebug = 1  print 'end' + + (CONVERT( VARCHAR(24), GETDATE(), 121))
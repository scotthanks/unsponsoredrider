﻿
--EXAMPLES:
--DECLARE @GrowerCount AS INTEGER
--DECLARE @GrowerGuid AS UNIQUEIDENTIFIER

--EXECUTE GrowerSearch
--	@UserGuid='026C26B3-CDF9-41DB-AE44-9F82E5164A01',
--	@GrowerNameSearchString='%',
--	@ZipCode='99998',
--	@PhoneNumberAreaCode='123',
--	@PhoneNumber='1234569',
--	@IncludeOnlyActivated=0,
--	@ReturnRecordSets=1,
--	@GrowerCount=@GrowerCount OUT,
--	@GrowerGuid=@GrowerGuid OUT

--SELECT
--	@GrowerCount AS GrowerCount,
--	@GrowerGuid AS GrowerGuid

--EXECUTE GrowerSearch
--	@UserGuid='026C26B3-CDF9-41DB-AE44-9F82E5164A01',
--	@GrowerNameSearchString='%',
--	@ZipCode='99999',
--	@PhoneNumberAreaCode='123',
--	@PhoneNumber='1234567',
--	@IncludeOnlyActivated=0,
--	@ReturnRecordSets=1,
--	@GrowerCount=@GrowerCount OUT,
--	@GrowerGuid=@GrowerGuid OUT

--SELECT
--	@GrowerCount AS GrowerCount,
--	@GrowerGuid AS GrowerGuid

--EXECUTE GrowerSearch
--	@UserGuid='026C26B3-CDF9-41DB-AE44-9F82E5164A01',
--	@GrowerNameSearchString='%rick%greenhouse%',
--	@ZipCode='99999',
--	@PhoneNumberAreaCode='000',
--	@PhoneNumber='0000000',
--	@IncludeOnlyActivated=0,
--	@ReturnRecordSets=1,
--	@GrowerCount=@GrowerCount OUT,
--	@GrowerGuid=@GrowerGuid OUT

--SELECT
--	@GrowerCount AS GrowerCount,
--	@GrowerGuid AS GrowerGuid

--EXECUTE GrowerSearch
--	@UserGuid='026C26B3-CDF9-41DB-AE44-9F82E5164A01',
--	@GrowerNameSearchString='%rick%greenhouse%',
--	@ZipCode='99999',
--	@PhoneNumberAreaCode='123',
--	@PhoneNumber='1234567',
--	@IncludeOnlyActivated=0,
--	@ReturnRecordSets=1,
--	@GrowerCount=@GrowerCount OUT,
--	@GrowerGuid=@GrowerGuid OUT

--SELECT
--	@GrowerCount AS GrowerCount,
--	@GrowerGuid AS GrowerGuid

CREATE procedure [dbo].[GrowerSearch]
	@UserGuid AS UNIQUEIDENTIFIER,
	@GrowerNameSearchString AS NVARCHAR(50),
	@ZipCode AS NCHAR(5),
	@PhoneNumberAreaCode AS DECIMAL(3),
	@PhoneNumber AS DECIMAL(7),
	@IncludeOnlyActivated AS BIT = 1,
	@ReturnRecordsets AS BIT = 0,
	@GrowerCount AS INTEGER = NULL OUT,
	@GrowerGuid AS UNIQUEIDENTIFIER = NULL OUT
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('GrowerNameSearchString',@GrowerNameSearchString,1) +
		dbo.XmlSegment('ZipCode',@ZipCode,1) +
		dbo.XmlSegment('PhoneNumberAreaCode',@PhoneNumberAreaCode,1) +
		dbo.XmlSegment('PhoneNumber',@PhoneNumber,1) +
		dbo.XmlSegment('IncludeOnlyActivated',@IncludeOnlyActivated,1) +
		dbo.XmlSegment('ReturnRecordsets',@ReturnRecordsets,1),
		0
	)

	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER	
	EXECUTE EventLogAdd
		@UserCode=@UserGuid,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerSearch',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	SET @GrowerNameSearchString = LTRIM(RTRIM(@GrowerNameSearchString))
	IF @GrowerNameSearchString IS NULL OR @GrowerNameSearchString=''
		SET @GrowerNameSearchString = NULL

	SET @ZipCode = LTRIM(RTRIM(@ZipCOde))
	IF @ZipCode IS NULL OR @ZipCode=''
		SET @ZipCode = NULL

	IF @PhoneNumberAreaCode = 0
		SET @PhoneNumberAreaCode = NULL

	IF @PhoneNumber = 0
		SET @PhoneNumber = NULL

	DECLARE @LocalTempGrowers TABLE
	(
		GrowerGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalTempGrowers
	SELECT GrowerGuid
	FROM GrowerView
	WHERE
		(GrowerIsActivated=1 OR @IncludeOnlyActivated=0) AND
		(
			(
				(GrowerName LIKE @GrowerNameSearchString) AND
				(ZipCode = @ZipCode)
			)
			OR
			(
				(PhoneAreaCode=@PhoneNumberAreaCode) AND
				(Phone=@PhoneNumber)
			)
		)

	SELECT
		@GrowerCount=COUNT(*),
		@GrowerGuid=MIN(GrowerGuid)
	FROM @LocalTempGrowers

	IF @GrowerGuid IS NULL
		SET @GrowerGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)

	IF @ReturnRecordsets=1
		BEGIN
			SELECT
				Grower.*
			FROM Grower
			WHERE Guid IN (SELECT GrowerGuid FROM @LocalTempGrowers)

			SELECT
				GrowerAddress.*
			FROM GrowerAddress
			WHERE GrowerGuid IN (SELECT GrowerGuid FROM @LocalTempGrowers)

			SELECT
				Address.*
			FROM Address
			WHERE Guid in (SELECT AddressGuid FROM GrowerAddressView WHERE GrowerGuid IN (SELECT GrowerGuid FROM @LocalTempGrowers))
		END

	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('GrowerCount',@GrowerCount,1) + 
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1),
		0
	)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerSearch',
		@ObjectTypeLookupCode='Grower',
		@ObjectGuid=@GrowerGuid,
		@UserCode=@UserGuid,
		@XmlData=@ReturnValues
﻿










CREATE PROCEDURE [dbo].[ReportDummenAvailability]
	@SupplierCodeExternal AS varchar(10)

AS
Declare @RunId bigint
Declare @PairedID bigint

 select top 1 @RunId = ID,@PairedID = ISNULL(PairedID,0)
 from epsb2bProd.dbo.AvailabilityIntegrationXML 
 where uploadstatus = 'Processed'
 AND SupplierCodeExternal = @SupplierCodeExternal
 Order by ID Desc

 print @RunId
 Print @PairedID

select d.SupplierIdentifier,ISNULL(p.SupplierDescription,'No Match') as SupplierDescription ,
ISNULL(v.Name,'') as VarietyName,
ISNULL(sp.name,'') as Speciesname,
sw.year,sw.week,qty
from AvailabilityIntegrationDetail d
left Join Product p on d.ProductGuid = p.Guid
left Join Variety v on p.VarietyGuid = v.Guid
left Join Species sp on v.SpeciesGuid = sp.Guid
left Join shipweek sw on d.Shipweekguid = sw.Guid
where 
 d.AvailabilityIntegrationID in(@RunId,@PairedID)


Order by d.SupplierIdentifier,year,week
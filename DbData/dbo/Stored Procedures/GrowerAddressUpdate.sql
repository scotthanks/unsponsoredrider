﻿

CREATE PROCEDURE [dbo].[GrowerAddressUpdate]
	@UserGuid AS UNIQUEIDENTIFIER,
	@GrowerAddressGuid AS UNIQUEIDENTIFIER,
	@Name AS NVARCHAR(50),
	@StreetAddress1 AS NVARCHAR(50),
	@StreetAddress2 AS NVARCHAR(50),
	@City AS NVARCHAR(50),
	@StateCode AS NCHAR(2),
	@ZipCode AS NCHAR(12),
	@PhoneAreaCode AS DECIMAL(3),
	@Phone AS DECIMAL(7),
	@SpecialInstructions AS VARCHAR(200),
	@IsDefault AS BIT,
	@Success AS BIT OUT
AS
	SET @Success = 1

	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('GrowerAddressGuid',@GrowerAddressGuid,1) + 
		dbo.XmlSegment('Name',@Name,1) +
		dbo.XmlSegment('StreetAddress1',@StreetAddress1,1) +
		dbo.XmlSegment('StreetAddress2',@StreetAddress2,1) +
		dbo.XmlSegment('City',@City,1) +
		dbo.XmlSegment('StateCode',@StateCode,1) +
		dbo.XmlSegment('ZipCode',@ZipCode,1) +
		dbo.XmlSegment('PhoneAreaCode',@PhoneAreaCode,1) +
		dbo.XmlSegment('Phone',@Phone,1) +
		dbo.XmlSegment('SpecialInstructions',@SpecialInstructions,1) + 
		dbo.XmlSegment('IsDefault',@IsDefault,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerAddressUpdate',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	DECLARE @StateGuid AS UNIQUEIDENTIFIER
	SELECT @StateGuid = StateGuid
	FROM StateView WHERE StateCode=@StateCode

	IF @StateGuid IS NULL
		EXECUTE EventLogAdd
			@Comment='The State code was not found.',
			@LogTypeLookupCode='ParameterError',
			@ProcessLookupCode='GrowerAddressUpdate',
			@UserGuid=@UserGuid,
			@GuidValue=@OriginalEventLogGuid

	DECLARE @RowCount AS INTEGER

	DECLARE @AddressGuid AS UNIQUEIDENTIFIER
	SELECT @AddressGuid=AddressGuid
	FROM GrowerAddress
	WHERE Guid=@GrowerAddressGuid

	IF @AddressGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@Comment='The GrowerAddress was not found.',
				@LogTypeLookupCode='ParameterError',
				@ProcessLookupCode='GrowerAddressUpdate',
				@UserGuid=@UserGuid,
				@GuidValue=@OriginalEventLogGuid

			SET @Success = 0

			RETURN
		END

	UPDATE Address
		SET
			Name=@Name,
			StreetAddress1=@StreetAddress1,
			StreetAddress2=@StreetAddress2,
			City=@City,
			StateGuid=@StateGuid,
			ZipCode=@ZipCode
	WHERE Guid=@AddressGuid

	SET @RowCount=@@ROWCOUNT

	IF @RowCount != 1
		BEGIN
			EXECUTE EventLogAdd
				@Comment='RowCount after updating Address was not 1',
				@IntValue=@RowCount,
				@LogTypeLookupCode='RowUpdateError',
				@ProcessLookupCode='GrowerAddressUpdate',
				@UserGuid=@UserGuid,
				@GuidValue=@OriginalEventLogGuid

			SET @Success = 0

			RETURN
		END
	ELSE
		SET @AddressGuid = NULL

	UPDATE GrowerAddress
		SET
			PhoneAreaCode=@PhoneAreaCode,
			Phone=@Phone,
			SpecialInstructions=@SpecialInstructions
	WHERE Guid=@GrowerAddressGuid

	SET @RowCount=@@ROWCOUNT

	IF @RowCount != 1
		BEGIN
			EXECUTE EventLogAdd
				@Comment='RowCount after updating GrowerAddress was not 1',
				@IntValue=@RowCount,
				@LogTypeLookupCode='RowUpdateError',
				@ProcessLookupCode='GrowerAddressUpdate',
				@UserGuid=@UserGuid,
				@GuidValue=@OriginalEventLogGuid

			SET @Success = 0
		END

	DECLARE @GrowerGuid AS UNIQUEIDENTIFIER
	SELECT @GrowerGuid = GrowerGuid FROM GrowerAddressView WHERE GrowerAddressGuid=@GrowerAddressGuid

	IF @IsDefault = 1
		BEGIN
			IF @IsDefault = 1 AND @GrowerAddressGuid IS NOT NULL
				UPDATE GrowerAddress
					SET DefaultAddress = CASE WHEN Guid=@GrowerAddressGuid THEN 1 ELSE 0 END
				WHERE GrowerGuid = @GrowerGuid
		END

	SET @Success = 1

	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('Success',@Success,1),
		0
	)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerAddressUpdate',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@UserGuid=@UserGuid,
		@XmlData=@ReturnValues
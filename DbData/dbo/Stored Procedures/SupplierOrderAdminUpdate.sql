﻿







CREATE PROCEDURE [dbo].[SupplierOrderAdminUpdate]
	@SupplierOrderGuid AS UNIQUEIDENTIFIER,
	@SupplierOrderNo AS NVARCHAR(30),
	@ExpectedDeliveredDate AS DATETIME  = NULL,
	@TrackingNumber AS NVARCHAR(200),
	@TrackingComment AS NVARCHAR(200),
	@ShipMethodCode AS NVARCHAR(50),
	@TagRatioCode AS NVARCHAR(50)
	

AS

	Declare @ShipMethodGuid Uniqueidentifier
	Declare @TagRatioGuid Uniqueidentifier
	Declare @CurrentShipMethodGuid Uniqueidentifier
	Declare @CurrentTagRatioGuid Uniqueidentifier
	Declare @UserGuid uniqueidentifier
	Declare @UserCode nVarchar(56)

	SELECT 
	@UserGuid = Userguid,
	@UserCode = UserCode
	FROM Person 
	WHERE 
	firstname  = 'Customer' 
	AND lastname = 'Service'

	set @ShipMethodGuid = (select Guid from Lookup where path  = 'Code/ShipMethod/' + @ShipMethodCode)
	set @TagRatioGuid = (select Guid from Lookup where path  = 'Code/TagRatio/' + @TagRatioCode)
	
	select 
	@CurrentShipMethodGuid = ShipMethodLookupGuid
	,@CurrentTagRatioGuid = TagRatioLookupGuid
	From SupplierOrder
	WHERE
	Guid=@SupplierOrderGuid

	UPDATE SupplierOrder
	SET
		SupplierOrderNo = @SupplierOrderNo
		,ExpectedDeliveredDate=@ExpectedDeliveredDate
		,TrackingNumber=@TrackingNumber
		,TrackingComment=@TrackingComment
		,ShipMethodLookupGuid=@ShipMethodGuid
		,TagRatioLookupGuid=@TagRatioGuid
		
	WHERE Guid=@SupplierOrderGuid

	
	If @CurrentShipMethodGuid != @ShipMethodGuid OR @CurrentTagRatioGuid != @TagRatioGuid
	BEGIN
		exec SupplierOrderRecalcFees @UserGuid, @UserCode,@SupplierOrderGuid
	End
﻿





CREATE PROCEDURE [dbo].[GrowerOrderUpdateStatusBeforeSendingConfirmation]
	@GrowerOrderGuid AS UNIQUEIDENTIFIER
AS
begin	
	Update OrderLine set OrderLineStatusLookupGuid  = (select Guid 
														from Lookup 
														where path  = 'Code/OrderLineStatus/Cancelled')
	where 
	Guid in (
		Select OrderLineGuid
		from OrderLineViewShowInactiveProduct 
		where GrowerOrderGuid = @GrowerOrderGuid
		AND QtyOrdered = 0
	)
	


	Update OrderLine set OrderLineStatusLookupGuid  = (select Guid 
														from Lookup 
														where path  = 'Code/OrderLineStatus/CustomerNotifiedAfterSupplierConfirmed')
	where 
	Guid in (
		Select OrderLineGuid
		from OrderLineViewShowInactiveProduct 
		where GrowerOrderGuid = @GrowerOrderGuid
		AND QtyOrdered != 0
	)
	AND OrderLineStatusLookupGuid in 
	(select Guid from lookup where path in(
		'Code/OrderLineStatus/SupplierAdd',
		'Code/OrderLineStatus/SupplierConfirmed',
		'Code/OrderLineStatus/SupplierEdit'))


	--select * from lookup where path like '%orderLinest%'

	


end
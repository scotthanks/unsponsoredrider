﻿



CREATE PROCEDURE [dbo].[ProgramSeasonsGet]
	@ProgramCode AS nvarchar(20),
	@SellerCode AS nvarchar(20)
AS
		Select 
	
		p.Code as ProgramCode
		,p.Name as ProgramName
		,ps.Guid as Guid
		,ps.Code as Code
		,ps.Name as Name
		,ps.StartDate
		,ps.EndDate
		,ps.InternalStatus as Status
		from 
		Program p
		Join ProgramSeason ps on p.Guid = ps.ProgramGuid
		Join Seller se on p.SellerGuid = se.Guid
		Where (p.Code = @ProgramCode or @ProgramCode = '')
		AND (se.Code = @SellerCode or @SellerCode = '')
		ORDER BY ps.StartDate
﻿







CREATE PROCEDURE [dbo].[AvailabilityUpdateNewProducts]
	 @Environment as nvarchar(50)
	
	 

AS
IF @Environment = 'TEST'
BEGIN
	Insert into ImageSet 
	select  
	newID(),
	v.code,
	v.name,
	v.code + ' ' + v.name,
	v.code + ' ' + v.name,
	(select guid from lookup where path = 'Code/ImageSetType/VarietyImage'),
	'',
	null,
	null
	from epsDataDev.dbo.variety v
	where v.code not in (select code from epsDataDev.dbo.ImageSet)

	Update epsDataDev.dbo.variety set imagesetguid
	 = s.guid
	 from epsDataDev.dbo.variety v
	 Join epsDataDev.dbo.imageset s on v.code = s.code
	 where v.imagesetguid <>  s.guid
	  or v.imagesetguid is null

	Update epsDataDev.dbo.Product set Code = rtrim(pr.code) + '_' + rtrim(v.code) + '_' + rtrim(pf.code)
	from epsDataDev.dbo.Product p 
	join epsDataDev.dbo.Variety v on p.varietyGuid = v.guid
	join epsDataDev.dbo.species s on v.speciesguid = s.guid
	Join epsDataDev.dbo.Program pr on p.programguid = pr.guid
	Join epsDataDev.dbo.ProductForm pf on p.ProductFormguid = pf.guid
	Where p.code <> rtrim(pr.code) + '_' + rtrim(v.code) + '_' + rtrim(pf.code)


	Insert into epsDataDev.dbo.ReportedAvailability  
	select newid(),getdate(),p.guid,sw.guid,0,(select guid from lookup where code = 'OPEN'),0
	From product p, shipweek sw 
	where sw.year in (2016,2017,2018)
	and p.guid not in (select distinct ProductGuid from epsDataDev.dbo.reportedavailability )
	order by p.id,year,week

END
ELSE
BEGIN  --PROD

	Insert into epsDataProd.dbo.ReportedAvailability  
	select newid(),getdate(),p.guid,sw.guid,0,(select guid from lookup where code = 'OPEN'),0
	From product p, shipweek sw 
	where sw.year in (2016,2017,2018)
	and p.guid not in (select distinct ProductGuid from epsDataProd.dbo.reportedavailability )
	order by p.id,year,week
END
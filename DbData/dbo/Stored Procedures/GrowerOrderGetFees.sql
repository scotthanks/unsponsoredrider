﻿








CREATE PROCEDURE [dbo].[GrowerOrderGetFees]
	@GrowerOrderGuid AS UNIQUEIDENTIFIER
AS
	

	SELECT
		ID,GUID,DateDeactivated,GrowerOrderGuid, 
		FeeTypeLookupGuid,Amount
		,FeeStatXLookupGuid,FeeDescription,IsManual
	FROM GrowerOrderFee 
	WHERE GrowerOrderGuid = @GrowerOrderGuid
﻿




CREATE PROCEDURE [dbo].[EmailQueueCountGet]
	
AS
	

	SELECT SendStatus,
	case when DateToSend < getdate() then 'Ready To Send' else 'Future' end as ReadyStatus,
	Count(*) as theCount
	FROM EmailQueue
	Group By SendStatus,case when DateToSend < getdate() then 'Ready To Send' else 'Future' end
﻿









CREATE PROCEDURE [dbo].[EmailQueueAdd]
	 @sSubject as nvarchar(200),
	 @sBody as nvarchar(max),
	 @iDelaySendMinutes as bigint,
	 @sEmailType as nvarchar(50),
	 @sEmailFrom as nvarchar(100),
	 @sEmailTo as nvarchar(100),
	 @sEmailCC as nvarchar(100),
	 @sEmailBCC as nvarchar(100),
	 @gOrderGuid as uniqueidentifier,
	 @iUpdateCount as int,
	 @iAddCount as int,
	 @iCancelCount as int

AS
	--to do, detect email already in queue and update
	DECLARE @iExistingEmailForOrder as int
	set @iExistingEmailForOrder = (SELECT count(*) 
									FROM  EmailQueue where OrderGuid = @gOrderGuid  
									and SendStatus = 'Not Sent'
									and EmailType = @sEmailType
									and EmailType = 'Order Update')
									 
	if @iExistingEmailForOrder = 0
	BEGIN
		Insert into EmailQueue values(
		NewID(),@sSubject,@sBody,
		DateAdd(n,@iDelaySendMinutes,getdate()),
		'Not Sent',
		NULL,
		@iUpdateCount,@iAddCount,@iCancelCount,
		@sEmailType,@sEmailTo,@sEmailCC,@sEmailBCC,
		@gOrderGuid,@sEmailFrom
		)
	END
	ELSE
	BEGIN
		Update EmailQueue 
		set DateToSend = DateAdd(n,@iDelaySendMinutes,getdate()),
		OrderLineUpdateCount = OrderLineUpdateCount + @iUpdateCount,
		OrderLineAddCount = OrderLineAddCount + @iAddCount,
		OrderLineCancelCount = OrderLineCancelCount + @iCancelCount
		WHERE 
			OrderGuid = @gOrderGuid
			and SendStatus = 'Not Sent'
			and EmailType = @sEmailType

		declare @sOrderChangeText as nvarchar(200)
		declare @iUpdate as int
		declare @iAdd as int
		declare @iCancel as int
		Select  @iUpdate = OrderLineUpdateCount, 
				@iAdd = OrderLineAddCount,
				@iCancel = OrderLineCancelCount
		From 
			EmailQueue
		WHERE 
			OrderGuid = @gOrderGuid
			and SendStatus = 'Not Sent'
			and EmailType = @sEmailType
			
		set @sOrderChangeText = '('
			
		if @iAdd > 0 
		BEGIN
			set @sOrderChangeText = @sOrderChangeText + cast(@iAdd as nvarchar(3)) + ' item'
			if  @iAdd> 1
			BEGIN
				set @sOrderChangeText = @sOrderChangeText + 's'
			END
			set @sOrderChangeText = @sOrderChangeText + ' added'
		END
			
			
		if @iUpdate > 0
		BEGIN
			if  @iAdd > 0
			BEGIN
				set @sOrderChangeText = @sOrderChangeText + ', '
			END
			set @sOrderChangeText = @sOrderChangeText + cast(@iUpdate as nvarchar(3)) + ' item'
			if  @iUpdate > 1
			BEGIN
				set @sOrderChangeText = @sOrderChangeText + 's'
			END
			set @sOrderChangeText = @sOrderChangeText + ' updated'
		END
			
		if @iCancel > 0
		BEGIN
			if  (@iUpdate > 0) OR ( @iAdd > 0) 
			BEGIN
				set @sOrderChangeText = @sOrderChangeText + ', '
			END
			set @sOrderChangeText = @sOrderChangeText + cast(@iCancel as nvarchar(3)) + ' item'
			if  @iCancel > 1
			BEGIN
				set @sOrderChangeText = @sOrderChangeText + 's'
			END
			set @sOrderChangeText = @sOrderChangeText + ' cancelled'
		END
			
		 
			
			
			
	
		set @sOrderChangeText = @sOrderChangeText + ')'

			
		Update EmailQueue
		SET EmailBody = left(EmailBody,charindex('update.', EmailBody)+ 7)
						+ @sOrderChangeText
						+ right(EmailBody,len(EmailBody)-charindex('We will', EmailBody)+6)
			

		WHERE 
				OrderGuid = @gOrderGuid
				and SendStatus = 'Not Sent'
				and EmailType = @sEmailType



	END
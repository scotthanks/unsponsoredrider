﻿

CREATE procedure [dbo].[B2BDummenAcknowledgementXMLJob]
	@B2BIntegrationXMLID as bigint,
	@iDebug int = 0

AS
DECLARE @XML AS XML
DECLARE @BrokerOrderID varchar(100)
DECLARE @CustomerPONumber varchar(100)
DECLARE @ExistingCustomerPONumber varchar(100)
DECLARE @VendorOrderID varchar(100)
DECLARE @RequestedShipDate varchar(100)
DECLARE @ExistingShipDate datetime
DECLARE @ConvertedShipDate datetime
DECLARE @ShipMethod varchar(100)
DECLARE @GrowerOrderGuid uniqueidentifier
DECLARE @B2BID bigint
Declare @Body varchar(200)
DECLARE @StatusMessage varchar(200)


set @B2BID = @B2BIntegrationXMLID

Update 
epsB2BProd.dbo.B2BIntegrationXML
set B2BStatus = 'Started Processing',
statusMessage = cast(GetDAte() as varchar(200))
WHERE ID = @B2BID



SELECT  
@XML = XMLData
FROM epsB2BProd.dbo.B2BIntegrationXML
WHERE ID = @B2BID

SELECT
@BrokerOrderID = m.c.value('@BrokerOrderID', 'varchar(100)'),  
@VendorOrderID = m.c.value('@VendorOrderID', 'varchar(100)'),
@CustomerPONumber = m.c.value('@CustomerPONumber','varchar(100)'),
@RequestedShipDate = m.c.value('@RequestedShipDate', 'varchar(100)'),
@ShipMethod = m.c.value('@ShipMethod', 'varchar(100)') 
FROM 
@XML.nodes('BrokerAcknowledgement/OrderHeader') as m(c)

SELECT @GrowerOrderGuid = o.Guid ,
@ExistingCustomerPONumber = o.CustomerPoNo,
@ExistingShipDate = sw.MondayDate
FROM GrowerOrder o
Join ShipWeek sw on o.ShipWeekGuid = sw.Guid
WHERE o.Orderno = @BrokerOrderID



set @StatusMessage = @BrokerOrderID + ' ' + cast(GetDate() as varchar(200))
set @BrokerOrderID = rtrim(@BrokerOrderID)

UPDATE epsB2BProd.dbo.B2BIntegrationXML
SET OrderGuid =  @GrowerOrderGuid ,
StatusMessage = @StatusMessage
WHERE ID = @B2BID

--Update SupplierOrder no
Update SupplierOrder
set SupplierOrderNo = @VendorOrderID
where GrowerOrderGuid = @GrowerOrderGuid

--check the Customer PO field
If (@CustomerPONumber != '' AND
	@CustomerPONumber !=  left(@ExistingCustomerPONumber,len(@CustomerPONumber))
	)
BEGIN
	--update the order
	Update GrowerOrder set CustomerPoNo =  @CustomerPONumber where Guid = @GrowerOrderGuid
	
	
	set @Body =  @BrokerOrderID + ' - Customer PO Number updated from ' + @ExistingCustomerPONumber + ' to '  + @CustomerPONumber
	--send the email to Alma
	exec EmailQueueAdd 'B2B Order Acknowledgement',@Body ,0,'B2B Ack','CustomerService@eplantsource.com','CustomerService@eplantsource.com','','',@GrowerOrderGuid,0,0,0

END

--Check the ship Method
If @ShipMethod not in( '01','FED EX','FEDEX','TRUCK')
BEGIN
	set @Body =  @BrokerOrderID + ' - ship method returned from Dummen is ' + @ShipMethod + '.  This is not in our system as valid, please alert Scott. ' 
   	--send the email to Alma
	exec EmailQueueAdd 'B2B Order Acknowledgement',@Body ,0,'B2B Ack','CustomerService@eplantsource.com','CustomerService@eplantsource.com','','',@GrowerOrderGuid,0,0,0

END

--check the Ship Week
set @ConvertedShipDate = (select cast(@RequestedShipDate as datetime))
If NOT (@ConvertedShipDate between  @ExistingShipDate and dateadd(d,6,@ExistingShipDate))
BEGIN
	set @Body =  @BrokerOrderID + ' - Requested Ship Week changed from ' + cast(@ExistingShipDate as varchar(30)) + ' to ' + @RequestedShipDate + '.  Order NOT updated, just for you to check into. ' 

   	--send the email to Alma
	exec EmailQueueAdd 'B2B Order Acknowledgement',@Body ,0,'B2B Ack','CustomerService@eplantsource.com','CustomerService@eplantsource.com','','',@GrowerOrderGuid,0,0,0

END





SELECT
m.c.value('@BrokerMaterialNumber', 'varchar(max)') as  BrokerMaterialNumber,  
m.c.value('@VendorMaterialNumber', 'varchar(max)') as VendorMaterialNumber,
m.c.value('@BrokerLineItemNumber', 'varchar(max)') as BrokerLineItemNumber,
m.c.value('@MaterialDescription', 'varchar(max)') as MaterialDescription,
m.c.value('@OrderdQty', 'bigint') as OrderdQty,
m.c.value('@ConfirmedQty', 'bigint') as ConfirmedQty,
m.c.value('@LineItemStatus', 'varchar(10)') as LineItemStatus,
m.c.value('@RejectionCode', 'varchar(10)') as RejectionCode,
m.c.value('@ScheduledShipDate', 'datetime') as ScheduledShipDate,
m.c.value('@LineItemComment', 'varchar(max)') as LineItemComment,
m.c.value('@PlantCode', 'varchar(max)') as PlantCode
into #OrderLine
FROM 
@XML.nodes('BrokerAcknowledgement/OrderDetails/Item') as m(c)

If @iDebug = 1
BEGIN
	select @GrowerOrderGuid,@BrokerOrderID,@VendorOrderID,@RequestedShipDate,@ShipMethod
	select * from #OrderLine
END


--check the ship week on all items
set @ConvertedShipDate = (select Max (ScheduledShipDate) from #OrderLine)
If NOT (@ConvertedShipDate between  @ExistingShipDate and dateadd(d,7,@ExistingShipDate))
BEGIN
	set @Body =  @BrokerOrderID + ' - Some line items are out of ship week range. Example is ' + cast(@ConvertedShipDate as varchar(30)) + '.' 
   	--send the email to Alma
	exec EmailQueueAdd 'B2B Order Acknowledgement',@Body ,0,'B2B Ack','CustomerService@eplantsource.com','CustomerService@eplantsource.com','','',@GrowerOrderGuid,0,0,0
END
set @ConvertedShipDate = (select Min(ScheduledShipDate) from #OrderLine)
If NOT (@ConvertedShipDate between  @ExistingShipDate and dateadd(d,7,@ExistingShipDate))
BEGIN
	set @Body =  @BrokerOrderID + ' - Some line items are out of ship week range. Example is ' + cast(@ConvertedShipDate as varchar(30)) + '.' 
   	--send the email to Alma
	exec EmailQueueAdd 'B2B Order Acknowledgement',@Body ,0,'B2B Ack','CustomerService@eplantsource.com','CustomerService@eplantsource.com','','',@GrowerOrderGuid,0,0,0
END


select * from lookup where path like 'Code/OrderLineStatus%'

-- If the Ordered qty is different, we need to find out what is going on.
--Unless in OrderPLace (adds) or Grower Edit (updates), then we leave it alone.
Update OrderLine
set OrderLineStatusLookupGuid = (select Guid from lookup where path = 'Code/OrderLineStatus/SupplierB2BError')
From OrderLineViewShowInactiveProduct olv
Join OrderLine ol on olv.OrderLineGuid = ol.Guid
Join #OrderLine on ol.LineNumber = #OrderLine.BrokerLineItemNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
WHERE
 #OrderLine.OrderdQty <> ol.QtyOrdered
 and #OrderLine.ConfirmedQty > 0 --Not cancelled in this run, handled below
 and ol.OrderLineStatusLookupGuid not in (	select Guid from lookup 
											where path in (
												'Code/OrderLineStatus/Ordered',
												 'Code/OrderLineStatus/GrowerEdit'
												)
											)
-- If the confirmed qty is different, we need to add comment and update status
Insert into GrowerOrderHistory
SELECT 
NewID(),NULL,@GrowerOrderGuid,
'Order for ' + cast(#OrderLine.OrderdQty as varchar(20)) + ' ' + #OrderLine.MaterialDescription + ' changed to '  + cast(#OrderLine.ConfirmedQty as varchar(20)),
0,
'27DE09B0-FDD9-40E1-BBAC-32A66081E3C9', --customer service guid
getdate(),
(select guid from Lookup where path = 'Logging/LogType/Grower/OrderLineChange')
FROM OrderLineViewShowInactiveProduct olv
Join OrderLine ol on olv.OrderLineGuid = ol.Guid
Join #OrderLine on ol.LineNumber = #OrderLine.BrokerLineItemNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
WHERE 
#OrderLine.ConfirmedQty > 0 --Not cancelled in this run
AND #OrderLine.ConfirmedQty <> #OrderLine.OrderdQty
AND ol.OrderLineStatusLookupGuid not in (	select Guid from lookup 
											where path in (
												'Code/OrderLineStatus/Ordered',
												 'Code/OrderLineStatus/GrowerEdit',
												 'Code/OrderLineStatus/SupplierB2BError'
												)
											)

--now update the order line on our side.
Update OrderLine
set 
--select OrderLine.*,
OrderLineStatusLookupGuid = (select Guid from lookup where path = 'Code/OrderLineStatus/SupplierEdit'),
QtyOrdered = #OrderLine.ConfirmedQty,
DateLastChanged = getdate(),
DateQtyLastChanged = getdate(),
LineComment = #OrderLine.LineItemComment  --may need to handle nulls here or only write if not null and not ''
FROM OrderLineViewShowInactiveProduct olv
Join OrderLine ol on olv.OrderLineGuid = ol.Guid
Join #OrderLine on ol.LineNumber = #OrderLine.BrokerLineItemNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
WHERE
	#OrderLine.ConfirmedQty <> ol.QtyOrdered
	AND #OrderLine.ConfirmedQty > 0
	AND ol.OrderLineStatusLookupGuid not in (	select Guid from lookup 
											where path in (
												'Code/OrderLineStatus/Ordered',
												 'Code/OrderLineStatus/GrowerEdit',
												 'Code/OrderLineStatus/SupplierB2BError'
												)
											)

-- If cancelled, we need to add comment and update status
Insert into GrowerOrderHistory
SELECT 
NewID(),NULL,@GrowerOrderGuid,
'Supplier Cancelled or Acknowledged cancelled order for ' + cast(#OrderLine.OrderdQty as varchar(20)) + ' ' + #OrderLine.MaterialDescription + '.',
0,
'27DE09B0-FDD9-40E1-BBAC-32A66081E3C9', --customer service guid
getdate(),
(select guid from Lookup where path = 'Logging/LogType/Grower/OrderLineCancel')
FROM OrderLineViewShowInactiveProduct olv
Join OrderLine ol on olv.OrderLineGuid = ol.Guid
Join #OrderLine on ol.LineNumber = #OrderLine.BrokerLineItemNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
WHERE 
#OrderLine.ConfirmedQty = 0 
AND #OrderLine.LineItemStatus = 'C' --Cancelled in this run
AND ol.OrderLineStatusLookupGuid != (select Guid from lookup where path = 'Code/OrderLineStatus/Cancelled')
AND ol.OrderLineStatusLookupGuid != (select Guid from lookup where path = 'Code/OrderLineStatus/SupplierB2BError')

--Now Cancel the items on our side.
Update OrderLine
set 
--select OrderLine.*,
OrderLineStatusLookupGuid = (select Guid from lookup where path = 'Code/OrderLineStatus/SupplierCancelled'),
QtyOrdered = #OrderLine.ConfirmedQty,
DateLastChanged = getdate(),
DateQtyLastChanged = getdate(),
LineComment = #OrderLine.LineItemComment  --may need to handle nulls here or only write if not null and not ''

FROM OrderLineViewShowInactiveProduct olv
Join OrderLine ol on olv.OrderLineGuid = ol.Guid
Join #OrderLine on ol.LineNumber = #OrderLine.BrokerLineItemNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
WHERE
	#OrderLine.ConfirmedQty = 0 
AND #OrderLine.LineItemStatus = 'C' --Cancelled in this run
AND ol.OrderLineStatusLookupGuid != (select Guid from lookup where path = 'Code/OrderLineStatus/Cancelled')
AND ol.OrderLineStatusLookupGuid != (select Guid from lookup where path = 'Code/OrderLineStatus/SupplierB2BError')

-- If the item is added, we need to add comment and add line items
Declare @iCountHistoryAdd int

Insert into GrowerOrderHistory
SELECT 
NewID(),NULL,@GrowerOrderGuid,
'Order for ' + cast(#OrderLine.OrderdQty as varchar(20)) + ' ' +  #OrderLine.MaterialDescription + ' added.',
0,
'27DE09B0-FDD9-40E1-BBAC-32A66081E3C9', --customer service guid
getdate(),
(select guid from Lookup where path = 'Logging/LogType/Grower/OrderLineAdd')
FROM  #OrderLine
WHERE #OrderLine.BrokerLineItemNumber
Not in(
	SELECT ol.LineNumber from OrderLineViewShowInactiveProduct olv
	Join OrderLine ol on olv.OrderLineGuid = ol.Guid
	where olv.GrowerOrderGuid = @GrowerOrderGuid
)
set @iCountHistoryAdd = @@ROWCOUNT



--to do, if more than one supplier order, pick the right one. based on plant codes
Declare @SupplierOrderGuid uniqueidentifier
set @SupplierOrderGuid = (select top 1 Guid from supplierOrder where GrowerOrderGuid = @GrowerOrderGuid)

Declare @iCountLinesAdd int
--add the lines
Insert into OrderLine
SELECT 
NewID(),NULL,
@SupplierOrderGuid,
(select top 1 guid from Product where SupplierIdentifier = #OrderLine.VendorMaterialNumber)  ,  --to do ensure right Product
#OrderLine.ConfirmedQty,
0,
NULL,
0,
(SELECT Guid from lookup where path = 'Code/OrderLineStatus/SupplierAdd'),
(SELECT Guid from Lookup where path = 'Code/ChangeType/NoChange'),
getdate(),
getdate(),
0,
NULL,
getdate(),
#OrderLine.BrokerLineItemNumber,
LineComment = #OrderLine.LineItemComment  --may need to handle nulls here or only write if not null and not ''

FROM  #OrderLine
WHERE #OrderLine.BrokerLineItemNumber
Not in(
	SELECT ol.LineNumber from OrderLineViewShowInactiveProduct olv
	Join OrderLine ol on olv.OrderLineGuid = ol.Guid
	where olv.GrowerOrderGuid = @GrowerOrderGuid
)
set @iCountLinesAdd = @@ROWCOUNT

If @iCountHistoryAdd != @iCountLinesAdd
Begin
	set @Body =  @BrokerOrderID + ' - Comments for lines added (' + cast(@iCountHistoryAdd as varchar(20)) + ') does not equal lines added (' + cast(@iCountLinesAdd as varchar(20)) + '). Some unknown items on B2B order.' 
   	--send the email to Alma
	exec EmailQueueAdd 'B2B Order Acknowledgement',@Body ,0,'B2B Ack','CustomerService@eplantsource.com','CustomerService@eplantsource.com','','',@GrowerOrderGuid,0,0,0
End

--now confirm the items that should be
Update OrderLine
set 
--select OrderLine.*,
OrderLineStatusLookupGuid = (select Guid from lookup where path = 'Code/OrderLineStatus/SupplierConfirmed')
FROM OrderLineViewShowInactiveProduct olv
Join OrderLine ol on olv.OrderLineGuid = ol.Guid
Join #OrderLine on ol.LineNumber = #OrderLine.BrokerLineItemNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
WHERE
#OrderLine.ConfirmedQty > 0 
AND #OrderLine.ConfirmedQty = ol.QtyOrdered
AND ol.OrderLineStatusLookupGuid not in  
		(select Guid 
		from lookup 
		where path in( 'Code/OrderLineStatus/SupplierB2BError',
						'Code/OrderLineStatus/CustomerNotifiedAfterSupplierConfirmed',
						'Code/OrderLineStatus/SupplierEdit',
						'Code/OrderLineStatus/SupplierAdd',
						'Code/OrderLineStatus/SupplierCancelled'			
						)
		)

--now mark lines we have that they don't
Update OrderLine
set 
OrderLineStatusLookupGuid = (select Guid from lookup where path = 'Code/OrderLineStatus/SupplierB2BError'),
LineComment = 'No Line Number Match'
FROM OrderLineViewShowInactiveProduct olv
Join OrderLine ol on olv.OrderLineGuid = ol.Guid
WHERE
	olv.GrowerOrderGuid = @GrowerOrderGuid
	AND ol.LineNumber not in (select #OrderLine.BrokerLineItemNumber from #OrderLine)
	AND ol.OrderLineStatusLookupGuid not in (	select Guid from lookup 
											where path in (
												'Code/OrderLineStatus/Ordered',
												 'Code/OrderLineStatus/GrowerEdit',
												 'Code/OrderLineStatus/SupplierB2BError'
												)
											)

--check the material ID on all items
declare @NoMatchCount int
set @NoMatchCount = (
	select count(*) 
	FROM OrderLineViewShowInactiveProduct olv
	Join OrderLine ol on olv.OrderLineGuid = ol.Guid
	Join Product p on ol.ProductGuid = p.Guid
	Join #OrderLine on ol.LineNumber = #OrderLine.BrokerLineItemNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
	WHERE rtrim(#OrderLine.BrokerMaterialNumber) <> rtrim(p.ID)
	AND ol.OrderLineStatusLookupGuid not in (	select Guid from lookup 
												where path in (
												'Code/OrderLineStatus/Ordered'
												)
											)
)
If @NoMatchCount > 0
BEGIN
	set @Body =  @BrokerOrderID + ' - Some line items do not have the correct Product ID.' + cast(@NoMatchCount as varchar(20)) + ' items.' 
   	--send the email to Alma
	exec EmailQueueAdd 'B2B Order Acknowledgement',@Body ,0,'B2B Ack','CustomerService@eplantsource.com','CustomerService@eplantsource.com','','',@GrowerOrderGuid,0,0,0
END

--check the vendor Material Number on all items
set @NoMatchCount = (
	select count(*) 
	FROM OrderLineViewShowInactiveProduct olv
	Join OrderLine ol on olv.OrderLineGuid = ol.Guid
	Join Product p on ol.ProductGuid = p.Guid
	Join #OrderLine on ol.LineNumber = #OrderLine.BrokerLineItemNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
	WHERE rtrim(#OrderLine.VendorMaterialNumber) <> rtrim(p.SupplierIdentifier)
	AND ol.OrderLineStatusLookupGuid not in (	select Guid from lookup 
												where path in (
												'Code/OrderLineStatus/Ordered'
												)
											)
)
If @NoMatchCount > 0
BEGIN
	set @Body =  @BrokerOrderID + ' - Some line items do not have the correct Supplier Identifier.' + cast(@NoMatchCount as varchar(5)) + ' items.' 
   	--send the email to Alma
	exec EmailQueueAdd 'B2B Order Acknowledgement',@Body ,0,'B2B Ack','CustomerService@eplantsource.com','CustomerService@eplantsource.com','','',@GrowerOrderGuid,0,0,0
END
--check the Line Number on all items
set @NoMatchCount = (
	select count(*) 
	FROM OrderLineViewShowInactiveProduct olv
	Join OrderLine ol on olv.OrderLineGuid = ol.Guid
	Join Product p on ol.ProductGuid = p.Guid
	left Join #OrderLine on ol.LineNumber = #OrderLine.BrokerLineItemNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
	WHERE
	olv.GrowerOrderGuid = @GrowerOrderGuid
	and #OrderLine.VendorMaterialNumber is null
	AND ol.OrderLineStatusLookupGuid not in (	select Guid from lookup 
												where path in (
												'Code/OrderLineStatus/Ordered'
												)
											)
)
If @NoMatchCount > 0
BEGIN
	set @Body =  @BrokerOrderID + ' - Some line items are missing on Dummen Acknowledgement. ' + cast(@NoMatchCount as varchar(5)) + ' items.' 
   	--send the email to Alma
	exec EmailQueueAdd 'B2B Order Acknowledgement',@Body ,0,'B2B Ack','CustomerService@eplantsource.com','CustomerService@eplantsource.com','','',@GrowerOrderGuid,0,0,0
END




--Now update the comments
Update OrderLine
set 
--select OrderLine.*,
LineComment =  #OrderLine.LineItemComment
FROM OrderLineViewShowInactiveProduct olv
Join OrderLine ol on olv.OrderLineGuid = ol.Guid
Join #OrderLine on ol.LineNumber = #OrderLine.BrokerLineItemNumber and olv.GrowerOrderGuid = @GrowerOrderGuid

drop table #OrderLine

set @StatusMessage = @StatusMessage + ' - Finished'

Update 
epsB2BProd.dbo.B2BIntegrationXML
set B2BStatus = 'Processed',
StatusMessage = @StatusMessage
WHERE ID = @B2BID
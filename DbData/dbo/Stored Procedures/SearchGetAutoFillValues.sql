﻿



CREATE procedure [dbo].[SearchGetAutoFillValues]
	@theSearchTerm AS nVarchar(200)
AS

		

	Select top 20 --ID,Guid,DateDeactivated,SearchString,RelatedSearches,ResultCount
	SearchString
	From SearchTerm
	Where SearchString like @theSearchTerm
		and IncludeinAutofill = 1
	Order by SearchString
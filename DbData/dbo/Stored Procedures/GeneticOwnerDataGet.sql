﻿CREATE PROCEDURE [dbo].[GeneticOwnerDataGet]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NCHAR(56) = NULL,
	@ProgramTypeCode AS NCHAR(50) = null,
	@ProductFormCategoryCode AS NCHAR(50) = null,
	@SupplierCodeList AS NVARCHAR(500) = null,
	@SpeciesCodeList AS NVARCHAR(500) = null
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('ProgramTypeCode',@ProgramTypeCode,1) +
		dbo.XmlSegment('ProductFormCategoryCode',@ProductFormCategoryCode,1) +
		dbo.XmlSegment('SupplierCodeList',@SupplierCodeList,1) +
		dbo.XmlSegment('SpeciesCodeList',@SpeciesCodeList,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GeneticOwnerDataGet',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT	IF @ProgramTypeCode Like '%*%'
		SET @ProgramTypeCode = NULL

	IF LEN(RTRIM(@ProgramTypeCode)) > 3
		BEGIN
			SELECT @ProgramTypeCode = Code FROM ProgramType WHERE Name LIKE RTRIM(@ProgramTypeCode) + '%'

			IF @ProgramTypeCode IS NULL
				SELECT @ProgramTypeCode = Code FROM ProgramType WHERE Name LIKE '%' + RTRIM(@ProgramTypeCode) + '%'

			IF @ProgramTypeCode IS NULL
				SET @ProgramTypeCode = '*'
		END

	IF @ProductFormCategoryCode Like '%*%'
		SET @ProductFormCategoryCode = NULL

	IF LEN(RTRIM(@ProductFormCategoryCode)) > 3
		BEGIN
			SELECT @ProductFormCategoryCode = Code FROM ProductFormCategory WHERE Name LIKE RTRIM(@ProductFormCategoryCode) + '%'

			IF @ProductFormCategoryCode IS NULL
				SELECT @ProductFormCategoryCode = Code FROM ProductFormCategory WHERE Name LIKE '%' + RTRIM(@ProductFormCategoryCode) + '%'

			IF @ProductFormCategoryCode IS NULL
				SET @ProductFormCategoryCode = '*'
		END

	IF @SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%*%'
		SET @SupplierCodeList = NULL
	ELSE
		SET @SupplierCodeList = ',' + @SupplierCodeList + ','

	IF @SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%*%'
		SET @SpeciesCodeList = NULL
	ELSE
		SET @SpeciesCodeList = ',' + @SpeciesCodeList + ','

	DECLARE @LocalTempGeneticOwnerData TABLE
	(
		GeneticOwnerGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalTempGeneticOwnerData
	SELECT
		GeneticOwnerGuid
	FROM ProductView
	WHERE
		(@ProgramTypeCode IS NULL OR ProgramTypeCode=@ProgramTypeCode) AND
		(@ProductFormCategoryCode IS NULL OR ProductFormCategoryCode=@ProductFormCategoryCode) AND
		(@SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%,' + RTRIM(SupplierCode) + ',%') AND
		(@SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%,' + RTRIM(SpeciesCode) + ',%')
	GROUP BY
		GeneticOwnerGuid

	SELECT GeneticOwner.*
	FROM GeneticOwner
	WHERE GeneticOwner.Guid IN
	(SELECT DISTINCT(GeneticOwnerGuid) FROM @LocalTempGeneticOwnerData)

	EXECUTE EventLogAdd
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GeneticOwnerDataGet',
		@ObjectGuid=@OriginalEventLogGuid
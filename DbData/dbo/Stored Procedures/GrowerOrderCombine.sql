﻿


CREATE PROCEDURE [dbo].[GrowerOrderCombine]
	@GrowerOrderKeepOrderNo AS NVARCHAR(10),
	@GrowerOrderDeleteOrderNo AS NVARCHAR(10),
	@Message nvarchar(50) Output
AS
	
declare @SupplierOrderToKeep uniqueidentifier
declare @DeletePO nvarchar(20)
declare @DeleteDesc nvarchar(50)


declare @GrowerOrderDeleteGuid uniqueidentifier
declare @GrowerOrderKeepGuid uniqueidentifier
--set @OrderToKeep = 'A00092062'
--set @OrderToDelete = 'A00092227' 

set @GrowerOrderKeepGuid = (select guid from GrowerOrder where orderno = @GrowerOrderKeepOrderNo)
set @GrowerOrderDeleteGuid = (select guid from GrowerOrder where orderno = @GrowerOrderDeleteOrderNo)
set @Message = ''

If @GrowerOrderKeepGuid is NULL
Begin
	set @Message = 'Order to Keep not found'
	return -1
End

If @GrowerOrderDeleteGuid is NULL
Begin
	set @Message = 'Order to Delete not found'
	return -1
End

declare @SupplierOrderCountKeep int
set @SupplierOrderCountKeep  = (
	Select count(s.Name) 
	from GrowerOrder o
	Join supplierOrder so on o.guid = so.GrowerOrderGuid
	Join supplier s on s.guid = so.SupplierGuid
	where o.guid = @GrowerOrderKeepGuid
	)
If @SupplierOrderCountKeep > 1 
Begin
	set @Message = 'Order to Keep has more than one supplier'
	return -1
End

declare @SupplierOrderCountDelete int
set @SupplierOrderCountDelete  = (
	Select count(s.Name) 
	from GrowerOrder o
	Join supplierOrder so on o.guid = so.GrowerOrderGuid
	Join supplier s on s.guid = so.SupplierGuid
	where o.guid = @GrowerOrderDeleteGuid
	)
If @SupplierOrderCountDelete > 1 
Begin
	set @Message = 'Order to Delete has more than one supplier'
	return -1
End

declare @SupplierKeepID int
set @SupplierKeepID  = (
	Select s.ID 
	from GrowerOrder o
	Join supplierOrder so on o.guid = so.GrowerOrderGuid
	Join supplier s on s.guid = so.SupplierGuid
	where o.guid = @GrowerOrderKeepGuid
	)
declare @SupplierDeleteID int
set @SupplierDeleteID  = (
	Select s.ID 
	from GrowerOrder o
	Join supplierOrder so on o.guid = so.GrowerOrderGuid
	Join supplier s on s.guid = so.SupplierGuid
	where o.guid = @GrowerOrderKeepGuid
	)

If @SupplierDeleteID <> @SupplierKeepID 
Begin
	set @Message = 'Supplier not the same'
	return -1
End




--Check Ship Methods

--Combine the P.O. and Description
--Update GrowerOrder set 
--CustomerPoNo = CustomerPoNo + ' - ' + (select CustomerPoNo from GrowerOrder where OrderNo=@OrderToDelete),
--Description = Description + ' - ' + (select Description from GrowerOrder where OrderNo=@OrderToDelete)
--Where OrderNo=@OrderToKeep



set @SupplierOrderToKeep = (
	select top 1 so.guid 
	from SupplierOrder so
	Join GrowerORder o on so.GrowerOrderGuid = o.guid
	Where o.Guid = @GrowerOrderKeepGuid)

update OrderLine set SupplierOrderGuid = @SupplierOrderToKeep
Where guid in (
Select ol.Guid 
From OrderLine ol 
Join supplierOrder so on ol.supplierOrderGuid = so.guid
Join GrowerORder o on so.GrowerOrderGuid = o.guid
Where o.Guid = @GrowerOrderDeleteGuid)

delete from SupplierOrderfee 
where supplierorderguid in(
	select so.Guid 
	from supplierOrder so
	Join GrowerORder o on so.GrowerOrderGuid = o.guid
	Where o.Guid = @GrowerOrderDeleteGuid
	)

delete from SupplierOrder where guid in (
select so.Guid 
from supplierOrder so
Join GrowerORder o on so.GrowerOrderGuid = o.guid
Where o.Guid = @GrowerOrderDeleteGuid)

delete from GrowerOrderHistory where GrowerOrderGuid in (
select o.Guid 
from GrowerOrder o 
Where o.Guid = @GrowerOrderDeleteGuid)

delete from GrowerOrder where Guid = @GrowerOrderDeleteGuid

exec supplierorderrecalcfees null,null,@SupplierOrderToKeep

Insert into GrowerOrderHistory 
values(
		newid(),null,@GrowerOrderKeepGuid,
		'Order ' + @GrowerOrderDeleteOrderNo + ' combined into this order',0,
		(select guid from person where ID = 161),
		getdate(),(select guid from lookup where path = 'Logging/LogType/CommentExternal')
		)

set @Message = 'Orders combined and fees recalculated'
return 0
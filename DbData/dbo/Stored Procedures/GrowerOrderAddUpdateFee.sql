﻿



CREATE PROCEDURE [dbo].[GrowerOrderAddUpdateFee]
	@GrowerOrderFeeGuid AS UNIQUEIDENTIFIER,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER,
	@FeeTypeCode AS NVARCHAR(200) = '',
	@DiscountPercent AS DECIMAL(8,3),
	@FeeAmount AS	DECIMAL(8,2),
	@FeeStatusCode AS NVARCHAR(200) = '',
	@FeeDescription AS NVARCHAR(500) = '',
	@IsManual AS BIT

AS

	DECLARE @iCount INTEGER





					
	IF @GrowerOrderFeeGuid = '00000000-0000-0000-0000-000000000000'
	BEGIN
		INSERT INTO GrowerOrderfee VALUES(
		NEWID(),NULL,@GrowerOrderGuid,
		(SELECT guid FROM Lookup WHERE path = 'Code/GrowerOrderFeeType/' + @FeeTypeCode),
		COALESCE(-1 * @DiscountPercent * @FeeAmount,0),
		(SELECT guid FROM Lookup WHERE path = 'Code/GrowerOrderFeeStatus/' + @FeeStatusCode),
		@FeeDescription,
		@IsManual
		)	

		--select guid,code,name,path from Lookup where path like 'Code/GrowerOrderFeeType%'
		--Update lookup set code = 'EPSDiscount' where guid  = '7D9D0263-854C-40C4-91FE-D13EC065632C'
	END
	ELSE
	BEGIN
		UPDATE GrowerOrderfee
		SET 
			Amount = COALESCE(-1 * @DiscountPercent * @FeeAmount,0),
			FeeStatXLookupGuid = (SELECT guid FROM Lookup WHERE path = 'Code/GrowerOrderFeeStatus/' + @FeeStatusCode),
			FeeTypeLookupGuid = (SELECT guid FROM Lookup WHERE path = 'Code/GrowerOrderFeeType/' + @FeeTypeCode),
			FeeDescription = @FeeDescription,
			IsManual = @IsManual
		WHERE	Guid = @GrowerOrderFeeGuid
				

	END
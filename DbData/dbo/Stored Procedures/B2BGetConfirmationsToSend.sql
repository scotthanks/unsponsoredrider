﻿



CREATE PROCEDURE [dbo].[B2BGetConfirmationsToSend]
	 

AS
	--select * from grower where name = 'Color Point'
	--select * from grower where guid = '341AAAC5-518F-492E-8C8B-FA21789A15E0'
	select 
	b2b.B2BStatus,
	b2b.StatusMessage,
	g.Name as GrowerName,
	g.Email as GrowerEmail,
	g.B2BOrderURL,
	b2b.OrderGuid
	from epsb2bprod.dbo.B2BIntegrationXML b2b
	Join Grower g on b2b.GrowerGuid = g.Guid
	where b2bType  in ( 'Order Place','Order Update')
	and communicationdirection = 'in'
	and B2BStatus = 'Sending Confirmation'

	Update epsb2bprod.dbo.B2BIntegrationXML 
	set B2BStatus = 'Confirmation Sent'
	where 
		b2bType  in ('Order Place','Order Update')
		and communicationdirection = 'in'
		and B2BStatus = 'Sending Confirmation'
﻿CREATE PROCEDURE [dbo].[PersonGetGuid]
	@UserCode AS NVARCHAR(56) = NULL,
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@ProcessLookupCode AS NVARCHAR(50),
	@PersonGuid AS UNIQUEIDENTIFIER OUT
AS
	SET @UserCode = LTRIM(RTRIM(@UserCode))
	IF @UserCode = ''
		SET @UserCode = NULL

	SET @PersonGuid = NULL

	IF @PersonGuid IS NULL AND @UserGuid IS NOT NULL
		BEGIN
			SELECT @PersonGuid = PersonGuid 
			FROM PersonView 
			WHERE UserGuid = @UserGuid

			IF @PersonGuid IS NULL
				EXECUTE EventLogKeyNotFound
					@UserGuid=@UserGuid,
					@UserCode=@UserCode,
					@KeyName='UserGuid',
					@Key=@UserGuid,
					@ObjectTypeLookupCode='Person',
					@ProcessLookupCode=@ProcessLookupCode
		END

	-- DEPRICATED OBSOLETE
	IF @PersonGuid IS NULL AND @UserCode IS NOT NULL
		BEGIN
			SELECT @PersonGuid = PersonGuid 
			FROM PersonView 
			WHERE UserCode = @UserCode

			IF @PersonGuid IS NULL
				EXECUTE EventLogKeyNotFound
					@UserGuid=@UserGuid,
					@UserCode=@UserCode,
					@KeyName='UserCode',
					@Key=@UserCode,
					@ObjectTypeLookupCode='Person',
					@ProcessLookupCode=@ProcessLookupCode
		END
﻿





CREATE PROCEDURE [dbo].[OrderLinesGet]
	 @GrowerOrderGuid as uniqueidentifier
	 

AS
	Declare @SellerCode nvarchar(10)

	set @SellerCode = (select Max(se.Code) from GrowerOrder o
						Join SupplierOrder so on o.Guid = so.GrowerOrderGuid
						Join OrderLine ol on so.Guid = ol.SupplierORderGuid
						Join Product p on ol.ProductGuid = p.Guid
						Join Program pr on p.ProgramGuid = pr.Guid
						Join Seller se on pr.SellerGuid = se.Guid
						where o.Guid = @GrowerOrderGuid )


--	TagRatio is “N”, whenever there are no tags.  Use “Tag Exclusion Cust” as Tag Ratio Description.  See above.  
--	A “1:1 Vendor Std Tag” for “V1” etc.   
--	“V0”  means a 1:0 ratio….   I assume 1 tag is put on shipment, but verifying this special case.
--	Tag ratio codes used are “V0-V9” as standard for Ball.   Ratios being 1:1 for V1.   1:2 for V2, 1 tag for every 2 cuttings and so on.   I see tagratios  if specified at header usually propagate down to line item level.  


	Select 
	 ol.Guid
	 ,ol.LineNumber as ID  
	 ,p.ID as ProductID
	 ,o.guid as GrowerOrderGuid
	 ,so.guid as SupplierOrderGuid
	 ,s.Code as SupplierCode
	 ,s.Name as SupplierName
	 ,@SellerCode as SellerCode  
     ,ol.ProductGuid               
     ,pf.Code as ProductForm
     ,rtrim(cast(pfc.code as nvarchar(10))) as ProductFormCategory
	 ,Case when ol.QtyOrdered = 0 AND ol.DateQtyLastChanged > so.DateLastSenttoSupplier then 'C'
		   when so.DateLastSenttoSupplier is null OR  ol.DateQtyLastChanged is null then 'N'	  
	       when ol.DateQtyLastChanged > so.DateLastSenttoSupplier then 'M' 
	       when ol.DateQtyLastChanged <= so.DateLastSenttoSupplier then '' 
		   else '' end as OrderB2BStatus 
	 ,Case when rtrim(pr.Code) = 'DGUURCA' then 'DORO' else s.DefaultB2BLocationCode end as SellerPlantCode                  
	 ,'unknown' as SellerPlantCodeDescription -- Todo                  
     ,rtrim(cast(p.SupplierIdentifier as nvarchar(50))) as SupplierProductIdentifier                   
     ,rtrim(cast(p.SupplierDescription as nvarchar(50))) as SupplierProductDescription                   
     ,ol.QtyOrdered as OrderQty
	 ,ol.LineComment
	 ,rtrim(sotr.Code) as TagRatioCode
	 ,ols.Name as OrderLineStatus 
	 ,ol.ActualPrice                  
                         
 	from OrderLine ol 
	Join Product p on ol.ProductGuid = p.Guid
	Join Program pr on p.ProgramGuid = pr.Guid
	Join ProductForm pf on p.ProductFormGuid = pf.Guid
	Join ProductFormCategory pfc on pf.ProductFormCategoryGuid = pfc.Guid
	Join SupplierOrder so on ol.SupplierOrderGuid = so.Guid
	Join Supplier s on so.SupplierGuid = s.Guid
	Join GrowerOrder o on so.GrowerOrderGuid = o.Guid
	Join Lookup sotr on so.TagRatioLookupGuid = sotr.Guid
	Join Lookup ols on ol.OrderLineStatusLookupGuid = ols.Guid
	

	WHERE 
		1 = 1
		AND o.Guid = @GrowerOrderGuid
	
       
	ORDER BY ol.ID
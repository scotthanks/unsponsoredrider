﻿


CREATE PROCEDURE [dbo].[AvailabilityUploadStartJob]
	 @UserName as nvarchar(56),
	 @SupplierCode as nvarchar(10),
	 @JobNumber as bigint Output

AS
	
		Insert into ReportedAvailabilityUploadJob values(
		NewID(),
		(Select UserGuid from person where UserCode = @UserName),
		getdate(),
		@SupplierCode
		)
	
		


	SELECT @JobNumber = SCOPE_IDENTITY()
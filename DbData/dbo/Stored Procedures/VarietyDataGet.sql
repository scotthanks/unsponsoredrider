﻿






CREATE PROCEDURE [dbo].[VarietyDataGet]
	@UserGuid As UniqueIdentifier,
	@ProgramTypeCode AS NCHAR(50) = null,
	@ProductFormCategoryCode AS NCHAR(50) = null,
	@GeneticOwnerCodeList AS NVARCHAR(3000) = null,
	@SupplierCodeList AS NVARCHAR(3000) = null,
	@SpeciesCodeList AS NVARCHAR(3000) = null,
	@VarietyCode AS NVARCHAR(50) = null
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('ProgramTypeCode',@ProgramTypeCode,1) +
		dbo.XmlSegment('ProductFormCategoryCode',@ProductFormCategoryCode,1) +
		dbo.XmlSegment('GeneticOwnerCodeList',@GeneticOwnerCodeList,1) +
		dbo.XmlSegment('SupplierCodeList',@SupplierCodeList,1) +
		dbo.XmlSegment('SpeciesCodeList',@SpeciesCodeList,1) +
		dbo.XmlSegment('VarietyCode',@VarietyCode,1),
		0
	)

	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER	
	EXECUTE EventLogAdd
		--@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='VarietyDataGet',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT


	Declare @GrowerGuid as UniqueIdentifier
	set @GrowerGuid  = (select top 1 GrowerGuid from Person where UserGuid = @UserGuid)
	
	 
	IF @ProgramTypeCode Like '%*%'
		SET @ProgramTypeCode = NULL

	IF LEN(RTRIM(@ProgramTypeCode)) > 3
		BEGIN
			SELECT @ProgramTypeCode = Code FROM ProgramType WHERE Name LIKE RTRIM(@ProgramTypeCode) + '%'

			IF @ProgramTypeCode IS NULL
				SELECT @ProgramTypeCode = Code FROM ProgramType WHERE Name LIKE '%' + RTRIM(@ProgramTypeCode) + '%'

			IF @ProgramTypeCode IS NULL
				SET @ProgramTypeCode = '*'
		END

	PRINT '@ProgramTypeCode=' + @ProgramTypeCode

	IF @ProductFormCategoryCode Like '%*%'
		SET @ProductFormCategoryCode = NULL

	IF LEN(RTRIM(@ProductFormCategoryCode)) > 3
		BEGIN
			SELECT @ProductFormCategoryCode = Code FROM ProductFormCategory WHERE Name LIKE RTRIM(@ProductFormCategoryCode) + '%'

			IF @ProductFormCategoryCode IS NULL
				SELECT @ProductFormCategoryCode = Code FROM ProductFormCategory WHERE Name LIKE '%' + RTRIM(@ProductFormCategoryCode) + '%'

			IF @ProductFormCategoryCode IS NULL
				SET @ProductFormCategoryCode = '*'
		END

	PRINT '@ProductFormCategoryCode=' + @ProductFormCategoryCode

	IF @GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%*%'
		SET @GeneticOwnerCodeList = NULL
	ELSE
		SET @GeneticOwnerCodeList = ',' + @GeneticOwnerCodeList + ','

	PRINT '@GeneticOwnerCodeList=' + @GeneticOwnerCodeList

	IF @SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%*%'
		SET @SupplierCodeList = NULL
	ELSE
		SET @SupplierCodeList = ',' + @SupplierCodeList + ','

	PRINT '@SupplierCodeList=' + @SupplierCodeList

	IF @SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%*%' OR @SpeciesCodeList = ''
		SET @SpeciesCodeList = NULL
	ELSE
		SET @SpeciesCodeList = ',' + @SpeciesCodeList + ','

	PRINT '@SpeciesCodeList=' + @SpeciesCodeList

	IF @VarietyCode Like '%*%'
		SET @VarietyCode = NULL

	PRINT '@VarietyCode=' + @VarietyCode

	DECLARE @LocalTempVarietyData TABLE
	(
		GeneticOwnerGuid UNIQUEIDENTIFIER,
		VarietyGuid UNIQUEIDENTIFIER,
		SpeciesGuid UNIQUEIDENTIFIER,
		VarietyColorLookupGuid UNIQUEIDENTIFIER,
		VarietyHabitLookupGuid UNIQUEIDENTIFIER,
		VarietyVigorLookupGuid UNIQUEIDENTIFIER,
		VarietyImageSetGuid UNIQUEIDENTIFIER
	)

	--;with ex (ProductGuid)
	--as 
	--(Select ProductGuid
	--From GrowerProductExclusive
	--where GrowerGuid = @GrowerGuid)

	INSERT INTO @LocalTempVarietyData
	SELECT
		GeneticOwnerGuid,
		VarietyGuid,
		SpeciesGuid,
		VarietyColorLookupGuid,
		VarietyHabitLookupGuid,
		VarietyVigorLookupGuid,
		VarietyImageSetGuid
	FROM ProductView
	--Left Join ex on ProductView.ProductGuid = ex.ProductGuid
	WHERE
		(@ProgramTypeCode IS NULL OR ProgramTypeCode=@ProgramTypeCode) AND
		(@ProductFormCategoryCode IS NULL OR ProductFormCategoryCode=@ProductFormCategoryCode) AND
		(@GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%,' + RTRIM(GeneticOwnerCode) + ',%') AND
		(@SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%,' + RTRIM(SupplierCode) + ',%') AND
		(@SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%,' + RTRIM(SpeciesCode) + ',%' or @SpeciesCodeList = '') AND
		(@VarietyCode IS NULL OR VarietyCode=@VarietyCode) 
		--AND		(ProductView.IsExclusive = 0 OR ex.ProductGuid is not null)
	GROUP BY
		GeneticOwnerGuid,
		VarietyGuid,
		SpeciesGuid,
		VarietyColorLookupGuid,
		VarietyHabitLookupGuid,
		VarietyVigorLookupGuid,
		VarietyImageSetGuid

	PRINT CAST(@@ROWCOUNT AS NVARCHAR(10)) + ' Varieties found.'

	SELECT GeneticOwner.*
	FROM GeneticOwner
	WHERE GeneticOwner.Guid IN
	(SELECT DISTINCT(GeneticOwnerGuid) FROM @LocalTempVarietyData)

	SELECT Variety.*
	FROM Variety
	WHERE Variety.Guid IN
	(SELECT DISTINCT(VarietyGuid) FROM @LocalTempVarietyData)

	SELECT Species.*
	FROM Species
	WHERE Species.Guid IN
	(SELECT DISTINCT(SpeciesGuid) FROM @LocalTempVarietyData)
order by species.code
	
	SELECT Lookup.*
	FROM Lookup
	WHERE Lookup.Guid IN
	(
		(SELECT DISTINCT(VarietyColorLookupGuid) FROM @LocalTempVarietyData)
		UNION
		(SELECT DISTINCT(VarietyHabitLookupGuid) FROM @LocalTempVarietyData)
		UNION
		(SELECT DISTINCT(VarietyVigorLookupGuid) FROM @LocalTempVarietyData)
		UNION
		(SELECT LookupGuid FROM LookupView WHERE LookupCode IN ('VarietyImage','Thumbnail','Full'))
	)

	SELECT ImageSet.*
	FROM ImageSet
	WHERE ImageSet.Guid IN
	(SELECT DISTINCT(VarietyImageSetGuid) FROM @LocalTempVarietyData)

	SELECT ImageFile.*
	FROM ImageFile
	WHERE ImageFile.ImageSetGuid IN
	(SELECT DISTINCT(VarietyImageSetGuid) FROM @LocalTempVarietyData)

	EXECUTE EventLogAdd
	--@UserCode=@UserCode,
	@LogTypeLookupCode='ProcedureCallComplete',
	@ProcessLookupCode='VarietyDataGet',
	@ObjectGuid=@OriginalEventLogGuid
﻿

CREATE PROCEDURE [dbo].[AccountingGetBillingList]
	 @PaymentTypeCode as nvarchar(50),
	 @OrderStatusCode as nvarchar(50)
	 

AS
select 
	    Grower.Name, 
	    Grower.CreditLimit, 
	    Grower.CreditLimitTemporaryIncrease,
	    Grower.CreditLimitTemporaryIncreaseExpiration,
	    Grower.SalesTaxNumber,
	    Grower.EMail,
	    Grower.PhoneAreaCode,
	    Grower.Phone,
        OrderBillingView.OrderGuid,
	    OrderBillingView.FeeTotal,
	    OrderBillingView.ProductTotal,
	    OrderBillingView.OrderTotal,	 
	    OrderBillingView.CustomerPoNo,
	    OrderBillingView.OrderNumber,
	    OrderBillingView.Description,
        OrderBillingView.GrowerGuid,
	    ShipWeek.Week, 
	    ShipWeek.Year,
	    GrowerOrderLowestOrderLineStatusView.OrderLineStatusLookupGuid,
	    orderLineStatus.Code as OrderStatus,
	    payType.Code as AccountType
    from OrderBillingView
	    left join Grower on Grower.Guid = OrderBillingView.GrowerGuid
	    left join ShipWeek on ShipWeek.Guid = OrderBillingView.ShipWeekGuid
	    left join Lookup as payType on OrderBillingView.PaymentTypeLookupGuid = payType.Guid
	    left join GrowerOrderLowestOrderLineStatusView on GrowerOrderLowestOrderLineStatusView.GrowerOrderGuid = OrderBillingView.OrderGuid
	    left join Lookup as orderLineStatus on GrowerOrderLowestOrderLineStatusView.OrderLineStatusLookupGuid = orderLineStatus.Guid
    where payType.Code like @PaymentTypeCode
	        AND orderLineStatus.Code like @OrderStatusCode
	        AND GrowerOrderStatusLookupGuid != 'A00FF27A-798C-41F3-B930-A4FD4F82725C' 
            AND GrowerOrderStatusLookupGuid != 'E5D18A23-076A-4936-99AC-EB533691671B'
			AND SellerCode != 'DMO'
	ORDER by ShipWeek.Week Desc
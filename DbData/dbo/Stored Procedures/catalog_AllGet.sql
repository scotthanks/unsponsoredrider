﻿



CREATE PROCEDURE [dbo].catalog_AllGet
	@UserGuid AS UNIQUEIDENTIFIER = Null,
	@ProgramTypeCode AS NCHAR(50),
	@ProductFormCategoryCode AS NCHAR(50)
	
AS
	Declare @CountryGuid AS UNIQUEIDENTIFIER
	If  NOT @UserGuid = '{00000000-0000-0000-0000-000000000000}'
	    set @CountryGuid = (select s.CountryGuid
							From Person p
							Join Grower g on p.Growerguid = g.Guid
							Join Address a on g.AddressGuid = a.Guid
							Join State s on a.StateGuid = s.Guid
							WHERE p.UserGuid = @UserGuid)
	
	
	
	--Categories
	declare @categrories as table ( ProgramTypeGuid uniqueidentifier);
	INSERT iNTO @categrories (ProgramTypeGuid)
	SELECT DISTINCT ProgramTypeView.ProgramTypeGuid
	FROM ProgramTypeView
	join ProductFormCategoryProgramTypeCombinationView CombinationView 
		on ProgramTypeView.ProgramTypeCode = CombinationView.ProgramTypeCode;
	
	Select ProgramTypeGuid, 
	ProgramTypeCode, 
	ProgramTypeName
	from ProgramTypeView 
	where ProgramTypeGuid IN (select * from @categrories)


	--Product Form Categories
	SELECT comb.ProductFormCategoryGuid, 
	comb.ProductFormCategoryCode, 
	pfc.Name as ProdcutFormCategoryName, 
	comb.ProgramTypeGuid, 
	comb.ProgramTypeCode, 
	cat.ProgramTypeName 
	FROM ProductFormCategoryProgramTypeCombinationView comb
	JOIN ProductFormCategoryView pfc on comb.ProductFormCategoryGuid = pfc.ProductFormCategoryGuid  
	JOIN ProgramTypeView cat on comb.ProgramTypeGuid = cat.ProgramTypeGuid
	WHERE 
	comb.ProgramTypeCode = @ProgramTypeCode
 

 	--Suppliers
	DECLARE @LocalTempSupplierData TABLE
	(
		SupplierGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalTempSupplierData
	SELECT
		SupplierGuid
	FROM ProductView
	WHERE
		ProgramTypeCode = @ProgramTypeCode AND
		ProductFormCategoryCode = @ProductFormCategoryCode 
	GROUP BY
		SupplierGuid
    
	If Not @CountryGuid IS NULL
		Delete from @LocalTempSupplierData Where SupplierGuid not in 
		(
		Select pr.SupplierGuid
		From ProgramView pr
		Join ProgramCountry pc on pr.ProgramGuid = pc.ProgramGuid
		Where pc.CountryGuid = @CountryGuid AND
		pr.ProgramTypeCode = @ProgramTypeCode AND
		pr.ProductFormCategoryCode = @ProductFormCategoryCode
		Group by pr.SupplierGuid
		)


	SELECT Supplier.Guid,Supplier.Code,Supplier.Name
	FROM Supplier
	WHERE Supplier.Guid IN
	(SELECT DISTINCT(SupplierGuid) FROM @LocalTempSupplierData)
	
	
 	--Species
	DECLARE @LocalTempSpeciesData TABLE
	(
		SpeciesGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalTempSpeciesData
	SELECT DISTINCT
		SpeciesGuid
	FROM ProductView
	WHERE
		ProgramTypeCode = @ProgramTypeCode AND
		ProductFormCategoryCode = @ProductFormCategoryCode
		

	SELECT Species.Guid,Species.Code,Species.Name
	FROM Species
	WHERE Species.Guid IN
	(SELECT DISTINCT(SpeciesGuid) FROM @LocalTempSpeciesData)

	--Varieties
	SELECT DISTINCT 
	v.VarietyCode,v.Name as VarietyName,o.Name as GeneticOwnerName,
	v.SpeciesCode,v.SpeciesGuid,
	--'' as ImageFileName
	ISNULL(ifv.ImageFileName,'') as ImageFileName
	FROM ProductView pv
	JOIN VarietyView v on pv.VarietyGuid = v.VarietyGuid
	JOIN GeneticOwner o on v.GeneticOwnerGuid = o.Guid
	Left Join ImageFileView ifv on v.ThumbnailImageFileGuid = ifv.ImageFileGuid
	WHERE
		pv.ProgramTypeCode = @ProgramTypeCode AND
		pv.ProductFormCategoryCode = @ProductFormCategoryCode

	--SpeciesToSuppliers
	SELECT Distinct
		SpeciesCode,SupplierCode
	FROM ProductView
	WHERE
		ProgramTypeCode= @ProgramTypeCode AND
		ProductFormCategoryCode = @ProductFormCategoryCode
		
	Order BY
		SpeciesCode,SupplierCode
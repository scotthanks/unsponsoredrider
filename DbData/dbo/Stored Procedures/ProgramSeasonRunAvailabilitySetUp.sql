﻿







CREATE PROCEDURE [dbo].[ProgramSeasonRunAvailabilitySetUp]
	@ProgramSeasonCode AS NVARCHAR(50)
	
AS
	Declare @dCurrentMondayDate  Datetime
	Declare @ProgramCode  NVARCHAR(50)
	Declare @ProgramFormCode  NVARCHAR(50)
	Declare @NAWeeks  NVARCHAR(50)
	Declare @OPENWeeks  NVARCHAR(50)
	Declare @SpecialFormRules  NVARCHAR(3000)
	Declare @SpecialSpeciesRules  NVARCHAR(3000)
	Declare @SpecialProductRules  NVARCHAR(2000)
	Declare @ProcessNAWeeks  NVARCHAR(50)
	Declare @ProcessOPENWeeks  NVARCHAR(50)
	Declare @ProcessSpecialFormRules  NVARCHAR(3000)
	Declare @ProcessSpecialSpeciesRules  NVARCHAR(3000)
	Declare @ProcessSpecialProductRules  NVARCHAR(2000)
	Declare @iStartWeek  int
	Declare @iEndWeek  int
	Declare @sFormCode varchar(20)
	Declare @sSpeciesCode varchar(10)
	Declare @sProductList varchar(2000)
	Declare @sStatusCode varchar(20)
	Declare @sWeekRange varchar(20)
	Declare @sql  nvarchar(3000)
	Declare @tempsql  nvarchar(3000)
	Declare @err bigint
	Declare @RC int
	Declare @sText  nvarchar(3000)
	
	set @ProgramSeasonCode = rtrim(@ProgramSeasonCode)

	--Get the correct Program Code
	set @ProgramCode = (
		SELECT rtrim(pr.code) 
		FROM ProgramSeason ps Join Program pr on ps.ProgramGuid = pr.guid  
		WHERE ps.Code = @ProgramSeasonCode	)
	
	set @sText = @ProgramSeasonCode + ':  Program is ' +  @ProgramCode
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'
	

	--Get the correct Program Form Code
	set @ProgramFormCode = (
			SELECT rtrim(pfc.Code)
			FROM Program pr
			JOIN ProductFormCategory pfc on pr.ProductFormCategoryGuid = pfc.guid
			WHERE pr.code = @ProgramCode	)
	
	set @sText = @ProgramSeasonCode + ':  Product Form Code is ' +  @ProgramFormCode
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'

	--Get the correct week
	set @dCurrentMondayDate = (
		SELECT TOP 1 sw.MondayDate 
		FROM shipweek sw WHERE mondaydate < getdate()
		order by MondayDate desc	)
	
	set @sText = @ProgramSeasonCode + ':  Current Monday Date is ' +  cast(@dCurrentMondayDate as varchar(50))
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'


 --/////////////////////	 
	set @sText = @ProgramSeasonCode + ':  Starting Update NA weeks'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'  
					    
	set @NAWeeks = (Select NAWeeksDesc from ProgramSeason where Code = @ProgramSeasonCode)
	set @sText = @ProgramSeasonCode + ':  NA Weeks: ' + @NAWeeks
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'  
	 
	 WHILE @NAWeeks <> ''
		BEGIN
			Print 'NA Weeks is: ' + @NAWeeks
		    set @ProcessNAWeeks = Left(@NAWeeks, charindex( ';',@NAWeeks,1 ) )
			Print 'Process Weeks are ' + @ProcessNAWeeks
			
			If charindex( '-',@ProcessNAWeeks,1 ) > 0 
			  Begin
				set @iStartWeek  = Left(@ProcessNAWeeks, charindex('-', @ProcessNAWeeks, 1) - 1)
				set @iEndWeek  =  Left(Right(@ProcessNAWeeks, Len(@ProcessNAWeeks) -  charindex('-', @ProcessNAWeeks, 1)),  Len(Right(@ProcessNAWeeks, Len(@ProcessNAWeeks) -  charindex('-', @ProcessNAWeeks, 1))) -1)
				Print 'Using Range - iStartweek = ' + cast( @iStartWeek as varchar(5))
				Print 'Using Range - iEndweek = ' + cast(@iEndWeek as varchar(5))
				
				UPDATE ReportedAvailability                    
				SET availabilitytypelookupguid  = (select guid from lookup where path = 'Code/AvailabilityType/NA'), 
				Qty = 0,SalesSinceDateReported = 0, DateReported = getdate()
				FROM ReportedAvailability ra
				JOIN Lookup avl on ra.availabilitytypelookupguid = avl.guid
				JOIN shipweek sw on ra.shipweekguid = sw.guid
				JOIN product p on ra.productguid = p.guid
				JOIN program pr on p.programguid = pr.guid
				JOIN programSeason ps on pr.guid = ps.ProgramGuid
				WHERE ps.code = @ProgramSeasonCode
				AND sw.MondayDate BETWEEN ps.StartDate and ps.EndDate
				AND ra.availabilitytypelookupguid <>  (select guid from lookup where code = 'AVAIL')
				AND sw.Week BETWEEN @iStartWeek AND @iEndWeek
			  End
			Else
			  Begin
				Print 'Using list = ' + @ProcessNAWeeks
				set @tempsql = (select left( @ProcessNAWeeks ,len(@ProcessNAWeeks)-1)) 
				Set @sql='
				UPDATE ReportedAvailability                    
				SET availabilitytypelookupguid  = (select guid from lookup where path = ''Code/AvailabilityType/NA''), 
				Qty = 0,SalesSinceDateReported= 0, DateReported = getdate()
				FROM ReportedAvailability ra
				JOIN Lookup avl on ra.availabilitytypelookupguid = avl.guid
				JOIN shipweek sw on ra.shipweekguid = sw.guid
				JOIN product p on ra.productguid = p.guid
				JOIN program pr on p.programguid = pr.guid
				JOIN programSeason ps on pr.guid = ps.ProgramGuid
				WHERE ps.code = ''' +  @ProgramSeasonCode + '''
				AND sw.MondayDate BETWEEN ps.StartDate and ps.EndDate
				AND ra.availabilitytypelookupguid <>  (select guid from lookup where code = ''AVAIL'')
				AND sw.Week in (' +   @tempsql +	 ')'
				 
				set @sText = left(@ProgramSeasonCode + ':  ' + @sql,2000)
				EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'

				exec sp_executesql @sql
				
			  End

			set @NAWeeks = Right(@NAWeeks, Len(@NAWeeks) - charindex( ';',@NAWeeks,1 ) )

		END
  	 
	set @sText = @ProgramSeasonCode + ':  Done Update NA weeks'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 
--/////////////////////	
	set @sText = @ProgramSeasonCode + ':  Starting Update OPEN weeks'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'    
	set @OPENWeeks = (Select OpenWeeksDesc from ProgramSeason where Code = @ProgramSeasonCode)
	set @sText = @ProgramSeasonCode + ':  Starting OPEN Weeks: ' + @OPENWeeks
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 
	 WHILE @OPENWeeks <> ''
		BEGIN
			--Print 'OPEN Weeks is: ' + @OPENWeeks
		    set @ProcessOPENWeeks = Left(@OPENWeeks, charindex( ';',@OPENWeeks,1 ) )
			--Print 'Process Weeks are ' + @ProcessOPENWeeks
			
			If charindex( '-',@ProcessOPENWeeks,1 ) > 0 
			  Begin
				set @iStartWeek  = Left(@ProcessOPENWeeks, charindex('-', @ProcessOPENWeeks, 1) - 1)
				set @iEndWeek  =  Left(Right(@ProcessOPENWeeks, Len(@ProcessOPENWeeks) -  charindex('-', @ProcessOPENWeeks, 1)),  Len(Right(@ProcessOPENWeeks, Len(@ProcessOPENWeeks) -  charindex('-', @ProcessOPENWeeks, 1))) -1)
				--Print 'Using Range - iStartweek = ' + cast( @iStartWeek as varchar(5))
				--Print 'Using Range - iEndweek = ' + cast(@iEndWeek as varchar(5))
				
				UPDATE ReportedAvailability                    
				SET availabilitytypelookupguid  = (select guid from lookup where code = 'OPEN'), 
				Qty = 0,SalesSinceDateReported = 0, DateReported = getdate() 
				FROM ReportedAvailability ra
				JOIN Lookup avl on ra.availabilitytypelookupguid = avl.guid
				JOIN shipweek sw on ra.shipweekguid = sw.guid
				JOIN product p on ra.productguid = p.guid
				JOIN program pr on p.programguid = pr.guid
				JOIN programSeason ps on pr.guid = ps.ProgramGuid
				WHERE ps.code = @ProgramSeasonCode
				AND sw.MondayDate BETWEEN ps.StartDate and ps.EndDate
				AND ra.availabilitytypelookupguid <>  (select guid from lookup where code = 'AVAIL')
				AND sw.Week BETWEEN @iStartWeek AND @iEndWeek
			  End
			Else
			  Begin
				Print 'Using list = ' + @ProcessOPENWeeks
				set @tempsql = (select left( @ProcessOPENWeeks ,len(@ProcessOPENWeeks)-1)) 
				Set @sql='
				UPDATE ReportedAvailability                    
				SET availabilitytypelookupguid  = (select guid from lookup where code = ''OPEN''), 
				Qty = 0,SalesSinceDateReported = 0,DateReported = getdate() 
				FROM ReportedAvailability ra
				JOIN Lookup avl on ra.availabilitytypelookupguid = avl.guid
				JOIN shipweek sw on ra.shipweekguid = sw.guid
				JOIN product p on ra.productguid = p.guid
				JOIN program pr on p.programguid = pr.guid
				JOIN programSeason ps on pr.guid = ps.ProgramGuid
				WHERE ps.code = ''' +  @ProgramSeasonCode + '''
				AND sw.MondayDate BETWEEN ps.StartDate and ps.EndDate AND ra.availabilitytypelookupguid <>  (select guid from lookup where code = ''AVAIL'')
				AND sw.Week in (' +   @tempsql +	 ')'
				
				set @sText = left(@ProgramSeasonCode + ':  ' + @sql,2000)
				EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 

				exec sp_executesql @sql
				
			  End

			set @OPENWeeks = Right(@OPENWeeks, Len(@OPENWeeks) - charindex( ';',@OPENWeeks,1 ) )

		END
  
	set @sText = @ProgramSeasonCode + ':  Done Update Open weeks'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'    
--/////////////////////		
    set @sText = @ProgramSeasonCode + ':  Starting Update Special Rules by form'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 
	set @SpecialFormRules = (Select replace(SpecialRulesByForm,char(10),'') from ProgramSeason where Code = @ProgramSeasonCode)
	set @sText = @ProgramSeasonCode + ':  Starting Form Rules: ' + @SpecialFormRules
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 
	WHILE @SpecialFormRules <> ''
		BEGIN      
			SELECT @err = @@error IF @err <> 0 BREAK
		    set @ProcessSpecialFormRules = Left(@SpecialFormRules, charindex( ';',@SpecialFormRules,1 ) )
			--Print 'Process Form Rules: ' + @ProcessSpecialFormRules
                
            set @sFormCode = ltrim(Left(@ProcessSpecialFormRules, charindex('--', @ProcessSpecialFormRules,1) - 1))
           -- Print 'Form Code: ' + @sFormCode
			set @ProcessSpecialFormRules = Right(@ProcessSpecialFormRules, Len(@ProcessSpecialFormRules) - charindex('--', @ProcessSpecialFormRules,1 ) - 1)
			--Print 'Process Form Rules: ' + @ProcessSpecialFormRules     
            set  @sStatusCode = Left(@ProcessSpecialFormRules, charindex('--', @ProcessSpecialFormRules,1 ) - 1)
			--Print 'Status Code: ' + @sStatusCode     
	        set @ProcessSpecialFormRules = Right(@ProcessSpecialFormRules, Len(@ProcessSpecialFormRules) - charindex('--', @ProcessSpecialFormRules,1 ) - 1)
			--Print 'Process Form Rules: ' + @ProcessSpecialFormRules     
            set @sWeekRange = Left(@ProcessSpecialFormRules, charindex(';', @ProcessSpecialFormRules,1))
			--Print 'Week Range: ' + @sWeekRangepath = 'Code/AvailabilityType/NA'
		    Set @sql='       
				UPDATE ReportedAvailability
				SET availabilitytypelookupguid  = (select guid from lookup where path = ''Code/AvailabilityType/' +  @sStatusCode + '''), 
					Qty = 0, SalesSinceDateReported = 0, DateReported = getdate() 
                FROM ReportedAvailability ra
                JOIN Lookup avl on ra.availabilitytypelookupguid = avl.guid
                JOIN shipweek sw on ra.shipweekguid = sw.guid
                JOIN product p on ra.productguid = p.guid
                JOIN productForm pf on p.productformguid = pf.guid
                JOIN program pr on p.programguid = pr.guid
                JOIN ProgramSeason ps on pr.guid = ps.ProgramGuid
                WHERE ps.code = ''' + @ProgramSeasonCode + '''
                AND sw.MondayDate BETWEEN ps.StartDate AND ps.EndDate
				AND ra.availabilitytypelookupguid <>  (select guid from lookup where code = ''AVAIL'')
                AND pf.Code  = ''' +  @sFormCode + ''''
                If charindex('-', @sWeekRange,1 ) > 0 
				  Begin
					set @iStartWeek  = Left(@sWeekRange, charindex('-', @sWeekRange, 1) - 1)
					set @iEndWeek  =  Left(Right(@sWeekRange, Len(@sWeekRange) -  charindex('-', @sWeekRange, 1)),  Len(Right(@sWeekRange, Len(@sWeekRange) -  charindex('-', @sWeekRange, 1))) -1)
					Print 'Using Range - iStartweek = ' + cast( @iStartWeek as varchar(5))
					Print 'Using Range - iEndweek = ' + cast(@iEndWeek as varchar(5))
				    set @sql = @sql + ' AND sw.Week BETWEEN ' + cast(@iStartWeek as varchar(2)) + ' AND ' + cast(@iEndWeek as varchar(2))
                  End      
                Else
				  Begin
                     set @tempsql = (select left(@sWeekRange ,len(@sWeekRange)-1)) 
					 set @sql = @sql + ' AND sw.Week in (' + @tempsql + ')'
                  End 
		
		     
			set @sText = left(@ProgramSeasonCode + ':  ' + @sql,2000)
			EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 

			exec sp_executesql @sql  
			
			
   
			set @SpecialFormRules = Right(@SpecialFormRules, Len(@SpecialFormRules) - charindex( ';',@SpecialFormRules,1 ) )
			--Print 'Form Rules: ' + @SpecialFormRules
		END        
    set @sText = @ProgramSeasonCode + ':  Done Update Special Rules by form'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 
	     
 --/////////////////////	        
	set @sText = @ProgramSeasonCode + ':  Starting Update Special Rules by Species'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 
	
	set @SpecialSpeciesRules = (Select replace(SpecialRulesBySpecies,char(10),'') from ProgramSeason where Code = @ProgramSeasonCode)
	 
	set @sText = @ProgramSeasonCode + ':  Starting Species Rules: ' + @SpecialSpeciesRules
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'
	
	WHILE @SpecialSpeciesRules <> ''
		BEGIN      
			SELECT @err = @@error IF @err <> 0 BREAK
		    set @ProcessSpecialSpeciesRules = Left(@SpecialSpeciesRules, charindex( ';',@SpecialSpeciesRules,1 ) )
			--Print 'Process Species Rules: ' + @ProcessSpecialSpeciesRules
            set @sSpeciesCode = ltrim(Left(@ProcessSpecialSpeciesRules, charindex('--', @ProcessSpecialSpeciesRules,1) - 1))
           -- Print 'Species Code: ' + @sSpeciesCode
			set @ProcessSpecialSpeciesRules = Right(@ProcessSpecialSpeciesRules, Len(@ProcessSpecialSpeciesRules) - charindex('--', @ProcessSpecialSpeciesRules,1 ) - 1)
			--Print 'Process Species Rules: ' + @ProcessSpecialSpeciesRules     
            set  @sStatusCode = Left(@ProcessSpecialSpeciesRules, charindex('--', @ProcessSpecialSpeciesRules,1 ) - 1)
			--Print 'Status Code: ' + @sStatusCode     
	        set @ProcessSpecialSpeciesRules = Right(@ProcessSpecialSpeciesRules, Len(@ProcessSpecialSpeciesRules) - charindex('--', @ProcessSpecialSpeciesRules,1 ) - 1)
			--Print 'Process Species Rules: ' + @ProcessSpecialSpeciesRules     
            set @sWeekRange = Left(@ProcessSpecialSpeciesRules, charindex(';', @ProcessSpecialSpeciesRules,1))
			--Print 'Week Range: ' + @sWeekRange
		    Set @sql='       
				UPDATE ReportedAvailability
				SET availabilitytypelookupguid  = (select guid from lookup where path = ''Code/AvailabilityType/' +  @sStatusCode + '''), 
					Qty = 0, SalesSinceDateReported = 0, DateReported = getdate() 
                FROM ReportedAvailability ra
                JOIN Lookup avl on ra.availabilitytypelookupguid = avl.guid
                JOIN shipweek sw on ra.shipweekguid = sw.guid
                JOIN product p on ra.productguid = p.guid
                JOIN variety v on p.varietyguid = v.guid
                JOIN species sp on v.speciesguid = sp.guid
                JOIN productForm pf on p.productformguid = pf.guid
                JOIN program pr on p.programguid = pr.guid
                JOIN ProgramSeason ps on pr.guid = ps.ProgramGuid
                WHERE ps.code = ''' + @ProgramSeasonCode + '''
                AND sw.MondayDate BETWEEN ps.StartDate AND ps.EndDate 
				AND ra.availabilitytypelookupguid <>  (select guid from lookup where code = ''AVAIL'')
                AND sp.Code  = ''' +  @sSpeciesCode + ''''
                If charindex('-', @sWeekRange,1 ) > 0 
				  Begin
					set @iStartWeek  = Left(@sWeekRange, charindex('-', @sWeekRange, 1) - 1)
					set @iEndWeek  =  Left(Right(@sWeekRange, Len(@sWeekRange) -  charindex('-', @sWeekRange, 1)),  Len(Right(@sWeekRange, Len(@sWeekRange) -  charindex('-', @sWeekRange, 1))) -1)
					Print 'Using Range - iStartweek = ' + cast( @iStartWeek as varchar(5))
					Print 'Using Range - iEndweek = ' + cast(@iEndWeek as varchar(5))
				    set @sql = @sql + ' AND sw.Week BETWEEN ' + cast(@iStartWeek as varchar(2)) + ' AND ' + cast(@iEndWeek as varchar(2))
                  End      
                Else
				  Begin
                     set @tempsql = (select left(@sWeekRange ,len(@sWeekRange)-1)) 
					 set @sql = @sql + ' AND sw.Week in (' + @tempsql + ')'
                  End 
		
		     
			set @sText = left(@ProgramSeasonCode + ':  ' + @sql,2000)
			EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'

			exec sp_executesql @sql  
			
			
   
			set @SpecialSpeciesRules = Right(@SpecialSpeciesRules, Len(@SpecialSpeciesRules) - charindex( ';',@SpecialSpeciesRules,1 ) )
			--Print 'Special Species Rules: ' + @SpecialSpeciesRules
		END        
     
  		set @sText = @ProgramSeasonCode + ':  Done Update Special Rules by Species'
		EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 
 --/////////////////////	        
    set @sText = @ProgramSeasonCode + ':  Starting Update Special Rules by Product'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 
	  
	set @SpecialProductRules = (Select replace(SpecialRulesByProduct,char(10),'') from ProgramSeason where Code = @ProgramSeasonCode)
	
	set @sText = @ProgramSeasonCode + ':  Starting Product Rules: ' + @SpecialProductRules
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 
	WHILE @SpecialProductRules <> ''
		BEGIN      
			SELECT @err = @@error IF @err <> 0 BREAK
		    set @ProcessSpecialProductRules = Left(@SpecialProductRules, charindex( ';',@SpecialProductRules,1 ) )
			--Print 'Process Product Rules: ' + @ProcessSpecialProductRules
            set @sProductList = ltrim(Left(@ProcessSpecialProductRules, charindex('--', @ProcessSpecialProductRules,1) - 1))
            --Print 'Product List: ' + @sProductList
			set @ProcessSpecialProductRules = Right(@ProcessSpecialProductRules, Len(@ProcessSpecialProductRules) - charindex('--', @ProcessSpecialProductRules,1 ) - 1)
			--Print 'Process Product Rules: ' + @ProcessSpecialProductRules     
            set  @sStatusCode = Left(@ProcessSpecialProductRules, charindex('--', @ProcessSpecialProductRules,1 ) - 1)
			--Print 'Status Code: ' + @sStatusCode     
	        set @ProcessSpecialProductRules = Right(@ProcessSpecialProductRules, Len(@ProcessSpecialProductRules) - charindex('--', @ProcessSpecialProductRules,1 ) - 1)
			--Print 'Process Product Rules: ' + @ProcessSpecialProductRules     
            set @sWeekRange = Left(@ProcessSpecialProductRules, charindex(';', @ProcessSpecialProductRules,1))
			--Print 'Week Range: ' + @sWeekRange
		    Set @sql='       
				UPDATE ReportedAvailability
				SET availabilitytypelookupguid  = (select guid from lookup where path = ''Code/AvailabilityType/' +  @sStatusCode + '''), 
					Qty = 0, SalesSinceDateReported = 0, DateReported = getdate() 
                FROM ReportedAvailability ra
                JOIN Lookup avl on ra.availabilitytypelookupguid = avl.guid
                JOIN shipweek sw on ra.shipweekguid = sw.guid
                JOIN product p on ra.productguid = p.guid
                JOIN variety v on p.varietyguid = v.guid
                JOIN species sp on v.speciesguid = sp.guid
                JOIN productForm pf on p.productformguid = pf.guid
                JOIN program pr on p.programguid = pr.guid
                JOIN ProgramSeason ps on pr.guid = ps.ProgramGuid
                WHERE ps.code = ''' + @ProgramSeasonCode + '''
                AND sw.MondayDate BETWEEN ps.StartDate AND ps.EndDate 
				AND ra.availabilitytypelookupguid <>  (select guid from lookup where code = ''AVAIL'')
                AND p.SupplierIdentifier in (' +  replace(@sProductList,'*','''') + ')'
                If charindex('-', @sWeekRange,1 ) > 0 
				  Begin
					set @iStartWeek  = Left(@sWeekRange, charindex('-', @sWeekRange, 1) - 1)
					set @iEndWeek  =  Left(Right(@sWeekRange, Len(@sWeekRange) -  charindex('-', @sWeekRange, 1)),  Len(Right(@sWeekRange, Len(@sWeekRange) -  charindex('-', @sWeekRange, 1))) -1)
					Print 'Using Range - iStartweek = ' + cast( @iStartWeek as varchar(5))
					Print 'Using Range - iEndweek = ' + cast(@iEndWeek as varchar(5))
				    set @sql = @sql + ' AND sw.Week BETWEEN ' + cast(@iStartWeek as varchar(2)) + ' AND ' + cast(@iEndWeek as varchar(2))
                  End      
                Else
				  Begin
                     set @tempsql = (select left(@sWeekRange ,len(@sWeekRange)-1)) 
					 set @sql = @sql + ' AND sw.Week in (' + @tempsql + ')'
                  End  
			
			set @sText = left(@ProgramSeasonCode + ':  ' + @sql,2000)
			EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info' 
			
			exec sp_executesql @sql  
   
			set @SpecialProductRules = Right(@SpecialProductRules, Len(@SpecialProductRules) - charindex( ';',@SpecialProductRules,1 ) )
			--Print 'Special Product Rules: ' + @SpecialProductRules
		END        
      set @sText = @ProgramSeasonCode + ':  Done Update Special Rules by Product'
	  EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','ProgramSeasonAvailUpdate','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'
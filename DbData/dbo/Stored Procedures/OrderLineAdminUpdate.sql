﻿








CREATE PROCEDURE [dbo].[OrderLineAdminUpdate]
	@OrderLineGuid AS UNIQUEIDENTIFIER,
	@QtyOrdered AS int,
	@QtyShipped AS int,
	@ActualPrice AS DECIMAL(8,4),
	@ActualCost AS DECIMAL(8,4),
	@OrderLineStatusCode AS NVARCHAR(50)
	

AS
	DECLARE @OrderLineStatusLookupGuid as UNIQUEIDENTIFIER
	DECLARE @GrowerOrderGuid UniqueIdentifier
	DECLARE @ProductGuid UniqueIdentifier
	DECLARE @ShipWeekGuid UniqueIdentifier
	DECLARE @OriginalQty int
	Declare @LogGuid uniqueidentifier
	Declare @UserGuid uniqueidentifier
	Declare @PersonGuid uniqueidentifier
	Declare @theDate DateTime
	Declare @sComment nvarchar(2000)
	Declare @VarietyName nvarchar(200)

	SELECT 
	@PersonGuid = guid
	,@UserGuid = userguid
	FROM Person 
	WHERE 
	firstname  = 'Customer' 
	AND lastname = 'Service'

	
	SELECT 
	@GrowerOrderGuid = GrowerOrderGuid
	,@OriginalQty = QtyOrdered
	,@ProductGuid = ProductGuid
	,@ShipWeekGuid = ShipWeekGuid
	,@VarietyName = VarietyName
	FROM
		OrderLineView
	WHERE 
	OrderLineGuid = @OrderLineGuid

	If @QtyOrdered != @OriginalQty
	BEGIN
		--add modification comment
		set @sComment = 'Order for ' + cast(@OriginalQty as nvarchar(10)) + ' ' + @VarietyName + ' changed to ' + cast(@QtyOrdered as varchar(10))
		set @LogGuid = (select guid from lookup where path = 'Logging/LogType/Grower/OrderLineChange')
		set @theDate = (select Getdate())
		Exec GrowerOrderHistoryAdd 
			@UserGuid,NULL,
			@GrowerOrderGuid,
			@sComment,0,@PersonGuid,@theDate,@LogGuid
	
		--update availability
		Update ReportedAvailability
		set SalesSinceDateReported = SalesSinceDateReported + (@QtyOrdered - @OriginalQty)
		Where productGuid = @ProductGuid
		and ShipWeekGuid = @ShipWeekGuid
	END
	

	--select * from lookup where path like 'Code/OrderLineStatus/%'
	--this should be eliminated and code should match path
	if @OrderLineStatusCode = 'GrowerNotifiedAfterSupplierConfirmed'
	Begin
		set @OrderLineStatusCode = 'CustomerNotifiedAfterSupplierConfirmed'
	End

	set @OrderLineStatusLookupGuid = (select Guid from Lookup where path  = 'Code/OrderLineStatus/' + @OrderLineStatusCode)

	--Update line
	UPDATE OrderLine
	SET
		QtyOrdered=@QtyOrdered
		,QtyShipped=@QtyShipped
		,ActualPrice=@ActualPrice
		,ActualCost=@ActualCost
		,OrderLineStatusLookupGuid=@OrderLineStatusLookupGuid
		
	WHERE Guid=@OrderLineGuid

	
	If @QtyOrdered != @OriginalQty
	BEGIN
		exec GrowerOrderRecalcFees @UserGuid, @GrowerOrderGuid, 1
	End
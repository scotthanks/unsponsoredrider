﻿CREATE procedure [dbo].[LookupList]
	@PathExpression AS NVARCHAR(200) = NULL
AS
	IF @PathExpression IS NULL OR LTRIM(RTRIM(@PathExpression)) = ''
		SET @PathExpression = '*'

	SET @PathExpression = LTRIM(RTRIM(@PathExpression))

	IF @PathExpression LIKE '%*%'
		SET @PathExpression = REPLACE(@PathExpression, '*', '%')
	ELSE
		SET @PathExpression = '%' + @PathExpression + '%'

	SELECT
		Path,
		CASE WHEN SortSequence != 0 THEN '*' ELSE ' ' END AS S ,
		LookupCode,
		LookupName,
		LookupGuid,
		SortSequence
	FROM LookupView
	WHERE Path LIKE @PathExpression
	ORDER BY SortKey
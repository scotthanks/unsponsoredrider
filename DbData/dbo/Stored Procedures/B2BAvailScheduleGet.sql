﻿








CREATE PROCEDURE [dbo].[B2BAvailScheduleGet]
	
	
	 

AS
	SELECT 
		g.name as GrowerName,rtrim(pr.Code) as ProgramCode
		,pr.name as ProgramName
		,gp.AvailHoursBetweenSend
		,DateAdd(HOUR,0,gp.AvailLastSendDate) as AvailLastSendDate 
		,DateAdd(HOUR,0,gp.AvailNextSendDate) as AvailNextSendDate 
		,gp.AvailWeeks
	FROM GrowerProgram gp
	JOIN Grower g on gp.GrowerGuid = g.Guid
	JOIN Program pr on gp.ProgramGuid = pr.Guid
	WHERE gp.AvailHoursBetweenSend > 0
	ORDER BY g.name,pr.name
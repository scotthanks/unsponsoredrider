﻿




CREATE procedure [dbo].[SellerCodeGet]
	@OrderGuid as uniqueidentifier,
	@SellerCode AS nvarchar(50) OUTPUT
AS
	
	
	
	   select @SellerCode = se.Code
	   from 
	   GrowerOrder o
	   join Seller se on o.SellerGuid = se.Guid
	   Where 
	   o.Guid = @OrderGuid
﻿


CREATE PROCEDURE [dbo].[GrowerOrderHistoryAdd]
	@UserGuid AS UNIQUEIDENTIFIER=NULL,
	@UserCode AS NCHAR(56)=NULL,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER,
	@Comment AS NVARCHAR(2000),
	@IsInternal AS bit,
	@PersonGuid AS UNIQUEIDENTIFIER,
	@EventDate AS DateTime,
	@LogTypeLookupGuid AS UNIQUEIDENTIFIER 
AS
	

	

	INSERT INTO GrowerOrderHistory
	(
		Guid,
		DateDeactivated,
		GrowerOrderGuid,
		Comment,
		IsInternal,
		PersonGuid,
		EventDate,
		LogTypeLookupGuid
		
	)
	VALUES
	(
		newid(),
		NULL,
		@GrowerOrderGuid,
		@Comment,
		@IsInternal,
		@PersonGuid,
		@EventDate,
		@LogTypeLookupGuid
	)
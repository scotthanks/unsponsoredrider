﻿



CREATE PROCEDURE [dbo].[UserGetFromUserName]
	 @userName as nvarchar(56),
	 @userGuid as uniqueidentifier output,
	 @Email as nvarchar(70) output
	
	 

AS
		SELECT @Email = IsNull(Email,''),
		@userGuid = UserGuid
		FROM Person
		WHERE Usercode = @userName
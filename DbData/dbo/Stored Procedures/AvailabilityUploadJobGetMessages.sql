﻿



CREATE PROCEDURE [dbo].[AvailabilityUploadJobGetMessages]
	 @JobNumber as bigint
	
	 

AS
		Select 
	
		MessageTime,
		MessageType,
		MessageState,
		Message
		
		from 
		ReportedAvailabilityUploadJobMessage
		Where ReportedAvailabilityUploadJobGuid = (select guid from ReportedAvailabilityUploadJob where ID = @JobNumber)

		ORDER BY ID desc
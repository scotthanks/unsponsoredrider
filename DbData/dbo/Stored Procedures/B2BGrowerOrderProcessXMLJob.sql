﻿








CREATE procedure [dbo].[B2BGrowerOrderProcessXMLJob]
	@B2BIntegrationXMLID as bigint,
	@iDebug int = 0

AS
DECLARE @XML AS XML
DECLARE @OrderNo varchar(100)
DECLARE @RevisionNumber int
DECLARE @GrowerName varchar(100)
DECLARE @GrowerGuid uniqueidentifier
DECLARE @CustomerPONumber varchar(100)
DECLARE @OrderDescription varchar(50)
DECLARE @OrderCreatedDate datetime
DECLARE @ShipWeekCode varchar(6)
DECLARE @ShipMethodCode varchar(100)
DECLARE @GrowerShipMethodCode varchar(100)
DECLARE @TagRatioCode varchar(100)
DECLARE @ShipToAddressID bigint
DECLARE @GrowerOrderID bigint
DECLARE @SupplierOrderID bigint

DECLARE @ShipWeekGuid uniqueidentifier
DECLARE @ProductFormCategoryGuid uniqueidentifier
DECLARE @ProgramTypeGuid uniqueidentifier
DECLARE @ShipToAddressGuid uniqueidentifier
DECLARE @TagRatioGuid uniqueidentifier
DECLARE @ShipMethodGuid uniqueidentifier
DECLARE @GrowerOrderGuid uniqueidentifier
DECLARE @SupplierOrderGuid uniqueidentifier
DECLARE @SellerGuid uniqueidentifier

DECLARE @SupplierGuid uniqueidentifier
DECLARE @B2BID bigint
Declare @Body varchar(200)
DECLARE @StatusMessage varchar(200)

Declare @PersonGuid uniqueidentifier
Declare @LogTypeLookupGuid uniqueidentifier

DECLARE @ExistingCustomerPONumber nvarchar(100)
DECLARE @ExistingShipDate datetime
Declare @POCheckGuid nvarchar(100)    

Declare @EmailSubject nvarchar(100) 
Declare @EmailBody nvarchar(100)

set @B2BID = @B2BIntegrationXMLID

Update 
epsB2BProd.dbo.B2BIntegrationXML
set B2BStatus = 'Started Processing',
statusMessage = cast(GetDAte() as varchar(200))
WHERE ID = @B2BID



SELECT  
@XML = XMLData
FROM epsB2BProd.dbo.B2BIntegrationXML
WHERE ID = @B2BID


SELECT  @GrowerGuid = b.GrowerGuid
	,@RevisionNumber= x.XmlCol.value('(OrderHeader/RevisionNumber)[1]','VARCHAR(100)') 
	,@GrowerName  = x.XmlCol.value('(OrderHeader/GrowerName)[1]','VARCHAR(50)')
	,@OrderNo  = x.XmlCol.value('(OrderHeader/EPSOrderID)[1]','VARCHAR(50)')
    ,@CustomerPONumber  = x.XmlCol.value('(OrderHeader/CustomerPONumber)[1]','VARCHAR(100)')
    ,@OrderDescription  = x.XmlCol.value('(OrderHeader/OrderDescription)[1]','VARCHAR(50)')
    ,@OrderCreatedDate  = x.XmlCol.value('(OrderHeader/OrderCreatedDate)[1]','DATETIME')
    ,@ShipWeekCode  = x.XmlCol.value('(OrderHeader/ShipWeek)[1]','VARCHAR(6)')
    ,@ShipMethodCode  = x.XmlCol.value('(OrderHeader/ShipMethod)[1]','VARCHAR(100)')
    ,@TagRatioCode = x.XmlCol.value('(OrderHeader/TagRatio)[1]','VARCHAR(100)')  
    ,@ShipToAddressID = x.XmlCol.value('(ShipToAddress/Number)[1]','VARCHAR(100)')  
FROM    epsB2BProd.dbo.B2BIntegrationXML b
CROSS APPLY @XML.nodes('/Order') x(XmlCol)
WHERE b.ID = @B2BID

set @PersonGuid = (select top 1 p.Guid from Person p join grower g on p.GrowerGuid = g.Guid where g.Guid = @GrowerGuid)

If @GrowerName = ''
Begin
	Select @GrowerName = Name from Grower where Guid = @GrowerGuid
end

Select @ShipWeekGuid = ShipWeekGuid from ShipweekView where ShipWeekCode = @ShipWeekCode


set @GrowerShipMethodCode = @ShipMethodCode

--select * from lookup where path like 'Code/ship%'
IF @ShipMethodCode = 'CU'  --Euro list
BEGIN
	Set @ShipMethodCode = 'CPU'
END
IF @ShipMethodCode in('FG', 'F1','F2','ZZ')  --Euro list
BEGIN
	Set @ShipMethodCode = 'FEDEX'
END        

--SELECT path from Lookup where path  = 'Code/TagRatio/' + 'NoTag'
                                    
SELECT @TagRatioGuid = Guid from Lookup where path  = 'Code/TagRatio/' + @TagRatioCode
SELECT @ShipMethodGuid = Guid from Lookup where path  = 'Code/ShipMethod/' + @ShipMethodCode

--Address Mappping if needed.
if @ShipToAddressID = 1
BEGIN
	set @ShipToAddressID = 32093  --Euro hard coded mapping
END

Print @GrowerGuid
Print @GrowerName
Print @RevisionNumber
Print @CustomerPONumber
Print @OrderDescription
Print @OrderCreatedDate
Print @ShipWeekCode
Print @ShipMethodCode
Print @GrowerShipMethodCode
Print @TagRatioCode
Print @ShipToAddressID
Print @ShipWeekGuid
	

create table #OrderLine (
	GrowerGuid uniqueidentifier,
	LineNumber bigint,
	EPSProductID bigint,
	ProductDescription nvarchar(200),
	OrderedQty int,
	ChangeType nvarchar(100),
	Status nvarchar(100),
	QtyChange bigint,
	AfterAvailQtyChange bigint,
	SupplierGuid uniqueidentifier,
)

IF @RevisionNumber= 1  -- means Order Place
BEGIN  --Order Place


	Update 
	epsB2BProd.dbo.B2BIntegrationXML
	set B2BType = 'Order Place',
	statusMessage = cast(GetDAte() as varchar(200))
	WHERE ID = @B2BID

	If @GrowerGuid <> (Select top 1 Guid from Grower where name = @GrowerName)
	BEGIN
		Update 
		epsB2BProd.dbo.B2BIntegrationXML
		set B2BStatus = 'Error' ,
		statusMessage = 'No Order Created for customer PO: ' + @CustomerPONumber + '. Grower Name ' + @GrowerName + ' does not match the Public Key.' 
		WHERE ID = @B2BID

		Return
	END


	Insert into #OrderLine
	SELECT  b.GrowerGuid
		, x.XmlCol.value('(LineNumber)[1]','bigint') as LineNumber
		,x.XmlCol.value('(EPSProductID)[1]','bigint') as EPSProductID
		,x.XmlCol.value('(ProductDescription)[1]','nvarchar(200)') as ProductDescription
		,x.XmlCol.value('(OrderedQty)[1]','int') as OrderedQty
		,x.XmlCol.value('(ChangeType)[1]','nvarchar(100)') as ChangeType
		,x.XmlCol.value('(Status)[1]','nvarchar(100)') as Status
		,x.XmlCol.value('(OrderedQty)[1]','int') as QtyChange 
		,0 as AfterAvailQtyChange
		,'00000000-0000-0000-0000-000000000000' as SupplierGuid
	FROM    epsB2BProd.dbo.B2BIntegrationXML b
	CROSS APPLY @XML.nodes('/Order/OrderLines/Item') x(XmlCol)
	WHERE b.ID = @B2BID

	--Clear out unmatched products
	Delete from #OrderLine where EPSProductID = 0

	--select * from #OrderLine
	If 0 = (Select count(*) from #OrderLine)
	BEGIN
		Update 
		epsB2BProd.dbo.B2BIntegrationXML
		set B2BStatus = 'Error' ,
		statusMessage = 'No Order Created for customer PO: ' + @CustomerPONumber + '. No Order Lines found in XML document.' 
		WHERE ID = @B2BID

		Return
	END
	Print 'here'

	--Assign the Guids
	select top 1
	@ProductFormCategoryGuid = pr.ProductFormCategoryGuid
	,@ProgramTypeGuid = pr.ProgramTypeGuid
	From #OrderLine l
	Join Product p on l.EPSProductID = p.ID
	JOIN Program pr on p.ProgramGuid = pr.Guid



	Select @ShipToAddressGuid = Guid from GrowerAddress where ID = @ShipToAddressID
	Print 'here end'
	Print '@ProductFormCategoryGuid --' + cast(@ProductFormCategoryGuid as varchar(50))

	--select * from #OrderLine
	If @ShipToAddressGuid not in (Select guid from GrowerAddress where GrowerGuid = @GrowerGuid)
	BEGIN
		Update 
		epsB2BProd.dbo.B2BIntegrationXML
		set B2BStatus = 'Error' ,
		statusMessage = 'No Order Created for customer PO: ' + @CustomerPONumber + '. Address ID not associated with Grower ' + @GrowerName + '.' 
		WHERE ID = @B2BID

		Return
	END

	
	SELECT @POCheckGuid =  ISNULL( cast(Guid as varchar(50)),'')
	FROM GrowerOrder  
	WHERE GrowerGuid = @GrowerGuid and CustomerPoNo = @CustomerPONumber
	


	Print @ProductFormCategoryGuid
	Print @ProgramTypeGuid
	Print @ShipToAddressGuid
	Print @TagRatioGuid
	Print @ShipMethodGuid


	If @TagRatioGuid IS NULL
	BEGIN
		Update 
		epsB2BProd.dbo.B2BIntegrationXML
		set B2BStatus = 'Error' ,
		statusMessage = 'No Order Created for customer PO: ' + @CustomerPONumber + '. Tag Ratio ' + @TagRatioCode + ' is not recognized.' 
		WHERE ID = @B2BID

		Return
	END


	If @ShipMethodGuid IS NULL
	BEGIN
		Update 
		epsB2BProd.dbo.B2BIntegrationXML
		set B2BStatus = 'Error' ,
		statusMessage = 'No Order Created for customer PO: ' + @CustomerPONumber + '. Ship Method ' + @ShipMethodCode + ' is not recognized.' 
		WHERE ID = @B2BID

		Return
	END

	If @POCheckGuid != ''
	BEGIN
		Update 
		epsB2BProd.dbo.B2BIntegrationXML
		set B2BStatus = 'Error' ,
		statusMessage = 'No Order Created for customer PO: ' + @CustomerPONumber + '. PO ' + @CustomerPONumber + ' already Exists.' 
		WHERE ID = @B2BID

		Return
	END

	--Check for multiple sellers
	Declare @SellerCount int
	set @SellerCount = (select count (Distinct pr.sellerGuid) From #OrderLine l
						Join Product p on l.EPSProductID = p.ID
						JOIN Program pr on p.ProgramGuid = pr.Guid)
	If @SellerCount != 1
	BEGIN
		Update 
		epsB2BProd.dbo.B2BIntegrationXML
		set B2BStatus = 'Error' ,
		statusMessage = 'No Order Created for customer PO: ' + @CustomerPONumber + '. Multiple Seller Proudcts on Order.' 
		WHERE ID = @B2BID

		Return
	END

	set @SellerGuid  = (select top 1 pr.sellerGuid From #OrderLine l
						Join Product p on l.EPSProductID = p.ID
						JOIN Program pr on p.ProgramGuid = pr.Guid)


	--Insert Order Record
	Insert into GrowerOrder Values
	(Newid(),NULL
	,@GrowerGuid
	,@ShipWeekGuid
	,'unassigned'
	,@OrderDescription
	,@CustomerPONumber
	,(SELECT Guid from Lookup where path = 'Code/OrderType/Order')
	,@ProductFormCategoryGuid
	,@ProgramTypeGuid
	,@ShipToAddressGuid
	,(SELECT Guid from Lookup where path = 'Code/PaymentType/Invoice')
	,(SELECT Guid from Lookup where path = 'Code/GrowerOrderStatus/AwaitingCreditApproval')
	,GetDAte()
	,NULL
	,@PersonGuid
	,''
	,@GrowerShipMethodCode
	,@SellerGuid
	,1
	)

	--select * from supplierorder order by ID desc
	Select  @GrowerOrderID = SCOPE_IDENTITY()
	Update GrowerOrder set 
	OrderNo = 'A' + '00' +  cast(@GrowerOrderID as varchar(30))
	where ID = @GrowerOrderID

	--Insert into GrowerOrder History 
	Declare @EventDate datetime
	Set @EventDate = getdate()
	SELECT @LogTypeLookupGuid = Guid from Lookup where Path = 'Logging/LogType/Grower/GrowerOrderPlace'

	Select @GrowerOrderGuid = Guid from GrowerOrder where ID = @GrowerOrderID 

	If @GrowerOrderGuid IS NULL
	BEGIN
		Update 
		epsB2BProd.dbo.B2BIntegrationXML
		set B2BStatus = 'Error' ,
		statusMessage = 'No Order Created for customer PO: ' + @CustomerPONumber + '. Order Header Insert Failed.' 
		WHERE ID = @B2BID

		Return
	END


	EXEC GrowerOrderHistoryAdd   NULL,NULL,@GrowerOrderGuid,'Order Placed B2B',0,@PersonGuid,@EventDate,@LogTypeLookupGuid

	--Insert into SupplierOrder 
	--select top 100 * from supplierOrder order by id desc
	DECLARE db_SO_Cursor CURSOR FOR  
	SELECT Distinct pr.SupplierGuid 
	FROM #OrderLine ol
	JOIN Product p on ol.EPSProductID  = p.ID
	JOIN Program pr on p.ProgramGuid = pr.Guid

	OPEN db_SO_Cursor   
	FETCH NEXT FROM db_SO_Cursor INTO @SupplierGuid   

	WHILE @@FETCH_STATUS = 0   
	BEGIN   
		   INSERT INTO SupplierOrder
		(
			Guid,
			GrowerOrderGuid,
			SupplierGuid,
			PoNo,
			SupplierOrderNo,
			TagRatioLookupGuid,
			ShipMethodLookupGuid,
			SupplierOrderStatusLookupGuid,
			TotalCost
		)
		VALUES
		(
			newid(),
			@GrowerOrderGuid,
			@SupplierGuid,
			'',
			'unassigned',
			@TagRatioGuid,
			@ShipMethodGuid,
			'FFB383EC-4CE9-4C50-87A4-234E07D15180',
			0.00 --TotalCost
		)

		Select  @SupplierOrderID = SCOPE_IDENTITY()
		Update SupplierOrder set 
		SupplierOrderNo = 'SO' + '00' +  cast(@SupplierOrderID as varchar(30))
		where ID = @SupplierOrderID

		FETCH NEXT FROM db_SO_Cursor INTO @SupplierGuid   
	END   

	CLOSE db_SO_Cursor   
	DEALLOCATE db_SO_Cursor


	--select * from lookup where path like 'code/orderlin%'
	--select top 100 * from orderline Order by ID desc

	--Insert into OrderLines 
	Insert into OrderLine
	SELECT newid(),null
	,so.Guid,p.Guid
	,ol.OrderedQty,0,null,0
	,(select Guid from Lookup where path = 'Code/OrderLineStatus/GrowerNotified')
	,'DBAD2E8D-B056-4CE8-B56D-94DC7E6B4769'
	,getdate()
	,getdate()
	,0
	,null
	,null
	,ol.LineNumber
	,''
	FROM #OrderLine ol
	JOIN Product p on ol.EPSProductID  = p.ID
	JOIN Program pr on p.ProgramGuid = pr.Guid
	Join SupplierOrder so on pr.SupplierGuid = so.SupplierGuid and so.GrowerOrderGuid = @GrowerOrderGuid

	--Update OrderLine based on Qty and comment for reductions
	Update OrderLine
	set QtyOrdered = 
		Case when ra.AvailabilityTypeLookupGuid = (select Guid from Lookup where path  =  'Code/AvailabilityType/OPEN')
				then ol.QtyOrdered
				when  ra.Qty - ra.SalesSinceDateReported <= 0 
				then 0
				when ra.Qty - ra.SalesSinceDateReported < ol.QtyOrdered
				then ra.Qty - ra.SalesSinceDateReported
			else
					ol.QtyOrdered
		end,
		LineComment = 
		Case when ra.AvailabilityTypeLookupGuid = (select Guid from Lookup where path  =  'Code/AvailabilityType/OPEN')
				then ''
				when  ra.Qty - ra.SalesSinceDateReported <= 0 
				then 'Line Cancelled because no availability'
				when ra.Qty - ra.SalesSinceDateReported < ol.QtyOrdered
				then 'Line Reduced due to availability limits'
			else
				''	
		end,
		OrderLineStatusLookupGuid = 
		case when ra.Qty - ra.SalesSinceDateReported <= 0 
			then (select Guid from Lookup where path = 'Code/OrderLineStatus/Cancelled')
			else ol.OrderLineStatusLookupGuid
		end
	From Orderline ol
	Join SupplierOrder so on ol.SupplierOrderGuid = so.Guid
	Join ReportedAvailability ra on ol.ProductGuid = ra.ProductGuid
		AND ra.ShipWeekGuid = @ShipWeekguid
	where so.GrowerOrderGuid = @GrowerOrderGuid

	--Update Availability
	Update ReportedAvailability
	set SalesSinceDateReported = SalesSinceDateReported + ol.QtyOrdered
		
	From ReportedAvailability ra 
	Join  Orderline ol on ol.ProductGuid = ra.ProductGuid
		AND ra.ShipWeekGuid = @ShipWeekguid
	Join SupplierOrder so on ol.SupplierOrderGuid = so.Guid
	where so.GrowerOrderGuid = @GrowerOrderGuid


	

	DECLARE db_SO2_Cursor CURSOR FOR  
	SELECT Distinct Guid 
	FROM SupplierOrder 
	where GrowerOrderGuid = @GrowerOrderGuid

	OPEN db_SO2_Cursor
	FETCH NEXT FROM db_SO2_Cursor INTO @SupplierOrderGuid   

	WHILE @@FETCH_STATUS = 0   
	BEGIN   
		--Recalc Fees 
		Exec SupplierOrderRecalcFees Null,Null, @SupplierOrderGuid
		--Recalc Prices
		Exec SupplierOrderReprice @SupplierOrderGuid
		FETCH NEXT FROM db_SO2_Cursor INTO @SupplierOrderGuid   
	END   

	CLOSE db_SO2_Cursor   
	DEALLOCATE db_SO2_Cursor

	--print 'got here'
	--print '@StatusMessage--' +  cast(@GrowerOrderID as varchar(200))
	--print '@GrowerOrderID--' + cast(@GrowerOrderID as varchar(20))

	select @StatusMessage = 'Order Created: ' + 'A00' + cast(@GrowerOrderID as varchar(20))
	
	set @EmailSubject = 'B2B Order Created ' + 'A00' + cast(@GrowerOrderID as varchar(20))
	set @EmailBody = 'B2B Order Created ' + 'A00' + cast(@GrowerOrderID as varchar(20)) + ' for Grower ' +  @GrowerName + '.'
	Exec EmailQueueAdd @EmailSubject,@EmailBody,0,'B2B Order Create','info@eplantsource.com','almam@eplantsource.com','scotth@eplantsource.com','',@GrowerOrderGuid,0,0,0
	
	--print 'got here2'
	Update 
	epsB2BProd.dbo.B2BIntegrationXML
	set B2BStatus = 'Sending Confirmation',
	StatusMessage = @StatusMessage,
	OrderGuid = ISNULL(@GrowerOrderGuid,'00000000-0000-0000-0000-000000000000')
	WHERE ID = @B2BID


END  --End Order Add
ELSE --Order Update
BEGIN
	--Print 'In Update'
	
	Update 
	epsB2BProd.dbo.B2BIntegrationXML
	set B2BType = 'Order Update',
	statusMessage = cast(GetDAte() as varchar(200))
	WHERE ID = @B2BID

	If @GrowerGuid <> (Select top 1 Guid from Grower where name = @GrowerName)
	BEGIN
		Update 
		epsB2BProd.dbo.B2BIntegrationXML
		set B2BStatus = 'Error' ,
		statusMessage = 'No Order Updated for customer PO: ' + @CustomerPONumber + '. Grower Name ' + @GrowerName + ' does not match the Public Key.' 
		WHERE ID = @B2BID

		Return
	END

	If @OrderNo = ''
	BEGIN
		select @OrderNo = OrderNo 
		from GrowerOrder
		where CustomerPoNo = @CustomerPONumber and GrowerGuid = @GrowerGuid
	END

	--Print 'In Update 2'
	Print 'ORder:  ' + @OrderNo
	SELECT @GrowerOrderGuid = o.Guid ,
	@ExistingCustomerPONumber = o.CustomerPoNo,
	@ExistingShipDate = sw.MondayDate,
	@SellerGuid  = o.SellerGuid
	FROM GrowerOrder o
	Join ShipWeek sw on o.ShipWeekGuid = sw.Guid
	WHERE o.Orderno = @OrderNo

	--Print 'In Update 3'
	If ISNULL(cast(@GrowerOrderGuid as varchar(50)),'') =  ''
	BEGIN
		Update 
		epsB2BProd.dbo.B2BIntegrationXML
		set B2BStatus = 'Error' ,
		statusMessage = 'No Order Updated for Grower Name: ' + @GrowerName + ' for PO: ' + @CustomerPONumber + '. Order No ' + @OrderNo + ' is not found.' 
		WHERE ID = @B2BID

		Return
	END
	--Print 'In Update 4'
	set @StatusMessage = @OrderNo + ' ' + cast(GetDate() as varchar(200))


	UPDATE epsB2BProd.dbo.B2BIntegrationXML
	SET OrderGuid =  @GrowerOrderGuid ,
	StatusMessage = @StatusMessage
	WHERE ID = @B2BID

	Print @OrderNo
	Print @GrowerGuid
	Print @GrowerName
	Print @RevisionNumber
	Print @CustomerPONumber
	Print @OrderDescription
	Print @OrderCreatedDate
	Print @ShipWeekCode
	Print @ShipMethodCode
	Print @TagRatioCode
	Print @ShipToAddressID
	Print @ExistingCustomerPONumber
	Print @ExistingShipDate

	--Print 'In Update 5'

	--check the Customer PO field
	If (@CustomerPONumber != '' AND
		@CustomerPONumber !=  left(@ExistingCustomerPONumber,len(@CustomerPONumber))
		)
	BEGIN
		--update the order
		Update GrowerOrder set CustomerPoNo =  @CustomerPONumber where Guid = @GrowerOrderGuid
	
	
		set @Body =  @OrderNo + ' - Customer PO Number updated from ' + @ExistingCustomerPONumber + ' to '  + @CustomerPONumber
		--send the email to Alma
		exec EmailQueueAdd 'B2B Order Update',@Body ,0,'B2B Order Update','CustomerService@eplantsource.com','CustomerService@eplantsource.com','','',@GrowerOrderGuid,0,0,0

	END

	--Check the ship Method
	If @ShipMethodCode not in( '01','FED EX','FEDEX','TRUCK','CPU')
	BEGIN
		set @Body =  @OrderNo + ' - ship method sent by Grower is ' + @ShipMethodCode + '.  This is not in our system as valid, please alert Scott. ' 
   		--send the email to Alma
		exec EmailQueueAdd 'B2B Order Update',@Body ,0,'B2B Order Update','CustomerService@eplantsource.com','CustomerService@eplantsource.com','','',@GrowerOrderGuid,0,0,0

	END

	----check the Ship Week

	--If @ShipWeekCode != 
	--BEGIN
	--	set @Body =  @OrderNo + ' - Requested Ship Week changed from ' + cast(@ExistingShipDate as varchar(30)) + ' to ' + @RequestedShipDate + '.  Order NOT updated, just for you to check into. ' 

	--   	--send the email to Alma
	--	exec EmailQueueAdd 'B2B Order Update',@Body ,0,'B2B Order Update','CustomerService@eplantsource.com','CustomerService@eplantsource.com','','',@GrowerOrderGuid,0,0,0

	--END



	INSERT INTO #OrderLine
	SELECT  b.GrowerGuid
		, x.XmlCol.value('(LineNumber)[1]','bigint') as LineNumber
		,x.XmlCol.value('(EPSProductID)[1]','bigint') as EPSProductID
		,x.XmlCol.value('(ProductDescription)[1]','nvarchar(200)') as ProductDescription
		,x.XmlCol.value('(OrderedQty)[1]','int') as OrderedQty
		,x.XmlCol.value('(ChangeType)[1]','nvarchar(100)') as ChangeType
		,x.XmlCol.value('(Status)[1]','nvarchar(100)') as Status
		,0 as QtyChange
		,0 as AfterAvailQtyChange
		,'00000000-0000-0000-0000-000000000000' as SupplierGuid
		--to do Grower Line Item comment
	 
	FROM    epsB2BProd.dbo.B2BIntegrationXML b
	CROSS APPLY @XML.nodes('/Order/OrderLines/Item') x(XmlCol)
	WHERE b.ID = @B2BID

	If @iDebug = 1
	BEGIN
		select @GrowerOrderGuid,@OrderNo,@CustomerPONumber,@ShipWeekCode,@ShipMethodCode
		select * from #OrderLine
	END

	--Clear out unmatched products
	Delete from #OrderLine where EPSProductID = 0

	--capture qty change
	Update #OrderLine
	set QtyChange =  #OrderLine.OrderedQty - IsNULL(olv.QtyOrdered,0)
	from #OrderLine
	Left Join OrderLineViewShowInactiveProduct olv on olv.GrowerOrderGuid = @GrowerOrderGuid and olv.LineNumber = #OrderLine.LineNumber  

	--set supplier orders
	Update #OrderLine
	set SupplierGuid =  pr.SupplierGuid
	from #OrderLine 
	JOIN Product p on #OrderLine.EPSProductID  = p.ID
	JOIN Program pr on p.ProgramGuid = pr.Guid
	

	----Check for multiple sellers
	Declare @WrongSellerCount int
	set @WrongSellerCount = (select count (*) From #OrderLine l
						Join Product p on l.EPSProductID = p.ID
						JOIN Program pr on p.ProgramGuid = pr.Guid
						Where pr.SellerGuid != @SellerGuid)
	If @WrongSellerCount != 0
	BEGIN
		Update 
		epsB2BProd.dbo.B2BIntegrationXML
		set B2BStatus = 'Error' ,
		statusMessage = 'No Order Updated for customer PO: ' + @CustomerPONumber + '. Multiple Seller Proudcts on Order.' 
		WHERE ID = @B2BID

		Return
	END
	
	if @iDebug = 1
	Begin
		 SELECT 'Supplier Guid', SupplierGuid from SupplierOrder where GrowerOrderGuid = @GrowerOrderGuid
		SELECT *  		FROM #OrderLine ol

		 SELECT Distinct ol.SupplierGuid 
		FROM #OrderLine ol
		SELECT Distinct ol.SupplierGuid 
		FROM #OrderLine ol
		Where ol.SupplierGuid not in 
			(
				SELECT SupplierGuid from SupplierOrder where GrowerOrderGuid = @GrowerOrderGuid
			)
	End
	--Add Supplier Orders if needed
	DECLARE db_SO_Cursor3 CURSOR FOR  
	SELECT Distinct ol.SupplierGuid 
	FROM #OrderLine ol
	Where ol.SupplierGuid not in 
		(
			SELECT SupplierGuid from SupplierOrder where GrowerOrderGuid = @GrowerOrderGuid
		)
		and ol.SupplierGuid != '00000000-0000-0000-0000-000000000000'

	OPEN db_SO_Cursor3   
	FETCH NEXT FROM db_SO_Cursor3 INTO @SupplierGuid   

	WHILE @@FETCH_STATUS = 0   
	BEGIN   
		
		INSERT INTO SupplierOrder
		(
			Guid,
			GrowerOrderGuid,
			SupplierGuid,
			PoNo,
			SupplierOrderNo,
			TagRatioLookupGuid,
			ShipMethodLookupGuid,
			SupplierOrderStatusLookupGuid,
			TotalCost
		)
		VALUES
		(
			newid(),
			@GrowerOrderGuid,
			@SupplierGuid,
			'',
			'unassigned',
			@TagRatioGuid,
			@ShipMethodGuid,
			'FFB383EC-4CE9-4C50-87A4-234E07D15180',
			0.00 --TotalCost
		)

		Select  @SupplierOrderID = SCOPE_IDENTITY()
		Update SupplierOrder set 
		SupplierOrderNo = 'SO' + '00' +  cast(@SupplierOrderID as varchar(30))
		where ID = @SupplierOrderID

		FETCH NEXT FROM db_SO_Cursor3 INTO @SupplierGuid   
	END   

	CLOSE db_SO_Cursor3   
	DEALLOCATE db_SO_Cursor3
	
	
	--Update incoming OrderLine based on avail Qty 
	Update #OrderLine
	set AfterAvailQtyChange = 
		Case when ra.AvailabilityTypeLookupGuid = (select Guid from Lookup where path  =  'Code/AvailabilityType/OPEN')
				then ol.QtyChange
			when  ra.Qty - ra.SalesSinceDateReported <= 0 
				then 0
			when ra.Qty - ra.SalesSinceDateReported < ol.OrderedQty
				then ra.Qty - ra.SalesSinceDateReported
			else
				ol.QtyChange
		end
	From #OrderLine ol
	Join OrderLineViewShowInactiveProduct olv on olv.LineNumber =  ol.LineNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
	Join ReportedAvailability ra on olv.ProductGuid = ra.ProductGuid
		AND ra.ShipWeekGuid = @ShipWeekguid
	

	-- If cancelled, we need to add comment and update status
	Insert into GrowerOrderHistory
	SELECT 
	NewID(),NULL,@GrowerOrderGuid,
	'Grower Cancelled or Acknowledged cancelled order for ' + cast(olv.QtyOrdered as varchar(20)) + ' ' + olv.VarietyName + '.',
	0,
	'27DE09B0-FDD9-40E1-BBAC-32A66081E3C9', --customer service guid
	getdate(),
	(select guid from Lookup where path = 'Logging/LogType/Grower/OrderLineCancel')
	FROM OrderLine ol
	Join OrderLineViewShowInactiveProduct olv on ol.Guid = olv.OrderLineGuid
	Join #OrderLine on ol.LineNumber = #OrderLine.LineNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
	WHERE 
	#OrderLine.OrderedQty = 0 
	AND olv.QtyOrdered != 0 --Cancelled in this run
	AND olv.OrderLineStatusLookupGuid != (select Guid from lookup where path = 'Code/OrderLineStatus/Cancelled')
	AND olv.OrderLineStatusLookupGuid != (select Guid from lookup where path = 'Code/OrderLineStatus/GrowerB2BError')

	--Now Cancel the items on our side.
	Update OrderLine
	set 
	--select ol.*,
	OrderLineStatusLookupGuid = (select Guid from lookup where path = 'Code/OrderLineStatus/GrowerCancelled'),
	QtyOrdered = #OrderLine.OrderedQty
	--,
	--to do grower line comment LineComment = #OrderLine.LineItemComment  --may need to handle nulls here or only write if not null and not ''

	FROM OrderLine ol
	Join OrderLineViewShowInactiveProduct olv on ol.Guid = olv.OrderLineGuid
	Join #OrderLine on ol.LineNumber = #OrderLine.LineNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
	WHERE
		#OrderLine.OrderedQty = 0 
		AND olv.QtyOrdered != 0 --Cancelled in this run
		AND olv.OrderLineStatusLookupGuid != (select Guid from lookup where path = 'Code/OrderLineStatus/Cancelled')
		AND olv.OrderLineStatusLookupGuid != (select Guid from lookup where path = 'Code/OrderLineStatus/GrowerB2BError')

	-- If the item is added, we need to add comment and add line items
	Insert into GrowerOrderHistory
	SELECT 
	NewID(),NULL,@GrowerOrderGuid,
	CAse when AfterAvailQtyChange < QtyChange 
		then 'Order for ' + cast(#OrderLine.AfterAvailQtyChange as varchar(20)) + ' ' + v.Name + ' added. Reduced from ' + cast(#OrderLine.OrderedQty as varchar(20)) + ' per Availability.'
		else 'Order for ' + cast(#OrderLine.AfterAvailQtyChange as varchar(20)) + ' ' + v.Name + ' added.'
		end,
	0,
	'27DE09B0-FDD9-40E1-BBAC-32A66081E3C9', --customer service guid
	getdate(),
	(select guid from Lookup where path = 'Logging/LogType/Grower/OrderLineAdd')
	FROM  #OrderLine
	Join Product p on #OrderLine.EPSProductID = p.ID
	Join Variety v on p.VarietyGuid = v.Guid
	WHERE #OrderLine.LineNumber
	Not in(
		SELECT olv.LineNumber 
		from OrderLineViewShowInactiveProduct olv
		where olv.GrowerOrderGuid = @GrowerOrderGuid
	)
	and #OrderLine.SupplierGuid != '00000000-0000-0000-0000-000000000000' 
	
	----add the lines
	Insert into OrderLine
	SELECT 
	NewID(),NULL,
	(select Guid from SupplierOrder where GrowerOrderGuid = @GrowerOrderGuid and SupplierGuid = #OrderLine.SupplierGuid),
	(select top 1 guid from Product where ID = #OrderLine.EPSProductID),  
	#OrderLine.AfterAvailQtyChange,
	0,
	NULL,
	0,
	(SELECT Guid from lookup where path = 'Code/OrderLineStatus/GrowerNotified'),
	(SELECT Guid from Lookup where path = 'Code/ChangeType/NoChange'),
	getdate(),
	getdate(),
	0,
	NULL,
	getdate(),
	#OrderLine.LineNumber,
	LineComment = ''   --To Do Grower comment #OrderLine.LineItemComment  --may need to handle nulls here or only write if not null and not ''

	FROM  #OrderLine
	WHERE #OrderLine.LineNumber
	Not in(
		SELECT olv.LineNumber 
		from OrderLineViewShowInactiveProduct olv
		where olv.GrowerOrderGuid = @GrowerOrderGuid
		
	)
	and #OrderLine.SupplierGuid != '00000000-0000-0000-0000-000000000000'


	--For Dummen updates, if the quantity in our side is 0, need to do an add else do an update
	-- If the confirmed qty is different, we need to add comment and update status
	Insert into GrowerOrderHistory
		SELECT 
		NewID(),NULL,@GrowerOrderGuid,
		 'Order for ' + cast(#OrderLine.AfterAvailQtyChange as varchar(20)) + ' ' + v.Name + ' cannot be added because original Dummen line was cancelled.',
		0,
		'27DE09B0-FDD9-40E1-BBAC-32A66081E3C9', --customer service guid
		getdate(),
		(select guid from Lookup where path = 'Logging/LogType/Grower/OrderLineChange')
	FROM OrderLine ol
		Join OrderLineViewShowInactiveProduct olv on ol.Guid = olv.OrderLineGuid
		Join #OrderLine on ol.LineNumber = #OrderLine.LineNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
		Join Product p on ol.ProductGuid = p.Guid
		Join Variety v on p.VarietyGuid = v.Guid
	WHERE 
		#OrderLine.OrderedQty > 0 --Not cancelled in this run
		AND #OrderLine.OrderedQty <> olv.QtyOrdered
		AND olv.OrderLineStatusLookupGuid <> (select Guid from lookup where path = 'Code/OrderLineStatus/GrowerB2BError')
		AND #OrderLine.SupplierGuid != '00000000-0000-0000-0000-000000000000'
		AND (olv.SellerCode = 'DMO' AND olv.QtyOrdered = 0)
	
	--send email due to lines missing
	declare @notAddedCount bigint
	set @notAddedCount = 
		(SELECT count(*)
			FROM OrderLine ol
			Join OrderLineViewShowInactiveProduct olv on ol.Guid = olv.OrderLineGuid
			Join #OrderLine on ol.LineNumber = #OrderLine.LineNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
			WHERE
				#OrderLine.OrderedQty <> olv.QtyOrdered
				AND #OrderLine.OrderedQty > 0
				AND olv.OrderLineStatusLookupGuid <> (select Guid from lookup where path = 'Code/OrderLineStatus/GrowerB2BError')
				AND #OrderLine.SupplierGuid != '00000000-0000-0000-0000-000000000000'
				AND (olv.SellerCode = 'DMO' AND olv.QtyOrdered = 0)
		)
	IF @notAddedCount > 0 
	BEGIN
		set @Body =  @OrderNo + ' - Some Qty could not be added to the order (' + cast(@notAddedCount as varchar(10)) + ') because the original line was cancelled. ' 

		exec EmailQueueAdd 'B2B Order No Uncancel Warning',@Body ,0,'B2B Order No Uncancel Warning','CustomerService@eplantsource.com','CustomerService@eplantsource.com','','',@GrowerOrderGuid,0,0,0

	END


	----add the lines
	--Took this out to replace with Email notification on no ability to "uncancel" a Dummen line
	--declare @lineCount bigint
	--set @lineCount = (select count(*) from OrderLineViewShowInactiveProduct where GrowerOrderGuid = @GrowerOrderGuid)
	--Insert into OrderLine
	--	SELECT 
	--	NewID(),NULL,
	--	(select Guid from SupplierOrder where GrowerOrderGuid = @GrowerOrderGuid and SupplierGuid = #OrderLine.SupplierGuid),
	--	(select top 1 guid from Product where ID = #OrderLine.EPSProductID),  
	--	#OrderLine.AfterAvailQtyChange,
	--	0,
	--	NULL,
	--	0,
	--	(SELECT Guid from lookup where path = 'Code/OrderLineStatus/GrowerNotified'),
	--	(SELECT Guid from Lookup where path = 'Code/ChangeType/NoChange'),
	--	getdate(),
	--	getdate(),
	--	0,
	--	NULL,
	--	getdate(),
	--	#OrderLine.LineNumber + @lineCount,
	--	LineComment = ''   --To Do Grower comment #OrderLine.LineItemComment  --may need to handle nulls here or only write if not null and not ''
	--FROM OrderLine ol
	--Join OrderLineViewShowInactiveProduct olv on ol.Guid = olv.OrderLineGuid
	--Join #OrderLine on ol.LineNumber = #OrderLine.LineNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
	--WHERE
	--	#OrderLine.OrderedQty <> olv.QtyOrdered
	--	AND #OrderLine.OrderedQty > 0
	--	AND olv.OrderLineStatusLookupGuid <> (select Guid from lookup where path = 'Code/OrderLineStatus/GrowerB2BError')
	--	AND #OrderLine.SupplierGuid != '00000000-0000-0000-0000-000000000000'
	--	AND (olv.SellerCode = 'DMO' AND olv.QtyOrdered = 0)

	--For Non Dummen updates, or if the original Qty is not 0, do the update
	Insert into GrowerOrderHistory
		SELECT 
		NewID(),NULL,@GrowerOrderGuid,
		CAse when AfterAvailQtyChange < QtyChange 
		then 'Order for ' + cast(olv.QtyOrdered as varchar(20)) + ' ' + olv.VarietyName + ' changed to '  + cast((olv.QtyOrdered + #OrderLine.AfterAvailQtyChange) as varchar(20)) + '. Reduced from ordered amount ' + cast(#OrderLine.OrderedQty as varchar(20)) + ' per Availability.'
		else 'Order for ' + cast(olv.QtyOrdered as varchar(20)) + ' ' + olv.VarietyName + ' changed to '  + cast((olv.QtyOrdered + #OrderLine.AfterAvailQtyChange) as varchar(20))
		end,
		0,
		'27DE09B0-FDD9-40E1-BBAC-32A66081E3C9', --customer service guid
		getdate(),
		(select guid from Lookup where path = 'Logging/LogType/Grower/OrderLineChange')
		FROM OrderLine ol
		Join OrderLineViewShowInactiveProduct olv on ol.Guid = olv.OrderLineGuid
		Join #OrderLine on ol.LineNumber = #OrderLine.LineNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
	WHERE 
		#OrderLine.OrderedQty > 0 --Not cancelled in this run
		AND #OrderLine.OrderedQty <> olv.QtyOrdered
		AND olv.OrderLineStatusLookupGuid <> (select Guid from lookup where path = 'Code/OrderLineStatus/GrowerB2BError')
		AND #OrderLine.SupplierGuid != '00000000-0000-0000-0000-000000000000'
		AND (olv.SellerCode != 'DMO' OR olv.QtyOrdered = 0)

	--now update the order line on our side.
	Update OrderLine
		set 
		--select ol.*,
		OrderLineStatusLookupGuid = (select Guid from lookup where path = 'Code/OrderLineStatus/GrowerEdit'),
		QtyOrdered = olv.QtyOrdered + #OrderLine.AfterAvailQtyChange
		--,--to do GrowerLine Comment  LineComment = #OrderLine.LineItemComment  --may need to handle nulls here or only write if not null and not ''
	FROM OrderLine ol
		Join OrderLineViewShowInactiveProduct olv on ol.Guid = olv.OrderLineGuid
		Join #OrderLine on ol.LineNumber = #OrderLine.LineNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
	WHERE
		#OrderLine.OrderedQty <> olv.QtyOrdered
		AND #OrderLine.OrderedQty > 0
		AND olv.OrderLineStatusLookupGuid <> (select Guid from lookup where path = 'Code/OrderLineStatus/GrowerB2BError')
		AND #OrderLine.SupplierGuid != '00000000-0000-0000-0000-000000000000'
		AND (olv.SellerCode != 'DMO' OR olv.QtyOrdered = 0)



	--check the Product ID on all items
	declare @NoMatchCount int
	set @NoMatchCount = (
		select count(*) 
		FROM OrderLine ol
		Join OrderLineViewShowInactiveProduct olv on ol.Guid = olv.OrderLineGuid
		Join Product p on olv.ProductGuid = p.Guid
		Join #OrderLine on ol.LineNumber = #OrderLine.LineNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
		WHERE #OrderLine.EPSProductID <> p.ID
	)
	If @NoMatchCount > 0
	BEGIN
		set @Body =  @OrderNo + ' - Some line items do not have the correct Product ID.' + cast(@NoMatchCount as varchar(5)) + ' items.' 
   		--send the email to Alma
		exec EmailQueueAdd 'B2B Order Update',@Body ,0,'B2B Order Update','CustomerService@eplantsource.com','CustomerService@eplantsource.com','','',@GrowerOrderGuid,0,0,0
	END



	--Update Availability
	Update ReportedAvailability
	set SalesSinceDateReported = ra.SalesSinceDateReported + #OrderLine.AfterAvailQtyChange
		
	FROM OrderLine ol
	Join OrderLineViewShowInactiveProduct olv on ol.Guid = olv.OrderLineGuid
	Join #OrderLine on ol.LineNumber = #OrderLine.LineNumber and olv.GrowerOrderGuid = @GrowerOrderGuid
	Join ReportedAvailability ra on olv.ProductGuid = ra.ProductGuid
		AND ra.ShipWeekGuid = @ShipWeekguid
	Join SupplierOrder so on olv.SupplierOrderGuid = so.Guid
	where so.GrowerOrderGuid = @GrowerOrderGuid


	DECLARE db_SO3_Cursor CURSOR FOR  
	SELECT Distinct Guid 
	FROM SupplierOrder 
	where GrowerOrderGuid = @GrowerOrderGuid

	OPEN db_SO3_Cursor
	FETCH NEXT FROM db_SO3_Cursor INTO @SupplierOrderGuid   

	WHILE @@FETCH_STATUS = 0   
	BEGIN   
		--Recalc Fees 
		Exec SupplierOrderRecalcFees Null,Null, @SupplierOrderGuid
		--Recalc Prices
		Exec SupplierOrderReprice @SupplierOrderGuid
		FETCH NEXT FROM db_SO3_Cursor INTO @SupplierOrderGuid   
	END   

	CLOSE db_SO3_Cursor   
	DEALLOCATE db_SO3_Cursor

	--Save the revision number
	Update GrowerOrder set RevisionNumber = @RevisionNumber where guid = @GrowerOrderGuid

	set @StatusMessage = @StatusMessage + ' - Finished'
	print @GrowerOrderID
	print 'sending mail ' + 'B2B Order Updated ' + @OrderNo
	set @EmailSubject = 'B2B Order Updated ' + @OrderNo
	set @EmailBody = 'B2B Order Updated ' + @OrderNo + ' for Grower ' + @GrowerName
	Exec EmailQueueAdd @EmailSubject,@EmailBody,0,'B2B Order Update','info@eplantsource.com','almam@eplantsource.com','scotth@eplantsource.com','',@GrowerOrderGuid,0,0,0


	Update 
	epsB2BProd.dbo.B2BIntegrationXML
	set B2BStatus = 'Sending Confirmation',
	StatusMessage = @StatusMessage,
	OrderGuid = ISNULL(@GrowerOrderGuid,'00000000-0000-0000-0000-000000000000')
	WHERE ID = @B2BID
END --Order ADD


drop table #OrderLine
﻿CREATE PROCEDURE [dbo].[SupplierOrderUpdate]
	@UserGuid AS UNIQUEIDENTIFIER,
	@SupplierOrderGuid AS UNIQUEIDENTIFIER,
	@TagRatioLookupCode AS NCHAR(20),
	@ShipMethodLookupCode AS NCHAR(20),
	@Success AS BIT OUT
AS
	SET @Success = 0
	
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('SupplierOrderGuid',@SupplierOrderGuid,1) +
		dbo.XmlSegment('TagRatioLookupCode',@TagRatioLookupCode,1) +
		dbo.XmlSegment('ShipMethodLookupCode',@ShipMethodLookupCode,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='SupplierOrderUpdate',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	DECLARE @TagRatioLookupGuid AS UNIQUEIDENTIFIER
	EXECUTE LookupGetGuid
		@UserGuid=@UserGuid,
		@ProcessLookupCode='SupplierOrderUpdate',
		@Path='Code/TagRatio',
		@LookupCode=@TagRatioLookupCode,
		@LookupGuid=@TagRatioLookupGuid OUT

	DECLARE @ShipMethodLookupGuid AS UNIQUEIDENTIFIER
	EXECUTE LookupGetGuid
		@UserGuid=@UserGuid,
		@ProcessLookupCode='SupplierOrderUpdate',
		@Path='Code/ShipMethod',
		@LookupCode=@ShipMethodLookupCode,
		@LookupGuid=@ShipMethodLookupGuid OUT

	DECLARE @RowCount AS INTEGER

	UPDATE SupplierOrder
	SET
		TagRatioLookupGuid=@TagRatioLookupGuid,
		ShipMethodLookupGuid=@ShipMethodLookupGuid
	WHERE Guid=@SupplierOrderGuid

	SET @RowCount=@@ROWCOUNT

	IF @RowCount != 1
		EXECUTE EventLogAdd
			@Comment='RowCount after updating SupplierOrder table was not 1',
			@IntValue=@RowCount,
			@LogTypeLookupCode='RowUpdateError',
			@ProcessLookupCode='SupplierOrderUpdate',
			@UserGuid=@UserGuid,
			@GuidValue=@OriginalEventLogGuid
	ELSE
		SET @Success = 1

	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('Success',@Success,1),
		0
	)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='SupplierOrderUpdate',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@UserGuid=@UserGuid,
		@XmlData=@ReturnValues
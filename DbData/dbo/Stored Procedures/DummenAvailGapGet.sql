﻿








CREATE PROCEDURE [dbo].[DummenAvailGapGet]
	@StartWeek  nvarchar(10) ,
	@EndWeek  nvarchar(10)  
	 

AS

--Select ShipWeekContinuousWeekNumber from shipweekView where ShipWeekCode = @StartWeek
--Select ShipWeekContinuousWeekNumber from shipweekView where ShipWeekCode = @EndWeek

SELECT --top 100
	 pr.Code as ProgramCode,pr.Name as ProgramName,sp.Name as SpeciesName,v.Name as VarietyName,SupplierIdentifier,SupplierDescription
	 ,rtrim(Max(rat.Code)) as Status
	FROM Program pr
	JOIN Seller se on pr.SellerGuid = se.Guid
	JOIN Product p on pr.guid = p.ProgramGuid
	JOIN Variety v on v.guid = p.VarietyGuid
	JOIN Species sp on sp.guid = v.SpeciesGuid
	JOIN REportedAvailability ra on p.guid = ra.ProductGuid
	JOIN Lookup rat on ra.AvailabilityTypeLookupGuid = rat.Guid
	JOIN ShipWeek sw on ra.ShipWeekGuid  = sw.Guid
WHERE 
	p.DateDeactivated is null
	AND se.code = 'DMO'
	AND sw.ContinuousWeekNumber >= (Select ShipWeekContinuousWeekNumber from shipweekView where ShipWeekCode = @StartWeek)
	AND sw.ContinuousWeekNumber <= (Select ShipWeekContinuousWeekNumber from shipweekView where ShipWeekCode = @EndWeek)

GROUP BY
	pr.Code,pr.Name,sp.Name,v.Name,SupplierIdentifier,SupplierDescription
HAVING 
	Min(rat.Code) = 'NA'
ORDER BY
	pr.Name,sp.Name,v.Name,SupplierIdentifier,SupplierDescription
--select * from lookup where path like 'Code/AvailabilityType%'
﻿






CREATE PROCEDURE [dbo].[VarietyCombine]
	@KeepCode AS varchar(10),
	@DeleteCode  AS varchar(10)
AS
	
Declare @CodeCheck AS varchar(10)


Select @CodeCheck = Code from variety v
where code = @DeleteCode

If @CodeCheck is null
BEGIN
	RETURN -1
END
ELSE
BEGIN
Select @CodeCheck = Code from variety v
where code = @KeepCode

	If @CodeCheck is null
	BEGIN
		RETURN -1
	END
	ELSE
	BEGIN
		update product set varietyguid = (select guid from variety where code = @KeepCode) 
		where varietyguid = (select guid from variety where code = @DeleteCode) 

		delete from variety where code = @DeleteCode


	END

END
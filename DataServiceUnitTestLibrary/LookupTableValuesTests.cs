﻿using System;
using System.Linq;
using System.Text;
using DataObjectLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataServiceLibrary;
using FluentAssertions;

namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class LookupTableValuesTests
    {
        [TestMethod]
        public void DataServiceLookupTableValuesTest()
        {
            var keepInDatabaseLookup = LookupTableValues.Category.Logging.KeepInDatabase;
            keepInDatabaseLookup.Code.Should().Be("KeepInDatabase");
            keepInDatabaseLookup.ParentLookup.Code.Should().Be("Logging");

            var keyNotFoundLookup = LookupTableValues.Logging.LogType.KeyNotFound;
            keyNotFoundLookup.Code.Should().Be("KeyNotFound");
            keyNotFoundLookup.ParentLookup.Code.Should().Be("LogType");
            keyNotFoundLookup.ParentLookup.ParentLookup.Code.Should().Be("Logging");

            keepInDatabaseLookup.Guid.Should().NotBe(keyNotFoundLookup.Guid);

            keepInDatabaseLookup.ParentLookup.Guid.Should().NotBe(keyNotFoundLookup.ParentLookup.ParentLookup.Guid);

            var calculatedCodeLookup = LookupTableValues.Code.Timing.Code110To120DaysToFlower;
            calculatedCodeLookup.Code.Should().Be("110 - 120 Days to flower");
        }

        [TestMethod]
        public void RegenerateLookupTableValuesClass()
        {
            LookupTableService.SingletonInstance.GetAllAndPreloadAllChildren();

            var csharp = new StringBuilder();

            csharp.AppendLine(AddIndent(0, "using System;"));
            csharp.AppendLine(AddIndent(0, "using DataObjectLibrary;"));
            csharp.AppendLine(AddIndent(0, ""));
            csharp.AppendLine(AddIndent(0, "namespace DataServiceLibrary"));
            csharp.AppendLine(AddIndent(0, "{"));
            csharp.AppendLine(AddIndent(1, "public class LookupTableValues"));
            csharp.AppendLine(AddIndent(1, "{"));

            var lookupList = LookupTableService.SingletonInstance.GetAllByField("ParentLookupGuid", null, "IS", false).ToList();
            lookupList.Sort(new Lookup.Comparer());

            foreach (var lookup in lookupList)
            {
                csharp.AppendLine(ProcessLookupHierarchy(2, lookup, isStatic: true));
            }

            csharp.AppendLine(AddIndent(1, "}"));
            csharp.AppendLine(AddIndent(0, "}"));

            System.Console.WriteLine(csharp.ToString());
        }

        private string ProcessLookup(int indentLevel, Lookup lookup)
        {
            return lookup.ParentLookupList.Count > 0 ? ProcessLookupHierarchy(indentLevel, lookup, isStatic: false) : ProcessLookupLeaf(indentLevel, lookup);
        }

        private string ProcessLookupLeaf(int indentLevel, Lookup lookup, string variableName = null)
        {
            if (string.IsNullOrEmpty(variableName))
            {
                variableName = ConvertCodeToVariableName(lookup.Code);
            }

            return AddIndent(indentLevel, string.Format("public Lookup {0}{1} {{ get{{ return LookupTableService.SingletonInstance.GetByGuid(new Guid(\"{2}\")); }}}}", variableName, CalculateComment(lookup.Code), lookup.Guid));
        }

        private string ProcessLookupHierarchy(int indentLevel, Lookup lookupLevel, bool isStatic)
        {
            var csharp = new StringBuilder();

            string variableName = ConvertCodeToVariableName(lookupLevel.Code);

            string staticText = isStatic ? "static " : "";
            csharp.AppendLine(AddIndent(indentLevel, string.Format("public {0}{1}Values {1}{2} {{ get {{ return new {1}Values();}}}}", staticText, variableName, CalculateComment(lookupLevel.Code))));
            csharp.AppendLine(AddIndent(indentLevel, ""));

            csharp.AppendLine(AddIndent(indentLevel, string.Format("public class {0}Values", variableName)));
            csharp.AppendLine(AddIndent(indentLevel, "{"));

            var childList = lookupLevel.ChildLookupList;
            childList.Sort(new Lookup.Comparer());

            csharp.AppendLine(ProcessLookupLeaf(indentLevel + 1, lookupLevel, "LookupValue"));
            foreach (var lookup in childList)
            {
                csharp.AppendLine(ProcessLookup(indentLevel + 1, lookup));
            }

            csharp.AppendLine(AddIndent(indentLevel, "}"));

            return csharp.ToString();
        }

        private string AddIndent(int indentLevel, string line)
        {
            var lineBuilder = new StringBuilder();

            if (!string.IsNullOrEmpty(line))
            {
                for (int i = 0; i < indentLevel; i++)
                {
                    lineBuilder.Append("    ");
                }
            }

            lineBuilder.Append(line);

            return lineBuilder.ToString();
        }

        private string ConvertCodeToVariableName(string code)
        {
            var stringBuilder = new StringBuilder();


            bool lastCharWasUpper = false;
            bool lastCharWasSpace = false;

            foreach (char c in code)
            {
                char character = c;

                if (char.IsWhiteSpace(character))
                {
                    lastCharWasSpace = true;
                }
                else
                {
                    if (stringBuilder.Length == 0 && !char.IsLetter(character))
                    {
                        stringBuilder.Append("Code");
                    }

                    if (char.IsUpper(character) && lastCharWasUpper)
                    {
                        character = char.ToLower(character);
                    }
                    else if (char.IsLower(character) && lastCharWasSpace)
                    {
                        character = char.ToUpper(character);
                    }

                    if (character == '"')
                    {
                        stringBuilder.Append("Inches");
                    }
                    else if (character == '-')
                    {
                        stringBuilder.Append("To");
                    }
                    else if (character == '.')
                    {
                        stringBuilder.Append("Point");
                    }
                    else
                    {
                        stringBuilder.Append(character);
                    }

                    lastCharWasUpper = char.IsUpper(c);
                    lastCharWasSpace = false;
                }
            }

            return stringBuilder.ToString();
        }

        public string CalculateComment(string code)
        {
            string variableName = ConvertCodeToVariableName(code);

            var comment = variableName == code ? "" : string.Format(" /* {0} */", code);

            return comment;
        }
    }
}

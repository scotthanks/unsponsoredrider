﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using BusinessObjectServices;
using BusinessObjectsLibrary;
namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class GetSupplierDataTests
    {
        [TestMethod]
        public void DataServiceGetSupplierDataTest()
        {
            var userGuid = new Guid(new LookupService().GetConfigValue("SCOTT_USER_GUID"));
            var status = new StatusObject(userGuid, true);
            var getSupplierDataProcedure = new DataServiceLibrary.SupplierDataService(status);
            

            var supplierList = getSupplierDataProcedure.GetSuppliers(null,null,null,null);

            supplierList.Should().NotBeNull();
            supplierList.Count.Should().BeGreaterThan(1);

            int fullSupplierListLength = supplierList.Count;

            foreach (var supplier in supplierList)
            {
                System.Console.WriteLine("{0}:{1}", supplier.Code, supplier.Name);
            }

            supplierList = getSupplierDataProcedure.GetSuppliers("EPS",programTypeCode: "GER");
            supplierList.Should().NotBeNull();
            supplierList.Count.Should().BeGreaterThan(1);
            supplierList.Count.Should().BeLessThan(fullSupplierListLength);

            supplierList = getSupplierDataProcedure.GetSuppliers("EPS", programTypeCode: "*");
            supplierList.Should().NotBeNull();
            supplierList.Count.Should().BeGreaterThan(1);
            supplierList.Count.Should().Be(fullSupplierListLength);

            supplierList = getSupplierDataProcedure.GetSuppliers("EPS", programTypeCode: "");
            supplierList.Should().NotBeNull();
            supplierList.Count.Should().Be(0);

            supplierList = getSupplierDataProcedure.GetSuppliers("EPS", productFormCategoryCode: "URC");
            supplierList.Should().NotBeNull();
            supplierList.Count.Should().BeGreaterOrEqualTo(1);
            supplierList.Count.Should().BeLessThan(fullSupplierListLength);

            supplierList = getSupplierDataProcedure.GetSuppliers("EPS", productFormCategoryCode: "*");
            supplierList.Should().NotBeNull();
            supplierList.Count.Should().BeGreaterOrEqualTo(1);
            supplierList.Count.Should().Be(fullSupplierListLength);

            supplierList = getSupplierDataProcedure.GetSuppliers("EPS", productFormCategoryCode: "");
            supplierList.Should().NotBeNull();
            supplierList.Count.Should().Be(0);

            supplierList = getSupplierDataProcedure.GetSuppliers("EPS", geneticOwnerCodes: new string[] { "BEE" });
            supplierList.Should().NotBeNull();
            supplierList.Count.Should().BeGreaterOrEqualTo(1);
            supplierList.Count.Should().BeLessThan(fullSupplierListLength);
            int justBee = supplierList.Count;

            supplierList = getSupplierDataProcedure.GetSuppliers("EPS", geneticOwnerCodes: new string[] { "BEE", "DUW" });
            supplierList.Should().NotBeNull();
            supplierList.Count.Should().BeGreaterOrEqualTo(1);
            supplierList.Count.Should().BeLessOrEqualTo(fullSupplierListLength);
            supplierList.Count.Should().BeGreaterOrEqualTo(justBee);

            supplierList = getSupplierDataProcedure.GetSuppliers("EPS", geneticOwnerCodes: new string[] { "SYN" });
            supplierList.Should().NotBeNull();
            supplierList.Count.Should().BeGreaterOrEqualTo(1);
            supplierList.Count.Should().BeLessOrEqualTo(fullSupplierListLength);

            supplierList = getSupplierDataProcedure.GetSuppliers("EPS", geneticOwnerCodes: new string[] { "*" });
            supplierList.Should().NotBeNull();
            supplierList.Count.Should().BeGreaterOrEqualTo(1);
            supplierList.Count.Should().Be(fullSupplierListLength);

            supplierList = getSupplierDataProcedure.GetSuppliers("EPS", speciesCodes: new string[] { "ANG" });
            supplierList.Should().NotBeNull();
            supplierList.Count.Should().BeGreaterOrEqualTo(1);
            supplierList.Count.Should().BeLessThan(fullSupplierListLength);

            supplierList = getSupplierDataProcedure.GetSuppliers("EPS", speciesCodes: new string[] { "PLZ" });
            supplierList.Should().NotBeNull();
            supplierList.Count.Should().BeGreaterOrEqualTo(1);
            supplierList.Count.Should().BeLessThan(fullSupplierListLength);

            supplierList = getSupplierDataProcedure.GetSuppliers("EPS", speciesCodes: new string[] { "ANG", "PLZ" });
            supplierList.Should().NotBeNull();
            supplierList.Count.Should().BeGreaterOrEqualTo(1);
            supplierList.Count.Should().BeLessThan(fullSupplierListLength);

            supplierList = getSupplierDataProcedure.GetSuppliers("EPS", speciesCodes: new string[] { "*" });
            supplierList.Should().NotBeNull();
            supplierList.Count.Should().BeGreaterOrEqualTo(1);
            supplierList.Count.Should().Be(fullSupplierListLength);
        }
    }
}

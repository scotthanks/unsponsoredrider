﻿using System;
using DataServiceLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using BusinessObjectServices;

namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class ReportedAvailabilityTableServiceTests
    {
        [TestMethod]
        public void DataServiceReportedAvailabilityGetByProductAndShipWeekTest()
        {
            var reportedAvailabilityTableService = new ReportedAvailabilityTableService();

            Guid productGuid = new Guid("506C35CE-CA8A-4963-92F4-0297ACE64158");
            Guid shipWeekGuid = new Guid(new LookupService().GetConfigValue("SHIPWEEK_GUID"));

            var reportedAvailability = reportedAvailabilityTableService.TryGetByProductAndShipWeek( productGuid, shipWeekGuid);

            reportedAvailability.Should().NotBeNull();
            reportedAvailability.ProductGuid.Should().Be(productGuid);
            reportedAvailability.ShipWeekGuid.Should().Be(shipWeekGuid);

            reportedAvailability = reportedAvailabilityTableService.TryGetByProductAndShipWeek(Guid.NewGuid(),
                                                                                               Guid.NewGuid());

            reportedAvailability.Should().BeNull();
        }
    }
}

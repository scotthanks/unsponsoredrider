﻿#define EPS
//#define BCH
using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataObjectLibrary;
using DataServiceLibrary;
using FluentAssertions;
using DataAccessLibrary;

namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class DataServiceUnitTests
    {

#if (EPS)
        [TestMethod]
        public void DataServiceProgramProductFormLazyLoadTest()
        {
            var dataService = new DataServiceLibrary.ProductFormTableService();

            var list = dataService.GetAll(1);
            var dataObject = list.FirstOrDefault();
            dataObject.Should().NotBeNull();
            dataObject.ProductFormCategoryIsLoaded.Should().BeFalse();
            dataObject.ProductFormCategoryIsLoaded.Should().BeFalse();
            var parentObject = dataObject.ProductFormCategory;
            parentObject.Should().NotBeNull();
            dataObject.ProductFormCategoryIsLoaded.Should().BeTrue();
            parentObject.ProductFormListIsLoaded.Should().BeFalse();
            parentObject.ProductFormListIsLoaded.Should().BeFalse();
            var parentsChildList = parentObject.ProductFormList;
            parentObject.ProductFormListIsLoaded.Should().BeTrue();
            parentsChildList.Should().NotBeNull();
            parentsChildList.Count.Should().BeGreaterThan(1);

            // Verify that caching is working. 
            bool dataObjectFoundInParentsChildList = false;
            foreach (var childObject in parentsChildList)
            {
                if (childObject.Guid == dataObject.Guid)
                {
                    dataObjectFoundInParentsChildList = true;
                    childObject.ProductFormCategory.Should().BeSameAs(parentObject);
                }
            }
            dataObjectFoundInParentsChildList.Should().BeTrue();
        }
#endif
    }
}

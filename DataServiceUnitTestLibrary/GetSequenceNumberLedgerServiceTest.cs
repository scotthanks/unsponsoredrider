﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataServiceLibrary;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class GetSequenceNumberLedgerServiceTest
    {
        [TestMethod]
        public void GetSquenceNumbers()
        {
            Task t1 = Task.Factory.StartNew(GetNumbers);
            Task t2 = Task.Factory.StartNew(GetNumbers);
            Task.WaitAll(t1, t2);

        }
        public void GetNumbers()
        {
            for(int i = 0; i<20; i++)
            {
                LedgerTransactionService ledgerService = new LedgerTransactionService(Guid.Empty, Guid.Empty, Guid.Empty, Guid.Empty);

                using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    SqlTransaction transaction;

                    // Start a local transaction.
                    transaction = connection.BeginTransaction("AccountTransaction");

                    command.Connection = connection;
                    command.Transaction = transaction;

                    long sequenceNumber = 0;
                    // Start a local transaction.
                    string seqNum = ledgerService.GetInternalIdNumber(command, "InvoiceNumber", "II-", ref sequenceNumber);
                    Console.WriteLine(seqNum);

                    transaction.Commit();
                }
            }
        }
    }
}

﻿using System;
using DataObjectLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataServiceLibrary;
using FluentAssertions;

namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class UpdateFieldTest
    {
        [TestMethod]
        public void DataServiceUpdateFieldTest()
        {
            var service = LookupTableService.SingletonInstance;

            var newLookupTableRow = service.UpdateField("TestItem8888", "Name", "WrittenValue");
            newLookupTableRow.Name.Should().Be("WrittenValue");
            newLookupTableRow = service.UpdateField("TestItem8888", "Name", "UpdatedValue");
            newLookupTableRow.Name.Should().Be("UpdatedValue");
        }
    }
}

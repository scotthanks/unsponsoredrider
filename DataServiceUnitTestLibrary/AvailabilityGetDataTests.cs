﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataObjectLibrary;
using DataServiceLibrary;
using BusinessObjectServices;
using FluentAssertions;
using BusinessObjectsLibrary;





namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class AvailabilityGetDataTests
    {
        [TestMethod]
        public void DataServiceGetAvailabilityDataTest()
        {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var service = new AvailabilityDataService(status);


            var availabilityData = service.GetAvailability
            (
                userCode: null,
                growerGuid: null,
                shipWeekCode: "201429", 
                weeksBefore: 1, 
                weeksAfter: 3,
                programTypeCode: "VA", 
                productFormCategoryCode: "RC", 
                supplierCodes: new string[] { "DUW","LIN","MLM","WEL" },
                varietyCodes: new string[] { "*" }, 
                geneticOwnerCodes: new string[] { "*" }, 
                speciesCodes: new string[] { "BAC" },
                productGuid: Guid.Empty,
                varietyMatchOptional: true,
                includePossibleSubstitutes: false
            );

            availabilityData.ReportedAvailabilityList.Should().NotBeNull();
            availabilityData.ReportedAvailabilityList.Count.Should().Be(245);

            System.Console.WriteLine("ReportedAvailability:");
            foreach (var reportedAvailabilityData in availabilityData.ReportedAvailabilityList)
            {
                System.Console.WriteLine(reportedAvailabilityData.ToString());
            }

            availabilityData.PreCartOrderLineList.Should().NotBeNull();
            availabilityData.PreCartOrderLineList.Count.Should().Be(0);

            System.Console.WriteLine("OrderLine:");
            foreach (var orderLine in availabilityData.PreCartOrderLineList)
            {
                System.Console.WriteLine(orderLine.ToString());
            }

            availabilityData.CartedOrderLineList.Should().NotBeNull();
            availabilityData.CartedOrderLineList.Count.Should().Be(0);

            System.Console.WriteLine("OrderLine:");
            foreach (var orderLine in availabilityData.CartedOrderLineList)
            {
                System.Console.WriteLine(orderLine.ToString());
            }

            availabilityData.ShipWeekList.Count.Should().Be(5);

         }
    }
}

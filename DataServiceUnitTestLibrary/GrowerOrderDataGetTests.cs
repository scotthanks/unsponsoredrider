﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataServiceLibrary;
using BusinessObjectServices;
using FluentAssertions;
using BusinessObjectsLibrary;

namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class GrowerOrderDataGetTests
    {
        [TestMethod]
        public void DataServiceGrowerOrderDataGetTest()
        {

            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var proc = new GrowerOrderDataService(status);

            var orderData = proc.GetGrowerOrderDetailData(new Guid(new LookupService().GetConfigValue("TEST_GROWERORDER_GUID")), includePriceData: true);

            int connectionCount = DataAccessLibrary.ConnectionServices.ConnectionCount;

            System.Console.WriteLine( "OrderNo: " + orderData.GrowerOrder.OrderNo);

            foreach (var supplierOrder in orderData.GrowerOrder.SupplierOrderList)
            {
                System.Console.WriteLine("\tSuppier: " + supplierOrder.Supplier.Name);

                foreach (var orderLine in supplierOrder.OrderLineList)
                {
                    System.Console.WriteLine("\t\tProduct: " + orderLine.Product.Variety.Name + " Qty: " + orderLine.QtyOrdered);
                }
            }

            connectionCount.Should().Be(connectionCount);
        }
       


    }
}

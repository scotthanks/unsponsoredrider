﻿#define EPS
//#define BCH

using System;
using DataObjectLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using System.Linq;
using DataServiceLibrary;

namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class PersonDataServiceUnitTests
    {
        [TestMethod]
        public void DataServicePersonListTest()
        {
            const int maxRowsToRead = 10;

            bool firstNameFound = false;
            bool lastNameFound = false;
            bool activeFound = false;
            bool inactiveFound = false;
            var dataService = new DataServiceLibrary.PersonTableService();
            var list = dataService.GetAll(maxRowsToRead);

            // ReSharper disable PossibleMultipleEnumeration

            list.Should().NotBeNull();

            list.Count().Should().BeGreaterThan(0);
            list.Count().Should().BeLessOrEqualTo(maxRowsToRead);
            list.FirstOrDefault().Should().NotBeNull();

            foreach (var person in list)
            {
                if (!string.IsNullOrEmpty(person.FirstName))
                    firstNameFound = true;

                if (!string.IsNullOrEmpty(person.LastName))
                    lastNameFound = true;


                if (person.DateDeactivated != null)
                {
                    person.DateDeactivated.Should().NotBe(DateTime.MinValue);
                    person.DateDeactivated.Should().NotBe(DateTime.MaxValue);
                    inactiveFound = true;
                }
                else
                {
                    activeFound = true;
                }
            }
            // ReSharper restore PossibleMultipleEnumeration

            firstNameFound.Should().BeTrue();
            lastNameFound.Should().BeTrue();
            activeFound.Should().BeTrue();
            
        }

        [TestMethod]
        public void DataServicePersonUpdateTest()
        {
            Guid testPersonGuid = new Guid("CA0F4E68-8F79-4CA0-AA9C-1CBA4DC5C7EF");

            var service = new DataServiceLibrary.PersonTableService();

            Person person;

            if (!service.Exists(testPersonGuid))
            {
                var lookupService = LookupTableService.SingletonInstance;
                Lookup personTypeLookup = lookupService.GetByPath("Code/PersonType/Other", excludeInactive: true);

                person = new Person()
                    {
                        Guid = testPersonGuid,
                        FirstName = "Test",
                        LastName = "Person",
                        IsActive = false,
                        PersonTypeLookupGuid = personTypeLookup.Guid
                    };

                service.Insert(person);

                if (service.GetAll().Count() == 1)
                {
                    person = new Person()
                    {
                        FirstName = "Rick",
                        LastName = "Harrison"
                    };

                    service.Insert(person);
                }

            }

            person = service.GetByGuid(testPersonGuid);

            person.FirstName.Should().Be("Test");
            person.LastName.Should().NotBeBlank();
            person.DateDeactivated.Should().HaveValue();
            person.IsActive.Should().BeFalse();

            person.IsActive = true;
            person.DateDeactivated.Should().NotHaveValue();

            person.IsActive = false;
            person.DateDeactivated.Should().HaveValue();

            person = service.GetByGuid(new Guid("CA0F4E68-8F79-4CA0-AA9C-1CBA4DC5C7EF"));

            person.LastName = "Person (changed)";

            var updatedPerson = service.Update(person);

            updatedPerson.LastName.Should().Be("Person (changed)");

            person.LastName = "Person";

            updatedPerson = service.Update(person);

            updatedPerson.LastName.Should().Be("Person");
        }


        [TestMethod]
        public void DataServicePersonInsertDeactivateActivateAndDeleteTest()
        {
            var lookupService = new DataServiceLibrary.LookupTableService();

            var service = new DataServiceLibrary.PersonTableService();

            var person = new DataObjectLibrary.Person
                {
                    FirstName = "Insert", 
                    LastName = "Test"
                };

            person.Guid.Should().NotBeEmpty();
            Guid personGuid = person.Guid;

            person.Id.Should().Be(default(long));

            Lookup personTypeLookup = lookupService.GetByPath("Code/PersonType/Other", excludeInactive: true);
            person.PersonTypeLookupGuid = personTypeLookup.Guid;

            // Insert it.
            person = service.Insert(person);
            person.Guid.Should().Be(personGuid);

            person.FirstName.Should().Be("Insert");
            person.LastName.Should().Be("Test");
            person.Id.Should().NotBe(default(long));
            person.Id.Should().BeGreaterThan(0);
            long personId = person.Id;
            person.IsActive.Should().BeTrue();
            person.DateDeactivated.Should().Be(null);
            person.DateDeactivated.Should().NotHaveValue();

            // Deactivate it.
            person = service.Deactivate(person);
            person.Guid.Should().Be(personGuid);
            person.IsActive.Should().BeFalse();
            person.DateDeactivated.Should().HaveValue();
            DateTime? dateDeactivated = person.DateDeactivated;

            // Deactivate it again (which should have no effect).
            person = service.Deactivate(person);
            person.DateDeactivated.Should().Be(dateDeactivated);

            // Activate it.
            person = service.Activate(person);
            person.Guid.Should().Be(personGuid);
            person.IsActive.Should().BeTrue();

            // Read it back from the cache.
            var personAgain = service.GetByGuid(personGuid);
            personAgain.Should().BeSameAs(person);

            // Read it back from the database.
            service.ClearCache();
            personAgain = service.GetByGuid(personGuid);
            personAgain.Should().NotBeSameAs(person);
            personAgain.Guid.Should().Be(person.Guid);
            personAgain.Id.Should().Be(person.Id);
            personAgain.FirstName.Should().Be(person.FirstName);
            personAgain.LastName.Should().Be(person.LastName);

            // Delete it.
            service.Delete(person);

            // Make sure it's gone.
            personAgain = service.TryGetByGuid(personGuid);
            personAgain.Should().Be(null);

            // Make sure that the database is actually doing something by seeing that it's giving out new id's.
            var anotherPerson = new DataObjectLibrary.Person {PersonTypeLookupGuid = personTypeLookup.Guid};
            anotherPerson = service.Insert(anotherPerson);
            anotherPerson.Id.Should().BeGreaterThan(personId);
            service.Delete(anotherPerson);
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataServiceLibrary;
using FluentAssertions;

namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class VarietyTableServiceTest
    {
        [TestMethod]
        public void DataServiceVarietyGetByGuidTest()
        {
            var service = new VarietyTableService();

            var variety = service.GetByGuid(new Guid("ED3CC7C1-32AC-4213-970E-4D8A709379BA"));

            variety.Code.Trim().Should().Be("S1056");
        }

        [TestMethod]
        public void DataServiceVarietyGetByCodeTest()
        {
            var service = new VarietyTableService();

            var variety = service.GetByCode("S1056", excludeInactive: true);

            variety.Guid.Should().Be(new Guid("ED3CC7C1-32AC-4213-970E-4D8A709379BA"));

            //TODO: Find out why uncommenting the following line causes a hang.
            //VarietyTableService.ClearCache();

            var variety2 = service.GetByCode("S1056", excludeInactive: true);

            // Prove that it came from the cache and didn't instantiate a new one.
            variety2.Should().BeSameAs(variety);
            variety2.Should().Be(variety);
            Assert.AreSame(variety, variety2);
        }
    }
}

﻿using System;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataServiceLibrary;
using DataObjectLibrary;
using FluentAssertions;

namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class LookupDataServiceTests
    {
        public void DataServiceLookupGetAll()
        {
            var lookupService = new LookupTableService();
            var stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            var lookuplist = lookupService.GetAll();
            foreach (var lookup in lookuplist)
            {
                System.Console.WriteLine(lookup.ToString());
            }
            stopwatch.Stop();
            System.Console.WriteLine("Elapsed:" + stopwatch.ElapsedMilliseconds);
        }

        [TestMethod]
        public void DataServiceLookupParentNullTest()
        {
            var lookup = LookupTableValues.Code.AvailabilityType.Avail;
            lookup.Code.Should().Be("AVAIL");
            lookup.ParentLookup.Code.Should().Be("AvailabilityType");
            lookup.ParentLookup.ParentLookup.Code.Should().Be("Code");
            lookup.ParentLookup.ParentLookup.ParentLookupGuid.Should().Be(null);

            //The following commented line throws an exception. Maybe that's OK.
            //lookup.ParentLookup.ParentLookup.ParentLookup.Should().BeNull();
        }
    }
}

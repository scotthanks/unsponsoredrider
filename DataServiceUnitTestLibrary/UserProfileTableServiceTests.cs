﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataServiceLibrary;
using FluentAssertions;

namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class UserProfileTableServiceTests
    {
        [TestMethod]
        public void UserProfileTableServiceTest()
        {
            var userProfileTableService = new UserProfileTableService();

            Guid growerGuid = Guid.NewGuid();
            const string authorizationProfileId = "TestID";

            userProfileTableService.GetGrowerAuthorizationProfileId(growerGuid).Should().BeNull();
            userProfileTableService.SaveGrowerAuthorizationProfile(growerGuid, authorizationProfileId);
            userProfileTableService.GetGrowerAuthorizationProfileId(growerGuid).Should().Be(authorizationProfileId);
            userProfileTableService.DeleteGrowerAuthorizationProfile(growerGuid);
            userProfileTableService.GetGrowerAuthorizationProfileId(growerGuid).Should().BeNull();
        }
    }
}

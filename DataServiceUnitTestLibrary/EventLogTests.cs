﻿using System;
using DataServiceLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using FluentAssertions;

namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class EventLogTests
    {
        [TestInitialize]
        public void Initialize()
        {
            LookupTableService.SingletonInstance.GetAllAndPreloadAllChildren();
        }

        [TestMethod]
        public void DataServiceEventLogAddLogTest()
        {
            var eventlogService = new EventLogTableService();

            var eventLog = eventlogService.Insert
                (userGuid: new Guid("7714B7E5-7148-404B-9E2E-93EE8B7E3B63"),
                    userCode: null,
                    processLookupCode: MethodBase.GetCurrentMethod().Name, 
                    comment: "Test Log.", logTypeLookup: LookupTableValues.Logging.LogType.Grower.CommentInternal, objectTypeLookup: LookupTableValues.Logging.ObjectType.Grower, objectGuid: new Guid("92CFACCB-5DF3-4832-8328-384B3BED6E8F"));

            System.Console.WriteLine( string.Format( "Id = {0}", eventLog.Id));
            eventLog.Id.Should().BeGreaterThan(1000);
            eventLog.Comment.ShouldBeEquivalentTo("Test Log.");
            eventLog.EventTime.Should().BeAfter(DateTime.Today);
           // eventLog.EventTime.Should().BeBefore(DateTime.Today.AddDays(1));
        }

        [TestMethod]
        public void DataServiceEventLogAddWithAllParmsLogTest()
        {
            var eventlogService = new EventLogTableService();

            var eventLog = eventlogService.Insert
                (
                    userGuid: new Guid("7714B7E5-7148-404B-9E2E-93EE8B7E3B63"),
                    userCode: null,
                    processLookupCode: MethodBase.GetCurrentMethod().Name,
                    comment: "Test Log.",
                    logTypeLookup: LookupTableValues.Logging.LogType.Grower.CommentInternal,
                    severityLookup: LookupTableValues.Logging.Severity.Performance,
                    priorityLookup: LookupTableValues.Logging.Priority.Medium,
                    verbosityLookup: LookupTableValues.Logging.Verbosity.Minimal,
                    objectTypeLookup: LookupTableValues.Logging.ObjectType.Grower,
                    objectGuid: new Guid("92CFACCB-5DF3-4832-8328-384B3BED6E8F"),
                    intValue: 88, processId: 9999, floatValue: (float?) 123.456, guidValue: Guid.NewGuid(), xmlData: "<test><one>1</one><two>2</two><three>3</three></test>");

            eventLog.Id.Should().BeGreaterThan(1000);
            eventLog.Comment.ShouldBeEquivalentTo("Test Log.");
            eventLog.EventTime.Should().BeAfter(DateTime.Today);
           // eventLog.EventTime.Should().BeBefore(DateTime.Today.AddDays(1));
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using BusinessObjectsLibrary;

namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class GrowerOrderSummaryByShipWeekGetDataProcedureTests
    {
        [TestMethod]
        public void DataServiceGrowerOrderSummaryByShipWeekGetDataProcedureTest()
        {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var growerOrderSummaryByShipWeekGetDataProcedure = new DataServiceLibrary.GrowerOrderSummaryDataService(status);

            var orderSummaryList =
                growerOrderSummaryByShipWeekGetDataProcedure.GetGrowerOrderSummaries(userCode: "scotth");

            foreach (var result in orderSummaryList)
            {
                if (result.GrowerOrderStatusLookupCode != "Cancelled" || result.GrowerOrderStatusLookupCode != "SupplierCancelled" || result.GrowerOrderStatusLookupCode != "GrowerCancelled")
                    {result.QtyOrderedCount.Should().BeGreaterThan(0);}
                else
                    {result.QtyOrderedCount.Should().Be(0);}

            }

            //TODO: Process results.
        }
    }
}

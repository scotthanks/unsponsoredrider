﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using BusinessObjectServices;
using BusinessObjectsLibrary;

namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class GrowerOrderProcessTransactionProcedureTests
    {
        [TestMethod]
        public void DataServiceGrowerOrderProcessTransactionProcedureTest()
        {
            var lookupService = new LookupService();
            var userGuid = new Guid(lookupService.GetConfigValue("SCOTT_USER_GUID"));
            var status = new StatusObject(userGuid, true);
            var service = new DataServiceLibrary.GrowerOrderDataService(status);

            service.UpdateCartQuantity(lookupService.GetConfigValue("USER_CODE"), "201420", new Guid("0905CE79-E69F-4A88-99E9-0448A091D617"), 3000);

            //TODO: Process results.
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataServiceLibrary;
using FluentAssertions;

namespace DataServiceUnitTestLibrary
{
    [TestClass]
    public class ContextUnitTests
    {
        //[TestMethod]
        //public void DataServiceContextTryGetTest()
        //{
        //    var context = new Context();
        //    var lookupTableService = context.TryGet<LookupTableService>();
        //    lookupTableService.Should().BeNull();

        //    lookupTableService = new LookupTableService();

        //    context.Add(lookupTableService);

        //    var returnedLookupTableService = context.TryGet<LookupTableService>();

        //    returnedLookupTableService.Should().BeSameAs(lookupTableService);
        //}

        [TestMethod]
        public void DataServiceContextGetTest()
        {
            var context = new Context();
            var lookupTableService = context.Get<LookupTableService>();
            lookupTableService.Should().NotBeNull();

            var lookup = lookupTableService.GetByGuid(new Guid("1B0671C8-5CB6-4DDA-844D-E7939D1F15FF"));
            lookup.Should().NotBeNull();
        }

        //[TestMethod]
        //public void DataServiceContextBasicTryGetTest()
        //{
        //    var context = new Context();

        //    var lookupTableService = new LookupTableService();

        //    var lookupTableService2 = new LookupTableService();
        //    lookupTableService2.Should().NotBeSameAs(lookupTableService);

        //    context.Add(lookupTableService);

        //    var personTableService = new PersonTableService();

        //    context.Add(personTableService);

        //    lookupTableService.Should().BeSameAs(lookupTableService);
        //    lookupTableService.Should().NotBeSameAs(personTableService);

        //    var returnedLookupTableService = context.TryGet<LookupTableService>();
        //    returnedLookupTableService.Should().NotBeNull();
        //    returnedLookupTableService.Should().BeSameAs(lookupTableService);
        //    returnedLookupTableService.Should().NotBeSameAs(personTableService);

        //    var returnedPersonTableService = context.TryGet<PersonTableService>();
        //    returnedPersonTableService.Should().NotBeNull();
        //    returnedPersonTableService.Should().BeSameAs(personTableService);
        //    returnedPersonTableService.Should().NotBeSameAs(lookupTableService);

        //    var lookup1 = lookupTableService.GetByGuid(new Guid("1B0671C8-5CB6-4DDA-844D-E7939D1F15FF"));
        //    var lookup2 = lookupTableService2.GetByGuid(new Guid("1B0671C8-5CB6-4DDA-844D-E7939D1F15FF"));

        //    lookup2.Should().NotBeSameAs(lookup1);

        //    var lookup3 = returnedLookupTableService.GetByGuid(new Guid("1B0671C8-5CB6-4DDA-844D-E7939D1F15FF"));

        //    lookup3.Should().BeSameAs(lookup1);
        //}


        [TestMethod]
        public void DataServiceContextBasicGetTest()
        {
            var context = new Context();

            var lookupTableService = context.Get<LookupTableService>();
            var lookupTableService2 = new LookupTableService();
            lookupTableService2.Should().NotBeSameAs(lookupTableService);

            var personTableService = context.Get<PersonTableService>();

            lookupTableService.Should().BeSameAs(lookupTableService);
            lookupTableService.Should().NotBeSameAs(personTableService);

            var returnedLookupTableService = context.Get<LookupTableService>();
            returnedLookupTableService.Should().NotBeNull();
            returnedLookupTableService.Should().BeSameAs(lookupTableService);
            returnedLookupTableService.Should().NotBeSameAs(personTableService);

            var returnedPersonTableService = context.Get<PersonTableService>();
            returnedPersonTableService.Should().NotBeNull();
            returnedPersonTableService.Should().BeSameAs(personTableService);
            returnedPersonTableService.Should().NotBeSameAs(lookupTableService);

            var lookup1 = lookupTableService.GetByGuid(new Guid("1B0671C8-5CB6-4DDA-844D-E7939D1F15FF"));
            var lookup2 = lookupTableService2.GetByGuid(new Guid("1B0671C8-5CB6-4DDA-844D-E7939D1F15FF"));

            lookup2.Should().NotBeSameAs(lookup1);

            var lookup3 = returnedLookupTableService.GetByGuid(new Guid("1B0671C8-5CB6-4DDA-844D-E7939D1F15FF"));

            lookup3.Should().BeSameAs(lookup1);
        }
    }
}

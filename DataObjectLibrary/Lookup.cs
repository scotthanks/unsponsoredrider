﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
    public partial class Lookup
    {
        
        
        // Because of the way code is generated, the ParentLookupList property is actually a list of the children who have this lookup as a parent.
        // To make the code more readable, we add this Children property.
        public List<Lookup> ChildLookupList
        {
            get { return ParentLookupList; }
        }

        public bool ChildLookupListIsLoaded
        {
            get { return ParentLookupIsLoaded; }
        }

        public void SetLazyChildLookupList(Lazy<List<Lookup>> lazyChildLookupList)
        {
            SetLazyParentLookupList(lazyChildLookupList);
        }

        public string GetInitializerCSharpCode(int tabs)
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.Append("new Lookup()");
            stringBuilder.AppendLine();

            InsertTabs(stringBuilder, tabs + 1);
            stringBuilder.Append("{");
            stringBuilder.AppendLine();

            InsertTabs(stringBuilder, tabs + 2);
            stringBuilder.AppendFormat("Id = {0}", this.Id);
            stringBuilder.Append(',');
            stringBuilder.AppendLine();

            InsertTabs(stringBuilder, tabs + 2);
            stringBuilder.AppendFormat("Guid = new Guid(\"{0}\")", this.Guid.ToString().ToUpper());
            stringBuilder.Append(',');
            stringBuilder.AppendLine();

            InsertTabs(stringBuilder, tabs + 2);
            stringBuilder.AppendFormat("ParentLookupGuid = new Guid(\"{0}\")", this.ParentLookupGuid.ToString().ToUpper());
            stringBuilder.Append(',');
            stringBuilder.AppendLine();
            
            InsertTabs(stringBuilder, tabs + 2);
            stringBuilder.AppendFormat("DateDeactivated = {0}", this.DateDeactivated == null ? "null" : this.DateDeactivated.ToString());
            stringBuilder.Append(',');
            stringBuilder.AppendLine();

            InsertTabs(stringBuilder, tabs + 2);
            stringBuilder.AppendFormat("Code = \"{0}\"", this.Code);
            stringBuilder.Append(',');
            stringBuilder.AppendLine();
            
            InsertTabs(stringBuilder, tabs + 2);
            stringBuilder.AppendFormat("Name = \"{0}\"", this.Name);
            stringBuilder.Append(',');
            stringBuilder.AppendLine();
            
            InsertTabs(stringBuilder, tabs + 2);
            stringBuilder.AppendFormat("SortSequence = {0}", this.SortSequence);
            stringBuilder.Append(',');
            stringBuilder.AppendLine();
            
            InsertTabs(stringBuilder, tabs + 2);
            stringBuilder.AppendFormat("Path = \"{0}\"", this.Path);
            stringBuilder.Append(',');
            stringBuilder.AppendLine();
            
            InsertTabs(stringBuilder, tabs + 2);
            stringBuilder.AppendFormat("SequenceChildren = {0}", this.SequenceChildren.ToString().ToLower());
            stringBuilder.AppendLine();

            InsertTabs(stringBuilder, tabs + 1);
            stringBuilder.Append("}");

            return stringBuilder.ToString();
        }

        private static void InsertTabs(StringBuilder stringBuilder, int count)
        {
            for (int i = 0; i < count; i++)
            {
                stringBuilder.Append("\t");
            }
        }

        public static string GetListInitializerCode(IEnumerable<Lookup> lookupList, int tabs)
        {
            var stringBuilder = new StringBuilder();

            InsertTabs(stringBuilder, tabs);
            stringBuilder.Append("var list = new List<Lookup>");
            stringBuilder.AppendLine();

            InsertTabs(stringBuilder, tabs + 1);
            stringBuilder.Append("{");

            string separator = "";

            foreach (var lookup in lookupList)
            {
                stringBuilder.Append(separator);
                stringBuilder.AppendLine();
                InsertTabs(stringBuilder, tabs + 2);
                stringBuilder.Append(lookup.GetInitializerCSharpCode(tabs + 2));

                separator = ",";
            }

            stringBuilder.AppendLine();

            InsertTabs(stringBuilder, tabs + 1);
            stringBuilder.Append("}");

            return stringBuilder.ToString();
        }

        public bool IsDescendantOf(Lookup lookup)
        {
            if (this.ParentLookupGuid == null || this.ParentLookupGuid == Guid.Empty)
            {
                return false;
            }

            if (this.ParentLookup.Guid == lookup.Guid)
            {
                return true;
            }

            return this.ParentLookup.IsDescendantOf(lookup);
        }

        public class Comparer : IComparer<Lookup>
        {
            //TODO: Improve this compare to sort based on the sort sequences.
            public int Compare(Lookup x, Lookup y)
            {
                return System.String.Compare(x.Path, y.Path, System.StringComparison.Ordinal);
            }
        }

        public string CalculatedPath
        {
            get
            {
                return ParentLookup.Path + "/" + Code;
            }
        }
    }
}

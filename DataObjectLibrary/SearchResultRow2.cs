﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
    public class SearchResultRow2
    {
        
        public Guid RowTypeTemplateGuid
        {
            get
            {
                Guid theGuid = new Guid();
                switch(this.ResultType)
                {
                    case ("Variety"):
                        //need to look this up from content DB......
                        theGuid = new Guid("08B041BC-65E9-4BD0-8F10-8B58ECE7CE27");
                        break;
                    case ("Series"):
                        //need to look this up from content DB......
                        theGuid = new Guid("A711B73B-D1EA-4B94-8F39-628C3EA24EB0");
                        break;
                    case ("Species"):
                        //need to look this up from content DB......
                        theGuid = new Guid("0A9C0A01-D485-4975-90B9-AEACA8AB8BBD");
                        break;
                    case ("Supplier"):
                        //need to look this up from content DB......
                        theGuid = new Guid("D163CAF0-897A-4804-B311-CF801B859B22");
                        break;
                   case ("GeneticOwner"):
                        //need to look this up from content DB......
                        theGuid = new Guid("17D327AD-4305-4B6A-AD95-98991286AEA4");
                        break;
                    case ("WebPage"):
                        //need to look this up from content DB......
                        theGuid = new Guid("3FD97A27-CC88-4958-8180-7B32B5B988B4");
                        break;
                        
                    default:
                        theGuid = new Guid("00000000-0000-0000-0000-000000000000");
                        break;
               
                }
                return theGuid;
            }
           
        }

        public string ResultType { get; set; }
        public string ResultCode { get; set; }
        public string ResultName { get; set; }
        public int SortOrder { get; set; }
        public int PageNumber { get; set; }
        public string ImageURI { get; set; }
        public string HeaderText { get; set; }
        public string SubHeaderText { get; set; }
        public string CatalogProgramTypeCode { get; set; }
        public string CatalogProductFormCategoryCode { get; set; }
        public string Program1Name { get; set; }
        public string Program1ProgramTypeCode { get; set; }
        public string Program1ProductFormCategoryCode { get; set; }
        public int Program1ProductCount { get; set; }
        public string Program2Name { get; set; }
        public string Program2ProgramTypeCode { get; set; }
        public string Program2ProductFormCategoryCode { get; set; }
        public int Program2ProductCount { get; set; }
        public string Program3Name { get; set; }
        public string Program3ProgramTypeCode { get; set; }
        public string Program3ProductFormCategoryCode { get; set; }
        public int Program3ProductCount { get; set; }
        public string Species1Code { get; set; }
        public string Species1Name { get; set; }
        public string Species1ProgramTypeCode { get; set; }
        public string Species1ProductFormCategoryCode { get; set; }
        public string Species2Code { get; set; }
        public string Species2Name { get; set; }
        public string Species2ProgramTypeCode { get; set; }
        public string Species2ProductFormCategoryCode { get; set; }
        public string Species3Code { get; set; }
        public string Species3Name { get; set; }
        public string Species3ProgramTypeCode { get; set; }
        public string Species3ProductFormCategoryCode { get; set; }
        public string Program1AvailabilityWeek { get; set; }
        public string Program2AvailabilityWeek { get; set; }
        public string Program3AvailabilityWeek { get; set; }

    }
}

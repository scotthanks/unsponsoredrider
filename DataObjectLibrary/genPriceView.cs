﻿using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
    public partial class PriceView : DataObjectBase
    {
        public enum ColumnEnum
        {
            RegularCost,
            EODCost,
            RegularMUPercent,
            EODMUPercent,
            PriceGroupCode,
            PriceGroupName,
            TagCost,
            VolumeLevelCode,
            VolumeLevelName,
            VolumeLow,
            VolumeHigh,
            ProgramSeasonCode,
            ProgramSeasonName,
            SeasonStartDate,
            SeasonEndDate,
            EODDate,
            ProgramCode,
            ProgramTypeCode,
            SupplierCode,
            SupplierName,
            ProductFormCategoryCode,
            PriceId,
            PriceGuid,
            PriceGroupId,
            PriceGroupGuid,
            VolumeLevelId,
            VolumeLevelGuid,
            ProgramSeasonId,
            ProgramSeasonGuid,
            ProgramGuid,
            ProgramTypeGuid,
            DefaultPeakWeekNumber,
            DefaultSeasonEndWeekNumber,
            SupplierGuid,
            ProductFormCategoryGuid,
        }

        new public enum ForeignKeyFieldEnum
        {
            PriceGuid,
            PriceGroupGuid,
            VolumeLevelGuid,
            ProgramSeasonGuid,
            ProgramGuid,
            ProgramTypeGuid,
            SupplierGuid,
            ProductFormCategoryGuid,
        }

        private static List<Type> _foreignKeyTableTypeList;

        public static List<Type> ForeignKeyTableTypeList
        {
            get
            {
                if (_foreignKeyTableTypeList == null)
                {
                    _foreignKeyTableTypeList = new List<Type>
                    {
                        typeof(Price),
                        typeof(PriceGroup),
                        typeof(VolumeLevel),
                        typeof(ProgramSeason),
                        typeof(Program),
                        typeof(ProgramType),
                        typeof(Supplier),
                        typeof(ProductFormCategory),
                    };
                }

                return _foreignKeyTableTypeList;
            }
        }

        public Decimal RegularCost { get; set; }
        public Decimal EODCost { get; set; }
        public Double RegularMUPercent { get; set; }
        public Double EODMUPercent { get; set; }
        public string PriceGroupCode { get; set; }
        public string PriceGroupName { get; set; }
        public Decimal TagCost { get; set; }
        public string VolumeLevelCode { get; set; }
        public string VolumeLevelName { get; set; }
        public int VolumeLow { get; set; }
        public int VolumeHigh { get; set; }
        public string ProgramSeasonCode { get; set; }
        public string ProgramSeasonName { get; set; }
        public DateTime SeasonStartDate { get; set; }
        public DateTime SeasonEndDate { get; set; }
        public DateTime EODDate { get; set; }
        public string ProgramCode { get; set; }
        public string ProgramTypeCode { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string ProductFormCategoryCode { get; set; }
        public long PriceId { get; set; }
        public Guid PriceGuid { get; set; }
        public long PriceGroupId { get; set; }
        public Guid PriceGroupGuid { get; set; }
        public long VolumeLevelId { get; set; }
        public Guid VolumeLevelGuid { get; set; }
        public long ProgramSeasonId { get; set; }
        public Guid ProgramSeasonGuid { get; set; }
        public Guid ProgramGuid { get; set; }
        public Guid ProgramTypeGuid { get; set; }
        public int? DefaultPeakWeekNumber { get; set; }
        public int? DefaultSeasonEndWeekNumber { get; set; }
        public Guid SupplierGuid { get; set; }
        public Guid ProductFormCategoryGuid { get; set; }

        public override string GetFieldValueAsString(string fieldName)
        {
            string value = null;
            switch (fieldName)
            {
                case "RegularCost":
                    value = DecimalToString(RegularCost);
                    break;
                case "EODCost":
                    value = DecimalToString(EODCost);
                    break;
                case "RegularMUPercent":
                    value = DoubleToString(RegularMUPercent);
                    break;
                case "EODMUPercent":
                    value = DoubleToString(EODMUPercent);
                    break;
                case "PriceGroupCode":
                    value = StringToString(PriceGroupCode);
                    break;
                case "PriceGroupName":
                    value = StringToString(PriceGroupName);
                    break;
                case "TagCost":
                    value = DecimalToString(TagCost);
                    break;
                case "VolumeLevelCode":
                    value = StringToString(VolumeLevelCode);
                    break;
                case "VolumeLevelName":
                    value = StringToString(VolumeLevelName);
                    break;
                case "VolumeLow":
                    value = IntToString(VolumeLow);
                    break;
                case "VolumeHigh":
                    value = IntToString(VolumeHigh);
                    break;
                case "ProgramSeasonCode":
                    value = StringToString(ProgramSeasonCode);
                    break;
                case "ProgramSeasonName":
                    value = StringToString(ProgramSeasonName);
                    break;
                case "SeasonStartDate":
                    value = DateTimeToString(SeasonStartDate);
                    break;
                case "SeasonEndDate":
                    value = DateTimeToString(SeasonEndDate);
                    break;
                case "EODDate":
                    value = DateTimeToString(EODDate);
                    break;
                case "ProgramCode":
                    value = StringToString(ProgramCode);
                    break;
                case "ProgramTypeCode":
                    value = StringToString(ProgramTypeCode);
                    break;
                case "SupplierCode":
                    value = StringToString(SupplierCode);
                    break;
                case "SupplierName":
                    value = StringToString(SupplierName);
                    break;
                case "ProductFormCategoryCode":
                    value = StringToString(ProductFormCategoryCode);
                    break;
                case "PriceId":
                    value = LongToString(PriceId);
                    break;
                case "PriceGuid":
                    value = GuidToString(PriceGuid);
                    break;
                case "PriceGroupId":
                    value = LongToString(PriceGroupId);
                    break;
                case "PriceGroupGuid":
                    value = GuidToString(PriceGroupGuid);
                    break;
                case "VolumeLevelId":
                    value = LongToString(VolumeLevelId);
                    break;
                case "VolumeLevelGuid":
                    value = GuidToString(VolumeLevelGuid);
                    break;
                case "ProgramSeasonId":
                    value = LongToString(ProgramSeasonId);
                    break;
                case "ProgramSeasonGuid":
                    value = GuidToString(ProgramSeasonGuid);
                    break;
                case "ProgramGuid":
                    value = GuidToString(ProgramGuid);
                    break;
                case "ProgramTypeGuid":
                    value = GuidToString(ProgramTypeGuid);
                    break;
                case "DefaultPeakWeekNumber":
                    value = IntToString(DefaultPeakWeekNumber);
                    break;
                case "DefaultSeasonEndWeekNumber":
                    value = IntToString(DefaultSeasonEndWeekNumber);
                    break;
                case "SupplierGuid":
                    value = GuidToString(SupplierGuid);
                    break;
                case "ProductFormCategoryGuid":
                    value = GuidToString(ProductFormCategoryGuid);
                    break;
            }
            return value;
        }

        public override bool ParseFieldFromString(string fieldName, string value)
        {
            bool valueSet = true;

            switch (fieldName)
            {
                case "RegularCost":
                    RegularCost = (Decimal)DecimalFromString(value, isNullable: false);
                    break;
                case "EODCost":
                    EODCost = (Decimal)DecimalFromString(value, isNullable: false);
                    break;
                case "RegularMUPercent":
                    RegularMUPercent = (Double)DoubleFromString(value, isNullable: false);
                    break;
                case "EODMUPercent":
                    EODMUPercent = (Double)DoubleFromString(value, isNullable: false);
                    break;
                case "PriceGroupCode":
                    PriceGroupCode = (string)StringFromString(value, isNullable: false);
                    break;
                case "PriceGroupName":
                    PriceGroupName = (string)StringFromString(value, isNullable: false);
                    break;
                case "TagCost":
                    TagCost = (Decimal)DecimalFromString(value, isNullable: false);
                    break;
                case "VolumeLevelCode":
                    VolumeLevelCode = (string)StringFromString(value, isNullable: false);
                    break;
                case "VolumeLevelName":
                    VolumeLevelName = (string)StringFromString(value, isNullable: false);
                    break;
                case "VolumeLow":
                    VolumeLow = (int)IntFromString(value, isNullable: false);
                    break;
                case "VolumeHigh":
                    VolumeHigh = (int)IntFromString(value, isNullable: false);
                    break;
                case "ProgramSeasonCode":
                    ProgramSeasonCode = (string)StringFromString(value, isNullable: false);
                    break;
                case "ProgramSeasonName":
                    ProgramSeasonName = (string)StringFromString(value, isNullable: false);
                    break;
                case "SeasonStartDate":
                    SeasonStartDate = (DateTime)DateTimeFromString(value, isNullable: false);
                    break;
                case "SeasonEndDate":
                    SeasonEndDate = (DateTime)DateTimeFromString(value, isNullable: false);
                    break;
                case "EODDate":
                    EODDate = (DateTime)DateTimeFromString(value, isNullable: false);
                    break;
                case "ProgramCode":
                    ProgramCode = (string)StringFromString(value, isNullable: false);
                    break;
                case "ProgramTypeCode":
                    ProgramTypeCode = (string)StringFromString(value, isNullable: false);
                    break;
                case "SupplierCode":
                    SupplierCode = (string)StringFromString(value, isNullable: false);
                    break;
                case "SupplierName":
                    SupplierName = (string)StringFromString(value, isNullable: false);
                    break;
                case "ProductFormCategoryCode":
                    ProductFormCategoryCode = (string)StringFromString(value, isNullable: false);
                    break;
                case "PriceId":
                    PriceId = (long)LongFromString(value, isNullable: false);
                    break;
                case "PriceGuid":
                    PriceGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "PriceGroupId":
                    PriceGroupId = (long)LongFromString(value, isNullable: false);
                    break;
                case "PriceGroupGuid":
                    PriceGroupGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "VolumeLevelId":
                    VolumeLevelId = (long)LongFromString(value, isNullable: false);
                    break;
                case "VolumeLevelGuid":
                    VolumeLevelGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ProgramSeasonId":
                    ProgramSeasonId = (long)LongFromString(value, isNullable: false);
                    break;
                case "ProgramSeasonGuid":
                    ProgramSeasonGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ProgramGuid":
                    ProgramGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ProgramTypeGuid":
                    ProgramTypeGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "DefaultPeakWeekNumber":
                    DefaultPeakWeekNumber = IntFromString(value, isNullable: true);
                    break;
                case "DefaultSeasonEndWeekNumber":
                    DefaultSeasonEndWeekNumber = IntFromString(value, isNullable: true);
                    break;
                case "SupplierGuid":
                    SupplierGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ProductFormCategoryGuid":
                    ProductFormCategoryGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                default:
                    valueSet = false;
                    break;
            }

            return valueSet;
        }

        public override object this[string fieldName]
        {
            get
            {
                object value = null;
                switch (fieldName)
                {
                    case "RegularCost":
                        value = RegularCost;
                        break;
                    case "EODCost":
                        value = EODCost;
                        break;
                    case "RegularMUPercent":
                        value = RegularMUPercent;
                        break;
                    case "EODMUPercent":
                        value = EODMUPercent;
                        break;
                    case "PriceGroupCode":
                        value = PriceGroupCode;
                        break;
                    case "PriceGroupName":
                        value = PriceGroupName;
                        break;
                    case "TagCost":
                        value = TagCost;
                        break;
                    case "VolumeLevelCode":
                        value = VolumeLevelCode;
                        break;
                    case "VolumeLevelName":
                        value = VolumeLevelName;
                        break;
                    case "VolumeLow":
                        value = VolumeLow;
                        break;
                    case "VolumeHigh":
                        value = VolumeHigh;
                        break;
                    case "ProgramSeasonCode":
                        value = ProgramSeasonCode;
                        break;
                    case "ProgramSeasonName":
                        value = ProgramSeasonName;
                        break;
                    case "SeasonStartDate":
                        value = SeasonStartDate;
                        break;
                    case "SeasonEndDate":
                        value = SeasonEndDate;
                        break;
                    case "EODDate":
                        value = EODDate;
                        break;
                    case "ProgramCode":
                        value = ProgramCode;
                        break;
                    case "ProgramTypeCode":
                        value = ProgramTypeCode;
                        break;
                    case "SupplierCode":
                        value = SupplierCode;
                        break;
                    case "SupplierName":
                        value = SupplierName;
                        break;
                    case "ProductFormCategoryCode":
                        value = ProductFormCategoryCode;
                        break;
                    case "PriceId":
                        value = PriceId;
                        break;
                    case "PriceGuid":
                        value = PriceGuid;
                        break;
                    case "PriceGroupId":
                        value = PriceGroupId;
                        break;
                    case "PriceGroupGuid":
                        value = PriceGroupGuid;
                        break;
                    case "VolumeLevelId":
                        value = VolumeLevelId;
                        break;
                    case "VolumeLevelGuid":
                        value = VolumeLevelGuid;
                        break;
                    case "ProgramSeasonId":
                        value = ProgramSeasonId;
                        break;
                    case "ProgramSeasonGuid":
                        value = ProgramSeasonGuid;
                        break;
                    case "ProgramGuid":
                        value = ProgramGuid;
                        break;
                    case "ProgramTypeGuid":
                        value = ProgramTypeGuid;
                        break;
                    case "DefaultPeakWeekNumber":
                        value = DefaultPeakWeekNumber;
                        break;
                    case "DefaultSeasonEndWeekNumber":
                        value = DefaultSeasonEndWeekNumber;
                        break;
                    case "SupplierGuid":
                        value = SupplierGuid;
                        break;
                    case "ProductFormCategoryGuid":
                        value = ProductFormCategoryGuid;
                        break;
                }
                return value;
            }
            set
            {
                switch (fieldName)
                {
                    case "RegularCost":
                        RegularCost = (Decimal)value;
                        break;
                    case "EODCost":
                        EODCost = (Decimal)value;
                        break;
                    case "RegularMUPercent":
                        RegularMUPercent = (Double)value;
                        break;
                    case "EODMUPercent":
                        EODMUPercent = (Double)value;
                        break;
                    case "PriceGroupCode":
                        PriceGroupCode = (string)value;
                        break;
                    case "PriceGroupName":
                        PriceGroupName = (string)value;
                        break;
                    case "TagCost":
                        TagCost = (Decimal)value;
                        break;
                    case "VolumeLevelCode":
                        VolumeLevelCode = (string)value;
                        break;
                    case "VolumeLevelName":
                        VolumeLevelName = (string)value;
                        break;
                    case "VolumeLow":
                        VolumeLow = (int)value;
                        break;
                    case "VolumeHigh":
                        VolumeHigh = (int)value;
                        break;
                    case "ProgramSeasonCode":
                        ProgramSeasonCode = (string)value;
                        break;
                    case "ProgramSeasonName":
                        ProgramSeasonName = (string)value;
                        break;
                    case "SeasonStartDate":
                        SeasonStartDate = (DateTime)value;
                        break;
                    case "SeasonEndDate":
                        SeasonEndDate = (DateTime)value;
                        break;
                    case "EODDate":
                        EODDate = (DateTime)value;
                        break;
                    case "ProgramCode":
                        ProgramCode = (string)value;
                        break;
                    case "ProgramTypeCode":
                        ProgramTypeCode = (string)value;
                        break;
                    case "SupplierCode":
                        SupplierCode = (string)value;
                        break;
                    case "SupplierName":
                        SupplierName = (string)value;
                        break;
                    case "ProductFormCategoryCode":
                        ProductFormCategoryCode = (string)value;
                        break;
                    case "PriceId":
                        PriceId = (long)value;
                        break;
                    case "PriceGuid":
                        PriceGuid = (Guid)value;
                        break;
                    case "PriceGroupId":
                        PriceGroupId = (long)value;
                        break;
                    case "PriceGroupGuid":
                        PriceGroupGuid = (Guid)value;
                        break;
                    case "VolumeLevelId":
                        VolumeLevelId = (long)value;
                        break;
                    case "VolumeLevelGuid":
                        VolumeLevelGuid = (Guid)value;
                        break;
                    case "ProgramSeasonId":
                        ProgramSeasonId = (long)value;
                        break;
                    case "ProgramSeasonGuid":
                        ProgramSeasonGuid = (Guid)value;
                        break;
                    case "ProgramGuid":
                        ProgramGuid = (Guid)value;
                        break;
                    case "ProgramTypeGuid":
                        ProgramTypeGuid = (Guid)value;
                        break;
                    case "DefaultPeakWeekNumber":
                        DefaultPeakWeekNumber = (int?)value;
                        break;
                    case "DefaultSeasonEndWeekNumber":
                        DefaultSeasonEndWeekNumber = (int?)value;
                        break;
                    case "SupplierGuid":
                        SupplierGuid = (Guid)value;
                        break;
                    case "ProductFormCategoryGuid":
                        ProductFormCategoryGuid = (Guid)value;
                        break;
                }
            }
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("RegularCost={0}", this.RegularCost);
            result.Append(", ");
            result.AppendFormat("EODCost={0}", this.EODCost);
            result.Append(", ");
            result.AppendFormat("RegularMUPercent={0}", this.RegularMUPercent);
            result.Append(", ");
            result.AppendFormat("EODMUPercent={0}", this.EODMUPercent);
            result.Append(", ");
            result.AppendFormat("PriceGroupCode={0}", this.PriceGroupCode);
            result.Append(", ");
            result.AppendFormat("PriceGroupName={0}", this.PriceGroupName);
            result.Append(", ");
            result.AppendFormat("TagCost={0}", this.TagCost);
            result.Append(", ");
            result.AppendFormat("VolumeLevelCode={0}", this.VolumeLevelCode);
            result.Append(", ");
            result.AppendFormat("VolumeLevelName={0}", this.VolumeLevelName);
            result.Append(", ");
            result.AppendFormat("VolumeLow={0}", this.VolumeLow);
            result.Append(", ");
            result.AppendFormat("VolumeHigh={0}", this.VolumeHigh);
            result.Append(", ");
            result.AppendFormat("ProgramSeasonCode={0}", this.ProgramSeasonCode);
            result.Append(", ");
            result.AppendFormat("ProgramSeasonName={0}", this.ProgramSeasonName);
            result.Append(", ");
            result.AppendFormat("SeasonStartDate={0}", this.SeasonStartDate);
            result.Append(", ");
            result.AppendFormat("SeasonEndDate={0}", this.SeasonEndDate);
            result.Append(", ");
            result.AppendFormat("EODDate={0}", this.EODDate);
            result.Append(", ");
            result.AppendFormat("ProgramCode={0}", this.ProgramCode);
            result.Append(", ");
            result.AppendFormat("ProgramTypeCode={0}", this.ProgramTypeCode);
            result.Append(", ");
            result.AppendFormat("SupplierCode={0}", this.SupplierCode);
            result.Append(", ");
            result.AppendFormat("SupplierName={0}", this.SupplierName);
            result.Append(", ");
            result.AppendFormat("ProductFormCategoryCode={0}", this.ProductFormCategoryCode);
            result.Append(", ");
            result.AppendFormat("PriceId={0}", this.PriceId);
            result.Append(", ");
            result.AppendFormat("PriceGuid={0}", this.PriceGuid);
            result.Append(", ");
            result.AppendFormat("PriceGroupId={0}", this.PriceGroupId);
            result.Append(", ");
            result.AppendFormat("PriceGroupGuid={0}", this.PriceGroupGuid);
            result.Append(", ");
            result.AppendFormat("VolumeLevelId={0}", this.VolumeLevelId);
            result.Append(", ");
            result.AppendFormat("VolumeLevelGuid={0}", this.VolumeLevelGuid);
            result.Append(", ");
            result.AppendFormat("ProgramSeasonId={0}", this.ProgramSeasonId);
            result.Append(", ");
            result.AppendFormat("ProgramSeasonGuid={0}", this.ProgramSeasonGuid);
            result.Append(", ");
            result.AppendFormat("ProgramGuid={0}", this.ProgramGuid);
            result.Append(", ");
            result.AppendFormat("ProgramTypeGuid={0}", this.ProgramTypeGuid);
            result.Append(", ");
            result.AppendFormat("DefaultPeakWeekNumber={0}", this.DefaultPeakWeekNumber);
            result.Append(", ");
            result.AppendFormat("DefaultSeasonEndWeekNumber={0}", this.DefaultSeasonEndWeekNumber);
            result.Append(", ");
            result.AppendFormat("SupplierGuid={0}", this.SupplierGuid);
            result.Append(", ");
            result.AppendFormat("ProductFormCategoryGuid={0}", this.ProductFormCategoryGuid);

            return result.ToString();
        }

        #region Lazy Loading Logic

        #region Price Lazy Loading Properties and Methods

        private Lazy<Price> _lazyPrice;

        public Price Price
        {
            get
            {
                return _lazyPrice == null ? null : _lazyPrice.Value;
            }
        }

        public bool PriceIsLoaded
        {
            get
            {
                return _lazyPrice == null ? false : _lazyPrice.IsValueCreated;
            }
        }

        public void SetLazyPrice(Lazy<Price> lazyPrice)
        {
            _lazyPrice = lazyPrice;
        }

        #endregion

        #region PriceGroup Lazy Loading Properties and Methods

        private Lazy<PriceGroup> _lazyPriceGroup;

        public PriceGroup PriceGroup
        {
            get
            {
                return _lazyPriceGroup == null ? null : _lazyPriceGroup.Value;
            }
        }

        public bool PriceGroupIsLoaded
        {
            get
            {
                return _lazyPriceGroup == null ? false : _lazyPriceGroup.IsValueCreated;
            }
        }

        public void SetLazyPriceGroup(Lazy<PriceGroup> lazyPriceGroup)
        {
            _lazyPriceGroup = lazyPriceGroup;
        }

        #endregion

        #region VolumeLevel Lazy Loading Properties and Methods

        private Lazy<VolumeLevel> _lazyVolumeLevel;

        public VolumeLevel VolumeLevel
        {
            get
            {
                return _lazyVolumeLevel == null ? null : _lazyVolumeLevel.Value;
            }
        }

        public bool VolumeLevelIsLoaded
        {
            get
            {
                return _lazyVolumeLevel == null ? false : _lazyVolumeLevel.IsValueCreated;
            }
        }

        public void SetLazyVolumeLevel(Lazy<VolumeLevel> lazyVolumeLevel)
        {
            _lazyVolumeLevel = lazyVolumeLevel;
        }

        #endregion

        #region ProgramSeason Lazy Loading Properties and Methods

        private Lazy<ProgramSeason> _lazyProgramSeason;

        public ProgramSeason ProgramSeason
        {
            get
            {
                return _lazyProgramSeason == null ? null : _lazyProgramSeason.Value;
            }
        }

        public bool ProgramSeasonIsLoaded
        {
            get
            {
                return _lazyProgramSeason == null ? false : _lazyProgramSeason.IsValueCreated;
            }
        }

        public void SetLazyProgramSeason(Lazy<ProgramSeason> lazyProgramSeason)
        {
            _lazyProgramSeason = lazyProgramSeason;
        }

        #endregion

        #region Program Lazy Loading Properties and Methods

        private Lazy<Program> _lazyProgram;

        public Program Program
        {
            get
            {
                return _lazyProgram == null ? null : _lazyProgram.Value;
            }
        }

        public bool ProgramIsLoaded
        {
            get
            {
                return _lazyProgram == null ? false : _lazyProgram.IsValueCreated;
            }
        }

        public void SetLazyProgram(Lazy<Program> lazyProgram)
        {
            _lazyProgram = lazyProgram;
        }

        #endregion

        #region ProgramType Lazy Loading Properties and Methods

        private Lazy<ProgramType> _lazyProgramType;

        public ProgramType ProgramType
        {
            get
            {
                return _lazyProgramType == null ? null : _lazyProgramType.Value;
            }
        }

        public bool ProgramTypeIsLoaded
        {
            get
            {
                return _lazyProgramType == null ? false : _lazyProgramType.IsValueCreated;
            }
        }

        public void SetLazyProgramType(Lazy<ProgramType> lazyProgramType)
        {
            _lazyProgramType = lazyProgramType;
        }

        #endregion

        #region Supplier Lazy Loading Properties and Methods

        private Lazy<Supplier> _lazySupplier;

        public Supplier Supplier
        {
            get
            {
                return _lazySupplier == null ? null : _lazySupplier.Value;
            }
        }

        public bool SupplierIsLoaded
        {
            get
            {
                return _lazySupplier == null ? false : _lazySupplier.IsValueCreated;
            }
        }

        public void SetLazySupplier(Lazy<Supplier> lazySupplier)
        {
            _lazySupplier = lazySupplier;
        }

        #endregion

        #region ProductFormCategory Lazy Loading Properties and Methods

        private Lazy<ProductFormCategory> _lazyProductFormCategory;

        public ProductFormCategory ProductFormCategory
        {
            get
            {
                return _lazyProductFormCategory == null ? null : _lazyProductFormCategory.Value;
            }
        }

        public bool ProductFormCategoryIsLoaded
        {
            get
            {
                return _lazyProductFormCategory == null ? false : _lazyProductFormCategory.IsValueCreated;
            }
        }

        public void SetLazyProductFormCategory(Lazy<ProductFormCategory> lazyProductFormCategory)
        {
            _lazyProductFormCategory = lazyProductFormCategory;
        }

        #endregion

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
    public partial class ShipWeek
    {
        new public string Code
        {
            get
            {
                return string.Format("{0:0000}{1:00}", this.Year, this.Week);
            }

            set
            {
                throw new ApplicationException("The ShipWeek code cannot be set.");
            }
        }
    }
}

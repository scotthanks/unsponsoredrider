using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class Variety : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			Code,
			SpeciesGuid,
			GeneticOwnerGuid,
			Name,
			ActiveDate,
			DeactiveDate,
			ColorDescription,
			ImageSetGuid,
			ColorLookupGuid,
			HabitLookupGuid,
			VigorLookupGuid,
			TimingLookupGuid,
			SeriesGuid,
			DescriptionHTMLGUID,
			ResearchStatus,
			CultureLibraryLink,
			LastUpdate,
			NameStripped,
			SpeciesAndNameStripped,
			ZoneLookupGuid,
			BrandLookupGuid,
		}

		new public enum ForeignKeyFieldEnum
		{
			SpeciesGuid,
			GeneticOwnerGuid,
			ImageSetGuid,
			ColorLookupGuid,
			HabitLookupGuid,
			VigorLookupGuid,
			TimingLookupGuid,
			SeriesGuid,
			ZoneLookupGuid,
			BrandLookupGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Species),
						typeof(GeneticOwner),
						typeof(ImageSet),
						typeof(Lookup),
						typeof(Lookup),
						typeof(Lookup),
						typeof(Lookup),
						typeof(Series),
						typeof(Lookup),
						typeof(Lookup),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public Guid SpeciesGuid { get; set; }
		public Guid GeneticOwnerGuid { get; set; }
		public string Name { get; set; }
		public DateTime ActiveDate { get; set; }
		public DateTime? DeactiveDate { get; set; }
		public string ColorDescription { get; set; }
		public Guid? ImageSetGuid { get; set; }
		public Guid? ColorLookupGuid { get; set; }
		public Guid? HabitLookupGuid { get; set; }
		public Guid? VigorLookupGuid { get; set; }
		public Guid TimingLookupGuid { get; set; }
		public Guid? SeriesGuid { get; set; }
		public Guid? DescriptionHTMLGUID { get; set; }
		public string ResearchStatus { get; set; }
		public string CultureLibraryLink { get; set; }
		public DateTime LastUpdate { get; set; }
		public string NameStripped { get; set; }
		public string SpeciesAndNameStripped { get; set; }
		public Guid? ZoneLookupGuid { get; set; }
		public Guid? BrandLookupGuid { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "SpeciesGuid":
					value = GuidToString( SpeciesGuid);
					break;
				case "GeneticOwnerGuid":
					value = GuidToString( GeneticOwnerGuid);
					break;
				case "Name":
					value = StringToString( Name);
					break;
				case "ActiveDate":
					value = DateTimeToString( ActiveDate);
					break;
				case "DeactiveDate":
					value = DateTimeToString( DeactiveDate);
					break;
				case "ColorDescription":
					value = StringToString( ColorDescription);
					break;
				case "ImageSetGuid":
					value = GuidToString( ImageSetGuid);
					break;
				case "ColorLookupGuid":
					value = GuidToString( ColorLookupGuid);
					break;
				case "HabitLookupGuid":
					value = GuidToString( HabitLookupGuid);
					break;
				case "VigorLookupGuid":
					value = GuidToString( VigorLookupGuid);
					break;
				case "TimingLookupGuid":
					value = GuidToString( TimingLookupGuid);
					break;
				case "SeriesGuid":
					value = GuidToString( SeriesGuid);
					break;
				case "DescriptionHTMLGUID":
					value = GuidToString( DescriptionHTMLGUID);
					break;
				case "ResearchStatus":
					value = StringToString( ResearchStatus);
					break;
				case "CultureLibraryLink":
					value = StringToString( CultureLibraryLink);
					break;
				case "LastUpdate":
					value = DateTimeToString( LastUpdate);
					break;
				case "NameStripped":
					value = StringToString( NameStripped);
					break;
				case "SpeciesAndNameStripped":
					value = StringToString( SpeciesAndNameStripped);
					break;
				case "ZoneLookupGuid":
					value = GuidToString( ZoneLookupGuid);
					break;
				case "BrandLookupGuid":
					value = GuidToString( BrandLookupGuid);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "SpeciesGuid":
					SpeciesGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "GeneticOwnerGuid":
					GeneticOwnerGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				case "ActiveDate":
					ActiveDate = (DateTime)DateTimeFromString( value, isNullable: false);
					break;
				case "DeactiveDate":
					DeactiveDate = DateTimeFromString( value, isNullable: true);
					break;
				case "ColorDescription":
					ColorDescription = (string)StringFromString( value, isNullable: false);
					break;
				case "ImageSetGuid":
					ImageSetGuid = GuidFromString( value, isNullable: true);
					break;
				case "ColorLookupGuid":
					ColorLookupGuid = GuidFromString( value, isNullable: true);
					break;
				case "HabitLookupGuid":
					HabitLookupGuid = GuidFromString( value, isNullable: true);
					break;
				case "VigorLookupGuid":
					VigorLookupGuid = GuidFromString( value, isNullable: true);
					break;
				case "TimingLookupGuid":
					TimingLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "SeriesGuid":
					SeriesGuid = GuidFromString( value, isNullable: true);
					break;
				case "DescriptionHTMLGUID":
					DescriptionHTMLGUID = GuidFromString( value, isNullable: true);
					break;
				case "ResearchStatus":
					ResearchStatus = StringFromString( value, isNullable: true);
					break;
				case "CultureLibraryLink":
					CultureLibraryLink = (string)StringFromString( value, isNullable: false);
					break;
				case "LastUpdate":
					LastUpdate = (DateTime)DateTimeFromString( value, isNullable: false);
					break;
				case "NameStripped":
					NameStripped = (string)StringFromString( value, isNullable: false);
					break;
				case "SpeciesAndNameStripped":
					SpeciesAndNameStripped = StringFromString( value, isNullable: true);
					break;
				case "ZoneLookupGuid":
					ZoneLookupGuid = GuidFromString( value, isNullable: true);
					break;
				case "BrandLookupGuid":
					BrandLookupGuid = GuidFromString( value, isNullable: true);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "Code":
						value = Code;
						break;
					case "SpeciesGuid":
						value = SpeciesGuid;
						break;
					case "GeneticOwnerGuid":
						value = GeneticOwnerGuid;
						break;
					case "Name":
						value = Name;
						break;
					case "ActiveDate":
						value = ActiveDate;
						break;
					case "DeactiveDate":
						value = DeactiveDate;
						break;
					case "ColorDescription":
						value = ColorDescription;
						break;
					case "ImageSetGuid":
						value = ImageSetGuid;
						break;
					case "ColorLookupGuid":
						value = ColorLookupGuid;
						break;
					case "HabitLookupGuid":
						value = HabitLookupGuid;
						break;
					case "VigorLookupGuid":
						value = VigorLookupGuid;
						break;
					case "TimingLookupGuid":
						value = TimingLookupGuid;
						break;
					case "SeriesGuid":
						value = SeriesGuid;
						break;
					case "DescriptionHTMLGUID":
						value = DescriptionHTMLGUID;
						break;
					case "ResearchStatus":
						value = ResearchStatus;
						break;
					case "CultureLibraryLink":
						value = CultureLibraryLink;
						break;
					case "LastUpdate":
						value = LastUpdate;
						break;
					case "NameStripped":
						value = NameStripped;
						break;
					case "SpeciesAndNameStripped":
						value = SpeciesAndNameStripped;
						break;
					case "ZoneLookupGuid":
						value = ZoneLookupGuid;
						break;
					case "BrandLookupGuid":
						value = BrandLookupGuid;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "SpeciesGuid":
						SpeciesGuid = (Guid)value;
						break;
					case "GeneticOwnerGuid":
						GeneticOwnerGuid = (Guid)value;
						break;
					case "Name":
						Name = (string)value;
						break;
					case "ActiveDate":
						ActiveDate = (DateTime)value;
						break;
					case "DeactiveDate":
						DeactiveDate = (DateTime?)value;
						break;
					case "ColorDescription":
						ColorDescription = (string)value;
						break;
					case "ImageSetGuid":
						ImageSetGuid = (Guid?)value;
						break;
					case "ColorLookupGuid":
						ColorLookupGuid = (Guid?)value;
						break;
					case "HabitLookupGuid":
						HabitLookupGuid = (Guid?)value;
						break;
					case "VigorLookupGuid":
						VigorLookupGuid = (Guid?)value;
						break;
					case "TimingLookupGuid":
						TimingLookupGuid = (Guid)value;
						break;
					case "SeriesGuid":
						SeriesGuid = (Guid?)value;
						break;
					case "DescriptionHTMLGUID":
						DescriptionHTMLGUID = (Guid?)value;
						break;
					case "ResearchStatus":
						ResearchStatus = (string)value;
						break;
					case "CultureLibraryLink":
						CultureLibraryLink = (string)value;
						break;
					case "LastUpdate":
						LastUpdate = (DateTime)value;
						break;
					case "NameStripped":
						NameStripped = (string)value;
						break;
					case "SpeciesAndNameStripped":
						SpeciesAndNameStripped = (string)value;
						break;
					case "ZoneLookupGuid":
						ZoneLookupGuid = (Guid?)value;
						break;
					case "BrandLookupGuid":
						BrandLookupGuid = (Guid?)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region Species Lazy Loading Properties and Methods

        private Lazy<Species> _lazySpecies;

        public Species Species
        {
            get
            {
                return _lazySpecies == null ? null : _lazySpecies.Value;
            }
            set
            {
                _lazySpecies = new Lazy<Species>(() => value);
            }
        }

        public bool SpeciesIsLoaded
        {
            get
            {
                return _lazySpecies == null ? false : _lazySpecies.IsValueCreated;
            }
        }

        public void SetLazySpecies(Lazy<Species> lazySpecies)
        {
            _lazySpecies = lazySpecies;
        }

		#endregion

		#region GeneticOwner Lazy Loading Properties and Methods

        private Lazy<GeneticOwner> _lazyGeneticOwner;

        public GeneticOwner GeneticOwner
        {
            get
            {
                return _lazyGeneticOwner == null ? null : _lazyGeneticOwner.Value;
            }
            set
            {
                _lazyGeneticOwner = new Lazy<GeneticOwner>(() => value);
            }
        }

        public bool GeneticOwnerIsLoaded
        {
            get
            {
                return _lazyGeneticOwner == null ? false : _lazyGeneticOwner.IsValueCreated;
            }
        }

        public void SetLazyGeneticOwner(Lazy<GeneticOwner> lazyGeneticOwner)
        {
            _lazyGeneticOwner = lazyGeneticOwner;
        }

		#endregion

		#region ImageSet Lazy Loading Properties and Methods

        private Lazy<ImageSet> _lazyImageSet;

        public ImageSet ImageSet
        {
            get
            {
                return _lazyImageSet == null ? null : _lazyImageSet.Value;
            }
            set
            {
                _lazyImageSet = new Lazy<ImageSet>(() => value);
            }
        }

        public bool ImageSetIsLoaded
        {
            get
            {
                return _lazyImageSet == null ? false : _lazyImageSet.IsValueCreated;
            }
        }

        public void SetLazyImageSet(Lazy<ImageSet> lazyImageSet)
        {
            _lazyImageSet = lazyImageSet;
        }

		#endregion

		#region ColorLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyColorLookup;

        public Lookup ColorLookup
        {
            get
            {
                return _lazyColorLookup == null ? null : _lazyColorLookup.Value;
            }
            set
            {
                _lazyColorLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool ColorLookupIsLoaded
        {
            get
            {
                return _lazyColorLookup == null ? false : _lazyColorLookup.IsValueCreated;
            }
        }

        public void SetLazyColorLookup(Lazy<Lookup> lazyColorLookup)
        {
            _lazyColorLookup = lazyColorLookup;
        }

		#endregion

		#region HabitLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyHabitLookup;

        public Lookup HabitLookup
        {
            get
            {
                return _lazyHabitLookup == null ? null : _lazyHabitLookup.Value;
            }
            set
            {
                _lazyHabitLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool HabitLookupIsLoaded
        {
            get
            {
                return _lazyHabitLookup == null ? false : _lazyHabitLookup.IsValueCreated;
            }
        }

        public void SetLazyHabitLookup(Lazy<Lookup> lazyHabitLookup)
        {
            _lazyHabitLookup = lazyHabitLookup;
        }

		#endregion

		#region VigorLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyVigorLookup;

        public Lookup VigorLookup
        {
            get
            {
                return _lazyVigorLookup == null ? null : _lazyVigorLookup.Value;
            }
            set
            {
                _lazyVigorLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool VigorLookupIsLoaded
        {
            get
            {
                return _lazyVigorLookup == null ? false : _lazyVigorLookup.IsValueCreated;
            }
        }

        public void SetLazyVigorLookup(Lazy<Lookup> lazyVigorLookup)
        {
            _lazyVigorLookup = lazyVigorLookup;
        }

		#endregion

		#region TimingLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyTimingLookup;

        public Lookup TimingLookup
        {
            get
            {
                return _lazyTimingLookup == null ? null : _lazyTimingLookup.Value;
            }
            set
            {
                _lazyTimingLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool TimingLookupIsLoaded
        {
            get
            {
                return _lazyTimingLookup == null ? false : _lazyTimingLookup.IsValueCreated;
            }
        }

        public void SetLazyTimingLookup(Lazy<Lookup> lazyTimingLookup)
        {
            _lazyTimingLookup = lazyTimingLookup;
        }

		#endregion

		#region Series Lazy Loading Properties and Methods

        private Lazy<Series> _lazySeries;

        public Series Series
        {
            get
            {
                return _lazySeries == null ? null : _lazySeries.Value;
            }
            set
            {
                _lazySeries = new Lazy<Series>(() => value);
            }
        }

        public bool SeriesIsLoaded
        {
            get
            {
                return _lazySeries == null ? false : _lazySeries.IsValueCreated;
            }
        }

        public void SetLazySeries(Lazy<Series> lazySeries)
        {
            _lazySeries = lazySeries;
        }

		#endregion

		#region ZoneLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyZoneLookup;

        public Lookup ZoneLookup
        {
            get
            {
                return _lazyZoneLookup == null ? null : _lazyZoneLookup.Value;
            }
            set
            {
                _lazyZoneLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool ZoneLookupIsLoaded
        {
            get
            {
                return _lazyZoneLookup == null ? false : _lazyZoneLookup.IsValueCreated;
            }
        }

        public void SetLazyZoneLookup(Lazy<Lookup> lazyZoneLookup)
        {
            _lazyZoneLookup = lazyZoneLookup;
        }

		#endregion

		#region BrandLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyBrandLookup;

        public Lookup BrandLookup
        {
            get
            {
                return _lazyBrandLookup == null ? null : _lazyBrandLookup.Value;
            }
            set
            {
                _lazyBrandLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool BrandLookupIsLoaded
        {
            get
            {
                return _lazyBrandLookup == null ? false : _lazyBrandLookup.IsValueCreated;
            }
        }

        public void SetLazyBrandLookup(Lazy<Lookup> lazyBrandLookup)
        {
            _lazyBrandLookup = lazyBrandLookup;
        }

		#endregion

		#region Product Lazy Loading Properties and Methods

        private Lazy<List<Product>> _lazyProductList;

        public List<Product> ProductList
        {
            get
            {
                return _lazyProductList == null ? null : _lazyProductList.Value;
            }
        }

        public bool ProductListIsLoaded
        {
            get
            {
                return _lazyProductList == null ? false : _lazyProductList.IsValueCreated;
            }
        }

        public void SetLazyProductList(Lazy<List<Product>> lazyProductList)
        {
            _lazyProductList = lazyProductList;
        }

		#endregion

        //#region SearchVarietyHelper Lazy Loading Properties and Methods

        //private Lazy<List<SearchVarietyHelper>> _lazySearchVarietyHelperList;

        //public List<SearchVarietyHelper> SearchVarietyHelperList
        //{
        //    get
        //    {
        //        return _lazySearchVarietyHelperList == null ? null : _lazySearchVarietyHelperList.Value;
        //    }
        //}

        //public bool SearchVarietyHelperListIsLoaded
        //{
        //    get
        //    {
        //        return _lazySearchVarietyHelperList == null ? false : _lazySearchVarietyHelperList.IsValueCreated;
        //    }
        //}

        //public void SetLazySearchVarietyHelperList(Lazy<List<SearchVarietyHelper>> lazySearchVarietyHelperList)
        //{
        //    _lazySearchVarietyHelperList = lazySearchVarietyHelperList;
        //}

        //#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "SpeciesGuid={0}",this.SpeciesGuid);
			result.Append(", ");
			result.AppendFormat( "GeneticOwnerGuid={0}",this.GeneticOwnerGuid);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);
			result.Append(", ");
			result.AppendFormat( "ActiveDate={0}",this.ActiveDate);
			result.Append(", ");
			result.AppendFormat( "DeactiveDate={0}",this.DeactiveDate);
			result.Append(", ");
			result.AppendFormat( "ColorDescription={0}",this.ColorDescription);
			result.Append(", ");
			result.AppendFormat( "ImageSetGuid={0}",this.ImageSetGuid);
			result.Append(", ");
			result.AppendFormat( "ColorLookupGuid={0}",this.ColorLookupGuid);
			result.Append(", ");
			result.AppendFormat( "HabitLookupGuid={0}",this.HabitLookupGuid);
			result.Append(", ");
			result.AppendFormat( "VigorLookupGuid={0}",this.VigorLookupGuid);
			result.Append(", ");
			result.AppendFormat( "TimingLookupGuid={0}",this.TimingLookupGuid);
			result.Append(", ");
			result.AppendFormat( "SeriesGuid={0}",this.SeriesGuid);
			result.Append(", ");
			result.AppendFormat( "DescriptionHTMLGUID={0}",this.DescriptionHTMLGUID);
			result.Append(", ");
			result.AppendFormat( "ResearchStatus={0}",this.ResearchStatus);
			result.Append(", ");
			result.AppendFormat( "CultureLibraryLink={0}",this.CultureLibraryLink);
			result.Append(", ");
			result.AppendFormat( "LastUpdate={0}",this.LastUpdate);
			result.Append(", ");
			result.AppendFormat( "NameStripped={0}",this.NameStripped);
			result.Append(", ");
			result.AppendFormat( "SpeciesAndNameStripped={0}",this.SpeciesAndNameStripped);
			result.Append(", ");
			result.AppendFormat( "ZoneLookupGuid={0}",this.ZoneLookupGuid);
			result.Append(", ");
			result.AppendFormat( "BrandLookupGuid={0}",this.BrandLookupGuid);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}

using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class ProductFormCategory : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			Code,
			Name,
			IsRooted,
			AvailabilityWeekCalcMethodLookupGuid,
		}

		new public enum ForeignKeyFieldEnum
		{
			AvailabilityWeekCalcMethodLookupGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Lookup),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public string Name { get; set; }
		public bool IsRooted { get; set; }
		public Guid AvailabilityWeekCalcMethodLookupGuid { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "Name":
					value = StringToString( Name);
					break;
				case "IsRooted":
					value = BoolToString( IsRooted);
					break;
				case "AvailabilityWeekCalcMethodLookupGuid":
					value = GuidToString( AvailabilityWeekCalcMethodLookupGuid);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				case "IsRooted":
					IsRooted = (bool)BoolFromString( value, isNullable: false);
					break;
				case "AvailabilityWeekCalcMethodLookupGuid":
					AvailabilityWeekCalcMethodLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "Code":
						value = Code;
						break;
					case "Name":
						value = Name;
						break;
					case "IsRooted":
						value = IsRooted;
						break;
					case "AvailabilityWeekCalcMethodLookupGuid":
						value = AvailabilityWeekCalcMethodLookupGuid;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "Name":
						Name = (string)value;
						break;
					case "IsRooted":
						IsRooted = (bool)value;
						break;
					case "AvailabilityWeekCalcMethodLookupGuid":
						AvailabilityWeekCalcMethodLookupGuid = (Guid)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region AvailabilityWeekCalcMethodLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyAvailabilityWeekCalcMethodLookup;

        public Lookup AvailabilityWeekCalcMethodLookup
        {
            get
            {
                return _lazyAvailabilityWeekCalcMethodLookup == null ? null : _lazyAvailabilityWeekCalcMethodLookup.Value;
            }
            set
            {
                _lazyAvailabilityWeekCalcMethodLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool AvailabilityWeekCalcMethodLookupIsLoaded
        {
            get
            {
                return _lazyAvailabilityWeekCalcMethodLookup == null ? false : _lazyAvailabilityWeekCalcMethodLookup.IsValueCreated;
            }
        }

        public void SetLazyAvailabilityWeekCalcMethodLookup(Lazy<Lookup> lazyAvailabilityWeekCalcMethodLookup)
        {
            _lazyAvailabilityWeekCalcMethodLookup = lazyAvailabilityWeekCalcMethodLookup;
        }

		#endregion

		#region GrowerOrder Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrder>> _lazyGrowerOrderList;

        public List<GrowerOrder> GrowerOrderList
        {
            get
            {
                return _lazyGrowerOrderList == null ? null : _lazyGrowerOrderList.Value;
            }
        }

        public bool GrowerOrderListIsLoaded
        {
            get
            {
                return _lazyGrowerOrderList == null ? false : _lazyGrowerOrderList.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrderList(Lazy<List<GrowerOrder>> lazyGrowerOrderList)
        {
            _lazyGrowerOrderList = lazyGrowerOrderList;
        }

		#endregion

		#region ProductForm Lazy Loading Properties and Methods

        private Lazy<List<ProductForm>> _lazyProductFormList;

        public List<ProductForm> ProductFormList
        {
            get
            {
                return _lazyProductFormList == null ? null : _lazyProductFormList.Value;
            }
        }

        public bool ProductFormListIsLoaded
        {
            get
            {
                return _lazyProductFormList == null ? false : _lazyProductFormList.IsValueCreated;
            }
        }

        public void SetLazyProductFormList(Lazy<List<ProductForm>> lazyProductFormList)
        {
            _lazyProductFormList = lazyProductFormList;
        }

		#endregion

		#region Program Lazy Loading Properties and Methods

        private Lazy<List<Program>> _lazyProgramList;

        public List<Program> ProgramList
        {
            get
            {
                return _lazyProgramList == null ? null : _lazyProgramList.Value;
            }
        }

        public bool ProgramListIsLoaded
        {
            get
            {
                return _lazyProgramList == null ? false : _lazyProgramList.IsValueCreated;
            }
        }

        public void SetLazyProgramList(Lazy<List<Program>> lazyProgramList)
        {
            _lazyProgramList = lazyProgramList;
        }

		#endregion

        //#region PromoCode Lazy Loading Properties and Methods

        //private Lazy<List<PromoCode>> _lazyPromoCodeList;

        //public List<PromoCode> PromoCodeList
        //{
        //    get
        //    {
        //        return _lazyPromoCodeList == null ? null : _lazyPromoCodeList.Value;
        //    }
        //}

        //public bool PromoCodeListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyPromoCodeList == null ? false : _lazyPromoCodeList.IsValueCreated;
        //    }
        //}

        //public void SetLazyPromoCodeList(Lazy<List<PromoCode>> lazyPromoCodeList)
        //{
        //    _lazyPromoCodeList = lazyPromoCodeList;
        //}

        //#endregion

        //#region SearchVarietyHelper Lazy Loading Properties and Methods

        //private Lazy<List<SearchVarietyHelper>> _lazySearchVarietyHelperList;

        //public List<SearchVarietyHelper> SearchVarietyHelperList
        //{
        //    get
        //    {
        //        return _lazySearchVarietyHelperList == null ? null : _lazySearchVarietyHelperList.Value;
        //    }
        //}

        //public bool SearchVarietyHelperListIsLoaded
        //{
        //    get
        //    {
        //        return _lazySearchVarietyHelperList == null ? false : _lazySearchVarietyHelperList.IsValueCreated;
        //    }
        //}

        //public void SetLazySearchVarietyHelperList(Lazy<List<SearchVarietyHelper>> lazySearchVarietyHelperList)
        //{
        //    _lazySearchVarietyHelperList = lazySearchVarietyHelperList;
        //}

        //#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);
			result.Append(", ");
			result.AppendFormat( "IsRooted={0}",this.IsRooted);
			result.Append(", ");
			result.AppendFormat( "AvailabilityWeekCalcMethodLookupGuid={0}",this.AvailabilityWeekCalcMethodLookupGuid);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}

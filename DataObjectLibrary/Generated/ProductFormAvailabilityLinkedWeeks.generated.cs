using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
    public partial class ProductFormAvailabilityLinkedWeeks : TableObjectBase
    {
        public enum ColumnEnum
        {
            Id,
            Guid,
            DateDeactivated,
            GroupingCode,
            ProductFormGuid,
            ShipWeekGuid,
        }

        new public enum ForeignKeyFieldEnum
        {
            ProductFormGuid,
            ShipWeekGuid,
        }

        private static List<Type> _foreignKeyTableTypeList;

        public static List<Type> ForeignKeyTableTypeList
        {
            get
            {
                if (_foreignKeyTableTypeList == null)
                {
                    _foreignKeyTableTypeList = new List<Type>
                    {
                        typeof(ProductForm),
                        typeof(ShipWeek),
                    };
                }

                return _foreignKeyTableTypeList;
            }
        }

        public int GroupingCode { get; set; }
        public Guid ProductFormGuid { get; set; }
        public Guid ShipWeekGuid { get; set; }

        public override string GetFieldValueAsString(string fieldName)
        {
            string value = null;
            switch (fieldName)
            {
                case "Id":
                    value = LongToString(Id);
                    break;
                case "Guid":
                    value = GuidToString(Guid);
                    break;
                case "DateDeactivated":
                    value = DateTimeToString(DateDeactivated);
                    break;
                case "GroupingCode":
                    value = IntToString(GroupingCode);
                    break;
                case "ProductFormGuid":
                    value = GuidToString(ProductFormGuid);
                    break;
                case "ShipWeekGuid":
                    value = GuidToString(ShipWeekGuid);
                    break;
            }
            return value;
        }

        public override bool ParseFieldFromString(string fieldName, string value)
        {
            bool valueSet = true;

            switch (fieldName)
            {
                case "Id":
                    Id = (long)LongFromString(value, isNullable: false);
                    break;
                case "Guid":
                    Guid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "DateDeactivated":
                    DateDeactivated = DateTimeFromString(value, isNullable: true);
                    break;
                case "GroupingCode":
                    GroupingCode = (int)IntFromString(value, isNullable: false);
                    break;
                case "ProductFormGuid":
                    ProductFormGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ShipWeekGuid":
                    ShipWeekGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                default:
                    valueSet = false;
                    break;
            }

            return valueSet;
        }

        public override object this[string fieldName]
        {
            get
            {
                object value = null;
                switch (fieldName)
                {
                    case "Id":
                        value = Id;
                        break;
                    case "Guid":
                        value = Guid;
                        break;
                    case "DateDeactivated":
                        value = DateDeactivated;
                        break;
                    case "GroupingCode":
                        value = GroupingCode;
                        break;
                    case "ProductFormGuid":
                        value = ProductFormGuid;
                        break;
                    case "ShipWeekGuid":
                        value = ShipWeekGuid;
                        break;
                }
                return value;
            }
            set
            {
                switch (fieldName)
                {
                    case "Id":
                        Id = (long)value;
                        break;
                    case "Guid":
                        Guid = (Guid)value;
                        break;
                    case "DateDeactivated":
                        DateDeactivated = (DateTime?)value;
                        break;
                    case "GroupingCode":
                        GroupingCode = (int)value;
                        break;
                    case "ProductFormGuid":
                        ProductFormGuid = (Guid)value;
                        break;
                    case "ShipWeekGuid":
                        ShipWeekGuid = (Guid)value;
                        break;
                }
            }
        }

        #region Lazy Loading Logic

        #region ProductForm Lazy Loading Properties and Methods

        private Lazy<ProductForm> _lazyProductForm;

        public ProductForm ProductForm
        {
            get
            {
                return _lazyProductForm == null ? null : _lazyProductForm.Value;
            }
            set
            {
                _lazyProductForm = new Lazy<ProductForm>(() => value);
            }
        }

        public bool ProductFormIsLoaded
        {
            get
            {
                return _lazyProductForm == null ? false : _lazyProductForm.IsValueCreated;
            }
        }

        public void SetLazyProductForm(Lazy<ProductForm> lazyProductForm)
        {
            _lazyProductForm = lazyProductForm;
        }

        #endregion

        #region ShipWeek Lazy Loading Properties and Methods

        private Lazy<ShipWeek> _lazyShipWeek;

        public ShipWeek ShipWeek
        {
            get
            {
                return _lazyShipWeek == null ? null : _lazyShipWeek.Value;
            }
            set
            {
                _lazyShipWeek = new Lazy<ShipWeek>(() => value);
            }
        }

        public bool ShipWeekIsLoaded
        {
            get
            {
                return _lazyShipWeek == null ? false : _lazyShipWeek.IsValueCreated;
            }
        }

        public void SetLazyShipWeek(Lazy<ShipWeek> lazyShipWeek)
        {
            _lazyShipWeek = lazyShipWeek;
        }

        #endregion

        #endregion

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("Id={0}", this.Id);
            result.Append(", ");
            result.AppendFormat("Guid={0}", this.Guid);
            result.Append(", ");
            result.AppendFormat("DateDeactivated={0}", this.DateDeactivated);
            result.Append(", ");
            result.AppendFormat("GroupingCode={0}", this.GroupingCode);
            result.Append(", ");
            result.AppendFormat("ProductFormGuid={0}", this.ProductFormGuid);
            result.Append(", ");
            result.AppendFormat("ShipWeekGuid={0}", this.ShipWeekGuid);

            return result.ToString();
        }

        new protected static bool GetHasIdColumn() { return true; }
        new protected static bool GetHasGuidColumn() { return true; }
        new protected static bool GetHasDateDeactivatedColumn() { return true; }
        new protected static bool GetHasCodeColumn() { return false; }
        new protected static bool GetHasNameColumn() { return false; }
        new protected static bool GetDateDeactivatedIsNullable() { return true; }
    }
}

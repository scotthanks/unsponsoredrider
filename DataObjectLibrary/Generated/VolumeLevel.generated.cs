using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class VolumeLevel : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			Name,
			Code,
			VolumeLow,
			VolumeHigh,
			ProgramSeasonGuid,
			LevelNumber,
			VolumeLevelTypeLookupGuid,
		}

		new public enum ForeignKeyFieldEnum
		{
			ProgramSeasonGuid,
			VolumeLevelTypeLookupGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(ProgramSeason),
						typeof(Lookup),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public string Name { get; set; }
		public int VolumeLow { get; set; }
		public int VolumeHigh { get; set; }
		public Guid ProgramSeasonGuid { get; set; }
		public int LevelNumber { get; set; }
		public Guid VolumeLevelTypeLookupGuid { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "Name":
					value = StringToString( Name);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "VolumeLow":
					value = IntToString( VolumeLow);
					break;
				case "VolumeHigh":
					value = IntToString( VolumeHigh);
					break;
				case "ProgramSeasonGuid":
					value = GuidToString( ProgramSeasonGuid);
					break;
				case "LevelNumber":
					value = IntToString( LevelNumber);
					break;
				case "VolumeLevelTypeLookupGuid":
					value = GuidToString( VolumeLevelTypeLookupGuid);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "VolumeLow":
					VolumeLow = (int)IntFromString( value, isNullable: false);
					break;
				case "VolumeHigh":
					VolumeHigh = (int)IntFromString( value, isNullable: false);
					break;
				case "ProgramSeasonGuid":
					ProgramSeasonGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "LevelNumber":
					LevelNumber = (int)IntFromString( value, isNullable: false);
					break;
				case "VolumeLevelTypeLookupGuid":
					VolumeLevelTypeLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "Name":
						value = Name;
						break;
					case "Code":
						value = Code;
						break;
					case "VolumeLow":
						value = VolumeLow;
						break;
					case "VolumeHigh":
						value = VolumeHigh;
						break;
					case "ProgramSeasonGuid":
						value = ProgramSeasonGuid;
						break;
					case "LevelNumber":
						value = LevelNumber;
						break;
					case "VolumeLevelTypeLookupGuid":
						value = VolumeLevelTypeLookupGuid;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "Name":
						Name = (string)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "VolumeLow":
						VolumeLow = (int)value;
						break;
					case "VolumeHigh":
						VolumeHigh = (int)value;
						break;
					case "ProgramSeasonGuid":
						ProgramSeasonGuid = (Guid)value;
						break;
					case "LevelNumber":
						LevelNumber = (int)value;
						break;
					case "VolumeLevelTypeLookupGuid":
						VolumeLevelTypeLookupGuid = (Guid)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region ProgramSeason Lazy Loading Properties and Methods

        private Lazy<ProgramSeason> _lazyProgramSeason;

        public ProgramSeason ProgramSeason
        {
            get
            {
                return _lazyProgramSeason == null ? null : _lazyProgramSeason.Value;
            }
            set
            {
                _lazyProgramSeason = new Lazy<ProgramSeason>(() => value);
            }
        }

        public bool ProgramSeasonIsLoaded
        {
            get
            {
                return _lazyProgramSeason == null ? false : _lazyProgramSeason.IsValueCreated;
            }
        }

        public void SetLazyProgramSeason(Lazy<ProgramSeason> lazyProgramSeason)
        {
            _lazyProgramSeason = lazyProgramSeason;
        }

		#endregion

		#region VolumeLevelTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyVolumeLevelTypeLookup;

        public Lookup VolumeLevelTypeLookup
        {
            get
            {
                return _lazyVolumeLevelTypeLookup == null ? null : _lazyVolumeLevelTypeLookup.Value;
            }
            set
            {
                _lazyVolumeLevelTypeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool VolumeLevelTypeLookupIsLoaded
        {
            get
            {
                return _lazyVolumeLevelTypeLookup == null ? false : _lazyVolumeLevelTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyVolumeLevelTypeLookup(Lazy<Lookup> lazyVolumeLevelTypeLookup)
        {
            _lazyVolumeLevelTypeLookup = lazyVolumeLevelTypeLookup;
        }

		#endregion

		#region ActualGrowerVolume Lazy Loading Properties and Methods

        private Lazy<List<GrowerVolume>> _lazyActualGrowerVolumeList;

        public List<GrowerVolume> ActualGrowerVolumeList
        {
            get
            {
                return _lazyActualGrowerVolumeList == null ? null : _lazyActualGrowerVolumeList.Value;
            }
        }

        public bool ActualGrowerVolumeListIsLoaded
        {
            get
            {
                return _lazyActualGrowerVolumeList == null ? false : _lazyActualGrowerVolumeList.IsValueCreated;
            }
        }

        public void SetLazyActualGrowerVolumeList(Lazy<List<GrowerVolume>> lazyActualGrowerVolumeList)
        {
            _lazyActualGrowerVolumeList = lazyActualGrowerVolumeList;
        }

		#endregion

		#region EstimatedGrowerVolume Lazy Loading Properties and Methods

        private Lazy<List<GrowerVolume>> _lazyEstimatedGrowerVolumeList;

        public List<GrowerVolume> EstimatedGrowerVolumeList
        {
            get
            {
                return _lazyEstimatedGrowerVolumeList == null ? null : _lazyEstimatedGrowerVolumeList.Value;
            }
        }

        public bool EstimatedGrowerVolumeListIsLoaded
        {
            get
            {
                return _lazyEstimatedGrowerVolumeList == null ? false : _lazyEstimatedGrowerVolumeList.IsValueCreated;
            }
        }

        public void SetLazyEstimatedGrowerVolumeList(Lazy<List<GrowerVolume>> lazyEstimatedGrowerVolumeList)
        {
            _lazyEstimatedGrowerVolumeList = lazyEstimatedGrowerVolumeList;
        }

		#endregion

		#region QuotedGrowerVolume Lazy Loading Properties and Methods

        private Lazy<List<GrowerVolume>> _lazyQuotedGrowerVolumeList;

        public List<GrowerVolume> QuotedGrowerVolumeList
        {
            get
            {
                return _lazyQuotedGrowerVolumeList == null ? null : _lazyQuotedGrowerVolumeList.Value;
            }
        }

        public bool QuotedGrowerVolumeListIsLoaded
        {
            get
            {
                return _lazyQuotedGrowerVolumeList == null ? false : _lazyQuotedGrowerVolumeList.IsValueCreated;
            }
        }

        public void SetLazyQuotedGrowerVolumeList(Lazy<List<GrowerVolume>> lazyQuotedGrowerVolumeList)
        {
            _lazyQuotedGrowerVolumeList = lazyQuotedGrowerVolumeList;
        }

		#endregion

		#region Price Lazy Loading Properties and Methods

        private Lazy<List<Price>> _lazyPriceList;

        public List<Price> PriceList
        {
            get
            {
                return _lazyPriceList == null ? null : _lazyPriceList.Value;
            }
        }

        public bool PriceListIsLoaded
        {
            get
            {
                return _lazyPriceList == null ? false : _lazyPriceList.IsValueCreated;
            }
        }

        public void SetLazyPriceList(Lazy<List<Price>> lazyPriceList)
        {
            _lazyPriceList = lazyPriceList;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "VolumeLow={0}",this.VolumeLow);
			result.Append(", ");
			result.AppendFormat( "VolumeHigh={0}",this.VolumeHigh);
			result.Append(", ");
			result.AppendFormat( "ProgramSeasonGuid={0}",this.ProgramSeasonGuid);
			result.Append(", ");
			result.AppendFormat( "LevelNumber={0}",this.LevelNumber);
			result.Append(", ");
			result.AppendFormat( "VolumeLevelTypeLookupGuid={0}",this.VolumeLevelTypeLookupGuid);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}

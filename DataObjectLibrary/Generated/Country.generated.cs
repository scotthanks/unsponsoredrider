using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class Country : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			Code,
			Name,
		}

		new public enum ForeignKeyFieldEnum
		{
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public string Name { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "Name":
					value = StringToString( Name);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "Code":
						value = Code;
						break;
					case "Name":
						value = Name;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "Name":
						Name = (string)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region Price Lazy Loading Properties and Methods

        private Lazy<List<Price>> _lazyPriceList;

        public List<Price> PriceList
        {
            get
            {
                return _lazyPriceList == null ? null : _lazyPriceList.Value;
            }
        }

        public bool PriceListIsLoaded
        {
            get
            {
                return _lazyPriceList == null ? false : _lazyPriceList.IsValueCreated;
            }
        }

        public void SetLazyPriceList(Lazy<List<Price>> lazyPriceList)
        {
            _lazyPriceList = lazyPriceList;
        }

		#endregion

        //#region ProgramCountry Lazy Loading Properties and Methods

        //private Lazy<List<ProgramCountry>> _lazyProgramCountryList;

        //public List<ProgramCountry> ProgramCountryList
        //{
        //    get
        //    {
        //        return _lazyProgramCountryList == null ? null : _lazyProgramCountryList.Value;
        //    }
        //}

        //public bool ProgramCountryListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyProgramCountryList == null ? false : _lazyProgramCountryList.IsValueCreated;
        //    }
        //}

        //public void SetLazyProgramCountryList(Lazy<List<ProgramCountry>> lazyProgramCountryList)
        //{
        //    _lazyProgramCountryList = lazyProgramCountryList;
        //}

        //#endregion

		#region State Lazy Loading Properties and Methods

        private Lazy<List<State>> _lazyStateList;

        public List<State> StateList
        {
            get
            {
                return _lazyStateList == null ? null : _lazyStateList.Value;
            }
        }

        public bool StateListIsLoaded
        {
            get
            {
                return _lazyStateList == null ? false : _lazyStateList.IsValueCreated;
            }
        }

        public void SetLazyStateList(Lazy<List<State>> lazyStateList)
        {
            _lazyStateList = lazyStateList;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}

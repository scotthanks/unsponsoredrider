using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class GrowerOrder : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			GrowerGuid,
			ShipWeekGuid,
			OrderNo,
			Description,
			CustomerPoNo,
			OrderTypeLookupGuid,
			ProductFormCategoryGuid,
			ProgramTypeGuid,
			ShipToAddressGuid,
			PaymentTypeLookupGuid,
			GrowerOrderStatusLookupGuid,
			DateEntered,
			GrowerCreditCardGuid,
			PersonGuid,
			PromotionalCode,
            GrowerShipMethodCode,
            SellerGuid,
            NavigationProgramTypeCode,
            NavigationProductFormCategoryCode,

		}

		new public enum ForeignKeyFieldEnum
		{
			GrowerGuid,
			ShipWeekGuid,
			OrderTypeLookupGuid,
			ProductFormCategoryGuid,
			ProgramTypeGuid,
			ShipToAddressGuid,
			PaymentTypeLookupGuid,
			GrowerOrderStatusLookupGuid,
			GrowerCreditCardGuid,
			PersonGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Grower),
						typeof(ShipWeek),
						typeof(Lookup),
						typeof(ProductFormCategory),
						typeof(ProgramType),
						typeof(Address),
						typeof(Lookup),
						typeof(Lookup),
						typeof(GrowerCreditCard),
						typeof(Person),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public Guid GrowerGuid { get; set; }
		public Guid ShipWeekGuid { get; set; }
		public string OrderNo { get; set; }
		public string Description { get; set; }
		public string CustomerPoNo { get; set; }
		public Guid OrderTypeLookupGuid { get; set; }
		public Guid ProductFormCategoryGuid { get; set; }
		public Guid ProgramTypeGuid { get; set; }
		public Guid? ShipToAddressGuid { get; set; }
		public Guid PaymentTypeLookupGuid { get; set; }
		public Guid GrowerOrderStatusLookupGuid { get; set; }
		public DateTime DateEntered { get; set; }
		public Guid? GrowerCreditCardGuid { get; set; }
		public Guid PersonGuid { get; set; }
        public string PromotionalCode { get; set; }
        public string GrowerShipMethodCode { get; set; }
        public Guid SellerGuid { get; set; }
        public string NavigationProgramTypeCode { get; set; }
        public string NavigationProductFormCategoryCode { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "GrowerGuid":
					value = GuidToString( GrowerGuid);
					break;
				case "ShipWeekGuid":
					value = GuidToString( ShipWeekGuid);
					break;
				case "OrderNo":
					value = StringToString( OrderNo);
					break;
				case "Description":
					value = StringToString( Description);
					break;
				case "CustomerPoNo":
					value = StringToString( CustomerPoNo);
					break;
				case "OrderTypeLookupGuid":
					value = GuidToString( OrderTypeLookupGuid);
					break;
				case "ProductFormCategoryGuid":
					value = GuidToString( ProductFormCategoryGuid);
					break;
				case "ProgramTypeGuid":
					value = GuidToString( ProgramTypeGuid);
					break;
				case "ShipToAddressGuid":
					value = GuidToString( ShipToAddressGuid);
					break;
				case "PaymentTypeLookupGuid":
					value = GuidToString( PaymentTypeLookupGuid);
					break;
				case "GrowerOrderStatusLookupGuid":
					value = GuidToString( GrowerOrderStatusLookupGuid);
					break;
				case "DateEntered":
					value = DateTimeToString( DateEntered);
					break;
				case "GrowerCreditCardGuid":
					value = GuidToString( GrowerCreditCardGuid);
					break;
				case "PersonGuid":
					value = GuidToString( PersonGuid);
					break;
                case "PromotionalCode":
                    value = StringToString(PromotionalCode);
                    break;
                case "GrowerShipMethodCode":
                    value = StringToString(GrowerShipMethodCode);
                    break;
                case "SellerGuid":
                    value = GuidToString(SellerGuid);
                    break;
                case "NavigationProgramTypeCode":
					value = StringToString( NavigationProgramTypeCode);
					break;
			    case "NavigationProductFormCategoryCode":
					value = StringToString( NavigationProductFormCategoryCode);
					break;

            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "GrowerGuid":
					GrowerGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "ShipWeekGuid":
					ShipWeekGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "OrderNo":
					OrderNo = (string)StringFromString( value, isNullable: false);
					break;
				case "Description":
					Description = (string)StringFromString( value, isNullable: false);
					break;
				case "CustomerPoNo":
					CustomerPoNo = (string)StringFromString( value, isNullable: false);
					break;
				case "OrderTypeLookupGuid":
					OrderTypeLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "ProductFormCategoryGuid":
					ProductFormCategoryGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "ProgramTypeGuid":
					ProgramTypeGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "ShipToAddressGuid":
					ShipToAddressGuid = GuidFromString( value, isNullable: true);
					break;
				case "PaymentTypeLookupGuid":
					PaymentTypeLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "GrowerOrderStatusLookupGuid":
					GrowerOrderStatusLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateEntered":
					DateEntered = (DateTime)DateTimeFromString( value, isNullable: false);
					break;
				case "GrowerCreditCardGuid":
					GrowerCreditCardGuid = GuidFromString( value, isNullable: true);
					break;
				case "PersonGuid":
					PersonGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "PromotionalCode":
					PromotionalCode = (string)StringFromString( value, isNullable: false);
					break;
                case "GrowerShipMethodCode":
                    GrowerShipMethodCode = (string)StringFromString(value, isNullable: false);
                    break;
                case "SellerGuid":
                    SellerGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;

                case "NavigationProgramTypeCode":
                    NavigationProgramTypeCode = (string)StringFromString(value, isNullable: false);
                    break;

                case "NavigationProductFormCategoryCode":
                    NavigationProductFormCategoryCode = (string)StringFromString(value, isNullable: false);
                    break;            
                
                default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "GrowerGuid":
						value = GrowerGuid;
						break;
					case "ShipWeekGuid":
						value = ShipWeekGuid;
						break;
					case "OrderNo":
						value = OrderNo;
						break;
					case "Description":
						value = Description;
						break;
					case "CustomerPoNo":
						value = CustomerPoNo;
						break;
					case "OrderTypeLookupGuid":
						value = OrderTypeLookupGuid;
						break;
					case "ProductFormCategoryGuid":
						value = ProductFormCategoryGuid;
						break;
					case "ProgramTypeGuid":
						value = ProgramTypeGuid;
						break;
					case "ShipToAddressGuid":
						value = ShipToAddressGuid;
						break;
					case "PaymentTypeLookupGuid":
						value = PaymentTypeLookupGuid;
						break;
					case "GrowerOrderStatusLookupGuid":
						value = GrowerOrderStatusLookupGuid;
						break;
					case "DateEntered":
						value = DateEntered;
						break;
					case "GrowerCreditCardGuid":
						value = GrowerCreditCardGuid;
						break;
					case "PersonGuid":
						value = PersonGuid;
						break;
					case "PromotionalCode":
						value = PromotionalCode;
						break;
                    case "GrowerShipMethodCode":
                       value = GrowerShipMethodCode;  
                        break;
                    case "SellerGuid":
                        value = SellerGuid;   
                        break;
                    case "NavigationProgramTypeCode":
                        value = NavigationProgramTypeCode;
                        break;
                    case "NavigationProductFormCategoryCode":
                        value = NavigationProductFormCategoryCode;
                        break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "GrowerGuid":
						GrowerGuid = (Guid)value;
						break;
					case "ShipWeekGuid":
						ShipWeekGuid = (Guid)value;
						break;
					case "OrderNo":
						OrderNo = (string)value;
						break;
					case "Description":
						Description = (string)value;
						break;
					case "CustomerPoNo":
						CustomerPoNo = (string)value;
						break;
					case "OrderTypeLookupGuid":
						OrderTypeLookupGuid = (Guid)value;
						break;
					case "ProductFormCategoryGuid":
						ProductFormCategoryGuid = (Guid)value;
						break;
					case "ProgramTypeGuid":
						ProgramTypeGuid = (Guid)value;
						break;
					case "ShipToAddressGuid":
						ShipToAddressGuid = (Guid?)value;
						break;
					case "PaymentTypeLookupGuid":
						PaymentTypeLookupGuid = (Guid)value;
						break;
					case "GrowerOrderStatusLookupGuid":
						GrowerOrderStatusLookupGuid = (Guid)value;
						break;
					case "DateEntered":
						DateEntered = (DateTime)value;
						break;
					case "GrowerCreditCardGuid":
						GrowerCreditCardGuid = (Guid?)value;
						break;
					case "PersonGuid":
						PersonGuid = (Guid)value;
						break;
					case "PromotionalCode":
						PromotionalCode = (string)value;
						break;
                    case "GrowerShipMethodCode":
                        GrowerShipMethodCode = (string)value;
                        break;
                    case "SellerGuid":
						SellerGuid = (Guid)value;
                        break;
                    case "NavigationProgramTypeCode":
 						NavigationProgramTypeCode = (string)value;
						break;
                    case "NavigationProductFormCategoryCode":
  						NavigationProductFormCategoryCode = (string)value;
						break;

                }
            }
	    }

		#region Lazy Loading Logic

		#region Grower Lazy Loading Properties and Methods

        private Lazy<Grower> _lazyGrower;

        public Grower Grower
        {
            get
            {
                return _lazyGrower == null ? null : _lazyGrower.Value;
            }
            set
            {
                _lazyGrower = new Lazy<Grower>(() => value);
            }
        }

        public bool GrowerIsLoaded
        {
            get
            {
                return _lazyGrower == null ? false : _lazyGrower.IsValueCreated;
            }
        }

        public void SetLazyGrower(Lazy<Grower> lazyGrower)
        {
            _lazyGrower = lazyGrower;
        }

		#endregion

		#region ShipWeek Lazy Loading Properties and Methods

        private Lazy<ShipWeek> _lazyShipWeek;

        public ShipWeek ShipWeek
        {
            get
            {
                return _lazyShipWeek == null ? null : _lazyShipWeek.Value;
            }
            set
            {
                _lazyShipWeek = new Lazy<ShipWeek>(() => value);
            }
        }

        public bool ShipWeekIsLoaded
        {
            get
            {
                return _lazyShipWeek == null ? false : _lazyShipWeek.IsValueCreated;
            }
        }

        public void SetLazyShipWeek(Lazy<ShipWeek> lazyShipWeek)
        {
            _lazyShipWeek = lazyShipWeek;
        }

		#endregion

		#region OrderTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyOrderTypeLookup;

        public Lookup OrderTypeLookup
        {
            get
            {
                return _lazyOrderTypeLookup == null ? null : _lazyOrderTypeLookup.Value;
            }
            set
            {
                _lazyOrderTypeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool OrderTypeLookupIsLoaded
        {
            get
            {
                return _lazyOrderTypeLookup == null ? false : _lazyOrderTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyOrderTypeLookup(Lazy<Lookup> lazyOrderTypeLookup)
        {
            _lazyOrderTypeLookup = lazyOrderTypeLookup;
        }

		#endregion

		#region ProductFormCategory Lazy Loading Properties and Methods

        private Lazy<ProductFormCategory> _lazyProductFormCategory;

        public ProductFormCategory ProductFormCategory
        {
            get
            {
                return _lazyProductFormCategory == null ? null : _lazyProductFormCategory.Value;
            }
            set
            {
                _lazyProductFormCategory = new Lazy<ProductFormCategory>(() => value);
            }
        }

        public bool ProductFormCategoryIsLoaded
        {
            get
            {
                return _lazyProductFormCategory == null ? false : _lazyProductFormCategory.IsValueCreated;
            }
        }

        public void SetLazyProductFormCategory(Lazy<ProductFormCategory> lazyProductFormCategory)
        {
            _lazyProductFormCategory = lazyProductFormCategory;
        }

		#endregion

		#region ProgramType Lazy Loading Properties and Methods

        private Lazy<ProgramType> _lazyProgramType;

        public ProgramType ProgramType
        {
            get
            {
                return _lazyProgramType == null ? null : _lazyProgramType.Value;
            }
            set
            {
                _lazyProgramType = new Lazy<ProgramType>(() => value);
            }
        }

        public bool ProgramTypeIsLoaded
        {
            get
            {
                return _lazyProgramType == null ? false : _lazyProgramType.IsValueCreated;
            }
        }

        public void SetLazyProgramType(Lazy<ProgramType> lazyProgramType)
        {
            _lazyProgramType = lazyProgramType;
        }

		#endregion

		#region ShipToAddress Lazy Loading Properties and Methods

        private Lazy<Address> _lazyShipToAddress;

        public Address ShipToAddress
        {
            get
            {
                return _lazyShipToAddress == null ? null : _lazyShipToAddress.Value;
            }
            set
            {
                _lazyShipToAddress = new Lazy<Address>(() => value);
            }
        }

        public bool ShipToAddressIsLoaded
        {
            get
            {
                return _lazyShipToAddress == null ? false : _lazyShipToAddress.IsValueCreated;
            }
        }

        public void SetLazyShipToAddress(Lazy<Address> lazyShipToAddress)
        {
            _lazyShipToAddress = lazyShipToAddress;
        }

		#endregion

		#region PaymentTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyPaymentTypeLookup;

        public Lookup PaymentTypeLookup
        {
            get
            {
                return _lazyPaymentTypeLookup == null ? null : _lazyPaymentTypeLookup.Value;
            }
            set
            {
                _lazyPaymentTypeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool PaymentTypeLookupIsLoaded
        {
            get
            {
                return _lazyPaymentTypeLookup == null ? false : _lazyPaymentTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyPaymentTypeLookup(Lazy<Lookup> lazyPaymentTypeLookup)
        {
            _lazyPaymentTypeLookup = lazyPaymentTypeLookup;
        }

		#endregion

		#region GrowerOrderStatusLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyGrowerOrderStatusLookup;

        public Lookup GrowerOrderStatusLookup
        {
            get
            {
                return _lazyGrowerOrderStatusLookup == null ? null : _lazyGrowerOrderStatusLookup.Value;
            }
            set
            {
                _lazyGrowerOrderStatusLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool GrowerOrderStatusLookupIsLoaded
        {
            get
            {
                return _lazyGrowerOrderStatusLookup == null ? false : _lazyGrowerOrderStatusLookup.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrderStatusLookup(Lazy<Lookup> lazyGrowerOrderStatusLookup)
        {
            _lazyGrowerOrderStatusLookup = lazyGrowerOrderStatusLookup;
        }

		#endregion

		#region GrowerCreditCard Lazy Loading Properties and Methods

        private Lazy<GrowerCreditCard> _lazyGrowerCreditCard;

        public GrowerCreditCard GrowerCreditCard
        {
            get
            {
                return _lazyGrowerCreditCard == null ? null : _lazyGrowerCreditCard.Value;
            }
            set
            {
                _lazyGrowerCreditCard = new Lazy<GrowerCreditCard>(() => value);
            }
        }

        public bool GrowerCreditCardIsLoaded
        {
            get
            {
                return _lazyGrowerCreditCard == null ? false : _lazyGrowerCreditCard.IsValueCreated;
            }
        }

        public void SetLazyGrowerCreditCard(Lazy<GrowerCreditCard> lazyGrowerCreditCard)
        {
            _lazyGrowerCreditCard = lazyGrowerCreditCard;
        }

		#endregion

		#region Person Lazy Loading Properties and Methods

        private Lazy<Person> _lazyPerson;

        public Person Person
        {
            get
            {
                return _lazyPerson == null ? null : _lazyPerson.Value;
            }
            set
            {
                _lazyPerson = new Lazy<Person>(() => value);
            }
        }

        public bool PersonIsLoaded
        {
            get
            {
                return _lazyPerson == null ? false : _lazyPerson.IsValueCreated;
            }
        }

        public void SetLazyPerson(Lazy<Person> lazyPerson)
        {
            _lazyPerson = lazyPerson;
        }

		#endregion

        //#region Claim Lazy Loading Properties and Methods

        //private Lazy<List<Claim>> _lazyClaimList;

        //public List<Claim> ClaimList
        //{
        //    get
        //    {
        //        return _lazyClaimList == null ? null : _lazyClaimList.Value;
        //    }
        //}

        //public bool ClaimListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyClaimList == null ? false : _lazyClaimList.IsValueCreated;
        //    }
        //}

        //public void SetLazyClaimList(Lazy<List<Claim>> lazyClaimList)
        //{
        //    _lazyClaimList = lazyClaimList;
        //}

        //#endregion

		#region GrowerOrderFee Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrderFee>> _lazyGrowerOrderFeeList;

        public List<GrowerOrderFee> GrowerOrderFeeList
        {
            get
            {
                return _lazyGrowerOrderFeeList == null ? null : _lazyGrowerOrderFeeList.Value;
            }
        }

        public bool GrowerOrderFeeListIsLoaded
        {
            get
            {
                return _lazyGrowerOrderFeeList == null ? false : _lazyGrowerOrderFeeList.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrderFeeList(Lazy<List<GrowerOrderFee>> lazyGrowerOrderFeeList)
        {
            _lazyGrowerOrderFeeList = lazyGrowerOrderFeeList;
        }

		#endregion

		#region GrowerOrderHistory Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrderHistory>> _lazyGrowerOrderHistoryList;

        public List<GrowerOrderHistory> GrowerOrderHistoryList
        {
            get
            {
                return _lazyGrowerOrderHistoryList == null ? null : _lazyGrowerOrderHistoryList.Value;
            }
        }

        public bool GrowerOrderHistoryListIsLoaded
        {
            get
            {
                return _lazyGrowerOrderHistoryList == null ? false : _lazyGrowerOrderHistoryList.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrderHistoryList(Lazy<List<GrowerOrderHistory>> lazyGrowerOrderHistoryList)
        {
            _lazyGrowerOrderHistoryList = lazyGrowerOrderHistoryList;
        }

		#endregion

        //#region GrowerOrderReminder Lazy Loading Properties and Methods

        //private Lazy<List<GrowerOrderReminder>> _lazyGrowerOrderReminderList;

        //public List<GrowerOrderReminder> GrowerOrderReminderList
        //{
        //    get
        //    {
        //        return _lazyGrowerOrderReminderList == null ? null : _lazyGrowerOrderReminderList.Value;
        //    }
        //}

        //public bool GrowerOrderReminderListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyGrowerOrderReminderList == null ? false : _lazyGrowerOrderReminderList.IsValueCreated;
        //    }
        //}

        //public void SetLazyGrowerOrderReminderList(Lazy<List<GrowerOrderReminder>> lazyGrowerOrderReminderList)
        //{
        //    _lazyGrowerOrderReminderList = lazyGrowerOrderReminderList;
        //}

        //#endregion

		#region SupplierOrder Lazy Loading Properties and Methods

        private Lazy<List<SupplierOrder>> _lazySupplierOrderList;

        public List<SupplierOrder> SupplierOrderList
        {
            get
            {
                return _lazySupplierOrderList == null ? null : _lazySupplierOrderList.Value;
            }
        }

        public bool SupplierOrderListIsLoaded
        {
            get
            {
                return _lazySupplierOrderList == null ? false : _lazySupplierOrderList.IsValueCreated;
            }
        }

        public void SetLazySupplierOrderList(Lazy<List<SupplierOrder>> lazySupplierOrderList)
        {
            _lazySupplierOrderList = lazySupplierOrderList;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "GrowerGuid={0}",this.GrowerGuid);
			result.Append(", ");
			result.AppendFormat( "ShipWeekGuid={0}",this.ShipWeekGuid);
			result.Append(", ");
			result.AppendFormat( "OrderNo={0}",this.OrderNo);
			result.Append(", ");
			result.AppendFormat( "Description={0}",this.Description);
			result.Append(", ");
			result.AppendFormat( "CustomerPoNo={0}",this.CustomerPoNo);
			result.Append(", ");
			result.AppendFormat( "OrderTypeLookupGuid={0}",this.OrderTypeLookupGuid);
			result.Append(", ");
			result.AppendFormat( "ProductFormCategoryGuid={0}",this.ProductFormCategoryGuid);
			result.Append(", ");
			result.AppendFormat( "ProgramTypeGuid={0}",this.ProgramTypeGuid);
			result.Append(", ");
			result.AppendFormat( "ShipToAddressGuid={0}",this.ShipToAddressGuid);
			result.Append(", ");
			result.AppendFormat( "PaymentTypeLookupGuid={0}",this.PaymentTypeLookupGuid);
			result.Append(", ");
			result.AppendFormat( "GrowerOrderStatusLookupGuid={0}",this.GrowerOrderStatusLookupGuid);
			result.Append(", ");
			result.AppendFormat( "DateEntered={0}",this.DateEntered);
			result.Append(", ");
			result.AppendFormat( "GrowerCreditCardGuid={0}",this.GrowerCreditCardGuid);
			result.Append(", ");
			result.AppendFormat( "PersonGuid={0}",this.PersonGuid);
			result.Append(", ");
			result.AppendFormat( "PromotionalCode={0}",this.PromotionalCode);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return false; }
		new protected static bool GetHasNameColumn() { return false; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}

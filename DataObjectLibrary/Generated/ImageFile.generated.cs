using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class ImageFile : TableObjectBase
	{
		public enum ColumnEnum
		{
			Guid,
			Id,
			Code,
			Name,
			FileExtensionLookupGuid,
			ImageFileTypeLookupGuid,
			Height,
			Width,
			Size,
			Notes,
			ImageSetGuid,
			DateDeactivated,
		}

		new public enum ForeignKeyFieldEnum
		{
			FileExtensionLookupGuid,
			ImageFileTypeLookupGuid,
			ImageSetGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Lookup),
						typeof(Lookup),
						typeof(ImageSet),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public string Name { get; set; }
		public Guid FileExtensionLookupGuid { get; set; }
		public Guid ImageFileTypeLookupGuid { get; set; }
		public int Height { get; set; }
		public int Width { get; set; }
		public long Size { get; set; }
		public string Notes { get; set; }
		public Guid ImageSetGuid { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "Id":
					value = LongToString( Id);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "Name":
					value = StringToString( Name);
					break;
				case "FileExtensionLookupGuid":
					value = GuidToString( FileExtensionLookupGuid);
					break;
				case "ImageFileTypeLookupGuid":
					value = GuidToString( ImageFileTypeLookupGuid);
					break;
				case "Height":
					value = IntToString( Height);
					break;
				case "Width":
					value = IntToString( Width);
					break;
				case "Size":
					value = LongToString( Size);
					break;
				case "Notes":
					value = StringToString( Notes);
					break;
				case "ImageSetGuid":
					value = GuidToString( ImageSetGuid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				case "FileExtensionLookupGuid":
					FileExtensionLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "ImageFileTypeLookupGuid":
					ImageFileTypeLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "Height":
					Height = (int)IntFromString( value, isNullable: false);
					break;
				case "Width":
					Width = (int)IntFromString( value, isNullable: false);
					break;
				case "Size":
					Size = (long)LongFromString( value, isNullable: false);
					break;
				case "Notes":
					Notes = (string)StringFromString( value, isNullable: false);
					break;
				case "ImageSetGuid":
					ImageSetGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Guid":
						value = Guid;
						break;
					case "Id":
						value = Id;
						break;
					case "Code":
						value = Code;
						break;
					case "Name":
						value = Name;
						break;
					case "FileExtensionLookupGuid":
						value = FileExtensionLookupGuid;
						break;
					case "ImageFileTypeLookupGuid":
						value = ImageFileTypeLookupGuid;
						break;
					case "Height":
						value = Height;
						break;
					case "Width":
						value = Width;
						break;
					case "Size":
						value = Size;
						break;
					case "Notes":
						value = Notes;
						break;
					case "ImageSetGuid":
						value = ImageSetGuid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Guid":
						Guid = (Guid)value;
						break;
					case "Id":
						Id = (long)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "Name":
						Name = (string)value;
						break;
					case "FileExtensionLookupGuid":
						FileExtensionLookupGuid = (Guid)value;
						break;
					case "ImageFileTypeLookupGuid":
						ImageFileTypeLookupGuid = (Guid)value;
						break;
					case "Height":
						Height = (int)value;
						break;
					case "Width":
						Width = (int)value;
						break;
					case "Size":
						Size = (long)value;
						break;
					case "Notes":
						Notes = (string)value;
						break;
					case "ImageSetGuid":
						ImageSetGuid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region FileExtensionLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyFileExtensionLookup;

        public Lookup FileExtensionLookup
        {
            get
            {
                return _lazyFileExtensionLookup == null ? null : _lazyFileExtensionLookup.Value;
            }
            set
            {
                _lazyFileExtensionLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool FileExtensionLookupIsLoaded
        {
            get
            {
                return _lazyFileExtensionLookup == null ? false : _lazyFileExtensionLookup.IsValueCreated;
            }
        }

        public void SetLazyFileExtensionLookup(Lazy<Lookup> lazyFileExtensionLookup)
        {
            _lazyFileExtensionLookup = lazyFileExtensionLookup;
        }

		#endregion

		#region ImageFileTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyImageFileTypeLookup;

        public Lookup ImageFileTypeLookup
        {
            get
            {
                return _lazyImageFileTypeLookup == null ? null : _lazyImageFileTypeLookup.Value;
            }
            set
            {
                _lazyImageFileTypeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool ImageFileTypeLookupIsLoaded
        {
            get
            {
                return _lazyImageFileTypeLookup == null ? false : _lazyImageFileTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyImageFileTypeLookup(Lazy<Lookup> lazyImageFileTypeLookup)
        {
            _lazyImageFileTypeLookup = lazyImageFileTypeLookup;
        }

		#endregion

		#region ImageSet Lazy Loading Properties and Methods

        private Lazy<ImageSet> _lazyImageSet;

        public ImageSet ImageSet
        {
            get
            {
                return _lazyImageSet == null ? null : _lazyImageSet.Value;
            }
            set
            {
                _lazyImageSet = new Lazy<ImageSet>(() => value);
            }
        }

        public bool ImageSetIsLoaded
        {
            get
            {
                return _lazyImageSet == null ? false : _lazyImageSet.IsValueCreated;
            }
        }

        public void SetLazyImageSet(Lazy<ImageSet> lazyImageSet)
        {
            _lazyImageSet = lazyImageSet;
        }

		#endregion

		#region OriginalImageSet Lazy Loading Properties and Methods

        private Lazy<List<ImageSet>> _lazyOriginalImageSetList;

        public List<ImageSet> OriginalImageSetList
        {
            get
            {
                return _lazyOriginalImageSetList == null ? null : _lazyOriginalImageSetList.Value;
            }
        }

        public bool OriginalImageSetListIsLoaded
        {
            get
            {
                return _lazyOriginalImageSetList == null ? false : _lazyOriginalImageSetList.IsValueCreated;
            }
        }

        public void SetLazyOriginalImageSetList(Lazy<List<ImageSet>> lazyOriginalImageSetList)
        {
            _lazyOriginalImageSetList = lazyOriginalImageSetList;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);
			result.Append(", ");
			result.AppendFormat( "FileExtensionLookupGuid={0}",this.FileExtensionLookupGuid);
			result.Append(", ");
			result.AppendFormat( "ImageFileTypeLookupGuid={0}",this.ImageFileTypeLookupGuid);
			result.Append(", ");
			result.AppendFormat( "Height={0}",this.Height);
			result.Append(", ");
			result.AppendFormat( "Width={0}",this.Width);
			result.Append(", ");
			result.AppendFormat( "Size={0}",this.Size);
			result.Append(", ");
			result.AppendFormat( "Notes={0}",this.Notes);
			result.Append(", ");
			result.AppendFormat( "ImageSetGuid={0}",this.ImageSetGuid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}

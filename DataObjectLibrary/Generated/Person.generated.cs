using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class Person : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			FirstName,
			LastName,
			UserGuid,
			UserId,
			UserCode,
			EMail,
			PhoneAreaCode,
			Phone,
			SubscribeNewsletter,
			PersonTypeLookupGuid,
			GrowerGuid,
			PersonGetsOrderEmail,
			SubscribePromotions,
			IsInnovator,
		}

		new public enum ForeignKeyFieldEnum
		{
			PersonTypeLookupGuid,
			GrowerGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Lookup),
						typeof(Grower),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public string FirstName { get; set; }
		public string LastName { get; set; }
		public Guid? UserGuid { get; set; }
		public long? UserId { get; set; }
		public string UserCode { get; set; }
		public string EMail { get; set; }
		public Decimal PhoneAreaCode { get; set; }
		public Decimal Phone { get; set; }
		public bool SubscribeNewsletter { get; set; }
		public Guid PersonTypeLookupGuid { get; set; }
		public Guid? GrowerGuid { get; set; }
		public bool PersonGetsOrderEmail { get; set; }
		public bool SubscribePromotions { get; set; }
		public bool IsInnovator { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "FirstName":
					value = StringToString( FirstName);
					break;
				case "LastName":
					value = StringToString( LastName);
					break;
				case "UserGuid":
					value = GuidToString( UserGuid);
					break;
				case "UserId":
					value = LongToString( UserId);
					break;
				case "UserCode":
					value = StringToString( UserCode);
					break;
				case "EMail":
					value = StringToString( EMail);
					break;
				case "PhoneAreaCode":
					value = DecimalToString( PhoneAreaCode);
					break;
				case "Phone":
					value = DecimalToString( Phone);
					break;
				case "SubscribeNewsletter":
					value = BoolToString( SubscribeNewsletter);
					break;
				case "PersonTypeLookupGuid":
					value = GuidToString( PersonTypeLookupGuid);
					break;
				case "GrowerGuid":
					value = GuidToString( GrowerGuid);
					break;
				case "PersonGetsOrderEmail":
					value = BoolToString( PersonGetsOrderEmail);
					break;
				case "SubscribePromotions":
					value = BoolToString( SubscribePromotions);
					break;
				case "IsInnovator":
					value = BoolToString( IsInnovator);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "FirstName":
					FirstName = (string)StringFromString( value, isNullable: false);
					break;
				case "LastName":
					LastName = (string)StringFromString( value, isNullable: false);
					break;
				case "UserGuid":
					UserGuid = GuidFromString( value, isNullable: true);
					break;
				case "UserId":
					UserId = LongFromString( value, isNullable: true);
					break;
				case "UserCode":
					UserCode = (string)StringFromString( value, isNullable: false);
					break;
				case "EMail":
					EMail = (string)StringFromString( value, isNullable: false);
					break;
				case "PhoneAreaCode":
					PhoneAreaCode = (Decimal)DecimalFromString( value, isNullable: false);
					break;
				case "Phone":
					Phone = (Decimal)DecimalFromString( value, isNullable: false);
					break;
				case "SubscribeNewsletter":
					SubscribeNewsletter = (bool)BoolFromString( value, isNullable: false);
					break;
				case "PersonTypeLookupGuid":
					PersonTypeLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "GrowerGuid":
					GrowerGuid = GuidFromString( value, isNullable: true);
					break;
				case "PersonGetsOrderEmail":
					PersonGetsOrderEmail = (bool)BoolFromString( value, isNullable: false);
					break;
				case "SubscribePromotions":
					SubscribePromotions = (bool)BoolFromString( value, isNullable: false);
					break;
				case "IsInnovator":
					IsInnovator = (bool)BoolFromString( value, isNullable: false);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "FirstName":
						value = FirstName;
						break;
					case "LastName":
						value = LastName;
						break;
					case "UserGuid":
						value = UserGuid;
						break;
					case "UserId":
						value = UserId;
						break;
					case "UserCode":
						value = UserCode;
						break;
					case "EMail":
						value = EMail;
						break;
					case "PhoneAreaCode":
						value = PhoneAreaCode;
						break;
					case "Phone":
						value = Phone;
						break;
					case "SubscribeNewsletter":
						value = SubscribeNewsletter;
						break;
					case "PersonTypeLookupGuid":
						value = PersonTypeLookupGuid;
						break;
					case "GrowerGuid":
						value = GrowerGuid;
						break;
					case "PersonGetsOrderEmail":
						value = PersonGetsOrderEmail;
						break;
					case "SubscribePromotions":
						value = SubscribePromotions;
						break;
					case "IsInnovator":
						value = IsInnovator;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "FirstName":
						FirstName = (string)value;
						break;
					case "LastName":
						LastName = (string)value;
						break;
					case "UserGuid":
						UserGuid = (Guid?)value;
						break;
					case "UserId":
						UserId = (long?)value;
						break;
					case "UserCode":
						UserCode = (string)value;
						break;
					case "EMail":
						EMail = (string)value;
						break;
					case "PhoneAreaCode":
						PhoneAreaCode = (Decimal)value;
						break;
					case "Phone":
						Phone = (Decimal)value;
						break;
					case "SubscribeNewsletter":
						SubscribeNewsletter = (bool)value;
						break;
					case "PersonTypeLookupGuid":
						PersonTypeLookupGuid = (Guid)value;
						break;
					case "GrowerGuid":
						GrowerGuid = (Guid?)value;
						break;
					case "PersonGetsOrderEmail":
						PersonGetsOrderEmail = (bool)value;
						break;
					case "SubscribePromotions":
						SubscribePromotions = (bool)value;
						break;
					case "IsInnovator":
						IsInnovator = (bool)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region PersonTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyPersonTypeLookup;

        public Lookup PersonTypeLookup
        {
            get
            {
                return _lazyPersonTypeLookup == null ? null : _lazyPersonTypeLookup.Value;
            }
            set
            {
                _lazyPersonTypeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool PersonTypeLookupIsLoaded
        {
            get
            {
                return _lazyPersonTypeLookup == null ? false : _lazyPersonTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyPersonTypeLookup(Lazy<Lookup> lazyPersonTypeLookup)
        {
            _lazyPersonTypeLookup = lazyPersonTypeLookup;
        }

		#endregion

		#region Grower Lazy Loading Properties and Methods

        private Lazy<Grower> _lazyGrower;

        public Grower Grower
        {
            get
            {
                return _lazyGrower == null ? null : _lazyGrower.Value;
            }
            set
            {
                _lazyGrower = new Lazy<Grower>(() => value);
            }
        }

        public bool GrowerIsLoaded
        {
            get
            {
                return _lazyGrower == null ? false : _lazyGrower.IsValueCreated;
            }
        }

        public void SetLazyGrower(Lazy<Grower> lazyGrower)
        {
            _lazyGrower = lazyGrower;
        }

		#endregion

        //#region ChallengeEvent Lazy Loading Properties and Methods

        //private Lazy<List<ChallengeEvent>> _lazyChallengeEventList;

        //public List<ChallengeEvent> ChallengeEventList
        //{
        //    get
        //    {
        //        return _lazyChallengeEventList == null ? null : _lazyChallengeEventList.Value;
        //    }
        //}

        //public bool ChallengeEventListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyChallengeEventList == null ? false : _lazyChallengeEventList.IsValueCreated;
        //    }
        //}

        //public void SetLazyChallengeEventList(Lazy<List<ChallengeEvent>> lazyChallengeEventList)
        //{
        //    _lazyChallengeEventList = lazyChallengeEventList;
        //}

        //#endregion

        //#region EventLog Lazy Loading Properties and Methods

        //private Lazy<List<EventLog>> _lazyEventLogList;

        //public List<EventLog> EventLogList
        //{
        //    get
        //    {
        //        return _lazyEventLogList == null ? null : _lazyEventLogList.Value;
        //    }
        //}

        //public bool EventLogListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyEventLogList == null ? false : _lazyEventLogList.IsValueCreated;
        //    }
        //}

        //public void SetLazyEventLogList(Lazy<List<EventLog>> lazyEventLogList)
        //{
        //    _lazyEventLogList = lazyEventLogList;
        //}

        //#endregion

		#region GrowerOrder Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrder>> _lazyGrowerOrderList;

        public List<GrowerOrder> GrowerOrderList
        {
            get
            {
                return _lazyGrowerOrderList == null ? null : _lazyGrowerOrderList.Value;
            }
        }

        public bool GrowerOrderListIsLoaded
        {
            get
            {
                return _lazyGrowerOrderList == null ? false : _lazyGrowerOrderList.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrderList(Lazy<List<GrowerOrder>> lazyGrowerOrderList)
        {
            _lazyGrowerOrderList = lazyGrowerOrderList;
        }

		#endregion

		#region GrowerOrderHistory Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrderHistory>> _lazyGrowerOrderHistoryList;

        public List<GrowerOrderHistory> GrowerOrderHistoryList
        {
            get
            {
                return _lazyGrowerOrderHistoryList == null ? null : _lazyGrowerOrderHistoryList.Value;
            }
        }

        public bool GrowerOrderHistoryListIsLoaded
        {
            get
            {
                return _lazyGrowerOrderHistoryList == null ? false : _lazyGrowerOrderHistoryList.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrderHistoryList(Lazy<List<GrowerOrderHistory>> lazyGrowerOrderHistoryList)
        {
            _lazyGrowerOrderHistoryList = lazyGrowerOrderHistoryList;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "FirstName={0}",this.FirstName);
			result.Append(", ");
			result.AppendFormat( "LastName={0}",this.LastName);
			result.Append(", ");
			result.AppendFormat( "UserGuid={0}",this.UserGuid);
			result.Append(", ");
			result.AppendFormat( "UserId={0}",this.UserId);
			result.Append(", ");
			result.AppendFormat( "UserCode={0}",this.UserCode);
			result.Append(", ");
			result.AppendFormat( "EMail={0}",this.EMail);
			result.Append(", ");
			result.AppendFormat( "PhoneAreaCode={0}",this.PhoneAreaCode);
			result.Append(", ");
			result.AppendFormat( "Phone={0}",this.Phone);
			result.Append(", ");
			result.AppendFormat( "SubscribeNewsletter={0}",this.SubscribeNewsletter);
			result.Append(", ");
			result.AppendFormat( "PersonTypeLookupGuid={0}",this.PersonTypeLookupGuid);
			result.Append(", ");
			result.AppendFormat( "GrowerGuid={0}",this.GrowerGuid);
			result.Append(", ");
			result.AppendFormat( "PersonGetsOrderEmail={0}",this.PersonGetsOrderEmail);
			result.Append(", ");
			result.AppendFormat( "SubscribePromotions={0}",this.SubscribePromotions);
			result.Append(", ");
			result.AppendFormat( "IsInnovator={0}",this.IsInnovator);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return false; }
		new protected static bool GetHasNameColumn() { return false; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}

using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class GrowerOrderHistory : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			GrowerOrderGuid,
			Comment,
			IsInternal,
			PersonGuid,
			EventDate,
			LogTypeLookupGuid,
		}

		new public enum ForeignKeyFieldEnum
		{
			GrowerOrderGuid,
			PersonGuid,
			LogTypeLookupGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(GrowerOrder),
						typeof(Person),
						typeof(Lookup),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public Guid GrowerOrderGuid { get; set; }
		public string Comment { get; set; }
		public bool IsInternal { get; set; }
		public Guid PersonGuid { get; set; }
		public DateTime EventDate { get; set; }
		public Guid LogTypeLookupGuid { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "GrowerOrderGuid":
					value = GuidToString( GrowerOrderGuid);
					break;
				case "Comment":
					value = StringToString( Comment);
					break;
				case "IsInternal":
					value = BoolToString( IsInternal);
					break;
				case "PersonGuid":
					value = GuidToString( PersonGuid);
					break;
				case "EventDate":
					value = DateTimeToString( EventDate);
					break;
				case "LogTypeLookupGuid":
					value = GuidToString( LogTypeLookupGuid);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "GrowerOrderGuid":
					GrowerOrderGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "Comment":
					Comment = StringFromString( value, isNullable: true);
					break;
				case "IsInternal":
					IsInternal = (bool)BoolFromString( value, isNullable: false);
					break;
				case "PersonGuid":
					PersonGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "EventDate":
					EventDate = (DateTime)DateTimeFromString( value, isNullable: false);
					break;
				case "LogTypeLookupGuid":
					LogTypeLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "GrowerOrderGuid":
						value = GrowerOrderGuid;
						break;
					case "Comment":
						value = Comment;
						break;
					case "IsInternal":
						value = IsInternal;
						break;
					case "PersonGuid":
						value = PersonGuid;
						break;
					case "EventDate":
						value = EventDate;
						break;
					case "LogTypeLookupGuid":
						value = LogTypeLookupGuid;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "GrowerOrderGuid":
						GrowerOrderGuid = (Guid)value;
						break;
					case "Comment":
						Comment = (string)value;
						break;
					case "IsInternal":
						IsInternal = (bool)value;
						break;
					case "PersonGuid":
						PersonGuid = (Guid)value;
						break;
					case "EventDate":
						EventDate = (DateTime)value;
						break;
					case "LogTypeLookupGuid":
						LogTypeLookupGuid = (Guid)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region GrowerOrder Lazy Loading Properties and Methods

        private Lazy<GrowerOrder> _lazyGrowerOrder;

        public GrowerOrder GrowerOrder
        {
            get
            {
                return _lazyGrowerOrder == null ? null : _lazyGrowerOrder.Value;
            }
            set
            {
                _lazyGrowerOrder = new Lazy<GrowerOrder>(() => value);
            }
        }

        public bool GrowerOrderIsLoaded
        {
            get
            {
                return _lazyGrowerOrder == null ? false : _lazyGrowerOrder.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrder(Lazy<GrowerOrder> lazyGrowerOrder)
        {
            _lazyGrowerOrder = lazyGrowerOrder;
        }

		#endregion

		#region Person Lazy Loading Properties and Methods

        private Lazy<Person> _lazyPerson;

        public Person Person
        {
            get
            {
                return _lazyPerson == null ? null : _lazyPerson.Value;
            }
            set
            {
                _lazyPerson = new Lazy<Person>(() => value);
            }
        }

        public bool PersonIsLoaded
        {
            get
            {
                return _lazyPerson == null ? false : _lazyPerson.IsValueCreated;
            }
        }

        public void SetLazyPerson(Lazy<Person> lazyPerson)
        {
            _lazyPerson = lazyPerson;
        }

		#endregion

		#region LogTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyLogTypeLookup;

        public Lookup LogTypeLookup
        {
            get
            {
                return _lazyLogTypeLookup == null ? null : _lazyLogTypeLookup.Value;
            }
            set
            {
                _lazyLogTypeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool LogTypeLookupIsLoaded
        {
            get
            {
                return _lazyLogTypeLookup == null ? false : _lazyLogTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyLogTypeLookup(Lazy<Lookup> lazyLogTypeLookup)
        {
            _lazyLogTypeLookup = lazyLogTypeLookup;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "GrowerOrderGuid={0}",this.GrowerOrderGuid);
			result.Append(", ");
			result.AppendFormat( "Comment={0}",this.Comment);
			result.Append(", ");
			result.AppendFormat( "IsInternal={0}",this.IsInternal);
			result.Append(", ");
			result.AppendFormat( "PersonGuid={0}",this.PersonGuid);
			result.Append(", ");
			result.AppendFormat( "EventDate={0}",this.EventDate);
			result.Append(", ");
			result.AppendFormat( "LogTypeLookupGuid={0}",this.LogTypeLookupGuid);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return false; }
		new protected static bool GetHasNameColumn() { return false; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}

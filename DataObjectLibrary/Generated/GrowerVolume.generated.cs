using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class GrowerVolume : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			GrowerGuid,
			ProgramSeasonGuid,
			ActualVolumeLevelGuid,
			EstimatedVolumeLevelGuid,
			QuotedVolumeLevelGuid,
		}

		new public enum ForeignKeyFieldEnum
		{
			GrowerGuid,
			ProgramSeasonGuid,
			ActualVolumeLevelGuid,
			EstimatedVolumeLevelGuid,
			QuotedVolumeLevelGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Grower),
						typeof(ProgramSeason),
						typeof(VolumeLevel),
						typeof(VolumeLevel),
						typeof(VolumeLevel),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public Guid GrowerGuid { get; set; }
		public Guid ProgramSeasonGuid { get; set; }
		public Guid? ActualVolumeLevelGuid { get; set; }
		public Guid? EstimatedVolumeLevelGuid { get; set; }
		public Guid? QuotedVolumeLevelGuid { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "GrowerGuid":
					value = GuidToString( GrowerGuid);
					break;
				case "ProgramSeasonGuid":
					value = GuidToString( ProgramSeasonGuid);
					break;
				case "ActualVolumeLevelGuid":
					value = GuidToString( ActualVolumeLevelGuid);
					break;
				case "EstimatedVolumeLevelGuid":
					value = GuidToString( EstimatedVolumeLevelGuid);
					break;
				case "QuotedVolumeLevelGuid":
					value = GuidToString( QuotedVolumeLevelGuid);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "GrowerGuid":
					GrowerGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "ProgramSeasonGuid":
					ProgramSeasonGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "ActualVolumeLevelGuid":
					ActualVolumeLevelGuid = GuidFromString( value, isNullable: true);
					break;
				case "EstimatedVolumeLevelGuid":
					EstimatedVolumeLevelGuid = GuidFromString( value, isNullable: true);
					break;
				case "QuotedVolumeLevelGuid":
					QuotedVolumeLevelGuid = GuidFromString( value, isNullable: true);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "GrowerGuid":
						value = GrowerGuid;
						break;
					case "ProgramSeasonGuid":
						value = ProgramSeasonGuid;
						break;
					case "ActualVolumeLevelGuid":
						value = ActualVolumeLevelGuid;
						break;
					case "EstimatedVolumeLevelGuid":
						value = EstimatedVolumeLevelGuid;
						break;
					case "QuotedVolumeLevelGuid":
						value = QuotedVolumeLevelGuid;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "GrowerGuid":
						GrowerGuid = (Guid)value;
						break;
					case "ProgramSeasonGuid":
						ProgramSeasonGuid = (Guid)value;
						break;
					case "ActualVolumeLevelGuid":
						ActualVolumeLevelGuid = (Guid?)value;
						break;
					case "EstimatedVolumeLevelGuid":
						EstimatedVolumeLevelGuid = (Guid?)value;
						break;
					case "QuotedVolumeLevelGuid":
						QuotedVolumeLevelGuid = (Guid?)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region Grower Lazy Loading Properties and Methods

        private Lazy<Grower> _lazyGrower;

        public Grower Grower
        {
            get
            {
                return _lazyGrower == null ? null : _lazyGrower.Value;
            }
            set
            {
                _lazyGrower = new Lazy<Grower>(() => value);
            }
        }

        public bool GrowerIsLoaded
        {
            get
            {
                return _lazyGrower == null ? false : _lazyGrower.IsValueCreated;
            }
        }

        public void SetLazyGrower(Lazy<Grower> lazyGrower)
        {
            _lazyGrower = lazyGrower;
        }

		#endregion

		#region ProgramSeason Lazy Loading Properties and Methods

        private Lazy<ProgramSeason> _lazyProgramSeason;

        public ProgramSeason ProgramSeason
        {
            get
            {
                return _lazyProgramSeason == null ? null : _lazyProgramSeason.Value;
            }
            set
            {
                _lazyProgramSeason = new Lazy<ProgramSeason>(() => value);
            }
        }

        public bool ProgramSeasonIsLoaded
        {
            get
            {
                return _lazyProgramSeason == null ? false : _lazyProgramSeason.IsValueCreated;
            }
        }

        public void SetLazyProgramSeason(Lazy<ProgramSeason> lazyProgramSeason)
        {
            _lazyProgramSeason = lazyProgramSeason;
        }

		#endregion

		#region ActualVolumeLevel Lazy Loading Properties and Methods

        private Lazy<VolumeLevel> _lazyActualVolumeLevel;

        public VolumeLevel ActualVolumeLevel
        {
            get
            {
                return _lazyActualVolumeLevel == null ? null : _lazyActualVolumeLevel.Value;
            }
            set
            {
                _lazyActualVolumeLevel = new Lazy<VolumeLevel>(() => value);
            }
        }

        public bool ActualVolumeLevelIsLoaded
        {
            get
            {
                return _lazyActualVolumeLevel == null ? false : _lazyActualVolumeLevel.IsValueCreated;
            }
        }

        public void SetLazyActualVolumeLevel(Lazy<VolumeLevel> lazyActualVolumeLevel)
        {
            _lazyActualVolumeLevel = lazyActualVolumeLevel;
        }

		#endregion

		#region EstimatedVolumeLevel Lazy Loading Properties and Methods

        private Lazy<VolumeLevel> _lazyEstimatedVolumeLevel;

        public VolumeLevel EstimatedVolumeLevel
        {
            get
            {
                return _lazyEstimatedVolumeLevel == null ? null : _lazyEstimatedVolumeLevel.Value;
            }
            set
            {
                _lazyEstimatedVolumeLevel = new Lazy<VolumeLevel>(() => value);
            }
        }

        public bool EstimatedVolumeLevelIsLoaded
        {
            get
            {
                return _lazyEstimatedVolumeLevel == null ? false : _lazyEstimatedVolumeLevel.IsValueCreated;
            }
        }

        public void SetLazyEstimatedVolumeLevel(Lazy<VolumeLevel> lazyEstimatedVolumeLevel)
        {
            _lazyEstimatedVolumeLevel = lazyEstimatedVolumeLevel;
        }

		#endregion

		#region QuotedVolumeLevel Lazy Loading Properties and Methods

        private Lazy<VolumeLevel> _lazyQuotedVolumeLevel;

        public VolumeLevel QuotedVolumeLevel
        {
            get
            {
                return _lazyQuotedVolumeLevel == null ? null : _lazyQuotedVolumeLevel.Value;
            }
            set
            {
                _lazyQuotedVolumeLevel = new Lazy<VolumeLevel>(() => value);
            }
        }

        public bool QuotedVolumeLevelIsLoaded
        {
            get
            {
                return _lazyQuotedVolumeLevel == null ? false : _lazyQuotedVolumeLevel.IsValueCreated;
            }
        }

        public void SetLazyQuotedVolumeLevel(Lazy<VolumeLevel> lazyQuotedVolumeLevel)
        {
            _lazyQuotedVolumeLevel = lazyQuotedVolumeLevel;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "GrowerGuid={0}",this.GrowerGuid);
			result.Append(", ");
			result.AppendFormat( "ProgramSeasonGuid={0}",this.ProgramSeasonGuid);
			result.Append(", ");
			result.AppendFormat( "ActualVolumeLevelGuid={0}",this.ActualVolumeLevelGuid);
			result.Append(", ");
			result.AppendFormat( "EstimatedVolumeLevelGuid={0}",this.EstimatedVolumeLevelGuid);
			result.Append(", ");
			result.AppendFormat( "QuotedVolumeLevelGuid={0}",this.QuotedVolumeLevelGuid);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return false; }
		new protected static bool GetHasNameColumn() { return false; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}

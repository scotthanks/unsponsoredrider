using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
    public partial class Config : TableObjectBase
    {
        public enum ColumnEnum
        {
            Id,
            Guid,
            Code,
            Value,
            DateDeactivated,
        }

        new public enum ForeignKeyFieldEnum
        {
        }

        private static List<Type> _foreignKeyTableTypeList;

        public static List<Type> ForeignKeyTableTypeList
        {
            get
            {
                if (_foreignKeyTableTypeList == null)
                {
                    _foreignKeyTableTypeList = new List<Type>
                    {
                    };
                }

                return _foreignKeyTableTypeList;
            }
        }

        public string Value { get; set; }

        public override string GetFieldValueAsString(string fieldName)
        {
            string value = null;
            switch (fieldName)
            {
                case "Id":
                    value = LongToString(Id);
                    break;
                case "Guid":
                    value = GuidToString(Guid);
                    break;
                case "Code":
                    value = StringToString(Code);
                    break;
                case "Value":
                    value = StringToString(Value);
                    break;
                case "DateDeactivated":
                    value = DateTimeToString(DateDeactivated);
                    break;
            }
            return value;
        }

        public override bool ParseFieldFromString(string fieldName, string value)
        {
            bool valueSet = true;

            switch (fieldName)
            {
                case "Id":
                    Id = (long)LongFromString(value, isNullable: false);
                    break;
                case "Guid":
                    Guid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "Code":
                    Code = (string)StringFromString(value, isNullable: false);
                    break;
                case "Value":
                    Value = (string)StringFromString(value, isNullable: false);
                    break;
                case "DateDeactivated":
                    DateDeactivated = DateTimeFromString(value, isNullable: true);
                    break;
                default:
                    valueSet = false;
                    break;
            }

            return valueSet;
        }

        public override object this[string fieldName]
        {
            get
            {
                object value = null;
                switch (fieldName)
                {
                    case "Id":
                        value = Id;
                        break;
                    case "Guid":
                        value = Guid;
                        break;
                    case "Code":
                        value = Code;
                        break;
                    case "Value":
                        value = Value;
                        break;
                    case "DateDeactivated":
                        value = DateDeactivated;
                        break;
                }
                return value;
            }
            set
            {
                switch (fieldName)
                {
                    case "Id":
                        Id = (long)value;
                        break;
                    case "Guid":
                        Guid = (Guid)value;
                        break;
                    case "Code":
                        Code = (string)value;
                        break;
                    case "Value":
                        Value = (string)value;
                        break;
                    case "DateDeactivated":
                        DateDeactivated = (DateTime?)value;
                        break;
                }
            }
        }

        #region Lazy Loading Logic

        #endregion

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("Id={0}", this.Id);
            result.Append(", ");
            result.AppendFormat("Guid={0}", this.Guid);
            result.Append(", ");
            result.AppendFormat("Code={0}", this.Code);
            result.Append(", ");
            result.AppendFormat("Value={0}", this.Value);
            result.Append(", ");
            result.AppendFormat("DateDeactivated={0}", this.DateDeactivated);

            return result.ToString();
        }

        new protected static bool GetHasIdColumn() { return true; }
        new protected static bool GetHasGuidColumn() { return true; }
        new protected static bool GetHasDateDeactivatedColumn() { return true; }
        new protected static bool GetHasCodeColumn() { return true; }
        new protected static bool GetHasNameColumn() { return false; }
        new protected static bool GetDateDeactivatedIsNullable() { return true; }
    }
}

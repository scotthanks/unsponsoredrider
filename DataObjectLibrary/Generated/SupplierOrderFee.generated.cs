using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class SupplierOrderFee : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			SupplierOrderGuid,
			FeeTypeLookupGuid,
			Amount,
			FeeStatXLookupGuid,
			FeeDescription,
			IsManual,
		}

		new public enum ForeignKeyFieldEnum
		{
			SupplierOrderGuid,
			FeeTypeLookupGuid,
			FeeStatXLookupGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(SupplierOrder),
						typeof(Lookup),
						typeof(Lookup),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public Guid SupplierOrderGuid { get; set; }
		public Guid FeeTypeLookupGuid { get; set; }
		public Decimal Amount { get; set; }
		public Guid? FeeStatXLookupGuid { get; set; }
		public string FeeDescription { get; set; }
		public bool IsManual { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "SupplierOrderGuid":
					value = GuidToString( SupplierOrderGuid);
					break;
				case "FeeTypeLookupGuid":
					value = GuidToString( FeeTypeLookupGuid);
					break;
				case "Amount":
					value = DecimalToString( Amount);
					break;
				case "FeeStatXLookupGuid":
					value = GuidToString( FeeStatXLookupGuid);
					break;
				case "FeeDescription":
					value = StringToString( FeeDescription);
					break;
				case "IsManual":
					value = BoolToString( IsManual);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "SupplierOrderGuid":
					SupplierOrderGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "FeeTypeLookupGuid":
					FeeTypeLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "Amount":
					Amount = (Decimal)DecimalFromString( value, isNullable: false);
					break;
				case "FeeStatXLookupGuid":
					FeeStatXLookupGuid = GuidFromString( value, isNullable: true);
					break;
				case "FeeDescription":
					FeeDescription = (string)StringFromString( value, isNullable: false);
					break;
				case "IsManual":
					IsManual = (bool)BoolFromString( value, isNullable: false);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "SupplierOrderGuid":
						value = SupplierOrderGuid;
						break;
					case "FeeTypeLookupGuid":
						value = FeeTypeLookupGuid;
						break;
					case "Amount":
						value = Amount;
						break;
					case "FeeStatXLookupGuid":
						value = FeeStatXLookupGuid;
						break;
					case "FeeDescription":
						value = FeeDescription;
						break;
					case "IsManual":
						value = IsManual;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "SupplierOrderGuid":
						SupplierOrderGuid = (Guid)value;
						break;
					case "FeeTypeLookupGuid":
						FeeTypeLookupGuid = (Guid)value;
						break;
					case "Amount":
						Amount = (Decimal)value;
						break;
					case "FeeStatXLookupGuid":
						FeeStatXLookupGuid = (Guid?)value;
						break;
					case "FeeDescription":
						FeeDescription = (string)value;
						break;
					case "IsManual":
						IsManual = (bool)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region SupplierOrder Lazy Loading Properties and Methods

        private Lazy<SupplierOrder> _lazySupplierOrder;

        public SupplierOrder SupplierOrder
        {
            get
            {
                return _lazySupplierOrder == null ? null : _lazySupplierOrder.Value;
            }
            set
            {
                _lazySupplierOrder = new Lazy<SupplierOrder>(() => value);
            }
        }

        public bool SupplierOrderIsLoaded
        {
            get
            {
                return _lazySupplierOrder == null ? false : _lazySupplierOrder.IsValueCreated;
            }
        }

        public void SetLazySupplierOrder(Lazy<SupplierOrder> lazySupplierOrder)
        {
            _lazySupplierOrder = lazySupplierOrder;
        }

		#endregion

		#region FeeTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyFeeTypeLookup;

        public Lookup FeeTypeLookup
        {
            get
            {
                return _lazyFeeTypeLookup == null ? null : _lazyFeeTypeLookup.Value;
            }
            set
            {
                _lazyFeeTypeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool FeeTypeLookupIsLoaded
        {
            get
            {
                return _lazyFeeTypeLookup == null ? false : _lazyFeeTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyFeeTypeLookup(Lazy<Lookup> lazyFeeTypeLookup)
        {
            _lazyFeeTypeLookup = lazyFeeTypeLookup;
        }

		#endregion

		#region FeeStatXLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyFeeStatXLookup;

        public Lookup FeeStatXLookup
        {
            get
            {
                return _lazyFeeStatXLookup == null ? null : _lazyFeeStatXLookup.Value;
            }
            set
            {
                _lazyFeeStatXLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool FeeStatXLookupIsLoaded
        {
            get
            {
                return _lazyFeeStatXLookup == null ? false : _lazyFeeStatXLookup.IsValueCreated;
            }
        }

        public void SetLazyFeeStatXLookup(Lazy<Lookup> lazyFeeStatXLookup)
        {
            _lazyFeeStatXLookup = lazyFeeStatXLookup;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "SupplierOrderGuid={0}",this.SupplierOrderGuid);
			result.Append(", ");
			result.AppendFormat( "FeeTypeLookupGuid={0}",this.FeeTypeLookupGuid);
			result.Append(", ");
			result.AppendFormat( "Amount={0}",this.Amount);
			result.Append(", ");
			result.AppendFormat( "FeeStatXLookupGuid={0}",this.FeeStatXLookupGuid);
			result.Append(", ");
			result.AppendFormat( "FeeDescription={0}",this.FeeDescription);
			result.Append(", ");
			result.AppendFormat( "IsManual={0}",this.IsManual);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return false; }
		new protected static bool GetHasNameColumn() { return false; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}

using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class ImageSet : TableObjectBase
	{
		public enum ColumnEnum
		{
			Guid,
			Id,
			Code,
			Name,
			Caption,
			Description,
			ImageSetTypeLookupGuid,
			Notes,
			OriginalImageFileGuid,
			DateDeactivated,
		}

		new public enum ForeignKeyFieldEnum
		{
			ImageSetTypeLookupGuid,
			OriginalImageFileGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Lookup),
						typeof(ImageFile),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public string Name { get; set; }
		public string Caption { get; set; }
		public string Description { get; set; }
		public Guid ImageSetTypeLookupGuid { get; set; }
		public string Notes { get; set; }
		public Guid? OriginalImageFileGuid { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "Id":
					value = LongToString( Id);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "Name":
					value = StringToString( Name);
					break;
				case "Caption":
					value = StringToString( Caption);
					break;
				case "Description":
					value = StringToString( Description);
					break;
				case "ImageSetTypeLookupGuid":
					value = GuidToString( ImageSetTypeLookupGuid);
					break;
				case "Notes":
					value = StringToString( Notes);
					break;
				case "OriginalImageFileGuid":
					value = GuidToString( OriginalImageFileGuid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				case "Caption":
					Caption = (string)StringFromString( value, isNullable: false);
					break;
				case "Description":
					Description = (string)StringFromString( value, isNullable: false);
					break;
				case "ImageSetTypeLookupGuid":
					ImageSetTypeLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "Notes":
					Notes = (string)StringFromString( value, isNullable: false);
					break;
				case "OriginalImageFileGuid":
					OriginalImageFileGuid = GuidFromString( value, isNullable: true);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Guid":
						value = Guid;
						break;
					case "Id":
						value = Id;
						break;
					case "Code":
						value = Code;
						break;
					case "Name":
						value = Name;
						break;
					case "Caption":
						value = Caption;
						break;
					case "Description":
						value = Description;
						break;
					case "ImageSetTypeLookupGuid":
						value = ImageSetTypeLookupGuid;
						break;
					case "Notes":
						value = Notes;
						break;
					case "OriginalImageFileGuid":
						value = OriginalImageFileGuid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Guid":
						Guid = (Guid)value;
						break;
					case "Id":
						Id = (long)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "Name":
						Name = (string)value;
						break;
					case "Caption":
						Caption = (string)value;
						break;
					case "Description":
						Description = (string)value;
						break;
					case "ImageSetTypeLookupGuid":
						ImageSetTypeLookupGuid = (Guid)value;
						break;
					case "Notes":
						Notes = (string)value;
						break;
					case "OriginalImageFileGuid":
						OriginalImageFileGuid = (Guid?)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region ImageSetTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyImageSetTypeLookup;

        public Lookup ImageSetTypeLookup
        {
            get
            {
                return _lazyImageSetTypeLookup == null ? null : _lazyImageSetTypeLookup.Value;
            }
            set
            {
                _lazyImageSetTypeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool ImageSetTypeLookupIsLoaded
        {
            get
            {
                return _lazyImageSetTypeLookup == null ? false : _lazyImageSetTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyImageSetTypeLookup(Lazy<Lookup> lazyImageSetTypeLookup)
        {
            _lazyImageSetTypeLookup = lazyImageSetTypeLookup;
        }

		#endregion

		#region OriginalImageFile Lazy Loading Properties and Methods

        private Lazy<ImageFile> _lazyOriginalImageFile;

        public ImageFile OriginalImageFile
        {
            get
            {
                return _lazyOriginalImageFile == null ? null : _lazyOriginalImageFile.Value;
            }
            set
            {
                _lazyOriginalImageFile = new Lazy<ImageFile>(() => value);
            }
        }

        public bool OriginalImageFileIsLoaded
        {
            get
            {
                return _lazyOriginalImageFile == null ? false : _lazyOriginalImageFile.IsValueCreated;
            }
        }

        public void SetLazyOriginalImageFile(Lazy<ImageFile> lazyOriginalImageFile)
        {
            _lazyOriginalImageFile = lazyOriginalImageFile;
        }

		#endregion

        //#region Claim Lazy Loading Properties and Methods

        //private Lazy<List<Claim>> _lazyClaimList;

        //public List<Claim> ClaimList
        //{
        //    get
        //    {
        //        return _lazyClaimList == null ? null : _lazyClaimList.Value;
        //    }
        //}

        //public bool ClaimListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyClaimList == null ? false : _lazyClaimList.IsValueCreated;
        //    }
        //}

        //public void SetLazyClaimList(Lazy<List<Claim>> lazyClaimList)
        //{
        //    _lazyClaimList = lazyClaimList;
        //}

        //#endregion

		#region ImageFile Lazy Loading Properties and Methods

        private Lazy<List<ImageFile>> _lazyImageFileList;

        public List<ImageFile> ImageFileList
        {
            get
            {
                return _lazyImageFileList == null ? null : _lazyImageFileList.Value;
            }
        }

        public bool ImageFileListIsLoaded
        {
            get
            {
                return _lazyImageFileList == null ? false : _lazyImageFileList.IsValueCreated;
            }
        }

        public void SetLazyImageFileList(Lazy<List<ImageFile>> lazyImageFileList)
        {
            _lazyImageFileList = lazyImageFileList;
        }

		#endregion

		#region ProgramType Lazy Loading Properties and Methods

        private Lazy<List<ProgramType>> _lazyProgramTypeList;

        public List<ProgramType> ProgramTypeList
        {
            get
            {
                return _lazyProgramTypeList == null ? null : _lazyProgramTypeList.Value;
            }
        }

        public bool ProgramTypeListIsLoaded
        {
            get
            {
                return _lazyProgramTypeList == null ? false : _lazyProgramTypeList.IsValueCreated;
            }
        }

        public void SetLazyProgramTypeList(Lazy<List<ProgramType>> lazyProgramTypeList)
        {
            _lazyProgramTypeList = lazyProgramTypeList;
        }

		#endregion

		#region Variety Lazy Loading Properties and Methods

        private Lazy<List<Variety>> _lazyVarietyList;

        public List<Variety> VarietyList
        {
            get
            {
                return _lazyVarietyList == null ? null : _lazyVarietyList.Value;
            }
        }

        public bool VarietyListIsLoaded
        {
            get
            {
                return _lazyVarietyList == null ? false : _lazyVarietyList.IsValueCreated;
            }
        }

        public void SetLazyVarietyList(Lazy<List<Variety>> lazyVarietyList)
        {
            _lazyVarietyList = lazyVarietyList;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);
			result.Append(", ");
			result.AppendFormat( "Caption={0}",this.Caption);
			result.Append(", ");
			result.AppendFormat( "Description={0}",this.Description);
			result.Append(", ");
			result.AppendFormat( "ImageSetTypeLookupGuid={0}",this.ImageSetTypeLookupGuid);
			result.Append(", ");
			result.AppendFormat( "Notes={0}",this.Notes);
			result.Append(", ");
			result.AppendFormat( "OriginalImageFileGuid={0}",this.OriginalImageFileGuid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}

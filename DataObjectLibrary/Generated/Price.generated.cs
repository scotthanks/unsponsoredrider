using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class Price : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			CountryGuid,
			PriceGroupGuid,
			VolumeLevelGuid,
			RegularCost,
			EODCost,
			RegularMUPercent,
			EODMUPercent,
		}

		new public enum ForeignKeyFieldEnum
		{
			CountryGuid,
			PriceGroupGuid,
			VolumeLevelGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Country),
						typeof(PriceGroup),
						typeof(VolumeLevel),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public Guid CountryGuid { get; set; }
		public Guid PriceGroupGuid { get; set; }
		public Guid VolumeLevelGuid { get; set; }
		public Decimal RegularCost { get; set; }
		public Decimal EODCost { get; set; }
		public Double RegularMUPercent { get; set; }
		public Double EODMUPercent { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "CountryGuid":
					value = GuidToString( CountryGuid);
					break;
				case "PriceGroupGuid":
					value = GuidToString( PriceGroupGuid);
					break;
				case "VolumeLevelGuid":
					value = GuidToString( VolumeLevelGuid);
					break;
				case "RegularCost":
					value = DecimalToString( RegularCost);
					break;
				case "EODCost":
					value = DecimalToString( EODCost);
					break;
				case "RegularMUPercent":
					value = DoubleToString( RegularMUPercent);
					break;
				case "EODMUPercent":
					value = DoubleToString( EODMUPercent);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "CountryGuid":
					CountryGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "PriceGroupGuid":
					PriceGroupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "VolumeLevelGuid":
					VolumeLevelGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "RegularCost":
					RegularCost = (Decimal)DecimalFromString( value, isNullable: false);
					break;
				case "EODCost":
					EODCost = (Decimal)DecimalFromString( value, isNullable: false);
					break;
				case "RegularMUPercent":
					RegularMUPercent = (Double)DoubleFromString( value, isNullable: false);
					break;
				case "EODMUPercent":
					EODMUPercent = (Double)DoubleFromString( value, isNullable: false);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "CountryGuid":
						value = CountryGuid;
						break;
					case "PriceGroupGuid":
						value = PriceGroupGuid;
						break;
					case "VolumeLevelGuid":
						value = VolumeLevelGuid;
						break;
					case "RegularCost":
						value = RegularCost;
						break;
					case "EODCost":
						value = EODCost;
						break;
					case "RegularMUPercent":
						value = RegularMUPercent;
						break;
					case "EODMUPercent":
						value = EODMUPercent;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "CountryGuid":
						CountryGuid = (Guid)value;
						break;
					case "PriceGroupGuid":
						PriceGroupGuid = (Guid)value;
						break;
					case "VolumeLevelGuid":
						VolumeLevelGuid = (Guid)value;
						break;
					case "RegularCost":
						RegularCost = (Decimal)value;
						break;
					case "EODCost":
						EODCost = (Decimal)value;
						break;
					case "RegularMUPercent":
						RegularMUPercent = (Double)value;
						break;
					case "EODMUPercent":
						EODMUPercent = (Double)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region Country Lazy Loading Properties and Methods

        private Lazy<Country> _lazyCountry;

        public Country Country
        {
            get
            {
                return _lazyCountry == null ? null : _lazyCountry.Value;
            }
            set
            {
                _lazyCountry = new Lazy<Country>(() => value);
            }
        }

        public bool CountryIsLoaded
        {
            get
            {
                return _lazyCountry == null ? false : _lazyCountry.IsValueCreated;
            }
        }

        public void SetLazyCountry(Lazy<Country> lazyCountry)
        {
            _lazyCountry = lazyCountry;
        }

		#endregion

		#region PriceGroup Lazy Loading Properties and Methods

        private Lazy<PriceGroup> _lazyPriceGroup;

        public PriceGroup PriceGroup
        {
            get
            {
                return _lazyPriceGroup == null ? null : _lazyPriceGroup.Value;
            }
            set
            {
                _lazyPriceGroup = new Lazy<PriceGroup>(() => value);
            }
        }

        public bool PriceGroupIsLoaded
        {
            get
            {
                return _lazyPriceGroup == null ? false : _lazyPriceGroup.IsValueCreated;
            }
        }

        public void SetLazyPriceGroup(Lazy<PriceGroup> lazyPriceGroup)
        {
            _lazyPriceGroup = lazyPriceGroup;
        }

		#endregion

		#region VolumeLevel Lazy Loading Properties and Methods

        private Lazy<VolumeLevel> _lazyVolumeLevel;

        public VolumeLevel VolumeLevel
        {
            get
            {
                return _lazyVolumeLevel == null ? null : _lazyVolumeLevel.Value;
            }
            set
            {
                _lazyVolumeLevel = new Lazy<VolumeLevel>(() => value);
            }
        }

        public bool VolumeLevelIsLoaded
        {
            get
            {
                return _lazyVolumeLevel == null ? false : _lazyVolumeLevel.IsValueCreated;
            }
        }

        public void SetLazyVolumeLevel(Lazy<VolumeLevel> lazyVolumeLevel)
        {
            _lazyVolumeLevel = lazyVolumeLevel;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "CountryGuid={0}",this.CountryGuid);
			result.Append(", ");
			result.AppendFormat( "PriceGroupGuid={0}",this.PriceGroupGuid);
			result.Append(", ");
			result.AppendFormat( "VolumeLevelGuid={0}",this.VolumeLevelGuid);
			result.Append(", ");
			result.AppendFormat( "RegularCost={0}",this.RegularCost);
			result.Append(", ");
			result.AppendFormat( "EODCost={0}",this.EODCost);
			result.Append(", ");
			result.AppendFormat( "RegularMUPercent={0}",this.RegularMUPercent);
			result.Append(", ");
			result.AppendFormat( "EODMUPercent={0}",this.EODMUPercent);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return false; }
		new protected static bool GetHasNameColumn() { return false; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}

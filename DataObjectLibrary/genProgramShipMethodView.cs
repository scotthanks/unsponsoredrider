﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
 
    public partial class ProgramShipMethodView : DataObjectBase
    {
        public enum ColumnEnum
        {
            ProgramGuid,
            ShipMethodLookupGuid,
            IsDefault,
        }

        new public enum ForeignKeyFieldEnum
        {
            ProgramGuid,
            ShipMethodLookupGuid,
        }

        private static List<Type> _foreignKeyTableTypeList;

        public static List<Type> ForeignKeyTableTypeList
        {
            get
            {
                if (_foreignKeyTableTypeList == null)
                {
                    _foreignKeyTableTypeList = new List<Type>
                    {
                        typeof(Program),
                        typeof(Lookup),
                    };
                }

                return _foreignKeyTableTypeList;
            }
        }

        public Guid ProgramGuid { get; set; }
        public Guid ShipMethodLookupGuid { get; set; }
        public bool IsDefault { get; set; }

    

        public override string GetFieldValueAsString(string fieldName)
        {
            string value = null;
            switch (fieldName)
            {
                case "ProgramGuid":
                    value = GuidToString(ProgramGuid);
                    break;
                case "ShipMethodLookupGuid":
                    value = GuidToString(ShipMethodLookupGuid);
                    break;
                case "IsDefault":
                    value = BoolToString(IsDefault);
                    break;
            }
            return value;
        }

        public override bool ParseFieldFromString(string fieldName, string value)
        {
            bool valueSet = true;

            switch (fieldName)
            {
                case "ProgramGuid":
                    ProgramGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ShipMethodLookupGuid":
                    ShipMethodLookupGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "IsDefault":
                    IsDefault = Convert.ToBoolean(BoolFromString(value, isNullable: true));
                    break;
                default:
                    valueSet = false;
                    break;
            }

            return valueSet;
        }

        public override object this[string fieldName]
        {
            get
            {
                object value = null;
                switch (fieldName)
                {
                    case "ProgramGuid":
                        value = ProgramGuid;
                        break;
                    case "ShipMethodLookupGuid":
                        value = ShipMethodLookupGuid;
                        break;
                    case "IsDefault":
                        value = IsDefault;
                        break;
                }
                return value;
            }
            set
            {
                switch (fieldName)
                {
                    case "ProgramGuid":
                        ProgramGuid = (Guid)value;
                        break;
                    case "ShipMethodLookupGuid":
                        ShipMethodLookupGuid = (Guid)value;
                        break;
                    case "IsDefault":
                        if ((bool?)value==true) {
                            IsDefault = true;
                        }
                        else{IsDefault = false;
                        }
                        //IsDefault = Convert.ToBoolean(value);
                         
                        break;
                }
            }
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("ProgramGuid={0}", this.ProgramGuid);
            result.Append(", ");
            result.AppendFormat("ShipMethodLookupGuid={0}", this.ShipMethodLookupGuid);
            result.Append(", ");
            result.AppendFormat("IsDefault={0}", this.IsDefault);

            return result.ToString();
        }

        #region Lazy Loading Logic

        #region Program Lazy Loading Properties and Methods

        private Lazy<Program> _lazyProgram;

        public Program Program
        {
            get
            {
                return _lazyProgram == null ? null : _lazyProgram.Value;
            }
        }

        public bool ProgramIsLoaded
        {
            get
            {
                return _lazyProgram == null ? false : _lazyProgram.IsValueCreated;
            }
        }

        public void SetLazyProgram(Lazy<Program> lazyProgram)
        {
            _lazyProgram = lazyProgram;
        }

        #endregion

        #region ShipMethodLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyShipMethodLookup;

        public Lookup ShipMethodLookup
        {
            get
            {
                return _lazyShipMethodLookup == null ? null : _lazyShipMethodLookup.Value;
            }
        }

        public bool ShipMethodLookupIsLoaded
        {
            get
            {
                return _lazyShipMethodLookup == null ? false : _lazyShipMethodLookup.IsValueCreated;
            }
        }

        public void SetLazyShipMethodLookup(Lazy<Lookup> lazyShipMethodLookup)
        {
            _lazyShipMethodLookup = lazyShipMethodLookup;
        }

        #endregion

        #endregion
    }
}

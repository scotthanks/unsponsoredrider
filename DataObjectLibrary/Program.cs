﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
    public partial class Program
    {
        private List<Lookup> _tagRatioLookupList;
        private List<Lookup> _shownShipMethodList;

        public List<Lookup> TagRatioLookupList
        {
            get { return _tagRatioLookupList ?? (_tagRatioLookupList = new List<Lookup>()); }
            set { _tagRatioLookupList = value; }
        }

        public Lookup TagRatioDefault { get; set; }

        public List<Lookup> ShownShipMethodList
        {
            get { return _shownShipMethodList ?? (_shownShipMethodList = new List<Lookup>()); }
            set { _shownShipMethodList = value; }
        }
        
        public Lookup ShipMethodDefault { get; set; }

        public ProgramSeason CurrentProgramSeason { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
    public partial class SupplierOrder
    {
        // The SupplierOrder does not have a reference to the Program, but there is always a single Program 
        // implied. This is because of the fact that there can only be one program for each supplier for a 
        // given ProductFormCategory and ProgramType, and each Grower order is limited to one of each of these.
        // The following value may be filled in by a stored procedure service.
        public Program Program { get; set; }
    }
}

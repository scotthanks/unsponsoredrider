﻿using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
    public partial class ProductFormCategoryProgramTypeCombinationView : DataObjectBase
    {
        public enum ColumnEnum
        {
            ProductFormCategoryGuid,
            ProductFormCategoryCode,
            ProgramTypeGuid,
            ProgramTypeCode,
        }

        new public enum ForeignKeyFieldEnum
        {
            ProductFormCategoryGuid,
            ProgramTypeGuid,
        }

        private static List<Type> _foreignKeyTableTypeList;

        public static List<Type> ForeignKeyTableTypeList
        {
            get
            {
                if (_foreignKeyTableTypeList == null)
                {
                    _foreignKeyTableTypeList = new List<Type>
                    {
                        typeof(ProductFormCategory),
                        typeof(ProgramType),
                    };
                }

                return _foreignKeyTableTypeList;
            }
        }

        public Guid ProductFormCategoryGuid { get; set; }
        public string ProductFormCategoryCode { get; set; }
        public Guid ProgramTypeGuid { get; set; }
        public string ProgramTypeCode { get; set; }

        public override string GetFieldValueAsString(string fieldName)
        {
            string value = null;
            switch (fieldName)
            {
                case "ProductFormCategoryGuid":
                    value = GuidToString(ProductFormCategoryGuid);
                    break;
                case "ProductFormCategoryCode":
                    value = StringToString(ProductFormCategoryCode);
                    break;
                case "ProgramTypeGuid":
                    value = GuidToString(ProgramTypeGuid);
                    break;
                case "ProgramTypeCode":
                    value = StringToString(ProgramTypeCode);
                    break;
            }
            return value;
        }

        public override bool ParseFieldFromString(string fieldName, string value)
        {
            bool valueSet = true;

            switch (fieldName)
            {
                case "ProductFormCategoryGuid":
                    ProductFormCategoryGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ProductFormCategoryCode":
                    ProductFormCategoryCode = (string)StringFromString(value, isNullable: false);
                    break;
                case "ProgramTypeGuid":
                    ProgramTypeGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ProgramTypeCode":
                    ProgramTypeCode = (string)StringFromString(value, isNullable: false);
                    break;
                default:
                    valueSet = false;
                    break;
            }

            return valueSet;
        }

        public override object this[string fieldName]
        {
            get
            {
                object value = null;
                switch (fieldName)
                {
                    case "ProductFormCategoryGuid":
                        value = ProductFormCategoryGuid;
                        break;
                    case "ProductFormCategoryCode":
                        value = ProductFormCategoryCode;
                        break;
                    case "ProgramTypeGuid":
                        value = ProgramTypeGuid;
                        break;
                    case "ProgramTypeCode":
                        value = ProgramTypeCode;
                        break;
                }
                return value;
            }
            set
            {
                switch (fieldName)
                {
                    case "ProductFormCategoryGuid":
                        ProductFormCategoryGuid = (Guid)value;
                        break;
                    case "ProductFormCategoryCode":
                        ProductFormCategoryCode = (string)value;
                        break;
                    case "ProgramTypeGuid":
                        ProgramTypeGuid = (Guid)value;
                        break;
                    case "ProgramTypeCode":
                        ProgramTypeCode = (string)value;
                        break;
                }
            }
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("ProductFormCategoryGuid={0}", this.ProductFormCategoryGuid);
            result.Append(", ");
            result.AppendFormat("ProductFormCategoryCode={0}", this.ProductFormCategoryCode);
            result.Append(", ");
            result.AppendFormat("ProgramTypeGuid={0}", this.ProgramTypeGuid);
            result.Append(", ");
            result.AppendFormat("ProgramTypeCode={0}", this.ProgramTypeCode);

            return result.ToString();
        }

        #region Lazy Loading Logic

        #region ProductFormCategory Lazy Loading Properties and Methods

        private Lazy<ProductFormCategory> _lazyProductFormCategory;

        public ProductFormCategory ProductFormCategory
        {
            get
            {
                return _lazyProductFormCategory == null ? null : _lazyProductFormCategory.Value;
            }
        }

        public bool ProductFormCategoryIsLoaded
        {
            get
            {
                return _lazyProductFormCategory == null ? false : _lazyProductFormCategory.IsValueCreated;
            }
        }

        public void SetLazyProductFormCategory(Lazy<ProductFormCategory> lazyProductFormCategory)
        {
            _lazyProductFormCategory = lazyProductFormCategory;
        }

        #endregion

        #region ProgramType Lazy Loading Properties and Methods

        private Lazy<ProgramType> _lazyProgramType;

        public ProgramType ProgramType
        {
            get
            {
                return _lazyProgramType == null ? null : _lazyProgramType.Value;
            }
        }

        public bool ProgramTypeIsLoaded
        {
            get
            {
                return _lazyProgramType == null ? false : _lazyProgramType.IsValueCreated;
            }
        }

        public void SetLazyProgramType(Lazy<ProgramType> lazyProgramType)
        {
            _lazyProgramType = lazyProgramType;
        }

        #endregion

        #endregion
    }
}

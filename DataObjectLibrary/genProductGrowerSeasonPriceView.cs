﻿using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
    public partial class ProductGrowerSeasonPriceView : DataObjectBase
    {
        public enum ColumnEnum
        {
            ProductGuid,
            GrowerGuid,
            ProgramSeasonGuid,
            SeasonStartDate,
            SeasonEndDate,
            PriceGuid,
            DummenPrice,
        }

        new public enum ForeignKeyFieldEnum
        {
            ProductGuid,
            GrowerGuid,
            ProgramSeasonGuid,
            PriceGuid,
            DummenPrice,
        }

        private static List<Type> _foreignKeyTableTypeList;

        public static List<Type> ForeignKeyTableTypeList
        {
            get
            {
                if (_foreignKeyTableTypeList == null)
                {
                    _foreignKeyTableTypeList = new List<Type>
                    {
                        typeof(Product),
                        typeof(Grower),
                        typeof(ProgramSeason),
                        typeof(Price),
                    };
                }

                return _foreignKeyTableTypeList;
            }
        }

        public Guid ProductGuid { get; set; }
        public Guid GrowerGuid { get; set; }
        public Guid ProgramSeasonGuid { get; set; }
        public DateTime SeasonStartDate { get; set; }
        public DateTime SeasonEndDate { get; set; }
        public Guid? PriceGuid { get; set; }
        public decimal? DummenPrice { get; set; }

        public override string GetFieldValueAsString(string fieldName)
        {
            string value = null;
            switch (fieldName)
            {
                case "ProductGuid":
                    value = GuidToString(ProductGuid);
                    break;
                case "GrowerGuid":
                    value = GuidToString(GrowerGuid);
                    break;
                case "ProgramSeasonGuid":
                    value = GuidToString(ProgramSeasonGuid);
                    break;
                case "SeasonStartDate":
                    value = DateTimeToString(SeasonStartDate);
                    break;
                case "SeasonEndDate":
                    value = DateTimeToString(SeasonEndDate);
                    break;
                case "PriceGuid":
                    value = GuidToString(PriceGuid);
                    break;
                case "DummenPrice":
                    value = DecimalToString(DummenPrice);
                    break;
            }
            return value;
        }

        public override bool ParseFieldFromString(string fieldName, string value)
        {
            bool valueSet = true;

            switch (fieldName)
            {
                case "ProductGuid":
                    ProductGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "GrowerGuid":
                    GrowerGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ProgramSeasonGuid":
                    ProgramSeasonGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "SeasonStartDate":
                    SeasonStartDate = (DateTime)DateTimeFromString(value, isNullable: false);
                    break;
                case "SeasonEndDate":
                    SeasonEndDate = (DateTime)DateTimeFromString(value, isNullable: false);
                    break;
                case "PriceGuid":
                    PriceGuid = GuidFromString(value, isNullable: true);
                    break;
                case "DummenPrice":
                    DummenPrice = DecimalFromString(value, isNullable: true);
                    break;
                default:
                    valueSet = false;
                    break;
            }

            return valueSet;
        }

        public override object this[string fieldName]
        {
            get
            {
                object value = null;
                switch (fieldName)
                {
                    case "ProductGuid":
                        value = ProductGuid;
                        break;
                    case "GrowerGuid":
                        value = GrowerGuid;
                        break;
                    case "ProgramSeasonGuid":
                        value = ProgramSeasonGuid;
                        break;
                    case "SeasonStartDate":
                        value = SeasonStartDate;
                        break;
                    case "SeasonEndDate":
                        value = SeasonEndDate;
                        break;
                    case "PriceGuid":
                        value = PriceGuid;
                        break;
                    case "DummenPrice":
                        value = DummenPrice;
                        break;
                }
                return value;
            }
            set
            {
                switch (fieldName)
                {
                    case "ProductGuid":
                        ProductGuid = (Guid)value;
                        break;
                    case "GrowerGuid":
                        GrowerGuid = (Guid)value;
                        break;
                    case "ProgramSeasonGuid":
                        ProgramSeasonGuid = (Guid)value;
                        break;
                    case "SeasonStartDate":
                        SeasonStartDate = (DateTime)value;
                        break;
                    case "SeasonEndDate":
                        SeasonEndDate = (DateTime)value;
                        break;
                    case "PriceGuid":
                        PriceGuid = (Guid?)value;
                        break;
                    case "DummenPrice":
                        DummenPrice = (Decimal?)value;
                        break;
                }
            }
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("ProductGuid={0}", this.ProductGuid);
            result.Append(", ");
            result.AppendFormat("GrowerGuid={0}", this.GrowerGuid);
            result.Append(", ");
            result.AppendFormat("ProgramSeasonGuid={0}", this.ProgramSeasonGuid);
            result.Append(", ");
            result.AppendFormat("SeasonStartDate={0}", this.SeasonStartDate);
            result.Append(", ");
            result.AppendFormat("SeasonEndDate={0}", this.SeasonEndDate);
            result.Append(", ");
            result.AppendFormat("PriceGuid={0}", this.PriceGuid);
            result.Append(", ");
            result.AppendFormat("DummenPrice={0}", this.DummenPrice);

            return result.ToString();
        }

        #region Lazy Loading Logic

        #region Product Lazy Loading Properties and Methods

        private Lazy<Product> _lazyProduct;

        public Product Product
        {
            get
            {
                return _lazyProduct == null ? null : _lazyProduct.Value;
            }
        }

        public bool ProductIsLoaded
        {
            get
            {
                return _lazyProduct == null ? false : _lazyProduct.IsValueCreated;
            }
        }

        public void SetLazyProduct(Lazy<Product> lazyProduct)
        {
            _lazyProduct = lazyProduct;
        }

        #endregion

        #region Grower Lazy Loading Properties and Methods

        private Lazy<Grower> _lazyGrower;

        public Grower Grower
        {
            get
            {
                return _lazyGrower == null ? null : _lazyGrower.Value;
            }
        }

        public bool GrowerIsLoaded
        {
            get
            {
                return _lazyGrower == null ? false : _lazyGrower.IsValueCreated;
            }
        }

        public void SetLazyGrower(Lazy<Grower> lazyGrower)
        {
            _lazyGrower = lazyGrower;
        }

        #endregion

        #region ProgramSeason Lazy Loading Properties and Methods

        private Lazy<ProgramSeason> _lazyProgramSeason;

        public ProgramSeason ProgramSeason
        {
            get
            {
                return _lazyProgramSeason == null ? null : _lazyProgramSeason.Value;
            }
        }

        public bool ProgramSeasonIsLoaded
        {
            get
            {
                return _lazyProgramSeason == null ? false : _lazyProgramSeason.IsValueCreated;
            }
        }

        public void SetLazyProgramSeason(Lazy<ProgramSeason> lazyProgramSeason)
        {
            _lazyProgramSeason = lazyProgramSeason;
        }

        #endregion

        #region Price Lazy Loading Properties and Methods

        private Lazy<Price> _lazyPrice;

        public Price Price
        {
            get
            {
                return _lazyPrice == null ? null : _lazyPrice.Value;
            }
        }

        public bool PriceIsLoaded
        {
            get
            {
                return _lazyPrice == null ? false : _lazyPrice.IsValueCreated;
            }
        }

        public void SetLazyPrice(Lazy<Price> lazyPrice)
        {
            _lazyPrice = lazyPrice;
        }

        #endregion

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
    public class AvailabilityData
    {
        public DateTime ShipDate { get; set; }
        public List<ShipWeek> ShipWeekList { get; set; }
        public List<ReportedAvailability> ReportedAvailabilityList { get; set; }
        public List<OrderLine> PreCartOrderLineList { get; set; }
        public List<OrderLine> CartedOrderLineList { get; set; }
        public List<ProductGrowerSeasonPriceView> ProductPriceList { get; set; }
        public List<Price> Prices { get; set; }
        public List<ProgramSeason> ProgramSeasonList { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
    public abstract class TableObjectBase : DataObjectBase
    {
        public Guid Guid { get; set; }
        public long Id { get; set; }
        public string Code { get; set; }
        public DateTime? DateDeactivated { get; set; }

        public enum ForeignKeyFieldEnum
        {
        };

        protected TableObjectBase()
        {
            Guid = Guid.NewGuid();
        }

        public bool IsActive
        {
            get { return DateDeactivated == null; }
            set
            {
                if (value != IsActive)
                {
                    if (value == true)
                    {
                        DateDeactivated = null;
                    }
                    else
                    {
                        DateDeactivated = DateTime.Now;
                    }
                }
            }
        }

        public static bool HasIdColumn
        {
            get { return GetHasIdColumn(); }
        }

        protected static bool GetHasIdColumn()
        {
            // This method should always be overridden in the subclass (which is generated).
            throw new NotImplementedException();            
        }

        public static bool HasGuidColumn
        {
            get { return GetHasGuidColumn(); }
        }

        protected static bool GetHasGuidColumn()
        {
            // This method should always be overridden in the subclass (which is generated).
            throw new NotImplementedException();            
        }

        public static bool HasDateDeactivatedColumn
        {
            get { return GetHasDateDeactivatedColumn(); }
        }

        protected static bool GetHasDateDeactivatedColumn()
        {
            // This method should always be overridden in the subclass (which is generated).
            throw new NotImplementedException();
        }

        protected static bool DateDeactivatedIsNullable
        {
            get { return GetDateDeactivatedIsNullable(); }
        }

        protected static bool GetDateDeactivatedIsNullable()
        {
            // This method should always be overridden in the subclass (which is generated).
            throw new NotImplementedException();
        }

        public static bool HasCodeColumn
        {
            get { return GetHasCodeColumn(); }
        }

        protected static bool GetHasCodeColumn()
        {
            // This method should always be overridden in the subclass (which is generated).
            throw new NotImplementedException();
        }

        public static bool HasNameColumn
        {
            get { return GetHasNameColumn(); }
        }

        protected static bool GetHasNameColumn()
        {
            // This method should always be overridden in the subclass (which is generated).
            throw new NotImplementedException();            
        }

        protected static bool IsValidDatabaseConfiguration
        {
            get { return HasIdColumn && HasGuidColumn && DateDeactivatedIsNullableIfExists; }
        }

        // If the DateDeactivated column exists, it must be nullable.
        private static bool DateDeactivatedIsNullableIfExists
        {
            get { return !HasDateDeactivatedColumn || DateDeactivatedIsNullable; }
        }
    }
}

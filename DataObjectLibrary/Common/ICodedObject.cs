﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
    public interface ICodedObject
    {
        string Code { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
    public abstract class DataObjectBase
    {
        public abstract string GetFieldValueAsString(string fieldName);

        public abstract bool ParseFieldFromString(string fieldName, string value);

        public abstract object this[string fieldName] { get; set; }

        public string GetFieldValueAsString2(string fieldName)
        {
            return this[fieldName].ToString();
        }

        public void ParseFieldFromString2(string fieldName, string value)
        {
            this[fieldName] = value;
        }

        protected bool? BoolFromString(string s, bool isNullable)
        {
            bool b;
            bool success = bool.TryParse(s, out b);

            return (!success && isNullable) ? null : (bool?)b;
        }

        protected string BoolToString(bool? b)
        {
            return b.ToString();
        }

        protected DateTime? DateTimeFromString(string s, bool isNullable)
        {
            DateTime dateTime;
            bool success = DateTime.TryParse(s, out dateTime);

            if (success)
                return dateTime;
            else if (isNullable)
                return null;
            else
                return DateTime.MinValue;
        }

        protected string DateTimeToString(DateTime? dateTime)
        {
            return dateTime.ToString();
        }

        protected decimal? DecimalFromString(string s, bool isNullable)
        {
            decimal d;
            bool success = decimal.TryParse(s, out d);

            if (success)
                return d;
            else if (isNullable)
                return null;
            else
                return decimal.MinValue;
        }

        protected string DecimalToString(decimal? d)
        {
            return d.ToString();
        }

        protected double? DoubleFromString(string s, bool isNullable)
        {
            double d;
            bool success = double.TryParse(s, out d);

            if (success)
                return d;
            else if (isNullable)
                return null;
            else return double.MinValue;
        }

        protected string DoubleToString(double? d)
        {
            return d.ToString();
        }

        protected Guid? GuidFromString(string s, bool isNullable)
        {
            Guid guid;
            bool success = Guid.TryParse(s, out guid);

            if (success)
                return guid;
            else if (isNullable)
                return null;
            else
                return Guid.Empty;
        }

        protected string GuidToString(Guid? guid)
        {
            return guid.ToString();
        }

        protected int? IntFromString(string s, bool isNullable)
        {
            int i;
            bool success = int.TryParse(s, out i);

            if (success)
                return i;
            else if (isNullable)
                return null;
            else
                return int.MinValue;
        }

        protected string IntToString(int? i)
        {
            return i.ToString();
        }

        protected long? LongFromString(string s, bool isNullable)
        {
            long l;
            bool success = long.TryParse(s, out l);

            if (success)
                return l;
            else if (isNullable)
                return null;
            else
                return long.MinValue;
        }

        protected string LongToString(long? l)
        {
            return l.ToString();
        }

        protected string StringFromString(string s, bool isNullable)
        {
            if (s == null && !isNullable)
                s = "";

            return StringToString(s);
        }

        protected string StringToString(string s)
        {
            if (s != null)
                s = s.Trim();

            return s;
        }
    }
}

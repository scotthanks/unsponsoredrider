﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimitiveObjectLibrary
{
    public abstract class PrimitiveObjectBase
    {
        public Guid Guid { get; set; }
        public long Id { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
    public partial class Product
    {
        public string ProductDescriptionCalculated
        {
            get
            {
                if (!string.IsNullOrEmpty(this.ProductDescription))
                {
                    return this.ProductDescription;
                }
                else
                {
                    return string.Format("{0} ({1})", this.Variety.Name, this.ProductForm.SalesUnitQty);
                }
            }
            set { this.ProductDescription = value; }
        }
    }
}

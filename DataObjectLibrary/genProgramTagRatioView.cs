﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
 
    public partial class ProgramTagRatioView : DataObjectBase
    {
        public enum ColumnEnum
        {
            ProgramGuid,
            TagRatioLookupGuid,
            IsDefault,
        }

        public enum ForeignKeyFieldEnum
        {
            ProgramGuid,
            TagRatioLookupGuid,
        }

        private static List<Type> _foreignKeyTableTypeList;

        public static List<Type> ForeignKeyTableTypeList
        {
            get
            {
                if (_foreignKeyTableTypeList == null)
                {
                    _foreignKeyTableTypeList = new List<Type>
                    {
                        typeof(Program),
                        typeof(Lookup),
                    };
                }

                return _foreignKeyTableTypeList;
            }
        }

        public Guid ProgramGuid { get; set; }
        public Guid TagRatioLookupGuid { get; set; }
        public bool IsDefault { get; set; }

        public override string GetFieldValueAsString(string fieldName)
        {
            string value = null;
            switch (fieldName)
            {
                case "ProgramGuid":
                    value = GuidToString(ProgramGuid);
                    break;
                case "TagRatioLookupGuid":
                    value = GuidToString(TagRatioLookupGuid);
                    break;
                case "IsDefault":
                    if (IsDefault == true)
                    {
                        value = BoolToString(true);
                    }
                    else
                    {
                        value = BoolToString(false);
                    }
                    
                    break;
            }
            return value;
        }

        public override bool ParseFieldFromString(string fieldName, string value)
        {
            bool valueSet = true;

            switch (fieldName)
            {
                case "ProgramGuid":
                    ProgramGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "TagRatioLookupGuid":
                    TagRatioLookupGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "IsDefault":
                    if (BoolFromString(value, isNullable: true) == true)
                    {
                        IsDefault = true;
                    }
                    else
                    {
                        IsDefault = false;
                    }
                   
                    break;
                default:
                    valueSet = false;
                    break;
            }

            return valueSet;
        }

        public override object this[string fieldName]
        {
            get
            {
                object value = null;
                switch (fieldName)
                {
                    case "ProgramGuid":
                        value = ProgramGuid;
                        break;
                    case "TagRatioLookupGuid":
                        value = TagRatioLookupGuid;
                        break;
                    case "IsDefault":
                        value = IsDefault;
                        break;
                }
                return value;
            }
            set
            {
                switch (fieldName)
                {
                    case "ProgramGuid":
                        ProgramGuid = (Guid)value;
                        break;
                    case "TagRatioLookupGuid":
                        TagRatioLookupGuid = (Guid)value;
                        break;
                    case "IsDefault":
                        if ((bool?)value == true)
                        {
                            IsDefault = true;
                        }
                        else
                        {
                            IsDefault = false;
                        }
                        //IsDefault = (bool?)value;
                        break;
                }
            }
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("ProgramGuid={0}", this.ProgramGuid);
            result.Append(", ");
            result.AppendFormat("TagRatioLookupGuid={0}", this.TagRatioLookupGuid);
            result.Append(", ");
            result.AppendFormat("IsDefault={0}", this.IsDefault);

            return result.ToString();
        }

        #region Lazy Loading Logic

        #region Program Lazy Loading Properties and Methods

        private Lazy<Program> _lazyProgram;

        public Program Program
        {
            get
            {
                return _lazyProgram == null ? null : _lazyProgram.Value;
            }
        }

        public bool ProgramIsLoaded
        {
            get
            {
                return _lazyProgram == null ? false : _lazyProgram.IsValueCreated;
            }
        }

        public void SetLazyProgram(Lazy<Program> lazyProgram)
        {
            _lazyProgram = lazyProgram;
        }

        #endregion

        #region TagRatioLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyTagRatioLookup;

        public Lookup TagRatioLookup
        {
            get
            {
                return _lazyTagRatioLookup == null ? null : _lazyTagRatioLookup.Value;
            }
        }

        public bool TagRatioLookupIsLoaded
        {
            get
            {
                return _lazyTagRatioLookup == null ? false : _lazyTagRatioLookup.IsValueCreated;
            }
        }

        public void SetLazyTagRatioLookup(Lazy<Lookup> lazyTagRatioLookup)
        {
            _lazyTagRatioLookup = lazyTagRatioLookup;
        }

        #endregion

        #endregion
    }
}

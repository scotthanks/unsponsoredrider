﻿CREATE TABLE [dbo].[PageContent] (
    [Id]                BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]              UNIQUEIDENTIFIER NOT NULL,
    [IsLive]            BIT              NOT NULL,
    [Description]       VARCHAR (100)    NOT NULL,
    [Path]              VARCHAR (255)    NOT NULL,
    [Title]             VARCHAR (255)    NOT NULL,
    [CssPageGuid]       UNIQUEIDENTIFIER NULL,
    [HTML]              VARCHAR (MAX)    NOT NULL,
    [CSS]               VARCHAR (MAX)    NOT NULL,
    [Javascript]        VARCHAR (MAX)    NOT NULL,
    [JavascriptOnReady] VARCHAR (MAX)    NOT NULL,
    [Note]              TEXT             NOT NULL,
    [Type]              VARCHAR (20)     NOT NULL,
    CONSTRAINT [PK__PageContent_Guid] PRIMARY KEY CLUSTERED ([Guid] ASC)
);


﻿CREATE TABLE [dbo].[CSSPages] (
    [Id]          BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]        UNIQUEIDENTIFIER NOT NULL,
    [IsLive]      BIT              NOT NULL,
    [Description] VARCHAR (100)    NOT NULL,
    [CSS]         VARCHAR (MAX)    NULL,
    [Note]        TEXT             NULL,
    [Type]        VARCHAR (20)     NOT NULL,
    CONSTRAINT [PK__CSSPages__GUID] PRIMARY KEY CLUSTERED ([Guid] ASC)
);


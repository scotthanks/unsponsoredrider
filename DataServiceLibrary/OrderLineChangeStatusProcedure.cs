﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using DataAccessLibrary;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public partial class OrderLineChangeStatusProcedure : Procedure<OrderLineChangeStatusProcedure.OrderLineChangeStatusParameters>
    {
        [Obsolete("Use ChangeOrderLineStatus() directly.")]
        public int AddToCart(Guid userGuid, string userCode, string shipWeekCode, string programTypeCode, string productFormCategoryCode)
        {
            Parameters.UserGuid = userGuid;
            Parameters.UserCode = userCode;
            Parameters.ProgramTypeCode = programTypeCode;
            Parameters.ProductFormCategoryCode = productFormCategoryCode;
            Parameters.ShipWeekCode = shipWeekCode;
            Parameters.FromOrderLineStatusCode = "PreCart";
            Parameters.ToOrderLineStatusCode = "Pending";

            var storedProcedureServices = new StoredProcedureServices();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                try
                {
                    connection.Open();

                    using (var reader = storedProcedureServices.ReaderFromSproc(connection, this))
                    {
                        var context = new Context();
                    }
                }
                finally
                {
                    connection.Close();
                }
            }

            storedProcedureServices.GetOutputParameterValues();

            int itemsAddedToCart = 0;

            if (Parameters.OrderLinesChanged != null)
            {
                itemsAddedToCart = (int) Parameters.OrderLinesChanged;
            }

            return itemsAddedToCart;
        }

        public int AddToCartAllWeeks(Guid userGuid, string userCode, string programTypeCode, string productFormCategoryCode)
        {
            Parameters.UserGuid = userGuid;
            Parameters.UserCode = userCode;
            Parameters.ProgramTypeCode = programTypeCode;
            Parameters.ProductFormCategoryCode = productFormCategoryCode;
            Parameters.ShipWeekCode = "ALLALL";
            Parameters.FromOrderLineStatusCode = "PreCart";
            Parameters.ToOrderLineStatusCode = "Pending";

            var storedProcedureServices = new StoredProcedureServices();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                try
                {
                    connection.Open();

                    using (var reader = storedProcedureServices.ReaderFromSproc(connection, this))
                    {
                        var context = new Context();
                    }
                }
                finally
                {
                    connection.Close();
                }
            }

            storedProcedureServices.GetOutputParameterValues();

            int itemsAddedToCart = 0;

            if (Parameters.OrderLinesChanged != null)
            {
                itemsAddedToCart = (int)Parameters.OrderLinesChanged;
            }

            return itemsAddedToCart;
        }
       



        public class ReturnData
        {
            public int OrderLinesChanged { get; set; }
            public int OrderLinesDeleted { get; set; }
        }

        [Obsolete("Use ChangeOrderLineStatus() directly.")]
        public ReturnData PlaceOrder(Guid userGuid, string userCode, Guid growerOrderGuid)
        {
            ReturnData returnData = null;

            Parameters.UserGuid = userGuid;
            Parameters.GrowerOrderGuid = growerOrderGuid;

            Parameters.FromOrderLineStatusCode = "Pending";
            Parameters.ToOrderLineStatusCode = "GrowerNotified";
            Parameters.DeleteOrderLinesWithOrderLineStatusCode = "PreCart";
           

            var storedProcedureServices = new StoredProcedureServices();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    

                    using (var reader = storedProcedureServices.ReaderFromSproc(connection, this, transaction: transaction))
                    {
                        reader.Close();
                    }

                    storedProcedureServices.GetOutputParameterValues();
                    if (Parameters.GrowerOrderGuid != null && Parameters.GrowerOrderGuid != Guid.Empty)
                    {


                        var personTableService = new PersonTableService();
                        var personTableService2 = personTableService.GetAllByField("userGuid", userGuid.ToString(), "=", false);

                        Guid sPersonGuid = new Guid();

                        foreach (var person in personTableService2)
                        {

                            sPersonGuid = person.Guid;
                        }
                    


                        var growerOrderHistoryTableService = new GrowerOrderHistoryTableService();
                        var theEvent = new DataObjectLibrary.GrowerOrderHistory();
                        theEvent.PersonGuid = sPersonGuid;
                        theEvent.Comment = "Order placed.";
                        theEvent.GrowerOrderGuid = growerOrderGuid;
                        theEvent.LogTypeLookup = LookupTableValues.Logging.LogType.Grower.GrowerOrderPlace;
                        theEvent.LogTypeLookupGuid = LookupTableValues.Logging.LogType.Grower.GrowerOrderPlace.Guid;
                        theEvent.EventDate = DateTime.Now;
                        var growerOrderHistory = growerOrderHistoryTableService.Insert
                            (theEvent);



                    }

                    transaction.Commit();
                }
                catch
                { 
                    transaction.Rollback(); 
                }
                finally
                {
                    connection.Close();
                }
            }

            returnData = new ReturnData();

            if (Parameters.OrderLinesChanged != null)
            {
                returnData.OrderLinesChanged = (int)Parameters.OrderLinesChanged;
            }

            if (Parameters.OrderLinesDeleted != null)
            {
                returnData.OrderLinesDeleted = (int) Parameters.OrderLinesDeleted;
            }

            return returnData;
        }

        public ReturnData UnnotifyGrowerFORTESTINGPURPOSESONLY(Guid userGuid, string userCode, Guid growerOrderGuid)
        {
            ReturnData returnData = null;

            ChangeOrderLineStatuses(userGuid, userCode, growerOrderGuid, Guid.Empty, Guid.Empty, LookupTableValues.Code.OrderLineStatus.GrowerNotified, LookupTableValues.Code.OrderLineStatus.Ordered);

            returnData = new ReturnData();

            if (Parameters.OrderLinesDeleted != null)
            {
                returnData.OrderLinesDeleted = (int)Parameters.OrderLinesDeleted;
            }

            return returnData;
        }

        public ReturnData DeleteCartOrder(Guid userGuid, string userCode, Guid growerOrderGuid)
        {
            return DeleteOrder(userGuid, userCode, growerOrderGuid, LookupTableValues.Code.OrderLineStatus.Pending);
        }

        public ReturnData DeletePlacedOrderFORTESTINGPURPOSESONLY(Guid userGuid, string userCode, Guid growerOrderGuid)
        {
            return DeleteOrder(userGuid, userCode, growerOrderGuid, LookupTableValues.Code.OrderLineStatus.Ordered);
        }

        private ReturnData DeleteOrder(Guid userGuid, string userCode, Guid growerOrderGuid, Lookup orderLineStatusLookup)
        {
            ReturnData returnData = null;

            ChangeOrderLineStatuses(userGuid, userCode, growerOrderGuid, Guid.Empty, Guid.Empty, orderLineStatusLookup, orderLineStatusLookup, orderLineStatusLookup);

            returnData = new ReturnData();

            if (Parameters.OrderLinesDeleted != null)
            {
                returnData.OrderLinesDeleted = (int)Parameters.OrderLinesDeleted;
            }

            return returnData;
        }

        [Obsolete("Use ChangeOrderLineStatuses() directly.")]
        public ReturnData GrowerNotified(Guid userGuid, Guid growerOrderGuid, Guid orderLineGuid)
        {
            ReturnData returnData = null;

            ChangeOrderLineStatuses(userGuid, null, growerOrderGuid, Guid.Empty, orderLineGuid, LookupTableValues.Code.OrderLineStatus.Ordered, LookupTableValues.Code.OrderLineStatus.GrowerNotified);

            returnData = new ReturnData();

            if (Parameters.OrderLinesChanged != null)
            {
                returnData.OrderLinesChanged = (int)Parameters.OrderLinesChanged;
            }

            if (Parameters.OrderLinesDeleted != null)
            {
                returnData.OrderLinesDeleted = (int)Parameters.OrderLinesDeleted;
            }

            return returnData;
        }

        [Obsolete("Use ChangeOrderLineStatuses() directly.")]
        public ReturnData SupplierNotified(Guid userGuid, Guid supplierOrderGuid, Guid orderLineGuid)
        {
            ReturnData returnData = null;

            ChangeOrderLineStatuses(userGuid, null, Guid.Empty, supplierOrderGuid, orderLineGuid, LookupTableValues.Code.OrderLineStatus.GrowerNotified, LookupTableValues.Code.OrderLineStatus.SupplierNotified);

            returnData = new ReturnData();

            if (Parameters.OrderLinesChanged != null)
            {
                returnData.OrderLinesChanged = (int)Parameters.OrderLinesChanged;
            }

            if (Parameters.OrderLinesDeleted != null)
            {
                returnData.OrderLinesDeleted = (int)Parameters.OrderLinesDeleted;
            }

            return returnData;
        }

        [Obsolete("Use ChangeOrderLineStatuses() directly.")]
        public ReturnData SupplierConfirmed(Guid userGuid, Guid supplierOrderGuid, Guid orderLineGuid)
        {
            ReturnData returnData = null;

            ChangeOrderLineStatuses(userGuid, null, Guid.Empty, supplierOrderGuid, orderLineGuid, LookupTableValues.Code.OrderLineStatus.SupplierNotified, LookupTableValues.Code.OrderLineStatus.SupplierConfirmed);

            returnData = new ReturnData();

            if (Parameters.OrderLinesChanged != null)
            {
                returnData.OrderLinesChanged = (int)Parameters.OrderLinesChanged;
            }

            if (Parameters.OrderLinesDeleted != null)
            {
                returnData.OrderLinesDeleted = (int)Parameters.OrderLinesDeleted;
            }

            return returnData;
        }

        [Obsolete("Use ChangeOrderLineStatuses() directly.")]
        public ReturnData GrowerNotifiedAfterSupplierConfirmed(Guid userGuid, Guid growerOrderGuid, Guid supplierOrderGuid, Guid orderLineGuid)
        {
            ReturnData returnData = null;

            ChangeOrderLineStatuses(userGuid, null, growerOrderGuid, supplierOrderGuid, orderLineGuid, LookupTableValues.Code.OrderLineStatus.SupplierConfirmed, LookupTableValues.Code.OrderLineStatus.GrowerNotifiedAfterSupplierConfirmed);

            returnData = new ReturnData();

            if (Parameters.OrderLinesChanged != null)
            {
                returnData.OrderLinesChanged = (int)Parameters.OrderLinesChanged;
            }

            if (Parameters.OrderLinesDeleted != null)
            {
                returnData.OrderLinesDeleted = (int)Parameters.OrderLinesDeleted;
            }

            return returnData;
        }

        [Obsolete("Use ChangeOrderLineStatuses() directly.")]
        public ReturnData Shipped(Guid userGuid, Guid growerOrderGuid, Guid supplierOrderGuid, Guid orderLineGuid)
        {
            ReturnData returnData = null;



            List<Lookup> fromStatusLookupList =  new List<Lookup> ();
            fromStatusLookupList.Add(LookupTableValues.Code.OrderLineStatus.GrowerNotifiedAfterSupplierConfirmed);
           


            ChangeOrderLineStatuses(userGuid, null, growerOrderGuid, supplierOrderGuid, orderLineGuid,
                                    fromStatusLookupList, LookupTableValues.Code.OrderLineStatus.Shipped, null, null, null);

  


            returnData = new ReturnData();

            if (Parameters.OrderLinesChanged != null)
            {
                returnData.OrderLinesChanged = (int)Parameters.OrderLinesChanged;
            }

            if (Parameters.OrderLinesDeleted != null)
            {
                returnData.OrderLinesDeleted = (int)Parameters.OrderLinesDeleted;
            }

            //Added this to handle the supplier cancelled status.  should probably be in the return data, maybe do later.
            List<Lookup> fromStatusLookupListCancelled = new List<Lookup>();

            fromStatusLookupListCancelled.Add(LookupTableValues.Code.OrderLineStatus.SupplierCancelled);
            fromStatusLookupListCancelled.Add(LookupTableValues.Code.OrderLineStatus.GrowerCancelled);
            ChangeOrderLineStatuses(userGuid, null, growerOrderGuid, supplierOrderGuid, orderLineGuid,
                                    fromStatusLookupListCancelled, LookupTableValues.Code.OrderLineStatus.Cancelled, null, null, null);


            return returnData;
        }

        public ReturnData ChangeOrderLineStatuses(Guid userGuid, string userCode, Guid growerOrderGuid,
                                                  Guid supplierOrderGuid, Guid orderLineGuid, Lookup fromStatusLookup,
                                                  Lookup toStatusLookup, Lookup deleteStatusLookup = null,
                                                  Lookup logTypeLookup = null, string logMessage = null)
        {
            List<Lookup> fromStatusLookupList = null;

            if (fromStatusLookup != null)
            {
                fromStatusLookupList = new List<Lookup> {fromStatusLookup};
            }

            return ChangeOrderLineStatuses(userGuid, userCode, growerOrderGuid, supplierOrderGuid, orderLineGuid,
                                    fromStatusLookupList, toStatusLookup, deleteStatusLookup, logTypeLookup, logMessage);
        }

        public ReturnData ChangeOrderLineStatuses(Guid userGuid, string userCode, Guid growerOrderGuid, Guid supplierOrderGuid, Guid orderLineGuid, IEnumerable<Lookup> fromStatusLookups, Lookup toStatusLookup, Lookup deleteStatusLookup = null, Lookup logTypeLookup = null, string logMessage = null)
        {
            ReturnData returnData;

            var storedProcedureServices = new StoredProcedureServices();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                
                try
                {
                     returnData =
                        ChangeOrderLineStatusesInTransaction
                            (
                                transaction,
                                userGuid,
                                userCode,
                                growerOrderGuid,
                                supplierOrderGuid,
                                orderLineGuid,
                                fromStatusLookups,
                                toStatusLookup,
                                deleteStatusLookup,
                                logTypeLookup,
                                logMessage
                            );

                    transaction.Commit();
                }
                catch(Exception ex)
                {
                    Console.Write(ex.Message);
                    transaction.Rollback();
                    returnData = null;
                }
                finally
                {
                    connection.Close();
                }
            }

            return returnData;
        }

        public ReturnData ChangeOrderLineStatusesInTransaction(SqlTransaction transaction, Guid userGuid, string userCode, Guid growerOrderGuid,
                                                  Guid supplierOrderGuid, Guid orderLineGuid, Lookup fromStatusLookup,
                                                  Lookup toStatusLookup, Lookup deleteStatusLookup = null,
                                                  Lookup logTypeLookup = null, string logMessage = null, string supplierOrGrower = "Grower")
        {
            List<Lookup> fromStatusLookupList = null;

            if (fromStatusLookup != null)
            {
                fromStatusLookupList = new List<Lookup> { fromStatusLookup };
            }
            //else
            //{  fromStatusLookupList = new List<Lookup>(); }

            return ChangeOrderLineStatusesInTransaction(transaction, userGuid, userCode, growerOrderGuid, supplierOrderGuid, orderLineGuid,
                                    fromStatusLookupList, toStatusLookup, deleteStatusLookup, logTypeLookup, logMessage, supplierOrGrower);
        }

        public ReturnData ChangeOrderLineStatusesInTransaction(SqlTransaction transaction, Guid userGuid, string userCode, Guid growerOrderGuid, Guid supplierOrderGuid, Guid orderLineGuid, IEnumerable<Lookup> fromStatusLookups, Lookup toStatusLookup, Lookup deleteStatusLookup = null, Lookup logTypeLookup = null, string logMessage = null, string supplierOrGrower = "Grower")
        {
            Guid logGuid = Guid.Empty;
            int guidCount = 0;
            if (growerOrderGuid != Guid.Empty)
            {
                Parameters.GrowerOrderGuid = growerOrderGuid;
                logGuid = growerOrderGuid;
                guidCount++;
            }
            if (supplierOrderGuid != Guid.Empty)
            {
                Parameters.SupplierOrderGuid = supplierOrderGuid;
                logGuid = supplierOrderGuid;
                guidCount++;
            }
            if (orderLineGuid != Guid.Empty)
            {
                Parameters.OrderLineGuid = orderLineGuid;
                logGuid = orderLineGuid;
                guidCount++;
            }
            if (guidCount != 1)
            {
                throw new ArgumentException(string.Format("One and only one of GrowerOrderGuid, SupplierOrderGuid or OrderLineGuid can be set (in this case {0} are set).", guidCount));
            }
            List<Lookup> fromStatusLookupList = new List<Lookup>();
            if (fromStatusLookups != null)
            {
                fromStatusLookupList = fromStatusLookups.ToList();
           
                if (fromStatusLookups == null && orderLineGuid == Guid.Empty)
                {
                    throw new ArgumentNullException("fromStatusLookups", "The fromStatusLookup cannot be null unless the orderLineGuid is specified.");
                }
            }
            else
            {
                foreach (var fromStatusLookup in fromStatusLookupList)
                {
                    if (fromStatusLookup != null && fromStatusLookup.ParentLookup.Guid != LookupTableValues.Code.OrderLineStatus.LookupValue.Guid)
                    {
                        throw new ArgumentException(string.Format("The lookup \"{0}\" is not a valid OrderLineStatusLookup.", fromStatusLookup.Path));
                    }
                }
            }

            if (toStatusLookup == null)
            {
                throw new ArgumentNullException("toStatusLookup");
            }
            else if (toStatusLookup.ParentLookup.Guid != LookupTableValues.Code.OrderLineStatus.LookupValue.Guid)
            {
                throw new ArgumentException(string.Format("The lookup \"{0}\" is not a valid OrderLineStatusLookup.", toStatusLookup.Path));
            }

            Parameters.UserGuid = userGuid;
            Parameters.UserCode = userCode;

            Parameters.FromOrderLineStatusCodeList = ToCommaSeparatedList(fromStatusLookupList.Select(lookup => lookup.Code).ToArray());

            Parameters.ToOrderLineStatusCode = toStatusLookup.Code;

            if (deleteStatusLookup != null)
                Parameters.DeleteOrderLinesWithOrderLineStatusCode = deleteStatusLookup.Code;

            var storedProcedureServices = new StoredProcedureServices();

            using (var reader = storedProcedureServices.ReaderFromSproc(transaction.Connection, this, transaction: transaction))
            {
                reader.Close();
            }

            storedProcedureServices.GetOutputParameterValues();

            if (logTypeLookup != null)
            {
                if (logTypeLookup.Code == "GrowerOrderPlace")
                {
                    var personTableService = new PersonTableService();
                    var personTableService2 = personTableService.GetAllByField("userGuid", userGuid.ToString(), "=", false);
                    Guid sPersonGuid = new Guid();
                    foreach (var person in personTableService2)
                    {
                        sPersonGuid = person.Guid;
                    }
                    var growerOrderHistoryTableService = new GrowerOrderHistoryTableService();
                    var theEvent = new DataObjectLibrary.GrowerOrderHistory();
                    theEvent.PersonGuid = sPersonGuid;
                    theEvent.Comment = "Order placed.";
                    theEvent.GrowerOrderGuid = growerOrderGuid;
                    theEvent.LogTypeLookup = LookupTableValues.Logging.LogType.Grower.GrowerOrderPlace;
                    theEvent.LogTypeLookupGuid = LookupTableValues.Logging.LogType.Grower.GrowerOrderPlace.Guid;
                    theEvent.EventDate = DateTime.Now;
                    var growerOrderHistory = growerOrderHistoryTableService.Insert
                        (theEvent);
                }
                else
                {
                    var eventLogTableService = new EventLogTableService();
                    eventLogTableService.InsertGrowerOrderLog
                    (
                        userGuid,
                        userCode,
                        "ChangeStatus",
                        logTypeLookup,
                        logMessage,
                        logGuid,
                        transaction
                    );
                }
             }


            storedProcedureServices.GetOutputParameterValues();

            var returnData = new ReturnData();

            if (Parameters.OrderLinesChanged != null)
            {
                returnData.OrderLinesChanged = (int)Parameters.OrderLinesChanged;
            }

            if (Parameters.OrderLinesDeleted != null)
            {
                returnData.OrderLinesDeleted = (int)Parameters.OrderLinesDeleted;
            }

            return returnData;
        }
    }

    


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary;
using DataObjectLibrary;
using System.Data.SqlClient;
using System.Data;
using BusinessObjectsLibrary;
namespace DataServiceLibrary
{
    public partial class PersonDataService: DataServiceBaseNew
    {
        public class PersonData
        {
            public DataObjectLibrary.Person Person { get; set; }
            public DataObjectLibrary.Grower Grower { get; set; }
        }

        public PersonDataService(StatusObject status)
            : base(status)
        {

        }
        public PersonData RegisterPerson
            (
                string companyName,
                string streetAddress1,
                string streetAddress2,
                string city,
                string stateCode,
                string zipCode,
                string companyEmail,
                string companyPhone,
                string companyTypeCode,
                string firstName,
                string lastName,
                string personEmail,
                string password,
                string personPhone,
                string roleCode,
                bool isAuthorizedToPlaceOrders,
                bool subscribeToNewsletter,
                bool subscribeToPromotions ,
                bool delete
            )
        {
            PersonData personData = null;
            var companyPhoneObject = new Utility.PhoneNumber(companyPhone);
            var growerPhoneAreaCode = companyPhoneObject.AreaCode;
            var growerPhoneNumber = companyPhoneObject.LocalPhoneNumber;


            var personPhoneObject = new Utility.PhoneNumber(personPhone);
            var personPhoneAreaCode = personPhoneObject.AreaCode;
            var personPhoneNumber = personPhoneObject.LocalPhoneNumber;

            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            
            try
            {

                var command = new SqlCommand("[dbo].[PersonRegister]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@CompanyName", SqlDbType.NVarChar, 50))
                    .Value = companyName;
                command.Parameters
                    .Add(new SqlParameter("@StreetAddress1", SqlDbType.NVarChar, 50))
                    .Value = streetAddress1;
                command.Parameters
                    .Add(new SqlParameter("@StreetAddress2", SqlDbType.NVarChar, 50))
                    .Value = streetAddress2;
                command.Parameters
                    .Add(new SqlParameter("@City", SqlDbType.NVarChar, 50))
                    .Value = city;
                command.Parameters
                    .Add(new SqlParameter("@StateCode", SqlDbType.NVarChar, 2))
                    .Value = stateCode;
                command.Parameters
                    .Add(new SqlParameter("@GrowerEmail", SqlDbType.NVarChar, 50))
                    .Value = companyEmail;
                command.Parameters
                    .Add(new SqlParameter("@ZipCode", SqlDbType.NVarChar, 12))
                    .Value = zipCode;
                command.Parameters
                    .Add(new SqlParameter("@GrowerPhoneAreaCode", SqlDbType.Decimal, 5))
                    .Value = growerPhoneAreaCode;
                command.Parameters
                    .Add(new SqlParameter("@GrowerPhoneNumber", SqlDbType.Decimal, 5))
                    .Value = growerPhoneNumber;
                command.Parameters
                    .Add(new SqlParameter("@GrowerTypeLookupCode", SqlDbType.NVarChar, 20))
                    .Value = companyTypeCode;
                command.Parameters
                    .Add(new SqlParameter("@FirstName", SqlDbType.NVarChar, 50))
                    .Value = firstName;
                command.Parameters
                    .Add(new SqlParameter("@LastName", SqlDbType.NVarChar, 50))
                    .Value = lastName;
                command.Parameters
                    .Add(new SqlParameter("@PersonEmail", SqlDbType.NVarChar, 50))
                    .Value = personEmail;
                command.Parameters
                    .Add(new SqlParameter("@Password", SqlDbType.NVarChar, 50))
                    .Value = password;
                command.Parameters
                    .Add(new SqlParameter("@PersonPhoneAreaCode", SqlDbType.Decimal, 5))
                    .Value = personPhoneAreaCode;
                command.Parameters
                    .Add(new SqlParameter("@PersonPhoneNumber", SqlDbType.Decimal, 5))
                    .Value = personPhoneNumber;
                command.Parameters
                    .Add(new SqlParameter("@PersonTypeLookupCode", SqlDbType.NVarChar, 50))
                    .Value = roleCode;
                command.Parameters
                    .Add(new SqlParameter("@IsAuthorizedToPlaceOrders", SqlDbType.Bit, 1))
                    .Value = isAuthorizedToPlaceOrders;
                command.Parameters
                    .Add(new SqlParameter("@SubscribeToNewsLetter", SqlDbType.Bit, 1))
                    .Value = subscribeToNewsletter;
                command.Parameters
                    .Add(new SqlParameter("@SubscribeToPromotions", SqlDbType.Bit, 1))
                    .Value = subscribeToPromotions;
                command.Parameters
                   .Add(new SqlParameter("@PersonGuid", SqlDbType.UniqueIdentifier))
                   .Direction = ParameterDirection.Output;
                command.Parameters
                   .Add(new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier))
                   .Direction = ParameterDirection.Output;
                command.Parameters
                   .Add(new SqlParameter("@GrowerAddressGuid", SqlDbType.UniqueIdentifier))
                   .Direction = ParameterDirection.Output;
                command.Parameters
                   .Add(new SqlParameter("@AddressGuid", SqlDbType.UniqueIdentifier))
                   .Direction = ParameterDirection.Output;
                command.Parameters
                   .Add(new SqlParameter("@GrowerPersonTripleGuid", SqlDbType.UniqueIdentifier))
                   .Direction = ParameterDirection.Output;
                command.Parameters
                    .Add(new SqlParameter("@Delete", SqlDbType.Bit, 1))
                    .Value = delete;

                var reader = command.ExecuteReader();
                var context = new Context();

                personData = GetPersonDataFromReader(context, reader);
                
                
                //promoName = (string)command.Parameters["@PromoName"].Value;


            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CartService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;
            }
            finally
            {
                connection.Close();
            }


            return personData;
        }

        


        

        public PersonData GetData(Guid personGuid)
        {
            PersonData personData = null;
            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[PersonDataGet]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@PersonGuid", SqlDbType.UniqueIdentifier))
                    .Value = personGuid;




                var reader = command.ExecuteReader();
                var context = new Context();

                personData = GetPersonDataFromReader(context, reader);

                Status.Success = true;
                Status.StatusMessage = "Person Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "PesronService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }


            return personData;
        }

        internal static PersonData GetPersonDataFromReader(Context context, System.Data.SqlClient.SqlDataReader reader)
        {
            var personData = new PersonData();

            var personTableService = context.Get<PersonTableService>();

            var personList = personTableService.GetAllFromReader(reader);
            if (personList.Count > 0)
            {
                personData.Person = personList[0];
            }

            reader.NextResult();

            var growerTableService = context.Get<GrowerTableService>();
            var growerList = growerTableService.GetAllFromReader(reader);

            reader.NextResult();

            var growerAddressTableService = context.Get<GrowerAddressTableService>();
            var growerAddressList = growerAddressTableService.GetAllFromReader(reader);

            // There will only be one Grower. Replace its Address lazy load with the growerAddressList from above.
            if (growerList.Count == 1)
            {
                personData.Grower = growerList[0];

                personData.Grower.SetLazyGrowerAddressList(new Lazy<List<GrowerAddress>>(() => growerAddressList));
            }

            reader.NextResult();

            var addressTableService = context.Get<AddressTableService>();
            addressTableService.GetAllFromReader(reader);

            reader.NextResult();

            var stateTableService = context.Get<StateTableService>();
            stateTableService.GetAllFromReader(reader);

            reader.NextResult();

            var countryTableService = context.Get<CountryTableService>();
            countryTableService.GetAllFromReader(reader);

            reader.NextResult();

            var lookupTableService = context.Get<LookupTableService>();
            var lookupList = lookupTableService.GetAllFromReader(reader);

            return personData;
        }

    }
}

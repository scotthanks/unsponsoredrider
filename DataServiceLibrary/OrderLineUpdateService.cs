﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataServiceLibrary
{
    public class UpdateStatus
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }

    public class OrderLineUpdateService
    {
        public UpdateStatus UpdatePriceAndCost(Guid UserGuid, Guid OrderLineGuid, string UserCode, decimal Cost, decimal Price)
        {
            UpdateStatus response = new UpdateStatus();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("OrderLineTransaction");

                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText = string.Format(@"
                    Update OrderLine 
                    set ActualCost = '{0}', 
                    ActualPrice = '{1}' 
                    where Guid = '{2}'",
                    Cost,
                    Price,
                    OrderLineGuid);

                    if (command.ExecuteNonQuery() != 1)
                    {
                        throw new Exception(string.Format("Sql command: {0} failed.", command.CommandText));
                    }

                    LogOrderLineUpdate(UserGuid, UserCode, OrderLineGuid, transaction);

                    // Attempt to commit the transaction.
                    transaction.Commit();
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    response.Success = false;
                    response.Message = ex.Message;
                }
                finally
                { 
                    connection.Close();
                }
            }

            return response;
        }

        public UpdateStatus UpdateQuanityShipped(Guid UserGuid, Guid OrderLineGuid, string UserCode, decimal QuantityShipped)
        {
            UpdateStatus response = new UpdateStatus();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("OrderLineTransaction");

                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText = string.Format(@"
                    Update OrderLine 
                    set QtyShipped = '{0}'  
                    where Guid = '{1}'",
                    QuantityShipped,
                    OrderLineGuid);

                    if (command.ExecuteNonQuery() != 1)
                    {
                        throw new Exception(string.Format("Sql command: {0} failed.", command.CommandText));
                    }

                    LogOrderLineUpdate(UserGuid, UserCode, OrderLineGuid, transaction);

                    // Attempt to commit the transaction.
                    transaction.Commit();
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    response.Success = false;
                    response.Message = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }

            return response;
        }

        private void LogOrderLineUpdate(Guid UserGuid, string UserCode, Guid OrderLineGuid, SqlTransaction transaction )
        {
           
            
            var eventlogService = new EventLogTableService();

            var eventLog = eventlogService.Insert
                (
                    userGuid: UserGuid,
                    userCode: UserCode,
                    processLookupCode: "UpdatePriceAndCost",
                    comment: "Admin App user updated order line Price and or Cost and or Quantity Shipped",
                    logTypeLookup: LookupTableValues.Logging.LogType.TableRowUpdate,
                    severityLookup: LookupTableValues.Logging.Severity.Info,
                    priorityLookup: LookupTableValues.Logging.Priority.Medium,
                    verbosityLookup: LookupTableValues.Logging.Verbosity.Minimal,
                    objectTypeLookup: LookupTableValues.Logging.ObjectType.OrderLine,
                    objectGuid: OrderLineGuid,
                    intValue: null, processId: null, floatValue: null, guidValue: Guid.NewGuid(), xmlData: null,
                    transaction: transaction
               );
        }

    }
}

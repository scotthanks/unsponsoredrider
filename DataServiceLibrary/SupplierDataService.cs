﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;
using DataAccessLibrary;
using System;
using BusinessObjectsLibrary;
using System.Data.SqlClient;
using System.Data;

namespace DataServiceLibrary
{
    public partial class SupplierDataService: DataServiceBaseNew
    {
        public SupplierDataService(StatusObject status)
            : base(status)
        {

        }

        public List<DataObjectLibrary.Supplier> GetSuppliers(string sellerCode,string programTypeCode = null, string productFormCategoryCode = null, string[] geneticOwnerCodes = null, string[] speciesCodes = null)
        {
            sellerCode = "GFB";
            List<DataObjectLibrary.Supplier> supplierList = null;
            
            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {

                var command = new SqlCommand("[dbo].[SupplierDataGet]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                   .Add(new SqlParameter("@SellerCode", SqlDbType.NChar, 10))
                   .Value = sellerCode;        
                command.Parameters
                    .Add(new SqlParameter("@ProgramTypeCode", SqlDbType.NChar, 50))
                    .Value = programTypeCode;
                command.Parameters
                    .Add(new SqlParameter("@ProductFormCategoryCode", SqlDbType.NVarChar, 50))
                    .Value = productFormCategoryCode;
                command.Parameters
                    .Add(new SqlParameter("@GeneticOwnerCodeList", SqlDbType.NVarChar, 500))
                    .Value = ToCommaSeparatedList(geneticOwnerCodes);
                command.Parameters
                    .Add(new SqlParameter("@SpeciesCodeList", SqlDbType.NVarChar, 500))
                    .Value = ToCommaSeparatedList(speciesCodes);
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
            

                var reader = (SqlDataReader)command.ExecuteReader();
                
                var context = new Context();

                var supplierTableService = context.Get<SupplierTableService>();
                supplierList = supplierTableService.GetAllFromReader(reader);     
               
               


            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "SupplierService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;
            }
            finally
            {
                connection.Close();

            }
    
            
            
            
          

            return supplierList;
        }
        protected string ToCommaSeparatedList(string[] codes)
        {
            if (codes == null)
            {
                return "*";
            }
            else
            {
                var list = new StringBuilder();
                foreach (string code in codes)
                {
                    if (list.Length > 0)
                        list.Append(',');

                    list.Append(code);
                }

                return list.ToString();
            }
        }

    }
}

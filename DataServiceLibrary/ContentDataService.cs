﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary;
using DataObjectLibrary;
using DataServiceLibrary;
using BusinessObjectsLibrary;

namespace DataServiceLibrary
{
    public class ContentDataService: DataServiceBaseNew
    {
        public ContentDataService(StatusObject status)
            : base(status)
        {

        }
    
       

  

        public string GetDynamicContentHtml(Guid guid)
        {
            var procedure = new GetDynamicContentFromGuidProcedure { Parameters = { Guid = guid } };

            return GetDynamicContent(procedure);
        }

        // Note: This method is currently identical to the one above, but over time is expected to diverge, which is why it is separate.
        private string GetDynamicContent(ProcedureBase procedure)
        {
            string content = "";

            var storedProcedureServices = new StoredProcedureServices();
            using (var connection = ConnectionServices.ConnectionToData)
            {
                try
                {
                    connection.Open();

                    using (var reader = storedProcedureServices.ReaderFromSproc(connection, procedure))
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();

                            var contentData = GetContentFromReader(reader);

                            if (contentData != null)
                            {
                                content = contentData.ContentHTML;
                            }
                        }
                    }
                }
                finally
                {
                    connection.Close();
                }
            }

            return content;
        }

        protected Content GetContentFromReader(SqlDataReader reader)
        {
            var content = new Content();

            string columnName = "(undefined)";
            try
            {
                columnName = "Id";
                var id = reader[columnName] ?? Int64.MinValue;
                content.Id = (long)id;

                columnName = "Guid";
                var guid = reader[columnName] ?? Guid.Empty;
                content.Guid = (Guid)guid;

                columnName = "ContentHtml";
                var contentHtml = reader[columnName];
                if (contentHtml == DBNull.Value) contentHtml = null;
                content.ContentHTML = (string)contentHtml;


                //Rick - I added this new field, we also need one for PageCSSGuid
                columnName = "Css";
                var css = reader[columnName];
                if (css == DBNull.Value) css = null;
                content.CSS = (string)css;

                columnName = "IsLive";
                var isLive = reader[columnName] ?? false;
                content.IsLive = (bool)isLive;

                columnName = "Path";
                var path = reader[columnName];
                if (path == DBNull.Value) path = null;
                content.Path = (string)path;

                columnName = "Description";
                var description = reader[columnName];
                if (description == DBNull.Value) description = null;
                content.Description = (string)description;

                columnName = "Type";
                var type = reader[columnName];
                if (type == DBNull.Value) type = null;
                content.Type = (string)type;
            }
            catch (Exception e)
            {
                throw new ApplicationException(String.Format("An exception has occurred while processing the reader on field \"{0}\" ({1}).", columnName, e.Message), e);
            }

            return content;
        }
    }
}

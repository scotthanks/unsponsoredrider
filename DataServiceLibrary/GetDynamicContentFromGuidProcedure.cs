﻿using System;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public class GetDynamicContentFromGuidProcedure : Procedure<GetDynamicContentFromGuidProcedure.GetDynamicContentByGuidParameters>
    {
        public GetDynamicContentFromGuidProcedure()
        {
            ProcedureName = "GetDynamicContentFromGuid";
        }

        public class GetDynamicContentByGuidParameters : ProcedureParameterBase
        {
            public Guid Guid { get; set; }

            public override void AddTo(SqlCommand command)
            {
                var contentGuidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier);
                contentGuidParameter.Value = Guid;
                command.Parameters.Add(contentGuidParameter);
            }

            public override void GetOutputValuesFrom(SqlCommand command)
            {
            }
        };
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BusinessObjectsLibrary;
using System.Data;

namespace DataServiceLibrary
{
    public class SupplierOrderService: DataServiceBaseNew
    {
        public SupplierOrderService(StatusObject status)
            : base(status)
        {

        }
    
        public Guid userGuid { get; set; }
        public string shipWeekCode { get; set; }
        public string programTypeCode { get; set; }
        public string productFormCategoryCode { get; set; }
      
        public UpdateStatus UpdateTrackingData(Guid UserGuid, Guid SupplierOrderGuid, string UserCode, string TrackingNumber, string TrackingComment,DateTime ExpectedDeliveryDate)
        {
            UpdateStatus response = new UpdateStatus();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("SupplierOrderTransaction");

                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText = string.Format(@"
                    Update SupplierOrder 
                    set ExpectedDeliveredDate = '{0}', 
                    TrackingNumber = '{1}' ,
                    TrackingComment = '{2}' 
                    where Guid = '{3}'",
                    ExpectedDeliveryDate,
                    TrackingNumber,
                    TrackingComment,
                    SupplierOrderGuid);

                    if (command.ExecuteNonQuery() != 1)
                    {
                        throw new Exception(string.Format("Sql command: {0} failed.", command.CommandText));
                    }

                    LogSupplierOrderUpdate(UserGuid, UserCode, SupplierOrderGuid, transaction);

                    // Attempt to commit the transaction.
                    transaction.Commit();
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    response.Success = false;
                    response.Message = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }

            return response;
        }

        public void RecalculateFeesbySupplierOrderGuid(Guid supplierOrderGuid)
        {
           SupplierOrderRecalcFees(supplierOrderGuid);
        }

        public void RecalculateFees(Guid userGuid, string shipWeekCode, string programTypeCode, string productFormCategoryCode)
        {
           // var SupplierOrderUpdateService = new SupplierOrderUpdateService();
            //SupplierOrderUpdateService.userGuid = userGuid;
            //SupplierOrderUpdateService.shipWeekCode = shipWeekCode;
           // SupplierOrderUpdateService.programTypeCode = programTypeCode;
            //SupplierOrderUpdateService.productFormCategoryCode = productFormCategoryCode;


            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {

                try
                {
                    //This grabs the order for the purpose of recalculating fees
                    var status = new StatusObject(userGuid, true);
                    var growerOrderDataService = new GrowerOrderDataService(status);
                    Guid growerOrderGuid = growerOrderDataService.GrowerOrderGetGuid(shipWeekCode, programTypeCode, productFormCategoryCode, false);

                    //For Green Fuse, we need to reprice before recalculating to get the price record guid on the order line
                    
                    growerOrderDataService.GrowerOrderRecalcFees(growerOrderGuid, true, true);


                }
                catch (Exception ex)
                {

                }
                finally
                {

                }
            }



          
        }
       



        private void LogSupplierOrderUpdate(Guid UserGuid, string UserCode, Guid SupplierOrderGuid, SqlTransaction transaction)
        {
            var eventlogService = new EventLogTableService();

            var eventLog = eventlogService.Insert
                (
                    userGuid: UserGuid,
                    userCode: UserCode,
                    processLookupCode: "UpdateTrackingData",
                    comment: "Admin App user updated SupplierOrder deliver date and tracking number.",
                    logTypeLookup: LookupTableValues.Logging.LogType.TableRowUpdate,
                    severityLookup: LookupTableValues.Logging.Severity.Info,
                    priorityLookup: LookupTableValues.Logging.Priority.Medium,
                    verbosityLookup: LookupTableValues.Logging.Verbosity.Minimal,
                    objectTypeLookup: LookupTableValues.Logging.ObjectType.SupplierOrder,
                    objectGuid: SupplierOrderGuid,
                    intValue: null, processId: null, floatValue: null, guidValue: Guid.NewGuid(), xmlData: null,
                    transaction: transaction
               );
        }


        public void SupplierOrderRecalcFees(Guid supplierOrderGuid)
        {


            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();

            try
            {
                var command = new SqlCommand("[dbo].[SupplierOrderRecalcFees]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@UserCode", SqlDbType.NChar, 56))
                    .Value = null;
                command.Parameters
                    .Add(new SqlParameter("@SupplierOrderGuid", SqlDbType.UniqueIdentifier))
                    .Value = supplierOrderGuid;



                var response = (int)command.ExecuteNonQuery();



            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "SupplierOrderRecalcFees",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;

            }
            finally
            {
                connection.Close();
            }

        }

        public bool UpdateOrder(Guid supplierOrderGuid, string tagRatioLookupCode, string shipMethodLookupCode)
        {
            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {

                var command = new SqlCommand("[dbo].[SupplierOrderUpdate]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@SupplierOrderGuid", SqlDbType.UniqueIdentifier))
                    .Value = supplierOrderGuid;
                command.Parameters
                    .Add(new SqlParameter("@TagRatioLookupCode", SqlDbType.NChar, 20))
                    .Value = tagRatioLookupCode;
                command.Parameters
                    .Add(new SqlParameter("@ShipMethodLookupCode", SqlDbType.NChar, 20))
                    .Value = shipMethodLookupCode; 
                command.Parameters
                    .Add(new SqlParameter("@Success", SqlDbType.Bit, 1))
                    .Direction = ParameterDirection.Output;

                


                var response = (int)command.ExecuteNonQuery();
                Status.Success  = (bool)command.Parameters["@Success"].Value;
                Status.StatusMessage = "Supplier Order Updated.";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "UpdateOrder",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return false;
            }
            finally
            {

                connection.Close();
            }


            return Status.Success;



        }

        public void Reprice(Guid supplierOrderGuid,bool onlyRepriceZero)
        {

            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {

                var command = new SqlCommand("[dbo].[SupplierOrderReprice]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                
                command.Parameters
                    .Add(new SqlParameter("@SupplierOrderGuid", SqlDbType.UniqueIdentifier))
                    .Value = supplierOrderGuid;
                  command.Parameters
                    .Add(new SqlParameter("@OnlyRepriceZero", SqlDbType.Bit))
                    .Value = onlyRepriceZero;
        


                var response = (int)command.ExecuteNonQuery();
               
                Status.StatusMessage = "Supplier Order Repriced.";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "Reprice",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                
            }
            finally
            {

                connection.Close();
            }

            Status.Success = true;
            
        }

        public List<DataObjectLibrary.SupplierOrderFee> GetFees(Guid supplierOrderGuid)
        {
             List<DataObjectLibrary.SupplierOrderFee> supplierOrderFeeList = null;
            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {

                var command = new SqlCommand("[dbo].[SupplierOrderGetFees]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };


                command.Parameters
                    .Add(new SqlParameter("@SupplierOrderGuid", SqlDbType.UniqueIdentifier))
                    .Value = supplierOrderGuid;




                var reader = command.ExecuteReader();
                var context = new Context();

                var supplierOrderFeeTableService = context.Get<SupplierOrderFeeTableService>();
                supplierOrderFeeList = supplierOrderFeeTableService.GetAllFromReader(reader);

                reader.Close();

                Status.StatusMessage = "Got Fees.";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "Reprice",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;

            }
            finally
            {

                connection.Close();
            }

            
            return supplierOrderFeeList;
           
                        
        }
    }
}

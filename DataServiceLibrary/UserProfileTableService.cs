﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

namespace DataServiceLibrary
{
	public partial class UserProfileTableService
	{
		public UserProfileTableService()
		{
		}

        public UserProfile Get(string userName)
        {
            const string sqlInsertCommand = "SELECT * FROM UserProfile WHERE UserName=@UserName";

            var userProfile = new UserProfile {UserName = userName};

            return TryGetOneFromCommand(sqlInsertCommand, userProfile);
        }

        public UserProfile GetByGuid(Guid userGuid)
        {
            const string sqlCommand = "SELECT * FROM UserProfile WHERE UserGuid=@UserGuid";

            var userProfile = new UserProfile { UserGuid = userGuid };

            return TryGetOneFromCommand(sqlCommand, userProfile);

        }

		public UserProfile Insert( UserProfile userProfile)
		{
            const string sqlInsertCommand = "INSERT INTO [UserProfile]([UserName], [EmailAddress], [UserGuid], [GrowerGuid]) VALUES (@UserName, @EmailAddress, @UserGuid, @GrowerGuid);SELECT * FROM [UserProfile] WHERE [UserGuid]=@UserGuid";

		    return TryGetOneFromCommand(sqlInsertCommand, userProfile);
		}

	    public UserProfile Update( UserProfile userProfile)
	    {
            const string sqlUpdateCommand = "UPDATE [UserProfile] SET [UserName]=@UserName, [EmailAddress]=@EmailAddress, [UserGuid]=@UserGuid, [GrowerGuid]=@GrowerGuid WHERE [UserId]=@UserId;SELECT * FROM [UserProfile] WHERE [UserId]=@UserId";

	        return TryGetOneFromCommand(sqlUpdateCommand, userProfile);
	    }

        public bool Delete(UserProfile userProfile)
        {
            bool success = false;

            //TODO: This does not work because if there are webpages_UsersInRoles that depend on this, which there will be, the delete will fail. Use a procedure.
            const string sqlDeleteCommand = "DELETE [UserProfile] WHERE [UserId]=@UserId;SELECT * FROM [UserProfile] WHERE [UserId]=@UserId";

            userProfile = TryGetOneFromCommand(sqlDeleteCommand, userProfile);

            if (userProfile == null)
            {
                success = true;
            }

            return success;
        }

	    public bool GrantRole(string userName, UserProfile.RoleEnum role)
	    {
	        return ExecuteRoleCommand(userName, role.ToString(), "RoleGrant");
	    }

        public bool RevokeRole(string userName, UserProfile.RoleEnum role)
        {
            return ExecuteRoleCommand(userName, role.ToString(), "RoleRevoke");
        }

        private bool ExecuteRoleCommand(string userName, string roleName, string procedureName)
        {
            var command = new SqlCommand(procedureName);
            command.CommandType = CommandType.StoredProcedure;

            var userNameParameter = new SqlParameter("@UserName", SqlDbType.NVarChar, 256);
            userNameParameter.IsNullable = false;
            userNameParameter.Value = userName;
            command.Parameters.Add(userNameParameter);

            var roleNameParameter = new SqlParameter("@RoleName", SqlDbType.NVarChar, 256);
            roleNameParameter.IsNullable = false;
            roleNameParameter.Value = roleName;
            command.Parameters.Add(roleNameParameter);

            var successParameter = new SqlParameter("@Success", SqlDbType.Bit);
            successParameter.IsNullable = true;
            successParameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(successParameter);

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToMembership)
            {
                command.Connection = connection;

                connection.Open();

                var reader = command.ExecuteReader();
                bool success = (bool)(successParameter.Value ?? false);

                connection.Close();

                return success;
            }
        }

	    private UserProfile TryGetOneFromCommand(string sqlCommand,  UserProfile userProfile)
        {
            UserProfile newUserProfile = null;

            var sqlQueryServices = new SqlQueryServices();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToMembership)
            {
                var command = new SqlCommand(sqlCommand, connection);

                AddUserProfileParameters(command, userProfile);

                connection.Open();

                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();

                    newUserProfile = LoadUserProfileFromSqlDataReader(reader);
                }

                connection.Close();
            }

            return newUserProfile;
        }

		private void AddUserProfileParameters( SqlCommand command, UserProfile userProfile)
		{
            var userIdParameter = new SqlParameter("@UserId", SqlDbType.Int);
            userIdParameter.IsNullable = false;
            userIdParameter.Value = userProfile.UserId;
            command.Parameters.Add(userIdParameter);

            var userGuidParameter = new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier, 16);
            userGuidParameter.IsNullable = false;
            userGuidParameter.Value = userProfile.UserGuid;
            command.Parameters.Add(userGuidParameter);

            var emailAddressParameter = new SqlParameter("@EmailAddress", SqlDbType.NVarChar, 56);
            emailAddressParameter.IsNullable = false;
            userProfile.EmailAddress = userProfile.EmailAddress ?? "";
            userProfile.EmailAddress = userProfile.EmailAddress.Trim();
            emailAddressParameter.Value = userProfile.EmailAddress;
            command.Parameters.Add(emailAddressParameter);

			var userNameParameter = new SqlParameter( "@UserName", SqlDbType.NVarChar, 56);
			userNameParameter.IsNullable = false;
			userProfile.UserName = userProfile.UserName ?? "";
			userProfile.UserName = userProfile.UserName.Trim();
			userNameParameter.Value = userProfile.UserName;
			command.Parameters.Add(userNameParameter);

            var growerGuidParameter = new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier, 16);
            growerGuidParameter.IsNullable = true;
            growerGuidParameter.Value = userProfile.GrowerGuid;
            command.Parameters.Add(growerGuidParameter);
        }

        private UserProfile LoadUserProfileFromSqlDataReader(SqlDataReader reader)
        {
            var userProfile = new UserProfile();

			string columnName = "";
            try
            {
                columnName = "UserId";
                int userId = (int)(reader[(int)UserProfile.ColumnEnum.UserId] ?? 0);
                userProfile.UserId = (int)userId;

                columnName = "UserName";
                string userName = (string)(reader[(int)UserProfile.ColumnEnum.UserName] ?? string.Empty);
                userName = userName.Trim();
                userProfile.UserName = (string)userName;

                columnName = "EmailAddress";
                string emailAddress = (string)(reader[(int)UserProfile.ColumnEnum.EmailAddress] ?? string.Empty);
                emailAddress = emailAddress.Trim();
                userProfile.EmailAddress = (string)emailAddress;

                columnName = "UserGuid";
                Guid userGuid = (Guid)(reader[(int)UserProfile.ColumnEnum.UserGuid] ?? Guid.Empty);
                userProfile.UserGuid = (Guid)userGuid;

                columnName = "GrowerGuid";
                Guid growerGuid = (Guid)(reader[(int)UserProfile.ColumnEnum.GrowerGuid] ?? Guid.Empty);
                userProfile.GrowerGuid = (Guid)growerGuid;
            }
            catch( Exception e)
            {
                throw new ApplicationException(string.Format("An exception has occurred while processing the reader on field \"{0}\" ({1}).", columnName, e.Message), e);
            }

            return userProfile;
        }

        public bool UserHasPermission(Guid userGuid, string permissionName)
        {
            var command = new SqlCommand("UserHasPermission");
            command.CommandType = CommandType.StoredProcedure;

            var userGuidParameter = new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier);
            userGuidParameter.IsNullable = false;
            userGuidParameter.Value = userGuid;
            command.Parameters.Add(userGuidParameter);

            var permissionNameParameter = new SqlParameter("@PermissionName", SqlDbType.NVarChar, 256);
            permissionNameParameter.IsNullable = false;
            permissionNameParameter.Value = permissionName;
            command.Parameters.Add(permissionNameParameter);

            var hasPermissionParameter = new SqlParameter("@HasPermission", SqlDbType.Bit);
            hasPermissionParameter.IsNullable = true;
            hasPermissionParameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(hasPermissionParameter);

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToMembership)
            {
                command.Connection = connection;

                connection.Open();

                var reader = command.ExecuteReader();
                bool hasPermission = (bool)(hasPermissionParameter.Value ?? false);

                connection.Close();

                return hasPermission;
            }
        }

        public Role GetUserRole(int userId)
        {
            Role userRole = new Role();

            var sqlQueryServices = new SqlQueryServices();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToMembership)
            {

                var command = new SqlCommand(
                    "SELECT webpages_Roles.RoleId, webpages_Roles.RoleName FROM webpages_Roles join webpages_UsersInRoles on webpages_Roles.RoleId = webpages_UsersInRoles.Roleid WHERE UserId = @UserId", connection);

                var userGuidParameter = new SqlParameter("@UserId", SqlDbType.Int);
                userGuidParameter.IsNullable = false;
                userGuidParameter.Value = userId;
                command.Parameters.Add(userGuidParameter);

                connection.Open();
                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();

                    userRole.RoleId = reader.GetInt32(0);
                    userRole.RoleName = reader.GetString(1);

                }

                connection.Close();

            }

            return userRole;
        }

        public string GetGrowerAuthorizationProfileId(Guid growerGuid)
        {
            string authorizationProfileId = null;

            var sqlQueryServices = new SqlQueryServices();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToMembership)
            {
                var command = new SqlCommand("SELECT AuthorizationProfileId FROM GrowerAuthorizationProfile WHERE GrowerGuid = @GrowerGuid", connection);

                var growerGuidParameter = new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier);
                growerGuidParameter.IsNullable = false;
                growerGuidParameter.Value = growerGuid;
                command.Parameters.Add(growerGuidParameter);

                connection.Open();

                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();

                    authorizationProfileId = (string)(reader[0] ?? "");

                    authorizationProfileId = authorizationProfileId.Trim();
                }

                connection.Close();
            }

            return authorizationProfileId;
        }

        public void SaveGrowerAuthorizationProfile(Guid growerGuid, string authorizationProfileId)
        {
            var sqlQueryServices = new SqlQueryServices();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToMembership)
            {
                var command = new SqlCommand("INSERT INTO GrowerAuthorizationProfile (GrowerGuid, AuthorizationProfileId) VALUES (@GrowerGuid, @AuthorizationProfileId)", connection);

                var growerGuidParameter = new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier)
                    {
                        IsNullable = false,
                        Value = growerGuid
                    };
                command.Parameters.Add(growerGuidParameter);

                var authorizationProfileIdParameter = new SqlParameter("@AuthorizationProfileId", SqlDbType.NVarChar, 50)
                    {
                        IsNullable = false,
                        Value = authorizationProfileId
                    };
                command.Parameters.Add(authorizationProfileIdParameter);

                connection.Open();

                command.ExecuteNonQuery();

                connection.Close();
            }

            string authorizationProfileIdFromDatabase = GetGrowerAuthorizationProfileId(growerGuid);
            if (authorizationProfileIdFromDatabase.Trim() != authorizationProfileId.Trim())
                throw new DataException("The AuthorizationProfile was not saved.");
        }

        public void DeleteGrowerAuthorizationProfile(Guid growerGuid)
        {
            var sqlQueryServices = new SqlQueryServices();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToMembership)
            {
                var command = new SqlCommand("DELETE GrowerAuthorizationProfile WHERE GrowerGuid = @GrowerGuid", connection);

                var growerGuidParameter = new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier)
                    {
                        IsNullable = false,
                        Value = growerGuid
                    };
                command.Parameters.Add(growerGuidParameter);

                connection.Open();

                command.ExecuteNonQuery();

                connection.Close();
            }

            string authorizationProfileIdFromDatabase = GetGrowerAuthorizationProfileId(growerGuid);
            if (authorizationProfileIdFromDatabase != null)
                throw new DataException("The GrowerAuthorizationProfile was not deleted.");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using DataAccessLibrary;
using DataObjectLibrary;
using BusinessObjectsLibrary;
using BusinessObjectsLibrary.Catalog;



using DataServiceLibrary;

//using BusinessObjectsLibrary.Catalog;



namespace DataServiceLibrary
{
    public class OrderLineUpdateServiceNew : DataServiceBaseNew
    {
        public OrderLineUpdateServiceNew(StatusObject status)
            : base(status)
        {

        }

        public bool OrderLineUpdate(Guid orderLineGuid, int quantity, decimal price, string SupplierOrGrower, out int newQuantity, out Guid growerOrderGuid)
        {
            newQuantity = 0;
            var orderGuid = new Guid();
            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {

               
                using (var command = new SqlCommand("[dbo].[OrderLineUpdate]", connection))
                {

                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters
                        .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                        .Value = Status.User.UserGuid;
                    command.Parameters
                        .Add(new SqlParameter("@OrderLineGuid", SqlDbType.UniqueIdentifier))
                        .Value = orderLineGuid;
                    command.Parameters
                        .Add(new SqlParameter("@QtyOrdered ", SqlDbType.Int))
                        .Value = quantity;
                    command.Parameters
                        .Add(new SqlParameter("@Price  ", SqlDbType.Decimal))
                        .Value = price;
                    command.Parameters
                        .Add(new SqlParameter("@SupplierOrGrower ", SqlDbType.NChar, 20))
                        .Value = SupplierOrGrower;
                    command.Parameters
                        .Add(new SqlParameter("@ActualQuantity", SqlDbType.Int))
                        .Direction = ParameterDirection.Output;
                    command.Parameters
                        .Add(new SqlParameter("@GrowerOrderGuidOut", SqlDbType.UniqueIdentifier))
                        .Direction = ParameterDirection.Output;
                    

                    var response = (int)command.ExecuteNonQuery();
                    newQuantity = (int)command.Parameters["@ActualQuantity"].Value;
                    orderGuid = (Guid)command.Parameters["@GrowerOrderGuidOut"].Value;

                   
                   
                 
                    if (newQuantity== quantity)
                    {
                        Status.StatusMessage = "Order Line Updated";
                    }
                    else
                    {
                        Status.StatusMessage = "Order Line quantity change reduced due to availability restriction";
                    }
                  
                    Status.Success = true;
                   
                   
                }
            }
                
            
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "OrderLineUpdateServiceNew",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                
            }
            finally
            {
                connection.Close();
            }

            growerOrderGuid = orderGuid;
            return Status.Success;

        }

    }
}

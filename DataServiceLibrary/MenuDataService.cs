﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary;
using DataObjectLibrary;
using System.Data.SqlClient;
using System.Data;
using BusinessObjectsLibrary;
namespace DataServiceLibrary
{
    public partial class MenuDataService: DataServiceBaseNew
    {


        public MenuDataService(StatusObject status)
            : base(status)
        {
        }
        public List<MenuDataItem> GetMenuData(string email, String sellerCode)
        {
            sellerCode = "GFB";
            var list = new List<MenuDataItem>();
            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerGetMenuTypesAndCategories]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@EMail", SqlDbType.NVarChar,70))
                    .Value = email;
                command.Parameters
                    .Add(new SqlParameter("@SellerCode", SqlDbType.NVarChar,10))
                    .Value = sellerCode;




                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var item = new MenuDataItem();
                    item.ProductFormCategoryGuid = reader.GetGuid(0);
                    item.ProductFormCategoryCode = reader.GetString(1);
                    item.ProductFormCategoryName = reader.GetString(2);
                    item.ProgramTypeGuid = reader.GetGuid(3);
                    item.ProgramTypeCode = reader.GetString(4);
                    item.ProgramTypeName = reader.GetString(5);
                    list.Add(item);
                }

                

                Status.Success = true;
                Status.StatusMessage = "Menu Data Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "MenuDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }


            return list;
        }

       

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;
using DataAccessLibrary;
using System.Data.SqlClient;
using System.Data;
using BusinessObjectsLibrary;



namespace DataServiceLibrary
{
    public partial class VarietyDataService: DataServiceBaseNew
    {
        public VarietyDataService(StatusObject status)
            : base(status)
        {

        }
    
        public List<DataObjectLibrary.Variety> GetVarieties(string programTypeCode = null, string productFormCategoryCode = null, string[] geneticOwnerCodes = null, string[] supplierCodes = null, string[] speciesCodes = null, string varietyCode = null)
        {
            List<DataObjectLibrary.Variety> varietyList = null;
            List<ImageFile> imageFileList = null;
            List<ImageSet> imageSetList = null;


            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[VarietyDataGet]", connection);
                
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@ProgramTypeCode", SqlDbType.NChar, 50))
                    .Value = programTypeCode;
                command.Parameters
                    .Add(new SqlParameter("@ProductFormCategoryCode", SqlDbType.NChar, 50))
                    .Value = productFormCategoryCode;
                command.Parameters
                    .Add(new SqlParameter("@GeneticOwnerCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(geneticOwnerCodes);
                command.Parameters
                    .Add(new SqlParameter("@SupplierCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(supplierCodes);
                command.Parameters
                    .Add(new SqlParameter("@SpeciesCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(speciesCodes);

                command.Parameters
                    .Add(new SqlParameter("@VarietyCode", SqlDbType.NVarChar, 50))
                    .Value = varietyCode;


                ImageSetTableService imageSetTableService = new ImageSetTableService();

                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                        
                       
                    var context = new Context();

                    var geneticOwnerTableService = context.Get<GeneticOwnerTableService>();
                    geneticOwnerTableService.GetAllFromReader(reader);

                    reader.NextResult();

                    var varietyTableService = context.Get<VarietyTableService>();
                    varietyList = varietyTableService.GetAllFromReader(reader);

                    reader.NextResult();

                    var speciesTableService = context.Get<SpeciesTableService>();
                    speciesTableService.GetAllFromReader(reader);

                    reader.NextResult();

                    var lookupTableService = context.Get<LookupTableService>();
                    lookupTableService.GetAllFromReader(reader);

                    reader.NextResult();

                    imageSetTableService = context.Get<ImageSetTableService>();
                    imageSetList = imageSetTableService.GetAllFromReader(reader);

                    reader.NextResult();

                    var imageFileTableService = context.Get<ImageFileTableService>();
                    imageFileList = imageFileTableService.GetAllFromReader(reader);
                        
                }
               

                //TODO: This shouldn't be necessary.
                foreach (var imageSet in imageSetList)
                {
                    imageSet.SetLazyImageFileList(new Lazy<List<ImageFile>>(() => new List<ImageFile>()));
                }

                //TODO: This shouldn't be necessary.
                foreach (var imageFile in imageFileList)
                {
                    var imageSet = imageSetTableService.GetByGuid(imageFile.ImageSetGuid);
                    imageSet.ImageFileList.Add(imageFile);
                }



                Status.Success = true;
                Status.StatusMessage = "Varieties Loaded";

                

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "VarietyService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }



           
            

            return varietyList;
        }


        public List<DataObjectLibrary.Species> GetSpecies(string programTypeCode = null, string sellerCode=null,string productFormCategoryCode = null, string[] geneticOwnerCodes = null, string[] supplierCodes = null)
        {
            sellerCode = "GFB";
            List<DataObjectLibrary.Species> speciesList = null;



            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[SpeciesDataGet]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@SellerCode", SqlDbType.NChar, 10))
                    .Value = sellerCode;
                command.Parameters
                    .Add(new SqlParameter("@ProgramTypeCode", SqlDbType.NChar, 50))
                    .Value = programTypeCode;
                command.Parameters
                    .Add(new SqlParameter("@ProductFormCategoryCode", SqlDbType.NChar, 50))
                    .Value = productFormCategoryCode;
                command.Parameters
                    .Add(new SqlParameter("@GeneticOwnerCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(geneticOwnerCodes);
                command.Parameters
                    .Add(new SqlParameter("@SupplierCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(supplierCodes);
               



                var reader = command.ExecuteReader();
                var context = new Context();

                var speciesTableService = context.Get<SpeciesTableService>();
                speciesList = speciesTableService.GetAllFromReader(reader);

                Status.Success = true;
                Status.StatusMessage = "Species Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "VarietyService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }


            return speciesList;
        }

        public List<DataObjectLibrary.GeneticOwner> GetGeneticOwners(string programTypeCode = null, string productFormCategoryCode = null, string[] supplierCodes = null, string[] speciesCodes = null)
        {
            List<DataObjectLibrary.GeneticOwner> geneticOwnerList = null;

            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GeneticOwnerDataGet]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@UserCode", SqlDbType.NChar, 56))
                    .Value = null;
                command.Parameters
                    .Add(new SqlParameter("@ProgramTypeCode", SqlDbType.NChar, 50))
                    .Value = programTypeCode;
                command.Parameters
                    .Add(new SqlParameter("@ProductFormCategoryCode", SqlDbType.NChar, 50))
                    .Value = productFormCategoryCode;
                command.Parameters
                    .Add(new SqlParameter("@SupplierCodeList", SqlDbType.NVarChar, 500))
                    .Value = ToCommaSeparatedList(supplierCodes);
                command.Parameters
                    .Add(new SqlParameter("@SpeciesCodeList", SqlDbType.NVarChar, 500))
                    .Value = ToCommaSeparatedList(speciesCodes);




                var reader = command.ExecuteReader();
                var context = new Context();

                var geneticOwnerTableService = context.Get<GeneticOwnerTableService>();
                geneticOwnerList = geneticOwnerTableService.GetAllFromReader(reader);

                Status.Success = true;
                Status.StatusMessage = "Genetic Owners Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "VarietyService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }

            return geneticOwnerList;
        }

        protected string ToCommaSeparatedList(string[] codes)
        {
            if (codes == null)
            {
                return "*";
            }
            else
            {
                var list = new StringBuilder();
                foreach (string code in codes)
                {
                    if (list.Length > 0)
                        list.Append(',');

                    list.Append(code);
                }

                return list.ToString();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;
using DataAccessLibrary;
using System.Data.SqlClient;
using System.Data;
using BusinessObjectsLibrary;

namespace DataServiceLibrary
{
    public partial class ProductDataService: DataServiceBaseNew
    {
        public ProductDataService(StatusObject status)
            : base(status)
        {

        }
        public List<DataObjectLibrary.Product> GetProducts(string programTypeCode = null, string productFormCategoryCode = null, string[] supplierCodes = null, string[] geneticOwnerCodes = null, string[] speciesCodes = null)
        {
            List<DataObjectLibrary.Product> productList = null;

            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[ProductDataGet]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@ProgramTypeCode", SqlDbType.NChar, 50))
                    .Value = programTypeCode;
                command.Parameters
                    .Add(new SqlParameter("@ProductFormCategoryCode", SqlDbType.NChar, 50))
                    .Value = productFormCategoryCode;
                command.Parameters
                    .Add(new SqlParameter("@SupplierCodeList", SqlDbType.NVarChar, 500))
                    .Value = ToCommaSeparatedList(supplierCodes);
                command.Parameters
                    .Add(new SqlParameter("@GeneticOwnerCodeList", SqlDbType.NVarChar, 500))
                    .Value = ToCommaSeparatedList(geneticOwnerCodes);




                var reader = command.ExecuteReader();
                var context = new Context();

                var programTypeTableService = context.Get<ProgramTypeTableService>();
                programTypeTableService.GetAllFromReader(reader);

                reader.NextResult();

                var productFormCategoryTableService = context.Get<ProductFormCategoryTableService>();
                productFormCategoryTableService.GetAllFromReader(reader);

                reader.NextResult();

                var productFormTableService = context.Get<ProductFormTableService>();
                productFormTableService.GetAllFromReader(reader);

                reader.NextResult();

                var speciesTableService = context.Get<SpeciesTableService>();
                speciesTableService.GetAllFromReader(reader);

                reader.NextResult();

                var supplierTableService = context.Get<SupplierTableService>();
                supplierTableService.GetAllFromReader(reader);

                reader.NextResult();

                var varietyTableService = context.Get<VarietyTableService>();
                varietyTableService.GetAllFromReader(reader);

                reader.NextResult();

                var programTableService = context.Get<ProgramTableService>();
                programTableService.GetAllFromReader(reader);

                reader.NextResult();

                var productTableService = context.Get<ProductTableService>();
                productList = productTableService.GetAllFromReader(reader);

                Status.Success = true;
                Status.StatusMessage = "Products Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ProductService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }

            return productList;
        }

        protected string ToCommaSeparatedList(string[] codes)
        {
            if (codes == null)
            {
                return "*";
            }
            else
            {
                var list = new StringBuilder();
                foreach (string code in codes)
                {
                    if (list.Length > 0)
                        list.Append(',');

                    list.Append(code);
                }

                return list.ToString();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using DataAccessLibrary;
using DataObjectLibrary;
using BusinessObjectsLibrary;


namespace DataServiceLibrary
{
    
     public class CartDataService : DataServiceBaseNew
    {
        public CartDataService(StatusObject status)
            : base(status)
        {

        }
    
       

        public string GetPromoName(string promoCode)
        {
            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            string promoName = "";
            try
            {
               
                var command = new SqlCommand("[dbo].[PromoCodeGet]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@PromoCode", SqlDbType.NVarChar, 20))
                    .Value = promoCode;
                command.Parameters
                    .Add(new SqlParameter("@PromoName", SqlDbType.NVarChar, 50))
                    .Direction = ParameterDirection.Output;

                var response = (int)command.ExecuteNonQuery();
                promoName = (string)command.Parameters["@PromoName"].Value;

                Status.Success = true;
                Status.StatusMessage = "Promo Retrieved";
            }
             catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CartDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;
            }
            finally
            {
                connection.Close();
            }
             return promoName;
        }

        public List<CartOrderSummary> GetCartSummaries()
        {
            var list  = new List<CartOrderSummary>();
            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            
            try
            {

                var command = new SqlCommand("[dbo].[CartOrdersGet]", connection)
                {
                    CommandType = CommandType.StoredProcedure

                };
                command.CommandTimeout = 300;
                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
              

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    
                 
                    
                    var item = new CartOrderSummary();
                    item.OrderGuid = (Guid)reader["GrowerOrderGuid"];        
                    item.ShipWeekString = reader["ShipWeekString"].ToString();
                    item.ProductFormName = reader["ProductFormName"].ToString();
                    item.ProgramCategoryName = reader["ProgramCategoryName"].ToString();
                    item.OrderQty = (int)reader["OrderQty"];
                    item.OrderID = (long)reader["OrderID"];
                
                    list.Add(item);
                }

                Status.Success = true;
                Status.StatusMessage = "Cart Orders Retrieved";
            
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CartDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }
            return list;
        }
        public List<OrderSummary2> GetOpenOrders()
        {
            var list = new List<OrderSummary2>();
            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            
            try
            {

                var command = new SqlCommand("[dbo].[OpenOrdersGet]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
              

                var reader = command.ExecuteReader();

                while (reader.Read())
                {



                    var item = new OrderSummary2();
                    item.OrderGuid = (Guid)reader["GrowerOrderGuid"];        
                    item.OrderNo = (string)reader["OrderNo"];        
                    item.CustomerPoNo = (string)reader["CustomerPoNo"];
                    item.ShipWeekString = (string)reader["ShipWeekString"];
                    item.ProductFormCategoryName = (string)reader["ProductFormCategorymName"];
                    item.ProgramTypeName = (string)reader["ProgramTypeName"];
                    item.PromotionalCode = (string)reader["PromotionalCode"];
                    item.OrderDescription = (string)reader["OrderDescription"];
                    item.GrowerCode = (string)reader["GrowerCode"];
                    item.GrowerName = (string)reader["GrowerName"];

                    item.GrowerOrderStatusCode = (string)reader["GrowerOrderStatusCode"];
                    item.GrowerOrderStatusName = (string)reader["GrowerOrderStatusName"];
                    item.OrderQty = (int)reader["OrderQty"];
                    
      
                    list.Add(item);
                }

                Status.Success = true;
                Status.StatusMessage = "Open Orders Retrieved";
            
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CartDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }
            return list;
        }
        public List<OrderSummary2> GetCancelledOrders()
        {
            var list = new List<OrderSummary2>();
            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();

            try
            {

                var command = new SqlCommand("[dbo].[CancelledOrdersGet]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;


                var reader = command.ExecuteReader();

                while (reader.Read())
                {



                    var item = new OrderSummary2();
                    item.OrderGuid = (Guid)reader["GrowerOrderGuid"];
                    item.OrderNo = (string)reader["OrderNo"];
                    item.CustomerPoNo = (string)reader["CustomerPoNo"];
                    item.ShipWeekString = (string)reader["ShipWeekString"];
                    item.ProductFormCategoryName = (string)reader["ProductFormCategorymName"];
                    item.ProgramTypeName = (string)reader["ProgramTypeName"];
                    item.PromotionalCode = (string)reader["PromotionalCode"];
                    item.OrderDescription = (string)reader["OrderDescription"];
                    item.GrowerCode = (string)reader["GrowerCode"];
                    item.GrowerName = (string)reader["GrowerName"];

                    item.GrowerOrderStatusCode = (string)reader["GrowerOrderStatusCode"];
                    item.GrowerOrderStatusName = (string)reader["GrowerOrderStatusName"];
                    item.OrderQty = (int)reader["OrderQty"];


                    list.Add(item);
                }

                Status.Success = true;
                Status.StatusMessage = "Cancelled Orders Retrieved";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CartDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }
            return list;
        }
        public List<OrderSummary2> GetShippedOrders()
        {
            var list = new List<OrderSummary2>();
            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();

            try
            {

                var command = new SqlCommand("[dbo].[ShippedOrdersGet]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;


                var reader = command.ExecuteReader();

                while (reader.Read())
                {



                    var item = new OrderSummary2();
                    item.OrderGuid = (Guid)reader["GrowerOrderGuid"];
                    item.OrderNo = (string)reader["OrderNo"];
                    item.CustomerPoNo = (string)reader["CustomerPoNo"];
                    item.ShipWeekString = (string)reader["ShipWeekString"];
                    item.ProductFormCategoryName = (string)reader["ProductFormCategorymName"];
                    item.ProgramTypeName = (string)reader["ProgramTypeName"];
                    item.PromotionalCode = (string)reader["PromotionalCode"];
                    item.OrderDescription = (string)reader["OrderDescription"];
                    item.GrowerCode = (string)reader["GrowerCode"];
                    item.GrowerName = (string)reader["GrowerName"];

                    item.GrowerOrderStatusCode = (string)reader["GrowerOrderStatusCode"];
                    item.GrowerOrderStatusName = (string)reader["GrowerOrderStatusName"];
                    item.OrderQty = (int)reader["OrderQty"];


                    list.Add(item);
                }

                Status.Success = true;
                Status.StatusMessage = "Shipped Orders Retrieved";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CartDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }
            return list;
        }
    }
}

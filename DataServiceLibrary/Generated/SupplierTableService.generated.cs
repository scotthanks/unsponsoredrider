using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class SupplierTableService : TableService<Supplier>
	{
		public SupplierTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override Supplier DefaultInsert( Supplier supplier, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [Supplier]([Guid], [DateDeactivated], [AddressGuid], [Name], [Code], [EMail], [PhoneAreaCode], [Phone], [AvailabilityInSalesUnit], ) VALUES (@Guid, @DateDeactivated, @AddressGuid, @Name, @Code, @EMail, @PhoneAreaCode, @Phone, @AvailabilityInSalesUnit, );SELECT * FROM [Supplier] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, supplier, transaction: transaction);
		}

		protected override Supplier DefaultUpdate( Supplier supplier, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [Supplier] SET [DateDeactivated]=@DateDeactivated, [AddressGuid]=@AddressGuid, [Name]=@Name, [Code]=@Code, [EMail]=@EMail, [PhoneAreaCode]=@PhoneAreaCode, [Phone]=@Phone, [AvailabilityInSalesUnit]=@AvailabilityInSalesUnit, WHERE [Guid]=@Guid;SELECT * FROM [Supplier] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, supplier, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, Supplier supplier)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = supplier.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = supplier.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = supplier.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var addressGuidParameter = new SqlParameter( "@AddressGuid", SqlDbType.UniqueIdentifier, 16);
			addressGuidParameter.IsNullable = false;
			addressGuidParameter.Value = supplier.AddressGuid;
			command.Parameters.Add(addressGuidParameter);

			var nameParameter = new SqlParameter( "@Name", SqlDbType.NVarChar, 50);
			nameParameter.IsNullable = false;
			supplier.Name = supplier.Name ?? "";
			supplier.Name = supplier.Name.Trim();
			nameParameter.Value = supplier.Name;
			command.Parameters.Add(nameParameter);

			var codeParameter = new SqlParameter( "@Code", SqlDbType.NChar, 10);
			codeParameter.IsNullable = false;
			supplier.Code = supplier.Code ?? "";
			supplier.Code = supplier.Code.Trim();
			codeParameter.Value = supplier.Code;
			command.Parameters.Add(codeParameter);

			var eMailParameter = new SqlParameter( "@EMail", SqlDbType.VarChar, 70);
			eMailParameter.IsNullable = false;
			supplier.EMail = supplier.EMail ?? "";
			supplier.EMail = supplier.EMail.Trim();
			eMailParameter.Value = supplier.EMail;
			command.Parameters.Add(eMailParameter);

			var phoneAreaCodeParameter = new SqlParameter( "@PhoneAreaCode", SqlDbType.Decimal, 5);
			phoneAreaCodeParameter.IsNullable = false;
			phoneAreaCodeParameter.Value = supplier.PhoneAreaCode;
			command.Parameters.Add(phoneAreaCodeParameter);

			var phoneParameter = new SqlParameter( "@Phone", SqlDbType.Decimal, 5);
			phoneParameter.IsNullable = false;
			phoneParameter.Value = supplier.Phone;
			command.Parameters.Add(phoneParameter);

			var availabilityInSalesUnitParameter = new SqlParameter( "@AvailabilityInSalesUnit", SqlDbType.Bit, 1);
			availabilityInSalesUnitParameter.IsNullable = false;
			availabilityInSalesUnitParameter.Value = supplier.AvailabilityInSalesUnit;
			command.Parameters.Add(availabilityInSalesUnitParameter);

		}

        protected override Supplier DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var supplier = new Supplier();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)Supplier.ColumnEnum.Id] ?? long.MinValue;
				supplier.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)Supplier.ColumnEnum.Guid] ?? Guid.Empty;
				supplier.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)Supplier.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				supplier.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "AddressGuid";
				var addressGuid = reader[(int)Supplier.ColumnEnum.AddressGuid] ?? Guid.Empty;
				supplier.AddressGuid = (Guid)addressGuid;

				columnName = "Name";
				var name = reader[(int)Supplier.ColumnEnum.Name] ?? string.Empty;
				name = TrimString(name);
				supplier.Name = (string)name;

				columnName = "Code";
				var code = reader[(int)Supplier.ColumnEnum.Code] ?? string.Empty;
				code = TrimString(code);
				supplier.Code = (string)code;

				columnName = "EMail";
				var eMail = reader[(int)Supplier.ColumnEnum.EMail] ?? string.Empty;
				eMail = TrimString(eMail);
				supplier.EMail = (string)eMail;

				columnName = "PhoneAreaCode";
				var phoneAreaCode = reader[(int)Supplier.ColumnEnum.PhoneAreaCode] ?? Decimal.MinValue;
				supplier.PhoneAreaCode = (Decimal)phoneAreaCode;

				columnName = "Phone";
				var phone = reader[(int)Supplier.ColumnEnum.Phone] ?? Decimal.MinValue;
				supplier.Phone = (Decimal)phone;

				columnName = "AvailabilityInSalesUnit";
				var availabilityInSalesUnit = reader[(int)Supplier.ColumnEnum.AvailabilityInSalesUnit] ?? false;
				supplier.AvailabilityInSalesUnit = (bool)availabilityInSalesUnit;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return supplier;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(Supplier supplier)
        {
			SetUpAddressLazyLoad( supplier);
            //SetUpFreightSupplierStateZoneListLazyLoad( supplier);
            //SetUpLedgerListLazyLoad( supplier);
            //SetUpProductExclusionListLazyLoad( supplier);
			SetUpProductFormListLazyLoad( supplier);
			SetUpProgramListLazyLoad( supplier);
            //SetUpPromoCodeListLazyLoad( supplier);
            //SetUpReportedAvailabilityUpdateListLazyLoad( supplier);
            //SetUpSupplierBoxListLazyLoad( supplier);
			SetUpSupplierOrderListLazyLoad( supplier);
		}

        protected override void FixAnyLazyLoadedLists(Supplier supplier, ListActionEnum listAction)
        {
			FixAddressList( supplier, listAction);
		}

        private void SetUpAddressLazyLoad( Supplier supplier)
        {
            var addressTableService = Context.Get<AddressTableService>();
            supplier.SetLazyAddress(new Lazy<Address>(() => addressTableService.GetByGuid(supplier.AddressGuid), false));
        }

        private void FixAddressList( Supplier supplier, ListActionEnum listAction)
        {
            if (supplier.AddressIsLoaded)
            {
                FixLazyLoadedList(supplier.Address.SupplierList, supplier, listAction);
            }
        }

        //private void SetUpFreightSupplierStateZoneListLazyLoad(Supplier supplier)
        //{
        //    var freightSupplierStateZoneTableService = Context.Get<FreightSupplierStateZoneTableService>();
        //    supplier.SetLazyFreightSupplierStateZoneList(new Lazy<List<FreightSupplierStateZone>>(() => freightSupplierStateZoneTableService.GetAllActiveByGuid(supplier.Guid, "SupplierGuid"), false));
        //}

        //private void SetUpLedgerListLazyLoad(Supplier supplier)
        //{
        //    var ledgerTableService = Context.Get<LedgerTableService>();
        //    supplier.SetLazyLedgerList(new Lazy<List<Ledger>>(() => ledgerTableService.GetAllActiveByGuid(supplier.Guid, "SupplierGuid"), false));
        //}

        //private void SetUpProductExclusionListLazyLoad(Supplier supplier)
        //{
        //    var productExclusionTableService = Context.Get<ProductExclusionTableService>();
        //    supplier.SetLazyProductExclusionList(new Lazy<List<ProductExclusion>>(() => productExclusionTableService.GetAllActiveByGuid(supplier.Guid, "SupplierGuid"), false));
        //}

        private void SetUpProductFormListLazyLoad(Supplier supplier)
        {
            var productFormTableService = Context.Get<ProductFormTableService>();
            supplier.SetLazyProductFormList(new Lazy<List<ProductForm>>(() => productFormTableService.GetAllActiveByGuid(supplier.Guid, "SupplierGuid"), false));
        }

        private void SetUpProgramListLazyLoad(Supplier supplier)
        {
            var programTableService = Context.Get<ProgramTableService>();
            supplier.SetLazyProgramList(new Lazy<List<Program>>(() => programTableService.GetAllActiveByGuid(supplier.Guid, "SupplierGuid"), false));
        }

        //private void SetUpPromoCodeListLazyLoad(Supplier supplier)
        //{
        //    var promoCodeTableService = Context.Get<PromoCodeTableService>();
        //    supplier.SetLazyPromoCodeList(new Lazy<List<PromoCode>>(() => promoCodeTableService.GetAllActiveByGuid(supplier.Guid, "SupplierGuid"), false));
        //}

        //private void SetUpReportedAvailabilityUpdateListLazyLoad(Supplier supplier)
        //{
        //    var reportedAvailabilityUpdateTableService = Context.Get<ReportedAvailabilityUpdateTableService>();
        //    supplier.SetLazyReportedAvailabilityUpdateList(new Lazy<List<ReportedAvailabilityUpdate>>(() => reportedAvailabilityUpdateTableService.GetAllActiveByGuid(supplier.Guid, "SupplierGuid"), false));
        //}

        //private void SetUpSupplierBoxListLazyLoad(Supplier supplier)
        //{
        //    var supplierBoxTableService = Context.Get<SupplierBoxTableService>();
        //    supplier.SetLazySupplierBoxList(new Lazy<List<SupplierBox>>(() => supplierBoxTableService.GetAllActiveByGuid(supplier.Guid, "SupplierGuid"), false));
        //}

        private void SetUpSupplierOrderListLazyLoad(Supplier supplier)
        {
            var supplierOrderTableService = Context.Get<SupplierOrderTableService>();
            supplier.SetLazySupplierOrderList(new Lazy<List<SupplierOrder>>(() => supplierOrderTableService.GetAllActiveByGuid(supplier.Guid, "SupplierGuid"), false));
        }

		#endregion
	}
}

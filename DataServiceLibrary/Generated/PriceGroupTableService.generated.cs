using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class PriceGroupTableService : TableService<PriceGroup>
	{
		public PriceGroupTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override PriceGroup DefaultInsert( PriceGroup priceGroup, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [PriceGroup]([Guid], [DateDeactivated], [Code], [Name], [TagCost], ) VALUES (@Guid, @DateDeactivated, @Code, @Name, @TagCost, );SELECT * FROM [PriceGroup] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, priceGroup, transaction: transaction);
		}

		protected override PriceGroup DefaultUpdate( PriceGroup priceGroup, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [PriceGroup] SET [DateDeactivated]=@DateDeactivated, [Code]=@Code, [Name]=@Name, [TagCost]=@TagCost, WHERE [Guid]=@Guid;SELECT * FROM [PriceGroup] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, priceGroup, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, PriceGroup priceGroup)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = priceGroup.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = priceGroup.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = priceGroup.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var codeParameter = new SqlParameter( "@Code", SqlDbType.NChar, 30);
			codeParameter.IsNullable = false;
			priceGroup.Code = priceGroup.Code ?? "";
			priceGroup.Code = priceGroup.Code.Trim();
			codeParameter.Value = priceGroup.Code;
			command.Parameters.Add(codeParameter);

			var nameParameter = new SqlParameter( "@Name", SqlDbType.NVarChar, 50);
			nameParameter.IsNullable = false;
			priceGroup.Name = priceGroup.Name ?? "";
			priceGroup.Name = priceGroup.Name.Trim();
			nameParameter.Value = priceGroup.Name;
			command.Parameters.Add(nameParameter);

			var tagCostParameter = new SqlParameter( "@TagCost", SqlDbType.Decimal, 5);
			tagCostParameter.IsNullable = false;
			tagCostParameter.Value = priceGroup.TagCost;
			command.Parameters.Add(tagCostParameter);

		}

        protected override PriceGroup DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var priceGroup = new PriceGroup();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)PriceGroup.ColumnEnum.Id] ?? long.MinValue;
				priceGroup.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)PriceGroup.ColumnEnum.Guid] ?? Guid.Empty;
				priceGroup.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)PriceGroup.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				priceGroup.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "Code";
				var code = reader[(int)PriceGroup.ColumnEnum.Code] ?? string.Empty;
				code = TrimString(code);
				priceGroup.Code = (string)code;

				columnName = "Name";
				var name = reader[(int)PriceGroup.ColumnEnum.Name] ?? string.Empty;
				name = TrimString(name);
				priceGroup.Name = (string)name;

				columnName = "TagCost";
				var tagCost = reader[(int)PriceGroup.ColumnEnum.TagCost] ?? Decimal.MinValue;
				priceGroup.TagCost = (Decimal)tagCost;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return priceGroup;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(PriceGroup priceGroup)
        {
			SetUpPriceListLazyLoad( priceGroup);
			SetUpProductListLazyLoad( priceGroup);
            //SetUpProductPriceGroupProgramSeasonListLazyLoad( priceGroup);
		}

        protected override void FixAnyLazyLoadedLists(PriceGroup priceGroup, ListActionEnum listAction)
        {
		}

        private void SetUpPriceListLazyLoad(PriceGroup priceGroup)
        {
            var priceTableService = Context.Get<PriceTableService>();
            priceGroup.SetLazyPriceList(new Lazy<List<Price>>(() => priceTableService.GetAllActiveByGuid(priceGroup.Guid, "PriceGroupGuid"), false));
        }

        private void SetUpProductListLazyLoad(PriceGroup priceGroup)
        {
            var productTableService = Context.Get<ProductTableService>();
            priceGroup.SetLazyProductList(new Lazy<List<Product>>(() => productTableService.GetAllActiveByGuid(priceGroup.Guid, "PriceGroupGuid"), false));
        }

        //private void SetUpProductPriceGroupProgramSeasonListLazyLoad(PriceGroup priceGroup)
        //{
        //    var productPriceGroupProgramSeasonTableService = Context.Get<ProductPriceGroupProgramSeasonTableService>();
        //    priceGroup.SetLazyProductPriceGroupProgramSeasonList(new Lazy<List<ProductPriceGroupProgramSeason>>(() => productPriceGroupProgramSeasonTableService.GetAllActiveByGuid(priceGroup.Guid, "PriceGroupGuid"), false));
        //}

		#endregion
	}
}

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class PriceTableService : TableService<Price>
	{
		public PriceTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override Price DefaultInsert( Price price, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [Price]([Guid], [DateDeactivated], [CountryGuid], [PriceGroupGuid], [VolumeLevelGuid], [RegularCost], [EODCost], [RegularMUPercent], [EODMUPercent], ) VALUES (@Guid, @DateDeactivated, @CountryGuid, @PriceGroupGuid, @VolumeLevelGuid, @RegularCost, @EODCost, @RegularMUPercent, @EODMUPercent, );SELECT * FROM [Price] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, price, transaction: transaction);
		}

		protected override Price DefaultUpdate( Price price, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [Price] SET [DateDeactivated]=@DateDeactivated, [CountryGuid]=@CountryGuid, [PriceGroupGuid]=@PriceGroupGuid, [VolumeLevelGuid]=@VolumeLevelGuid, [RegularCost]=@RegularCost, [EODCost]=@EODCost, [RegularMUPercent]=@RegularMUPercent, [EODMUPercent]=@EODMUPercent, WHERE [Guid]=@Guid;SELECT * FROM [Price] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, price, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, Price price)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = price.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = price.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = price.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var countryGuidParameter = new SqlParameter( "@CountryGuid", SqlDbType.UniqueIdentifier, 16);
			countryGuidParameter.IsNullable = false;
			countryGuidParameter.Value = price.CountryGuid;
			command.Parameters.Add(countryGuidParameter);

			var priceGroupGuidParameter = new SqlParameter( "@PriceGroupGuid", SqlDbType.UniqueIdentifier, 16);
			priceGroupGuidParameter.IsNullable = false;
			priceGroupGuidParameter.Value = price.PriceGroupGuid;
			command.Parameters.Add(priceGroupGuidParameter);

			var volumeLevelGuidParameter = new SqlParameter( "@VolumeLevelGuid", SqlDbType.UniqueIdentifier, 16);
			volumeLevelGuidParameter.IsNullable = false;
			volumeLevelGuidParameter.Value = price.VolumeLevelGuid;
			command.Parameters.Add(volumeLevelGuidParameter);

			var regularCostParameter = new SqlParameter( "@RegularCost", SqlDbType.Decimal, 5);
			regularCostParameter.IsNullable = false;
			regularCostParameter.Value = price.RegularCost;
			command.Parameters.Add(regularCostParameter);

			var eODCostParameter = new SqlParameter( "@EODCost", SqlDbType.Decimal, 5);
			eODCostParameter.IsNullable = false;
			eODCostParameter.Value = price.EODCost;
			command.Parameters.Add(eODCostParameter);

			var regularMUPercentParameter = new SqlParameter( "@RegularMUPercent", SqlDbType.Float, 8);
			regularMUPercentParameter.IsNullable = false;
			regularMUPercentParameter.Value = price.RegularMUPercent;
			command.Parameters.Add(regularMUPercentParameter);

			var eODMUPercentParameter = new SqlParameter( "@EODMUPercent", SqlDbType.Float, 8);
			eODMUPercentParameter.IsNullable = false;
			eODMUPercentParameter.Value = price.EODMUPercent;
			command.Parameters.Add(eODMUPercentParameter);

		}

        protected override Price DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var price = new Price();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)Price.ColumnEnum.Id] ?? long.MinValue;
				price.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)Price.ColumnEnum.Guid] ?? Guid.Empty;
				price.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)Price.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				price.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "CountryGuid";
				var countryGuid = reader[(int)Price.ColumnEnum.CountryGuid] ?? Guid.Empty;
				price.CountryGuid = (Guid)countryGuid;

				columnName = "PriceGroupGuid";
				var priceGroupGuid = reader[(int)Price.ColumnEnum.PriceGroupGuid] ?? Guid.Empty;
				price.PriceGroupGuid = (Guid)priceGroupGuid;

				columnName = "VolumeLevelGuid";
				var volumeLevelGuid = reader[(int)Price.ColumnEnum.VolumeLevelGuid] ?? Guid.Empty;
				price.VolumeLevelGuid = (Guid)volumeLevelGuid;

				columnName = "RegularCost";
				var regularCost = reader[(int)Price.ColumnEnum.RegularCost] ?? Decimal.MinValue;
				price.RegularCost = (Decimal)regularCost;

				columnName = "EODCost";
				var eODCost = reader[(int)Price.ColumnEnum.EODCost] ?? Decimal.MinValue;
				price.EODCost = (Decimal)eODCost;

				columnName = "RegularMUPercent";
				var regularMUPercent = reader[(int)Price.ColumnEnum.RegularMUPercent] ?? Double.MinValue;
				price.RegularMUPercent = (Double)regularMUPercent;

				columnName = "EODMUPercent";
				var eODMUPercent = reader[(int)Price.ColumnEnum.EODMUPercent] ?? Double.MinValue;
				price.EODMUPercent = (Double)eODMUPercent;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return price;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(Price price)
        {
			SetUpCountryLazyLoad( price);
			SetUpPriceGroupLazyLoad( price);
            //SetUpVolumeLevelLazyLoad( price);
		}

        protected override void FixAnyLazyLoadedLists(Price price, ListActionEnum listAction)
        {
			FixCountryList( price, listAction);
			FixPriceGroupList( price, listAction);
			FixVolumeLevelList( price, listAction);
		}

        private void SetUpCountryLazyLoad( Price price)
        {
            var countryTableService = Context.Get<CountryTableService>();
            price.SetLazyCountry(new Lazy<Country>(() => countryTableService.GetByGuid(price.CountryGuid), false));
        }

        private void FixCountryList( Price price, ListActionEnum listAction)
        {
            if (price.CountryIsLoaded)
            {
                FixLazyLoadedList(price.Country.PriceList, price, listAction);
            }
        }

        private void SetUpPriceGroupLazyLoad( Price price)
        {
            var priceGroupTableService = Context.Get<PriceGroupTableService>();
            price.SetLazyPriceGroup(new Lazy<PriceGroup>(() => priceGroupTableService.GetByGuid(price.PriceGroupGuid), false));
        }

        private void FixPriceGroupList( Price price, ListActionEnum listAction)
        {
            if (price.PriceGroupIsLoaded)
            {
                FixLazyLoadedList(price.PriceGroup.PriceList, price, listAction);
            }
        }

        //private void SetUpVolumeLevelLazyLoad( Price price)
        //{
        //    var volumeLevelTableService = Context.Get<VolumeLevelTableService>();
        //    price.SetLazyVolumeLevel(new Lazy<VolumeLevel>(() => volumeLevelTableService.GetByGuid(price.VolumeLevelGuid), false));
        //}

        private void FixVolumeLevelList( Price price, ListActionEnum listAction)
        {
            if (price.VolumeLevelIsLoaded)
            {
                FixLazyLoadedList(price.VolumeLevel.PriceList, price, listAction);
            }
        }

		#endregion
	}
}

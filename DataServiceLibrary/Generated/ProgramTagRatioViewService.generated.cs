using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class ProgramTagRatioViewService : ViewService<ProgramTagRatioView>
    {
        protected override ProgramTagRatioView DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var programTagRatioView = new ProgramTagRatioView();

            string columnName = "";
            try
            {
                columnName = "ProgramGuid";
                var programGuid = reader[(int)ProgramTagRatioView.ColumnEnum.ProgramGuid] ?? Guid.Empty;
                programTagRatioView.ProgramGuid = (Guid)programGuid;

                columnName = "TagRatioLookupGuid";
                var tagRatioLookupGuid = reader[(int)ProgramTagRatioView.ColumnEnum.TagRatioLookupGuid] ?? Guid.Empty;
                programTagRatioView.TagRatioLookupGuid = (Guid)tagRatioLookupGuid;

                columnName = "IsDefault";
                var isDefault = reader[(int)ProgramTagRatioView.ColumnEnum.IsDefault];
                if (isDefault == DBNull.Value) isDefault = null;
                programTagRatioView.IsDefault = Convert.ToBoolean(isDefault);

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return programTagRatioView;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class ProductFormCategoryProgramTypeCombinationViewService : ViewService<ProductFormCategoryProgramTypeCombinationView>
	{
        protected override ProductFormCategoryProgramTypeCombinationView DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var productFormCategoryProgramTypeCombinationView = new ProductFormCategoryProgramTypeCombinationView();

			string columnName = "";
            try
            {
				columnName = "ProductFormCategoryGuid";
				var productFormCategoryGuid = reader[(int)ProductFormCategoryProgramTypeCombinationView.ColumnEnum.ProductFormCategoryGuid] ?? Guid.Empty;
				productFormCategoryProgramTypeCombinationView.ProductFormCategoryGuid = (Guid)productFormCategoryGuid;

				columnName = "ProductFormCategoryCode";
				var productFormCategoryCode = reader[(int)ProductFormCategoryProgramTypeCombinationView.ColumnEnum.ProductFormCategoryCode] ?? string.Empty;
				productFormCategoryCode = TrimString(productFormCategoryCode);
				productFormCategoryProgramTypeCombinationView.ProductFormCategoryCode = (string)productFormCategoryCode;

				columnName = "ProgramTypeGuid";
				var programTypeGuid = reader[(int)ProductFormCategoryProgramTypeCombinationView.ColumnEnum.ProgramTypeGuid] ?? Guid.Empty;
				productFormCategoryProgramTypeCombinationView.ProgramTypeGuid = (Guid)programTypeGuid;

				columnName = "ProgramTypeCode";
				var programTypeCode = reader[(int)ProductFormCategoryProgramTypeCombinationView.ColumnEnum.ProgramTypeCode] ?? string.Empty;
				programTypeCode = TrimString(programTypeCode);
				productFormCategoryProgramTypeCombinationView.ProgramTypeCode = (string)programTypeCode;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return productFormCategoryProgramTypeCombinationView;
        }
	}
}

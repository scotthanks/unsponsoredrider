using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class LookupViewService : ViewService<LookupView>
	{
        protected override LookupView DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var lookupView = new LookupView();

			string columnName = "";
            try
            {
				columnName = "LookupGuid";
				var lookupGuid = reader[(int)LookupView.ColumnEnum.LookupGuid] ?? Guid.Empty;
				lookupView.LookupGuid = (Guid)lookupGuid;

				columnName = "LookupId";
				var lookupId = reader[(int)LookupView.ColumnEnum.LookupId] ?? long.MinValue;
				lookupView.LookupId = (long)lookupId;

				columnName = "LookupCode";
				var lookupCode = reader[(int)LookupView.ColumnEnum.LookupCode] ?? string.Empty;
				lookupCode = TrimString(lookupCode);
				lookupView.LookupCode = (string)lookupCode;

				columnName = "LookupName";
				var lookupName = reader[(int)LookupView.ColumnEnum.LookupName] ?? string.Empty;
				lookupName = TrimString(lookupName);
				lookupView.LookupName = (string)lookupName;

				columnName = "SortSequence";
				var sortSequence = reader[(int)LookupView.ColumnEnum.SortSequence] ?? Double.MinValue;
				lookupView.SortSequence = (Double)sortSequence;

				columnName = "Path";
				var path = reader[(int)LookupView.ColumnEnum.Path];
				if (path == DBNull.Value) path = null;
				path = TrimString(path);
				lookupView.Path = (string)path;

				columnName = "ParentLookupGuid";
				var parentLookupGuid = reader[(int)LookupView.ColumnEnum.ParentLookupGuid];
				if (parentLookupGuid == DBNull.Value) parentLookupGuid = null;
				lookupView.ParentLookupGuid = (Guid?)parentLookupGuid;

				columnName = "ParentLookupCode";
				var parentLookupCode = reader[(int)LookupView.ColumnEnum.ParentLookupCode];
				if (parentLookupCode == DBNull.Value) parentLookupCode = null;
				parentLookupCode = TrimString(parentLookupCode);
				lookupView.ParentLookupCode = (string)parentLookupCode;

				columnName = "ParentLookupName";
				var parentLookupName = reader[(int)LookupView.ColumnEnum.ParentLookupName];
				if (parentLookupName == DBNull.Value) parentLookupName = null;
				parentLookupName = TrimString(parentLookupName);
				lookupView.ParentLookupName = (string)parentLookupName;

				columnName = "ParentLookupPath";
				var parentLookupPath = reader[(int)LookupView.ColumnEnum.ParentLookupPath];
				if (parentLookupPath == DBNull.Value) parentLookupPath = null;
				parentLookupPath = TrimString(parentLookupPath);
				lookupView.ParentLookupPath = (string)parentLookupPath;

				columnName = "SortKey";
				var sortKey = reader[(int)LookupView.ColumnEnum.SortKey];
				if (sortKey == DBNull.Value) sortKey = null;
				sortKey = TrimString(sortKey);
				lookupView.SortKey = (string)sortKey;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return lookupView;
        }
	}
}

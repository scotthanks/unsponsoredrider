using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class PersonTableService : TableService<Person>
	{
		public PersonTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override Person DefaultInsert( Person person, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [Person]([Guid], [DateDeactivated], [FirstName], [LastName], [UserGuid], [UserId], [UserCode], [EMail], [PhoneAreaCode], [Phone], [SubscribeNewsletter], [PersonTypeLookupGuid], [GrowerGuid], [PersonGetsOrderEmail], [SubscribePromotions], [IsInnovator], ) VALUES (@Guid, @DateDeactivated, @FirstName, @LastName, @UserGuid, @UserId, @UserCode, @EMail, @PhoneAreaCode, @Phone, @SubscribeNewsletter, @PersonTypeLookupGuid, @GrowerGuid, @PersonGetsOrderEmail, @SubscribePromotions, @IsInnovator, );SELECT * FROM [Person] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, person, transaction: transaction);
		}

		protected override Person DefaultUpdate( Person person, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [Person] SET [DateDeactivated]=@DateDeactivated, [FirstName]=@FirstName, [LastName]=@LastName, [UserGuid]=@UserGuid, [UserId]=@UserId, [UserCode]=@UserCode, [EMail]=@EMail, [PhoneAreaCode]=@PhoneAreaCode, [Phone]=@Phone, [SubscribeNewsletter]=@SubscribeNewsletter, [PersonTypeLookupGuid]=@PersonTypeLookupGuid, [GrowerGuid]=@GrowerGuid, [PersonGetsOrderEmail]=@PersonGetsOrderEmail, [SubscribePromotions]=@SubscribePromotions, [IsInnovator]=@IsInnovator, WHERE [Guid]=@Guid;SELECT * FROM [Person] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, person, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, Person person)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = person.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = person.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = person.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var firstNameParameter = new SqlParameter( "@FirstName", SqlDbType.NVarChar, 50);
			firstNameParameter.IsNullable = false;
			person.FirstName = person.FirstName ?? "";
			person.FirstName = person.FirstName.Trim();
			firstNameParameter.Value = person.FirstName;
			command.Parameters.Add(firstNameParameter);

			var lastNameParameter = new SqlParameter( "@LastName", SqlDbType.NVarChar, 50);
			lastNameParameter.IsNullable = false;
			person.LastName = person.LastName ?? "";
			person.LastName = person.LastName.Trim();
			lastNameParameter.Value = person.LastName;
			command.Parameters.Add(lastNameParameter);

			var userGuidParameter = new SqlParameter( "@UserGuid", SqlDbType.UniqueIdentifier, 16);
			userGuidParameter.IsNullable = true;
			userGuidParameter.Value = person.UserGuid ?? (object)DBNull.Value;
			command.Parameters.Add(userGuidParameter);

			var userIdParameter = new SqlParameter( "@UserId", SqlDbType.BigInt, 8);
			userIdParameter.IsNullable = true;
			userIdParameter.Value = person.UserId ?? (object)DBNull.Value;
			command.Parameters.Add(userIdParameter);

			var userCodeParameter = new SqlParameter( "@UserCode", SqlDbType.NChar, 56);
			userCodeParameter.IsNullable = false;
			person.UserCode = person.UserCode ?? "";
			person.UserCode = person.UserCode.Trim();
			userCodeParameter.Value = person.UserCode;
			command.Parameters.Add(userCodeParameter);

			var eMailParameter = new SqlParameter( "@EMail", SqlDbType.VarChar, 70);
			eMailParameter.IsNullable = false;
			person.EMail = person.EMail ?? "";
			person.EMail = person.EMail.Trim();
			eMailParameter.Value = person.EMail;
			command.Parameters.Add(eMailParameter);

			var phoneAreaCodeParameter = new SqlParameter( "@PhoneAreaCode", SqlDbType.Decimal, 5);
			phoneAreaCodeParameter.IsNullable = false;
			phoneAreaCodeParameter.Value = person.PhoneAreaCode;
			command.Parameters.Add(phoneAreaCodeParameter);

			var phoneParameter = new SqlParameter( "@Phone", SqlDbType.Decimal, 5);
			phoneParameter.IsNullable = false;
			phoneParameter.Value = person.Phone;
			command.Parameters.Add(phoneParameter);

			var subscribeNewsletterParameter = new SqlParameter( "@SubscribeNewsletter", SqlDbType.Bit, 1);
			subscribeNewsletterParameter.IsNullable = false;
			subscribeNewsletterParameter.Value = person.SubscribeNewsletter;
			command.Parameters.Add(subscribeNewsletterParameter);

			var personTypeLookupGuidParameter = new SqlParameter( "@PersonTypeLookupGuid", SqlDbType.UniqueIdentifier, 16);
			personTypeLookupGuidParameter.IsNullable = false;
			personTypeLookupGuidParameter.Value = person.PersonTypeLookupGuid;
			command.Parameters.Add(personTypeLookupGuidParameter);

			var growerGuidParameter = new SqlParameter( "@GrowerGuid", SqlDbType.UniqueIdentifier, 16);
			growerGuidParameter.IsNullable = true;
			growerGuidParameter.Value = person.GrowerGuid ?? (object)DBNull.Value;
			command.Parameters.Add(growerGuidParameter);

			var personGetsOrderEmailParameter = new SqlParameter( "@PersonGetsOrderEmail", SqlDbType.Bit, 1);
			personGetsOrderEmailParameter.IsNullable = false;
			personGetsOrderEmailParameter.Value = person.PersonGetsOrderEmail;
			command.Parameters.Add(personGetsOrderEmailParameter);

			var subscribePromotionsParameter = new SqlParameter( "@SubscribePromotions", SqlDbType.Bit, 1);
			subscribePromotionsParameter.IsNullable = false;
			subscribePromotionsParameter.Value = person.SubscribePromotions;
			command.Parameters.Add(subscribePromotionsParameter);

			var isInnovatorParameter = new SqlParameter( "@IsInnovator", SqlDbType.Bit, 1);
			isInnovatorParameter.IsNullable = false;
			isInnovatorParameter.Value = person.IsInnovator;
			command.Parameters.Add(isInnovatorParameter);

		}

        protected override Person DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var person = new Person();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)Person.ColumnEnum.Id] ?? long.MinValue;
				person.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)Person.ColumnEnum.Guid] ?? Guid.Empty;
				person.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)Person.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				person.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "FirstName";
				var firstName = reader[(int)Person.ColumnEnum.FirstName] ?? string.Empty;
				firstName = TrimString(firstName);
				person.FirstName = (string)firstName;

				columnName = "LastName";
				var lastName = reader[(int)Person.ColumnEnum.LastName] ?? string.Empty;
				lastName = TrimString(lastName);
				person.LastName = (string)lastName;

				columnName = "UserGuid";
				var userGuid = reader[(int)Person.ColumnEnum.UserGuid];
				if (userGuid == DBNull.Value) userGuid = null;
				person.UserGuid = (Guid?)userGuid;

				columnName = "UserId";
				var userId = reader[(int)Person.ColumnEnum.UserId];
				if (userId == DBNull.Value) userId = null;
				person.UserId = (long?)userId;

				columnName = "UserCode";
				var userCode = reader[(int)Person.ColumnEnum.UserCode] ?? string.Empty;
				userCode = TrimString(userCode);
				person.UserCode = (string)userCode;

				columnName = "EMail";
				var eMail = reader[(int)Person.ColumnEnum.EMail] ?? string.Empty;
				eMail = TrimString(eMail);
				person.EMail = (string)eMail;

				columnName = "PhoneAreaCode";
				var phoneAreaCode = reader[(int)Person.ColumnEnum.PhoneAreaCode] ?? Decimal.MinValue;
				person.PhoneAreaCode = (Decimal)phoneAreaCode;

				columnName = "Phone";
				var phone = reader[(int)Person.ColumnEnum.Phone] ?? Decimal.MinValue;
				person.Phone = (Decimal)phone;

				columnName = "SubscribeNewsletter";
				var subscribeNewsletter = reader[(int)Person.ColumnEnum.SubscribeNewsletter] ?? false;
				person.SubscribeNewsletter = (bool)subscribeNewsletter;

				columnName = "PersonTypeLookupGuid";
				var personTypeLookupGuid = reader[(int)Person.ColumnEnum.PersonTypeLookupGuid] ?? Guid.Empty;
				person.PersonTypeLookupGuid = (Guid)personTypeLookupGuid;

				columnName = "GrowerGuid";
				var growerGuid = reader[(int)Person.ColumnEnum.GrowerGuid];
				if (growerGuid == DBNull.Value) growerGuid = null;
				person.GrowerGuid = (Guid?)growerGuid;

				columnName = "PersonGetsOrderEmail";
				var personGetsOrderEmail = reader[(int)Person.ColumnEnum.PersonGetsOrderEmail] ?? false;
				person.PersonGetsOrderEmail = (bool)personGetsOrderEmail;

				columnName = "SubscribePromotions";
				var subscribePromotions = reader[(int)Person.ColumnEnum.SubscribePromotions] ?? false;
				person.SubscribePromotions = (bool)subscribePromotions;

				columnName = "IsInnovator";
				var isInnovator = reader[(int)Person.ColumnEnum.IsInnovator] ?? false;
				person.IsInnovator = (bool)isInnovator;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return person;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(Person person)
        {
			SetUpPersonTypeLookupLazyLoad( person);
			SetUpGrowerLazyLoad( person);
            //SetUpChallengeEventListLazyLoad( person);
            //SetUpEventLogListLazyLoad(person);
			SetUpGrowerOrderListLazyLoad( person);
			SetUpGrowerOrderHistoryListLazyLoad( person);
		}

        protected override void FixAnyLazyLoadedLists(Person person, ListActionEnum listAction)
        {
			FixPersonTypeLookupList( person, listAction);
			FixGrowerList( person, listAction);
		}

        private void SetUpPersonTypeLookupLazyLoad( Person person)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            person.SetLazyPersonTypeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(person.PersonTypeLookupGuid), false));
        }

        private void FixPersonTypeLookupList( Person person, ListActionEnum listAction)
        {
            if (person.PersonTypeLookupIsLoaded)
            {
                FixLazyLoadedList(person.PersonTypeLookup.PersonTypePersonList, person, listAction);
            }
        }

        private void SetUpGrowerLazyLoad( Person person)
        {
            var growerTableService = Context.Get<GrowerTableService>();
            person.SetLazyGrower(new Lazy<Grower>(() => growerTableService.GetByGuid(person.GrowerGuid), false));
        }

        private void FixGrowerList( Person person, ListActionEnum listAction)
        {
            if (person.GrowerIsLoaded)
            {
                FixLazyLoadedList(person.Grower.PersonList, person, listAction);
            }
        }

        //private void SetUpChallengeEventListLazyLoad(Person person)
        //{
        //    var challengeEventTableService = Context.Get<ChallengeEventTableService>();
        //    person.SetLazyChallengeEventList(new Lazy<List<ChallengeEvent>>(() => challengeEventTableService.GetAllActiveByGuid(person.Guid, "PersonGuid"), false));
        //}

        //private void SetUpEventLogListLazyLoad(Person person)
        //{
        //    var eventLogTableService = Context.Get<EventLogTableService>();
        //    person.SetLazyEventLogList(new Lazy<List<EventLog>>(() => eventLogTableService.GetAllActiveByGuid(person.Guid, "PersonGuid"), false));
        //}

        private void SetUpGrowerOrderListLazyLoad(Person person)
        {
            var growerOrderTableService = Context.Get<GrowerOrderTableService>();
            person.SetLazyGrowerOrderList(new Lazy<List<GrowerOrder>>(() => growerOrderTableService.GetAllActiveByGuid(person.Guid, "PersonGuid"), false));
        }

        private void SetUpGrowerOrderHistoryListLazyLoad(Person person)
        {
            var growerOrderHistoryTableService = Context.Get<GrowerOrderHistoryTableService>();
            person.SetLazyGrowerOrderHistoryList(new Lazy<List<GrowerOrderHistory>>(() => growerOrderHistoryTableService.GetAllActiveByGuid(person.Guid, "PersonGuid"), false));
        }

		#endregion
	}
}

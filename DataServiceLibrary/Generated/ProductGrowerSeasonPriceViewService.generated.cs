using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class ProductGrowerSeasonPriceViewService : ViewService<ProductGrowerSeasonPriceView>
	{
        protected override ProductGrowerSeasonPriceView DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var productGrowerSeasonPriceView = new ProductGrowerSeasonPriceView();

			string columnName = "";
            try
            {
				columnName = "ProductGuid";
				var productGuid = reader[(int)ProductGrowerSeasonPriceView.ColumnEnum.ProductGuid] ?? Guid.Empty;
				productGrowerSeasonPriceView.ProductGuid = (Guid)productGuid;

				columnName = "GrowerGuid";
				var growerGuid = reader[(int)ProductGrowerSeasonPriceView.ColumnEnum.GrowerGuid] ?? Guid.Empty;
				productGrowerSeasonPriceView.GrowerGuid = (Guid)growerGuid;

				columnName = "ProgramSeasonGuid";
				var programSeasonGuid = reader[(int)ProductGrowerSeasonPriceView.ColumnEnum.ProgramSeasonGuid] ?? Guid.Empty;
				productGrowerSeasonPriceView.ProgramSeasonGuid = (Guid)programSeasonGuid;

				columnName = "SeasonStartDate";
				var seasonStartDate = reader[(int)ProductGrowerSeasonPriceView.ColumnEnum.SeasonStartDate] ?? DateTime.MinValue;
				productGrowerSeasonPriceView.SeasonStartDate = (DateTime)seasonStartDate;

				columnName = "SeasonEndDate";
				var seasonEndDate = reader[(int)ProductGrowerSeasonPriceView.ColumnEnum.SeasonEndDate] ?? DateTime.MinValue;
				productGrowerSeasonPriceView.SeasonEndDate = (DateTime)seasonEndDate;

                columnName = "PriceGuid";
                var priceGuid = reader[(int)ProductGrowerSeasonPriceView.ColumnEnum.PriceGuid];
                if (priceGuid == DBNull.Value) priceGuid = null;
                productGrowerSeasonPriceView.PriceGuid = (Guid?)priceGuid;
                
                columnName = "DummenPrice";
                var dummenPrice = reader[(int)ProductGrowerSeasonPriceView.ColumnEnum.DummenPrice];
                if (dummenPrice == DBNull.Value) dummenPrice = null;
                productGrowerSeasonPriceView.DummenPrice = (decimal?)dummenPrice;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return productGrowerSeasonPriceView;
        }
	}
}

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class ProgramTypeTableService : TableService<ProgramType>
	{
		public ProgramTypeTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override ProgramType DefaultInsert( ProgramType programType, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [ProgramType]([Guid], [DateDeactivated], [Code], [Name], [TagMultiple], [ImageSetGuid], [DefaultPeakWeekNumber], [DefaultSeasonEndWeekNumber], ) VALUES (@Guid, @DateDeactivated, @Code, @Name, @TagMultiple, @ImageSetGuid, @DefaultPeakWeekNumber, @DefaultSeasonEndWeekNumber, );SELECT * FROM [ProgramType] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, programType, transaction: transaction);
		}

		protected override ProgramType DefaultUpdate( ProgramType programType, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [ProgramType] SET [DateDeactivated]=@DateDeactivated, [Code]=@Code, [Name]=@Name, [TagMultiple]=@TagMultiple, [ImageSetGuid]=@ImageSetGuid, [DefaultPeakWeekNumber]=@DefaultPeakWeekNumber, [DefaultSeasonEndWeekNumber]=@DefaultSeasonEndWeekNumber, WHERE [Guid]=@Guid;SELECT * FROM [ProgramType] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, programType, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, ProgramType programType)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = programType.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = programType.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = programType.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var codeParameter = new SqlParameter( "@Code", SqlDbType.NChar, 10);
			codeParameter.IsNullable = false;
			programType.Code = programType.Code ?? "";
			programType.Code = programType.Code.Trim();
			codeParameter.Value = programType.Code;
			command.Parameters.Add(codeParameter);

			var nameParameter = new SqlParameter( "@Name", SqlDbType.NVarChar, 50);
			nameParameter.IsNullable = false;
			programType.Name = programType.Name ?? "";
			programType.Name = programType.Name.Trim();
			nameParameter.Value = programType.Name;
			command.Parameters.Add(nameParameter);

			var tagMultipleParameter = new SqlParameter( "@TagMultiple", SqlDbType.Int, 4);
			tagMultipleParameter.IsNullable = false;
			tagMultipleParameter.Value = programType.TagMultiple;
			command.Parameters.Add(tagMultipleParameter);

			var imageSetGuidParameter = new SqlParameter( "@ImageSetGuid", SqlDbType.UniqueIdentifier, 16);
			imageSetGuidParameter.IsNullable = true;
			imageSetGuidParameter.Value = programType.ImageSetGuid ?? (object)DBNull.Value;
			command.Parameters.Add(imageSetGuidParameter);

			var defaultPeakWeekNumberParameter = new SqlParameter( "@DefaultPeakWeekNumber", SqlDbType.Int, 4);
			defaultPeakWeekNumberParameter.IsNullable = true;
			defaultPeakWeekNumberParameter.Value = programType.DefaultPeakWeekNumber ?? (object)DBNull.Value;
			command.Parameters.Add(defaultPeakWeekNumberParameter);

			var defaultSeasonEndWeekNumberParameter = new SqlParameter( "@DefaultSeasonEndWeekNumber", SqlDbType.Int, 4);
			defaultSeasonEndWeekNumberParameter.IsNullable = true;
			defaultSeasonEndWeekNumberParameter.Value = programType.DefaultSeasonEndWeekNumber ?? (object)DBNull.Value;
			command.Parameters.Add(defaultSeasonEndWeekNumberParameter);

		}

        protected override ProgramType DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var programType = new ProgramType();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)ProgramType.ColumnEnum.Id] ?? long.MinValue;
				programType.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)ProgramType.ColumnEnum.Guid] ?? Guid.Empty;
				programType.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)ProgramType.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				programType.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "Code";
				var code = reader[(int)ProgramType.ColumnEnum.Code] ?? string.Empty;
				code = TrimString(code);
				programType.Code = (string)code;

				columnName = "Name";
				var name = reader[(int)ProgramType.ColumnEnum.Name] ?? string.Empty;
				name = TrimString(name);
				programType.Name = (string)name;

				columnName = "TagMultiple";
				var tagMultiple = reader[(int)ProgramType.ColumnEnum.TagMultiple] ?? int.MinValue;
				programType.TagMultiple = (int)tagMultiple;

				columnName = "ImageSetGuid";
				var imageSetGuid = reader[(int)ProgramType.ColumnEnum.ImageSetGuid];
				if (imageSetGuid == DBNull.Value) imageSetGuid = null;
				programType.ImageSetGuid = (Guid?)imageSetGuid;

				columnName = "DefaultPeakWeekNumber";
				var defaultPeakWeekNumber = reader[(int)ProgramType.ColumnEnum.DefaultPeakWeekNumber];
				if (defaultPeakWeekNumber == DBNull.Value) defaultPeakWeekNumber = null;
				programType.DefaultPeakWeekNumber = (int?)defaultPeakWeekNumber;

				columnName = "DefaultSeasonEndWeekNumber";
				var defaultSeasonEndWeekNumber = reader[(int)ProgramType.ColumnEnum.DefaultSeasonEndWeekNumber];
				if (defaultSeasonEndWeekNumber == DBNull.Value) defaultSeasonEndWeekNumber = null;
				programType.DefaultSeasonEndWeekNumber = (int?)defaultSeasonEndWeekNumber;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return programType;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(ProgramType programType)
        {
			SetUpImageSetLazyLoad( programType);
			SetUpGrowerOrderListLazyLoad( programType);
			SetUpProgramListLazyLoad( programType);
            //SetUpPromoCodeListLazyLoad( programType);
		}

        protected override void FixAnyLazyLoadedLists(ProgramType programType, ListActionEnum listAction)
        {
			FixImageSetList( programType, listAction);
		}

        private void SetUpImageSetLazyLoad( ProgramType programType)
        {
            var imageSetTableService = Context.Get<ImageSetTableService>();
            programType.SetLazyImageSet(new Lazy<ImageSet>(() => imageSetTableService.GetByGuid(programType.ImageSetGuid), false));
        }

        private void FixImageSetList( ProgramType programType, ListActionEnum listAction)
        {
            if (programType.ImageSetIsLoaded)
            {
                FixLazyLoadedList(programType.ImageSet.ProgramTypeList, programType, listAction);
            }
        }

        private void SetUpGrowerOrderListLazyLoad(ProgramType programType)
        {
            var growerOrderTableService = Context.Get<GrowerOrderTableService>();
            programType.SetLazyGrowerOrderList(new Lazy<List<GrowerOrder>>(() => growerOrderTableService.GetAllActiveByGuid(programType.Guid, "ProgramTypeGuid"), false));
        }

        private void SetUpProgramListLazyLoad(ProgramType programType)
        {
            var programTableService = Context.Get<ProgramTableService>();
            programType.SetLazyProgramList(new Lazy<List<Program>>(() => programTableService.GetAllActiveByGuid(programType.Guid, "ProgramTypeGuid"), false));
        }

        //private void SetUpPromoCodeListLazyLoad(ProgramType programType)
        //{
        //    var promoCodeTableService = Context.Get<PromoCodeTableService>();
        //    programType.SetLazyPromoCodeList(new Lazy<List<PromoCode>>(() => promoCodeTableService.GetAllActiveByGuid(programType.Guid, "ProgramTypeGuid"), false));
        //}

		#endregion
	}
}

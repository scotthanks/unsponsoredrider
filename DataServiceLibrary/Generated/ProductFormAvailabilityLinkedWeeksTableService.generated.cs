using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class ProductFormAvailabilityLinkedWeeksTableService : TableService<ProductFormAvailabilityLinkedWeeks>
    {
        public ProductFormAvailabilityLinkedWeeksTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override ProductFormAvailabilityLinkedWeeks DefaultInsert(ProductFormAvailabilityLinkedWeeks productFormAvailabilityLinkedWeeks, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [ProductFormAvailabilityLinkedWeeks]([Guid], [DateDeactivated], [GroupingCode], [ProductFormGuid], [ShipWeekGuid], ) VALUES (@Guid, @DateDeactivated, @GroupingCode, @ProductFormGuid, @ShipWeekGuid, );SELECT * FROM [ProductFormAvailabilityLinkedWeeks] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, productFormAvailabilityLinkedWeeks, transaction: transaction);
        }

        protected override ProductFormAvailabilityLinkedWeeks DefaultUpdate(ProductFormAvailabilityLinkedWeeks productFormAvailabilityLinkedWeeks, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [ProductFormAvailabilityLinkedWeeks] SET [DateDeactivated]=@DateDeactivated, [GroupingCode]=@GroupingCode, [ProductFormGuid]=@ProductFormGuid, [ShipWeekGuid]=@ShipWeekGuid, WHERE [Guid]=@Guid;SELECT * FROM [ProductFormAvailabilityLinkedWeeks] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, productFormAvailabilityLinkedWeeks, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, ProductFormAvailabilityLinkedWeeks productFormAvailabilityLinkedWeeks)
        {
            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = productFormAvailabilityLinkedWeeks.Id;
            command.Parameters.Add(idParameter);

            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = productFormAvailabilityLinkedWeeks.Guid;
            command.Parameters.Add(guidParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = productFormAvailabilityLinkedWeeks.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

            var groupingCodeParameter = new SqlParameter("@GroupingCode", SqlDbType.Int, 4);
            groupingCodeParameter.IsNullable = false;
            groupingCodeParameter.Value = productFormAvailabilityLinkedWeeks.GroupingCode;
            command.Parameters.Add(groupingCodeParameter);

            var productFormGuidParameter = new SqlParameter("@ProductFormGuid", SqlDbType.UniqueIdentifier, 16);
            productFormGuidParameter.IsNullable = false;
            productFormGuidParameter.Value = productFormAvailabilityLinkedWeeks.ProductFormGuid;
            command.Parameters.Add(productFormGuidParameter);

            var shipWeekGuidParameter = new SqlParameter("@ShipWeekGuid", SqlDbType.UniqueIdentifier, 16);
            shipWeekGuidParameter.IsNullable = false;
            shipWeekGuidParameter.Value = productFormAvailabilityLinkedWeeks.ShipWeekGuid;
            command.Parameters.Add(shipWeekGuidParameter);

        }

        protected override ProductFormAvailabilityLinkedWeeks DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var productFormAvailabilityLinkedWeeks = new ProductFormAvailabilityLinkedWeeks();

            string columnName = "";
            try
            {
                columnName = "Id";
                var id = reader[(int)ProductFormAvailabilityLinkedWeeks.ColumnEnum.Id] ?? long.MinValue;
                productFormAvailabilityLinkedWeeks.Id = (long)id;

                columnName = "Guid";
                var guid = reader[(int)ProductFormAvailabilityLinkedWeeks.ColumnEnum.Guid] ?? Guid.Empty;
                productFormAvailabilityLinkedWeeks.Guid = (Guid)guid;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)ProductFormAvailabilityLinkedWeeks.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                productFormAvailabilityLinkedWeeks.DateDeactivated = (DateTime?)dateDeactivated;

                columnName = "GroupingCode";
                var groupingCode = reader[(int)ProductFormAvailabilityLinkedWeeks.ColumnEnum.GroupingCode] ?? int.MinValue;
                productFormAvailabilityLinkedWeeks.GroupingCode = (int)groupingCode;

                columnName = "ProductFormGuid";
                var productFormGuid = reader[(int)ProductFormAvailabilityLinkedWeeks.ColumnEnum.ProductFormGuid] ?? Guid.Empty;
                productFormAvailabilityLinkedWeeks.ProductFormGuid = (Guid)productFormGuid;

                columnName = "ShipWeekGuid";
                var shipWeekGuid = reader[(int)ProductFormAvailabilityLinkedWeeks.ColumnEnum.ShipWeekGuid] ?? Guid.Empty;
                productFormAvailabilityLinkedWeeks.ShipWeekGuid = (Guid)shipWeekGuid;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return productFormAvailabilityLinkedWeeks;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(ProductFormAvailabilityLinkedWeeks productFormAvailabilityLinkedWeeks)
        {
            SetUpProductFormLazyLoad(productFormAvailabilityLinkedWeeks);
            SetUpShipWeekLazyLoad(productFormAvailabilityLinkedWeeks);
        }

        protected override void FixAnyLazyLoadedLists(ProductFormAvailabilityLinkedWeeks productFormAvailabilityLinkedWeeks, ListActionEnum listAction)
        {
            FixProductFormList(productFormAvailabilityLinkedWeeks, listAction);
            FixShipWeekList(productFormAvailabilityLinkedWeeks, listAction);
        }

        private void SetUpProductFormLazyLoad(ProductFormAvailabilityLinkedWeeks productFormAvailabilityLinkedWeeks)
        {
            var productFormTableService = Context.Get<ProductFormTableService>();
            productFormAvailabilityLinkedWeeks.SetLazyProductForm(new Lazy<ProductForm>(() => productFormTableService.GetByGuid(productFormAvailabilityLinkedWeeks.ProductFormGuid), false));
        }

        private void FixProductFormList(ProductFormAvailabilityLinkedWeeks productFormAvailabilityLinkedWeeks, ListActionEnum listAction)
        {
            //if (productFormAvailabilityLinkedWeeks.ProductFormIsLoaded)
            //{
            //    FixLazyLoadedList(productFormAvailabilityLinkedWeeks.ProductForm.ProductFormAvailabilityLinkedWeeksList, productFormAvailabilityLinkedWeeks, listAction);
            //}
        }

        private void SetUpShipWeekLazyLoad(ProductFormAvailabilityLinkedWeeks productFormAvailabilityLinkedWeeks)
        {
            var shipWeekTableService = Context.Get<ShipWeekTableService>();
            productFormAvailabilityLinkedWeeks.SetLazyShipWeek(new Lazy<ShipWeek>(() => shipWeekTableService.GetByGuid(productFormAvailabilityLinkedWeeks.ShipWeekGuid), false));
        }

        private void FixShipWeekList(ProductFormAvailabilityLinkedWeeks productFormAvailabilityLinkedWeeks, ListActionEnum listAction)
        {
            //if (productFormAvailabilityLinkedWeeks.ShipWeekIsLoaded)
            //{
            //    FixLazyLoadedList(productFormAvailabilityLinkedWeeks.ShipWeek.ProductFormAvailabilityLinkedWeeksList, productFormAvailabilityLinkedWeeks, listAction);
            //}
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class StateTableService : TableService<State>
	{
		public StateTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override State DefaultInsert( State state, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [State]([Guid], [Code], [Name], [CountryGuid], ) VALUES (@Guid, @Code, @Name, @CountryGuid, );SELECT * FROM [State] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, state, transaction: transaction);
		}

		protected override State DefaultUpdate( State state, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [State] SET [Code]=@Code, [Name]=@Name, [CountryGuid]=@CountryGuid, WHERE [Guid]=@Guid;SELECT * FROM [State] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, state, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, State state)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = state.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = state.Guid;
			command.Parameters.Add(guidParameter);

			var codeParameter = new SqlParameter( "@Code", SqlDbType.NChar, 2);
			codeParameter.IsNullable = false;
			state.Code = state.Code ?? "";
			state.Code = state.Code.Trim();
			codeParameter.Value = state.Code;
			command.Parameters.Add(codeParameter);

			var nameParameter = new SqlParameter( "@Name", SqlDbType.NVarChar, 30);
			nameParameter.IsNullable = false;
			state.Name = state.Name ?? "";
			state.Name = state.Name.Trim();
			nameParameter.Value = state.Name;
			command.Parameters.Add(nameParameter);

			var countryGuidParameter = new SqlParameter( "@CountryGuid", SqlDbType.UniqueIdentifier, 16);
			countryGuidParameter.IsNullable = false;
			countryGuidParameter.Value = state.CountryGuid;
			command.Parameters.Add(countryGuidParameter);

		}

        protected override State DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var state = new State();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)State.ColumnEnum.Id] ?? long.MinValue;
				state.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)State.ColumnEnum.Guid] ?? Guid.Empty;
				state.Guid = (Guid)guid;

				columnName = "Code";
				var code = reader[(int)State.ColumnEnum.Code] ?? string.Empty;
				code = TrimString(code);
				state.Code = (string)code;

				columnName = "Name";
				var name = reader[(int)State.ColumnEnum.Name] ?? string.Empty;
				name = TrimString(name);
				state.Name = (string)name;

				columnName = "CountryGuid";
				var countryGuid = reader[(int)State.ColumnEnum.CountryGuid] ?? Guid.Empty;
				state.CountryGuid = (Guid)countryGuid;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return state;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(State state)
        {
			SetUpCountryLazyLoad( state);
			SetUpAddressListLazyLoad( state);
            //SetUpFreightSupplierStateZoneListLazyLoad( state);
		}

        protected override void FixAnyLazyLoadedLists(State state, ListActionEnum listAction)
        {
			FixCountryList( state, listAction);
		}

        private void SetUpCountryLazyLoad( State state)
        {
            var countryTableService = Context.Get<CountryTableService>();
            state.SetLazyCountry(new Lazy<Country>(() => countryTableService.GetByGuid(state.CountryGuid), false));
        }

        private void FixCountryList( State state, ListActionEnum listAction)
        {
            if (state.CountryIsLoaded)
            {
                FixLazyLoadedList(state.Country.StateList, state, listAction);
            }
        }

        private void SetUpAddressListLazyLoad(State state)
        {
            var addressTableService = Context.Get<AddressTableService>();
            state.SetLazyAddressList(new Lazy<List<Address>>(() => addressTableService.GetAllActiveByGuid(state.Guid, "StateGuid"), false));
        }

        //private void SetUpFreightSupplierStateZoneListLazyLoad(State state)
        //{
        //    var freightSupplierStateZoneTableService = Context.Get<FreightSupplierStateZoneTableService>();
        //    state.SetLazyFreightSupplierStateZoneList(new Lazy<List<FreightSupplierStateZone>>(() => freightSupplierStateZoneTableService.GetAllActiveByGuid(state.Guid, "StateGuid"), false));
        //}

		#endregion
	}
}

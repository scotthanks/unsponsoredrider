using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class SpeciesTableService : TableService<Species>
    {
        public SpeciesTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override Species DefaultInsert(Species species, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [Species]([Guid], [DateDeactivated], [Name], [Code], ) VALUES (@Guid, @DateDeactivated, @Name, @Code, );SELECT * FROM [Species] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, species, transaction: transaction);
        }

        protected override Species DefaultUpdate(Species species, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [Species] SET [DateDeactivated]=@DateDeactivated, [Name]=@Name, [Code]=@Code, WHERE [Guid]=@Guid;SELECT * FROM [Species] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, species, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, Species species)
        {
            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = species.Id;
            command.Parameters.Add(idParameter);

            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = species.Guid;
            command.Parameters.Add(guidParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = species.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

            var nameParameter = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            nameParameter.IsNullable = false;
            species.Name = species.Name ?? "";
            species.Name = species.Name.Trim();
            nameParameter.Value = species.Name;
            command.Parameters.Add(nameParameter);

            var codeParameter = new SqlParameter("@Code", SqlDbType.NChar, 30);
            codeParameter.IsNullable = false;
            species.Code = species.Code ?? "";
            species.Code = species.Code.Trim();
            codeParameter.Value = species.Code;
            command.Parameters.Add(codeParameter);

        }

        protected override Species DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var species = new Species();

            string columnName = "";
            try
            {
                columnName = "Id";
                var id = reader[(int)Species.ColumnEnum.Id] ?? long.MinValue;
                species.Id = (long)id;

                columnName = "Guid";
                var guid = reader[(int)Species.ColumnEnum.Guid] ?? Guid.Empty;
                species.Guid = (Guid)guid;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)Species.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                species.DateDeactivated = (DateTime?)dateDeactivated;

                columnName = "Name";
                var name = reader[(int)Species.ColumnEnum.Name] ?? string.Empty;
                name = TrimString(name);
                species.Name = (string)name;

                columnName = "Code";
                var code = reader[(int)Species.ColumnEnum.Code] ?? string.Empty;
                code = TrimString(code);
                species.Code = (string)code;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return species;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(Species species)
        {
            //SetUpPromoCodeListLazyLoad( species);
            SetUpSeriesListLazyLoad(species);
            SetUpVarietyListLazyLoad(species);
        }

        protected override void FixAnyLazyLoadedLists(Species species, ListActionEnum listAction)
        {
        }

        //private void SetUpPromoCodeListLazyLoad(Species species)
        //{
        //    var promoCodeTableService = Context.Get<PromoCodeTableService>();
        //    species.SetLazyPromoCodeList(new Lazy<List<PromoCode>>(() => promoCodeTableService.GetAllActiveByGuid(species.Guid, "SpeciesGuid"), false));
        //}

        private void SetUpSeriesListLazyLoad(Species species)
        {
            var seriesTableService = Context.Get<SeriesTableService>();
            species.SetLazySeriesList(new Lazy<List<Series>>(() => seriesTableService.GetAllActiveByGuid(species.Guid, "SpeciesGuid"), false));
        }

        private void SetUpVarietyListLazyLoad(Species species)
        {
            var varietyTableService = Context.Get<VarietyTableService>();
            species.SetLazyVarietyList(new Lazy<List<Variety>>(() => varietyTableService.GetAllActiveByGuid(species.Guid, "SpeciesGuid"), false));
        }

        #endregion
    }
}

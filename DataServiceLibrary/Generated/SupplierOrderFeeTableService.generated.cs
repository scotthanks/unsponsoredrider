using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class SupplierOrderFeeTableService : TableService<SupplierOrderFee>
	{
		public SupplierOrderFeeTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override SupplierOrderFee DefaultInsert( SupplierOrderFee supplierOrderFee, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [SupplierOrderFee]([Guid], [DateDeactivated], [SupplierOrderGuid], [FeeTypeLookupGuid], [Amount], [FeeStatXLookupGuid], [FeeDescription], [IsManual], ) VALUES (@Guid, @DateDeactivated, @SupplierOrderGuid, @FeeTypeLookupGuid, @Amount, @FeeStatXLookupGuid, @FeeDescription, @IsManual, );SELECT * FROM [SupplierOrderFee] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, supplierOrderFee, transaction: transaction);
		}

		protected override SupplierOrderFee DefaultUpdate( SupplierOrderFee supplierOrderFee, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [SupplierOrderFee] SET [DateDeactivated]=@DateDeactivated, [SupplierOrderGuid]=@SupplierOrderGuid, [FeeTypeLookupGuid]=@FeeTypeLookupGuid, [Amount]=@Amount, [FeeStatXLookupGuid]=@FeeStatXLookupGuid, [FeeDescription]=@FeeDescription, [IsManual]=@IsManual, WHERE [Guid]=@Guid;SELECT * FROM [SupplierOrderFee] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, supplierOrderFee, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, SupplierOrderFee supplierOrderFee)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = supplierOrderFee.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = supplierOrderFee.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = supplierOrderFee.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var supplierOrderGuidParameter = new SqlParameter( "@SupplierOrderGuid", SqlDbType.UniqueIdentifier, 16);
			supplierOrderGuidParameter.IsNullable = false;
			supplierOrderGuidParameter.Value = supplierOrderFee.SupplierOrderGuid;
			command.Parameters.Add(supplierOrderGuidParameter);

			var feeTypeLookupGuidParameter = new SqlParameter( "@FeeTypeLookupGuid", SqlDbType.UniqueIdentifier, 16);
			feeTypeLookupGuidParameter.IsNullable = false;
			feeTypeLookupGuidParameter.Value = supplierOrderFee.FeeTypeLookupGuid;
			command.Parameters.Add(feeTypeLookupGuidParameter);

			var amountParameter = new SqlParameter( "@Amount", SqlDbType.Decimal, 5);
			amountParameter.IsNullable = false;
			amountParameter.Value = supplierOrderFee.Amount;
			command.Parameters.Add(amountParameter);

			var feeStatXLookupGuidParameter = new SqlParameter( "@FeeStatXLookupGuid", SqlDbType.UniqueIdentifier, 16);
			feeStatXLookupGuidParameter.IsNullable = true;
			feeStatXLookupGuidParameter.Value = supplierOrderFee.FeeStatXLookupGuid ?? (object)DBNull.Value;
			command.Parameters.Add(feeStatXLookupGuidParameter);

			var feeDescriptionParameter = new SqlParameter( "@FeeDescription", SqlDbType.NVarChar, 500);
			feeDescriptionParameter.IsNullable = false;
			supplierOrderFee.FeeDescription = supplierOrderFee.FeeDescription ?? "";
			supplierOrderFee.FeeDescription = supplierOrderFee.FeeDescription.Trim();
			feeDescriptionParameter.Value = supplierOrderFee.FeeDescription;
			command.Parameters.Add(feeDescriptionParameter);

			var isManualParameter = new SqlParameter( "@IsManual", SqlDbType.Bit, 1);
			isManualParameter.IsNullable = false;
			isManualParameter.Value = supplierOrderFee.IsManual;
			command.Parameters.Add(isManualParameter);

		}

        protected override SupplierOrderFee DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var supplierOrderFee = new SupplierOrderFee();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)SupplierOrderFee.ColumnEnum.Id] ?? long.MinValue;
				supplierOrderFee.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)SupplierOrderFee.ColumnEnum.Guid] ?? Guid.Empty;
				supplierOrderFee.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)SupplierOrderFee.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				supplierOrderFee.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "SupplierOrderGuid";
				var supplierOrderGuid = reader[(int)SupplierOrderFee.ColumnEnum.SupplierOrderGuid] ?? Guid.Empty;
				supplierOrderFee.SupplierOrderGuid = (Guid)supplierOrderGuid;

				columnName = "FeeTypeLookupGuid";
				var feeTypeLookupGuid = reader[(int)SupplierOrderFee.ColumnEnum.FeeTypeLookupGuid] ?? Guid.Empty;
				supplierOrderFee.FeeTypeLookupGuid = (Guid)feeTypeLookupGuid;

				columnName = "Amount";
				var amount = reader[(int)SupplierOrderFee.ColumnEnum.Amount] ?? Decimal.MinValue;
				supplierOrderFee.Amount = (Decimal)amount;

				columnName = "FeeStatXLookupGuid";
				var feeStatXLookupGuid = reader[(int)SupplierOrderFee.ColumnEnum.FeeStatXLookupGuid];
				if (feeStatXLookupGuid == DBNull.Value) feeStatXLookupGuid = null;
				supplierOrderFee.FeeStatXLookupGuid = (Guid?)feeStatXLookupGuid;

				columnName = "FeeDescription";
				var feeDescription = reader[(int)SupplierOrderFee.ColumnEnum.FeeDescription] ?? string.Empty;
				feeDescription = TrimString(feeDescription);
				supplierOrderFee.FeeDescription = (string)feeDescription;

				columnName = "IsManual";
				var isManual = reader[(int)SupplierOrderFee.ColumnEnum.IsManual] ?? false;
				supplierOrderFee.IsManual = (bool)isManual;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return supplierOrderFee;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(SupplierOrderFee supplierOrderFee)
        {
			SetUpSupplierOrderLazyLoad( supplierOrderFee);
			SetUpFeeTypeLookupLazyLoad( supplierOrderFee);
			SetUpFeeStatXLookupLazyLoad( supplierOrderFee);
		}

        protected override void FixAnyLazyLoadedLists(SupplierOrderFee supplierOrderFee, ListActionEnum listAction)
        {
			FixSupplierOrderList( supplierOrderFee, listAction);
			FixFeeTypeLookupList( supplierOrderFee, listAction);
			FixFeeStatXLookupList( supplierOrderFee, listAction);
		}

        private void SetUpSupplierOrderLazyLoad( SupplierOrderFee supplierOrderFee)
        {
            var supplierOrderTableService = Context.Get<SupplierOrderTableService>();
            supplierOrderFee.SetLazySupplierOrder(new Lazy<SupplierOrder>(() => supplierOrderTableService.GetByGuid(supplierOrderFee.SupplierOrderGuid), false));
        }

        private void FixSupplierOrderList( SupplierOrderFee supplierOrderFee, ListActionEnum listAction)
        {
            if (supplierOrderFee.SupplierOrderIsLoaded)
            {
                FixLazyLoadedList(supplierOrderFee.SupplierOrder.SupplierOrderFeeList, supplierOrderFee, listAction);
            }
        }

        private void SetUpFeeTypeLookupLazyLoad( SupplierOrderFee supplierOrderFee)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            supplierOrderFee.SetLazyFeeTypeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(supplierOrderFee.FeeTypeLookupGuid), false));
        }

        private void FixFeeTypeLookupList( SupplierOrderFee supplierOrderFee, ListActionEnum listAction)
        {
            if (supplierOrderFee.FeeTypeLookupIsLoaded)
            {
                FixLazyLoadedList(supplierOrderFee.FeeTypeLookup.FeeTypeSupplierOrderFeeList, supplierOrderFee, listAction);
            }
        }

        private void SetUpFeeStatXLookupLazyLoad( SupplierOrderFee supplierOrderFee)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            supplierOrderFee.SetLazyFeeStatXLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(supplierOrderFee.FeeStatXLookupGuid), false));
        }

        private void FixFeeStatXLookupList( SupplierOrderFee supplierOrderFee, ListActionEnum listAction)
        {
            if (supplierOrderFee.FeeStatXLookupIsLoaded)
            {
                FixLazyLoadedList(supplierOrderFee.FeeStatXLookup.FeeStatXSupplierOrderFeeList, supplierOrderFee, listAction);
            }
        }

		#endregion
	}
}

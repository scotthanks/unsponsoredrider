using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class GrowerOrderSummaryByShipWeekViewService : ViewService<GrowerOrderSummaryByShipWeekView>
	{
        protected override GrowerOrderSummaryByShipWeekView DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var growerOrderSummaryByShipWeekView = new GrowerOrderSummaryByShipWeekView();

			string columnName = "";
            try
            {
				columnName = "GrowerOrderGuid";
				var growerOrderGuid = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.GrowerOrderGuid];
				if (growerOrderGuid == DBNull.Value) growerOrderGuid = null;
				growerOrderSummaryByShipWeekView.GrowerOrderGuid = (Guid?)growerOrderGuid;

				columnName = "GrowerGuid";
				var growerGuid = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.GrowerGuid];
				if (growerGuid == DBNull.Value) growerGuid = null;
				growerOrderSummaryByShipWeekView.GrowerGuid = (Guid?)growerGuid;

				columnName = "OrderTypeLookupGuid";
				var orderTypeLookupGuid = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.OrderTypeLookupGuid];
				if (orderTypeLookupGuid == DBNull.Value) orderTypeLookupGuid = null;
				growerOrderSummaryByShipWeekView.OrderTypeLookupGuid = (Guid?)orderTypeLookupGuid;

				columnName = "OrderTypeLookupCode";
				var orderTypeLookupCode = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.OrderTypeLookupCode];
				if (orderTypeLookupCode == DBNull.Value) orderTypeLookupCode = null;
				orderTypeLookupCode = TrimString(orderTypeLookupCode);
				growerOrderSummaryByShipWeekView.OrderTypeLookupCode = (string)orderTypeLookupCode;

				columnName = "ProgramTypeGuid";
				var programTypeGuid = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.ProgramTypeGuid] ?? Guid.Empty;
				growerOrderSummaryByShipWeekView.ProgramTypeGuid = (Guid)programTypeGuid;

				columnName = "ProgramTypeCode";
				var programTypeCode = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.ProgramTypeCode] ?? string.Empty;
				programTypeCode = TrimString(programTypeCode);
				growerOrderSummaryByShipWeekView.ProgramTypeCode = (string)programTypeCode;

				columnName = "ProductFormCategoryGuid";
				var productFormCategoryGuid = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.ProductFormCategoryGuid] ?? Guid.Empty;
				growerOrderSummaryByShipWeekView.ProductFormCategoryGuid = (Guid)productFormCategoryGuid;

				columnName = "ProductFormCategoryCode";
				var productFormCategoryCode = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.ProductFormCategoryCode] ?? string.Empty;
				productFormCategoryCode = TrimString(productFormCategoryCode);
				growerOrderSummaryByShipWeekView.ProductFormCategoryCode = (string)productFormCategoryCode;

				columnName = "ShipWeekGuid";
				var shipWeekGuid = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.ShipWeekGuid];
				if (shipWeekGuid == DBNull.Value) shipWeekGuid = null;
				growerOrderSummaryByShipWeekView.ShipWeekGuid = (Guid?)shipWeekGuid;

				columnName = "ShipWeekCode";
				var shipWeekCode = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.ShipWeekCode];
				if (shipWeekCode == DBNull.Value) shipWeekCode = null;
				shipWeekCode = TrimString(shipWeekCode);
				growerOrderSummaryByShipWeekView.ShipWeekCode = (string)shipWeekCode;

				columnName = "SupplierOrderCount";
				var supplierOrderCount = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.SupplierOrderCount] ?? int.MinValue;
				growerOrderSummaryByShipWeekView.SupplierOrderCount = (int)supplierOrderCount;

				columnName = "SupplierOrderWithOrderLinesCount";
				var supplierOrderWithOrderLinesCount = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.SupplierOrderWithOrderLinesCount] ?? int.MinValue;
				growerOrderSummaryByShipWeekView.SupplierOrderWithOrderLinesCount = (int)supplierOrderWithOrderLinesCount;

				columnName = "ProductCount";
				var productCount = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.ProductCount] ?? int.MinValue;
				growerOrderSummaryByShipWeekView.ProductCount = (int)productCount;

				columnName = "ProductWithNonZeroOrderQtyCount";
				var productWithNonZeroOrderQtyCount = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.ProductWithNonZeroOrderQtyCount] ?? int.MinValue;
				growerOrderSummaryByShipWeekView.ProductWithNonZeroOrderQtyCount = (int)productWithNonZeroOrderQtyCount;

				columnName = "OrderLineCount";
				var orderLineCount = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.OrderLineCount] ?? int.MinValue;
				growerOrderSummaryByShipWeekView.OrderLineCount = (int)orderLineCount;

				columnName = "OrderLineWithNonZeroOrderQtyCount";
				var orderLineWithNonZeroOrderQtyCount = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.OrderLineWithNonZeroOrderQtyCount];
				if (orderLineWithNonZeroOrderQtyCount == DBNull.Value) orderLineWithNonZeroOrderQtyCount = null;
				growerOrderSummaryByShipWeekView.OrderLineWithNonZeroOrderQtyCount = (int?)orderLineWithNonZeroOrderQtyCount;

				columnName = "QtyOrderedCount";
				var qtyOrderedCount = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.QtyOrderedCount] ?? int.MinValue;
				growerOrderSummaryByShipWeekView.QtyOrderedCount = (int)qtyOrderedCount;

				columnName = "GrowerOrderStatusLookupGuid";
				var growerOrderStatusLookupGuid = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.GrowerOrderStatusLookupGuid];
				if (growerOrderStatusLookupGuid == DBNull.Value) growerOrderStatusLookupGuid = null;
				growerOrderSummaryByShipWeekView.GrowerOrderStatusLookupGuid = (Guid?)growerOrderStatusLookupGuid;

				columnName = "GrowerOrderStatusLookupCode";
				var growerOrderStatusLookupCode = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.GrowerOrderStatusLookupCode];
				if (growerOrderStatusLookupCode == DBNull.Value) growerOrderStatusLookupCode = null;
				growerOrderStatusLookupCode = TrimString(growerOrderStatusLookupCode);
				growerOrderSummaryByShipWeekView.GrowerOrderStatusLookupCode = (string)growerOrderStatusLookupCode;

				columnName = "LowestSupplierOrderStatusLookupGuid";
				var lowestSupplierOrderStatusLookupGuid = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.LowestSupplierOrderStatusLookupGuid] ?? Guid.Empty;
				growerOrderSummaryByShipWeekView.LowestSupplierOrderStatusLookupGuid = (Guid)lowestSupplierOrderStatusLookupGuid;

				columnName = "LowestSupplierOrderStatusLookupCode";
				var lowestSupplierOrderStatusLookupCode = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.LowestSupplierOrderStatusLookupCode] ?? string.Empty;
				lowestSupplierOrderStatusLookupCode = TrimString(lowestSupplierOrderStatusLookupCode);
				growerOrderSummaryByShipWeekView.LowestSupplierOrderStatusLookupCode = (string)lowestSupplierOrderStatusLookupCode;

				columnName = "LowestOrderLineStatusLookupGuid";
				var lowestOrderLineStatusLookupGuid = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.LowestOrderLineStatusLookupGuid] ?? Guid.Empty;
				growerOrderSummaryByShipWeekView.LowestOrderLineStatusLookupGuid = (Guid)lowestOrderLineStatusLookupGuid;

				columnName = "LowestOrderLineStatusLookupCode";
				var lowestOrderLineStatusLookupCode = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.LowestOrderLineStatusLookupCode] ?? string.Empty;
				lowestOrderLineStatusLookupCode = TrimString(lowestOrderLineStatusLookupCode);
				growerOrderSummaryByShipWeekView.LowestOrderLineStatusLookupCode = (string)lowestOrderLineStatusLookupCode;

				columnName = "PersonGuid";
				var personGuid = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.PersonGuid];
				if (personGuid == DBNull.Value) personGuid = null;
				growerOrderSummaryByShipWeekView.PersonGuid = (Guid?)personGuid;

				columnName = "CountOfPendingStatusLines";
				var countOfPendingStatusLines = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.CountOfPendingStatusLines];
				if (countOfPendingStatusLines == DBNull.Value) countOfPendingStatusLines = null;
				growerOrderSummaryByShipWeekView.CountOfPendingStatusLines = (int?)countOfPendingStatusLines;

				columnName = "InvoiceNo";
				var invoiceNo = reader[(int)GrowerOrderSummaryByShipWeekView.ColumnEnum.InvoiceNo] ?? string.Empty;
				invoiceNo = TrimString(invoiceNo);
				growerOrderSummaryByShipWeekView.InvoiceNo = (string)invoiceNo;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return growerOrderSummaryByShipWeekView;
        }
	}
}

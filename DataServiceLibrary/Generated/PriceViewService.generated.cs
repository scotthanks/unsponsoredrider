using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class PriceViewService : ViewService<PriceView>
	{
        protected override PriceView DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var priceView = new PriceView();

			string columnName = "";
            try
            {
				columnName = "RegularCost";
				var regularCost = reader[(int)PriceView.ColumnEnum.RegularCost] ?? Decimal.MinValue;
				priceView.RegularCost = (Decimal)regularCost;

				columnName = "EODCost";
				var eODCost = reader[(int)PriceView.ColumnEnum.EODCost] ?? Decimal.MinValue;
				priceView.EODCost = (Decimal)eODCost;

				columnName = "RegularMUPercent";
				var regularMUPercent = reader[(int)PriceView.ColumnEnum.RegularMUPercent] ?? Double.MinValue;
				priceView.RegularMUPercent = (Double)regularMUPercent;

				columnName = "EODMUPercent";
				var eODMUPercent = reader[(int)PriceView.ColumnEnum.EODMUPercent] ?? Double.MinValue;
				priceView.EODMUPercent = (Double)eODMUPercent;

				columnName = "PriceGroupCode";
				var priceGroupCode = reader[(int)PriceView.ColumnEnum.PriceGroupCode] ?? string.Empty;
				priceGroupCode = TrimString(priceGroupCode);
				priceView.PriceGroupCode = (string)priceGroupCode;

				columnName = "PriceGroupName";
				var priceGroupName = reader[(int)PriceView.ColumnEnum.PriceGroupName] ?? string.Empty;
				priceGroupName = TrimString(priceGroupName);
				priceView.PriceGroupName = (string)priceGroupName;

				columnName = "TagCost";
				var tagCost = reader[(int)PriceView.ColumnEnum.TagCost] ?? Decimal.MinValue;
				priceView.TagCost = (Decimal)tagCost;

				columnName = "VolumeLevelCode";
				var volumeLevelCode = reader[(int)PriceView.ColumnEnum.VolumeLevelCode] ?? string.Empty;
				volumeLevelCode = TrimString(volumeLevelCode);
				priceView.VolumeLevelCode = (string)volumeLevelCode;

				columnName = "VolumeLevelName";
				var volumeLevelName = reader[(int)PriceView.ColumnEnum.VolumeLevelName] ?? string.Empty;
				volumeLevelName = TrimString(volumeLevelName);
				priceView.VolumeLevelName = (string)volumeLevelName;

				columnName = "VolumeLow";
				var volumeLow = reader[(int)PriceView.ColumnEnum.VolumeLow] ?? int.MinValue;
				priceView.VolumeLow = (int)volumeLow;

				columnName = "VolumeHigh";
				var volumeHigh = reader[(int)PriceView.ColumnEnum.VolumeHigh] ?? int.MinValue;
				priceView.VolumeHigh = (int)volumeHigh;

				columnName = "ProgramSeasonCode";
				var programSeasonCode = reader[(int)PriceView.ColumnEnum.ProgramSeasonCode] ?? string.Empty;
				programSeasonCode = TrimString(programSeasonCode);
				priceView.ProgramSeasonCode = (string)programSeasonCode;

				columnName = "ProgramSeasonName";
				var programSeasonName = reader[(int)PriceView.ColumnEnum.ProgramSeasonName] ?? string.Empty;
				programSeasonName = TrimString(programSeasonName);
				priceView.ProgramSeasonName = (string)programSeasonName;

				columnName = "SeasonStartDate";
				var seasonStartDate = reader[(int)PriceView.ColumnEnum.SeasonStartDate] ?? DateTime.MinValue;
				priceView.SeasonStartDate = (DateTime)seasonStartDate;

				columnName = "SeasonEndDate";
				var seasonEndDate = reader[(int)PriceView.ColumnEnum.SeasonEndDate] ?? DateTime.MinValue;
				priceView.SeasonEndDate = (DateTime)seasonEndDate;

				columnName = "EODDate";
				var eODDate = reader[(int)PriceView.ColumnEnum.EODDate] ?? DateTime.MinValue;
				priceView.EODDate = (DateTime)eODDate;

				columnName = "ProgramCode";
				var programCode = reader[(int)PriceView.ColumnEnum.ProgramCode] ?? string.Empty;
				programCode = TrimString(programCode);
				priceView.ProgramCode = (string)programCode;

				columnName = "ProgramTypeCode";
				var programTypeCode = reader[(int)PriceView.ColumnEnum.ProgramTypeCode] ?? string.Empty;
				programTypeCode = TrimString(programTypeCode);
				priceView.ProgramTypeCode = (string)programTypeCode;

				columnName = "SupplierCode";
				var supplierCode = reader[(int)PriceView.ColumnEnum.SupplierCode] ?? string.Empty;
				supplierCode = TrimString(supplierCode);
				priceView.SupplierCode = (string)supplierCode;

				columnName = "SupplierName";
				var supplierName = reader[(int)PriceView.ColumnEnum.SupplierName] ?? string.Empty;
				supplierName = TrimString(supplierName);
				priceView.SupplierName = (string)supplierName;

				columnName = "ProductFormCategoryCode";
				var productFormCategoryCode = reader[(int)PriceView.ColumnEnum.ProductFormCategoryCode] ?? string.Empty;
				productFormCategoryCode = TrimString(productFormCategoryCode);
				priceView.ProductFormCategoryCode = (string)productFormCategoryCode;

				columnName = "PriceId";
				var priceId = reader[(int)PriceView.ColumnEnum.PriceId] ?? long.MinValue;
				priceView.PriceId = (long)priceId;

				columnName = "PriceGuid";
				var priceGuid = reader[(int)PriceView.ColumnEnum.PriceGuid] ?? Guid.Empty;
				priceView.PriceGuid = (Guid)priceGuid;

				columnName = "PriceGroupId";
				var priceGroupId = reader[(int)PriceView.ColumnEnum.PriceGroupId] ?? long.MinValue;
				priceView.PriceGroupId = (long)priceGroupId;

				columnName = "PriceGroupGuid";
				var priceGroupGuid = reader[(int)PriceView.ColumnEnum.PriceGroupGuid] ?? Guid.Empty;
				priceView.PriceGroupGuid = (Guid)priceGroupGuid;

				columnName = "VolumeLevelId";
				var volumeLevelId = reader[(int)PriceView.ColumnEnum.VolumeLevelId] ?? long.MinValue;
				priceView.VolumeLevelId = (long)volumeLevelId;

				columnName = "VolumeLevelGuid";
				var volumeLevelGuid = reader[(int)PriceView.ColumnEnum.VolumeLevelGuid] ?? Guid.Empty;
				priceView.VolumeLevelGuid = (Guid)volumeLevelGuid;

				columnName = "ProgramSeasonId";
				var programSeasonId = reader[(int)PriceView.ColumnEnum.ProgramSeasonId] ?? long.MinValue;
				priceView.ProgramSeasonId = (long)programSeasonId;

				columnName = "ProgramSeasonGuid";
				var programSeasonGuid = reader[(int)PriceView.ColumnEnum.ProgramSeasonGuid] ?? Guid.Empty;
				priceView.ProgramSeasonGuid = (Guid)programSeasonGuid;

				columnName = "ProgramGuid";
				var programGuid = reader[(int)PriceView.ColumnEnum.ProgramGuid] ?? Guid.Empty;
				priceView.ProgramGuid = (Guid)programGuid;

				columnName = "ProgramTypeGuid";
				var programTypeGuid = reader[(int)PriceView.ColumnEnum.ProgramTypeGuid] ?? Guid.Empty;
				priceView.ProgramTypeGuid = (Guid)programTypeGuid;

				columnName = "DefaultPeakWeekNumber";
				var defaultPeakWeekNumber = reader[(int)PriceView.ColumnEnum.DefaultPeakWeekNumber];
				if (defaultPeakWeekNumber == DBNull.Value) defaultPeakWeekNumber = null;
				priceView.DefaultPeakWeekNumber = (int?)defaultPeakWeekNumber;

				columnName = "DefaultSeasonEndWeekNumber";
				var defaultSeasonEndWeekNumber = reader[(int)PriceView.ColumnEnum.DefaultSeasonEndWeekNumber];
				if (defaultSeasonEndWeekNumber == DBNull.Value) defaultSeasonEndWeekNumber = null;
				priceView.DefaultSeasonEndWeekNumber = (int?)defaultSeasonEndWeekNumber;

				columnName = "SupplierGuid";
				var supplierGuid = reader[(int)PriceView.ColumnEnum.SupplierGuid] ?? Guid.Empty;
				priceView.SupplierGuid = (Guid)supplierGuid;

				columnName = "ProductFormCategoryGuid";
				var productFormCategoryGuid = reader[(int)PriceView.ColumnEnum.ProductFormCategoryGuid] ?? Guid.Empty;
				priceView.ProductFormCategoryGuid = (Guid)productFormCategoryGuid;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return priceView;
        }
	}
}

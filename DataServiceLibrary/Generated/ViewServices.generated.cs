using System;
using System.Collections.Generic;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public static partial class ViewServices
	{
		new public enum TypeEnum
		{
            ClaimViewViewService,
            //EventLogViewViewService,
			GrowerOrderSummaryByShipWeekViewViewService,
			LookupViewViewService,
			PriceViewViewService,
			ProductFormCategoryProgramTypeCombinationViewViewService,     
			ProductGrowerSeasonPriceViewViewService,
			ProgramShipMethodViewViewService,
			ProgramTagRatioViewViewService,
		}

		private static List<Type> _typeList;

		new public static List<Type> TypeList
		{
			get
			{
				if (_typeList == null)
				{
					_typeList = new List<Type>
					{
                        typeof( ClaimViewService),
                        //typeof( EventLogViewService),
						typeof( GrowerOrderSummaryByShipWeekViewService),
						typeof( LookupViewService),
						typeof( PriceViewService),
						typeof( ProductFormCategoryProgramTypeCombinationViewService),
						typeof( ProductGrowerSeasonPriceViewService),
						typeof( ProgramShipMethodViewService),
						typeof( ProgramTagRatioViewService),
					};
				}

				return _typeList;
			}
		}
	}
}

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class ProductFormCategoryTableService : TableService<ProductFormCategory>
	{
		public ProductFormCategoryTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override ProductFormCategory DefaultInsert( ProductFormCategory productFormCategory, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [ProductFormCategory]([Guid], [DateDeactivated], [Code], [Name], [IsRooted], [AvailabilityWeekCalcMethodLookupGuid], ) VALUES (@Guid, @DateDeactivated, @Code, @Name, @IsRooted, @AvailabilityWeekCalcMethodLookupGuid, );SELECT * FROM [ProductFormCategory] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, productFormCategory, transaction: transaction);
		}

		protected override ProductFormCategory DefaultUpdate( ProductFormCategory productFormCategory, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [ProductFormCategory] SET [DateDeactivated]=@DateDeactivated, [Code]=@Code, [Name]=@Name, [IsRooted]=@IsRooted, [AvailabilityWeekCalcMethodLookupGuid]=@AvailabilityWeekCalcMethodLookupGuid, WHERE [Guid]=@Guid;SELECT * FROM [ProductFormCategory] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, productFormCategory, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, ProductFormCategory productFormCategory)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = productFormCategory.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = productFormCategory.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = productFormCategory.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var codeParameter = new SqlParameter( "@Code", SqlDbType.NChar, 10);
			codeParameter.IsNullable = false;
			productFormCategory.Code = productFormCategory.Code ?? "";
			productFormCategory.Code = productFormCategory.Code.Trim();
			codeParameter.Value = productFormCategory.Code;
			command.Parameters.Add(codeParameter);

			var nameParameter = new SqlParameter( "@Name", SqlDbType.NVarChar, 50);
			nameParameter.IsNullable = false;
			productFormCategory.Name = productFormCategory.Name ?? "";
			productFormCategory.Name = productFormCategory.Name.Trim();
			nameParameter.Value = productFormCategory.Name;
			command.Parameters.Add(nameParameter);

			var isRootedParameter = new SqlParameter( "@IsRooted", SqlDbType.Bit, 1);
			isRootedParameter.IsNullable = false;
			isRootedParameter.Value = productFormCategory.IsRooted;
			command.Parameters.Add(isRootedParameter);

			var availabilityWeekCalcMethodLookupGuidParameter = new SqlParameter( "@AvailabilityWeekCalcMethodLookupGuid", SqlDbType.UniqueIdentifier, 16);
			availabilityWeekCalcMethodLookupGuidParameter.IsNullable = false;
			availabilityWeekCalcMethodLookupGuidParameter.Value = productFormCategory.AvailabilityWeekCalcMethodLookupGuid;
			command.Parameters.Add(availabilityWeekCalcMethodLookupGuidParameter);

		}

        protected override ProductFormCategory DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var productFormCategory = new ProductFormCategory();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)ProductFormCategory.ColumnEnum.Id] ?? long.MinValue;
				productFormCategory.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)ProductFormCategory.ColumnEnum.Guid] ?? Guid.Empty;
				productFormCategory.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)ProductFormCategory.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				productFormCategory.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "Code";
				var code = reader[(int)ProductFormCategory.ColumnEnum.Code] ?? string.Empty;
				code = TrimString(code);
				productFormCategory.Code = (string)code;

				columnName = "Name";
				var name = reader[(int)ProductFormCategory.ColumnEnum.Name] ?? string.Empty;
				name = TrimString(name);
				productFormCategory.Name = (string)name;

				columnName = "IsRooted";
				var isRooted = reader[(int)ProductFormCategory.ColumnEnum.IsRooted] ?? false;
				productFormCategory.IsRooted = (bool)isRooted;

				columnName = "AvailabilityWeekCalcMethodLookupGuid";
				var availabilityWeekCalcMethodLookupGuid = reader[(int)ProductFormCategory.ColumnEnum.AvailabilityWeekCalcMethodLookupGuid] ?? Guid.Empty;
				productFormCategory.AvailabilityWeekCalcMethodLookupGuid = (Guid)availabilityWeekCalcMethodLookupGuid;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return productFormCategory;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(ProductFormCategory productFormCategory)
        {
			SetUpAvailabilityWeekCalcMethodLookupLazyLoad( productFormCategory);
			SetUpGrowerOrderListLazyLoad( productFormCategory);
			SetUpProductFormListLazyLoad( productFormCategory);
			SetUpProgramListLazyLoad( productFormCategory);
            //SetUpPromoCodeListLazyLoad( productFormCategory);
            //SetUpSearchVarietyHelperListLazyLoad( productFormCategory);
		}

        protected override void FixAnyLazyLoadedLists(ProductFormCategory productFormCategory, ListActionEnum listAction)
        {
			FixAvailabilityWeekCalcMethodLookupList( productFormCategory, listAction);
		}

        private void SetUpAvailabilityWeekCalcMethodLookupLazyLoad( ProductFormCategory productFormCategory)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            productFormCategory.SetLazyAvailabilityWeekCalcMethodLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(productFormCategory.AvailabilityWeekCalcMethodLookupGuid), false));
        }

        private void FixAvailabilityWeekCalcMethodLookupList( ProductFormCategory productFormCategory, ListActionEnum listAction)
        {
            if (productFormCategory.AvailabilityWeekCalcMethodLookupIsLoaded)
            {
                FixLazyLoadedList(productFormCategory.AvailabilityWeekCalcMethodLookup.AvailabilityWeekCalcMethodProductFormCategoryList, productFormCategory, listAction);
            }
        }

        private void SetUpGrowerOrderListLazyLoad(ProductFormCategory productFormCategory)
        {
            var growerOrderTableService = Context.Get<GrowerOrderTableService>();
            productFormCategory.SetLazyGrowerOrderList(new Lazy<List<GrowerOrder>>(() => growerOrderTableService.GetAllActiveByGuid(productFormCategory.Guid, "ProductFormCategoryGuid"), false));
        }

        private void SetUpProductFormListLazyLoad(ProductFormCategory productFormCategory)
        {
            var productFormTableService = Context.Get<ProductFormTableService>();
            productFormCategory.SetLazyProductFormList(new Lazy<List<ProductForm>>(() => productFormTableService.GetAllActiveByGuid(productFormCategory.Guid, "ProductFormCategoryGuid"), false));
        }

        private void SetUpProgramListLazyLoad(ProductFormCategory productFormCategory)
        {
            var programTableService = Context.Get<ProgramTableService>();
            productFormCategory.SetLazyProgramList(new Lazy<List<Program>>(() => programTableService.GetAllActiveByGuid(productFormCategory.Guid, "ProductFormCategoryGuid"), false));
        }

        //private void SetUpPromoCodeListLazyLoad(ProductFormCategory productFormCategory)
        //{
        //    var promoCodeTableService = Context.Get<PromoCodeTableService>();
        //    productFormCategory.SetLazyPromoCodeList(new Lazy<List<PromoCode>>(() => promoCodeTableService.GetAllActiveByGuid(productFormCategory.Guid, "ProductFormCategoryGuid"), false));
        //}

        //private void SetUpSearchVarietyHelperListLazyLoad(ProductFormCategory productFormCategory)
        //{
        //    var searchVarietyHelperTableService = Context.Get<SearchVarietyHelperTableService>();
        //    productFormCategory.SetLazySearchVarietyHelperList(new Lazy<List<SearchVarietyHelper>>(() => searchVarietyHelperTableService.GetAllActiveByGuid(productFormCategory.Guid, "ProductFormCategoryGuid"), false));
        //}

		#endregion
	}
}

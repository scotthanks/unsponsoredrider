using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class ProgramShipMethodViewService : ViewService<ProgramShipMethodView>
	{
        protected override ProgramShipMethodView DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var programShipMethodView = new ProgramShipMethodView();

			string columnName = "";
            try
            {
				columnName = "ProgramGuid";
				var programGuid = reader[(int)ProgramShipMethodView.ColumnEnum.ProgramGuid] ?? Guid.Empty;
				programShipMethodView.ProgramGuid = (Guid)programGuid;

				columnName = "ShipMethodLookupGuid";
				var shipMethodLookupGuid = reader[(int)ProgramShipMethodView.ColumnEnum.ShipMethodLookupGuid] ?? Guid.Empty;
				programShipMethodView.ShipMethodLookupGuid = (Guid)shipMethodLookupGuid;

				columnName = "IsDefault";
				var isDefault = reader[(int)ProgramShipMethodView.ColumnEnum.IsDefault];
				if (isDefault == DBNull.Value) isDefault = null;
				programShipMethodView.IsDefault = Convert.ToBoolean(isDefault);

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return programShipMethodView;
        }
	}
}

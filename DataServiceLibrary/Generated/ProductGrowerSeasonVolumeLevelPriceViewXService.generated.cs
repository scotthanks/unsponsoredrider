using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    //public partial class ProductGrowerSeasonVolumeLevelPriceViewXService : ViewService<ProductGrowerSeasonVolumeLevelPriceViewX>
    //{
    //    protected override ProductGrowerSeasonVolumeLevelPriceViewX DefaultLoadFromSqlDataReader(SqlDataReader reader)
    //    {
    //        var productGrowerSeasonVolumeLevelPriceViewX = new ProductGrowerSeasonVolumeLevelPriceViewX();

    //        string columnName = "";
    //        try
    //        {
    //            columnName = "ProductGuid";
    //            var productGuid = reader[(int)ProductGrowerSeasonVolumeLevelPriceViewX.ColumnEnum.ProductGuid] ?? Guid.Empty;
    //            productGrowerSeasonVolumeLevelPriceViewX.ProductGuid = (Guid)productGuid;

    //            columnName = "GrowerGuid";
    //            var growerGuid = reader[(int)ProductGrowerSeasonVolumeLevelPriceViewX.ColumnEnum.GrowerGuid] ?? Guid.Empty;
    //            productGrowerSeasonVolumeLevelPriceViewX.GrowerGuid = (Guid)growerGuid;

    //            columnName = "ProgramSeasonGuid";
    //            var programSeasonGuid = reader[(int)ProductGrowerSeasonVolumeLevelPriceViewX.ColumnEnum.ProgramSeasonGuid] ?? Guid.Empty;
    //            productGrowerSeasonVolumeLevelPriceViewX.ProgramSeasonGuid = (Guid)programSeasonGuid;

    //            columnName = "SeasonStartDate";
    //            var seasonStartDate = reader[(int)ProductGrowerSeasonVolumeLevelPriceViewX.ColumnEnum.SeasonStartDate] ?? DateTime.MinValue;
    //            productGrowerSeasonVolumeLevelPriceViewX.SeasonStartDate = (DateTime)seasonStartDate;

    //            columnName = "SeasonEndDate";
    //            var seasonEndDate = reader[(int)ProductGrowerSeasonVolumeLevelPriceViewX.ColumnEnum.SeasonEndDate] ?? DateTime.MinValue;
    //            productGrowerSeasonVolumeLevelPriceViewX.SeasonEndDate = (DateTime)seasonEndDate;

    //            columnName = "PriceGuid";
    //            var priceGuid = reader[(int)ProductGrowerSeasonVolumeLevelPriceViewX.ColumnEnum.PriceGuid] ?? Guid.Empty;
    //            productGrowerSeasonVolumeLevelPriceViewX.PriceGuid = (Guid)priceGuid;

    //            columnName = "GrowerVolumeAndPriceHaveSameSeason";
    //            var growerVolumeAndPriceHaveSameSeason = reader[(int)ProductGrowerSeasonVolumeLevelPriceViewX.ColumnEnum.GrowerVolumeAndPriceHaveSameSeason];
    //            if (growerVolumeAndPriceHaveSameSeason == DBNull.Value) growerVolumeAndPriceHaveSameSeason = null;
    //            productGrowerSeasonVolumeLevelPriceViewX.GrowerVolumeAndPriceHaveSameSeason = (bool?)growerVolumeAndPriceHaveSameSeason;

    //            columnName = "ProductPriceGroupProgramSeasonAndPriceHaveSameSeason";
    //            var productPriceGroupProgramSeasonAndPriceHaveSameSeason = reader[(int)ProductGrowerSeasonVolumeLevelPriceViewX.ColumnEnum.ProductPriceGroupProgramSeasonAndPriceHaveSameSeason];
    //            if (productPriceGroupProgramSeasonAndPriceHaveSameSeason == DBNull.Value) productPriceGroupProgramSeasonAndPriceHaveSameSeason = null;
    //            productGrowerSeasonVolumeLevelPriceViewX.ProductPriceGroupProgramSeasonAndPriceHaveSameSeason = (bool?)productPriceGroupProgramSeasonAndPriceHaveSameSeason;

    //        }
    //        catch( Exception e)
    //        {
    //            ThrowReaderException(e, columnName);
    //        }

    //        return productGrowerSeasonVolumeLevelPriceViewX;
    //    }
    //}
}

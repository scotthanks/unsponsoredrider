using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class ReportedAvailabilityTableService : TableService<ReportedAvailability>
	{
		public ReportedAvailabilityTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override ReportedAvailability DefaultInsert( ReportedAvailability reportedAvailability, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [ReportedAvailability]([Guid], [DateReported], [ProductGuid], [ShipWeekGuid], [Qty], [AvailabilityTypeLookupGuid], [SalesSinceDateReported], ) VALUES (@Guid, @DateReported, @ProductGuid, @ShipWeekGuid, @Qty, @AvailabilityTypeLookupGuid, @SalesSinceDateReported, );SELECT * FROM [ReportedAvailability] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, reportedAvailability, transaction: transaction);
		}

		protected override ReportedAvailability DefaultUpdate( ReportedAvailability reportedAvailability, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [ReportedAvailability] SET [DateReported]=@DateReported, [ProductGuid]=@ProductGuid, [ShipWeekGuid]=@ShipWeekGuid, [Qty]=@Qty, [AvailabilityTypeLookupGuid]=@AvailabilityTypeLookupGuid, [SalesSinceDateReported]=@SalesSinceDateReported, WHERE [Guid]=@Guid;SELECT * FROM [ReportedAvailability] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, reportedAvailability, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, ReportedAvailability reportedAvailability)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = reportedAvailability.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = reportedAvailability.Guid;
			command.Parameters.Add(guidParameter);

			var dateReportedParameter = new SqlParameter( "@DateReported", SqlDbType.DateTime, 8);
			dateReportedParameter.IsNullable = false;
			dateReportedParameter.Value = reportedAvailability.DateReported;
			command.Parameters.Add(dateReportedParameter);

			var productGuidParameter = new SqlParameter( "@ProductGuid", SqlDbType.UniqueIdentifier, 16);
			productGuidParameter.IsNullable = false;
			productGuidParameter.Value = reportedAvailability.ProductGuid;
			command.Parameters.Add(productGuidParameter);

			var shipWeekGuidParameter = new SqlParameter( "@ShipWeekGuid", SqlDbType.UniqueIdentifier, 16);
			shipWeekGuidParameter.IsNullable = false;
			shipWeekGuidParameter.Value = reportedAvailability.ShipWeekGuid;
			command.Parameters.Add(shipWeekGuidParameter);

			var qtyParameter = new SqlParameter( "@Qty", SqlDbType.Int, 4);
			qtyParameter.IsNullable = false;
			qtyParameter.Value = reportedAvailability.Qty;
			command.Parameters.Add(qtyParameter);

			var availabilityTypeLookupGuidParameter = new SqlParameter( "@AvailabilityTypeLookupGuid", SqlDbType.UniqueIdentifier, 16);
			availabilityTypeLookupGuidParameter.IsNullable = false;
			availabilityTypeLookupGuidParameter.Value = reportedAvailability.AvailabilityTypeLookupGuid;
			command.Parameters.Add(availabilityTypeLookupGuidParameter);

			var salesSinceDateReportedParameter = new SqlParameter( "@SalesSinceDateReported", SqlDbType.Int, 4);
			salesSinceDateReportedParameter.IsNullable = true;
			salesSinceDateReportedParameter.Value = reportedAvailability.SalesSinceDateReported ?? (object)DBNull.Value;
			command.Parameters.Add(salesSinceDateReportedParameter);

		}

        protected override ReportedAvailability DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var reportedAvailability = new ReportedAvailability();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)ReportedAvailability.ColumnEnum.Id] ?? long.MinValue;
				reportedAvailability.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)ReportedAvailability.ColumnEnum.Guid] ?? Guid.Empty;
				reportedAvailability.Guid = (Guid)guid;

				columnName = "DateReported";
				var dateReported = reader[(int)ReportedAvailability.ColumnEnum.DateReported] ?? DateTime.MinValue;
				reportedAvailability.DateReported = (DateTime)dateReported;

				columnName = "ProductGuid";
				var productGuid = reader[(int)ReportedAvailability.ColumnEnum.ProductGuid] ?? Guid.Empty;
				reportedAvailability.ProductGuid = (Guid)productGuid;

				columnName = "ShipWeekGuid";
				var shipWeekGuid = reader[(int)ReportedAvailability.ColumnEnum.ShipWeekGuid] ?? Guid.Empty;
				reportedAvailability.ShipWeekGuid = (Guid)shipWeekGuid;

				columnName = "Qty";
				var qty = reader[(int)ReportedAvailability.ColumnEnum.Qty] ?? int.MinValue;
				reportedAvailability.Qty = (int)qty;

				columnName = "AvailabilityTypeLookupGuid";
				var availabilityTypeLookupGuid = reader[(int)ReportedAvailability.ColumnEnum.AvailabilityTypeLookupGuid] ?? Guid.Empty;
				reportedAvailability.AvailabilityTypeLookupGuid = (Guid)availabilityTypeLookupGuid;

				columnName = "SalesSinceDateReported";
				var salesSinceDateReported = reader[(int)ReportedAvailability.ColumnEnum.SalesSinceDateReported];
				if (salesSinceDateReported == DBNull.Value) salesSinceDateReported = null;
				reportedAvailability.SalesSinceDateReported = (int?)salesSinceDateReported;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return reportedAvailability;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(ReportedAvailability reportedAvailability)
        {
			SetUpProductLazyLoad( reportedAvailability);
			SetUpShipWeekLazyLoad( reportedAvailability);
			SetUpAvailabilityTypeLookupLazyLoad( reportedAvailability);
		}

        protected override void FixAnyLazyLoadedLists(ReportedAvailability reportedAvailability, ListActionEnum listAction)
        {
			FixProductList( reportedAvailability, listAction);
			FixShipWeekList( reportedAvailability, listAction);
			FixAvailabilityTypeLookupList( reportedAvailability, listAction);
		}

        private void SetUpProductLazyLoad( ReportedAvailability reportedAvailability)
        {
            var productTableService = Context.Get<ProductTableService>();
            reportedAvailability.SetLazyProduct(new Lazy<Product>(() => productTableService.GetByGuid(reportedAvailability.ProductGuid), false));
        }

        private void FixProductList( ReportedAvailability reportedAvailability, ListActionEnum listAction)
        {
            if (reportedAvailability.ProductIsLoaded)
            {
                FixLazyLoadedList(reportedAvailability.Product.ReportedAvailabilityList, reportedAvailability, listAction);
            }
        }

        private void SetUpShipWeekLazyLoad( ReportedAvailability reportedAvailability)
        {
            var shipWeekTableService = Context.Get<ShipWeekTableService>();
            reportedAvailability.SetLazyShipWeek(new Lazy<ShipWeek>(() => shipWeekTableService.GetByGuid(reportedAvailability.ShipWeekGuid), false));
        }

        private void FixShipWeekList( ReportedAvailability reportedAvailability, ListActionEnum listAction)
        {
            if (reportedAvailability.ShipWeekIsLoaded)
            {
                FixLazyLoadedList(reportedAvailability.ShipWeek.ReportedAvailabilityList, reportedAvailability, listAction);
            }
        }

        private void SetUpAvailabilityTypeLookupLazyLoad( ReportedAvailability reportedAvailability)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            reportedAvailability.SetLazyAvailabilityTypeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(reportedAvailability.AvailabilityTypeLookupGuid), false));
        }

        private void FixAvailabilityTypeLookupList( ReportedAvailability reportedAvailability, ListActionEnum listAction)
        {
            if (reportedAvailability.AvailabilityTypeLookupIsLoaded)
            {
                FixLazyLoadedList(reportedAvailability.AvailabilityTypeLookup.AvailabilityTypeReportedAvailabilityList, reportedAvailability, listAction);
            }
        }

		#endregion
	}
}

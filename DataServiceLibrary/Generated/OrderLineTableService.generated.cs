using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class OrderLineTableService : TableService<OrderLine>
	{
		public OrderLineTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override OrderLine DefaultInsert( OrderLine orderLine, SqlTransaction transaction = null)
		{
            string sqlInsertCommand = "INSERT INTO [OrderLine]([Guid], [DateDeactivated], [SupplierOrderGuid], [ProductGuid], [QtyOrdered], [QtyShipped], [ActualPriceUsedGuid], [ActualPrice], [OrderLineStatusLookupGuid], [ChangeTypeLookupGuid], [DateEntered], [DateLastChanged], [ActualCost], [ActualCostUsedGuid],[DateQtyLastChanged] ) VALUES (@Guid, @DateDeactivated, @SupplierOrderGuid, @ProductGuid, @QtyOrdered, @QtyShipped, @ActualPriceUsedGuid, @ActualPrice, @OrderLineStatusLookupGuid, @ChangeTypeLookupGuid, @DateEntered, @DateLastChanged, @ActualCost, @ActualCostUsedGuid, @DateQtyLastChanged);SELECT * FROM [OrderLine] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, orderLine, transaction: transaction);
		}

		protected override OrderLine DefaultUpdate( OrderLine orderLine, SqlTransaction transaction = null)
		{
            string sqlUpdateCommand = "UPDATE [OrderLine] SET [DateDeactivated]=@DateDeactivated, [SupplierOrderGuid]=@SupplierOrderGuid, [ProductGuid]=@ProductGuid, [QtyOrdered]=@QtyOrdered, [QtyShipped]=@QtyShipped, [ActualPriceUsedGuid]=@ActualPriceUsedGuid, [ActualPrice]=@ActualPrice, [OrderLineStatusLookupGuid]=@OrderLineStatusLookupGuid, [ChangeTypeLookupGuid]=@ChangeTypeLookupGuid, [DateEntered]=@DateEntered, [DateLastChanged]=@DateLastChanged, [ActualCost]=@ActualCost, [ActualCostUsedGuid]=@ActualCostUsedGuid, [DateQtyLastChanged]=@DateQtyLastChanged, WHERE [Guid]=@Guid;SELECT * FROM [OrderLine] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, orderLine, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, OrderLine orderLine)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = orderLine.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = orderLine.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = orderLine.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var supplierOrderGuidParameter = new SqlParameter( "@SupplierOrderGuid", SqlDbType.UniqueIdentifier, 16);
			supplierOrderGuidParameter.IsNullable = false;
			supplierOrderGuidParameter.Value = orderLine.SupplierOrderGuid;
			command.Parameters.Add(supplierOrderGuidParameter);

			var productGuidParameter = new SqlParameter( "@ProductGuid", SqlDbType.UniqueIdentifier, 16);
			productGuidParameter.IsNullable = false;
			productGuidParameter.Value = orderLine.ProductGuid;
			command.Parameters.Add(productGuidParameter);

			var qtyOrderedParameter = new SqlParameter( "@QtyOrdered", SqlDbType.Int, 4);
			qtyOrderedParameter.IsNullable = false;
			qtyOrderedParameter.Value = orderLine.QtyOrdered;
			command.Parameters.Add(qtyOrderedParameter);

			var qtyShippedParameter = new SqlParameter( "@QtyShipped", SqlDbType.Int, 4);
			qtyShippedParameter.IsNullable = false;
			qtyShippedParameter.Value = orderLine.QtyShipped;
			command.Parameters.Add(qtyShippedParameter);

			var actualPriceUsedGuidParameter = new SqlParameter( "@ActualPriceUsedGuid", SqlDbType.UniqueIdentifier, 16);
			actualPriceUsedGuidParameter.IsNullable = true;
			actualPriceUsedGuidParameter.Value = orderLine.ActualPriceUsedGuid ?? (object)DBNull.Value;
			command.Parameters.Add(actualPriceUsedGuidParameter);

			var actualPriceParameter = new SqlParameter( "@ActualPrice", SqlDbType.Decimal, 5);
			actualPriceParameter.IsNullable = false;
			actualPriceParameter.Value = orderLine.ActualPrice;
			command.Parameters.Add(actualPriceParameter);

			var orderLineStatusLookupGuidParameter = new SqlParameter( "@OrderLineStatusLookupGuid", SqlDbType.UniqueIdentifier, 16);
			orderLineStatusLookupGuidParameter.IsNullable = false;
			orderLineStatusLookupGuidParameter.Value = orderLine.OrderLineStatusLookupGuid;
			command.Parameters.Add(orderLineStatusLookupGuidParameter);

			var changeTypeLookupGuidParameter = new SqlParameter( "@ChangeTypeLookupGuid", SqlDbType.UniqueIdentifier, 16);
			changeTypeLookupGuidParameter.IsNullable = false;
			changeTypeLookupGuidParameter.Value = orderLine.ChangeTypeLookupGuid;
			command.Parameters.Add(changeTypeLookupGuidParameter);

			var dateEnteredParameter = new SqlParameter( "@DateEntered", SqlDbType.DateTime, 8);
			dateEnteredParameter.IsNullable = true;
			dateEnteredParameter.Value = orderLine.DateEntered ?? (object)DBNull.Value;
			command.Parameters.Add(dateEnteredParameter);

			var dateLastChangedParameter = new SqlParameter( "@DateLastChanged", SqlDbType.DateTime, 8);
			dateLastChangedParameter.IsNullable = true;
			dateLastChangedParameter.Value = orderLine.DateLastChanged ?? (object)DBNull.Value;
			command.Parameters.Add(dateLastChangedParameter);

			var actualCostParameter = new SqlParameter( "@ActualCost", SqlDbType.Decimal, 5);
			actualCostParameter.IsNullable = true;
			actualCostParameter.Value = orderLine.ActualCost ?? (object)DBNull.Value;
			command.Parameters.Add(actualCostParameter);

			var actualCostUsedGuidParameter = new SqlParameter( "@ActualCostUsedGuid", SqlDbType.UniqueIdentifier, 16);
			actualCostUsedGuidParameter.IsNullable = true;
			actualCostUsedGuidParameter.Value = orderLine.ActualCostUsedGuid ?? (object)DBNull.Value;
			command.Parameters.Add(actualCostUsedGuidParameter);

            var dateQtyLastChangedParameter = new SqlParameter("@DateQtyLastChanged", SqlDbType.DateTime, 8);
            dateQtyLastChangedParameter.IsNullable = true;
            dateQtyLastChangedParameter.Value = orderLine.DateQtyLastChanged ?? (object)DBNull.Value;
            command.Parameters.Add(dateQtyLastChangedParameter);

         

		}

        protected override OrderLine DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var orderLine = new OrderLine();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)OrderLine.ColumnEnum.Id] ?? long.MinValue;
				orderLine.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)OrderLine.ColumnEnum.Guid] ?? Guid.Empty;
				orderLine.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)OrderLine.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				orderLine.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "SupplierOrderGuid";
				var supplierOrderGuid = reader[(int)OrderLine.ColumnEnum.SupplierOrderGuid] ?? Guid.Empty;
				orderLine.SupplierOrderGuid = (Guid)supplierOrderGuid;

				columnName = "ProductGuid";
				var productGuid = reader[(int)OrderLine.ColumnEnum.ProductGuid] ?? Guid.Empty;
				orderLine.ProductGuid = (Guid)productGuid;

				columnName = "QtyOrdered";
				var qtyOrdered = reader[(int)OrderLine.ColumnEnum.QtyOrdered] ?? int.MinValue;
				orderLine.QtyOrdered = (int)qtyOrdered;

				columnName = "QtyShipped";
				var qtyShipped = reader[(int)OrderLine.ColumnEnum.QtyShipped] ?? int.MinValue;
				orderLine.QtyShipped = (int)qtyShipped;

				columnName = "ActualPriceUsedGuid";
				var actualPriceUsedGuid = reader[(int)OrderLine.ColumnEnum.ActualPriceUsedGuid];
				if (actualPriceUsedGuid == DBNull.Value) actualPriceUsedGuid = null;
				orderLine.ActualPriceUsedGuid = (Guid?)actualPriceUsedGuid;

				columnName = "ActualPrice";
				var actualPrice = reader[(int)OrderLine.ColumnEnum.ActualPrice] ?? Decimal.MinValue;
				orderLine.ActualPrice = (Decimal)actualPrice;

				columnName = "OrderLineStatusLookupGuid";
				var orderLineStatusLookupGuid = reader[(int)OrderLine.ColumnEnum.OrderLineStatusLookupGuid] ?? Guid.Empty;
				orderLine.OrderLineStatusLookupGuid = (Guid)orderLineStatusLookupGuid;

				columnName = "ChangeTypeLookupGuid";
				var changeTypeLookupGuid = reader[(int)OrderLine.ColumnEnum.ChangeTypeLookupGuid] ?? Guid.Empty;
				orderLine.ChangeTypeLookupGuid = (Guid)changeTypeLookupGuid;

				columnName = "DateEntered";
				var dateEntered = reader[(int)OrderLine.ColumnEnum.DateEntered];
				if (dateEntered == DBNull.Value) dateEntered = null;
				orderLine.DateEntered = (DateTime?)dateEntered;

				columnName = "DateLastChanged";
				var dateLastChanged = reader[(int)OrderLine.ColumnEnum.DateLastChanged];
				if (dateLastChanged == DBNull.Value) dateLastChanged = null;
				orderLine.DateLastChanged = (DateTime?)dateLastChanged;

				columnName = "ActualCost";
				var actualCost = reader[(int)OrderLine.ColumnEnum.ActualCost];
				if (actualCost == DBNull.Value) actualCost = null;
				orderLine.ActualCost = (Decimal?)actualCost;

				columnName = "ActualCostUsedGuid";
				var actualCostUsedGuid = reader[(int)OrderLine.ColumnEnum.ActualCostUsedGuid];
				if (actualCostUsedGuid == DBNull.Value) actualCostUsedGuid = null;
				orderLine.ActualCostUsedGuid = (Guid?)actualCostUsedGuid;

                columnName = "DateQtyLastChanged";
                var dateQtyLastChanged = reader[(int)OrderLine.ColumnEnum.DateQtyLastChanged];
                if (dateQtyLastChanged == DBNull.Value) dateQtyLastChanged = null;
                orderLine.DateQtyLastChanged = (DateTime?)dateQtyLastChanged;

                columnName = "LineNumber";
                var lineNumber = reader[(int)OrderLine.ColumnEnum.LineNumber] ?? long.MinValue;
                orderLine.LineNumber = (int)lineNumber;

                columnName = "LineComment";
                var lineComment = reader[(int)OrderLine.ColumnEnum.LineComment];
                if (lineComment == DBNull.Value) lineComment = null;
                orderLine.LineComment = (string)lineComment;
                
                columnName = "ShipWeekCode";
                var ShipWeekCode = "";
                if (reader.FieldCount > 18)
                { 
                   var oShipWeekCode = reader[(int)OrderLine.ColumnEnum.ShipWeekCode];
                   if (oShipWeekCode == DBNull.Value)
                   { 
                       ShipWeekCode = null; 
                   }
                   else
                   { 
                       ShipWeekCode = (string)oShipWeekCode; 
                   }
                }
                orderLine.ShipWeekCode = ShipWeekCode;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return orderLine;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(OrderLine orderLine)
        {
			SetUpSupplierOrderLazyLoad( orderLine);
			SetUpProductLazyLoad( orderLine);
			SetUpOrderLineStatusLookupLazyLoad( orderLine);
			SetUpChangeTypeLookupLazyLoad( orderLine);
            //SetUpClaimOrderLineListLazyLoad( orderLine);
            //SetUpGrowerOrderReminderListLazyLoad( orderLine);
		}

        protected override void FixAnyLazyLoadedLists(OrderLine orderLine, ListActionEnum listAction)
        {
			FixSupplierOrderList( orderLine, listAction);
			FixProductList( orderLine, listAction);
			FixOrderLineStatusLookupList( orderLine, listAction);
			FixChangeTypeLookupList( orderLine, listAction);
		}

        private void SetUpSupplierOrderLazyLoad( OrderLine orderLine)
        {
            var supplierOrderTableService = Context.Get<SupplierOrderTableService>();
            orderLine.SetLazySupplierOrder(new Lazy<SupplierOrder>(() => supplierOrderTableService.GetByGuid(orderLine.SupplierOrderGuid), false));
        }

        private void FixSupplierOrderList( OrderLine orderLine, ListActionEnum listAction)
        {
            if (orderLine.SupplierOrderIsLoaded)
            {
                FixLazyLoadedList(orderLine.SupplierOrder.OrderLineList, orderLine, listAction);
            }
        }

        private void SetUpProductLazyLoad( OrderLine orderLine)
        {
            var productTableService = Context.Get<ProductTableService>();
            orderLine.SetLazyProduct(new Lazy<Product>(() => productTableService.GetByGuid(orderLine.ProductGuid), false));
        }

        private void FixProductList( OrderLine orderLine, ListActionEnum listAction)
        {
            if (orderLine.ProductIsLoaded)
            {
                FixLazyLoadedList(orderLine.Product.OrderLineList, orderLine, listAction);
            }
        }

        private void SetUpOrderLineStatusLookupLazyLoad( OrderLine orderLine)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            orderLine.SetLazyOrderLineStatusLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(orderLine.OrderLineStatusLookupGuid), false));
        }

        private void FixOrderLineStatusLookupList( OrderLine orderLine, ListActionEnum listAction)
        {
            if (orderLine.OrderLineStatusLookupIsLoaded)
            {
                FixLazyLoadedList(orderLine.OrderLineStatusLookup.OrderLineStatusOrderLineList, orderLine, listAction);
            }
        }

        private void SetUpChangeTypeLookupLazyLoad( OrderLine orderLine)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            orderLine.SetLazyChangeTypeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(orderLine.ChangeTypeLookupGuid), false));
        }

        private void FixChangeTypeLookupList( OrderLine orderLine, ListActionEnum listAction)
        {
            if (orderLine.ChangeTypeLookupIsLoaded)
            {
                FixLazyLoadedList(orderLine.ChangeTypeLookup.ChangeTypeOrderLineList, orderLine, listAction);
            }
        }

        //private void SetUpClaimOrderLineListLazyLoad(OrderLine orderLine)
        //{
        //    var claimOrderLineTableService = Context.Get<ClaimOrderLineTableService>();
        //    orderLine.SetLazyClaimOrderLineList(new Lazy<List<ClaimOrderLine>>(() => claimOrderLineTableService.GetAllActiveByGuid(orderLine.Guid, "OrderLineGuid"), false));
        //}

        //private void SetUpGrowerOrderReminderListLazyLoad(OrderLine orderLine)
        //{
        //    var growerOrderReminderTableService = Context.Get<GrowerOrderReminderTableService>();
        //    orderLine.SetLazyGrowerOrderReminderList(new Lazy<List<GrowerOrderReminder>>(() => growerOrderReminderTableService.GetAllActiveByGuid(orderLine.Guid, "OrderLineGuid"), false));
        //}

		#endregion
	}
}

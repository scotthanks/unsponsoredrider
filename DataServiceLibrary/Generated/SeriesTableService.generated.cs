using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class SeriesTableService : TableService<Series>
	{
		public SeriesTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override Series DefaultInsert( Series series, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [Series]([Guid], [DateDeactivated], [Code], [Name], [SpeciesGuid], [DescriptionHTMLGUID], [CultureLibraryLink], ) VALUES (@Guid, @DateDeactivated, @Code, @Name, @SpeciesGuid, @DescriptionHTMLGUID, @CultureLibraryLink, );SELECT * FROM [Series] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, series, transaction: transaction);
		}

		protected override Series DefaultUpdate( Series series, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [Series] SET [DateDeactivated]=@DateDeactivated, [Code]=@Code, [Name]=@Name, [SpeciesGuid]=@SpeciesGuid, [DescriptionHTMLGUID]=@DescriptionHTMLGUID, [CultureLibraryLink]=@CultureLibraryLink, WHERE [Guid]=@Guid;SELECT * FROM [Series] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, series, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, Series series)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = series.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = series.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = series.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var codeParameter = new SqlParameter( "@Code", SqlDbType.NChar, 30);
			codeParameter.IsNullable = false;
			series.Code = series.Code ?? "";
			series.Code = series.Code.Trim();
			codeParameter.Value = series.Code;
			command.Parameters.Add(codeParameter);

			var nameParameter = new SqlParameter( "@Name", SqlDbType.NVarChar, 50);
			nameParameter.IsNullable = false;
			series.Name = series.Name ?? "";
			series.Name = series.Name.Trim();
			nameParameter.Value = series.Name;
			command.Parameters.Add(nameParameter);

			var speciesGuidParameter = new SqlParameter( "@SpeciesGuid", SqlDbType.UniqueIdentifier, 16);
			speciesGuidParameter.IsNullable = false;
			speciesGuidParameter.Value = series.SpeciesGuid;
			command.Parameters.Add(speciesGuidParameter);

			var descriptionHTMLGUIDParameter = new SqlParameter( "@DescriptionHTMLGUID", SqlDbType.UniqueIdentifier, 16);
			descriptionHTMLGUIDParameter.IsNullable = true;
			descriptionHTMLGUIDParameter.Value = series.DescriptionHTMLGUID ?? (object)DBNull.Value;
			command.Parameters.Add(descriptionHTMLGUIDParameter);

			var cultureLibraryLinkParameter = new SqlParameter( "@CultureLibraryLink", SqlDbType.NVarChar, 200);
			cultureLibraryLinkParameter.IsNullable = false;
			series.CultureLibraryLink = series.CultureLibraryLink ?? "";
			series.CultureLibraryLink = series.CultureLibraryLink.Trim();
			cultureLibraryLinkParameter.Value = series.CultureLibraryLink;
			command.Parameters.Add(cultureLibraryLinkParameter);

		}

        protected override Series DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var series = new Series();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)Series.ColumnEnum.Id] ?? long.MinValue;
				series.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)Series.ColumnEnum.Guid] ?? Guid.Empty;
				series.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)Series.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				series.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "Code";
				var code = reader[(int)Series.ColumnEnum.Code] ?? string.Empty;
				code = TrimString(code);
				series.Code = (string)code;

				columnName = "Name";
				var name = reader[(int)Series.ColumnEnum.Name] ?? string.Empty;
				name = TrimString(name);
				series.Name = (string)name;

				columnName = "SpeciesGuid";
				var speciesGuid = reader[(int)Series.ColumnEnum.SpeciesGuid] ?? Guid.Empty;
				series.SpeciesGuid = (Guid)speciesGuid;

				columnName = "DescriptionHTMLGUID";
				var descriptionHTMLGUID = reader[(int)Series.ColumnEnum.DescriptionHTMLGUID];
				if (descriptionHTMLGUID == DBNull.Value) descriptionHTMLGUID = null;
				series.DescriptionHTMLGUID = (Guid?)descriptionHTMLGUID;

				columnName = "CultureLibraryLink";
				var cultureLibraryLink = reader[(int)Series.ColumnEnum.CultureLibraryLink] ?? string.Empty;
				cultureLibraryLink = TrimString(cultureLibraryLink);
				series.CultureLibraryLink = (string)cultureLibraryLink;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return series;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(Series series)
        {
			SetUpSpeciesLazyLoad( series);
            //SetUpPromoCodeListLazyLoad( series);
			SetUpVarietyListLazyLoad( series);
		}

        protected override void FixAnyLazyLoadedLists(Series series, ListActionEnum listAction)
        {
			FixSpeciesList( series, listAction);
		}

        private void SetUpSpeciesLazyLoad( Series series)
        {
            var speciesTableService = Context.Get<SpeciesTableService>();
            series.SetLazySpecies(new Lazy<Species>(() => speciesTableService.GetByGuid(series.SpeciesGuid), false));
        }

        private void FixSpeciesList( Series series, ListActionEnum listAction)
        {
            if (series.SpeciesIsLoaded)
            {
                FixLazyLoadedList(series.Species.SeriesList, series, listAction);
            }
        }

        //private void SetUpPromoCodeListLazyLoad(Series series)
        //{
        //    var promoCodeTableService = Context.Get<PromoCodeTableService>();
        //    series.SetLazyPromoCodeList(new Lazy<List<PromoCode>>(() => promoCodeTableService.GetAllActiveByGuid(series.Guid, "SeriesGuid"), false));
        //}

        private void SetUpVarietyListLazyLoad(Series series)
        {
            var varietyTableService = Context.Get<VarietyTableService>();
            series.SetLazyVarietyList(new Lazy<List<Variety>>(() => varietyTableService.GetAllActiveByGuid(series.Guid, "SeriesGuid"), false));
        }

		#endregion
	}
}

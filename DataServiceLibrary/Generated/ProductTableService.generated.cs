using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class ProductTableService : TableService<Product>
	{
		public ProductTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override Product DefaultInsert( Product product, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [Product]([Guid], [DateDeactivated], [ProgramGuid], [VarietyGuid], [Code], [SupplierIdentifier], [ProductFormGuid], [SupplierDescription], [ProductDescription], [PriceGroupGuid], [ProductionLeadTimeWeeks], [IsOrganic], [LastUpdate], ) VALUES (@Guid, @DateDeactivated, @ProgramGuid, @VarietyGuid, @Code, @SupplierIdentifier, @ProductFormGuid, @SupplierDescription, @ProductDescription, @PriceGroupGuid, @ProductionLeadTimeWeeks, @IsOrganic, @LastUpdate, );SELECT * FROM [Product] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, product, transaction: transaction);
		}

		protected override Product DefaultUpdate( Product product, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [Product] SET [DateDeactivated]=@DateDeactivated, [ProgramGuid]=@ProgramGuid, [VarietyGuid]=@VarietyGuid, [Code]=@Code, [SupplierIdentifier]=@SupplierIdentifier, [ProductFormGuid]=@ProductFormGuid, [SupplierDescription]=@SupplierDescription, [ProductDescription]=@ProductDescription, [PriceGroupGuid]=@PriceGroupGuid, [ProductionLeadTimeWeeks]=@ProductionLeadTimeWeeks, [IsOrganic]=@IsOrganic, [LastUpdate]=@LastUpdate, WHERE [Guid]=@Guid;SELECT * FROM [Product] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, product, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, Product product)
		{		
			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = product.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = product.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var programGuidParameter = new SqlParameter( "@ProgramGuid", SqlDbType.UniqueIdentifier, 16);
			programGuidParameter.IsNullable = false;
			programGuidParameter.Value = product.ProgramGuid;
			command.Parameters.Add(programGuidParameter);

			var varietyGuidParameter = new SqlParameter( "@VarietyGuid", SqlDbType.UniqueIdentifier, 16);
			varietyGuidParameter.IsNullable = false;
			varietyGuidParameter.Value = product.VarietyGuid;
			command.Parameters.Add(varietyGuidParameter);

			var codeParameter = new SqlParameter( "@Code", SqlDbType.NChar, 40);
			codeParameter.IsNullable = false;
			product.Code = product.Code ?? "";
			product.Code = product.Code.Trim();
			codeParameter.Value = product.Code;
			command.Parameters.Add(codeParameter);

			var supplierIdentifierParameter = new SqlParameter( "@SupplierIdentifier", SqlDbType.NChar, 50);
			supplierIdentifierParameter.IsNullable = false;
			product.SupplierIdentifier = product.SupplierIdentifier ?? "";
			product.SupplierIdentifier = product.SupplierIdentifier.Trim();
			supplierIdentifierParameter.Value = product.SupplierIdentifier;
			command.Parameters.Add(supplierIdentifierParameter);

			var productFormGuidParameter = new SqlParameter( "@ProductFormGuid", SqlDbType.UniqueIdentifier, 16);
			productFormGuidParameter.IsNullable = false;
			productFormGuidParameter.Value = product.ProductFormGuid;
			command.Parameters.Add(productFormGuidParameter);

			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = product.Id;
			command.Parameters.Add(idParameter);

			var supplierDescriptionParameter = new SqlParameter( "@SupplierDescription", SqlDbType.NVarChar, 100);
			supplierDescriptionParameter.IsNullable = false;
			product.SupplierDescription = product.SupplierDescription ?? "";
			product.SupplierDescription = product.SupplierDescription.Trim();
			supplierDescriptionParameter.Value = product.SupplierDescription;
			command.Parameters.Add(supplierDescriptionParameter);

			var productDescriptionParameter = new SqlParameter( "@ProductDescription", SqlDbType.NVarChar, 100);
			productDescriptionParameter.IsNullable = false;
			product.ProductDescription = product.ProductDescription ?? "";
			product.ProductDescription = product.ProductDescription.Trim();
			productDescriptionParameter.Value = product.ProductDescription;
			command.Parameters.Add(productDescriptionParameter);

			var priceGroupGuidParameter = new SqlParameter( "@PriceGroupGuid", SqlDbType.UniqueIdentifier, 16);
			priceGroupGuidParameter.IsNullable = false;
			priceGroupGuidParameter.Value = product.PriceGroupGuid;
			command.Parameters.Add(priceGroupGuidParameter);

			var productionLeadTimeWeeksParameter = new SqlParameter( "@ProductionLeadTimeWeeks", SqlDbType.Int, 4);
			productionLeadTimeWeeksParameter.IsNullable = false;
			productionLeadTimeWeeksParameter.Value = product.ProductionLeadTimeWeeks;
			command.Parameters.Add(productionLeadTimeWeeksParameter);

			var isOrganicParameter = new SqlParameter( "@IsOrganic", SqlDbType.Bit, 1);
			isOrganicParameter.IsNullable = false;
			isOrganicParameter.Value = product.IsOrganic;
			command.Parameters.Add(isOrganicParameter);

			var lastUpdateParameter = new SqlParameter( "@LastUpdate", SqlDbType.DateTime, 8);
			lastUpdateParameter.IsNullable = false;
			lastUpdateParameter.Value = product.LastUpdate;
			command.Parameters.Add(lastUpdateParameter);

		}

        protected override Product DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var product = new Product();

			string columnName = "";
            try
            {
				columnName = "Guid";
				var guid = reader[(int)Product.ColumnEnum.Guid] ?? Guid.Empty;
				product.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)Product.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				product.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "ProgramGuid";
				var programGuid = reader[(int)Product.ColumnEnum.ProgramGuid] ?? Guid.Empty;
				product.ProgramGuid = (Guid)programGuid;

				columnName = "VarietyGuid";
				var varietyGuid = reader[(int)Product.ColumnEnum.VarietyGuid] ?? Guid.Empty;
				product.VarietyGuid = (Guid)varietyGuid;

				columnName = "Code";
				var code = reader[(int)Product.ColumnEnum.Code] ?? string.Empty;
				code = TrimString(code);
				product.Code = (string)code;

				columnName = "SupplierIdentifier";
				var supplierIdentifier = reader[(int)Product.ColumnEnum.SupplierIdentifier] ?? string.Empty;
				supplierIdentifier = TrimString(supplierIdentifier);
				product.SupplierIdentifier = (string)supplierIdentifier;

				columnName = "ProductFormGuid";
				var productFormGuid = reader[(int)Product.ColumnEnum.ProductFormGuid] ?? Guid.Empty;
				product.ProductFormGuid = (Guid)productFormGuid;

				columnName = "Id";
				var id = reader[(int)Product.ColumnEnum.Id] ?? long.MinValue;
				product.Id = (long)id;

				columnName = "SupplierDescription";
				var supplierDescription = reader[(int)Product.ColumnEnum.SupplierDescription] ?? string.Empty;
				supplierDescription = TrimString(supplierDescription);
				product.SupplierDescription = (string)supplierDescription;

				columnName = "ProductDescription";
				var productDescription = reader[(int)Product.ColumnEnum.ProductDescription] ?? string.Empty;
				productDescription = TrimString(productDescription);
				product.ProductDescription = (string)productDescription;

				columnName = "PriceGroupGuid";
				var priceGroupGuid = reader[(int)Product.ColumnEnum.PriceGroupGuid] ?? Guid.Empty;
				product.PriceGroupGuid = (Guid)priceGroupGuid;

				columnName = "ProductionLeadTimeWeeks";
				var productionLeadTimeWeeks = reader[(int)Product.ColumnEnum.ProductionLeadTimeWeeks] ?? int.MinValue;
				product.ProductionLeadTimeWeeks = (int)productionLeadTimeWeeks;

				columnName = "IsOrganic";
				var isOrganic = reader[(int)Product.ColumnEnum.IsOrganic] ?? false;
				product.IsOrganic = (bool)isOrganic;

				columnName = "LastUpdate";
				var lastUpdate = reader[(int)Product.ColumnEnum.LastUpdate] ?? DateTime.MinValue;
				product.LastUpdate = (DateTime)lastUpdate;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return product;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(Product product)
        {
			SetUpProgramLazyLoad( product);
			SetUpVarietyLazyLoad( product);
			SetUpProductFormLazyLoad( product);
			SetUpPriceGroupLazyLoad( product);
			SetUpOrderLineListLazyLoad( product);
            //SetUpProductPriceGroupProgramSeasonListLazyLoad( product);
			SetUpReportedAvailabilityListLazyLoad( product);
            //SetUpReportedAvailabilityUpdateListLazyLoad( product);
		}

        protected override void FixAnyLazyLoadedLists(Product product, ListActionEnum listAction)
        {
			FixProgramList( product, listAction);
			FixVarietyList( product, listAction);
			FixProductFormList( product, listAction);
			FixPriceGroupList( product, listAction);
		}

        private void SetUpProgramLazyLoad( Product product)
        {
            var programTableService = Context.Get<ProgramTableService>();
            product.SetLazyProgram(new Lazy<Program>(() => programTableService.GetByGuid(product.ProgramGuid), false));
        }

        private void FixProgramList( Product product, ListActionEnum listAction)
        {
            if (product.ProgramIsLoaded)
            {
                FixLazyLoadedList(product.Program.ProductList, product, listAction);
            }
        }

        private void SetUpVarietyLazyLoad( Product product)
        {
            var varietyTableService = Context.Get<VarietyTableService>();
            product.SetLazyVariety(new Lazy<Variety>(() => varietyTableService.GetByGuid(product.VarietyGuid), false));
        }

        private void FixVarietyList( Product product, ListActionEnum listAction)
        {
            if (product.VarietyIsLoaded)
            {
                FixLazyLoadedList(product.Variety.ProductList, product, listAction);
            }
        }

        private void SetUpProductFormLazyLoad( Product product)
        {
            var productFormTableService = Context.Get<ProductFormTableService>();
            product.SetLazyProductForm(new Lazy<ProductForm>(() => productFormTableService.GetByGuid(product.ProductFormGuid), false));
        }

        private void FixProductFormList( Product product, ListActionEnum listAction)
        {
            if (product.ProductFormIsLoaded)
            {
                FixLazyLoadedList(product.ProductForm.ProductList, product, listAction);
            }
        }

        private void SetUpPriceGroupLazyLoad( Product product)
        {
            var priceGroupTableService = Context.Get<PriceGroupTableService>();
            product.SetLazyPriceGroup(new Lazy<PriceGroup>(() => priceGroupTableService.GetByGuid(product.PriceGroupGuid), false));
        }

        private void FixPriceGroupList( Product product, ListActionEnum listAction)
        {
            if (product.PriceGroupIsLoaded)
            {
                FixLazyLoadedList(product.PriceGroup.ProductList, product, listAction);
            }
        }

        private void SetUpOrderLineListLazyLoad(Product product)
        {
            var orderLineTableService = Context.Get<OrderLineTableService>();
            product.SetLazyOrderLineList(new Lazy<List<OrderLine>>(() => orderLineTableService.GetAllActiveByGuid(product.Guid, "ProductGuid"), false));
        }

        //private void SetUpProductPriceGroupProgramSeasonListLazyLoad(Product product)
        //{
        //    var productPriceGroupProgramSeasonTableService = Context.Get<ProductPriceGroupProgramSeasonTableService>();
        //    product.SetLazyProductPriceGroupProgramSeasonList(new Lazy<List<ProductPriceGroupProgramSeason>>(() => productPriceGroupProgramSeasonTableService.GetAllActiveByGuid(product.Guid, "ProductGuid"), false));
        //}

        private void SetUpReportedAvailabilityListLazyLoad(Product product)
        {
            var reportedAvailabilityTableService = Context.Get<ReportedAvailabilityTableService>();
            product.SetLazyReportedAvailabilityList(new Lazy<List<ReportedAvailability>>(() => reportedAvailabilityTableService.GetAllActiveByGuid(product.Guid, "ProductGuid"), false));
        }

        //private void SetUpReportedAvailabilityUpdateListLazyLoad(Product product)
        //{
        //    var reportedAvailabilityUpdateTableService = Context.Get<ReportedAvailabilityUpdateTableService>();
        //    product.SetLazyReportedAvailabilityUpdateList(new Lazy<List<ReportedAvailabilityUpdate>>(() => reportedAvailabilityUpdateTableService.GetAllActiveByGuid(product.Guid, "ProductGuid"), false));
        //}

		#endregion
	}
}

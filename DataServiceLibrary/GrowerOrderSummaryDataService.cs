﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;
using DataAccessLibrary;
using System.Data.SqlClient;
using System.Data;
using BusinessObjectsLibrary;

namespace DataServiceLibrary
{
    public partial class GrowerOrderSummaryDataService: DataServiceBaseNew
    {
        public GrowerOrderSummaryDataService(StatusObject status)
            : base(status)
        {

        }
        public List<GrowerOrderSummaryByShipWeekView> GetGrowerOrderSummaries
            (
                string userCode, 
                Guid? growerGuid = null, 
                string orderTypeLookupCode = null, 
                string growerOrderStatusLookupCode = null, 
                string supplierOrderStatusLookupCode = null, 
                string orderLineStatusLookupCode = null, 
                string shipWeekCode = null,
                bool returnJustMyOrders = true
            )
        {
            List<GrowerOrderSummaryByShipWeekView> growerOrderSummaryList = null;


            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerOrderSummaryByShipWeekGetData]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserCode", SqlDbType.NChar, 56))
                    .Value = userCode;
                command.Parameters
                    .Add(new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerGuid;
                command.Parameters
                    .Add(new SqlParameter("@ShipWeekCode", SqlDbType.NChar, 6))
                    .Value = shipWeekCode;
                command.Parameters
                    .Add(new SqlParameter("@OrderTypeLookupCode", SqlDbType.NChar, 20))
                    .Value = orderTypeLookupCode;
                command.Parameters
                    .Add(new SqlParameter("@GrowerOrderStatusLookupCode", SqlDbType.NChar, 20))
                    .Value = growerOrderStatusLookupCode;
                command.Parameters
                    .Add(new SqlParameter("@SupplierOrderStatusLookupCode", SqlDbType.NChar, 20))
                    .Value = supplierOrderStatusLookupCode;
                command.Parameters
                    .Add(new SqlParameter("@OrderLineStatusLookupCode", SqlDbType.NChar, 20))
                    .Value = orderLineStatusLookupCode;
                command.Parameters
                    .Add(new SqlParameter("@ShowJustMyOrders", SqlDbType.Bit, 1))
                    .Value = returnJustMyOrders;
                               

                

                var reader = command.ExecuteReader();
                var context = new Context();

                var shipWeekTableService = context.Get<ShipWeekTableService>();
                shipWeekTableService.GetAllFromReader(reader);

                reader.NextResult();

                var programTypeTableService = context.Get<ProgramTypeTableService>();
                programTypeTableService.GetAllFromReader(reader);

                reader.NextResult();

                var productFormCategoryTableService = context.Get<ProductFormCategoryTableService>();
                productFormCategoryTableService.GetAllFromReader(reader);

                reader.NextResult();

                var growerTableService = context.Get<GrowerTableService>();
                growerTableService.GetAllFromReader(reader);

                reader.NextResult();

                var growerOrderTableService = context.Get<GrowerOrderTableService>();
                growerOrderTableService.GetAllFromReader(reader);

                reader.NextResult();

                var growerOrderSummaryByShipWeekService = context.Get<GrowerOrderSummaryByShipWeekViewService>();
                growerOrderSummaryList = growerOrderSummaryByShipWeekService.GetAllFromReader(reader);

                reader.NextResult();

                var lookupService = context.Get<LookupTableService>();
                lookupService.GetAllFromReader(reader);

                reader.NextResult();

                var personService = context.Get<PersonTableService>();
                personService.GetAllFromReader(reader);

                //TODO: These should not be necessary. Lazy loading is supported by views!!!
                foreach (var orderSummaryDataObject in growerOrderSummaryList)
                {
                    //TODO: There are some more items to be added here.
                    SetUpGrowerLazyLoad(context, orderSummaryDataObject);
                    SetUpGrowerOrderLazyLoad(context, orderSummaryDataObject);
                    SetUpShipWeekLazyLoad(context, orderSummaryDataObject);
                    SetUpProductFormCategoryLazyLoad(context, orderSummaryDataObject);
                    SetUpGrowerOrderStatusLookupLazyLoad(context, orderSummaryDataObject);
                    SetUpLowestSupplierOrderStatusLookupLazyLoad(context, orderSummaryDataObject);
                    SetUpLowestOrderLineStatusLookupLazyLoad(context, orderSummaryDataObject);
                    SetUpPersonLazyLoad(context, orderSummaryDataObject);
                }

                Status.Success = true;
                Status.StatusMessage = "Summaries Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderSummaryDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }

            return growerOrderSummaryList;
        }

        private void SetUpGrowerLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        {
            var growerTableService = context.Get<GrowerTableService>();
            growerOrderSummaryByShipWeekView.SetLazyGrower(new Lazy<DataObjectLibrary.Grower>(() => growerTableService.GetByGuid(growerOrderSummaryByShipWeekView.GrowerGuid), false));
        }

        private void SetUpGrowerOrderLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        {
            var growerOrderTableService = context.Get<GrowerOrderTableService>();
            growerOrderSummaryByShipWeekView.SetLazyGrowerOrder(new Lazy<DataObjectLibrary.GrowerOrder>(() => growerOrderTableService.GetByGuid(growerOrderSummaryByShipWeekView.GrowerOrderGuid), false));
        }

        private void SetUpShipWeekLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        {
            var supplierOrderTableService = context.Get<ShipWeekTableService>();
            growerOrderSummaryByShipWeekView.SetLazyShipWeek(new Lazy<DataObjectLibrary.ShipWeek>(() => supplierOrderTableService.GetByGuid(growerOrderSummaryByShipWeekView.ShipWeekGuid), false));
        }

        private void SetUpProductFormCategoryLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        {
            var supplierOrderTableService = context.Get<ProductFormCategoryTableService>();
            growerOrderSummaryByShipWeekView.SetLazyProductFormCategory(new Lazy<DataObjectLibrary.ProductFormCategory>(() => supplierOrderTableService.GetByGuid(growerOrderSummaryByShipWeekView.ProductFormCategoryGuid), false));
        }

        private void SetUpGrowerOrderStatusLookupLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        {
            var lookupTableService = context.Get<LookupTableService>();
            growerOrderSummaryByShipWeekView.SetLazyGrowerOrderStatusLookup(new Lazy<DataObjectLibrary.Lookup>(() => lookupTableService.GetByGuid(growerOrderSummaryByShipWeekView.GrowerOrderStatusLookupGuid), false));
        }

        private void SetUpLowestSupplierOrderStatusLookupLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        {
            var lookupTableService = context.Get<LookupTableService>();
            growerOrderSummaryByShipWeekView.SetLazyLowestSupplierOrderStatusLookup(new Lazy<DataObjectLibrary.Lookup>(() => lookupTableService.GetByGuid(growerOrderSummaryByShipWeekView.LowestSupplierOrderStatusLookupGuid), false));
        }

        private void SetUpLowestOrderLineStatusLookupLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        {
            var lookupTableService = context.Get<LookupTableService>();
            growerOrderSummaryByShipWeekView.SetLazyLowestOrderLineStatusLookup(new Lazy<DataObjectLibrary.Lookup>(() => lookupTableService.GetByGuid(growerOrderSummaryByShipWeekView.LowestOrderLineStatusLookupGuid), false));
        }

        private void SetUpPersonLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        {
            var personTableService = context.Get<PersonTableService>();
            growerOrderSummaryByShipWeekView.SetLazyPerson(new Lazy<DataObjectLibrary.Person>(() => personTableService.GetByGuid(growerOrderSummaryByShipWeekView.PersonGuid), false));
        }
    }
}

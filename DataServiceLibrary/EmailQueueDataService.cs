﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using DataAccessLibrary;
using DataObjectLibrary;
using BusinessObjectsLibrary;
using BusinessObjectsLibrary.Catalog;


namespace DataServiceLibrary
{
    public class EmailQueueDataService : DataServiceBaseNew
    {

        public EmailQueueDataService(StatusObject status)
            : base(status)
        {

        }
        public bool AddEmail(string subject, string body, int delaySendMinutes, string emailType,string emailFrom,string emailTo, string emailCC, string emailBCC, Guid orderGuid, int updateCount, int addCount, int cancelCount)
        {

            try
            {
                using (var connection = ConnectionServices.ConnectionToData)
                {
                    using (var command = new SqlCommand("[dbo].[EmailQueueAdd]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters
                            .Add(new SqlParameter("@sSubject", SqlDbType.NVarChar, 200))
                            .Value = subject;
                        command.Parameters
                            .Add(new SqlParameter("@sBody", SqlDbType.NVarChar, 0))
                            .Value = body;
                        command.Parameters
                            .Add(new SqlParameter("@iDelaySendMinutes", SqlDbType.Int))
                            .Value = delaySendMinutes;
                        command.Parameters
                            .Add(new SqlParameter("@sEmailType", SqlDbType.NVarChar, 20))
                            .Value = emailType;
                        command.Parameters
                            .Add(new SqlParameter("@sEmailTo", SqlDbType.NVarChar, 100))
                            .Value = emailTo;
                        command.Parameters
                            .Add(new SqlParameter("@sEmailCC", SqlDbType.NVarChar, 100))
                            .Value = emailCC;
                        command.Parameters
                            .Add(new SqlParameter("@sEmailBCC", SqlDbType.NVarChar, 100))
                            .Value = emailBCC;
                        command.Parameters
                            .Add(new SqlParameter("@gOrderGuid", SqlDbType.UniqueIdentifier))
                            .Value = orderGuid;
                        command.Parameters
                            .Add(new SqlParameter("@iUpdateCount", SqlDbType.Int))
                            .Value = updateCount;
                        command.Parameters
                            .Add(new SqlParameter("@iAddCount", SqlDbType.Int))
                            .Value = addCount;
                        command.Parameters
                            .Add(new SqlParameter("@iCancelCount", SqlDbType.Int))
                            .Value = cancelCount;
                        command.Parameters
                           .Add(new SqlParameter("@sEmailFrom", SqlDbType.NVarChar, 100))
                           .Value = emailFrom;
                        connection.Open();
                        var iReturn = command.ExecuteNonQuery();
                      
                    }
                }
               
                Status.StatusMessage = "Email Added to Queue";
                Status.Success = true;

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {


            }

            return Status.Success;
            
            
            
            
            
            
           
 
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary;
using Utility;
using DataObjectLibrary;
using System.Data.SqlClient;
using System.Data;
using BusinessObjectsLibrary;

namespace DataServiceLibrary
{
    public partial class PageHitDataService: DataServiceBaseNew
    {
        public PageHitDataService(StatusObject status)
            : base(status)
        {

        }
       

       
        public bool AddPageHit(PageHit pageHit)
        {

            //Search was chosen just to have it outside of main database in case it gets big.

            var connection = ConnectionServices.ConnectionToSearch;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[PageHitAdd]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@Environment", SqlDbType.NVarChar,70))
                    .Value = pageHit.Environment;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                 command.Parameters
                    .Add(new SqlParameter("@UserName", SqlDbType.NVarChar,70))
                    .Value = pageHit.UserName;
                 command.Parameters
                    .Add(new SqlParameter("@QueryString", SqlDbType.NVarChar,2000))
                    .Value = pageHit.QueryString;
                 command.Parameters
                    .Add(new SqlParameter("@RawURL", SqlDbType.NVarChar,2000))
                    .Value = pageHit.RawUrl;
                 command.Parameters
                    .Add(new SqlParameter("@UrlReferrer", SqlDbType.NVarChar,2000))
                    .Value = pageHit.UrlReferrer;
                 command.Parameters
                    .Add(new SqlParameter("@UserAgent", SqlDbType.NVarChar,2000))
                    .Value = pageHit.UserAgent;
                 command.Parameters
                    .Add(new SqlParameter("@BrowserName", SqlDbType.NVarChar,50))
                    .Value = pageHit.BrowserName;
                 command.Parameters
                    .Add(new SqlParameter("@BrowserVersion", SqlDbType.NVarChar,50))
                    .Value = pageHit.BrowserVersion;
                 command.Parameters
                    .Add(new SqlParameter("@BrowserPlatform", SqlDbType.NVarChar,50))
                    .Value = pageHit.BrowserPlatform;
                 command.Parameters
                    .Add(new SqlParameter("@PartitionKey", SqlDbType.NVarChar,40))
                    .Value = pageHit.PartitionKey;
                 command.Parameters
                    .Add(new SqlParameter("@IPAddress", SqlDbType.NVarChar,15))
                    .Value = pageHit.IPAddress;
                 command.Parameters
                    .Add(new SqlParameter("@IsAuthenticated", SqlDbType.Bit))
                    .Value = pageHit.IsAuthenticated;
                 command.Parameters
                    .Add(new SqlParameter("@IsCrawler", SqlDbType.Bit))
                    .Value = pageHit.IsCrawler;
                 command.Parameters
                    .Add(new SqlParameter("@IsMobileDevice", SqlDbType.Bit))
                    .Value = pageHit.IsMobileDevice;

                int iRet = command.ExecuteNonQuery();
                
                Status.Success = true;
                Status.StatusMessage = "Page Added";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "PageHitDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.StatusMessage = ex.Message;
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }


            
            return Status.Success;
        }
      
    }
}

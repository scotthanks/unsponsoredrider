﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using DataAccessLibrary;
using DataObjectLibrary;
using BusinessObjectsLibrary;
using BusinessObjectsLibrary.Catalog;



using DataServiceLibrary;

//using BusinessObjectsLibrary.Catalog;



namespace DataServiceLibrary
{
    public class CatalogDataService : DataServiceBaseNew
    {
        public CatalogDataService(StatusObject status)
            : base(status)
        {

        }

        public SqlDataReader AllGet(SqlConnection connection, String programTypeCode, String productFormCategoryCode)
        {
           
            try
            {

               
            using (var command = new SqlCommand("[dbo].[catalog_AllGet]", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                
                command.Parameters
                    .Add(new SqlParameter("@ProgramTypeCode", SqlDbType.NChar, 50))
                    .Value = programTypeCode;
                command.Parameters
                    .Add(new SqlParameter("@ProductFormCategoryCode", SqlDbType.NChar, 50))
                    .Value = productFormCategoryCode;
                var reader = command.ExecuteReader();
               
                Status.Success = true;
                Status.StatusMessage = "All Catalog Data Loaded";
                return reader;
            }
        }
                
            
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;
            }
            finally
            {


            }
           


        }

        public IEnumerable<CategoryType> CategoriesGet()
        {
            var list = new List<CategoryType>();
            try
            {
              
                using (var connection = ConnectionServices.ConnectionToData)
                {
                    using (var command = new SqlCommand("[dbo].[catalog_CategoriesGet]", connection))
                    {

                        connection.Open();
                        var reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            list.Add(new CategoryType()
                            {
                                Guid = (Guid)reader["CategoryGuid"],
                                Code = reader["CategoryCode"].ToString(),
                                Name = reader["CategoryName"].ToString()
                            });
                        }
                    }
                }
                Status.Success = true;
                Status.StatusMessage = "Categories Loaded";
               
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                
                
            }
            return list;


        }

        public IEnumerable<ProductFormType> ProductFormsGet(string categoryCode)
        {

            var list = new List<ProductFormType>();
            try
            {
                using (var connection = ConnectionServices.ConnectionToData)
                {
                    using (var command = new SqlCommand("[dbo].[catalog_ProductFormsGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters
                            .Add(new SqlParameter("@programTypeCode", SqlDbType.NChar, 10))
                            .Value = categoryCode;
                        connection.Open();
                        var reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            list.Add(new ProductFormType()
                            {
                                Guid = (Guid)reader["FormGuid"],
                                Code = reader["FormCode"].ToString(),
                                Name = reader["FormName"].ToString(),
                                CategoryGuid = (Guid)reader["CategoryGuid"],

                                //ProductFormGuid = (Guid)reader["FormGuid"],
                                //ProductFormCode = reader["FormCode"].ToString(),
                                //ProductFormName = reader["FormName"].ToString(),
                                //CategoryGuid = (Guid)reader["CategoryGuid"],
                                //CategoryCode = reader["CategoryCode"].ToString(),
                                //CategoryName = reader["CategoryName"].ToString()
                            });
                        }
                    }
                }
                Status.Success = true;
                Status.StatusMessage = "Product Forms Loaded";
               
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {


            }
               
            return list;
        }

        public IEnumerable<SupplierType> SuppliersGet(Guid userGuid, string categoryCode, string productFormCode)
        {


            var list = new List<SupplierType>();
            try
            {
                using (var connection = ConnectionServices.ConnectionToData)
                {
                    using (var command = new SqlCommand("[dbo].[catalog_SuppliersGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters
                            .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                            .Value = userGuid;
                        command.Parameters
                            .Add(new SqlParameter("@programTypeCode", SqlDbType.NChar, 50))
                            .Value = categoryCode;
                        command.Parameters
                            .Add(new SqlParameter("@ProductFormCategoryCode", SqlDbType.NChar, 50))
                            .Value = productFormCode;
                        connection.Open();
                        var reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            list.Add(new SupplierType()
                            {
                                Guid = (Guid)reader["Guid"],
                                Code = reader["Code"].ToString(),
                                Name = reader["Name"].ToString()
                            });
                        }
                    }
                }
                Status.Success = true;
                Status.StatusMessage = "Suppliers Loaded";


            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {


            }

            return list;
        }

        public IEnumerable<SpeciesType> SpeciesGet(string categoryCode, string productFormCode, string supplierCodes)
        {
              var list = new List<SpeciesType>();
            try
            {
              

                using (var connection = ConnectionServices.ConnectionToData)
                {
                    using (var command = new SqlCommand("[dbo].[catalog_SpeciesGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters
                            .Add(new SqlParameter("@categoryCode", SqlDbType.NChar, 50))
                            .Value = categoryCode;
                        command.Parameters
                            .Add(new SqlParameter("@productFormCode", SqlDbType.NChar, 50))
                            .Value = productFormCode;
                        command.Parameters
                            .Add(new SqlParameter("@supplierCodes", SqlDbType.NVarChar, 500))
                            .Value = supplierCodes;
                        connection.Open();
                        var reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            list.Add(new SpeciesType()
                            {
                                Guid = (Guid) reader["Guid"],
                                Code = reader["Code"].ToString(),
                                Name = reader["Name"].ToString()
                            });
                        }
                    }
                }
                Status.Success = true;
                Status.StatusMessage = "Species Loaded";
         


            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {


            }

            return list;
        }

        public IEnumerable<SpeciesToSuppliersType> SpeciesSuppliersGet(string categoryCode, string productFormCode)
        {
            var list = new List<SpeciesToSuppliersType>();
            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                using (var command = new SqlCommand("[dbo].[catalog_SpeciesSuppliersGet]", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters
                        .Add(new SqlParameter("@categoryCode", SqlDbType.NChar, 50))
                        .Value = categoryCode;
                    command.Parameters
                        .Add(new SqlParameter("@productFormCode", SqlDbType.NChar, 50))
                        .Value = productFormCode;
                       
                        
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        list.Add(new SpeciesToSuppliersType()
                        {

                            SpeciesCode = reader["SpeciesCode"].ToString(),
                            SupplierCode = reader["SupplierCode"].ToString()
                        });
                    }
                }
                
                Status.Success = true;
                Status.StatusMessage = "SpeciesSuppliers Loaded";



            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();

            }

            return list;
        }


        public IEnumerable<VarietyType> VarietiesInSpeciesGetV2(string categoryCode,
          string productFormCode, string supplierCodes, string speciesCodes)
        {
            var list = new List<VarietyType>();
            try
            {


                
                if (string.IsNullOrEmpty(supplierCodes) || string.IsNullOrEmpty(speciesCodes)) { return list; }

                string[] supplierCodesArray = supplierCodes.Split(',');
                string[] speciesCodesArray = speciesCodes.Split(',');

                var procedure = new DataServiceLibrary.VarietyDataService(Status);

                var varietyTableObjectList =
                    procedure.GetVarieties
                        (          
                            programTypeCode: categoryCode,
                            productFormCategoryCode: productFormCode,
                            geneticOwnerCodes: null,
                            supplierCodes: supplierCodesArray,
                            speciesCodes: speciesCodesArray
                        );

                foreach (var varietyTableObject in varietyTableObjectList)
                {
                    string thumbnailImageUrl = GetImageUrl(varietyTableObject, "Thumbnail");
                    string fullImageUrl = GetImageUrl(varietyTableObject, "Full");
                    if (Status.IsSecureRequest)
                    {
                        thumbnailImageUrl = thumbnailImageUrl.Replace("http://", "https://");
                        fullImageUrl = fullImageUrl.Replace("http://", "https://");
                    }
                    list.Add(new VarietyType()
                    {
                        Code = varietyTableObject.Code,
                        Name = varietyTableObject.Name,
                        //GeneticOwnerCode = varietyTableObject.GeneticOwner.Code,
                        GeneticOwnerName = varietyTableObject.GeneticOwner.Name,
                        SpeciesCode = varietyTableObject.Species.Code,
                        SpeciesGuid = varietyTableObject.SpeciesGuid,
                       
                       // ImageUrl = thumbnailImageUrl,
                        ThumbnailImageUrl = thumbnailImageUrl,
                       // FUllImageUrl = fullImageUrl,

                       // CultureLibraryUrl = varietyTableObject.CultureLibraryLink
                    });
                }

                Status.Success = true;
                Status.StatusMessage = "Varieties Loaded";
                



            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {


            }

            return list;

        }

        //Copied in from old service for reference
        private string GetImageUrl(DataObjectLibrary.Variety varietyTableObject, string imageType)
        {
            //TODO: This is not the way to handle the default!!!
            //string defaultImageName = "Unknown" + (imageType == "Full" ? "Large" : "Thumb");
            string defaultImageName = "Unknown_" + (imageType == "Full" ? "FUILL" : "Thumbnail");

            string imageUrl = DataObjectLibrary.ImageFile.GetImageUrl(ImageFile.ImageContainerNameEnum.CatalogImages, defaultImageName, "JPG");

            //TODO: Make it so you don't have to check for guid = null
            if (varietyTableObject.ImageSetGuid != null && varietyTableObject.ImageSet != null)
            {
                var lookupService = DataServiceLibrary.LookupTableService.SingletonInstance;
                //TODO: Do something better with this (maybe bury it in the lookup service).
                var thumnailImageFileTypeLookup = lookupService.GetByPath("Code/ImageFileType/" + imageType, excludeInactive: true);
                foreach (var imageFile in varietyTableObject.ImageSet.ImageFileList)
                {
                    if (imageFile.ImageFileTypeLookupGuid == thumnailImageFileTypeLookup.Guid)
                    {
                        imageUrl = imageFile.GetImageUrl(ImageFile.ImageContainerNameEnum.CatalogImages);
                    }
                }
            }

            return imageUrl;
        }

        //public IEnumerable<BusinessObjectsLibrary.Species> VarietiesInSpeciesGet(string categoryCode, string productFormCode, string supplierCodes, string speciesCodes)
        //{
        //    var list = new List<BusinessObjectsLibrary.Species>();
        //    try
        //    {
               
        //        if (string.IsNullOrEmpty(supplierCodes) || string.IsNullOrEmpty(speciesCodes)) { return list; }

        //        string[] supplierCodesArray = supplierCodes.Split(',');
        //        string[] speciesCodesArray = speciesCodes.Split(',');


        //       // var service = new VarietyService();


        //      //  var varietiesInSpecies = service.SelectVarietiesGroupedBySpecies(plantCategoryCode: categoryCode, productFormCode: productFormCode, supplierCodeList: supplierCodesArray, speciesCodeList: speciesCodesArray);

        //        Status.Success = true;
        //        Status.StatusMessage = "Varieties Loaded";
        //       // return varietiesInSpecies;
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        var error = new ErrorObject()
        //        {
        //            ErrorNumber = ex.HResult,
        //            ErrorLocation = "CatalogDataService",
        //            ErrorMessageUser = ex.Message,
        //            ErrorMessageSystem = ex.Message,
        //            Error = ex.Source
        //        };
        //        AddError(error);
        //        Status.Success = false;
        //    }
        //    finally
        //    {


        //    }

           

        //}
    }
}

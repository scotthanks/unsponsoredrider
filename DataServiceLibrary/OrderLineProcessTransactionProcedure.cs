﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;
using DataAccessLibrary;





namespace DataServiceLibrary
{
    public partial class OrderLineProcessTransactionProcedure
    {
        public class OrderLineUpdateReturnData
        {
            public int Quantity { get; set; }
            public Lookup OrderLineTrxStatusLookup { get; set; }
        }

        public OrderLineUpdateReturnData UpdateOrderLineQuantity(Guid userGuid, Guid orderLineGuid, int quantity, decimal price = 0, SqlTransaction transaction = null, string supplierOrGrower = "Grower")
        {
            ExecuteProcedureDelegate executeProcedureDelegate;

            if (transaction != null)
            {
                executeProcedureDelegate = 
                        (proc) =>
                            {
                                var storedProcedureServices = new StoredProcedureServices();

                                var reader = storedProcedureServices.ReaderFromSproc
                                    (
                                        transaction.Connection,
                                        this,
                                        transaction: transaction
                                    );

                                reader.Close();

                                storedProcedureServices.GetOutputParameterValues();
                            };
            }
            else
            {
                executeProcedureDelegate =
                    (proc) =>
                        {
                            var storedProcedureServices = new StoredProcedureServices();

                            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
                            {
                                try
                                {
                                    connection.Open();

                                    using (var reader = storedProcedureServices.ReaderFromSproc(connection, this))
                                    {
                                        reader.Close();

                                        storedProcedureServices.GetOutputParameterValues();
                                    }
                                }
                                finally
                                {
                                    connection.Close();
                                }
                            }
                        };
            }

            return UpdateOrderLineQuantity
            (
                userGuid,
                orderLineGuid,
                quantity,
                price,
                transaction,
                executeProcedureDelegate,
                supplierOrGrower
            );
        }

        private delegate void ExecuteProcedureDelegate(ProcedureBase procedure);

        private OrderLineUpdateReturnData UpdateOrderLineQuantity(Guid userGuid, Guid orderLineGuid, int quantity, decimal price,
                                                                 SqlTransaction transaction,
                                                                 ExecuteProcedureDelegate executeProcedureDelegate, string supplierOrGrower)
        {
            AvailabilityData availabilityData = null;

            Parameters.UserCode = userGuid.ToString();
            Parameters.OrderLineToUpdateGuid = orderLineGuid;
            Parameters.SupplierOrderGuid = null;
            Parameters.ProductGuid = null;
            Parameters.QtyOrdered = quantity;
            Parameters.Price = price;
            Parameters.SupplierOrGrower = supplierOrGrower;

            var storedProcedureServices = new StoredProcedureServices();

            var returnData = new OrderLineUpdateReturnData();

            var context = new Context();

            executeProcedureDelegate(this);

            returnData.Quantity = Parameters.ActualQuantity ?? 0;

            var lookupService = context.Get<LookupTableService>();

            string orderLineTrxStatusPath = string.Format("Code/OrderLineTrxStatus/{0}",
                                                          Parameters.OrderLineTrxStatusLookupCode);

            returnData.OrderLineTrxStatusLookup =
                lookupService.TryGetByPath(orderLineTrxStatusPath) ??
                lookupService.GetByPath("Code/OrderLineTrxStatus/Failure");

            return returnData;
        }
    }
}

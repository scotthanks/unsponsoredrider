﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;
using DataAccessLibrary;
using System.Data.SqlClient;
using System.Data;
using BusinessObjectsLibrary;

namespace DataServiceLibrary
{
    public partial class AvailabilityDataService: DataServiceBaseNew
    {
        public AvailabilityDataService(StatusObject status)
            : base(status)
        {

        }

        public long GetAvailabilityCount(string userCode, string shipWeekCode, int weeksBefore, int weeksAfter, Guid? growerGuid = null, string programTypeCode = null, string productFormCategoryCode = null, string[] supplierCodes = null, string[] geneticOwnerCodes = null, string[] speciesCodes = null, string[] varietyCodes = null, Guid? productGuid = null, bool organicOnly = false, bool availableOnly = true, string sellerCode = "GFB")
        {
            sellerCode = "GFB";
            if (productGuid == null)
            {
                productGuid = Guid.Empty;
            }

            long theCount = 0;


            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[AvailabilityDataCountGet]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@UserCode", SqlDbType.NVarChar, 56))
                    .Value = userCode;
                command.Parameters
                    .Add(new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerGuid;
                command.Parameters
                    .Add(new SqlParameter("@ShipWeekCode", SqlDbType.NChar, 6))
                    .Value = shipWeekCode;
                command.Parameters
                    .Add(new SqlParameter("@WeeksBefore", SqlDbType.Int, 4))
                    .Value = weeksBefore;
                command.Parameters
                    .Add(new SqlParameter("@WeeksAfter", SqlDbType.Int, 4))
                    .Value = weeksAfter;
                command.Parameters
                    .Add(new SqlParameter("@ProgramTypeCode", SqlDbType.NChar, 50))
                    .Value = programTypeCode;
                command.Parameters
                    .Add(new SqlParameter("@ProductFormCategoryCode", SqlDbType.NChar, 50))
                    .Value = productFormCategoryCode;
                command.Parameters
                    .Add(new SqlParameter("@SupplierCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(supplierCodes);
                command.Parameters
                    .Add(new SqlParameter("@GeneticOwnerCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(geneticOwnerCodes);
                command.Parameters
                    .Add(new SqlParameter("@SpeciesCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(speciesCodes);
                command.Parameters
                    .Add(new SqlParameter("@VarietyCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(varietyCodes);
                command.Parameters
                    .Add(new SqlParameter("@ProductGuid", SqlDbType.UniqueIdentifier))
                    .Value = productGuid;
                command.Parameters
                    .Add(new SqlParameter("@OrganicOnly", SqlDbType.Bit, 1))
                    .Value = organicOnly;
                command.Parameters
                    .Add(new SqlParameter("@AvailableOnly", SqlDbType.Bit, 1))
                    .Value = availableOnly;
                command.Parameters
                    .Add(new SqlParameter("@SellerCode", SqlDbType.NVarChar, 30))
                    .Value = sellerCode;




                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    theCount = (long)reader["theCount"];
                }


                Status.Success = true;
                Status.StatusMessage = "Count Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "AvailabilityDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }



            return theCount;
        }

        public AvailabilityData GetAvailabilityByPage(string userCode, string shipWeekCode, int weeksBefore, int weeksAfter, Guid? growerGuid = null, string programTypeCode = null, string productFormCategoryCode = null, string[] supplierCodes = null, string[] geneticOwnerCodes = null, string[] speciesCodes = null, string[] varietyCodes = null, Guid? productGuid = null, bool organicOnly = false, bool availableOnly = true, string sortOption = "Species", int pageSize = 50, int pageNumber = 1, string sellerCode = "")
        {
            sellerCode = "GFB";
            if (productGuid == null)
            {
                productGuid = Guid.Empty;
            }

            AvailabilityData availabilityData = null;


            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[AvailabilityDataGetByPage]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 300;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@UserCode", SqlDbType.NVarChar, 56))
                    .Value = userCode;
                command.Parameters
                    .Add(new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerGuid;
                command.Parameters
                    .Add(new SqlParameter("@ShipWeekCode", SqlDbType.NChar, 6))
                    .Value = shipWeekCode;
                command.Parameters
                    .Add(new SqlParameter("@WeeksBefore", SqlDbType.Int, 4))
                    .Value = weeksBefore;
                command.Parameters
                    .Add(new SqlParameter("@WeeksAfter", SqlDbType.Int, 4))
                    .Value = weeksAfter;
                command.Parameters
                    .Add(new SqlParameter("@ProgramTypeCode", SqlDbType.NChar, 50))
                    .Value = programTypeCode;
                command.Parameters
                    .Add(new SqlParameter("@ProductFormCategoryCode", SqlDbType.NChar, 50))
                    .Value = productFormCategoryCode;
                command.Parameters
                    .Add(new SqlParameter("@SupplierCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(supplierCodes);
                command.Parameters
                    .Add(new SqlParameter("@GeneticOwnerCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(geneticOwnerCodes);
                command.Parameters
                    .Add(new SqlParameter("@SpeciesCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(speciesCodes);
                command.Parameters
                    .Add(new SqlParameter("@VarietyCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(varietyCodes);
                command.Parameters
                    .Add(new SqlParameter("@OrganicOnly", SqlDbType.Bit, 1))
                    .Value = organicOnly;
                command.Parameters
                    .Add(new SqlParameter("@AvailableOnly", SqlDbType.Bit, 1))
                    .Value = availableOnly;
                command.Parameters
                    .Add(new SqlParameter("@SortOption", SqlDbType.NVarChar, 30))
                    .Value = sortOption;
                command.Parameters
                    .Add(new SqlParameter("@PageSize", SqlDbType.Int, 4))
                    .Value = pageSize;
                command.Parameters
                    .Add(new SqlParameter("@PageNumber", SqlDbType.Int, 4))
                    .Value = pageNumber;
                command.Parameters
                   .Add(new SqlParameter("@SellerCode", SqlDbType.NVarChar, 30))
                   .Value = sellerCode;



                var reader = command.ExecuteReader();
                

                availabilityData = GetAvailabilityDataFromReader(reader, pricingShipWeekCode: shipWeekCode, sellerCode: sellerCode);


                Status.Success = true;
                Status.StatusMessage = "Availability Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "AvailabilityDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }



            return  availabilityData;
        }



        public AvailabilityData GetAvailability(string userCode, string shipWeekCode, int weeksBefore, int weeksAfter, Guid? growerGuid = null, string programTypeCode = null, string productFormCategoryCode = null, string[] supplierCodes = null, string[] geneticOwnerCodes = null, string[] speciesCodes = null, string[] varietyCodes = null, Guid? productGuid = null, bool varietyMatchOptional = false, bool includePossibleSubstitutes = false, bool organicOnly = false, string sellerCode = "GFB")
        {
            sellerCode = "GFB";
            if (productGuid == null)
            {
                productGuid = Guid.Empty;
            }

            AvailabilityData availabilityData = null;


            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[AvailabilityDataGet]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@UserCode", SqlDbType.NVarChar, 56))
                    .Value = userCode;
                command.Parameters
                    .Add(new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerGuid;
                command.Parameters
                    .Add(new SqlParameter("@ShipWeekCode", SqlDbType.NChar, 6))
                    .Value = shipWeekCode;
                command.Parameters
                    .Add(new SqlParameter("@WeeksBefore", SqlDbType.Int, 4))
                    .Value = weeksBefore;
                command.Parameters
                    .Add(new SqlParameter("@WeeksAfter", SqlDbType.Int, 4))
                    .Value = weeksAfter;
                command.Parameters
                    .Add(new SqlParameter("@ProgramTypeCode", SqlDbType.NChar, 50))
                    .Value = programTypeCode;
                command.Parameters
                    .Add(new SqlParameter("@ProductFormCategoryCode", SqlDbType.NChar, 50))
                    .Value = productFormCategoryCode;
                command.Parameters
                    .Add(new SqlParameter("@SupplierCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(supplierCodes);
                command.Parameters
                    .Add(new SqlParameter("@GeneticOwnerCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(geneticOwnerCodes);
                command.Parameters
                    .Add(new SqlParameter("@SpeciesCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(speciesCodes);
                command.Parameters
                    .Add(new SqlParameter("@VarietyCodeList", SqlDbType.NVarChar, 3000))
                    .Value = ToCommaSeparatedList(varietyCodes);
                command.Parameters
                    .Add(new SqlParameter("@ProductGuid", SqlDbType.UniqueIdentifier))
                    .Value = productGuid;
                command.Parameters
                    .Add(new SqlParameter("@VarietyMatchOptional", SqlDbType.Bit, 1))
                    .Value = varietyMatchOptional;
                command.Parameters
                    .Add(new SqlParameter("@IncludePossibleSubstitutes", SqlDbType.Bit, 1))
                    .Value = includePossibleSubstitutes;
                command.Parameters
                    .Add(new SqlParameter("@OrganicOnly", SqlDbType.Bit, 1))
                    .Value = organicOnly;
                command.Parameters
                   .Add(new SqlParameter("@SellerCode", SqlDbType.NVarChar, 30))
                   .Value = sellerCode;

             
                var reader = command.ExecuteReader();

                availabilityData = GetAvailabilityDataFromReader(reader, pricingShipWeekCode: shipWeekCode, sellerCode: sellerCode);


                Status.Success = true;
                Status.StatusMessage = "Availability Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "AvailabilityDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }
            


            return availabilityData;
        }

        internal static AvailabilityData GetAvailabilityDataFromReader(System.Data.SqlClient.SqlDataReader reader, string pricingShipWeekCode, string sellerCode)
        {
            sellerCode = "GFB";
            var availabilityData = new AvailabilityData();

            var context = new Context();

            //TODO: Make it easier to detect what's wrong if these are missing or out of order.

            //var lookupTableService = context.Get<LookupTableService>();
            //lookupTableService.GetAllFromReader(reader);

            reader.NextResult();

            var shipWeekTableService = context.Get<ShipWeekTableService>();
            availabilityData.ShipWeekList = shipWeekTableService.GetAllFromReader(reader);

            //TODO: If there is a timeout, it fails here!!!
            reader.NextResult();

            var programTypeTableService = context.Get<ProgramTypeTableService>();
            programTypeTableService.GetAllFromReader(reader);

            reader.NextResult();

            var productFormCategoryTableService = context.Get<ProductFormCategoryTableService>();
            productFormCategoryTableService.GetAllFromReader(reader);

            reader.NextResult();

            var productFormTableService = context.Get<ProductFormTableService>();
            productFormTableService.GetAllFromReader(reader);

            reader.NextResult();

            var speciesTableService = context.Get<SpeciesTableService>();
            speciesTableService.GetAllFromReader(reader);

            reader.NextResult();

            var supplierTableService = context.Get<SupplierTableService>();
            supplierTableService.GetAllFromReader(reader);

            reader.NextResult();

            var varietyTableService = context.Get<VarietyTableService>();
            varietyTableService.GetAllFromReader(reader);

            reader.NextResult();

            var programTableService = context.Get<ProgramTableService>();
            programTableService.GetAllFromReader(reader);

            reader.NextResult();

            var productTableService = context.Get<ProductTableService>();
            productTableService.GetAllFromReader(reader);

            reader.NextResult();

            var reportedAvailabilityTableService = context.Get<ReportedAvailabilityTableService>();
            availabilityData.ReportedAvailabilityList = reportedAvailabilityTableService.GetAllFromReader(reader);

            reader.NextResult();

            var orderLineTableService = context.Get<OrderLineTableService>();
            availabilityData.PreCartOrderLineList = orderLineTableService.GetAllFromReader(reader);

            reader.NextResult();

            availabilityData.CartedOrderLineList = orderLineTableService.GetAllFromReader(reader);

            reader.NextResult();

            var productGrowerSeasonPriceViewService = context.Get<ProductGrowerSeasonPriceViewService>();
            availabilityData.ProductPriceList = productGrowerSeasonPriceViewService.GetAllFromReader(reader);

            reader.NextResult();

            var priceTableService = context.Get<PriceTableService>();
            availabilityData.Prices = priceTableService.GetAllFromReader(reader);

            reader.NextResult();

            var programSeasonService = context.Get<ProgramSeasonTableService>();
            availabilityData.ProgramSeasonList = programSeasonService.GetAllFromReader(reader);

            if (sellerCode == "EPS" || sellerCode == "GFB")
            {
                //TODO: This should not be necessary; LazyLoad should be set up automatically.
                foreach (var productPrice in availabilityData.ProductPriceList)
                {
                    var price = priceTableService.GetByGuid(productPrice.PriceGuid);
                    productPrice.SetLazyPrice(new Lazy<Price>(() => price));

                    var product = productTableService.GetByGuid(productPrice.ProductGuid);
                    productPrice.SetLazyProduct(new Lazy<DataObjectLibrary.Product>(() => product));
                }
            }
            else
            {
                //nothing?
            }
            

            foreach (var programSeason in availabilityData.ProgramSeasonList)
            {
                programSeason.Program.CurrentProgramSeason = programSeason;
            }

            bool pricingShipWeekFound = false;
            foreach (var shipWeek in availabilityData.ShipWeekList)
            {
                if (shipWeek.Code == pricingShipWeekCode)
                {
                    availabilityData.ShipDate = shipWeek.MondayDate;
                    pricingShipWeekFound = true;
                    break;
                }
            }
            if (!pricingShipWeekFound)
            {
                throw new ApplicationException("The pricing ship week was not found.");
            }

            return availabilityData;
        }

        protected string ToCommaSeparatedList(string[] codes)
        {
            if (codes == null)
            {
                return "*";
            }
            else
            {
                var list = new StringBuilder();
                foreach (string code in codes)
                {
                    if (list.Length > 0)
                        list.Append(',');

                    list.Append(code);
                }

                return list.ToString();
            }
        }
    }
}

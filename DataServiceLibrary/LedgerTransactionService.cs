﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
//using DataAccessLibrary;
//using DataObjectLibrary;



namespace DataServiceLibrary
{
    public class LedgerEntry
    {
        public DateTime TransactionDate { get; set; }
        public string TransactionDateString { get
            { 
                return TransactionDate.ToString("g", CultureInfo.CreateSpecificCulture("en-us"));  
            }}
        public decimal Amount { get; set; }
        public Guid TransactionGuid { get; set; }
        public Guid AssetEntryTypeLookupGuid { get; set; }
        public string AssetInternalIdNumber { get; set; } 
        public string AssetExternalIdNumber { get; set; } 
        public Guid AssetParentGuid { get; set; }
	    public Guid LiablityLedgerEntryTypeLookupGuid { get; set; } 
        public string LiablityLedgerInternalIdNumber { get; set; } 
        public string LiablityLedgerExternalIdNumber { get; set; }
        public Guid LiablityParentGuid { get; set; }

    }

    public class LedgerEntityBalance
    {
        public DateTime TransactionDate { get; set; }
        public string TransactionDateString
        {
            get
            {
                return TransactionDate.ToString("g", CultureInfo.CreateSpecificCulture("en-us"));
            }
        }
        public decimal IncreaseTotal { get; set; }
        public decimal DecreaseTotal { get; set; }
        public decimal Balance { get; set; }
        public Guid ParentGuid { get; set; } 
        public Guid GrowerGuid { get; set; }
        public string InternalIdNumber { get; set; }
        public string ExternalIdNumber { get; set; }
        public string DecreaseExternalIdNumber { get; set; }
        public string TransactionType { get; set; }
        public decimal FinalBalance { get; set; }
    }

    public class LedgerEntityResponse
    {
        public List<LedgerEntityBalance> EntityList { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }

    public class LedgerAccountBalance
    {
        public decimal AccountBalance { get; set; }
        public decimal DebitTotal { get; set; }
        public decimal CreditTotal { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }


    public class LedgerTransactionService
    {
        private Guid GrowerGuid { get; set; }
        private Guid SupplierGuid { get; set; }
        public Guid AssetParentGuid { get; set; }
        public Guid LiabilityParentGuid { get; set; }
        private DateTime TransactionDate { get; set; }
        public string TransactionDateString { get { return TransactionDate.ToString("g", CultureInfo.CreateSpecificCulture("en-us")); } }
        public string InternalIdNumber { get; set; }

        public LedgerTransactionService(Guid growerGuid, Guid supplierGuid, Guid assetParentGuid, Guid liabilityParentGuid)
        {

            GrowerGuid = growerGuid;
            SupplierGuid = supplierGuid;
            AssetParentGuid = assetParentGuid;
            LiabilityParentGuid = liabilityParentGuid;
        }

        private Guid AccountTransaction(Decimal amount, string userCode, string internalNumber, string externalNumber,
            Guid debitAccountGuid, Guid creditAccountGuid,
            Guid debitAccountEntryTypeGuid, Guid creditAccountEntryTypeGuid,
            string sequenceName = "", string sequenceNumberPrefix = "")
        {
            TransactionDate = DateTime.Now;
            Guid transactionGuid = Guid.NewGuid();
            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("AccountTransaction");

                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    long sequenceNumber = -1;
                    if (sequenceName != "")
                    {
                        InternalIdNumber = GetInternalIdNumber(command, sequenceName, sequenceNumberPrefix, ref sequenceNumber);
                        internalNumber = InternalIdNumber;
                    }
                    CreateLedger(command, amount, TransactionDate, GrowerGuid, SupplierGuid, debitAccountEntryTypeGuid, debitAccountGuid, transactionGuid, internalNumber, externalNumber);
                    CreateLedger(command, amount, TransactionDate, GrowerGuid, SupplierGuid, creditAccountEntryTypeGuid, creditAccountGuid, transactionGuid, internalNumber, externalNumber);
                    //if (sequenceName != "")
                    //{
                    //    UpdateInternalIdNumber(command, sequenceName, sequenceNumber);
                    //}

                    // Attempt to commit the transaction.
                    transaction.Commit();
                    connection.Close();

                    int bRet = CheckInvoiceUpdateStatus(new Guid(), internalNumber);


                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    return Guid.Empty;
                }
              

            }

            return transactionGuid;
        }

        public bool InvoiceTransaction(Decimal amount, string userCode, string creditCardReference)
        {
            DateTime transactionDate = DateTime.Now;
            Guid transactionGuid = Guid.NewGuid();
            LookupTableValues.CodeValues.GrowerOrderStatusValues orderStatusLookupService = new LookupTableValues.CodeValues.GrowerOrderStatusValues();
            Guid InvoicedLookupStatus = orderStatusLookupService.Invoiced.Guid;

            LookupTableValues.CodeValues.GeneralLedgerAccountValues accountLookupService = new LookupTableValues.CodeValues.GeneralLedgerAccountValues();
            Guid debitAccountGuid = accountLookupService.AccountsRecievable.Guid;
            Guid creditAccountGuid = accountLookupService.Sales.Guid;

            LookupTableValues.CodeValues.GeneralLedgerEntryTypeValues entryTypeLookupService = new LookupTableValues.CodeValues.GeneralLedgerEntryTypeValues();
            Guid debitAccountEntryTypeGuid = entryTypeLookupService.Debit.Guid;
            Guid creditAccountEntryTypeGuid = entryTypeLookupService.Credit.Guid;


            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("AccountTransaction");

                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    long sequenceNumber = -1;
                    string invoiceNumber = GetInternalIdNumber(command, "InvoiceNumber", "I-", ref sequenceNumber);

                    CreateLedger(command, amount, transactionDate, GrowerGuid, SupplierGuid, debitAccountEntryTypeGuid, debitAccountGuid, transactionGuid, invoiceNumber, creditCardReference);
                    CreateLedger(command, amount, transactionDate, GrowerGuid, SupplierGuid, creditAccountEntryTypeGuid, creditAccountGuid, transactionGuid, invoiceNumber, creditCardReference);

                    //UpdateInternalIdNumber(command, "InvoiceNumber", sequenceNumber);
                    UpdateGrowerOrderStatus(command, InvoicedLookupStatus, AssetParentGuid);

                    // Attempt to commit the transaction.
                    transaction.Commit();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    return false;
                }
             
            }

            return true;
        }

        //public Guid CreditCardPayment(Decimal amount, string userCode, string invoiceNumber, string referenceNumber)
        //{
        //    // Gib: I changed the following code to show how you can use the LookupTableValues more easily.
        //    Guid debitAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.Cash.Guid;
        //    Guid creditAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.AccountsRecievable.Guid;

        //    Guid debitAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
        //    Guid creditAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;

        //    return AccountTransaction(amount, userCode, invoiceNumber, referenceNumber, debitAccountGuid, creditAccountGuid, debitAccountEntryTypeGuid, creditAccountEntryTypeGuid);
        //}

        public Guid PayInvoiceFromUnappliedPayments(Decimal amount, string userCode, string invoiceNumber, string checkNumber)
        {
            Guid debitAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.UnappliedPayments.Guid;
            Guid creditAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.AccountsRecievable.Guid;

            Guid debitAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
            Guid creditAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;

            return AccountTransaction(amount, userCode, invoiceNumber, checkNumber, debitAccountGuid, creditAccountGuid, debitAccountEntryTypeGuid, creditAccountEntryTypeGuid);
        }

        public Guid PayInvoiceFromUnappliedCredits(Decimal amount, string userCode, string invoiceNumber, string checkNumber)
        {
            Guid debitAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.UnappliedCredits.Guid;
            Guid creditAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.AccountsRecievable.Guid;

            Guid debitAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
            Guid creditAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;

            return AccountTransaction(amount, userCode, invoiceNumber, checkNumber, debitAccountGuid, creditAccountGuid, debitAccountEntryTypeGuid, creditAccountEntryTypeGuid);
        }

        public Guid PayInvoiceFromUnappliedCreditCardPayments(Decimal amount, string userCode, string invoiceNumber, string creditCardReferenceNumber)
        {
            Guid debitAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.UnappliedCreditCardPayments.Guid;
            Guid creditAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.AccountsRecievable.Guid;

            Guid debitAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
            Guid creditAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;

            return AccountTransaction(amount, userCode, invoiceNumber, creditCardReferenceNumber, debitAccountGuid, creditAccountGuid, debitAccountEntryTypeGuid, creditAccountEntryTypeGuid);
        }

        public Guid CheckPayment(Decimal amount, string userCode, string checkNumber)
        {
            LookupTableValues.CodeValues.GeneralLedgerAccountValues accountLookupService = new LookupTableValues.CodeValues.GeneralLedgerAccountValues();
            Guid debitAccountGuid = accountLookupService.Cash.Guid;
            Guid creditAccountGuid = accountLookupService.UnappliedPayments.Guid;

            LookupTableValues.CodeValues.GeneralLedgerEntryTypeValues entryTypeLookupService = new LookupTableValues.CodeValues.GeneralLedgerEntryTypeValues();
            Guid debitAccountEntryTypeGuid = entryTypeLookupService.Debit.Guid;
            Guid creditAccountEntryTypeGuid = entryTypeLookupService.Credit.Guid;
            // add internal check number
            return AccountTransaction(amount, userCode, null, checkNumber, debitAccountGuid, 
                creditAccountGuid, debitAccountEntryTypeGuid, creditAccountEntryTypeGuid,
                "CheckNumber", "Ck-");
        }

        public Guid CreditPayment(Decimal amount, string userCode, string creditNumber)
        {
            LookupTableValues.CodeValues.GeneralLedgerAccountValues accountLookupService = new LookupTableValues.CodeValues.GeneralLedgerAccountValues();
            Guid debitAccountGuid = accountLookupService.Cash.Guid;
            Guid creditAccountGuid = accountLookupService.UnappliedCredits.Guid;

            LookupTableValues.CodeValues.GeneralLedgerEntryTypeValues entryTypeLookupService = new LookupTableValues.CodeValues.GeneralLedgerEntryTypeValues();
            Guid debitAccountEntryTypeGuid = entryTypeLookupService.Debit.Guid;
            Guid creditAccountEntryTypeGuid = entryTypeLookupService.Credit.Guid;
            // add internal check number
            return AccountTransaction(amount, userCode, creditNumber, creditNumber, debitAccountGuid,
                creditAccountGuid, debitAccountEntryTypeGuid, creditAccountEntryTypeGuid,
                "CreditNumber", "Cr-");
        }

        public Guid CreditCardPayment(Decimal amount, string userCode, string creditCardReferenceNumber)
        {
            Guid debitAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.Cash.Guid;
            Guid creditAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.UnappliedCreditCardPayments.Guid;

            Guid debitAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
            Guid creditAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;

            return AccountTransaction(amount, userCode, "", creditCardReferenceNumber, debitAccountGuid, creditAccountGuid, 
                debitAccountEntryTypeGuid, creditAccountEntryTypeGuid,
                "CreditCardPaymentNumber", "Cc-");
        }

        public Guid RecordSupplierInvoice(Decimal amount, string userCode, string supplierInvoiceNumber)
        {
            Guid debitAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.SupplierExpense.Guid;
            Guid creditAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.AccountsPayable.Guid;

            Guid debitAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
            Guid creditAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;

            return AccountTransaction(amount, userCode, "", supplierInvoiceNumber, debitAccountGuid, creditAccountGuid,
                debitAccountEntryTypeGuid, creditAccountEntryTypeGuid,
                "SupplierInvoiceNumber", "SI-");
        }

        public Guid UnappliedPaymentFromEpsCheck(Decimal amount, string userCode, string epsCheckNumber)
        {
            Guid debitAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.UnappliedEpsPayments.Guid;
            Guid creditAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.Cash.Guid;

            Guid debitAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
            Guid creditAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;

            return AccountTransaction(amount, userCode, "", epsCheckNumber, debitAccountGuid, creditAccountGuid,
                debitAccountEntryTypeGuid, creditAccountEntryTypeGuid);
        }

        public Guid PaySupplierInvoiceFromEpsCheck(Decimal amount, string userCode, string epsCheckNumber)
        {
            Guid debitAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.AccountsPayable.Guid;
            Guid creditAccountGuid = LookupTableValues.Code.GeneralLedgerAccount.UnappliedEpsPayments.Guid;

            Guid debitAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
            Guid creditAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;

            return AccountTransaction(amount, userCode, "", epsCheckNumber, debitAccountGuid, creditAccountGuid,
                debitAccountEntryTypeGuid, creditAccountEntryTypeGuid);
        }



        public List<LedgerEntry> GetOrderTransactions(Guid AssetAccountGuid, Guid LiablityAccountGuid, Guid ledgerEntryTypeGuid)
        {
            List<LedgerEntry> entryList = new List<LedgerEntry>();
            try
            {
                //LookupTableValues.CodeValues.GeneralLedgerAccountValues accountLookupService = new LookupTableValues.CodeValues.GeneralLedgerAccountValues();
                //Guid unappliedPaymentsGuid = accountLookupService.UnappliedPayments.Guid;

                using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
                {
                    connection.Open();

                    string query2 =  string.Format(
                        @"select 
                            AssetLedger.TransactionGuid, 
                            AssetLedger.Amount, 
                            AssetLedger.TransactionDate, 
                            AssetLedger.EntryTypeLookupGuid as AssetEntryTypeLookupGuid, 
                            AssetLedger.InternalIdNumber as AssetInternalIdNumber, 
                            AssetLedger.ExternalIdNumber as AssetExternalIdNumber, 
                            AssetLedger.ParentGuid as AssetParentGuid,
                            AssetLedger.GLAccountLookupGuid,
	                        LiablityLedger.EntryTypeLookupGuid as LiablityLedgerEntryTypeLookupGuid, 
                            LiablityLedger.InternalIdNumber as LiablityLedgerInternalIdNumber, 
                            LiablityLedger.ExternalIdNumber as LiablityLedgerExternalIdNumber, 
	                        LiablityLedger.ParentGuid as LiablityParentGuid,
	                        LiablityLedger.GLAccountLookupGuid 
                        from Ledger as AssetLedger 
                            join Ledger as LiablityLedger on LiablityLedger.TransactionGuid = AssetLedger.TransactionGuid
                        where 
                            AssetLedger.GrowerGuid = '{0}'
	                        AND 
	                        AssetLedger.GLAccountLookupGuid = '{1}' 
	                        AND 
	                        LiablityLedger.GLAccountLookupGuid = '{2}'", 
                            GrowerGuid,
                            AssetAccountGuid,
                            LiablityAccountGuid
                            );


                    //if (GetParentGuid(GLAccountGuid) != Guid.Empty)
                    if (AssetParentGuid != Guid.Empty)
                    {
                        //query2 += string.Format(" AND Ledger.ParentGuid = '{0}'", GetParentGuid(GLAccountGuid));
                        query2 += string.Format(" AND AssetLedger.ParentGuid = '{0}'", AssetParentGuid);
                    }

                    SqlCommand sqlCmd = new SqlCommand(query2, connection);

                    SqlDataReader reader = sqlCmd.ExecuteReader();
                    Guid currentTransactionGuid = Guid.Empty;
                    while (reader.Read())
                    {
                        LedgerEntry ledgerEntry = new LedgerEntry();

                        ledgerEntry.Amount = (decimal)reader["Amount"];
                        ledgerEntry.TransactionDate = (DateTime)reader["TransactionDate"];

                        ledgerEntry.AssetInternalIdNumber = (string)reader["AssetInternalIdNumber"];
                        ledgerEntry.AssetExternalIdNumber = (string)reader["AssetExternalIdNumber"];
                        ledgerEntry.AssetParentGuid = (Guid)reader["AssetParentGuid"];

                        ledgerEntry.LiablityLedgerInternalIdNumber = (string)reader["LiablityLedgerInternalIdNumber"];
                        ledgerEntry.LiablityLedgerInternalIdNumber = (string)reader["LiablityLedgerExternalIdNumber"];
                        ledgerEntry.LiablityParentGuid = (Guid)reader["LiablityParentGuid"];

                        entryList.Add(ledgerEntry);

                        //if ((Guid)reader["TransactionGuid"] != currentTransactionGuid)
                        //{
                        //    if(ledgerEntry != null)
                        //        entryList.Add(ledgerEntry);
                        //    ledgerEntry = new LedgerEntry();
                        //    ledgerEntry.TransactionGuid = (Guid)reader["TransactionGuid"];
                        //    currentTransactionGuid = ledgerEntry.TransactionGuid;
                        //}

                        //ledgerEntry.TransactionDate = (DateTime)reader["TransactionDate"];
                        //if (reader["InternalIdNumber"] != DBNull.Value)
                        //    ledgerEntry.InteralIdNumber = reader["InternalIdNumber"] as string;
                        //if (reader["ExternalIdNumber"] != DBNull.Value)
                        //    ledgerEntry.ExternalIdNumber = reader["ExternalIdNumber"] as string;
                        //ledgerEntry.Amount = Convert.ToDecimal(reader["Amount"]);

                        //if ((Guid)reader["GLAccountLookupGuid"] == LookupTableValues.Code.GeneralLedgerAccount.AccountsRecievable.Guid
                        //    || (Guid)reader["GLAccountLookupGuid"] == LookupTableValues.Code.GeneralLedgerAccount.GrowerCredits.Guid)
                        //{
                        //    ledgerEntry.AssetParentGuid = (Guid)reader["ParentGuid"];
                        //}
                        //else
                        //    ledgerEntry.LiabilityParentGuid = (Guid)reader["ParentGuid"];

                        //entryList.Add(ledgerEntry);
                    }
                    //if (ledgerEntry != null)
                    //    entryList.Add(ledgerEntry);

                    var sortedResponseList = entryList.OrderBy(x => x.TransactionDate).ToList();

                    return sortedResponseList;

                }
            }
            catch (Exception ex)
            {
                return entryList;
            }
        }

        public LedgerEntityResponse LedgerEntityRunningBalances(Guid GLAccountLookupGuid)
        {

            LedgerEntityResponse Response = new LedgerEntityResponse();
            Response.EntityList = new List<LedgerEntityBalance>();

            Guid increasingEntyTypeGuid = Guid.Empty;
            Guid decreasingEntyTypeGuid = Guid.Empty;
            string transactionType = "";
            GetAccountsGuids(GLAccountLookupGuid, out increasingEntyTypeGuid, out decreasingEntyTypeGuid, out transactionType);

            string query = string.Format
                (
                @"
 
                    select 
                    ISNULL (decRecs.InternalIdNumber, incRecs.InternalIdNumber)  as InternalIdNumber, 
                    ISNULL (decRecs.ExternalIdNumber, incRecs.ExternalIdNumber) as ExternalIdNumber, 
                    ISNULL (decRecs.TransactionDate, incRecs.TransactionDate) as Date, 
                    ISNULL (decRecs.DecrementFinalTotal, 0) as DecrementFinalTotal,
                    incRecs.InternalIdNumber as IncreaseInternalNumber, 
                    incRecs.ExternalIdNumber as IncreaseExternalIdNumber, 
                    incRecs.TransactionDate as IncreaseDate, 
                    incRecs.IncrmentFinalTotal as IncrmentFinalTotal,
                    incRecs.ParentGuid as ParentGuid,
                    ISNULL (DecreaseAmount, 0) as DecreaseTotal, 
                    IncreaseAmount as IncreaseTotal,
                    ISNULL((IncreaseAmount - DecreaseAmount), IncreaseAmount) as Balance, 
                    ISNULL((IncrmentFinalTotal - DecrementFinalTotal), IncrmentFinalTotal)  as FinalBalance 
					from
					(
	                    SELECT 
	                    a.TransactionDate as TransactionDate, 
	                    a.Amount, 
	                    a.InternalIdNumber as InternalIdNumber, 
	                    a.ExternalIdNumber as ExternalIdNumber, 
	                    a.ParentGuid as ParentGuid,
	                    (select sum(c.Amount) from Ledger c where c.ParentGuid = a.ParentGuid
		                    AND c.GrowerGuid = '{1}'
		                    AND c.GLAccountLookupGuid = '{0}'
		                    AND c.EntryTypeLookupGuid = '{2}'
	                    ) as IncrmentFinalTotal,
	                    ISNULL((SELECT SUM(b.Amount) 
		                    FROM Ledger b
		                    WHERE b.TransactionDate <= a.TransactionDate
		                    AND b.ParentGuid = a.ParentGuid
		                    AND b.GrowerGuid = '{1}'
		                    AND b.GLAccountLookupGuid = '{0}'
		                    AND b.EntryTypeLookupGuid = '{2}'), a.Amount
	                    ) as IncreaseAmount
	                    FROM  Ledger a
	                    where a.GLAccountLookupGuid = '{0}'
	                    AND a.GrowerGuid = '{1}'
	                    AND a.EntryTypeLookupGuid = '{2}'
                    )incRecs 
					left join 
                    (
	                    SELECT a.TransactionDate as TransactionDate, 
	                    a.Amount, a.InternalIdNumber as InternalIdNumber, 
	                    a.ExternalIdNumber as  ExternalIdNumber,
	                    a.ParentGuid,
	                    ISNULL ((select sum(c.Amount) from Ledger c where c.ParentGuid = a.ParentGuid
		                    AND c.GrowerGuid = '{1}'
		                    AND c.GLAccountLookupGuid = '{0}'
		                    AND c.EntryTypeLookupGuid = '{3}'), a.Amount
	                    ) as DecrementFinalTotal,
	                    ISNULL((SELECT SUM(b.Amount) 
		                    FROM Ledger b
		                    WHERE b.TransactionDate <= a.TransactionDate
		                    AND b.ParentGuid = a.ParentGuid
		                    AND b.GrowerGuid = '{1}'
		                    AND b.GLAccountLookupGuid = '{0}'
		                    AND b.EntryTypeLookupGuid = '{3}'), a.Amount
	                    ) as DecreaseAmount
	                    FROM  Ledger a
	                    where a.GLAccountLookupGuid = '{0}'
	                    AND a.GrowerGuid = '{1}'
	                    AND a.EntryTypeLookupGuid = '{3}'
                    )decRecs 
					on decRecs.ParentGuid = incRecs.ParentGuid 
                    order by Date", GLAccountLookupGuid, GrowerGuid, increasingEntyTypeGuid, decreasingEntyTypeGuid
                );
            try
            {
                using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();

                    command.CommandText = query;

                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        LedgerEntityBalance entityBal = new LedgerEntityBalance();

                        entityBal.IncreaseTotal = (decimal)reader["IncreaseTotal"];

                        if (reader["DecreaseTotal"] != DBNull.Value)
                            entityBal.DecreaseTotal = (decimal)reader["DecreaseTotal"];
                        else
                            entityBal.DecreaseTotal = 0;
                        if (reader["Balance"] != DBNull.Value)
                            entityBal.Balance = (decimal)reader["Balance"];
                        else
                            entityBal.Balance = entityBal.IncreaseTotal - entityBal.DecreaseTotal;

                        entityBal.ExternalIdNumber = (string)reader["ExternalIdNumber"];
                        entityBal.DecreaseExternalIdNumber = "";
                        //if (reader["DecreaseExternalIdNumber"] != DBNull.Value)
                        //    entityBal.DecreaseExternalIdNumber = (string)reader["DecreaseExternalIdNumber"];
                        entityBal.GrowerGuid = GrowerGuid;
                        entityBal.InternalIdNumber = (string)reader["InternalIdNumber"];
                        entityBal.ParentGuid = (Guid)reader["ParentGuid"];
                        entityBal.TransactionDate = (DateTime)reader["Date"];
                        entityBal.TransactionType = transactionType;
                        entityBal.FinalBalance = (decimal)reader["FinalBalance"];

                        Response.EntityList.Add(entityBal);
                    }
                    Response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Response.Success = false;
                Response.Message = ex.Message;
            }
            return Response;
        }

        private void GetAccountsGuids(Guid GLAccountLookupGuid, out Guid increasingEntyTypeGuid, out Guid decreasingEntyTypeGuid, out string transactionType)
        {
            increasingEntyTypeGuid = Guid.Empty;
            decreasingEntyTypeGuid = Guid.Empty;
            transactionType = "";
            if (GLAccountLookupGuid == LookupTableValues.Code.GeneralLedgerAccount.AccountsRecievable.Guid)
            {
                transactionType = "Invoice";
                increasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
                decreasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;
            }
            else if (GLAccountLookupGuid == LookupTableValues.Code.GeneralLedgerAccount.UnappliedPayments.Guid)
            {
                transactionType = "Check";
                increasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;
                decreasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
            }
            else if (GLAccountLookupGuid == LookupTableValues.Code.GeneralLedgerAccount.UnappliedCredits.Guid)
            {
                transactionType = "Credit";
                increasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;
                decreasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
            }
            else if (GLAccountLookupGuid == LookupTableValues.Code.GeneralLedgerAccount.UnappliedCreditCardPayments.Guid)
            {
                transactionType = "Credit Card";
                increasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;
                decreasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
            }
        }

        public LedgerEntityResponse LedgerEntityBalances (Guid GLAccountLookupGuid)
        {
            ///////////////////////////////////////////////
            // baic rule:  Debits increase Assets
            //             Credits increase Liabliites
            //////////////////////////////////////////////

            // get bal of a sequence of transaction on a given GLAccount for a given Transaction (Entity) type (parent)
            // where Transaction types (parents) are:
            //      - OrderGuid (Invoice)
            //      - CheckGuid (Grower Check Payment)
            //      - CreditGuid (Credit Memo)
            // and account types are:
            //      - Accounts Recievable (Invoices)
            //      - Accounts Payable (Grower Check Payments)
            //      - Grower Credits (Credit Memo)
            //

            // TBD: get this from the triple table if Rick can update triple table with new lookup guids

            LedgerEntityResponse Response = new LedgerEntityResponse();
            Response.EntityList = new List<LedgerEntityBalance>();

            Guid increasingEntyTypeGuid = Guid.Empty;
            Guid decreasingEntyTypeGuid = Guid.Empty;
            string transactionType = "";
            if (GLAccountLookupGuid == LookupTableValues.Code.GeneralLedgerAccount.AccountsRecievable.Guid)
            {
                transactionType = "Invoice";
                increasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
                decreasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;
            }
            else if (GLAccountLookupGuid == LookupTableValues.Code.GeneralLedgerAccount.UnappliedPayments.Guid)
            {
                transactionType = "Check";
                increasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;
                decreasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
            }
            else if (GLAccountLookupGuid == LookupTableValues.Code.GeneralLedgerAccount.UnappliedCredits.Guid)
            {
                // TBD:  Think Dan has this wrong in the triple table
                transactionType = "Credit";
                increasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;
                decreasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
            }
            else if (GLAccountLookupGuid == LookupTableValues.Code.GeneralLedgerAccount.UnappliedCreditCardPayments.Guid)
            {
                transactionType = "Credit Card";
                increasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;
                decreasingEntyTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
            }


            try
            {
                using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();

                    command.CommandText = string.Format(@"
                        select 
                        IncreaseTotal, 
                        DecreaseTotal, 
                        (IncreaseTotal - DecreaseTotal) as Balance, 
                        ParentGuid, 
                        GrowerGuid,
                        InternalIdNumber,
                        ExternalIdNumber,
                        TransactionDate,
                        DecreaseExternalIdNumber  
                        from
                        (select 
                        sum(Amount) as IncreaseTotal, 
                        ParentGuid as ParentGuid, 
                        InternalIdNumber as InternalIdNumber,
                        ExternalIdNumber as ExternalIdNumber,
                        GrowerGuid as GrowerGuid,
                        TransactionDate as TransactionDate  
                        from Ledger 
                        where 
                        GLAccountLookupGuid = '{0}' AND
                        EntryTypeLookupGuid = '{1}'
                        group by 
                        ParentGuid,
                        GLAccountLookupGuid,
                        EntryTypeLookupGuid,
                        GrowerGuid,
                        InternalIdNumber,
                        ExternalIdNumber,
                        TransactionDate
                        )
                        Increase
                        left join
                        (select 
                        sum(Amount) as DecreaseTotal, 
                        ParentGuid as  DecreaseParentGuid,
                        ExternalIdNumber as DecreaseExternalIdNumber
                        from Ledger 
                        where 
                        GLAccountLookupGuid = '{0}' AND
                        EntryTypeLookupGuid = '{2}'
                        group by 
                        ParentGuid,
                        GLAccountLookupGuid,
                        EntryTypeLookupGuid,
                        ExternalIdNumber
                        )
                        Decrease 
                        on Decrease.DecreaseParentGuid = Increase.ParentGuid",
                        GLAccountLookupGuid,
                        increasingEntyTypeGuid,
                        decreasingEntyTypeGuid);

                    if (GrowerGuid != Guid.Empty)
                        command.CommandText += string.Format(" where GrowerGuid = '{0}'", GrowerGuid);

                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        LedgerEntityBalance entityBal = new LedgerEntityBalance();

                        entityBal.IncreaseTotal = (decimal)reader["IncreaseTotal"];

                        if (reader["DecreaseTotal"] != DBNull.Value)
                            entityBal.DecreaseTotal = (decimal)reader["DecreaseTotal"];
                        else
                            entityBal.DecreaseTotal = 0;
                        if (reader["Balance"] != DBNull.Value)
                            entityBal.Balance = (decimal)reader["Balance"];
                        else
                            entityBal.Balance = entityBal.IncreaseTotal - entityBal.DecreaseTotal;

                        entityBal.ExternalIdNumber = (string)reader["ExternalIdNumber"];
                        entityBal.DecreaseExternalIdNumber = "";
                        if (reader["DecreaseExternalIdNumber"] != DBNull.Value)
                            entityBal.DecreaseExternalIdNumber = (string)reader["DecreaseExternalIdNumber"];
                        entityBal.GrowerGuid = (Guid)reader["GrowerGuid"];
                        entityBal.InternalIdNumber = (string)reader["InternalIdNumber"];
                        entityBal.ParentGuid = (Guid)reader["ParentGuid"];
                        entityBal.TransactionDate = (DateTime)reader["TransactionDate"];
                        entityBal.TransactionType = transactionType;

                        Response.EntityList.Add(entityBal);
                    }
                    Response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Response.Success = false;
                Response.Message = ex.Message;
            }
            return Response;

        }

        public LedgerAccountBalance GetAccountBalance(Guid GLAccountLookupGuid)
        {
            LedgerAccountBalance response = new LedgerAccountBalance();
            Guid debitAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Debit.Guid;
            Guid creditAccountEntryTypeGuid = LookupTableValues.Code.GeneralLedgerEntryType.Credit.Guid;

            try
            {
                using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();

                    command.CommandText = string.Format(
                        @"select (CreditTotal - DebitTotal) as AccountBalance, 
                        DebitTotal, 
                        CreditTotal 
                    from 
                    (select 
                        ISNULL(DebitTbl.AmtTotal, 0) as DebitTotal,  
                        ISNULL(CreditTbl.AmtTotal, 0) as CreditTotal
                    from
                    (select 
                        sum(Amount) as AmtTotal, 
                        GrowerGuid from Ledger 
                        where GrowerGuid = '{0}'
                        AND GLAccountLookupGuid = '{1}'
                        AND EntryTypeLookupGuid = '{2}'
                    group by GrowerGuid
                    ) DebitTbl
                full outer join
                    (select 
                        sum(Amount) as AmtTotal, 
                        GrowerGuid from Ledger 
                        where GrowerGuid = '{0}'
                        AND GLAccountLookupGuid = '{1}'
                        AND EntryTypeLookupGuid = '{3}'
                    group by GrowerGuid
                ) CreditTbl
                on DebitTbl.GrowerGuid = CreditTbl.GrowerGuid) AgTbl",
                        GrowerGuid,
                        GLAccountLookupGuid,
                        debitAccountEntryTypeGuid,
                        creditAccountEntryTypeGuid
                    );

                    var reader = command.ExecuteReader();

                    if (!reader.HasRows)
                    {
                        response.AccountBalance = 0;
                        response.CreditTotal = 0;
                        response.DebitTotal = 0;
                    }

                    while (reader.Read())
                    {
                        response.AccountBalance = (decimal)reader["AccountBalance"];
                        response.CreditTotal = (decimal)reader["CreditTotal"];
                        response.DebitTotal = (decimal)reader["DebitTotal"];
                    }
                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;

        }

        private Guid GetParentGuid(Guid GLAccountLookupGuid)
        {
            if(GLAccountLookupGuid  == LookupTableValues.Code.GeneralLedgerAccount.AccountsRecievable.Guid
                || GLAccountLookupGuid  == LookupTableValues.Code.GeneralLedgerAccount.GrowerCredits.Guid)
            {
                return AssetParentGuid;
            }
            return LiabilityParentGuid;
        }

        private void CreateLedger(SqlCommand command, /*Guid parentGuid,*/ decimal amount, DateTime transactionDate,
            Guid growerGuid, Guid supplierGuid, Guid entryTypeLookupGuid, Guid GLAccountLookupGuid, Guid transactionGuid,
            string internalNumber, string externalNumber)
        {
            Guid parentGuid = GetParentGuid(GLAccountLookupGuid);
            command.CommandText = string.Format(
                @"Insert into Ledger 
                    (Guid,                            
                    ParentGuid,                     
                    Amount,                         
                    TransactionDate,                 
                    GrowerGuid,                      
                    SupplierGuid,                    
                    EntryTypeLookupGuid,             
                    GLAccountLookupGuid,             
                    TransactionGuid,                 
                    InternalIdNumber,                
                    ExternalIdNumber) 
                    VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}')",
                Guid.NewGuid(),
                parentGuid,
                amount,
                transactionDate,
                growerGuid,
                supplierGuid,
                entryTypeLookupGuid,
                GLAccountLookupGuid,
                transactionGuid,
                internalNumber,
                externalNumber
                );
            command.ExecuteNonQuery();

        }

        public string GetInternalIdNumber(SqlCommand command, string sequenceName, string preFix, ref long sequenceNumber)
        {
            string internalIdNumber = "";

            command.CommandText =
                string.Format("UPDATE Sequence SET SequenceNumber =  SequenceNumber + 1 where SequenceName like '{0}'", sequenceName);
            command.ExecuteNonQuery();

            command.CommandText =
                string.Format("select * from Sequence where SequenceName = '{0}'", sequenceName);
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
                throw new Exception("Sequence table read failed");

            reader.Read();

            sequenceNumber = (long)reader["SequenceNumber"];
            if (sequenceNumber < 999999)
                internalIdNumber = string.Format("{0}{1:000000}", preFix, sequenceNumber);
            else if (sequenceNumber < 999999999)
                internalIdNumber = string.Format("{0}{1:000000000}", preFix, sequenceNumber);
            reader.Close();

            return internalIdNumber;
        }

        //private void UpdateInternalIdNumber(SqlCommand command, string sequenceName, long sequenceNumber)
        //{
        //    command.CommandText =
        //        string.Format("Update Sequence set SequenceNumber='{0}' where SequenceName = '{1}'",
        //        sequenceNumber + 1, sequenceName);
        //    command.ExecuteNonQuery();

        //}

        private void UpdateGrowerOrderStatus(SqlCommand command, Guid InvoicedLookupStatus, Guid growerOrderGuid)
        {
            command.CommandText =
                string.Format("Update GrowerOrder set GrowerOrderStatusLookupGuid='{0}' where Guid = '{1}'",
                InvoicedLookupStatus, growerOrderGuid);
            command.ExecuteNonQuery();
        }


        public int CheckInvoiceUpdateStatus(Guid userGuid,  string InternalIDNumber)
        {
           ;
            int iResult = 0;
            try
            {
                var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
                var command = new SqlCommand("[dbo].[InvoiceCheckLedgerUpdateStatus]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

              
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = userGuid;
                command.Parameters
                    .Add(new SqlParameter("@InternalIDNumber", SqlDbType.NVarChar, 10))
                    .Value = InternalIDNumber;
               

                connection.Open();
                iResult = (int)command.ExecuteNonQuery();
                connection.Close();


            }
            catch (Exception ex)
            {
                var errormsg = ex.Message;
                iResult = 0;
            }
            return iResult;
        }



    }
 }


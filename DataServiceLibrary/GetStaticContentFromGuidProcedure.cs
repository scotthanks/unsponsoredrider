﻿using System;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public class GetStaticContentFromGuidProcedure : Procedure<GetStaticContentFromGuidProcedure.GetStaticContentByGuidParameters>
    {
        public GetStaticContentFromGuidProcedure()
        {
            ProcedureName = "GetStaticContentFromGuid";
        }

        public class GetStaticContentByGuidParameters : ProcedureParameterBase
        {
            public Guid Guid { get; set; }

            public override void AddTo(SqlCommand command)
            {
                var contentGuidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier);
                contentGuidParameter.Value = Guid;
                command.Parameters.Add(contentGuidParameter);
            }

            public override void GetOutputValuesFrom(SqlCommand command)
            {
            }
        };
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public sealed partial class LookupTableService
    {
        private static volatile LookupTableService _instance;
        private static readonly object SyncRoot = new Object();

        private Dictionary<string, Lookup> _pathDictioary;

        private Dictionary<string, Lookup> PathDictionary
        {
            get
            {
                if (_pathDictioary == null)
                {
                    _pathDictioary = new Dictionary<string, Lookup>();
                }

                return _pathDictioary;
            }
        }

        //TODO: Put in some mechanism that will prevent anyone from instantiating LookupTableService except this property (and any unit tests that need to).
        public static LookupTableService SingletonInstance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new LookupTableService();
                        }
                    }
                }

                return _instance;
            }
        }

        public static void Initialize()
        {
            var lookupTableService = LookupTableService.SingletonInstance;
            lookupTableService.ClearCache();
            lookupTableService.GetAll();            
        }

        [Obsolete("Use GetByCode(string path, string code, bool excludeInactive) instead.")]
        public new Lookup GetByCode(string code, bool excludeInactive = false)
        {
            throw new NotImplementedException("The GetByCode() method cannot be used on Lookups because codes are only unique within a single parent. Use GetByPath instead.");
        }

        [Obsolete("Use TryGetByCode(string path, string code, bool excludeInactive) instead.")]
        public new Lookup TryGetByCode(string code, bool excludeInactive = false)
        {
            throw new NotImplementedException("The TryGetByCode() method cannot be used on Lookups because codes are only unique within a single parent. Use GetByPath instead.");
        }

        public Lookup GetByCode(Lookup parentLookup, string code, bool excludeInactive = false)
        {
            var lookupTableService = new LookupTableService();

            return GetByCode(parentLookup.Path, code, excludeInactive);
        }

        private Lookup GetByCode(string parentPath, string code, bool excludeInactive = false)
        {
            parentPath = CleanUpParentPath(parentPath);

            return GetByPath(string.Format("{0}{1}", parentPath, code.Trim()));
        }

        public Lookup TryGetByCode(string parentPath, string code, bool excludeInactive = false)
        {
            parentPath = CleanUpParentPath(parentPath);

            return TryGetByPath(string.Format("{0}{1}", parentPath, code.Trim()));
        }

        private string CleanUpParentPath(string parentPath)
        {
            parentPath = parentPath.Trim();
            if (parentPath.Length > 0)
            {
                if (parentPath.Substring(parentPath.Length - 1, 1) == "/")
                    parentPath = parentPath.Substring(0, parentPath.Length - 1);
            }

            if (parentPath.Length > 0)
            {
                parentPath = parentPath + "/";
            }

            return parentPath;
        }

        //TODO: We no longer to lookup these by path, because we always know the guid.
        public Lookup GetByPath(string path, bool excludeInactive = true)
        {
            Lookup lookup;

            if (string.IsNullOrEmpty(path))
            {
                string valueDescription = path == null ? "null" : "empty";

                throw new ApplicationException(string.Format("The path is {0}.", valueDescription));
            }

            if (PathDictionary.TryGetValue(path, out lookup))
            {
                return lookup;
            }

            lookup = TryGetByPath(path, excludeInactive);

            if (lookup == null)
            {
                throw new ApplicationException(string.Format("No {0} was found with a path of {1}.", typeof(Lookup).Name, path));
            }

            PathDictionary.Add(path, lookup);

            return lookup;
        }

        public Lookup TryGetByPath(string path, bool excludeInactive = true)
        {
            Lookup lookup = default(Lookup);

            if (path != null)
            {
                path = path.Trim();
            }

            if (!string.IsNullOrEmpty(path))
            {
                //T cachedDataObject = TryGetFromCache(path);

                //if (cachedDataObject != null)
                //{
                //    lookup = cachedDataObject;
                //}
                //else
                //{
                    //TODO: Shouldn't we notice if there are more than one?
                    lookup = TryGetOneFromReader(connection => GetByPathReader(connection, path, excludeInactive));
                //}
            }

            return lookup;
        }

        public List<Lookup> GetAllByPath(string path, bool excludeInactive = true)
        {
            var lookupList = new List<Lookup>();

            if (!string.IsNullOrEmpty(path))
            {
                lookupList = GetAllFromReader(connection => GetByPathReader(connection, path, excludeInactive));
            }

            return lookupList;
        }

        private SqlDataReader GetByPathReader(SqlConnection connection, string path, bool excludeInactive)
        {
            VerifyThatTableNameHasBeenSet();
            var sqlQueryServices = new SqlQueryServices();
            //return sqlQueryServices.GetByFieldStartsWithReader(TableName, connection, "Path", path, excludeInactive);
            return sqlQueryServices.GetByFieldReader(TableName, connection, "Path", path, "=", excludeInactive);
        }

        //NOTE: The templates don't call SetUpParentLookupListLazyLoad() because it is a self reference (the only case in our schema). In this case we do want to call it, so I have fixed SetUpLazyLoadDelegates() so that it gets called.

        new private void SetUpLazyLoadDelegates()
        {
            base.SetUpLazyLoadDelegates(CustomSetUpLazyLoads);
        }

        private void CustomSetUpLazyLoads(Lookup lookup)
        {
            SetUpLazyLoads(lookup);
            SetUpParentLookupListLazyLoad(lookup);
        }
    }
}

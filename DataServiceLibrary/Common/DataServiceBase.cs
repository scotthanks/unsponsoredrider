﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public abstract class DataServiceBase : ServiceBase
    {
        protected string TrimString(object o)
        {
            if (o != null)
            {
                o = ((string) o).Trim();
            }
            return (string)o;
        }
    }
}

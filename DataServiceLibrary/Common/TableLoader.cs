﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public class TableLoader<TR, T>
        where TR : TableService<T>
        where T : TableObjectBase, ICodedObject, new()
    {
        private readonly TR _dataService;
        private string[] _fieldList;
        private Dictionary<string, int> _fieldDictionary;
        private bool _excludeInactive;

        private string DataObjectTypeName
        {
            get { return typeof (T).Name; }
        }

        public TableLoader(TR dataService, bool excludeInactive)
        {
            _dataService = dataService;
            _excludeInactive = excludeInactive;
        }

        public void Load(string[] fieldList)
        {
            if (_fieldList == null)
            {
                LoadFieldList(fieldList);
            }
            else
            {
                LoadData(fieldList);
            }
        }

        public void LoadFieldList(string[] fieldList)
        {
            if (fieldList == null)
            {
                throw new ArgumentNullException("fieldList");
            }

            _fieldList = new string[fieldList.Length];

            _fieldDictionary = new Dictionary<string, int>();

            int index = 0;
            foreach (var fieldName in fieldList)
            {
                string cleanedFieldName = CleanFieldName(fieldName);

                if (_fieldDictionary.ContainsKey(cleanedFieldName))
                {
                    cleanedFieldName = "";
                }
                else
                {
                    _fieldDictionary.Add(cleanedFieldName, index);
                }

                _fieldList[index] = cleanedFieldName;

                index++;
            }
        }

        public void LoadData(string[] data)
        {
            if (_fieldList == null)
            {
                throw new ApplicationException("The field list has not been set.");
            }
            else if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            else if (data.Length > _fieldList.Length)
            {
                throw new ApplicationException(string.Format("The data list length cannot be greater than {0}, but it is {1}.", _fieldList.Length, data.Length));
            }

            T item = null;

            string guidValue = GetFieldData("Guid", data);
            if (!string.IsNullOrEmpty(guidValue))
            {
                item = _dataService.TryGetByGuid(new Guid(guidValue));
            }

            string codeValue = GetFieldData("Code", data);
            if (item == null)
            {
                if (!string.IsNullOrEmpty(codeValue))
                {
                    item = _dataService.TryGetByCode(codeValue, _excludeInactive);
                }
            }

            if (item == null)
            {
                item = new T {Code = codeValue};

                if (!string.IsNullOrEmpty(guidValue))
                {
                    var guid = new Guid(guidValue);

                    if (guid != Guid.Empty)
                    {
                        item.Guid = guid;
                    }
                }

                if (string.IsNullOrEmpty(codeValue))
                {
                    item.Code = codeValue;
                }
            }

            int index = 0;
            foreach (var fieldName in _fieldList)
            {
                string fieldData = data[index];
                bool valueSet = false;
                if (fieldName != "Code" && fieldName != "Guid")
                {
                    valueSet = item.ParseFieldFromString(fieldName, fieldData);

                    if (TableObjects.IsForeignKeyOf(typeof(T), fieldName))
                    {
                        var dataObject = TableServices.TryGetForeignTableObjectFrom(typeof(T), fieldName, fieldData);
                    }
                }
                index++;
            }
        }

        private string GetFieldData(string fieldName, string[] dataList)
        {
            int index = GetIndexOf(fieldName);
            string data = null;

            if (index >= 0)
            {
                data = CleanData(dataList[index]);
            }

            return data;
        }

        private int GetIndexOf(string fieldName)
        {
            int index = -1;
            if (!string.IsNullOrEmpty(fieldName))
            {
                bool found = _fieldDictionary.TryGetValue(fieldName, out index);

                if (!found)
                {
                    index = -1;
                }                
            }

            return index;
        }

        private string CleanFieldName(string fieldName)
        {
            return Clean(fieldName);
        }

        private string CleanData(string data)
        {
            return Clean(data);
        }

        private string Clean(string fieldName)
        {
            if (string.IsNullOrEmpty(fieldName))
            {
                fieldName = "";
            }

            return fieldName.Trim();
        }

        private bool IsForeignKeyCode(string fieldName)
        {

            return DataObjectClassForeignKeyCodeNames.Contains(fieldName);
        }

        private IEnumerable<string> DataObjectClassForeignKeyCodeNames
        {
            get
            {
                var dataObjectClassForeignKeyFieldNames = DataObjectClassForeignKeyFieldNames;
                var dataObjectClassForeignKeyCodeNames =
                    new string[dataObjectClassForeignKeyFieldNames.Length];

                int index = 0;
                foreach(string fieldName in dataObjectClassForeignKeyFieldNames)
                {
                    if (!fieldName.EndsWith("Guid"))
                    {
                        throw new ApplicationException(string.Format( "The field {0} does not end with \"Guid\".", fieldName));
                    }

                    dataObjectClassForeignKeyCodeNames[index++] = fieldName.Substring(0, fieldName.Length - 4) + "Code";
                }

                return dataObjectClassForeignKeyCodeNames;
            }
        }

        private string[] DataObjectClassForeignKeyFieldNames
        {
            get
            {
                string foreignKeyFieldEnumTypeName = DataObjectTypeName + ".ForeignKeyFieldEnum";
                Type type = Type.GetType(foreignKeyFieldEnumTypeName);
                if (type == null)
                {
                    throw new ApplicationException(string.Format("Type {0} not found.", foreignKeyFieldEnumTypeName));
                }
                if (!type.IsEnum)
                {
                    throw new ApplicationException(string.Format("Type {0} is not an Enum.", foreignKeyFieldEnumTypeName));
                }

                return type.GetEnumNames();
            }
        }

        private bool IsForeignKeyField(string fieldName)
        {
                return true;
        }

        private bool GetBooleanStaticPropertyValue(Type type, string propertyName)
        {
            return (bool) type.GetProperty(propertyName).GetGetMethod().Invoke(null, new object[0]);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccessLibrary;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public abstract class TableService<T> : DataService<T,TableObjectBase> where T : TableObjectBase, new()
    {
        private Dictionary<Guid, T> _cache;
        private Dictionary<string, T> _codeCache;

        public Dictionary<Guid, T> Cache
        {
            get
            {
                if (_cache == null)
                {
                    _cache = new Dictionary<Guid, T>();
                }

                return _cache;
            }
        }

        public Dictionary<string, T> CodeCache
        {
            get
            {
                if (_codeCache == null)
                {
                    _codeCache = new Dictionary<string, T>();
                }

                return _codeCache;
            }
        }

        public void ClearCache()
        {
            _cache = null;
            _codeCache = null;            
        }

        public T UpdateField(string code, string fieldName, string newValue)
        {
            ClearCache();
            var newItem = GetOneFromReader(connection => UpdateFieldReader(connection, code, fieldName, newValue));
            return newItem;
        }

        public T UpdateField(Guid guid, string fieldName, object newValue)
        {
            ClearCache();
            var newItem = GetOneFromReader(connection => UpdateFieldReader(connection, guid, fieldName, newValue));
            return newItem;
        }

        private SqlDataReader UpdateFieldReader(SqlConnection connection, string code, string fieldName, string newValue)
        {
            VerifyThatTableNameHasBeenSet();
            var sqlQueryServices = new SqlQueryServices();
            return sqlQueryServices.UpdateFieldReader(TableName, connection, code, fieldName, newValue);
        }

        private SqlDataReader UpdateFieldReader(SqlConnection connection, Guid guid, string fieldName, object newValue)
        {
            VerifyThatTableNameHasBeenSet();
            var sqlQueryServices = new SqlQueryServices();
            return sqlQueryServices.UpdateFieldReader(TableName, connection, guid, fieldName, newValue);
        }

        public IEnumerable<T> GetAllActive(int maxRowsToGet = int.MaxValue)
        {
            if (maxRowsToGet == int.MaxValue)
                return GetAllFromReader(GetAllActiveReader);
            else
                return GetNFromReader(GetAllActiveReader, maxRowsToGet);
        }

        public IEnumerable<TSub> GetAllActive<TSub>(int maxRowsToGet = int.MaxValue) where TSub : T
        {
            return GetAllActive(maxRowsToGet).Select(item => item as TSub).ToList();
        }

        public List<T> GetAllActiveByGuid(Guid? guid, string guidFieldName = null)
        {
            return GetAllFromReader(connection => GetByGuidReader(connection, guid, guidFieldName));
        }

        public List<TSub> GetAllActiveByGuid<TSub>(Guid? guid, string guidFieldName = null) where TSub : T
        {
            return GetAllActiveByGuid(guid, guidFieldName).Select(item => item as TSub).ToList();
        }

        public SqlDataReader GetAllActiveReader(SqlConnection connection)
        {
            VerifyThatTableNameHasBeenSet();
            var sqlQueryServices = new SqlQueryServices();
            return sqlQueryServices.GetAllActiveReader(TableName, connection);
        }

        public T Insert(T dataObject, SqlTransaction transaction = null)
        {
            T item = DefaultInsert(dataObject, transaction);
            //FixAnyLazyLoadedLists(dataObject, ListActionEnum.Inserting);
            return item;
        }

        protected abstract T DefaultInsert(T dataObject, SqlTransaction transaction = null);

        public virtual T Update(T dataObject, SqlTransaction transaction = null)
        {
            ClearCache();

            return DefaultUpdate(dataObject, transaction);
        }

        protected abstract T DefaultUpdate(T dataObject, SqlTransaction transaction = null);

        public T Deactivate(T dataObject)
        {
            ClearCache();
            var sqlQueryService = new SqlQueryServices();
            string sqlDeactivateCommand = sqlQueryService.DeactivateByGuidQuery(TableName);

            return GetOneFromCommand(sqlDeactivateCommand, AddParameters, dataObject);
        }

        public T Activate(T dataObject)
        {
            ClearCache();
            var sqlQueryService = new SqlQueryServices();
            string sqlActivateCommand = sqlQueryService.ActivateByGuidQuery(TableName);

            return GetOneFromCommand(sqlActivateCommand, AddParameters, dataObject);
        }

        public void Delete(T dataObject)
        {
            ClearCache();
            var sqlQueryService = new SqlQueryServices();
            string sqlDeleteCommand = sqlQueryService.DeleteByGuidQuery(TableName);

            var itemWhichShouldntExist = TryGetOneFromCommand(sqlDeleteCommand, AddParameters, dataObject);

            if (itemWhichShouldntExist != null)
            {
                throw new DataException("Delete method returned a value.");
            }
        }

        public virtual void SetUpLazyLoads(T dataObject)
        {
            throw new NotImplementedException();
        }

        protected enum ListActionEnum {Inserting, Deleting};

        protected abstract void FixAnyLazyLoadedLists(T dataObject, ListActionEnum listAction);

        protected void FixLazyLoadedList(List<T> list, T item, ListActionEnum listAction)
        {
            switch (listAction)
            {
                case ListActionEnum.Inserting:
                    if (!list.Contains(item))
                    {
                        list.Add(item);
                    }
                    break;
                case ListActionEnum.Deleting:
                    if (list.Contains(item))
                    {
                        list.Remove(item);
                    }
                    break;
                default:
                    throw new NotImplementedException(string.Format("The ListActionEnum value {0} has not been implemented.", listAction.ToString()));
            }
        }

        protected void SetUpCacheDelegates()
        {
            SetUpCacheDelegates(AddToCacheDelegate, TryGetFromCacheDelegate);
        }

        new private void AddToCacheDelegate(T dataObject)
        {
            TableObjectBase tableObject = dataObject as TableObjectBase;
            if (tableObject == null)
            {
                throw new ApplicationException(string.Format( "Object of type {0} cannot be converted to {1}.", dataObject.GetType().Name, typeof(TableObjectBase).Name));
            }

            Cache.Add(tableObject.Guid, dataObject);

            if (!string.IsNullOrEmpty(tableObject.Code))
            {
                //TODO: Get rid of this sloppy solution to duplicate codes.
                if (!CodeCache.ContainsKey(tableObject.Code.Trim()))
                {
                    CodeCache.Add(tableObject.Code.Trim(), dataObject);                    
                }
            }
        }

        new private T TryGetFromCacheDelegate(T dataObject)
        {
            T cachedDataObject = null;

            TableObjectBase tableObject = dataObject as TableObjectBase;
            if (tableObject == null)
            {
                throw new ApplicationException(string.Format("Object of type {0} cannot be converted to {1}.", dataObject.GetType().Name, typeof(TableObjectBase).Name));
            }

            cachedDataObject = TryGetFromCache(tableObject.Guid);

            return cachedDataObject;
        }

        private T TryGetFromCache(Guid guid)
        {
            T cachedDataObject = null;
            Cache.TryGetValue(guid, out cachedDataObject);
            return cachedDataObject;
        }

        protected T TryGetFromCache(string code)
        {
            T cachedDataObject = null;
            CodeCache.TryGetValue(code, out cachedDataObject);
            return cachedDataObject;
        }

        protected void SetUpLazyLoadDelegates()
        {
            SetUpLazyLoadDelegates(SetUpLazyLoads);
        }

        protected override T DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            throw new NotImplementedException();
        }

        public T TryGetByGuidBase(Guid? guid)
        {
            return TryGetByGuid(guid);
        }

        public T GetByGuid(Guid guid, SqlTransaction transaction = null)
        {
            return GetByGuid((Guid?)guid, transaction);
        }

        public T GetByGuid(Guid? guid, SqlTransaction transaction = null)
        {
            //TODO: Find a better way to handle nulls. It currently returns an empty item.
            if (guid == null || guid == Guid.Empty)
            {
                return new T();
                //    string valueDescription = guid == null ? "null" : "empty";

                //    throw new ApplicationException(string.Format("The guid is {0}.", valueDescription));
            }

            T dataObject = TryGetByGuid(guid, transaction);

            if (dataObject == null)
            {
                throw new ApplicationException(string.Format("No {0} was found with a guid of {1}.", typeof(T).Name, guid));
            }

            return dataObject;
        }

        public bool Exists(Guid guid, SqlTransaction transaction = null)
        {
            var item = TryGetByGuid(guid, transaction);

            return item != null;
        }

        public T TryGetByGuid(Guid? guid, SqlTransaction transaction = null)
        {
            T dataObject = default(T);

            if (guid != null && guid != Guid.Empty)
            {
                T cachedDataObject = TryGetFromCache((Guid)guid);

                if (cachedDataObject != null)
                {
                    dataObject = cachedDataObject;
                }
                else
                {
                    dataObject = TryGetOneFromReader(connection => GetByGuidReader(connection, guid, transaction: transaction));
                }
            }

            return dataObject;
        }

        public T GetByGuidBase(Guid? guid)
        {
            return GetByGuid(guid);
        }

        protected SqlDataReader GetByGuidReader(SqlConnection connection, Guid? guid, string guidFieldName = null, SqlTransaction transaction = null)
        {
            VerifyThatTableNameHasBeenSet();
            var sqlQueryServices = new SqlQueryServices();
            return sqlQueryServices.GetByGuidReader(TableName, connection, guid, guidFieldName, true, transaction);
        }

        public T TryGetByCodeBase(string code, bool excludeInactive)
        {
            return TryGetByCode(code, excludeInactive);
        }

        public T GetByCodeBase(string code, bool excludeInactive)
        {
            return (T)GetByCode(code, excludeInactive);
        }

        public T GetByCode(string code, bool excludeInactive = true)
        {
            if (string.IsNullOrEmpty(code))
            {
                string valueDescription = code == null ? "null" : "empty";

                throw new ApplicationException(string.Format("The code is {0}.", valueDescription));
            }

            T dataObject = TryGetByCode(code, excludeInactive);

            if (dataObject == null)
            {
                throw new ApplicationException(string.Format("No {0} was found with a code of {1}.", typeof(T).Name, code));
            }

            return dataObject;
        }

        public T TryGetByCode(string code, bool excludeInactive)
        {
            T dataObject = default(T);

            if (!string.IsNullOrEmpty(code))
            {
                T cachedDataObject = TryGetFromCache(code);

                if (cachedDataObject != null)
                {
                    dataObject = cachedDataObject;
                }
                else
                {
                    dataObject = TryGetOneFromReader(connection => GetByCodeReader(connection, code, excludeInactive));
                }
            }

            return dataObject;
        }

        public T TryGetByField(string fieldName, string value, string operatorString, bool excludeInactive)
        {
            T dataObject = default(T);

            dataObject = TryGetOneFromReader(connection => GetByFieldReader(connection, fieldName, value, operatorString, excludeInactive));

            return dataObject;
        }

        public List<T> GetAllByField(string fieldName, string value, string operatorString, bool excludeInactive)
        {
            return GetAllFromReader( connection => GetByFieldReader(connection, fieldName, value, operatorString, excludeInactive));
        }

        private SqlDataReader GetByCodeReader(SqlConnection connection, string code, bool excludeInactive)
        {
            VerifyThatTableNameHasBeenSet();
            var sqlQueryServices = new SqlQueryServices();
            return sqlQueryServices.GetByCodeReader(TableName, connection, code, excludeInactive);
        }

        private SqlDataReader GetByFieldReader(SqlConnection connection, string fieldName, string value, string operatorString, bool excludeInactive)
        {
            VerifyThatTableNameHasBeenSet();
            var sqlQueryServices = new SqlQueryServices();
            return sqlQueryServices.GetByFieldReader(TableName, connection, fieldName, value, operatorString, excludeInactive);
        }

        protected delegate void AddParametersDelegate(SqlCommand command, T tableObject);

        protected T GetOneFromCommand(string sqlCommand, AddParametersDelegate addParametersDelegate, T tableObject, SqlTransaction transaction = null)
        {
            sqlCommand = FixCommand(sqlCommand);

            var sqlQueryServices = new SqlQueryServices();

            return
                GetOneFromReader
                (
                    connection =>
                    sqlQueryServices.ReaderFromSqlQuery
                    (
                        connection,
                        sqlCommand,
                        cmd => AddParameters(cmd, tableObject),
                        transaction: transaction
                    )
                );            
        }

        protected T TryGetOneFromCommand(string sqlCommand, AddParametersDelegate addParametersDelegate, T tableObject, SqlTransaction transaction = null)
        {
            sqlCommand = FixCommand(sqlCommand);

            var sqlQueryServices = new SqlQueryServices();

            return
                TryGetOneFromReader
                (
                    connection =>
                    sqlQueryServices.ReaderFromSqlQuery
                    (
                        connection,
                        sqlCommand,
                        cmd => AddParameters(cmd, tableObject),
                        transaction: transaction
                    )
                );
        }

        private static string FixCommand(string sqlCommand)
        {
            sqlCommand =
                sqlCommand.Replace
                (
                    ", )",
                    ")"
                );

            sqlCommand =
                sqlCommand.Replace
                (
                    ", WHERE [",
                    " WHERE ["
                );

            return sqlCommand;
        }
    }
}
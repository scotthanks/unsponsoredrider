﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class ImageSetTableService : TableService<ImageSet>
    {
        public ImageSetTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override ImageSet DefaultInsert(ImageSet imageSet, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [ImageSet]([Guid], [Code], [Name], [Caption], [Description], [ImageSetTypeLookupGuid], [Notes], [OriginalImageFileGuid], [DateDeactivated], ) VALUES (@Guid, @Code, @Name, @Caption, @Description, @ImageSetTypeLookupGuid, @Notes, @OriginalImageFileGuid, @DateDeactivated, );SELECT * FROM [ImageSet] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, imageSet, transaction: transaction);
        }

        protected override ImageSet DefaultUpdate(ImageSet imageSet, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [ImageSet] SET [Code]=@Code, [Name]=@Name, [Caption]=@Caption, [Description]=@Description, [ImageSetTypeLookupGuid]=@ImageSetTypeLookupGuid, [Notes]=@Notes, [OriginalImageFileGuid]=@OriginalImageFileGuid, [DateDeactivated]=@DateDeactivated, WHERE [Guid]=@Guid;SELECT * FROM [ImageSet] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, imageSet, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, ImageSet imageSet)
        {
            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = imageSet.Guid;
            command.Parameters.Add(guidParameter);

            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = imageSet.Id;
            command.Parameters.Add(idParameter);

            var codeParameter = new SqlParameter("@Code", SqlDbType.NChar, 50);
            codeParameter.IsNullable = false;
            imageSet.Code = imageSet.Code ?? "";
            imageSet.Code = imageSet.Code.Trim();
            codeParameter.Value = imageSet.Code;
            command.Parameters.Add(codeParameter);

            var nameParameter = new SqlParameter("@Name", SqlDbType.NVarChar, 100);
            nameParameter.IsNullable = false;
            imageSet.Name = imageSet.Name ?? "";
            imageSet.Name = imageSet.Name.Trim();
            nameParameter.Value = imageSet.Name;
            command.Parameters.Add(nameParameter);

            var captionParameter = new SqlParameter("@Caption", SqlDbType.NVarChar, 200);
            captionParameter.IsNullable = false;
            imageSet.Caption = imageSet.Caption ?? "";
            imageSet.Caption = imageSet.Caption.Trim();
            captionParameter.Value = imageSet.Caption;
            command.Parameters.Add(captionParameter);

            var descriptionParameter = new SqlParameter("@Description", SqlDbType.NVarChar, 500);
            descriptionParameter.IsNullable = false;
            imageSet.Description = imageSet.Description ?? "";
            imageSet.Description = imageSet.Description.Trim();
            descriptionParameter.Value = imageSet.Description;
            command.Parameters.Add(descriptionParameter);

            var imageSetTypeLookupGuidParameter = new SqlParameter("@ImageSetTypeLookupGuid", SqlDbType.UniqueIdentifier, 16);
            imageSetTypeLookupGuidParameter.IsNullable = false;
            imageSetTypeLookupGuidParameter.Value = imageSet.ImageSetTypeLookupGuid;
            command.Parameters.Add(imageSetTypeLookupGuidParameter);

            var notesParameter = new SqlParameter("@Notes", SqlDbType.NVarChar, 500);
            notesParameter.IsNullable = false;
            imageSet.Notes = imageSet.Notes ?? "";
            imageSet.Notes = imageSet.Notes.Trim();
            notesParameter.Value = imageSet.Notes;
            command.Parameters.Add(notesParameter);

            var originalImageFileGuidParameter = new SqlParameter("@OriginalImageFileGuid", SqlDbType.UniqueIdentifier, 16);
            originalImageFileGuidParameter.IsNullable = true;
            originalImageFileGuidParameter.Value = imageSet.OriginalImageFileGuid ?? (object)DBNull.Value;
            command.Parameters.Add(originalImageFileGuidParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = imageSet.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

        }

        protected override ImageSet DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var imageSet = new ImageSet();

            string columnName = "";
            try
            {
                columnName = "Guid";
                var guid = reader[(int)ImageSet.ColumnEnum.Guid] ?? Guid.Empty;
                imageSet.Guid = (Guid)guid;

                columnName = "Id";
                var id = reader[(int)ImageSet.ColumnEnum.Id] ?? long.MinValue;
                imageSet.Id = (long)id;

                columnName = "Code";
                var code = reader[(int)ImageSet.ColumnEnum.Code] ?? string.Empty;
                code = TrimString(code);
                imageSet.Code = (string)code;

                columnName = "Name";
                var name = reader[(int)ImageSet.ColumnEnum.Name] ?? string.Empty;
                name = TrimString(name);
                imageSet.Name = (string)name;

                columnName = "Caption";
                var caption = reader[(int)ImageSet.ColumnEnum.Caption] ?? string.Empty;
                caption = TrimString(caption);
                imageSet.Caption = (string)caption;

                columnName = "Description";
                var description = reader[(int)ImageSet.ColumnEnum.Description] ?? string.Empty;
                description = TrimString(description);
                imageSet.Description = (string)description;

                columnName = "ImageSetTypeLookupGuid";
                var imageSetTypeLookupGuid = reader[(int)ImageSet.ColumnEnum.ImageSetTypeLookupGuid] ?? Guid.Empty;
                imageSet.ImageSetTypeLookupGuid = (Guid)imageSetTypeLookupGuid;

                columnName = "Notes";
                var notes = reader[(int)ImageSet.ColumnEnum.Notes] ?? string.Empty;
                notes = TrimString(notes);
                imageSet.Notes = (string)notes;

                columnName = "OriginalImageFileGuid";
                var originalImageFileGuid = reader[(int)ImageSet.ColumnEnum.OriginalImageFileGuid];
                if (originalImageFileGuid == DBNull.Value) originalImageFileGuid = null;
                imageSet.OriginalImageFileGuid = (Guid?)originalImageFileGuid;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)ImageSet.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                imageSet.DateDeactivated = (DateTime?)dateDeactivated;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return imageSet;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(ImageSet imageSet)
        {
            SetUpImageSetTypeLookupLazyLoad(imageSet);
            SetUpOriginalImageFileLazyLoad(imageSet);
            //SetUpClaimListLazyLoad( imageSet);
            SetUpImageFileListLazyLoad(imageSet);
            SetUpProgramTypeListLazyLoad(imageSet);
            SetUpVarietyListLazyLoad(imageSet);
        }

        protected override void FixAnyLazyLoadedLists(ImageSet imageSet, ListActionEnum listAction)
        {
            FixImageSetTypeLookupList(imageSet, listAction);
            FixOriginalImageFileList(imageSet, listAction);
        }

        private void SetUpImageSetTypeLookupLazyLoad(ImageSet imageSet)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            imageSet.SetLazyImageSetTypeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(imageSet.ImageSetTypeLookupGuid), false));
        }

        private void FixImageSetTypeLookupList(ImageSet imageSet, ListActionEnum listAction)
        {
            if (imageSet.ImageSetTypeLookupIsLoaded)
            {
                FixLazyLoadedList(imageSet.ImageSetTypeLookup.ImageSetTypeImageSetList, imageSet, listAction);
            }
        }

        private void SetUpOriginalImageFileLazyLoad(ImageSet imageSet)
        {
            var imageFileTableService = Context.Get<ImageFileTableService>();
            imageSet.SetLazyOriginalImageFile(new Lazy<ImageFile>(() => imageFileTableService.GetByGuid(imageSet.OriginalImageFileGuid), false));
        }

        private void FixOriginalImageFileList(ImageSet imageSet, ListActionEnum listAction)
        {
            if (imageSet.OriginalImageFileIsLoaded)
            {
                FixLazyLoadedList(imageSet.OriginalImageFile.OriginalImageSetList, imageSet, listAction);
            }
        }

        //private void SetUpClaimListLazyLoad(ImageSet imageSet)
        //{
        //    var claimTableService = Context.Get<ClaimTableService>();
        //    imageSet.SetLazyClaimList(new Lazy<List<Claim>>(() => claimTableService.GetAllActiveByGuid(imageSet.Guid, "ImageSetGuid"), false));
        //}

        private void SetUpImageFileListLazyLoad(ImageSet imageSet)
        {
            var imageFileTableService = Context.Get<ImageFileTableService>();
            imageSet.SetLazyImageFileList(new Lazy<List<ImageFile>>(() => imageFileTableService.GetAllActiveByGuid(imageSet.Guid, "ImageSetGuid"), false));
        }

        private void SetUpProgramTypeListLazyLoad(ImageSet imageSet)
        {
            var programTypeTableService = Context.Get<ProgramTypeTableService>();
            imageSet.SetLazyProgramTypeList(new Lazy<List<ProgramType>>(() => programTypeTableService.GetAllActiveByGuid(imageSet.Guid, "ImageSetGuid"), false));
        }

        private void SetUpVarietyListLazyLoad(ImageSet imageSet)
        {
            var varietyTableService = Context.Get<VarietyTableService>();
            imageSet.SetLazyVarietyList(new Lazy<List<Variety>>(() => varietyTableService.GetAllActiveByGuid(imageSet.Guid, "ImageSetGuid"), false));
        }

        #endregion
    }
}

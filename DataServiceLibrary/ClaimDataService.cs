﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;
using DataAccessLibrary;
using System.Data.SqlClient;
using System.Data;
using BusinessObjectsLibrary;

namespace DataServiceLibrary
{
    public partial class ClaimDataGetProcedure: DataServiceBaseNew
    {
        public ClaimDataGetProcedure(StatusObject status)
            : base(status)
        {

        }
    
        public List<ClaimView> GetClaims
            (
                string userCode, 
                Guid? growerGuid = null, 
                bool returnJustMyClaims = true
            )
        {

            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            List<ClaimView> claimList = null;

            try
            {

                var command = new SqlCommand("[dbo].[ClaimDataGet]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                
                command.Parameters
                    .Add(new SqlParameter("@UserCode", SqlDbType.NVarChar, 56))
                    .Value = userCode;
                command.Parameters
                    .Add(new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerGuid;
                command.Parameters
                    .Add(new SqlParameter("@GrowerClaimGuid", SqlDbType.UniqueIdentifier))
                    .Value = null;

                var reader = (SqlDataReader)command.ExecuteReader();
                
                var context = new Context();
                var claimViewService = context.Get<ClaimViewService>();
                claimList = claimViewService.GetAllFromReader(reader);

                reader.NextResult();

                var lookupService = context.Get<LookupTableService>();
                lookupService.GetAllFromReader(reader);


                //TODO: These should not be necessary. Lazy loading is supported by views!!!
                foreach (var oClaimView in claimList)
                {
                    SetUpShipWeekLazyLoad(context, oClaimView);
                    SetUpClaimStatusLookupLazyLoad(context, oClaimView);
                    SetUpProductFormCategoryLazyLoad(context, oClaimView);
                }
                
               
               


            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CartService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;
            }
            finally
            {
                connection.Close();

            }
            return claimList;


        }

        private void SetUpShipWeekLazyLoad(Context context, DataObjectLibrary.ClaimView oClaimView)
        {
            var oClaimViewTableService = context.Get<ShipWeekTableService>();
            oClaimView.SetLazyShipWeek(new Lazy<DataObjectLibrary.ShipWeek>(() => oClaimViewTableService.GetByGuid(oClaimView.ShipWeekGuid), false));
        }
        private void SetUpClaimStatusLookupLazyLoad(Context context, DataObjectLibrary.ClaimView oClaimView)
        {
            var lookupTableService = context.Get<LookupTableService>();
            oClaimView.SetLazyClaimStatusLookup(new Lazy<DataObjectLibrary.Lookup>(() => lookupTableService.GetByGuid(oClaimView.ClaimStatusLookupGuid), false));
        }

        private void SetUpProductFormCategoryLazyLoad(Context context, DataObjectLibrary.ClaimView oClaimView )
        {
            var oProductFormCategoryTableService = context.Get<ProductFormCategoryTableService>();
        //    oClaimView.SetLazyProductFormCategory(new Lazy<ProductFormCategory>(() => oProductFormCategoryTableService.GetByGuid(oClaimView.ProductFormCategoryGuid), false));
        }


        //private void SetUpGrowerLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        //{
        //    var growerTableService = context.Get<GrowerTableService>();
        //    growerOrderSummaryByShipWeekView.SetLazyGrower(new Lazy<DataObjectLibrary.Grower>(() => growerTableService.GetByGuid(growerOrderSummaryByShipWeekView.GrowerGuid), false));
        //}

        //private void SetUpGrowerOrderLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        //{
        //    var growerOrderTableService = context.Get<GrowerOrderTableService>();
        //    growerOrderSummaryByShipWeekView.SetLazyGrowerOrder(new Lazy<DataObjectLibrary.GrowerOrder>(() => growerOrderTableService.GetByGuid(growerOrderSummaryByShipWeekView.GrowerOrderGuid), false));
        //}

     

        //private void SetUpProductFormCategoryLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        //{
        //    var supplierOrderTableService = context.Get<ProductFormCategoryTableService>();
        //    growerOrderSummaryByShipWeekView.SetLazyProductFormCategory(new Lazy<ProductFormCategory>(() => supplierOrderTableService.GetByGuid(growerOrderSummaryByShipWeekView.ProductFormCategoryGuid), false));
        //}

        //private void SetUpGrowerOrderStatusLookupLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        //{
        //    var lookupTableService = context.Get<LookupTableService>();
        //    growerOrderSummaryByShipWeekView.SetLazyGrowerOrderStatusLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(growerOrderSummaryByShipWeekView.GrowerOrderStatusLookupGuid), false));
        //}

        //private void SetUpLowestSupplierOrderStatusLookupLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        //{
        //    var lookupTableService = context.Get<LookupTableService>();
        //    growerOrderSummaryByShipWeekView.SetLazyLowestSupplierOrderStatusLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(growerOrderSummaryByShipWeekView.LowestSupplierOrderStatusLookupGuid), false));
        //}

        //private void SetUpLowestOrderLineStatusLookupLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        //{
        //    var lookupTableService = context.Get<LookupTableService>();
        //    growerOrderSummaryByShipWeekView.SetLazyLowestOrderLineStatusLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(growerOrderSummaryByShipWeekView.LowestOrderLineStatusLookupGuid), false));
        //}

        //private void SetUpPersonLazyLoad(Context context, DataObjectLibrary.GrowerOrderSummaryByShipWeekView growerOrderSummaryByShipWeekView)
        //{
        //    var personTableService = context.Get<PersonTableService>();
        //    growerOrderSummaryByShipWeekView.SetLazyPerson(new Lazy<Person>(() => personTableService.GetByGuid(growerOrderSummaryByShipWeekView.PersonGuid), false));
        //}
    }
}

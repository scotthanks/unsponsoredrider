﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;
using Utility;

namespace DataServiceLibrary
{
    public partial class EventLogTableService
    {
        public EventLog InsertGrowerLog(Guid userGuid, string userCode, string processLookupCode, Lookup logTypeLookup, string text, Grower grower)
        {
            return InsertAnyGrowerLog(userGuid, userCode, processLookupCode, logTypeLookup, text,
                                   LookupTableValues.Logging.ObjectType.Grower, grower.Guid);
        }

        public EventLog InsertGrowerOrderLog(Guid userGuid, string userCode, string processLookupCode, Lookup logTypeLookup, string text, GrowerOrder growerOrder, SqlTransaction transaction = null)
        {
            return InsertGrowerOrderLog(userGuid, userCode, processLookupCode, logTypeLookup, text, growerOrder.Guid, transaction);
        }

        public EventLog InsertGrowerOrderLog(Guid userGuid, string userCode, string processLookupCode, Lookup logTypeLookup, string text, Guid growerOrderGuid, SqlTransaction transaction = null)
        {
            return InsertAnyGrowerLog(userGuid, userCode, processLookupCode, logTypeLookup, text,
                                   LookupTableValues.Logging.ObjectType.GrowerOrder, growerOrderGuid, transaction);
        }

        public EventLog InsertSupplierOrderLog(Guid userGuid, string userCode, string processLookupCode, Lookup logTypeLookup, string text, SupplierOrder supplierOrder, SqlTransaction transaction = null)
        {
            return InsertAnyGrowerLog(userGuid, userCode, processLookupCode, logTypeLookup, text,
                                   LookupTableValues.Logging.ObjectType.SupplierOrder, supplierOrder.Guid, transaction);
        }

        public EventLog InsertSupplierOrderLog(Guid userGuid, string userCode, string processLookupCode, Lookup logTypeLookup, string text, Guid supplierOrderGuid, SqlTransaction transaction = null)
        {
            return InsertAnyGrowerLog(userGuid, userCode, processLookupCode, logTypeLookup, text,
                                   LookupTableValues.Logging.ObjectType.SupplierOrder, supplierOrderGuid, transaction);
        }

        public EventLog InsertOrderLineLog(Guid userGuid, string userCode, string processLookupCode, Lookup logTypeLookup, string text, OrderLine orderLine, SqlTransaction transaction = null)
        {
            return InsertOrderLineLog(userGuid, userCode, processLookupCode, logTypeLookup, text, orderLine.Guid, transaction);
        }

        public EventLog InsertOrderLineLog(Guid userGuid, string userCode, string processLookupCode, Lookup logTypeLookup, string text, Guid orderLineGuid, SqlTransaction transaction = null)
        {
            return InsertAnyGrowerLog(userGuid, userCode, processLookupCode, logTypeLookup, text, LookupTableValues.Logging.ObjectType.OrderLine, orderLineGuid, transaction);
        }

        private EventLog InsertAnyGrowerLog(Guid userGuid, string userCode, string processLookupCode, Lookup logTypeLookup, string text, Lookup objectTypeLookup, Guid objectGuid, SqlTransaction transaction = null)
        {
            if (!logTypeLookup.IsDescendantOf(LookupTableValues.Logging.LogType.Grower.LookupValue) &&
                !logTypeLookup.IsDescendantOf(LookupTableValues.Logging.LogType.LookupValue))//logTypeLookups.RowUpdate
            {
                throw new ArgumentException("The logTypeLookup is not a type within Logging/LogType/Grower.", "logTypeLookup");
            }

            return Insert
                (
                    userGuid,
                    userCode,
                    processLookupCode,
                    text,
                    logTypeLookup,
                    severityLookup: LookupTableValues.Logging.Severity.Info,
                    priorityLookup: LookupTableValues.Logging.Priority.Low, verbosityLookup: LookupTableValues.Logging.Verbosity.Terse, objectTypeLookup: objectTypeLookup, objectGuid: objectGuid);        
        }

        public EventLog Insert(Guid userGuid, string userCode, string processLookupCode, string comment, Lookup logTypeLookup, Lookup severityLookup = null, Lookup priorityLookup = null, Lookup verbosityLookup = null, Lookup objectTypeLookup = null, Guid? objectGuid = null, long? intValue = null, long? processId = 0, float? floatValue = null, Guid? guidValue = null, string xmlData = null, SqlTransaction transaction = null)
        {
            Lookup processLookup = GetOrCreateLookup( LookupTableValues.Logging.Process.LookupValue, processLookupCode, "processLookupCode");

            CheckLookupParameter("objectTypeLookup", objectTypeLookup, LookupTableValues.Logging.ObjectType.LookupValue);

            if (objectGuid != null && objectGuid != Guid.Empty && objectTypeLookup == null)
            {
                throw new ArgumentException("objectTypeLookup not specified for objectGuid.", "objectGuid");
            }

            CheckLookupParameter("verbosityLookup", verbosityLookup, LookupTableValues.Logging.Verbosity.LookupValue);
            CheckLookupParameter("priorityLookup", priorityLookup, LookupTableValues.Logging.Priority.LookupValue);
            CheckLookupParameter("severityLookup", severityLookup, LookupTableValues.Logging.Severity.LookupValue);

            var personService = new PersonTableService();
            var person = personService.TryGetByField("UserGuid", userGuid.ToString(), "=", false);
            Guid personGuid = person.Guid;

            var eventLog = new EventLog
                {
                    PersonGuid = personGuid,
                    UserCode = userCode,
                    EventTime = DateTime.Now.ToUniversalTime(),
                    ProcessLookupGuid = processLookup.Guid,
                    LogTypeLookupGuid = logTypeLookup.Guid,
                    VerbosityLookupGuid = verbosityLookup == null ? Guid.Empty : verbosityLookup.Guid,
                    PriorityLookupGuid = priorityLookup == null ? Guid.Empty : priorityLookup.Guid,
                    SeverityLookupGuid = severityLookup == null ? Guid.Empty : severityLookup.Guid,
                    IntValue = intValue,
                    FloatValue = floatValue,
                    GuidValue = guidValue
                };

            if (processId != null)
            {
                eventLog.ProcessId = (long)processId;                
            }

            if (objectTypeLookup != null)
            {
                eventLog.ObjectTypeLookupGuid = objectTypeLookup.Guid;
                eventLog.ObjectGuid = objectGuid;
            }

            eventLog.Comment = comment;

            eventLog.XmlData = xmlData;

            var eventLogTableService = new EventLogTableService();
            eventLog = eventLogTableService.Insert(eventLog, transaction);

            return eventLog;
        }

        //TODO: call this at some point.
        public static void SetLoggingUtilityDelegate()
        {
            Logger.SetLogDelegate(InsertDelegateForLoggingUtility);
        }

        private static void InsertDelegateForLoggingUtility
        (
            Guid userGuid,
            string userCode,
            string processCode,
            string comment,
            Logger.LogType logType,
            Logger.Severity severity,
            Logger.Priority priority,
            Logger.Verbosity verbosity,
            string objectTypeCode,
            Guid? objectGuid = null,
            long? intValue = null,
            long? processId = 0,
            float? floatValue = null,
            Guid? guidValue = null,
            string xmlData = null
        )
        {
            var lookupTableService = new LookupTableService();

            var logTypeLookup = lookupTableService.GetByCode(LookupTableValues.Logging.LogType.LookupValue, logType.ToString());
            var severityLookup = lookupTableService.GetByCode(LookupTableValues.Logging.Severity.LookupValue, severity.ToString());
            var priorityLookup = lookupTableService.GetByCode(LookupTableValues.Logging.Priority.LookupValue, priority.ToString());
            var verbosityLookup = lookupTableService.GetByCode(LookupTableValues.Logging.Verbosity.LookupValue, verbosity.ToString());

            Lookup objectTypeLookup = null;
            if (!string.IsNullOrEmpty(objectTypeCode))
            {
                objectTypeLookup = 
                    GetOrCreateLookup
                    (
                        LookupTableValues.Logging.ObjectType.LookupValue, 
                        objectTypeCode,
                        "objectTypeCode"
                    );
            }

            var eventLogTableService = new EventLogTableService();
            eventLogTableService.Insert(userGuid, userCode, processCode, comment, logTypeLookup, severityLookup, priorityLookup, verbosityLookup, objectTypeLookup, objectGuid, intValue, processId, floatValue, guidValue, xmlData);
        }

        private static Lookup GetOrCreateLookup(Lookup parentLookup, string lookupCode, string parameterName)
        {
            Lookup lookup;

            if (string.IsNullOrEmpty(lookupCode))
            {
                throw new ArgumentNullException(parameterName);
            }
            else
            {
                var lookupTableService = new LookupTableService();
                lookup = lookupTableService.TryGetByCode(parentLookup.Path, lookupCode);

                if (lookup == null)
                {
                    lookup = new Lookup
                    {
                        Guid = Guid.NewGuid(),
                        ParentLookupGuid = parentLookup.Guid,
                        Code = lookupCode,
                        Name = lookupCode
                    };

                    lookup = lookupTableService.Insert(lookup);
                }
            }

            return lookup;
        }

        private void CheckLookupParameter(string lookupName, Lookup lookup, Lookup parentLookup)
        {
            if (lookup != null && !lookup.IsDescendantOf(parentLookup))
            {
                throw new ArgumentException("Parameter is not the right type of lookup.", lookupName);
            }            
        }

        new public EventLog Insert(EventLog eventLog, SqlTransaction transaction = null)
        {
            var lookupTableService = new LookupTableService();

            if (eventLog.LogTypeLookupGuid == Guid.Empty)
            {
                throw new ApplicationException("LogTypeLookup is required.");
            }

            eventLog.LogTypeLookup = lookupTableService.GetByGuid(eventLog.LogTypeLookupGuid);
            if (!eventLog.LogTypeLookup.IsDescendantOf(LookupTableValues.Logging.LogType.LookupValue))
            {
                throw new ApplicationException("logTypeLookup is not valid.");
            }

            eventLog.VerbosityLookup = lookupTableService.GetByGuid(eventLog.VerbosityLookupGuid);
            if (eventLog.VerbosityLookupGuid == Guid.Empty)
            {
                eventLog.VerbosityLookup = LookupTableValues.Logging.Verbosity.Normal;
                eventLog.VerbosityLookupGuid = eventLog.VerbosityLookup.Guid;
            }

            if (!eventLog.VerbosityLookup.IsDescendantOf(LookupTableValues.Logging.Verbosity.LookupValue))
            {
                throw new ApplicationException("VerbosityLookup is not valid.");
            }

            eventLog.PriorityLookup = lookupTableService.GetByGuid(eventLog.PriorityLookupGuid);
            if (eventLog.PriorityLookupGuid == Guid.Empty)
            {
                eventLog.PriorityLookup = LookupTableValues.Logging.Priority.Low;
                eventLog.PriorityLookupGuid = eventLog.PriorityLookup.Guid;
            }

            if (!eventLog.PriorityLookup.IsDescendantOf(LookupTableValues.Logging.Priority.LookupValue))
            {
                throw new ApplicationException("PriorityLookup is invalid.");
            }

            eventLog.SeverityLookup = lookupTableService.GetByGuid(eventLog.SeverityLookupGuid);
            if (eventLog.SeverityLookupGuid == Guid.Empty)
            {
                eventLog.SeverityLookup = LookupTableValues.Logging.Severity.Info;
                eventLog.SeverityLookupGuid = eventLog.SeverityLookup.Guid;
            }

            if (!eventLog.SeverityLookup.IsDescendantOf(LookupTableValues.Logging.Severity.LookupValue))
            {
                throw new ArgumentException("SeverityLookup is invalid.");
            }

            return base.Insert(eventLog, transaction);
        }
    }
}

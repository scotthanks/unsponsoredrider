﻿	
using System;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public class LookupTableValues
    {
        public static CategoryValues Category { get { return new CategoryValues();}}

        public class CategoryValues
        {
            public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("47b2a77e-4478-41a4-aa25-4ce2917265bd")); }}
            public LoggingValues Logging { get { return new LoggingValues();}}

            public class LoggingValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d091f888-f98d-4557-80af-259d6e7a9ae5")); }}
                public Lookup KeepInDatabase { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b9286d81-28fe-4e5e-b931-39a64fe55774")); }}
            }

        }

        public static CodeValues Code { get { return new CodeValues();}}

        public class CodeValues
        {
            public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1b0671c8-5cb6-4dda-844d-e7939d1f15ff")); }}
            public AvailabilityTypeValues AvailabilityType { get { return new AvailabilityTypeValues();}}

            public class AvailabilityTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("983fa04a-f6b0-49a3-9091-bb56f3cab3d4")); }}
                public Lookup Avail /* AVAIL */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e2061eb9-264c-43ef-a1c2-e2f142425102")); }}
                public Lookup Na /* NA */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("95d3adaa-6143-4ac0-919f-d9fe8d5269d4")); }}
                public Lookup Open /* OPEN */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("38f101e0-3bff-4479-99ec-af410205639f")); }}
            }

            public AvailabilityWeekCalcMethodValues AvailabilityWeekCalcMethod { get { return new AvailabilityWeekCalcMethodValues();}}

            public class AvailabilityWeekCalcMethodValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4f59b9e3-3f6a-4029-bc11-f11b438701fe")); }}
                public Lookup CurrentWeek { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6cd378c7-f5ac-4806-b75a-03183dd636ac")); }}
                public Lookup CurrentWeekPlus1 { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4563963a-903e-4144-945a-8070dea08a70")); }}
                public Lookup CurrentWeekPlus3 { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("22f2cfba-5e41-4293-8fd4-4be42a4b3bd5")); }}
                public Lookup Poipreweek39 /* POIPREWeek39 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("48074f93-2f2e-47c6-9aa0-09b0c297d75b")); }}
                public Lookup Week11Thru17Odd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c4654e48-a6e0-4b7d-88ec-f3b41709470d")); }}
            }

            public ChangeTypeValues ChangeType { get { return new ChangeTypeValues();}}

            public class ChangeTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f03a6fce-7456-4640-ac55-19b3a4dd287c")); }}
                public Lookup GrowerCancellation { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d5f4d614-105d-479c-b421-be485d8f8b70")); }}
                public Lookup GrowerQtyChange { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("50a6836f-bd1e-43dd-addf-5ef0f4f08ad5")); }}
                public Lookup NoChange { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("dbad2e8d-b056-4ce8-b56d-94dc7e6b4769")); }}
                public Lookup SupplierQtyChange { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("38ed58ef-6939-48c8-a8a4-bd75f8ea2144")); }}
                public Lookup SupplierRejected { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d38c5be5-49b8-45b6-bb34-09a6906bd30a")); }}
                public Lookup SupplierSubstitute { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("88cfb7a1-38ce-4611-b9f0-65d5764e223c")); }}
            }

            public ClaimReasonValues ClaimReason { get { return new ClaimReasonValues();}}

            public class ClaimReasonValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1214551c-d0a9-4869-8274-33f9fc465cec")); }}
                public Lookup Disease { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9fc63b1b-6d94-4a4b-860a-ac934a3f608e")); }}
                public Lookup Frozen { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("622a4488-27ab-40c8-ab67-88055c6ceb17")); }}
                public Lookup Insects { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ebb0892b-eba1-413a-aa94-54a1c92aa153")); }}
                public Lookup MissingBoxes { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("76158152-a791-4e16-98db-f99291971dd6")); }}
                public Lookup Other { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e9846c90-c057-4910-9026-c0b79e4e0588")); }}
                public Lookup OverHeated { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8dac6014-8768-41a5-b77e-c78c4f84c423")); }}
                public Lookup ShippingDamage { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("eda9bd1e-a3fe-4c9b-9a4f-65ae3e73e26d")); }}
                public Lookup Shortage { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6a109423-e8bf-4f8b-af8b-bfe9416958f8")); }}
                public Lookup Stretched { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("58973a74-cbd9-4c99-bac3-730beea60604")); }}
                public Lookup TooSmall { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("caf52d5d-45d6-4844-8166-ad05aa238dd8")); }}
                public Lookup WrongProduct { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("956a7998-da1e-456e-b6e2-fe72e96e9c43")); }}
            }

            public ClaimStatusValues ClaimStatus { get { return new ClaimStatusValues();}}

            public class ClaimStatusValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("66788714-f32d-4db2-802f-330e17bc3885")); }}
                public Lookup Approved { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f5a7929a-d02d-4582-bfe9-fea887db0607")); }}
                public Lookup PartiallyApproved { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2790b97d-a65c-4b52-b404-2efd02b4db67")); }}
                public Lookup Rejected { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e9585d43-dbb8-46a4-b835-8e6f7656cf48")); }}
                public Lookup Submitted { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3918327e-b5d7-45b5-a735-3f3e12846a0e")); }}
            }

            public ColorValues Color { get { return new ColorValues();}}

            public class ColorValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("016ff5ab-b703-453f-bff8-b94f77153705")); }}
                public Lookup Black { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9604e154-4df0-4d3e-ae9f-f814bcacbd63")); }}
                public Lookup Blue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("cc42cda3-ec80-4a88-af25-74cac0c77e63")); }}
                public Lookup Combination { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2909c87e-64d9-47e7-876a-9b7f7327c7c4")); }}
                public Lookup Green { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("91c373f5-d569-4537-81e8-58c8129fa073")); }}
                public Lookup Lavender { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f31252bf-e293-4e6a-9762-9215573dfd52")); }}
                public Lookup Mix { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d562d2ef-2ab5-4b27-b46e-7d7ae7c0a58f")); }}
                public Lookup None { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("146fcd97-bdd5-4fd8-a4aa-49d446a6dd95")); }}
                public Lookup Orange { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ca32827a-2f55-49cd-800c-e226a5a88b7f")); }}
                public Lookup Other { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("cfc61a97-b313-426c-bc8a-b39dbcbf8f8f")); }}
                public Lookup Pink { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("11f486e5-30e9-416d-a86c-285a7b87259b")); }}
                public Lookup Purple { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("334f3ffc-26a6-4e23-9687-36df63a7c35d")); }}
                public Lookup Red { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b01ec3d0-96cf-4d9e-baae-e6fdb1e629b5")); }}
                public Lookup Rose { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b9073077-c57e-4b03-9bed-6a9d0167e1b5")); }}
                public Lookup Salmon { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("dca865f9-a499-4574-8596-79c910e17b85")); }}
                public Lookup White { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b1a1bfcd-cba6-4f9c-af57-82674bffa6c3")); }}
                public Lookup Yellow { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("48f7fd41-fbb3-43e3-858c-ca1df57793b2")); }}
            }

            public CreditAppStatusValues CreditAppStatus { get { return new CreditAppStatusValues();}}

            public class CreditAppStatusValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a03a3118-37fc-40f0-8ced-f5ebffcdef15")); }}
                public Lookup Approved { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d739b901-bb75-4c54-9ad7-c8e2bfbb1b09")); }}
                public Lookup None { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d7bf5dc3-c447-4fca-adbc-acc47a2acb27")); }}
                public Lookup Pending { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("91f8c078-86ac-4882-8034-743173be5731")); }}
                public Lookup Rejected { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e6bacaa0-3f34-48af-85c7-fa28a758959b")); }}
            }

            public FileExtensionValues FileExtension { get { return new FileExtensionValues();}}

            public class FileExtensionValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2e48cbf2-f7b4-45b8-9342-90171e220dc4")); }}
                public Lookup Jpg /* JPG */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("05f95eeb-5f24-41b2-aa9f-b0639b0a1d07")); }}
                public Lookup Png /* PNG */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f45e1e89-038b-438f-b56f-2305c2b7a620")); }}
            }

            public GeneralLedgerAccountValues GeneralLedgerAccount { get { return new GeneralLedgerAccountValues();}}

            public class GeneralLedgerAccountValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("acf30bd0-2567-4c95-a26f-bb344131af7f")); }}
                public Lookup AccountsPayable { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("fe952852-454e-4657-82e7-2e292d2b3c02")); }}
                public Lookup AccountsRecievable { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d7739b18-1906-47b2-9fd8-8ffac63a8b9a")); }}
                public Lookup Cash { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e09d31c1-8183-4a04-9823-055e28384945")); }}
                public Lookup GrowerCredits { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("363b0b70-e6a4-485d-8a8b-184b1d3706cd")); }}
                public Lookup Sales { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("166bb7bb-628a-4dca-889e-cdbe9e97dcb5")); }}
                public Lookup SupplierCredits { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("740feaa0-1c4f-432c-97e2-4440ef87557a")); }}
                public Lookup SupplierExpense { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("42381453-da8b-40bb-8097-3174c7ef172d")); }}
                public Lookup UnappliedCreditCardPayments { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6b346b3c-1689-4a8f-9dbb-07f1ed465999")); }}
                public Lookup UnappliedCredits { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ae36c771-b378-49c6-a6ca-5b871c6efbc7")); }}
                public Lookup UnappliedEpsPayments { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("06802d8d-9777-44d7-9466-c3f64a3c9ad9")); }}
                public Lookup UnappliedPayments { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("aabd1a98-1a26-4ed1-a9df-cc2612919a5b")); }}
            }

            public GeneralLedgerEntryTypeValues GeneralLedgerEntryType { get { return new GeneralLedgerEntryTypeValues();}}

            public class GeneralLedgerEntryTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0872d40b-d768-43bf-a558-411757df189d")); }}
                public Lookup Credit { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9b7335b3-4d1e-4ea7-99f6-d3266d18d536")); }}
                public Lookup Debit { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("07d9c62f-73d5-4905-91bc-662e0269a217")); }}
            }

            public GeneralLedgerTransactionTypeValues GeneralLedgerTransactionType { get { return new GeneralLedgerTransactionTypeValues();}}

            public class GeneralLedgerTransactionTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ea958b1a-9e42-4fd3-864a-214510bbcd39")); }}
                public Lookup GrowerAppliedPayment { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a802e566-881d-44ca-8f53-44daff799a9d")); }}
                public Lookup GrowerCheckPayment { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c2bf1ca9-f8a0-4686-a9c5-7b0621d102ce")); }}
                public Lookup GrowerCredit { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e2f9bec5-c8bf-42c9-81f8-80e2fef02035")); }}
                public Lookup GrowerCreditCardPayment { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("277a0e31-0817-4a08-b1b7-bb5b36da3f76")); }}
                public Lookup GrowerDebit { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1bd61048-40c7-4da8-a2e6-0ecb3cd151f8")); }}
                public Lookup GrowerInvoice { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2ecd8df4-c07a-43d4-b838-b14efeb5ea2c")); }}
                public Lookup SupplierCredit { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1f943033-0d86-46ca-8cf5-8651d4098807")); }}
                public Lookup SupplierInvoice { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("af954afc-386b-4734-8eb3-eddce0286988")); }}
                public Lookup SupplierPayment { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("eba4feb2-b737-41bc-8a70-8350af06f259")); }}
            }

            public GrowerOrderFeeStatusValues GrowerOrderFeeStatus { get { return new GrowerOrderFeeStatusValues();}}

            public class GrowerOrderFeeStatusValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8f3ccec8-088d-45a5-beb5-9f07f091af35")); }}
                public Lookup Actual { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("602ea0e0-5f85-453e-89cb-addf3d0ec858")); }}
                public Lookup Estimated { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("61017c91-f624-4838-9aa5-9f48bf48be44")); }}
                public Lookup Included { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5211dcaf-83b4-4ccc-b2b0-85a508808414")); }}
                public Lookup Na /* NA */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0673f981-49c5-4723-8f3a-5c0c6c0538f9")); }}
                public Lookup Tbd /* TBD */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("dd6f683c-ec61-4d9e-88dc-56e3096cbebc")); }}
            }

            public GrowerOrderFeeTypeValues GrowerOrderFeeType { get { return new GrowerOrderFeeTypeValues();}}

            public class GrowerOrderFeeTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f48c5eb4-b851-44c4-a525-6a31d72b42f2")); }}
                public Lookup Carrying { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7a951c9c-5993-41c0-9877-a624b90f8493")); }}
                public Lookup CyberMonday3_5 { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("12a472e4-b355-4c24-912a-72d71a85510f")); }}
                public Lookup CyberMonday4 { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5c1c3ff0-d3b3-4073-b9fd-28106abc0ddc")); }}
                public Lookup Epsdiscount /* EPS Discount */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7d9d0263-854c-40c4-91fe-d13ec065632c")); }}
                public Lookup FirstOrderDiscount { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5c4e784c-ab3d-407a-af40-4c043b2223bc")); }}
                public Lookup Hmadiscount /* HMA Discount */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f079e816-4990-4240-99e2-206420dafbdf")); }}
                public Lookup InnovatorClubDiscount { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("62283fcf-924b-41dd-ac9c-d878e58e3e53")); }}
                public Lookup LoyalCustomerDiscount { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b98f76bf-2ca1-4cf0-96f2-412db34b447e")); }}
                public Lookup MumDiscount { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("88a6e6b4-a3b0-47b7-afa4-61c386381661")); }}
                public Lookup NewCustomerDiscount { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3633629f-2f03-41d6-ae28-1713e275a5e5")); }}
                public Lookup Psispecial /* PSI Special */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("dc39a7fa-8c1a-49da-bab4-98e2aeda7832")); }}
                public Lookup Permit { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("475ca61a-09bb-4f8b-b898-9bc1d86d46a0")); }}
                public Lookup PoinsettiaDiscount { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6ac47da0-16dd-430b-96fc-964035ab202a")); }}
                public Lookup WeeklyDeal { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e89a4d32-6570-477f-adc7-c3076fca5836")); }}
            }

            public GrowerOrderStatusValues GrowerOrderStatus { get { return new GrowerOrderStatusValues();}}

            public class GrowerOrderStatusValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b918a0b0-b15b-442a-9df6-5250184297a0")); }}
                public Lookup AwaitingCreditApproval { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("75b575db-3d9d-4b47-b0a6-46d63c21d86b")); }}
                public Lookup Cancelled { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e5ed3613-466d-4cb2-833c-581ff1cc8bf8")); }}
                public Lookup CreditApproved { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7dda2946-c7b5-4a81-bd62-7e5ab2a898d3")); }}
                public Lookup Invoiced { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a00ff27a-798c-41f3-b930-a4fd4f82725c")); }}
                public Lookup Paid { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e5d18a23-076a-4936-99ac-eb533691671b")); }}
            }

            public GrowerSizeValues GrowerSize { get { return new GrowerSizeValues();}}

            public class GrowerSizeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1b34a9ff-259b-43b0-b9eb-7c2aa6a3fb1a")); }}
                public Lookup Large { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ea016f01-4c2f-4691-84a7-8545bb9b9cbc")); }}
                public Lookup RootAndSell { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5f891bfd-8c53-4608-ad8d-0c320711b25e")); }}
                public Lookup Standard { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7e09ebf5-bb19-47d9-8ceb-86f459ace9cd")); }}
            }

            public GrowerTypeValues GrowerType { get { return new GrowerTypeValues();}}

            public class GrowerTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("26f2e1f2-83f4-4dd3-b06c-e5c1f0259dcd")); }}
                public Lookup Breeder { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2eb5e2bf-f2f0-4c61-9355-1e79224a1812")); }}
                public Lookup Consumer { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("dd862751-57f1-44b6-bb54-fa0bf8dfc88e")); }}
                public Lookup Other { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f59673c0-fd9f-4f7f-8797-bbd3887bf49f")); }}
                public Lookup ProGh /* ProGH */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("307936fc-c0f6-4d61-8ca5-c8874011a5d9")); }}
                public Lookup Retail { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("68be6c83-3e72-4eec-8eb3-f9c1d2b638d3")); }}
                public Lookup Supplier { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d51433f3-9da2-44b5-b1c8-58db1a762c1d")); }}
                public Lookup Trade { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3e25bc72-3f00-4897-847d-51434c763290")); }}
            }

            public HabitValues Habit { get { return new HabitValues();}}

            public class HabitValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b676a16f-dc5a-4415-be21-777f19678b01")); }}
                public Lookup Compact { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("417b30c7-df1c-40b4-982d-708b799b6311")); }}
                public Lookup Mounding { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("158ee4d9-0606-4156-8c2f-57257d716c9b")); }}
                public Lookup SemiToTrailing /* Semi-Trailing */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("56c217e3-0077-459f-8e18-f8d91f499122")); }}
                public Lookup Spreading { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1702406d-088e-4070-b072-650cb8a8be4f")); }}
                public Lookup Trailing { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b93dfb41-060b-45f0-8f73-ed996ca85bd6")); }}
                public Lookup Unk /* UNK */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d0cc6773-98f8-41f2-8b01-a0029537b284")); }}
                public Lookup Upright { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3d6a0644-eb62-4ff7-95ae-12dc52401f9b")); }}
            }

            public ImageFileTypeValues ImageFileType { get { return new ImageFileTypeValues();}}

            public class ImageFileTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9b5c4b77-97f0-4082-8d34-08af66986d79")); }}
                public Lookup Full { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d8d3681e-0101-4bb4-b5c1-0ed1165f1176")); }}
                public Lookup Original { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7dbf1e08-de2d-40b2-b700-d996d957bd75")); }}
                public Lookup Small { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("72493ca5-1e25-437c-a5cc-4ff90e75b53c")); }}
                public Lookup Thumbnail { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("29a4b7e9-716e-4762-b702-20af387885d7")); }}
            }

            public ImageSetTypeValues ImageSetType { get { return new ImageSetTypeValues();}}

            public class ImageSetTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5a2382b3-8d62-4e8b-823a-a1d7239da717")); }}
                public Lookup ProgramTypeImage { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("11ea5a93-2f53-43ba-8e34-6bc56ce038db")); }}
                public Lookup VarietyImage { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9938abd8-65d3-4db4-9858-50490e9201bf")); }}
            }

            public MimeTypeValues MimeType { get { return new MimeTypeValues();}}

            public class MimeTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("34a71f8a-b83d-41c2-abb9-0a9633624449")); }}
                public Lookup Pdf /* PDF */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("06e1cae5-dbdb-4e70-8eb8-5caee776369c")); }}
            }

            public OrderLineStatusValues OrderLineStatus { get { return new OrderLineStatusValues();}}

            public class OrderLineStatusValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("551b7728-fbee-4a50-a23a-47e0e0915cf1")); }}
                public Lookup Cancelled { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7e6a8e0e-a933-4469-a723-419df68e3310")); }}
                public Lookup GrowerNotifiedAfterSupplierConfirmed { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c8210e43-9cdd-4f8a-bae4-dac4dda70d05")); }}
                public Lookup GrowerCancelled { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("db6bf38e-1aea-47c0-9f26-cb1ece810997")); }}
                public Lookup GrowerEdit { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a1924f7d-98e5-485f-8651-fde4c544c579")); }}
                public Lookup GrowerNotified { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d9f220ee-df06-4010-ab94-9405c197c66a")); }}
                public Lookup NotUsed { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("304742e0-9739-4bae-a55b-11558889dfd5")); }}
                public Lookup Ordered { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f6b2dcd7-f10b-45d5-b047-ca917e9814a6")); }}
                public Lookup Pending { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f268924e-c461-4bda-88e1-bc1f2f882c85")); }}
                public Lookup PreCart { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6dc504df-1418-4030-a787-1f6b2336abfe")); }}
                public Lookup Shipped { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1f7dd5b3-dda9-4861-85d8-a80f2e8dca38")); }}
                public Lookup SupplierCancelled { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("16810f92-6925-49ae-aff2-26416684575c")); }}
                public Lookup SupplierConfirmed { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("32b4e4c4-38f0-4835-be68-abe247958fe8")); }}
                public Lookup SupplierEdit { get { return LookupTableService.SingletonInstance.GetByGuid(new Guid("604af270-df03-4dc7-9ba2-e5a8e1d60d41")); } }
                public Lookup SupplierAdd { get { return LookupTableService.SingletonInstance.GetByGuid(new Guid("B9A99AE4-2794-42E1-A60C-9A0C5DF39AB6")); } }
                public Lookup SupplierNotified { get { return LookupTableService.SingletonInstance.GetByGuid(new Guid("93e1dc5f-672f-4e74-850c-741de9b964f2")); } }
            }

            public OrderLineTrxStatusValues OrderLineTrxStatus { get { return new OrderLineTrxStatusValues();}}

            public class OrderLineTrxStatusValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c193e904-7009-433a-a59a-e5fba8153a61")); }}
                public Lookup AdjustAvail { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9e005694-e1a0-4f61-8527-c5bd1c9529d3")); }}
                public Lookup AdjustMin { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("394e9c9a-7b18-420f-9ef0-a218d0873b22")); }}
                public Lookup AdjustMult { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c887c047-8fcb-430e-98ce-05dfdc993def")); }}
                public Lookup Cancelled { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e1544ad8-a84d-4615-b403-1e9efbc5070f")); }}
                public Lookup Deleted { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("bc59bd0d-2663-44e9-b551-9e8b948e4dd8")); }}
                public Lookup Failed { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1894673e-e7c4-4327-b77f-915944be607f")); }}
                public Lookup NoChange { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("22b3497a-2630-482d-83e5-6cc426b18d13")); }}
                public Lookup Requested { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("90304b0c-aa4a-4def-81b1-64e9c2a491ed")); }}
                public Lookup Success { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8c02db62-c3f3-4928-8c29-e155c858e9fc")); }}
                public Lookup Unavailable { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ac969265-f3e2-492d-a676-ee2450158973")); }}
                public Lookup Updated { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1067eae2-0a52-420f-976e-46c98f71929c")); }}
                public Lookup ZeroQtyAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f0b6da43-054c-4f80-b892-0e62c296eda2")); }}
            }

            public OrderTransitionTypeValues OrderTransitionType { get { return new OrderTransitionTypeValues();}}

            public class OrderTransitionTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b09b925e-074f-4167-86f1-aa4309301911")); }}
                public Lookup AddToCart { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("40eebd3c-ffe1-4ff0-8451-594442002ce9")); }}
                public Lookup Cancelled { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8cb61c6f-c7ce-4a5c-a089-816dc1aa8984")); }}
                public Lookup GrowerNotified { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("43b83b1f-4f7b-4e05-9e2b-a7d4bcb3fb12")); }}
                public Lookup GrowerNotifiedAfterSupplierConfirmed { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2f26cd89-94cd-4d15-903a-b1e33e5fed13")); }}
                public Lookup Invoiced { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("57f9dd2f-8a9e-4728-af00-0dc880a44375")); }}
                public Lookup Paid { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e24cf127-10f1-48a3-bc61-3d421a2394c8")); }}
                public Lookup PlaceOrder { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("86e6cb61-9558-4a4f-bb98-d01e34a21689")); }}
                public Lookup Shipped { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c0cf5d10-75eb-4985-b8c8-f1380660bfa4")); }}
                public Lookup SupplierConfirmed { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ad658aa5-7702-4480-8cb9-42c5ec51664f")); }}
                public Lookup SupplierNotified { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("412c0ba4-cb16-4727-88e4-d928bc9d9ecf")); }}
            }

            public OrderTrxStatusValues OrderTrxStatus { get { return new OrderTrxStatusValues();}}

            public class OrderTrxStatusValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("814e164f-e7f5-4d47-a933-bd8acf8a5077")); }}
                public Lookup Failed { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("39d0f579-3e81-4c85-ba94-55d4251c3c15")); }}
                public Lookup NoChange { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("848cd8c9-ecab-444e-a256-82728b5e908c")); }}
                public Lookup OrderMinNotMet { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("144df892-b783-43b4-b09b-8b9b75cb155b")); }}
                public Lookup Requested { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("700f3a18-aea8-4e97-bca7-a673f87d8060")); }}
                public Lookup Success { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a88f822f-64a0-492e-a0e0-74bc2f9f1ffa")); }}
                public Lookup Updated { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("33535840-96d8-4eaf-b374-25bae89334c7")); }}
            }

            public OrderTypeValues OrderType { get { return new OrderTypeValues();}}

            public class OrderTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ce42c87f-7e5f-4c33-9ec2-17ccac8860d7")); }}
                public Lookup CreditMemo { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9195968b-83ca-41cc-a55d-27736a82b5c2")); }}
                public Lookup DebitMemo { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6189911f-a5aa-4431-bcbc-e453b0ea5f32")); }}
                public Lookup Invoice { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("215cd299-8ebc-458e-ad2b-f35fbba1e779")); }}
                public Lookup Order { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6f844a05-80f8-4057-af46-4cb8af9ead55")); }}
            }

            public PaymentTypeValues PaymentType { get { return new PaymentTypeValues();}}

            public class PaymentTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5c053092-77ea-4bdd-ad40-1be5edf4e330")); }}
                public Lookup Credit { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e1e73b39-e4f0-488d-ac93-87afe75025a7")); }}
                public Lookup Invoice { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("996ac2a7-9225-4e7a-8d61-5ccaf6c70ad0")); }}
            }

            public PermissionsValues Permissions { get { return new PermissionsValues();}}

            public class PermissionsValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f19cb4f4-7ebc-45aa-9369-c3a3ac83624b")); }}
                public Lookup PlaceOrders { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("48225cbb-293c-46c6-b5c7-e610a6f18335")); }}
                public Lookup SeePrices { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2200a84d-4aec-4b2e-915e-175393a2afa7")); }}
            }

            public PersonTypeValues PersonType { get { return new PersonTypeValues();}}

            public class PersonTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("822316ed-1398-47dd-b6f5-a588d56435eb")); }}
                public Lookup Agent { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5ca98961-17c1-4a4b-b21a-b68894212e32")); }}
                public Lookup Grower { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4c3fd4a1-dadc-49dc-aeab-d3cbded6117e")); }}
                public Lookup Other { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2fd76a54-4ed8-4ef9-a63b-bbeb05d70e1e")); }}
                public Lookup Owner { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("85c6c752-ed22-49a7-9671-066baa8fe7d6")); }}
            }

            public RolesValues Roles { get { return new RolesValues();}}

            public class RolesValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("aac616e6-0ce3-4a83-b5eb-0d9afc7f65e2")); }}
                public Lookup Administrator { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6e17236a-ef85-4b07-833c-2617016b1ef9")); }}
                public Lookup CustomerAdministrator { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("970d8323-f2e3-4a9c-8518-4c78260076eb")); }}
                public Lookup CustomerOrderer { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b8d4c3b6-2c58-4d54-801b-496449872f06")); }}
                public Lookup CustomerReadOnly { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6b5f4a32-40e2-41a0-bab5-b4402ea81b0d")); }}
                public Lookup CustomerReadOnlyNoPrice { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("70cf2835-400d-4f9c-a9e1-4a4d1022827d")); }}
            }

            public ShipMethodValues ShipMethod { get { return new ShipMethodValues();}}

            public class ShipMethodValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9f96afe4-fa3a-4ee4-b002-1fbdb3a371bb")); }}
                public Lookup Air /* AIR */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8a8ecaee-89dd-4194-b682-60e06dd498e0")); }}
                public Lookup Cpu /* CPU */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b60100da-04dc-41b0-810c-75e5c4faa860")); }}
                public Lookup Fedex /* FEDEX */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7aca9615-5fa4-4c73-b66b-df6a86daed46")); }}
                public Lookup Truck /* TRUCK */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("798867b6-af24-445b-8d52-07f0b32e1cec")); }}
                public Lookup Ups2ndDayAir /* UPS 2nd Day Air */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4a524d23-e4c9-4605-bb72-6b9a9b9ef871")); }}
                public Lookup Upsground /* UPS Ground */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f2b7b5df-27d0-4713-beae-7628941e7609")); }}
            }

            public SupplierOrderFeeStatusValues SupplierOrderFeeStatus { get { return new SupplierOrderFeeStatusValues();}}

            public class SupplierOrderFeeStatusValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5ddd76a3-9dca-414d-87ba-63da224653d4")); }}
                public Lookup Actual { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e20f9e3c-975e-483f-bb10-61f1676a523d")); }}
                public Lookup Estimated { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("35358a39-8bfd-4937-b7ec-9bff75c51746")); }}
                public Lookup Included { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5126bb10-6091-4227-9f80-016256b7eb45")); }}
                public Lookup Na /* NA */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0276b128-e924-4ce3-aaff-111a6e4f9747")); }}
                public Lookup Tbd /* TBD */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ae5c6b43-0f9d-44a4-8042-432107579587")); }}
            }

            public SupplierOrderFeeTypeValues SupplierOrderFeeType { get { return new SupplierOrderFeeTypeValues();}}

            public class SupplierOrderFeeTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("caee4f0e-4dc9-4796-b04d-188ed597673b")); }}
                public Lookup BelowMinFee { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a82b4326-9ec6-4959-bc8a-ba170ffb6ddc")); }}
                public Lookup Boxing { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f232ac51-2373-4362-b8de-280b3f900446")); }}
                public Lookup ClearanceHandling { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("46254581-401c-4db5-8948-c9d0475f8d75")); }}
                public Lookup Drop { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5d580c6f-cb8c-4b0e-a49a-63d5bd7bddae")); }}
                public Lookup EnergySurcharge { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c3a41163-cd6a-41fa-b061-b8b32b9a2ddf")); }}
                public Lookup Freight { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("585a76e3-d7a5-46f8-a521-43572b34f542")); }}
                public Lookup FreightSurcharge { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1f1d3074-45e7-42a1-9734-4d12f03ce776")); }}
                public Lookup FuelSurcharge { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b5ee8285-aabe-48af-b6a5-4071a78f1074")); }}
                public Lookup LargeOrderDiscount /* Large Order Discount */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("494251ca-8c45-4c68-8ff5-eb830757697b")); }}
                public Lookup NewCustomerDiscount { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("bbf1959f-930f-4aa9-b307-502728ad9bb3")); }}
                public Lookup PartialBoxFee { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3b844110-82a9-40e7-a733-c56c5cdd69f1")); }}
                public Lookup Phyto { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("41806a66-2ee3-429a-ab1e-427b2a558472")); }}
                public Lookup Product { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1054ef31-e09f-4c80-850f-cdb3deddece3")); }}
                public Lookup RestockingFee { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8d7fbdbe-f677-4805-91f7-46e0639db7f4")); }}
                public Lookup Royalties { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7730dcd1-fb94-433a-b220-ab4ac70ad83a")); }}
                public Lookup Tags { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("918a3fc3-371a-4eee-a117-c61a06bc06bc")); }}
                public Lookup Vat /* VAT */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("63601add-d374-4782-8ecc-f95152a4d990")); }}
                public Lookup InnovatorClubDiscount { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("52201a77-96bf-454f-a0c2-a917259e4dd1")); }}
                public Lookup WinterBoxFee { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("56a87bb0-a4be-4bbe-8bfa-592be7eb06ad")); }}
            }

            public SupplierOrderStatusValues SupplierOrderStatus { get { return new SupplierOrderStatusValues();}}

            public class SupplierOrderStatusValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("37cefab2-63d4-466e-8c5d-63df2a1ad607")); }}
                public Lookup Cancelled { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("390a7cd7-b352-474c-a614-1687702a81c6")); }}
                public Lookup NotShipped { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ffb383ec-4ce9-4c50-87a4-234e07d15180")); }}
                public Lookup Shipped { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4bd446c9-700d-4eb4-9913-2048f8a03245")); }}
            }

            public TagRatioValues TagRatio { get { return new TagRatioValues();}}

            public class TagRatioValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("572cc70a-0c56-4092-9a55-8d23c0441b6c")); }}
                public Lookup Code10To1 /* 10To1 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("df06337e-dba6-456f-94bd-713bddea6d7e")); }}
                public Lookup Code1To1 /* 1To1 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5ebfd1ec-b98d-4ad9-b09f-8522754b5ac6")); }}
                public Lookup Code2To1 /* 2To1 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d018badc-1eca-41db-8f66-1cc94344c0e5")); }}
                public Lookup Code3To1 /* 3To1 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("785f3e97-f98c-4cd4-a21f-6b154cddb7a1")); }}
                public Lookup Code4To1 /* 4To1 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("def6c3e5-6601-4ba9-be50-da9542d0d28f")); }}
                public Lookup Code5To1 /* 5To1 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("215a8769-b5a5-4cde-a0c2-482fdb64873a")); }}
                public Lookup Code6To1 /* 6To1 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("694c38af-b134-4c87-b7a3-00e0b2fe6058")); }}
                public Lookup Code7To1 /* 7To1 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6faa1285-b726-4c5e-b8e3-25beab88c98d")); }}
                public Lookup Code8To1 /* 8To1 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("93347187-4cea-478f-a7d9-368eba86ff2a")); }}
                public Lookup Code9To1 /* 9To1 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d56046d8-b97d-4087-85d4-74bf99b3a5b0")); }}
                public Lookup NoTag { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("67317173-2c64-4a44-b48d-31c1718c33e2")); }}
                public Lookup TagInc { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3e5d35a1-eaea-48d0-ae4d-96b394519d89")); }}
            }

            public TimingValues Timing { get { return new TimingValues();}}

            public class TimingValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c99ea2e0-fed9-439c-a612-cbe04136fd7d")); }}
                public Lookup Code10To11WeeksGallonPot /* 10 - 11 weeks gallon pot */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("930a4bb6-0ce2-40ff-a012-c7531f6e7573")); }}
                public Lookup Code10To11WeeksSowToSellInPacks /* 10 - 11 weeks sow to sell in Packs */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("942cc2d3-41f1-4d9f-8c9d-934f981960fb")); }}
                public Lookup Code10To12Weeks /* 10 - 12 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f495ca52-e53d-4764-ae38-39efd53eeb38")); }}
                public Lookup Code10To12Months /* 10 - 12 months */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("28c29d83-13d1-44ca-b4ca-d13fc136bae8")); }}
                public Lookup Code10To12WeeksFromSowing /* 10 - 12 weeks from sowing */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b8846896-439f-4ae0-a9ea-8601cad470fc")); }}
                public Lookup Code10To16Weeks /* 10 - 16 weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4b33d38f-721d-4860-8bb1-07ceb3fa316d")); }}
                public Lookup Code10Weeks /* 10 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a1c88fd7-daba-4a46-992b-eca50e1fc689")); }}
                public Lookup Code11To13Weeks /* 11 - 13 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0fb8775f-13cd-4780-a6c4-3beab6bf8574")); }}
                public Lookup Code11To13WeeksFromSowing /* 11 - 13 weeks from sowing */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("51f520f2-e454-4e66-96c0-43122bdc4be2")); }}
                public Lookup Code11To15Weeks /* 11 - 15 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("180639d4-8f65-4628-86ad-5197836e610c")); }}
                public Lookup Code110To120DaysToFlower /* 110 - 120 Days to flower */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ca3b8234-36b1-49e9-82ae-539c0f5d191e")); }}
                public Lookup Code110DaysToFlower /* 110 Days to flower */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("91b9bad6-4f3e-4f31-b702-46029c69a48d")); }}
                public Lookup Code115DaysAfterTransplant /* 115 Days after transplant */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b8d5d3aa-d420-4616-b0d6-dff2c766b587")); }}
                public Lookup Code12To14Months /* 12 - 14 Months */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e3eecd37-66d0-4d4f-bc82-ed954baad90e")); }}
                public Lookup Code12To14Weeks /* 12 - 14 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("27d6d06e-79f8-4d0f-9292-612a7b3b2952")); }}
                public Lookup Code12To15Weeks /* 12 - 15 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("deb13091-df55-4978-927e-a80ad592c51c")); }}
                public Lookup Code12To16WeeksFromSowing /* 12 - 16 Weeks from sowing */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3f1c6b77-aeab-4ea5-8f32-9124ddbd6a87")); }}
                public Lookup Code12Weeks /* 12 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4cf37053-62e4-437c-a448-396be51715f6")); }}
                public Lookup Code13To15Weeks /* 13 - 15 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("78cf6ec8-b603-47dd-b6e8-8697d28b845c")); }}
                public Lookup Code13To16WeeksFromSowing /* 13 - 16 Weeks from sowing */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("047f7692-ed58-41df-85c2-3d89f28aa4a9")); }}
                public Lookup Code14To16Months /* 14 - 16 Months */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2b08c7c3-728a-4666-8cbf-1f82149b0254")); }}
                public Lookup Code14To18Weeks /* 14 - 18 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9a528354-f1c9-42ec-9d66-92ce07d6848b")); }}
                public Lookup Code14Weeks /* 14 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("33473127-2732-4065-8b5e-b324ef90eeb9")); }}
                public Lookup Code14WeeksFromSowing /* 14 Weeks from sowing */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f44fe97d-77c8-43f4-8336-cab9f8db152a")); }}
                public Lookup Code15To16WeeksFromSowing /* 15 - 16 Weeks from sowing */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d9402db6-34e2-4628-80de-d3dfd7276ba2")); }}
                public Lookup Code15WeeksFromSowing /* 15 Weeks from sowing */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("52ee6729-caeb-47f8-98da-bc67380001e5")); }}
                public Lookup Code16Weeks /* 16 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6ac7f780-23ca-491b-a336-4ed9c60682dd")); }}
                public Lookup Code17To19Weeks /* 17 - 19 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5fab204a-6a92-438f-aba9-4ea991d30769")); }}
                public Lookup Code17To21WeeksFromSowing /* 17 - 21 Weeks from sowing */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("fe6849ab-dd8f-45fa-b27f-33d400e653a2")); }}
                public Lookup Code17WeeksFromSowingIn4InchesPots /* 17 Weeks from sowing in 4" pots */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("809a911f-d331-49ad-b905-83bac41f2761")); }}
                public Lookup Code18To20Weeks /* 18 - 20 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8cc34cf5-f99e-498f-bf7f-4c4eef2483e1")); }}
                public Lookup Code19To21Weeks /* 19 - 21 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1f411127-286b-414d-a8ce-cc0a330629c7")); }}
                public Lookup Code20To25WeeksFromSowing /* 20 - 25 Weeks from sowing */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("53622d40-ae3e-4d9a-ae4f-3ff0623c603a")); }}
                public Lookup Code20Weeks /* 20 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("886e2a51-9641-474e-ae66-d24e1c01f952")); }}
                public Lookup Code22Weeks /* 22 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("94d0e0dc-7a8d-473d-84e6-cd3ba9cf1a11")); }}
                public Lookup Code25To27Weeks /* 25 - 27 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("393ab560-3a55-4326-bb3c-7797f4b6335c")); }}
                public Lookup Code36To48Weeks /* 36 - 48 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("838727db-e138-4b4c-86ef-a2f1eb4ab5ed")); }}
                public Lookup Code40To45Days /* 40 - 45 Days */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c96fee59-9341-4fe9-819f-9168a0fba0a0")); }}
                public Lookup Code5To6WeeksFromSowing /* 5 - 6 Weeks from sowing */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("078e46d6-207c-47e4-9e6b-4e4a79f5c090")); }}
                public Lookup Code5To8Weeks /* 5 - 8 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a636a037-7546-4535-b081-461698c9f204")); }}
                public Lookup Code50DaysAfterTransplant /* 50 Days after transplant */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("78b027d3-da52-4438-8afd-70f640da6c12")); }}
                public Lookup Code58To62Days /* 58 - 62 Days */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("df1f5145-4b51-4b49-b981-66144d7856d0")); }}
                public Lookup Code6To7Weeks /* 6 - 7 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b736ea99-c258-4035-bbd1-e206ae74763c")); }}
                public Lookup Code6To8Weeks /* 6 - 8 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("cf458694-8317-4dda-bdc6-dd0948e572cb")); }}
                public Lookup Code60To65Days /* 60 - 65 Days */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5058329f-7bf5-486a-87f1-135b9579a04c")); }}
                public Lookup Code7To10WeeksFromSowing /* 7 - 10 Weeks from sowing */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("62bed3c6-77fb-4169-9f8e-d910db4d17bc")); }}
                public Lookup Code8To10MonthsFromSowing /* 8 - 10 Months from sowing */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d5323a5d-8f5e-44c0-9b82-21392415c209")); }}
                public Lookup Code8To10Weeks /* 8 - 10 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f7b3353c-42de-4dd0-8f57-6979970a3aa2")); }}
                public Lookup Code8To10WeeksFromSowing /* 8 - 10 Weeks from sowing */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8322691e-5e2f-4653-ad96-10dd87bde719")); }}
                public Lookup Code8To11Weeks /* 8 - 11 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("03aaaa9e-eedc-4eb7-8483-3f894af58b79")); }}
                public Lookup Code8To12Weeks /* 8 - 12 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("588f271e-f001-4502-ac52-a44d1db4cd31")); }}
                public Lookup Code8To9Weeks /* 8 - 9 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("47ad89b9-032d-4ad0-ba2f-6b2a59e0c05c")); }}
                public Lookup Code8To9WeeksAfterTransplant6InchesPot /* 8 - 9 Weeks after transplant 6" pot */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("cd4a3741-0b3f-4a1f-9a96-047a3472851f")); }}
                public Lookup Code8To9WeeksFromTransplant /* 8 - 9 Weeks from transplant */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1057448b-01f4-4ab1-b52d-d200ec681d28")); }}
                public Lookup Code8Months /* 8 Months */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("47400289-9f02-4c09-be69-4e8a43239978")); }}
                public Lookup Code8Weeks /* 8 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c158bf22-5c12-4599-9843-37803590ee74")); }}
                public Lookup Code80DaysFromTransplant /* 80 Days from transplant */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d4afbed7-d0f9-4ed4-8706-5db1c7fd448e")); }}
                public Lookup Code9To10Months /* 9 - 10 Months */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e187a1d8-6d0b-4029-8a0b-96a2d46cc50f")); }}
                public Lookup Code9To10Weeks /* 9 - 10 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("745406c8-93ae-4941-9336-42d2947898a2")); }}
                public Lookup Code9To11Weeks /* 9 - 11 Weeks */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b2ef5bfe-2925-4231-8b14-0c649e05206b")); }}
                public Lookup Compact { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c76e14a4-b0f6-488b-9cad-1366dda03c27")); }}
                public Lookup Early { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("698cbb10-c9c6-4587-bdd9-845c252f3ea9")); }}
                public Lookup Late { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("34a5522e-21a1-4dcc-a3e0-6f130c2fe6f1")); }}
                public Lookup Medium { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e1abb23e-fc52-4b06-8a40-6edc7d3b8b78")); }}
                public Lookup Unknown { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("eac1f316-b7f6-4558-aaa2-9d716bbb1d60")); }}
                public Lookup Vigorous { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2bbd7b4c-5f9d-4e82-bb69-c9c2ea9bb3b2")); }}
            }

            public VigorValues Vigor { get { return new VigorValues();}}

            public class VigorValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c40a4379-606f-4bde-9167-b44fa80766ab")); }}
                public Lookup Code0To1Inches /* 0 - 1" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f54aa13f-4cb7-43a6-b301-da85274d38bf")); }}
                public Lookup Code0To2Inches /* 0 - 2" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("dfcaf00f-3a82-4b7c-bbd5-bc9bd4f1f23c")); }}
                public Lookup Code0To6Inches /* 0 - 6" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("eb6e9139-3ad5-4b55-b386-2a573a089bd6")); }}
                public Lookup Code1To2Lbs /* 1 - 2 lbs */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ecfbc957-bdda-4190-8ceb-cace88cca46a")); }}
                public Lookup Code1To2Inches /* 1 - 2" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("636c044f-72ae-4b0e-a97e-1d269d620f5b")); }}
                public Lookup Code1To4Inches /* 1 - 4" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("caad81d2-2b1b-4ffe-bfe2-a6e8ec685525")); }}
                public Lookup Code1To6Inches /* 1 - 6" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e0d124b8-705f-4916-94ea-953824cb475b")); }}
                public Lookup Code1Inches /* 1" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("fbab61d7-6253-4e0d-ac04-7988297e5178")); }}
                public Lookup Code10To11Feet /* 10 - 11 Feet */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c24a5caa-c46f-4b83-9622-d9210bda288a")); }}
                public Lookup Code10To12Feet /* 10 - 12 Feet */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d313d0de-62d9-4267-b229-26bbeb3b8abd")); }}
                public Lookup Code10To12Inches /* 10 - 12" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b0a36a0f-f592-4c28-a2c7-0f8205b817a4")); }}
                public Lookup Code10To13Inches /* 10 - 13" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("738c174f-099e-443a-9021-04e2183cec1a")); }}
                public Lookup Code10To14Inches /* 10 - 14" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0605c094-56ca-4a99-b6fc-8ac0f5008599")); }}
                public Lookup Code10To15Inches /* 10 - 15" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("52bd7d30-b1d6-42e9-93f8-41fbd4744490")); }}
                public Lookup Code10To16Inches /* 10 - 16" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("fd120452-f377-4fa2-b57e-d418929d5caf")); }}
                public Lookup Code10To18Inches /* 10 - 18" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a1150482-80e2-486a-9c2a-7a15de644e5c")); }}
                public Lookup Code10To20Inches /* 10 - 20" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0b341792-d623-4f8b-9b05-db4c3ec30b1e")); }}
                public Lookup Code10To24Inches /* 10 - 24" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("03c304d6-f40a-48b5-9afc-3074032a7548")); }}
                public Lookup Code10Feet /* 10 Feet */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ebe1f070-fe65-4f66-b84e-0ae1ce73ae2b")); }}
                public Lookup Code10Inches /* 10" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f599b3fc-da2d-4c1f-9c70-3b86e6b86420")); }}
                public Lookup Code11To15Inches /* 11 - 15" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d75d8429-1002-4971-b907-0ab1d53915b2")); }}
                public Lookup Code11To24Inches /* 11 - 24" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a8ab4ada-9beb-4397-af0a-81785a9d5216")); }}
                public Lookup Code11To28Inches /* 11 - 28" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0651706d-5eae-41bc-a570-c9e24d14c97a")); }}
                public Lookup Code11Inches /* 11" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6aee563b-9d4f-4e7a-b09d-9b5372325021")); }}
                public Lookup Code12To14Inches /* 12 - 14" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f6357242-b1f2-4bed-a3c6-52e294660bad")); }}
                public Lookup Code12To15Feet /* 12 - 15 Feet */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("501c494e-ab0f-4d69-99e1-30c1f5c5aea5")); }}
                public Lookup Code12To16Inches /* 12 - 16" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e1184252-4770-4ba0-ba82-58149f67c695")); }}
                public Lookup Code12To18Inches /* 12 - 18" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d8cc1e29-7cbf-4d1a-b63b-6beff5119f12")); }}
                public Lookup Code12To20Inches /* 12 - 20" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("be8bdbdc-65b5-4ba9-bbd1-85801b9da7d6")); }}
                public Lookup Code12To21Inches /* 12 - 21" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("28d44486-bcc0-49bc-8523-3a2b39d6a03a")); }}
                public Lookup Code12To22Inches /* 12 - 22" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0092f728-6829-4c3e-a198-4d679963e031")); }}
                public Lookup Code12To23Inches /* 12 - 23" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a514c163-a053-4545-b7cf-c65cad0d1947")); }}
                public Lookup Code12To24Inches /* 12 - 24" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("386ead6e-d9e7-446d-a7a9-541167fef98b")); }}
                public Lookup Code12To26Inches /* 12 - 26" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2dda0014-bb05-4f9a-a951-4a37a8cd931b")); }}
                public Lookup Code12To30Inches /* 12 - 30" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("75c821af-92d4-4946-ab61-bef64541abbf")); }}
                public Lookup Code12To36Inches /* 12 - 36" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("78d1b84d-c05c-4aae-9c8f-850cfab0be21")); }}
                public Lookup Code12To48Inches /* 12 - 48" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("035f8f96-5ffa-46d0-8d98-36810e76d3ac")); }}
                public Lookup Code12Inches /* 12" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b1989d9b-4a54-4254-91a9-664cd9f5e66b")); }}
                public Lookup Code13To15Inches /* 13 - 15" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1d1ab88c-7175-4113-ade9-4abf6be7b407")); }}
                public Lookup Code13To16Feet /* 13 - 16 Feet */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("03e647c6-fd52-468b-bd54-5e0871d991ec")); }}
                public Lookup Code13To22Inches /* 13 - 22" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e474386f-9547-47fa-8324-5502a6cd6e5b")); }}
                public Lookup Code13Inches /* 13" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4ccd328b-1033-4aad-95aa-dd9c4064dc92")); }}
                public Lookup Code14To16Inches /* 14 - 16" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("36278ba2-ba12-4538-8ffa-a6d0d6575e8c")); }}
                public Lookup Code14To18Inches /* 14 - 18" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6ae130e5-40d6-4bb3-bd96-719a738b2c60")); }}
                public Lookup Code14To22Inches /* 14 - 22" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("445f30c1-aa08-457e-a529-e2547427bd30")); }}
                public Lookup Code14To28Inches /* 14 - 28" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9df06845-16ab-4093-b7fe-c9fbacdefca4")); }}
                public Lookup Code14To30Inches /* 14 - 30" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("eed2cbcc-e370-4d01-b8bf-c23252f74658")); }}
                public Lookup Code14Inches /* 14" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d79feb8d-1cc3-4696-990e-4f0695f7a417")); }}
                public Lookup Code15To18Inches /* 15 - 18" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f9a8ea57-4f24-4747-ada6-248707340f4a")); }}
                public Lookup Code15To20Inches /* 15 - 20" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c37d70e9-8727-4de4-ac77-a2950ceb961e")); }}
                public Lookup Code15Inches /* 15" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b0bfa060-13c1-47b1-b71b-cc4fd9f69c93")); }}
                public Lookup Code16To18Inches /* 16 - 18" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ff4bf1aa-3a0d-4c17-a86d-dad606b989eb")); }}
                public Lookup Code16To20Inches /* 16 - 20" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("64d5bcd0-a75c-4de9-a879-461120c22455")); }}
                public Lookup Code16To24Inches /* 16 - 24" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d4976226-9252-409e-a321-48429e796b1f")); }}
                public Lookup Code16Inches /* 16" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ee9f7f44-4776-48fe-b07f-7eed7043ca22")); }}
                public Lookup Code17To18Inches /* 17 - 18" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a96f1347-868e-4f2e-9a0c-6017815328e4")); }}
                public Lookup Code17To20Inches /* 17 - 20" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8fb4a14a-1bb8-4945-bf6e-be314c946c0d")); }}
                public Lookup Code18To20Inches /* 18 - 20" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0f114676-de58-4e1b-b1b4-baf3ce7ad516")); }}
                public Lookup Code18To21Inches /* 18 - 21" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d4a4530a-9496-4c9e-bc58-d0b09b1ccc1c")); }}
                public Lookup Code18To22Inches /* 18 - 22" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4374294b-d64f-4e69-9e4d-817328502b11")); }}
                public Lookup Code18To23Inches /* 18 - 23" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1cb0052a-8845-42f6-9127-c538bd5e799a")); }}
                public Lookup Code18To24Inches /* 18 - 24" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e549ef57-1e30-47e8-86bb-4d539c050f2b")); }}
                public Lookup Code18To28Inches /* 18 - 28" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ad42c36b-a336-497c-aa49-383dd4ff9eaa")); }}
                public Lookup Code18To30Inches /* 18 - 30" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1c51699f-73fd-4550-9469-749659019a13")); }}
                public Lookup Code18To36Inches /* 18 - 36" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("295d841b-cd61-4190-8c85-81887d78a897")); }}
                public Lookup Code18Inches /* 18" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("35eaf6b9-ee74-4906-82e0-e90e2d472dd9")); }}
                public Lookup Code2To3Inches /* 2 - 3" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b702699e-b1e7-4a71-b49a-203e3fd3a9b0")); }}
                public Lookup Code2To4Inches /* 2 - 4" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5c6abe0d-38a8-4890-a31d-57c582aa8b28")); }}
                public Lookup Code2To6Inches /* 2 - 6" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("eeed137a-9048-4aac-aab0-e9a9d190bf81")); }}
                public Lookup Code2Inches /* 2" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("24641b07-44b9-4ba2-8113-509c69b6cf22")); }}
                public Lookup Code20To23Inches /* 20 - 23" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("314143b6-6fd5-4afa-a93d-8625b7bde928")); }}
                public Lookup Code20To24Inches /* 20 - 24" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f68b0cad-c742-4b90-9b37-c9b8065f67c6")); }}
                public Lookup Code20To25Inches /* 20 - 25" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("dd13fa85-6211-4bb1-a45c-8eee01c54cc2")); }}
                public Lookup Code20To26Inches /* 20 - 26" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("494573fe-1884-48ce-a5ac-ec4118f5dc6d")); }}
                public Lookup Code20To29Inches /* 20 - 29" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c74997f3-48da-4adb-ba64-307d3975a720")); }}
                public Lookup Code20To30Feet /* 20 - 30 Feet */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("561a487f-5619-47b5-bb09-2a8a450e56e5")); }}
                public Lookup Code20To30Inches /* 20 - 30" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("077343c2-1f34-47d3-821c-128a31a0f0dd")); }}
                public Lookup Code20To40Inches /* 20 - 40" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f7080eca-584a-4679-b880-f879057336d3")); }}
                public Lookup Code20To61Inches /* 20 - 61" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f178012e-dd51-4900-9444-d5fda2155b0c")); }}
                public Lookup Code20Inches /* 20" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8d5e07ab-0a33-4adf-9893-9a1f7ca46556")); }}
                public Lookup Code22To24Inches /* 22 - 24" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("da1dda9d-01e9-4142-98e7-4dcc9b520aa9")); }}
                public Lookup Code22Inches /* 22" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8528da91-79b1-4ffc-9490-1cbf1a04a5e2")); }}
                public Lookup Code23To27Inches /* 23 - 27" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e0cb67b7-c504-4464-974c-38908721aeb4")); }}
                public Lookup Code23To29Inches /* 23 - 29" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7da014b0-4434-4bf7-b6d7-85c36e17ed34")); }}
                public Lookup Code23To47Inches /* 23 - 47" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("69b4d3e5-a26a-4028-a643-b64474bfb60a")); }}
                public Lookup Code24To27Inches /* 24 - 27" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0acad5a6-6aaf-4e0a-ba72-de45d37c742c")); }}
                public Lookup Code24To28Inches /* 24 - 28" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("52aa2bbb-23da-4a1f-93b0-b78e65f89fc2")); }}
                public Lookup Code24To30Inches /* 24 - 30" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("55b6b3e0-803e-4197-ab72-4731758b60f6")); }}
                public Lookup Code24To32Inches /* 24 - 32" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5ccaf7a8-2384-4882-9021-d45c92c360b4")); }}
                public Lookup Code24To36Inches /* 24 - 36" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8f8d0938-1328-4688-955d-169922fee6b0")); }}
                public Lookup Code24To48Inches /* 24 - 48" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9cb51f9e-dc67-4113-b3ff-b3aedaa0b59e")); }}
                public Lookup Code24Inches /* 24" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ccc71842-e9ac-4b10-b2dd-e664d1f38ecc")); }}
                public Lookup Code27To29Inches /* 27 - 29" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("009c7f23-3eab-42e3-93ec-576e6e8c177c")); }}
                public Lookup Code27To31Inches /* 27 - 31" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f7ab293c-f306-42c7-a99a-e4e06fdb61a7")); }}
                public Lookup Code28To32Inches /* 28 - 32" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8e504fcb-ca9d-4bd7-93cc-71d84deb01ad")); }}
                public Lookup Code28To36Inches /* 28 - 36" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0f9a9f36-cf19-4b54-b469-fdf0788795d5")); }}
                public Lookup Code28Inches /* 28" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8db1f371-62fc-4413-955b-a740fa607300")); }}
                public Lookup Code29To31Inches /* 29 - 31" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("50525380-55bb-4bad-8eef-00706bcd9164")); }}
                public Lookup Code29To35Inches /* 29 - 35" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("445305f3-55a6-4361-85f3-3a4e322b22b4")); }}
                public Lookup Code29To39Inches /* 29 - 39" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("de5dd035-e6a2-4c74-95a0-649869da1e2d")); }}
                public Lookup Code3To12Inches /* 3 - 12" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2fe189b9-d436-433f-bd34-53123b94c34f")); }}
                public Lookup Code3To4Lbs /* 3 - 4 lbs */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a3442504-80d9-40b9-a239-eed694c933fd")); }}
                public Lookup Code3To4Inches /* 3 - 4" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("646d204b-0a78-4413-b780-b5bf1fe6ce78")); }}
                public Lookup Code3To6Inches /* 3 - 6" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("45c2a901-844a-4df5-acdc-4fe87891853d")); }}
                public Lookup Code3To8Feet /* 3 - 8 Feet */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("07e05f65-880b-4c07-b6bf-fd8a8357a6ef")); }}
                public Lookup Code3Inches /* 3" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("31b811b3-4b59-43cc-a539-bf5a5d7f0008")); }}
                public Lookup Code3Point5Lbs /* 3.5 lbs */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b454411b-ed63-49a4-828e-416c305bcd3f")); }}
                public Lookup Code30To36Inches /* 30 - 36" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("54450852-fcb3-4ed3-b8ae-6d19c4990788")); }}
                public Lookup Code30To40Inches /* 30 - 40" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ce541088-2054-487c-bfee-1b1e144e7b70")); }}
                public Lookup Code30To42Inches /* 30 - 42" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2957855b-d207-492c-a9d0-58bfaa879450")); }}
                public Lookup Code30To45Inches /* 30 - 45" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("bbe1e362-0636-47d5-8c27-81213cde17a8")); }}
                public Lookup Code30Inches /* 30" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2e02636b-1f99-465e-a261-23c44ac1598c")); }}
                public Lookup Code35To41Inches /* 35 - 41" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("23886345-eada-45e2-9941-3afd4037a6da")); }}
                public Lookup Code35To43Inches /* 35 - 43" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("acf48c44-8d60-48b4-bf3e-294dd861288f")); }}
                public Lookup Code35To47Inches /* 35 - 47" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3bb3189b-4f47-45f5-b92b-b63072822bab")); }}
                public Lookup Code36To40Inches /* 36 - 40" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6e40a127-a4e0-40c2-84dc-6d23746cd5a2")); }}
                public Lookup Code36To42Inches /* 36 - 42" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7fab178a-a0a8-4f89-a8c7-ca2ceebc9d7c")); }}
                public Lookup Code36To45Inches /* 36 - 45" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7878ce81-4622-478d-ad13-c200116412d1")); }}
                public Lookup Code36To48Inches /* 36 - 48" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c02db80f-1d8f-4587-bd34-8641f9d44975")); }}
                public Lookup Code36To60Inches /* 36 - 60" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5a2db21b-c1ce-43f9-b7f6-193bfda86f8c")); }}
                public Lookup Code36Inches /* 36" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("62b52cae-b97b-4a17-99e4-cf0645f7be51")); }}
                public Lookup Code38To39Inches /* 38 - 39" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("92afe900-9764-4ec9-895f-2a1b3647dfc2")); }}
                public Lookup Code4To10Inches /* 4 - 10" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("44ee6463-8e03-492f-ac0b-6ae3e06fad8d")); }}
                public Lookup Code4To12Inches /* 4 - 12" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e9df54fc-ec06-4f00-bc10-9c6ffe194879")); }}
                public Lookup Code4To5Inches /* 4 - 5" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("14355cbc-6b80-4878-b408-b642f2921b47")); }}
                public Lookup Code4To6Inches /* 4 - 6" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("bb07d32d-62c8-4862-8cfe-db7715807ff3")); }}
                public Lookup Code4To8Inches /* 4 - 8" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b1c76b94-fef7-4907-8592-eba2d36e7bab")); }}
                public Lookup Code4Inches /* 4" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("24af611a-91de-4a15-a625-f569218ce1d4")); }}
                public Lookup Code47To59Inches /* 47 - 59" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4700a86e-d7c3-4913-8671-876af892bc7a")); }}
                public Lookup Code48To72Inches /* 48 - 72" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("564d17b2-6659-4bf7-a35f-e75b3666f99a")); }}
                public Lookup Code48Inches /* 48" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ea0e0a16-6930-4504-aac5-1ca8fb387b2d")); }}
                public Lookup Code5To10Inches /* 5 - 10" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ab296e4d-d616-4544-8c8c-25d5c6d8f5af")); }}
                public Lookup Code5To12Inches /* 5 - 12" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b98ace1e-2cdd-47fb-8fce-5ee49e274cf2")); }}
                public Lookup Code5To6Inches /* 5 - 6" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0dbd1764-2ae0-49b0-baf2-a64e53b9470a")); }}
                public Lookup Code5To7Inches /* 5 - 7" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("baeae1dc-77ac-4860-98ed-dba098cbbecd")); }}
                public Lookup Code5To8Inches /* 5 - 8" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2c70b09a-2ff4-430b-b336-0bb7c8d0c3be")); }}
                public Lookup Code5To9Inches /* 5 - 9" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5263e619-ffdc-440c-b4f5-1cc7b06891a2")); }}
                public Lookup Code5Inches /* 5" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3017dfcb-22a4-4d8a-87ad-352a70d30094")); }}
                public Lookup Code6To10Feet /* 6 - 10 Feet */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7f537f04-9f90-4122-8321-a1d62a4bbb87")); }}
                public Lookup Code6To10Inches /* 6 - 10" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f442161b-55ab-41d4-b66b-66945266ac89")); }}
                public Lookup Code6To11Inches /* 6 - 11" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("49264bf9-e77a-4a57-83d8-2cca9af0d6f6")); }}
                public Lookup Code6To12Inches /* 6 - 12" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("622268dd-6ae2-4013-b949-df616cf2ab07")); }}
                public Lookup Code6To12InchesTall3FootSpread /* 6 - 12" tall 3 foot spread */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9cc04736-19d7-42b8-9e83-6515a56ceab8")); }}
                public Lookup Code6To15Inches /* 6 - 15" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("938ec322-8963-4045-bfca-4356d4eaea08")); }}
                public Lookup Code6To18Inches /* 6 - 18" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1e369a63-cf90-4209-b2f2-6482fb72d4a7")); }}
                public Lookup Code6To7Feet /* 6 - 7 Feet */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("43f6547c-7984-4962-a144-d46a890974c8")); }}
                public Lookup Code6To7Inches /* 6 - 7" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c4271830-047c-400e-b397-956496680a59")); }}
                public Lookup Code6To8Feet /* 6 - 8 Feet */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("41c3d421-f18f-4a2f-8f1e-8d7a7928b43a")); }}
                public Lookup Code6To8Inches /* 6 - 8" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("fd6d55e8-faf5-408b-aeef-bbb615dd6084")); }}
                public Lookup Code6To9Inches /* 6 - 9" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1d5423ba-3abd-43bb-a9fc-cf9eacf2d7b7")); }}
                public Lookup Code6Inches /* 6" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("fc2529f2-e15e-4dba-b947-2cc2b6c6075c")); }}
                public Lookup Code6InchesTall2To3FeetSpread /* 6" Tall 2 -3 feet spread */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("84d52c79-edbb-4f91-80a9-4facd00f12fa")); }}
                public Lookup Code60To72Inches /* 60 - 72" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f5e8df64-e55d-4080-b5eb-623506240a61")); }}
                public Lookup Code7To10Inches /* 7 - 10" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6984e48d-114e-4f9f-bb85-95c5ca4e70ed")); }}
                public Lookup Code7To13Inches /* 7 - 13" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("29d1b36b-7ea7-4f3d-b982-864cfb706298")); }}
                public Lookup Code7To14Inches /* 7 - 14" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d1ba0824-dc0d-4df4-894b-8dae5f6a6383")); }}
                public Lookup Code7To16Inches /* 7 - 16" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0f7cafcc-3fe7-468f-8270-438716206ade")); }}
                public Lookup Code7To17Inches /* 7 - 17" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5746af38-3b8e-4155-9962-614e5defbfe9")); }}
                public Lookup Code7To8Feet /* 7 - 8 Feet */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("fd24ff0b-b7c7-40fd-89d7-050a38f562aa")); }}
                public Lookup Code7Inches /* 7" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("db9de1cf-d9a8-4fe0-81e6-dfd70fa214a8")); }}
                public Lookup Code8To10Feet /* 8 - 10 Feet */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("418385f3-4d3b-408b-9c9d-7b110993c37b")); }}
                public Lookup Code8To10Inches /* 8 - 10" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d027649c-fe3d-47e7-895c-4d5cb39741bb")); }}
                public Lookup Code8To12Feet /* 8 - 12 Feet */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("16f78e34-e008-42e5-969a-bc1b16bf47d5")); }}
                public Lookup Code8To12Inches /* 8 - 12" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("15ac4f34-9fdb-4a29-9162-4da22e8c5652")); }}
                public Lookup Code8To14Inches /* 8 - 14" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9b4bf7ef-c4bb-41df-a2d9-0b661b65ab99")); }}
                public Lookup Code8To16Inches /* 8 - 16" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("56d1f1d8-1ebd-4436-9ebb-29d6031f1951")); }}
                public Lookup Code8To17Inches /* 8 - 17" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("794cb8ce-4c77-4d23-9b3c-a7126caf1472")); }}
                public Lookup Code8To18Inches /* 8 - 18" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("303291fb-d371-4d02-87bc-8efd93eeaea7")); }}
                public Lookup Code8To20Inches /* 8 - 20" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2dcbb21f-8ab0-412c-8aff-16f77c766648")); }}
                public Lookup Code8Inches /* 8" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8727b008-f1d0-4955-8d3a-9682f1a47cbc")); }}
                public Lookup Code9To10Inches /* 9 - 10" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("aa606204-06ff-41b7-a323-c86a630be3ee")); }}
                public Lookup Code9To12Inches /* 9 - 12" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("01c12c73-f01f-440b-b53a-e599e6637af3")); }}
                public Lookup Code9To13Inches /* 9 - 13" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ca455fd0-5e7c-462d-aa9a-bb6a6fbe33a4")); }}
                public Lookup Code9To14Inches /* 9 - 14" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e22cf525-1746-41b6-9a9b-2d397a3354ed")); }}
                public Lookup Code9To16Inches /* 9 - 16" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("24b68199-00f9-430b-8b1f-00f2f874e175")); }}
                public Lookup Code9To18Inches /* 9 - 18" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5ab14f3f-d774-44ae-9919-95ea436591da")); }}
                public Lookup Code9To28Inches /* 9 - 28" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("43eed1bb-873e-4b20-bbfd-6ac69191b304")); }}
                public Lookup Code9Inches /* 9" */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6aa52a10-7bf1-4652-a02c-a10cf3570349")); }}
                public Lookup Compact { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2fb2898d-c058-4745-8eb1-208d261e855e")); }}
                public Lookup Medium { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("65c21a09-0d5c-42ea-8d59-2a22dca11e1b")); }}
                public Lookup Strong { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("eef21eff-5baf-47f3-8080-8c5f76884761")); }}
                public Lookup Unk /* UNK */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("db192550-57ca-4af0-ad1c-670cc814d698")); }}
            }

            public VolumeTypeValues VolumeType { get { return new VolumeTypeValues();}}

            public class VolumeTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1038509a-fa95-4dfe-add4-54e42ee2aff5")); }}
                public Lookup Cutting { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f4a6a9ce-2975-44e9-be23-4adf0082a2b9")); }}
                public Lookup Tray { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("77c49629-f0b9-45cb-9f1c-cbc7fff9ad1a")); }}
            }

            public ZoneValues Zone { get { return new ZoneValues();}}

            public class ZoneValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("060f7742-cae2-4d8d-87e2-09e6e7b5793c")); }}
                public Lookup Zone1 /* Zone 1 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4da0cd76-b6ae-4902-a540-94f8176e3657")); }}
                public Lookup Zone2 /* Zone 2 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4b9e37e6-6c4e-4bf7-a6de-0d5daddc95ca")); }}
                public Lookup Zone3 /* Zone 3 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("93387a38-b11e-461b-9547-83e5169acf09")); }}
                public Lookup Zone4 /* Zone 4 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c353a663-a39c-4ce3-809b-37c6c087fe27")); }}
                public Lookup Zone5 /* Zone 5 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("63cf4fa1-04ff-41c5-91e7-c0bb02036c3f")); }}
                public Lookup Zone6 /* Zone 6 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c9ede847-711f-4a60-9928-d72fcad3d8c8")); }}
                public Lookup Zone7 /* Zone 7 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d2168847-500d-4efd-bbec-5449007dd441")); }}
                public Lookup Zone8 /* Zone 8 */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c616d2e9-b059-49f5-891a-765de87d0e5b")); }}
            }

        }

        public static EntryTypeValues EntryType /* Entry Type */ { get { return new EntryTypeValues();}}

        public class EntryTypeValues
        {
            public Lookup LookupValue /* Entry Type */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("897b5ca0-ea81-403e-ae0c-b0057d8d4328")); }}
            public Lookup Credit { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("37c1baee-f92a-4b4b-a5db-7f0f9bbdcb66")); }}
            public Lookup Debit { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("683f8f22-8702-4d87-b712-45d9275f8dcb")); }}
        }

        public static LoggingValues Logging { get { return new LoggingValues();}}

        public class LoggingValues
        {
            public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8286eedc-ee2f-4191-a1eb-6a5f91b04978")); }}
            public DataValues Data { get { return new DataValues();}}

            public class DataValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e6855022-3d4a-43af-be99-10c3e9674320")); }}
                public Lookup Add { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d8c5a2ee-0b81-45da-803a-b0bf742686c1")); }}
                public Lookup Delete { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("efcb9838-32f3-4a11-b61c-2d79790b37f2")); }}
                public Lookup Update { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("88237af3-b12f-4a97-aab9-14ae4b4fc8ac")); }}
            }

            public LogTypeValues LogType { get { return new LogTypeValues();}}

            public class LogTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("12aba2b9-7f76-4cee-847d-7e230b9fc833")); }}
                public Lookup CommentExternal { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8dd32f59-f25c-44e8-a321-4d530beb1924")); }}
                public Lookup CommentInternal { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d706520c-c5f8-415d-80b7-2e86680d8713")); }}
                public Lookup DatabaseError { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b4f0beda-f50f-4a8e-a939-3c9d44cce2bc")); }}
                public Lookup Debug { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3dc3f8aa-3571-4fcc-9469-6c2ec7a5a0d3")); }}
                public GrowerValues Grower { get { return new GrowerValues();}}

                public class GrowerValues
                {
                    public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("aff35203-6946-4d85-afc6-a6dddbd8669d")); }}
                    public Lookup CommentExternal { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("58ac1227-1213-47f4-87c6-a682aa19b8db")); }}
                    public Lookup CommentInternal { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6befa2fb-2cbe-4e1b-a804-fc7e5dcfdb94")); }}
                    public Lookup GrowerOrderCancel { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1211bfdb-ee89-43f7-9040-cd672cff5946")); }}
                    public Lookup GrowerOrderPlace { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b2242e6a-ae6d-4f37-b4f9-3bc417dbbb9a")); }}
                    public Lookup OrderLineAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2bf232d7-03c5-4fdb-bec2-2eb3d12ee613")); }}
                    public Lookup OrderLineCancel { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3dca94b5-09ad-476d-9c74-6d2035d9928f")); }}
                    public Lookup OrderLineChange { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ac5ca053-139e-4f5e-a89a-b760f8b7ab94")); }}
                    public Lookup OrderLineSubstitute { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9667f3e2-d55a-4bff-8882-fdde85e032e4")); }}
                    public Lookup SupplierOrderCancel { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0c11e737-fd51-432e-b440-b077bceba445")); }}
                }

                public Lookup InternalProcDebug { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("608c7ec2-12aa-49fb-97b9-6bfb8803f620")); }}
                public Lookup InvalidParameters { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d2a60f94-8795-4d67-973b-395d1ad6a800")); }}
                public Lookup KeyNotFound { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5dcae8d7-7a12-40ec-9fcb-968cd0d5ab9e")); }}
                public Lookup ParameterError { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2fa169a5-6a07-4da2-8f28-9b3b3e182b36")); }}
                public Lookup PersonNotAssociatedWithAgrower /* PersonNotAssociatedWithAGrower */ { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("12388dd1-9110-483f-a8d7-c9bce1595708")); }}
                public Lookup PersonNotAssociatedWithProcedureCall { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a023a729-d3f6-4cd8-b638-d14768a2c63f")); }}
                public Lookup ProcedureCall { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b5b75e4d-cc45-437f-85ef-7e8e51df6a7c")); }}
                public Lookup ProcedureCallComplete { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a6e09b9e-f04f-4d38-8977-720dbbac70ab")); }}
                public Lookup ProcedureCallEnd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f9c28a8e-0b19-4b78-ab32-8c7b329b83cb")); }}
                public Lookup RollBack { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1ecde3bd-73d5-40d3-964b-3d519831fce8")); }}
                public Lookup RowAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7d088718-2be8-456a-986f-e71ca4762b6e")); }}
                public Lookup RowAddError { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("dfc62c5e-7c24-4c59-b666-ccb0c439f26b")); }}
                public Lookup RowDelete { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("58edc940-fda9-4951-a159-d07628a89dda")); }}
                public Lookup RowDeleteError { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5e0dd765-41f5-4a32-917a-fd56a8db6cea")); }}
                public Lookup RowDeleteFail { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ce8ecf76-571b-4455-abad-1d832a9f5025")); }}
                public Lookup RowUpdate { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("db1af7dd-3054-4d83-ad04-12eb9c6a04dd")); }}
                public Lookup RowUpdateError { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a0b5b6b5-791b-4a51-a9ed-68d775898c19")); }}
                public Lookup ServerCode { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e5d92184-472a-46c1-935a-0e8a395d5fb9")); }}
                public Lookup TableRowUpdate { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("30a3cbc1-6b8e-4ae5-b3ec-888bb96d0f54")); }}
                public Lookup TransactionFail { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("441664cd-dae4-4f15-a468-92f687b6833d")); }}
            }

            public ObjectTypeValues ObjectType { get { return new ObjectTypeValues();}}

            public class ObjectTypeValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c8f22abd-d6b0-43f6-a5e0-cc5b27dfbe40")); }}
                public Lookup Address { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("6a50a92c-eef8-4883-a165-69e21136707a")); }}
                public Lookup Debug { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3aa672d1-247e-48d0-90c9-4933f08b2e1a")); }}
                public Lookup EventLog { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8bbbe169-d96e-4ec0-9ab8-3931c07fa540")); }}
                public Lookup Grower { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3cb17779-d57a-4ae0-acc5-a59673934346")); }}
                public Lookup GrowerAddress { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("381d1479-00fe-4046-9b8b-023bcc6df8e7")); }}
                public Lookup GrowerOrder { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("52043a09-7339-4dee-9fe9-0a1273df8413")); }}
                public Lookup GrowerPersonTriple { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("79b2429f-da73-4786-8ba8-e09aac55d60c")); }}
                public Lookup Lookup { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("2ce29dc5-a0be-458f-a965-f99b4e820424")); }}
                public Lookup OrderLine { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ab992d8e-35ed-4754-b166-a5998e1427fc")); }}
                public Lookup Parameters { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d0f9e337-7918-4af5-8d2d-7847c6b52f0b")); }}
                public Lookup Person { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("53bb1e2f-e322-4480-b4a4-37238b5b8522")); }}
                public Lookup Product { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0827c8bf-84db-45c2-bda9-1230766c3d32")); }}
                public Lookup Program { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d448a009-62f6-4964-baf0-c8a8834f885f")); }}
                public Lookup ShipWeek { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b2772af2-05d4-461f-be96-128e2e47b601")); }}
                public Lookup Supplier { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("406b53c4-1f15-4695-abc9-55d7deac94d6")); }}
                public Lookup SupplierOrder { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f2bda474-587f-4d1c-a68f-426f0122b82d")); }}
            }

            public PriorityValues Priority { get { return new PriorityValues();}}

            public class PriorityValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("123abd86-1c20-4189-b229-e2fc1a6ebc72")); }}
                public Lookup High { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("fb4350ea-e34f-4c46-93f8-c971f4928102")); }}
                public Lookup Low { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("efc6bad2-acdf-441b-9b3b-418847612fc5")); }}
                public Lookup Medium { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ac331ec8-72f2-4df2-8f54-57b28291c0c5")); }}
            }

            public ProcessValues Process { get { return new ProcessValues();}}

            public class ProcessValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f83277cd-167c-483c-babb-1d76d3074f0c")); }}
                public Lookup AddGrowerOrderLog { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("dcf25b5a-4e93-4547-8776-77f59968a144")); }}
                public Lookup AddressAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("937c7af4-7db7-4aa0-b055-b751cfd0afb1")); }}
                public Lookup AvailabilityDataGet { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7e99bc5f-f80c-4f28-ae43-59fd4dff1943")); }}
                public Lookup CancelGrowerOrder { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("be2c83d7-aa22-4a8f-83af-507a0181b722")); }}
                public Lookup CancelOrderLine { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("886af73b-675d-4be8-8727-d7b011bfa358")); }}
                public Lookup ChangeStatus { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7fcafceb-8487-4c56-982c-a56fe0d899a4")); }}
                public Lookup CheckForParameterOverflow { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("cfdb9cd1-9029-481e-92be-caca87d65732")); }}
                public Lookup DataServiceEventLogAddLogTest { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b3f0e83b-6574-423b-b09b-28107725088e")); }}
                public Lookup DataServiceEventLogAddWithAllParmsLogTest { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f13d5fee-f4ba-4be4-b3d4-350322cd42e8")); }}
                public Lookup DatabaseCleanup { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("23d2ffb4-da8c-4092-9e34-c421588fe53e")); }}
                public Lookup Debug { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3051637b-cfe4-4fc6-bfb5-c2b38000494d")); }}
                public Lookup DeletePlacedOrder { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8e70905b-b4a2-4e83-b50f-aeb79cac5bba")); }}
                public Lookup EventLogAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0a418bf3-de93-4bb0-98c6-2f44ea9adf91")); }}
                public Lookup EventLogAddBase { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("766e3aeb-65ab-49c4-8a71-056d4dd7e1f4")); }}
                public Lookup EventLogOrderDataGet { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("dd247a0a-71e5-4f8c-831e-44312f57518b")); }}
                public Lookup GeneticOwnerDataGet { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("af4a5a55-6d10-4a6c-9ed8-08ad805cc64a")); }}
                public Lookup GrowerAddressAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f082ad0c-f619-439b-842d-84eb00e16b46")); }}
                public Lookup GrowerAddressAddExisting { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c1243d80-36f2-4da0-a479-bd40fc22b1e4")); }}
                public Lookup GrowerAddressDataGet { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("adf50b9c-b3ab-42cc-84d6-2407fa4399dd")); }}
                public Lookup GrowerAddressDelete { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7f5e3b31-d3c0-4684-84c3-3534eae9a67d")); }}
                public Lookup GrowerAddressUpdate { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1fdfe74d-46a4-4403-8e16-f9b50e4a0157")); }}
                public Lookup GrowerClaimDataGet { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("be1f1df9-cf18-45a4-98d7-4712372278e5")); }}
                public Lookup GrowerDataGet { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f4bbbdfc-8fe2-4ce1-9213-c3fe0cf18487")); }}
                public Lookup GrowerGetGuid { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("18e068d9-1981-4002-a8c0-2782b26bad64")); }}
                public Lookup GrowerOrderAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b84f53cc-f90f-4086-83f9-89298f1d9820")); }}
                public Lookup GrowerOrderDataGet { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("0e98a099-a990-422c-80ab-2c353b62aa52")); }}
                public Lookup GrowerOrderDetailDataGet { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c646b5f1-6a0c-4ee6-997d-e563c40390d1")); }}
                public Lookup GrowerOrderGetOrAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d14334e4-2206-42d2-b2b3-72f85fcf999d")); }}
                public Lookup GrowerOrderHistoryDataGet { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d9f3c2b8-6905-4812-b407-021b5b65083e")); }}
                public Lookup GrowerOrderProcessTransaction { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("bfc5b760-5abf-4cc9-9d42-f251cb0ca920")); }}
                public Lookup GrowerOrderSummaryByShipWeekGetData { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("b98eb3f0-43de-4527-b8e5-563fd0527d64")); }}
                public Lookup GrowerOrderUpdate { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("27adf0d9-6cb4-4c23-a0c7-dde18dffa875")); }}
                public Lookup GrowerSearch { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ff86d891-30f5-49e6-93df-cc10bc445012")); }}
                public Lookup GrowerVolumeUpdateQuotes { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("fdc7c892-b874-410f-9a9b-50847664c907")); }}
                public Lookup LookupAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("8871f600-a335-46c2-8e81-04f29c2a539f")); }}
                public Lookup LookupCheck { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7170e585-bda6-41d3-b96f-8eef51698d88")); }}
                public Lookup LookupGetOrAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7d5b2b57-ff95-4968-b2cb-365c23b8e05d")); }}
                public Lookup OrderLineAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f1ec6e43-623e-476b-bc79-29cb5b7f8f0f")); }}
                public Lookup OrderLineChangeStatus { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("618e68f8-77b9-4f30-9fd2-3b8cb088ade8")); }}
                public Lookup OrderLineProcessTransaction { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a33fbdce-33e6-470b-b44f-4d450b8fd067")); }}
                public Lookup PersonRegister { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("07d3c684-f5b5-4b8a-be0e-a4309963e70c")); }}
                public Lookup PlaceOrder { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3664256d-18e7-4691-bab2-0616fb87c2a1")); }}
                public Lookup ProgramSeasonAvailUpdate { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("d1388540-20bf-46a9-8477-572a9ffa6740")); }}
                public Lookup RowAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("603caf9f-24ee-435a-8505-ac7dfc6ed652")); }}
                public Lookup SearchTermGetAllResults { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e0dbff41-f3b1-436b-802d-38a29457144f")); }}
                public Lookup ShipWeekDataGet { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("c25d92d9-c0a7-4f3a-b74f-1e6935eb59b0")); }}
                public Lookup ShipWeekDefaultWeekNumberGet { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4b2b01f4-9455-4df5-9168-26f87898c98b")); }}
                public Lookup ShipWeekGetData { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e2b9b95b-96f9-49d0-a9e1-86d4262775db")); }}
                public Lookup SpeciesDataGet { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("ee881d94-ac2d-46c7-8b98-766058ab05b6")); }}
                public Lookup SubstituteProductOnOrderLine { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("eabca635-efb8-4e43-a31a-22e988c67f0d")); }}
                public Lookup SupplierGetOrAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9b130361-974c-47a3-ad1c-9eefb917b7fb")); }}
                public Lookup SupplierOrderAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("bd56444e-7e6f-4c79-8150-b46c5a4e7d01")); }}
                public Lookup SupplierOrderChangeState { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("dd39e5ec-871a-456e-b502-411d6533311b")); }}
                public Lookup SupplierOrderGetOrAdd { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e975ced2-cc93-4697-8406-b7f150a79320")); }}
                public Lookup SupplierOrderRecalcFees { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("da740758-56aa-4782-a093-96bf377f52f3")); }}
                public Lookup SupplierOrderUpdate { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("385ddd6c-ae4f-4778-aa14-bb34d92f2790")); }}
                public Lookup Test { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1ecb5c91-bfdb-4a4c-8ca1-793789366ed9")); }}
                public Lookup UpdateCartQuantity { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("32c547ba-5bb4-4659-87c9-f9a0145c28d3")); }}
                public Lookup UpdatePriceAndCost { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("535852db-51fe-4707-9a46-4c3861c79d87")); }}
                public Lookup UpdateTrackingData { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("38fd94c0-ebd2-4e75-81ec-c2426cef1842")); }}
                public Lookup VarietyDataGet { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("893bb9f0-7a96-4de4-9dea-8f09395ee75d")); }}
            }

            public SeverityValues Severity { get { return new SeverityValues();}}

            public class SeverityValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4521ed00-eba0-4378-87ee-5fe7f66b441e")); }}
                public Lookup Debug { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("a419f830-954a-4478-9f32-d4ac0ae1b60a")); }}
                public Lookup Error { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("84df56e3-43b7-4688-b82e-9fc9541e031b")); }}
                public Lookup Failure { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("125e370c-e294-4685-903f-e50c09e2a3a7")); }}
                public Lookup Info { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("94c1af68-e716-4e81-85a8-56fcdecfcb68")); }}
                public Lookup Performance { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("583fe6d1-6ebe-4aad-8b36-a8ded38746c2")); }}
                public Lookup Warning { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("e59de133-2132-4ec0-9c98-2c5764d554d6")); }}
            }

            public VerbosityValues Verbosity { get { return new VerbosityValues();}}

            public class VerbosityValues
            {
                public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("1c321b37-5c30-40db-8f0b-845d785bbda5")); }}
                public Lookup Minimal { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("76b0a995-fdb0-4c62-9725-493143703936")); }}
                public Lookup Normal { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f92f4d16-1ba3-4f3e-8a41-de56736c3064")); }}
                public Lookup Terse { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4fe84502-282a-48c6-b416-9d6dadbef2a3")); }}
                public Lookup Verbose { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("7accb820-eac6-49aa-8cd6-cbb9dda87e53")); }}
            }

        }

        public static PredicateValues Predicate { get { return new PredicateValues();}}

        public class PredicateValues
        {
            public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("4d0443b7-be0f-44b8-8e35-6797e00b6a76")); }}
            public Lookup FileExtensionMimeType { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("db7a4cf3-9e7d-45a5-8f9c-a720762cd107")); }}
            public Lookup GrowerPerson { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9c73713d-0634-40fe-8a52-739c26d51a9d")); }}
            public Lookup NormalBalance { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("74982c08-c701-401c-8359-9fc34d2f7884")); }}
            public Lookup ProgramShipMethodDefault { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("5581faee-b139-498a-adcf-d921b32d1b45")); }}
            public Lookup ProgramShipMethodShown { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("81bc8954-e3e3-4360-a2d8-fce010a66789")); }}
            public Lookup ProgramShipMethodValid { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("9d5b6a62-6949-4fbe-bdc7-1c64b16d4eb1")); }}
            public Lookup ProgramTagRatio { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("39d76f7d-3570-47b3-8d3b-15b49c091662")); }}
            public Lookup ProgramTagRatioDefault { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("3098d6ba-7241-4aef-abd3-bf85fa763afa")); }}
            public Lookup TestGrower { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("f3329cc9-2f3c-4d7c-ae71-259ed4f42dd4")); }}
        }

        public static TestDataValues TestData { get { return new TestDataValues();}}

        public class TestDataValues
        {
            public Lookup LookupValue { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("02cc0dd2-1de8-4b56-b034-5b960b2d46b4")); }}
            public Lookup TestItem8888 { get{ return LookupTableService.SingletonInstance.GetByGuid(new Guid("035c9a11-f62b-463d-b63e-76cd398beba8")); }}
        }

    }
}


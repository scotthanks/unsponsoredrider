﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using DataAccessLibrary;
using DataObjectLibrary;
using BusinessObjectsLibrary;

namespace DataServiceLibrary
{

    public class GrowerOrderData
    {
        public DataObjectLibrary.GrowerOrder GrowerOrder { get; set; }
        public List<ProductGrowerSeasonPriceView> PriceList { get; set; }
        public List<ProgramSeason> ProgramSeasonList { get; set; }
        public List<GrowerOrderHistory> GrowerOrderHistoryList { get; set; }
        public List<DataObjectLibrary.GrowerOrderFee> GrowerOrderFeeList { get; set; }
    }
    
    public class GrowerOrderDataService : DataServiceBaseNew
    {
        public GrowerOrderDataService(StatusObject status)
            : base(status)
        {

        }
    
        public Guid userGuid { get; set; }
        public string shipWeekCode { get; set; }
        public string programTypeCode { get; set; }
        public string productFormCategoryCode { get; set; }


        public void ResetProgramCategoryType(string shipWeekCode, string programTypeCode, string productFormCategoryCode)
        {

            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open(); 

            try
            {
                //This grabs the order for the purpose of recalculating fees

                var getGuidProc = new GrowerOrderDataService(Status);
                Guid growerOrderGuid = getGuidProc.GrowerOrderGetGuid(shipWeekCode, programTypeCode, productFormCategoryCode, false);
               
                var command = new SqlCommand("[dbo].[GrowerOrderResetProgramCategoryType]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = userGuid;
                command.Parameters
                    .Add(new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerOrderGuid;

                var response = (int)command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderResetProgramCategoryType",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                    
            }


            finally
            {
                connection.Close();
            }


           
        }
        public bool UpdateCustomerPoNoOnOrder(Guid growerOrderGuid, string poNo)
        {
            Guid creditCardGuid = new Guid();
            var order = this.GetGrowerOrderDetailData(growerOrderGuid, false);
            string paymentType = order.GrowerOrder.PaymentTypeLookup.Code;
            if (order.GrowerOrder.GrowerCreditCardGuid != null)
            {
                creditCardGuid = new Guid(order.GrowerOrder.GrowerCreditCardGuid.ToString());
            }
            Guid addressGuid = new Guid(order.GrowerOrder.ShipToAddressGuid.ToString());
            var bRet = this.UpdateOrder(Status.User.UserGuid, growerOrderGuid, poNo, order.GrowerOrder.Description, addressGuid, paymentType, creditCardGuid, order.GrowerOrder.PromotionalCode);
            return bRet;
        }

        public bool UpdatePromotionalCodeOnOrder(Guid growerOrderGuid, string promotionalCode)
        {
            var order = this.GetGrowerOrderDetailData(growerOrderGuid, false);
            Guid creditCardGuid = new Guid();
            string paymentType = order.GrowerOrder.PaymentTypeLookup.Code;
            if (order.GrowerOrder.GrowerCreditCardGuid != null)
            {
                 creditCardGuid = new Guid(order.GrowerOrder.GrowerCreditCardGuid.ToString());
            }
           
            Guid addressGuid = new Guid(order.GrowerOrder.ShipToAddressGuid.ToString());
            var bRet = this.UpdateOrder(Status.User.UserGuid, growerOrderGuid, order.GrowerOrder.CustomerPoNo, order.GrowerOrder.Description, addressGuid, paymentType, creditCardGuid, promotionalCode);
            return bRet;
        }

        public bool UpdateDescriptionOnOrder(Guid growerOrderGuid, string description)
        {
            Guid creditCardGuid = new Guid();
            var order = this.GetGrowerOrderDetailData(growerOrderGuid, false);
            string paymentType = order.GrowerOrder.PaymentTypeLookup.Code;
            if (order.GrowerOrder.GrowerCreditCardGuid != null)
            {
                creditCardGuid = new Guid(order.GrowerOrder.GrowerCreditCardGuid.ToString());
            }
            Guid addressGuid = new Guid(order.GrowerOrder.ShipToAddressGuid.ToString());
            string promotionalCode = order.GrowerOrder.PromotionalCode;
            var bRet = this.UpdateOrder(Status.User.UserGuid, growerOrderGuid, order.GrowerOrder.CustomerPoNo, description, addressGuid, paymentType, creditCardGuid, promotionalCode);
            return bRet;
        }

        public bool UpdatePaymentTypeOnOrder(Guid growerOrderGuid, string paymentType)
        {
            Guid creditCardGuid = new Guid();
            var order = this.GetGrowerOrderDetailData(growerOrderGuid, false);
            if (order.GrowerOrder.GrowerCreditCardGuid != null)
            {
                creditCardGuid = new Guid(order.GrowerOrder.GrowerCreditCardGuid.ToString());
            }
            Guid addressGuid = new Guid(order.GrowerOrder.ShipToAddressGuid.ToString());
            string promotionalCode = order.GrowerOrder.PromotionalCode;
            var bRet = this.UpdateOrder(Status.User.UserGuid, growerOrderGuid, order.GrowerOrder.CustomerPoNo, order.GrowerOrder.Description, addressGuid, paymentType, creditCardGuid, promotionalCode);
            return bRet;
        }
        public bool UpdateCardOnOrder( Guid growerOrderGuid,  Guid growerCreditCardGuid)
        {
            var order = this.GetGrowerOrderDetailData(growerOrderGuid, false);
            string paymentMethod = order.GrowerOrder.PaymentTypeLookup.Code;
            Guid addressGuid =new Guid(order.GrowerOrder.ShipToAddressGuid.ToString());
            string promotionalCode = order.GrowerOrder.PromotionalCode;
            var bRet = this.UpdateOrder(Status.User.UserGuid, growerOrderGuid, order.GrowerOrder.CustomerPoNo, order.GrowerOrder.Description, addressGuid, paymentMethod, growerCreditCardGuid, promotionalCode);
            return bRet;
        }
        public bool UpdateAddressOnOrder(Guid growerOrderGuid, Guid growerAddressGuid)
        {
            Guid creditCardGuid = new Guid();
            var order = this.GetGrowerOrderDetailData(growerOrderGuid, false);
            string paymentMethod = order.GrowerOrder.PaymentTypeLookup.Code;
            if (order.GrowerOrder.GrowerCreditCardGuid != null)
            {
                creditCardGuid = new Guid(order.GrowerOrder.GrowerCreditCardGuid.ToString());
            }
            string promotionalCode = order.GrowerOrder.PromotionalCode;
            var bRet = this.UpdateOrder(Status.User.UserGuid, growerOrderGuid, order.GrowerOrder.CustomerPoNo, order.GrowerOrder.Description, growerAddressGuid, paymentMethod, creditCardGuid, promotionalCode);
            return bRet;
        }
        public bool UpdateOrder(Guid userGuid, Guid growerOrderGuid, string customerPoNo, string orderDescription, Guid shipToAddressGuid, string paymentTypeLookupCode, Guid? growerCreditCardGuid, string promotionalCode)
        {
            orderDescription = orderDescription ?? "";
            customerPoNo = customerPoNo ?? "";
            customerPoNo = customerPoNo.Trim();

            

            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
           
            try
            {

                var command = new SqlCommand("[dbo].[GrowerOrderUpdate]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = userGuid;
                command.Parameters
                    .Add(new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerOrderGuid;
                command.Parameters
                    .Add(new SqlParameter("@CustomerPoNo", SqlDbType.NChar, 100))
                    .Value = customerPoNo;
                command.Parameters
                    .Add(new SqlParameter("@Description", SqlDbType.NChar, 50))
                    .Value = orderDescription;
                command.Parameters
                    .Add(new SqlParameter("@ShipToAddressGuid", SqlDbType.UniqueIdentifier))
                    .Value = shipToAddressGuid;
                command.Parameters
                   .Add(new SqlParameter("@PaymentTypeLookupCode", SqlDbType.NChar, 20))
                   .Value = paymentTypeLookupCode;
                command.Parameters
                   .Add(new SqlParameter("@GrowerCreditCardGuid", SqlDbType.UniqueIdentifier))
                   .Value = growerCreditCardGuid;
                command.Parameters
                   .Add(new SqlParameter("@PromotionalCode", SqlDbType.NVarChar,20))
                   .Value = promotionalCode;
                command.Parameters
                    .Add(new SqlParameter("@Success", SqlDbType.Bit, 1))
                    .Direction = ParameterDirection.Output;


                var response = (int)command.ExecuteNonQuery();
                var success = (bool)command.Parameters["@Success"].Value;
                
                Status.Success = success;
                Status.StatusMessage = "Order Updated";
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                
            }
            finally
            {
                connection.Close();
            }
            return Status.Success;

        }






        public int AddToOrder(string userCode, Guid growerOrderGuid,string shipWeekCode)
        {
            int itemsAddedToOrder = 0;
            try
            {
                var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
                var command = new SqlCommand("[dbo].[GrowerOrderAddPreCartLines]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@UserCode", SqlDbType.NVarChar, 56))
                    .Value = userCode;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@ShipWeekCode", SqlDbType.NChar, 6))
                    .Value = shipWeekCode;
                command.Parameters
                    .Add(new SqlParameter("@GrowerOrderToGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerOrderGuid;
                command.Parameters
                    .Add(new SqlParameter("@OrderLinesChanged", SqlDbType.Int))
                    .Direction = ParameterDirection.Output;


                connection.Open();
                var response = (int)command.ExecuteNonQuery();
                itemsAddedToOrder = (int)command.Parameters["@OrderLinesChanged"].Value;
                

            }
            catch (Exception ex) 
            {
                var errormsg = ex.Message;
                itemsAddedToOrder = 0;
            }
            return itemsAddedToOrder;
        }

        public void RecalculateFeesbyGrowerOrderGuid(Guid growerOrderGuid,Boolean recalcSupplierFees)
        {
            var recalcFeesProc = new GrowerOrderDataService(Status);
            recalcFeesProc.GrowerOrderRecalcFees(growerOrderGuid, recalcSupplierFees,true);
        }

        public AvailabilityData UpdateCartQuantity(string userCode, string shipWeekCode, Guid productGuid, int quantity, string orderTypeLookupCode = "Order", decimal price = 0, bool getNewRowData = true, string sellerCode = "GFB")
        {
            sellerCode = "GFB";
            AvailabilityData availabilityData = null;



            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerOrderProcessTransaction]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@UserCode", SqlDbType.NChar, 56))
                    .Value = userCode;
                command.Parameters
                    .Add(new SqlParameter("@ShipWeekCode", SqlDbType.NChar, 6))
                    .Value = shipWeekCode;
                command.Parameters
                    .Add(new SqlParameter("@ProductGuid", SqlDbType.UniqueIdentifier))
                    .Value = productGuid;
                command.Parameters
                    .Add(new SqlParameter("@Quantity", SqlDbType.Int, 4))
                    .Value = quantity;
                command.Parameters
                    .Add(new SqlParameter("@OrderTypeLookupCode", SqlDbType.NChar, 10))
                    .Value = orderTypeLookupCode;
                command.Parameters
                    .Add(new SqlParameter("@Price", SqlDbType.Decimal, 5))
                    .Value = price;
                command.Parameters
                    .Add(new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier))
                     .Direction = ParameterDirection.Output;
                command.Parameters
                    .Add(new SqlParameter("@SupplierOrderGuid", SqlDbType.UniqueIdentifier))
                     .Direction = ParameterDirection.Output;
                command.Parameters
                    .Add(new SqlParameter("@OrderLineGuid", SqlDbType.UniqueIdentifier))
                     .Direction = ParameterDirection.Output;
                command.Parameters
                    .Add(new SqlParameter("@OrderLineTrxStatusLookupCode", SqlDbType.NVarChar, 20))
                     .Direction = ParameterDirection.Output;
                command.Parameters
                    .Add(new SqlParameter("@ActualQuantity", SqlDbType.Int, 4))
                     .Direction = ParameterDirection.Output;
                command.Parameters
                   .Add(new SqlParameter("@SellerCode", SqlDbType.NVarChar, 3))
                   .Value = sellerCode;
                var reader = command.ExecuteReader();

                if(getNewRowData==true)
                {
                    availabilityData = AvailabilityDataService.GetAvailabilityDataFromReader(reader, pricingShipWeekCode: shipWeekCode, sellerCode: sellerCode);
                }

                Status.Success = true;
                Status.StatusMessage = "Availability Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }

            return availabilityData;
        }

        public class GrowerHistoryData
        {

            public List<GrowerOrderHistory> GrowerOrderHistoryList { get; set; }

        }

        public GrowerHistoryData GetGrowerOrderHistoryData(Guid growerOrderGuid)
        {
            GrowerHistoryData growerOrderHistoryData = new GrowerHistoryData();


            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerOrderHistoryDataGet]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerOrderGuid;



                var reader = command.ExecuteReader();

                var context = new Context();

                
                var growerOrderHistoryService = context.Get<GrowerOrderHistoryTableService>();
                growerOrderHistoryData.GrowerOrderHistoryList = growerOrderHistoryService.GetAllFromReader(reader);


              

                Status.Success = true;
                Status.StatusMessage = "History Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }


            
            return growerOrderHistoryData;
        }

        public bool GrowerOrderHistoryAdd(string usercode, Guid growerOrderGuid, string comment, bool isInternal, Guid logTypeLookupGuid, Guid personGuid, DateTime eventDate)
        {

            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerOrderHistoryAdd]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@UserCode", SqlDbType.NChar, 56))
                    .Value = usercode;
                command.Parameters
                    .Add(new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerOrderGuid;
                command.Parameters
                    .Add(new SqlParameter("@Comment", SqlDbType.NVarChar, 2000))
                    .Value = comment;
                command.Parameters
                    .Add(new SqlParameter("@IsInternal", SqlDbType.Bit, 1))
                    .Value = isInternal;
                command.Parameters
                    .Add(new SqlParameter("@PersonGuid", SqlDbType.UniqueIdentifier))
                    .Value = personGuid;
                command.Parameters
                    .Add(new SqlParameter("@EventDate", SqlDbType.DateTime, 8))
                    .Value = eventDate;
                command.Parameters
                    .Add(new SqlParameter("@LogTypeLookupGuid", SqlDbType.UniqueIdentifier))
                    .Value = logTypeLookupGuid;

               

                var reader = command.ExecuteReader();

                var context = new Context();

                reader.Close();



                Status.Success = true;
                Status.StatusMessage = "History Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }


            return Status.Success;
        }
        public List<GrowerWeekSummary> GrowerGetTotalsPerWeek(Guid userGuid, string productFormCategoryCode, string programTypeCode)
        {
            List<GrowerWeekSummary> list = new List<GrowerWeekSummary>();

            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerGetTotalsPerWeek]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = userGuid;
                command.Parameters
                    .Add(new SqlParameter("@ProductFormCategoryCode", SqlDbType.NVarChar, 50))
                    .Value = productFormCategoryCode;
                command.Parameters
                    .Add(new SqlParameter("@ProgramTypeCode", SqlDbType.NVarChar, 50))
                    .Value = programTypeCode;

                var reader = command.ExecuteReader();

                var context = new Context();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var item = new GrowerWeekSummary();
                        item.Year = (int)reader["Year"];
                        item.Week = (int)reader["Week"];
                        item.ShipWeekCode = (string)reader["ShipWeekCode"];
                        item.Carted = (int)reader["Carted"];
                        item.Uncarted = (int)reader["Uncarted"];
                        item.Total = item.Carted + item.Uncarted;
                        list.Add(item);
                    }
                }
                reader.Close();

                Status.Success = true;
                Status.StatusMessage = "Totals Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }
            return list;
        }

        public GrowerOrderSummary GetSummaryByGuid(Guid orderGuid )
        {
            var growerOrderSummary = new GrowerOrderSummary();
            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("Select Guid,OrderNo,ShipWeekGuid,CustomerPoNo from GrowerOrder where Guid = '" + orderGuid + "'", connection);

                command.CommandType = CommandType.Text;
               

                var reader = command.ExecuteReader();

                var context = new Context();
                reader.Read();
                
                if (reader.HasRows)
                {

                    
                    growerOrderSummary.GrowerOrderGuid = (Guid)reader["Guid"];
                    growerOrderSummary.OrderNo = (string)reader["OrderNo"];
                    growerOrderSummary.CustomerPoNo = (string)reader["CustomerPoNo"];
                    growerOrderSummary.ShipWeekGuid = (Guid)reader["ShipWeekGuid"];
                    growerOrderSummary.QtyOrdered = 0;               
                      
                }
                reader.Close();

              

                Status.Success = true;
                Status.StatusMessage = "Order Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }


            return growerOrderSummary;
       
    }
        public List<StatusTotal> GrowerGetTotals(Guid userGuid,string productFormCategoryCode,string programTypeCode  )
        {
            List<StatusTotal> statusTotalList = new List<StatusTotal>();

            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerGetTotals]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = userGuid;
                command.Parameters
                    .Add(new SqlParameter("@ProductFormCategoryCode", SqlDbType.NVarChar,50))
                    .Value = productFormCategoryCode;
                command.Parameters
                    .Add(new SqlParameter("@ProgramTypeCode", SqlDbType.NVarChar,50))
                    .Value = programTypeCode;

                var reader = command.ExecuteReader();

                var context = new Context();

                
                if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var oStatusTotal = new StatusTotal();
                            oStatusTotal.Status = (string)reader["Status"];
                            oStatusTotal.Qty = (int)reader["QtyOrdered"];
                            statusTotalList.Add(oStatusTotal);
                        }
                    }
                reader.Close();

              

                Status.Success = true;
                Status.StatusMessage = "Totals Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }


            return statusTotalList;
        }

        public List<StatusTotal> GrowerOrderGetTotals(Guid growerOrderGuid)
        {
            List<StatusTotal> statusTotalList = new List<StatusTotal>();

            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerOrderGetTotals]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerOrderGuid;

                var reader = command.ExecuteReader();

                var context = new Context();

                
                if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var oStatusTotal = new StatusTotal();
                            oStatusTotal.Status = (string)reader["Status"];
                            oStatusTotal.Qty = (int)reader["QtyOrdered"];
                            statusTotalList.Add(oStatusTotal);
                        }
                    }
                reader.Close();

              

                Status.Success = true;
                Status.StatusMessage = "Totals Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }


            return statusTotalList;
        }

       

        public Guid GrowerOrderGetGuid( string shipWeekCode, string programTypeCode, string productFormCategoryCode, bool placed)
        {
            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            Guid growerOrderGuid = new Guid();
            try
            {

                var command = new SqlCommand("[dbo].[GrowerOrderGetGuid]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@ShipWeekCode", SqlDbType.NVarChar, 10))
                    .Value = shipWeekCode;
                command.Parameters
                    .Add(new SqlParameter("@ProgramTypeCode", SqlDbType.NVarChar, 10))
                    .Value = programTypeCode;
                command.Parameters
                    .Add(new SqlParameter("@ProductFormCategoryCode", SqlDbType.NVarChar, 10))
                    .Value = productFormCategoryCode;
                command.Parameters
                    .Add(new SqlParameter("@Placed", SqlDbType.Bit, 1))
                    .Value = placed;
           
                
                
                command.Parameters
                    .Add(new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier))
                    .Direction = ParameterDirection.Output;

                var reader = command.ExecuteReader();
                growerOrderGuid = (Guid)command.Parameters["@GrowerOrderGuid"].Value;

                Status.Success = true;
                Status.StatusMessage = "Guid Returned";
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
               
            }
            finally
            {
                connection.Close();
            }

            return growerOrderGuid;
        }
        public void GrowerOrderRecalcFees(Guid growerOrderGuid, Boolean recalcSupplierFees,Boolean reprice)
        {

            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {

                var command = new SqlCommand("[dbo].[GrowerOrderRecalcFees]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerOrderGuid;
                command.Parameters
                    .Add(new SqlParameter("@IncludeSupplierOrderFees", SqlDbType.Bit, 1))
                    .Value = recalcSupplierFees;
                command.Parameters
                    .Add(new SqlParameter("@Reprice", SqlDbType.Bit, 1))
                    .Value = reprice;


                var response = (int)command.ExecuteNonQuery();
                Status.Success = true;
                Status.StatusMessage = "Fees Calculated";
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;

            }
            finally
            {
                connection.Close();
            }


        }

        public List<GrowerOrderSummary> GrowerOrderGetGrowerShipWeekOrders( string shipWeekCode, string sellerCode)
        {
            sellerCode = "GFB";
            var list = new List<GrowerOrderSummary>();
            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerOrderGetGrowerShipWeekOrders]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@ShipWeekCode", SqlDbType.NVarChar, 6))
                    .Value = shipWeekCode;
                command.Parameters
                    .Add(new SqlParameter("@SellerCode", SqlDbType.NVarChar, 10))
                    .Value = sellerCode;



                var reader = command.ExecuteReader();

                var context = new Context();

                
                if (reader != null && reader.HasRows)
                {
                    while (reader.Read())
                    {

                        var orderSummary = new GrowerOrderSummary();
                        orderSummary.GrowerOrderGuid = new Guid(reader["GrowerOrderGuid"].ToString());
                        orderSummary.ShipWeekGuid = new Guid(reader["ShipWeekGuid"].ToString());
                        orderSummary.OrderNo = reader["OrderNo"].ToString();
                        orderSummary.CustomerPoNo = reader["CustomerPoNo"].ToString();
                        orderSummary.ShipToCity = reader["ShipToCity"].ToString();
                        orderSummary.QtyOrdered = Convert.ToInt32(reader["QtyOrderedCount"].ToString());

                        list.Add(orderSummary);
                    }
                    reader.Close();
                }

                Status.Success = true;
                Status.StatusMessage = "Orders Loaded";
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }

            return list;
        }
       

        public GrowerOrderData GetGrowerOrderDetailData(Guid growerOrderGuid, bool includePriceData)
        {
            GrowerOrderData growerOrderData = new GrowerOrderData();

            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerOrderDetailDataGet]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@UserCode", SqlDbType.NVarChar, 56))
                    .Value = null;
                command.Parameters
                    .Add(new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerOrderGuid;
                command.Parameters
                    .Add(new SqlParameter("@IncludePriceData", SqlDbType.Bit, 1))
                    .Value = includePriceData;


                var currentRecordSetName  ="";
                var reader = command.ExecuteReader();

                var context = new Context();
                
               
                //var growerOrder = new DataObjectLibrary.GrowerOrder();
                //if (reader != null && reader.HasRows)
                //{

                //    while (reader.Read())
                //    {
            //Id,
            //Guid,
            //DateDeactivated,
            //GrowerGuid,
            //ShipWeekGuid,
            //OrderNo,
            //Description,
            //CustomerPoNo,
            //OrderTypeLookupGuid,
            //ProductFormCategoryGuid,
            //ProgramTypeGuid,
            //ShipToAddressGuid,
            //PaymentTypeLookupGuid,
            //GrowerOrderStatusLookupGuid,
            //DateEntered,
            //GrowerCreditCardGuid,
            //PersonGuid,
            //PromotionalCode,
            //NavigationProgramTypeCode,
            //NavigationProductFormCategoryCode,

                //        growerOrder.Id = (long)reader.GetInt64(0);
                //        growerOrder.Guid =(Guid)reader.GetGuid(1);
                //        //growerOrder.DateDeactivated = (DateTime)reader.GetDateTime(2);
                //        growerOrder.GrowerGuid = (Guid)reader.GetGuid(3);
                //        growerOrder.ShipWeekGuid = (Guid)reader.GetGuid(4);
                //        growerOrder.OrderNo = (string)reader.GetString(5);
                //        growerOrder.Description = (string)reader.GetString(6);
                //        growerOrder.CustomerPoNo = (string)reader.GetString(7);
                //        growerOrder.OrderTypeLookupGuid = (Guid)reader.GetGuid(8);
                //        growerOrder.ProductFormCategoryGuid = (Guid)reader.GetGuid(9);
                //        growerOrder.ProgramTypeGuid = (Guid)reader.GetGuid(10);
                //        growerOrder.ShipToAddressGuid = (Guid)reader.GetGuid(11);
                //        growerOrder.PaymentTypeLookupGuid = (Guid)reader.GetGuid(12);
                //        growerOrder.GrowerOrderStatusLookupGuid = (Guid)reader.GetGuid(13);
                //        growerOrder.DateEntered =  (DateTime)reader.GetDateTime(14);
                //        growerOrder.GrowerCreditCardGuid = (Guid)reader.GetGuid(15);
                //        growerOrder.PersonGuid = (Guid)reader.GetGuid(16);
                //        growerOrder.PromotionalCode =  (string)reader.GetString(17);
                //        //growerOrder.NavigationProgramTypeCode = (string)reader.GetString(18);
                //        //growerOrder.NavigationProductFormCategoryCode = (string)reader.GetString(19);

                      

                //    }
             
                //}
              
                currentRecordSetName = "GrowerOrder";
                var growerOrderTableService = context.Get<GrowerOrderTableService>();
                var growerOrderList = growerOrderTableService.GetAllFromReader(reader);
                reader.NextResult();

                currentRecordSetName = "Grower";
                var growerTableService = context.Get<GrowerTableService>();
                growerTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "ShipWeek";
                var shipWeekTableService = context.Get<ShipWeekTableService>();
                shipWeekTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "SupplierOrder";
                var supplierOrderTableService = context.Get<SupplierOrderTableService>();
                var supplierOrderList = supplierOrderTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "Supplier";
                var supplierTableService = context.Get<SupplierTableService>();
                supplierTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "OrderLine";
                var orderLineTableService = context.Get<OrderLineTableService>();
                var orderLineList = orderLineTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "Product";
                var productTableService = context.Get<ProductTableService>();
                var productList = productTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "ProductFormCategory";
                var productFormCategoryTableService = context.Get<ProductFormCategoryTableService>();
                productFormCategoryTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "ProductForm";
                var productFormTableService = context.Get<ProductFormTableService>();
                productFormTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "Variety";
                var varietyTableService = context.Get<VarietyTableService>();
                varietyTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "Program";
                var programTableService = context.Get<ProgramTableService>();
                programTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "GrowerAddress";
                var growerAddressTableService = context.Get<GrowerAddressTableService>();
                var growerAddressList = growerAddressTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "Address";
                var addressTableService = context.Get<AddressTableService>();
                addressTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "State";
                var stateTableService = context.Get<StateTableService>();
                stateTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "Country";
                var countryTableService = context.Get<CountryTableService>();
                countryTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "GrowerCreditCard";
                var growerCreditCardTableService = context.Get<GrowerCreditCardTableService>();
                var growerCreditCardList = growerCreditCardTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "ProductGrowerSeasonPriceView";
                var productGrowerSeasonPriceViewTableService =
                    context.Get<ProductGrowerSeasonPriceViewService>();
                growerOrderData.PriceList = productGrowerSeasonPriceViewTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "Price";
                var priceTableService = context.Get<PriceTableService>();
                priceTableService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "ProgramTagRatioView";
                var programTagRatioViewService = context.Get<ProgramTagRatioViewService>();
                var programTagRatioList = programTagRatioViewService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "ProgramShipMethodView";
                var programShipMethodViewService = context.Get<ProgramShipMethodViewService>();
                var programShipMethodList = programShipMethodViewService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "ReportedAvailability";
                var reportedAvailabilityService = context.Get<ReportedAvailabilityTableService>();
                var reportedAvailabilityList = reportedAvailabilityService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "ProgramSeason";
                var programSeasonService = context.Get<ProgramSeasonTableService>();
                growerOrderData.ProgramSeasonList = programSeasonService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "Person";
                var personService = context.Get<PersonTableService>();
                personService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "SupplierOrderFee";
                var supplierOrderFeeService = context.Get<SupplierOrderFeeTableService>();
                var supplierOrderFeeList = supplierOrderFeeService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "GrowerOrderFee";
                var growerOrderFeeService = context.Get<GrowerOrderFeeTableService>();
                var growerOrderFeeList = growerOrderFeeService.GetAllFromReader(reader);

                reader.NextResult();

                currentRecordSetName = "EventLog";
                var growerOrderHistoryService = context.Get<GrowerOrderHistoryTableService>();
                growerOrderData.GrowerOrderHistoryList = growerOrderHistoryService.GetAllFromReader(reader);

                // Done reading.

                currentRecordSetName = "";

                var lookupTableService = context.Get<LookupTableService>();
                foreach (var programTagRatio in programTagRatioList)
                {
                    //TODO: This should be handled by LazyLoad, but it's not supported for Views. It should be!
                    var program = programTableService.GetByGuid(programTagRatio.ProgramGuid);

                    var tagRatioLookup = lookupTableService.GetByGuid(programTagRatio.TagRatioLookupGuid);
                    program.TagRatioLookupList.Add(tagRatioLookup);

                    if (programTagRatio.IsDefault)
                        program.TagRatioDefault = tagRatioLookup;
                }

                foreach (var programShipMethod in programShipMethodList)
                {
                    //TODO: This should be handled by LazyLoad, but it's not supported for Views. It should be!
                    var program = programTableService.GetByGuid(programShipMethod.ProgramGuid);

                    var shipMethodLookup = lookupTableService.GetByGuid(programShipMethod.ShipMethodLookupGuid);
                    program.ShownShipMethodList.Add(shipMethodLookup);

                    if (programShipMethod.IsDefault)
                        program.ShipMethodDefault = shipMethodLookup;
                }

                var supplierOrderDictionary = new Dictionary<Guid, DataObjectLibrary.SupplierOrder>();

                foreach (var orderLine in orderLineList)
                {
                    DataObjectLibrary.SupplierOrder supplierOrder;
                    if (!supplierOrderDictionary.ContainsKey(orderLine.SupplierOrderGuid))
                    {
                        supplierOrder = orderLine.SupplierOrder;
                        supplierOrderDictionary.Add(orderLine.SupplierOrderGuid, supplierOrder);

                        // Set the program for each SupplierOrder. 
                        // There is no direct reference to this, but it is nevertheless true that there is always one Program per SupplierOrder.
                        supplierOrder.Program = orderLine.Product.Program;

                        supplierOrder.SetLazyOrderLineList(new Lazy<List<DataObjectLibrary.OrderLine>>(() => new List<DataObjectLibrary.OrderLine>()));
                    }
                    else
                    {
                        bool found = supplierOrderDictionary.TryGetValue(orderLine.SupplierOrderGuid,
                                                                            out supplierOrder);

                        if (!found)
                        {
                            throw new ApplicationException("SuppierOrder not found in the dictionary.");
                        }
                    }
                    supplierOrder.OrderLineList.Add(orderLine);
                }

                if (growerOrderList.Count != 1)
                {
                    throw new DataException("Expected a single GrowerOrder to be returned.");
                }
                else
                {
                    growerOrderData.GrowerOrder = growerOrderList[0];
                    growerOrderData.GrowerOrder.Grower.SetLazyGrowerAddressList(
                        new Lazy<List<GrowerAddress>>(() => growerAddressList));
                    growerOrderData.GrowerOrder.Grower.SetLazyGrowerCreditCardList(
                        new Lazy<List<GrowerCreditCard>>(() => growerCreditCardList));
                    growerOrderData.GrowerOrder.SetLazySupplierOrderList(
                        new Lazy<List<DataObjectLibrary.SupplierOrder>>(() => supplierOrderDictionary.Values.ToList()));
                }

                foreach (var reportedAvailability in reportedAvailabilityList)
                {
                    var product = productTableService.GetByGuid(reportedAvailability.ProductGuid);

                    var productReportedAvailabilityList = new List<ReportedAvailability> { reportedAvailability };

                    // Set the product's reported availability list to include one item (the availability for the current week).
                    product.SetLazyReportedAvailabilityList(
                        new Lazy<List<ReportedAvailability>>(() => productReportedAvailabilityList));

                    // Get the list once to set the "IsLoaded" flag to true. The business object will check for this to avoid loading *ALL* availability!
                    var list = product.ReportedAvailabilityList;
                }

                //TODO: This should not be necessary; LazyLoad should be set up automatically.
                foreach (var productPrice in growerOrderData.PriceList)
                {
                    var price = priceTableService.GetByGuid(productPrice.PriceGuid);
                    productPrice.SetLazyPrice(new Lazy<Price>(() => price));

                    var product = productTableService.GetByGuid(productPrice.ProductGuid);
                    productPrice.SetLazyProduct(new Lazy<DataObjectLibrary.Product>(() => product));
                }

                foreach (var programSeason in growerOrderData.ProgramSeasonList)
                {
                    programSeason.Program.CurrentProgramSeason = programSeason;
                }

                //TODO: Verify that code exists to prevent a lazy load call to get all supplier orders for a grower order.
                foreach (var supplierOrder in supplierOrderList)
                {
                    supplierOrder.SetLazySupplierOrderFeeList(
                        new Lazy<List<DataObjectLibrary.SupplierOrderFee>>(() => new List<DataObjectLibrary.SupplierOrderFee>()));
                }

                foreach (var supplierOrderFee in supplierOrderFeeList)
                {
                    supplierOrderFee.SupplierOrder.SupplierOrderFeeList.Add(supplierOrderFee);
                }

                growerOrderData.GrowerOrderFeeList = new List<DataObjectLibrary.GrowerOrderFee>();
                foreach (var growerOrderFee in growerOrderFeeList)
                {
                    growerOrderFee.GrowerOrder.GrowerOrderFeeList.Add(growerOrderFee);
                    growerOrderData.GrowerOrderFeeList.Add(growerOrderFee);
                }
                reader.Close();
                 

                Status.Success = true;
                Status.StatusMessage = "Order Detail Loaded";
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }

            return growerOrderData;
        }



    }
}

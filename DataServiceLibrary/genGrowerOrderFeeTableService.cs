﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class GrowerOrderFeeTableService : TableService<GrowerOrderFee>
    {
        public GrowerOrderFeeTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override GrowerOrderFee DefaultInsert(GrowerOrderFee growerOrderFee, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [GrowerOrderFee]([Guid], [DateDeactivated], [GrowerOrderGuid], [FeeTypeLookupGuid], [Amount], [FeeStatXLookupGuid], [FeeDescription], [IsManual], ) VALUES (@Guid, @DateDeactivated, @GrowerOrderGuid, @FeeTypeLookupGuid, @Amount, @FeeStatXLookupGuid, @FeeDescription, @IsManual, );SELECT * FROM [GrowerOrderFee] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, growerOrderFee, transaction: transaction);
        }

        protected override GrowerOrderFee DefaultUpdate(GrowerOrderFee growerOrderFee, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [GrowerOrderFee] SET [DateDeactivated]=@DateDeactivated, [GrowerOrderGuid]=@GrowerOrderGuid, [FeeTypeLookupGuid]=@FeeTypeLookupGuid, [Amount]=@Amount, [FeeStatXLookupGuid]=@FeeStatXLookupGuid, [FeeDescription]=@FeeDescription, [IsManual]=@IsManual, WHERE [Guid]=@Guid;SELECT * FROM [GrowerOrderFee] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, growerOrderFee, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, GrowerOrderFee growerOrderFee)
        {
            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = growerOrderFee.Id;
            command.Parameters.Add(idParameter);

            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = growerOrderFee.Guid;
            command.Parameters.Add(guidParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = growerOrderFee.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

            var growerOrderGuidParameter = new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier, 16);
            growerOrderGuidParameter.IsNullable = false;
            growerOrderGuidParameter.Value = growerOrderFee.GrowerOrderGuid;
            command.Parameters.Add(growerOrderGuidParameter);

            var feeTypeLookupGuidParameter = new SqlParameter("@FeeTypeLookupGuid", SqlDbType.UniqueIdentifier, 16);
            feeTypeLookupGuidParameter.IsNullable = false;
            feeTypeLookupGuidParameter.Value = growerOrderFee.FeeTypeLookupGuid;
            command.Parameters.Add(feeTypeLookupGuidParameter);

            var amountParameter = new SqlParameter("@Amount", SqlDbType.Decimal, 5);
            amountParameter.IsNullable = false;
            amountParameter.Value = growerOrderFee.Amount;
            command.Parameters.Add(amountParameter);

            var feeStatXLookupGuidParameter = new SqlParameter("@FeeStatXLookupGuid", SqlDbType.UniqueIdentifier, 16);
            feeStatXLookupGuidParameter.IsNullable = true;
            feeStatXLookupGuidParameter.Value = growerOrderFee.FeeStatXLookupGuid ?? (object)DBNull.Value;
            command.Parameters.Add(feeStatXLookupGuidParameter);

            var feeDescriptionParameter = new SqlParameter("@FeeDescription", SqlDbType.NVarChar, 500);
            feeDescriptionParameter.IsNullable = false;
            growerOrderFee.FeeDescription = growerOrderFee.FeeDescription ?? "";
            growerOrderFee.FeeDescription = growerOrderFee.FeeDescription.Trim();
            feeDescriptionParameter.Value = growerOrderFee.FeeDescription;
            command.Parameters.Add(feeDescriptionParameter);

            var isManualParameter = new SqlParameter("@IsManual", SqlDbType.Bit, 1);
            isManualParameter.IsNullable = false;
            isManualParameter.Value = growerOrderFee.IsManual;
            command.Parameters.Add(isManualParameter);

        }

        protected override GrowerOrderFee DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var growerOrderFee = new GrowerOrderFee();

            string columnName = "";
            try
            {
                columnName = "Id";
                var id = reader[(int)GrowerOrderFee.ColumnEnum.Id] ?? long.MinValue;
                growerOrderFee.Id = (long)id;

                columnName = "Guid";
                var guid = reader[(int)GrowerOrderFee.ColumnEnum.Guid] ?? Guid.Empty;
                growerOrderFee.Guid = (Guid)guid;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)GrowerOrderFee.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                growerOrderFee.DateDeactivated = (DateTime?)dateDeactivated;

                columnName = "GrowerOrderGuid";
                var growerOrderGuid = reader[(int)GrowerOrderFee.ColumnEnum.GrowerOrderGuid] ?? Guid.Empty;
                growerOrderFee.GrowerOrderGuid = (Guid)growerOrderGuid;

                columnName = "FeeTypeLookupGuid";
                var feeTypeLookupGuid = reader[(int)GrowerOrderFee.ColumnEnum.FeeTypeLookupGuid] ?? Guid.Empty;
                growerOrderFee.FeeTypeLookupGuid = (Guid)feeTypeLookupGuid;

                columnName = "Amount";
                var amount = reader[(int)GrowerOrderFee.ColumnEnum.Amount] ?? Decimal.MinValue;
                growerOrderFee.Amount = (Decimal)amount;

                columnName = "FeeStatXLookupGuid";
                var feeStatXLookupGuid = reader[(int)GrowerOrderFee.ColumnEnum.FeeStatXLookupGuid];
                if (feeStatXLookupGuid == DBNull.Value) feeStatXLookupGuid = null;
                growerOrderFee.FeeStatXLookupGuid = (Guid?)feeStatXLookupGuid;

                columnName = "FeeDescription";
                var feeDescription = reader[(int)GrowerOrderFee.ColumnEnum.FeeDescription] ?? string.Empty;
                feeDescription = TrimString(feeDescription);
                growerOrderFee.FeeDescription = (string)feeDescription;

                columnName = "IsManual";
                var isManual = reader[(int)GrowerOrderFee.ColumnEnum.IsManual] ?? false;
                growerOrderFee.IsManual = (bool)isManual;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return growerOrderFee;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(GrowerOrderFee growerOrderFee)
        {
            SetUpGrowerOrderLazyLoad(growerOrderFee);
            SetUpFeeTypeLookupLazyLoad(growerOrderFee);
            SetUpFeeStatXLookupLazyLoad(growerOrderFee);
        }

        protected override void FixAnyLazyLoadedLists(GrowerOrderFee growerOrderFee, ListActionEnum listAction)
        {
            FixGrowerOrderList(growerOrderFee, listAction);
            FixFeeTypeLookupList(growerOrderFee, listAction);
            FixFeeStatXLookupList(growerOrderFee, listAction);
        }

        private void SetUpGrowerOrderLazyLoad(GrowerOrderFee growerOrderFee)
        {
            var growerOrderTableService = Context.Get<GrowerOrderTableService>();
            growerOrderFee.SetLazyGrowerOrder(new Lazy<GrowerOrder>(() => growerOrderTableService.GetByGuid(growerOrderFee.GrowerOrderGuid), false));
        }

        private void FixGrowerOrderList(GrowerOrderFee growerOrderFee, ListActionEnum listAction)
        {
            if (growerOrderFee.GrowerOrderIsLoaded)
            {
                FixLazyLoadedList(growerOrderFee.GrowerOrder.GrowerOrderFeeList, growerOrderFee, listAction);
            }
        }

        private void SetUpFeeTypeLookupLazyLoad(GrowerOrderFee growerOrderFee)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            growerOrderFee.SetLazyFeeTypeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(growerOrderFee.FeeTypeLookupGuid), false));
        }

        private void FixFeeTypeLookupList(GrowerOrderFee growerOrderFee, ListActionEnum listAction)
        {
            if (growerOrderFee.FeeTypeLookupIsLoaded)
            {
                FixLazyLoadedList(growerOrderFee.FeeTypeLookup.FeeTypeGrowerOrderFeeList, growerOrderFee, listAction);
            }
        }

        private void SetUpFeeStatXLookupLazyLoad(GrowerOrderFee growerOrderFee)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            growerOrderFee.SetLazyFeeStatXLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(growerOrderFee.FeeStatXLookupGuid), false));
        }

        private void FixFeeStatXLookupList(GrowerOrderFee growerOrderFee, ListActionEnum listAction)
        {
            if (growerOrderFee.FeeStatXLookupIsLoaded)
            {
                FixLazyLoadedList(growerOrderFee.FeeStatXLookup.FeeStatXGrowerOrderFeeList, growerOrderFee, listAction);
            }
        }

        #endregion
    }
}

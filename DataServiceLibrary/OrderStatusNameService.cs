﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataServiceLibrary
{
    public interface ILookupService
    {
        string GetCodeName(string code);
    }

    public class OrderStatusNameService : ILookupService
    {
        public string GetCodeName(string code)
        {
            try
            {
                var lookupService = new DataServiceLibrary.LookupTableService();  //Code/OrderLineStatus/Pending
                var statusLookup = lookupService.GetByCode(LookupTableValues.Code.OrderLineStatus.LookupValue, code);
                //var lookupList = lookupService.GetAll();
                //foreach (var lu in lookupList)
                //{
                //    if (lu.Code == code)
                //        return lu.Name;
                //}
                //return code;
                return statusLookup.Name;
            }
            catch
            {
                return code;
            }
        }
    }
}

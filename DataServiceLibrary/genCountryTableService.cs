﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class CountryTableService : TableService<Country>
    {
        public CountryTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override Country DefaultInsert(Country country, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [Country]([Guid], [DateDeactivated], [Code], [Name], ) VALUES (@Guid, @DateDeactivated, @Code, @Name, );SELECT * FROM [Country] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, country, transaction: transaction);
        }

        protected override Country DefaultUpdate(Country country, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [Country] SET [DateDeactivated]=@DateDeactivated, [Code]=@Code, [Name]=@Name, WHERE [Guid]=@Guid;SELECT * FROM [Country] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, country, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, Country country)
        {
            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = country.Id;
            command.Parameters.Add(idParameter);

            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = country.Guid;
            command.Parameters.Add(guidParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = country.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

            var codeParameter = new SqlParameter("@Code", SqlDbType.NChar, 10);
            codeParameter.IsNullable = false;
            country.Code = country.Code ?? "";
            country.Code = country.Code.Trim();
            codeParameter.Value = country.Code;
            command.Parameters.Add(codeParameter);

            var nameParameter = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            nameParameter.IsNullable = false;
            country.Name = country.Name ?? "";
            country.Name = country.Name.Trim();
            nameParameter.Value = country.Name;
            command.Parameters.Add(nameParameter);

        }

        protected override Country DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var country = new Country();

            string columnName = "";
            try
            {
                columnName = "Id";
                var id = reader[(int)Country.ColumnEnum.Id] ?? long.MinValue;
                country.Id = (long)id;

                columnName = "Guid";
                var guid = reader[(int)Country.ColumnEnum.Guid] ?? Guid.Empty;
                country.Guid = (Guid)guid;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)Country.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                country.DateDeactivated = (DateTime?)dateDeactivated;

                columnName = "Code";
                var code = reader[(int)Country.ColumnEnum.Code] ?? string.Empty;
                code = TrimString(code);
                country.Code = (string)code;

                columnName = "Name";
                var name = reader[(int)Country.ColumnEnum.Name] ?? string.Empty;
                name = TrimString(name);
                country.Name = (string)name;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return country;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(Country country)
        {
            SetUpPriceListLazyLoad(country);
            //SetUpProgramCountryListLazyLoad( country);
            SetUpStateListLazyLoad(country);
        }

        protected override void FixAnyLazyLoadedLists(Country country, ListActionEnum listAction)
        {
        }

        private void SetUpPriceListLazyLoad(Country country)
        {
            var priceTableService = Context.Get<PriceTableService>();
            country.SetLazyPriceList(new Lazy<List<Price>>(() => priceTableService.GetAllActiveByGuid(country.Guid, "CountryGuid"), false));
        }

        //private void SetUpProgramCountryListLazyLoad(Country country)
        //{
        //    var programCountryTableService = Context.Get<ProgramCountryTableService>();
        //    country.SetLazyProgramCountryList(new Lazy<List<ProgramCountry>>(() => programCountryTableService.GetAllActiveByGuid(country.Guid, "CountryGuid"), false));
        //}

        private void SetUpStateListLazyLoad(Country country)
        {
            var stateTableService = Context.Get<StateTableService>();
            country.SetLazyStateList(new Lazy<List<State>>(() => stateTableService.GetAllActiveByGuid(country.Guid, "CountryGuid"), false));
        }

        #endregion
    }
}

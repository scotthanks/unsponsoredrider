﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class GrowerAddressTableService : TableService<GrowerAddress>
    {
        public GrowerAddressTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override GrowerAddress DefaultInsert(GrowerAddress growerAddress, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [GrowerAddress]([Guid], [DateDeactivated], [GrowerGuid], [AddressGuid], [PhoneAreaCode], [Phone], [DefaultAddress], [SpecialInstructions],[SellerCustomerID], ) VALUES (@Guid, @DateDeactivated, @GrowerGuid, @AddressGuid, @PhoneAreaCode, @Phone, @DefaultAddress, @SpecialInstructions,@SellerCustomerID, );SELECT * FROM [GrowerAddress] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, growerAddress, transaction: transaction);
        }

        protected override GrowerAddress DefaultUpdate(GrowerAddress growerAddress, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [GrowerAddress] SET [DateDeactivated]=@DateDeactivated, [GrowerGuid]=@GrowerGuid, [AddressGuid]=@AddressGuid, [PhoneAreaCode]=@PhoneAreaCode, [Phone]=@Phone, [DefaultAddress]=@DefaultAddress, [SpecialInstructions]=@SpecialInstructions, WHERE [Guid]=@Guid;SELECT * FROM [GrowerAddress] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, growerAddress, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, GrowerAddress growerAddress)
        {
            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = growerAddress.Id;
            command.Parameters.Add(idParameter);

            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = growerAddress.Guid;
            command.Parameters.Add(guidParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = growerAddress.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

            var growerGuidParameter = new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier, 16);
            growerGuidParameter.IsNullable = false;
            growerGuidParameter.Value = growerAddress.GrowerGuid;
            command.Parameters.Add(growerGuidParameter);

            var addressGuidParameter = new SqlParameter("@AddressGuid", SqlDbType.UniqueIdentifier, 16);
            addressGuidParameter.IsNullable = false;
            addressGuidParameter.Value = growerAddress.AddressGuid;
            command.Parameters.Add(addressGuidParameter);

            var phoneAreaCodeParameter = new SqlParameter("@PhoneAreaCode", SqlDbType.Decimal, 5);
            phoneAreaCodeParameter.IsNullable = false;
            phoneAreaCodeParameter.Value = growerAddress.PhoneAreaCode;
            command.Parameters.Add(phoneAreaCodeParameter);

            var phoneParameter = new SqlParameter("@Phone", SqlDbType.Decimal, 5);
            phoneParameter.IsNullable = false;
            phoneParameter.Value = growerAddress.Phone;
            command.Parameters.Add(phoneParameter);

            var defaultAddressParameter = new SqlParameter("@DefaultAddress", SqlDbType.Bit, 1);
            defaultAddressParameter.IsNullable = false;
            defaultAddressParameter.Value = growerAddress.DefaultAddress;
            command.Parameters.Add(defaultAddressParameter);

            var specialInstructionsParameter = new SqlParameter("@SpecialInstructions", SqlDbType.VarChar, 200);
            specialInstructionsParameter.IsNullable = false;
            growerAddress.SpecialInstructions = growerAddress.SpecialInstructions ?? "";
            growerAddress.SpecialInstructions = growerAddress.SpecialInstructions.Trim();
            specialInstructionsParameter.Value = growerAddress.SpecialInstructions;
            command.Parameters.Add(specialInstructionsParameter);

            var sellerCustomerIDParamter = new SqlParameter("@SellerCustomerID", SqlDbType.VarChar, 20);
            sellerCustomerIDParamter.IsNullable = false;
            growerAddress.SellerCustomerID = growerAddress.SellerCustomerID ?? "";
            growerAddress.SellerCustomerID = growerAddress.SellerCustomerID.Trim();
            sellerCustomerIDParamter.Value = growerAddress.SellerCustomerID;
            command.Parameters.Add(sellerCustomerIDParamter);

        }

        protected override GrowerAddress DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var growerAddress = new GrowerAddress();

            string columnName = "";
            try
            {
                columnName = "Id";
                var id = reader[(int)GrowerAddress.ColumnEnum.Id] ?? long.MinValue;
                growerAddress.Id = (long)id;

                columnName = "Guid";
                var guid = reader[(int)GrowerAddress.ColumnEnum.Guid] ?? Guid.Empty;
                growerAddress.Guid = (Guid)guid;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)GrowerAddress.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                growerAddress.DateDeactivated = (DateTime?)dateDeactivated;

                columnName = "GrowerGuid";
                var growerGuid = reader[(int)GrowerAddress.ColumnEnum.GrowerGuid] ?? Guid.Empty;
                growerAddress.GrowerGuid = (Guid)growerGuid;

                columnName = "AddressGuid";
                var addressGuid = reader[(int)GrowerAddress.ColumnEnum.AddressGuid] ?? Guid.Empty;
                growerAddress.AddressGuid = (Guid)addressGuid;

                columnName = "PhoneAreaCode";
                var phoneAreaCode = reader[(int)GrowerAddress.ColumnEnum.PhoneAreaCode] ?? Decimal.MinValue;
                growerAddress.PhoneAreaCode = (Decimal)phoneAreaCode;

                columnName = "Phone";
                var phone = reader[(int)GrowerAddress.ColumnEnum.Phone] ?? Decimal.MinValue;
                growerAddress.Phone = (Decimal)phone;

                columnName = "DefaultAddress";
                var defaultAddress = reader[(int)GrowerAddress.ColumnEnum.DefaultAddress] ?? false;
                growerAddress.DefaultAddress = (bool)defaultAddress;

                columnName = "SpecialInstructions";
                var specialInstructions = reader[(int)GrowerAddress.ColumnEnum.SpecialInstructions] ?? string.Empty;
                specialInstructions = TrimString(specialInstructions);
                growerAddress.SpecialInstructions = (string)specialInstructions;

                columnName = "SellerCustomerID";
                var sellerCustomerID = reader[(int)GrowerAddress.ColumnEnum.SellerCustomerID] ?? string.Empty;
                sellerCustomerID = TrimString(sellerCustomerID);
                growerAddress.SellerCustomerID = (string)sellerCustomerID;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return growerAddress;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(GrowerAddress growerAddress)
        {
            SetUpGrowerLazyLoad(growerAddress);
            SetUpAddressLazyLoad(growerAddress);
        }

        protected override void FixAnyLazyLoadedLists(GrowerAddress growerAddress, ListActionEnum listAction)
        {
            FixGrowerList(growerAddress, listAction);
            FixAddressList(growerAddress, listAction);
        }

        private void SetUpGrowerLazyLoad(GrowerAddress growerAddress)
        {
            var growerTableService = Context.Get<GrowerTableService>();
            growerAddress.SetLazyGrower(new Lazy<Grower>(() => growerTableService.GetByGuid(growerAddress.GrowerGuid), false));
        }

        private void FixGrowerList(GrowerAddress growerAddress, ListActionEnum listAction)
        {
            if (growerAddress.GrowerIsLoaded)
            {
                FixLazyLoadedList(growerAddress.Grower.GrowerAddressList, growerAddress, listAction);
            }
        }

        private void SetUpAddressLazyLoad(GrowerAddress growerAddress)
        {
            var addressTableService = Context.Get<AddressTableService>();
            growerAddress.SetLazyAddress(new Lazy<Address>(() => addressTableService.GetByGuid(growerAddress.AddressGuid), false));
        }

        private void FixAddressList(GrowerAddress growerAddress, ListActionEnum listAction)
        {
            if (growerAddress.AddressIsLoaded)
            {
                FixLazyLoadedList(growerAddress.Address.GrowerAddressList, growerAddress, listAction);
            }
        }

        #endregion
    }
}

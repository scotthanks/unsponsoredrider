﻿using System;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;
using System.Collections.Generic;

using BusinessObjectsLibrary;

namespace DataServiceLibrary
{
    public partial class ReportDataService: DataServiceBaseNew
    {
        public ReportDataService(StatusObject status)
            : base(status)
        {

        }
        public List<ShipWeek2> GetNextShipWeekCodes(int weeks)
        {
            var shipWeekList = new List<ShipWeek2>();
            try
            {
                var connection = ConnectionServices.ConnectionToData;
                
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ReportGetNextShipWeeks]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                        .Add(new SqlParameter("@ShipWeekCodeStart", SqlDbType.VarChar, 100))
                        .Value = "";
                        command.Parameters
                        .Add(new SqlParameter("@Weeks", SqlDbType.Int))
                        .Value = weeks;


                        connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var item = new ShipWeek2();
                            item.ShipWeekCode = (string)reader["ShipWeekCode"];


                            shipWeekList.Add(item);
                        }
                        connection.Close();
                    }
                }
                Status.StatusMessage = "ShipWeek Data Returned";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ReportDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return shipWeekList;
        }


        public List<AvailabilityItemAcrossWeeks> GetCurrentAvailabilityReport()
        {
            var reportList = new List<AvailabilityItemAcrossWeeks>();
            try
            {
                var connection = ConnectionServices.ConnectionToData;
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ReportGetCurrentAvailability]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var item = new AvailabilityItemAcrossWeeks();
                            item.Program = (string)reader["Program"];
                            item.Variety = (string)reader["Variety"];
                            item.Species = (string)reader["Species"];
                            item.SupplierIdentifier = (string)reader["SupplierIdentifier"];
                            item.Week01Qty = (int)reader["1"];
                            item.Week02Qty = (int)reader["2"];
                            item.Week03Qty = (int)reader["3"];
                            item.Week04Qty = (int)reader["4"];
                            item.Week05Qty = (int)reader["5"];
                            item.Week06Qty = (int)reader["6"];
                            item.Week07Qty = (int)reader["7"];
                            item.Week08Qty = (int)reader["8"];
                            item.Week09Qty = (int)reader["9"];
                            item.Week10Qty = (int)reader["10"];
                            item.Week11Qty = (int)reader["11"];
                            item.Week12Qty = (int)reader["12"];
                            item.Week13Qty = (int)reader["13"];
                            item.Week14Qty = (int)reader["14"];
                            item.Week15Qty = (int)reader["15"];
                            item.Week16Qty = (int)reader["16"];
                            item.Week17Qty = (int)reader["17"];
                            item.Week18Qty = (int)reader["18"];
                            item.Week19Qty = (int)reader["19"];
                            item.Week20Qty = (int)reader["20"];
                            item.Week21Qty = (int)reader["21"];
                            item.Week22Qty = (int)reader["22"];
                            item.Week23Qty = (int)reader["23"];
                            item.Week24Qty = (int)reader["24"];
                            item.Week25Qty = (int)reader["25"];
                            item.Week26Qty = (int)reader["26"];
                            item.Week27Qty = (int)reader["27"];
                            item.Week28Qty = (int)reader["28"];
                            item.Week29Qty = (int)reader["29"];
                            item.Week30Qty = (int)reader["30"];
                            item.Week31Qty = (int)reader["31"];
                            item.Week32Qty = (int)reader["32"];
                            item.Week33Qty = (int)reader["33"];
                            item.Week34Qty = (int)reader["34"];
                            item.Week35Qty = (int)reader["35"];
                            item.Week36Qty = (int)reader["36"];
                            item.Week37Qty = (int)reader["37"];
                            item.Week38Qty = (int)reader["38"];
                            item.Week39Qty = (int)reader["39"];
                            item.Week40Qty = (int)reader["40"];
                            item.Week41Qty = (int)reader["41"];
                            item.Week42Qty = (int)reader["42"];
                            item.Week43Qty = (int)reader["43"];
                            item.Week44Qty = (int)reader["44"];
                            item.Week45Qty = (int)reader["45"];
                            item.Week46Qty = (int)reader["46"];
                            item.Week47Qty = (int)reader["47"];
                            item.Week48Qty = (int)reader["48"];
                            item.Week49Qty = (int)reader["49"];
                            item.Week50Qty = (int)reader["50"];
                            item.Week51Qty = (int)reader["51"];
                            item.Week52Qty = (int)reader["52"];
                            reportList.Add(item);
                        }
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Report Data Returned";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ReportDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return reportList;
        }

    }
}

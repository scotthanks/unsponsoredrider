﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public partial class LookupTableService
    {
        public List<Lookup> GetTopLevelLookups()
        {
            return GetByParentGuid(null);
        }

        private List<Lookup> GetByParentGuid(Guid? parentGuid)
        {
            return GetAllByField("ParentLookupGuid", null, "IS", excludeInactive: false);
        }

        public IEnumerable<Lookup> GetAllAndPreloadAllChildren()
        {
            var lookupList = GetAll().ToList();

            // Remove the lazy loading delegates. We don't need it because we are going to preload.
            // Replace each with a method that just returns an empty list.
            foreach (var lookup in lookupList)
            {
                lookup.SetLazyChildLookupList(new Lazy<List<Lookup>>(() => new List<Lookup>()));
            }

            foreach (var lookup in lookupList)
            {
                if (lookup.ParentLookupGuid != null)
                {
                    var list = lookup.ParentLookup.ChildLookupList;

                    lookup.ParentLookup.ChildLookupList.Add(lookup);
                }
            }

            return lookupList;
        }

        public new Lookup Update(Lookup lookup)
        {
            if (lookup.Path != lookup.CalculatedPath)
            {
                throw new NotImplementedException("It is not implemented to update a Lookup in such a way as to alter its path.");
            }

            return base.Insert(lookup);
        }

        public new Lookup Insert(Lookup lookup)
        {
            if (lookup.Guid == Guid.Empty)
            {
                lookup.Guid = Guid.NewGuid();
            }

            if (lookup.ParentLookupGuid != null)
            {
                lookup.ParentLookup = GetByGuid(lookup.ParentLookupGuid);                
            }

            lookup.Path = lookup.CalculatedPath;

            return base.Insert(lookup);
        }
    }
}

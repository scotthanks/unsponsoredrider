﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace BusinessObjectsLibrary
{
    public class ManagedPageContentType
    {
        public ManagedPageContentType() { }
        public ManagedPageContentType(string Path) {

            ////First Retrieve the Page Content for this Path
            //using (SqlConnection connection = DataAccessLibrary.ConnectionServices.ConnectionToContent)
            //{
            //    connection.Open();
            //    string sqlString = "SELECT * FROM PageContent WHERE IsLive = 1 AND Path = @Path";

            //    SqlCommand command = new SqlCommand(sqlString, connection);
            //    command.CommandType = CommandType.Text;

            //    command.Parameters.Add(new SqlParameter("@Path", Path));
            //    command.CommandTimeout = 5;
                
            //    SqlDataReader reader = command.ExecuteReader();
            //    if (reader.HasRows)
            //    {
            //        while (reader.Read())
            //        {
            //            this.Title = reader["Title"].ToString();
            //            this.Html = reader["Html"].ToString();
            //            this.Css = reader["Css"].ToString();
            //            this.CssPageGuid = (Guid)reader["CssPageGuid"];
                        
            //            this.IsLoaded = true;
            //        }
            //    }
            //    else
            //    {
            //        this.IsLoaded = false; ;
            //    }
            //    reader.Close();
            //};

            ////ToDo: Validate this.CssPageGuid before using it

            ////ToDo: Now we need to retrieve the CssPage itself
            //using (SqlConnection connection = DataAccessLibrary.ConnectionServices.ConnectionToContent)
            //{
            //    connection.Open();
            //    string sqlString = "SELECT * FROM CSSPages WHERE IsLive = 1 AND Guid = @Guid";

            //    SqlCommand command = new SqlCommand(sqlString, connection);
            //    command.CommandType = CommandType.Text;

            //    command.Parameters.Add(new SqlParameter("@Guid", this.CssPageGuid));
            //    command.CommandTimeout = 5;

            //    SqlDataReader reader = command.ExecuteReader();
            //    if (reader.HasRows)
            //    {
            //        while (reader.Read())
            //        {
            //            this.CssPage = reader["Css"].ToString();
            //        }
            //    }
            //    else
            //    {
            //        this.CssPage = string.Empty;
            //    }
            //    reader.Close();
            //}


        }
        
        public bool IsLoaded { get; set; }
        public string Title { get; set; }
        public string Html { get; set; }
        public string Css { get; set; }
        public string CssPage { get; set; }
        public string Javascript { get; set; }
        public string JavascriptOnReady { get; set; }

    }
}

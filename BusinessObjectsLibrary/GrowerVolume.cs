﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;
using Utility;

namespace BusinessObjectsLibrary
{
    public class GrowerVolume
    {
        public GrowerVolume()
        {
        }


        //public GrowerVolume(DataObjectLibrary.GrowerVolumeMax growerGrowerVolumeDataRow)
        //{
        //    SupplierGuid = growerGrowerVolumeDataRow.SupplierGuid;
        //    SupplierCode = growerGrowerVolumeDataRow.SupplierCode;
        //    SupplierName = growerGrowerVolumeDataRow.SupplierName;
        //    LevelNumber = growerGrowerVolumeDataRow.LevelNumber;
        //    RangeLow = growerGrowerVolumeDataRow.RangeLow;
        //    RangeHigh = growerGrowerVolumeDataRow.RangeHigh;
        //    RangeType = growerGrowerVolumeDataRow.RangeType;
        //    CurrentLevelType = growerGrowerVolumeDataRow.CurrentLevelType;
          
        //}
        public GrowerVolume(Guid supplierGuid,string supplierCode,string supplierName,int levelNumber,int rangeLow,int rangeHigh,string rangeType,string currentLevelType)
        {
            SupplierGuid = supplierGuid;
            SupplierCode = supplierCode;
            SupplierName =supplierName;
            LevelNumber = levelNumber;
            RangeLow = rangeLow;
            RangeHigh = rangeHigh;
            RangeType = rangeType;
            CurrentLevelType = currentLevelType;

        }

        public Guid SupplierGuid { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public int LevelNumber { get; set; }
        public int RangeLow { get; set; }
        public int RangeHigh { get; set; }
        public string RangeType { get; set; }
        public string CurrentLevelType { get; set; }
    }
}



 


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class Country
    {
        public Country(DataObjectLibrary.Country countryDataRow)
        {
            CountryName = countryDataRow.Name;
            CountryCode = countryDataRow.Code;
        }

        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary.Catalog
{
    public class ProductFormType
    {
        public Guid Guid { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public Guid CategoryGuid { get; set; }
        //public string CategoryCode { get; set; }
        //public string CategoryName { get; set; }
    }
}

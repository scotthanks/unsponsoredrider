﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;

namespace BusinessObjectsLibrary
{
    public class OrderSummary2
    {
       
        public OrderSummary2()
        {
          
        }

        public Guid OrderGuid { get; set; }
        public string OrderNo { get; set; }
        public ShipWeek ShipWeek { get; set; }
        public string ShipWeekString { get; set; }
        public string ProductFormCategoryCode { get; set; }
        public string ProductFormCategoryName { get; set; }
        public BusinessObjectsLibrary.ProductFormCategory ProductFormCategory { get; set; }
        //TODO: Make this a ProgramType business class instead?
        public string ProgramTypeCode { get; set; }
        public string ProgramTypeName { get; set; }             //Database name for ProgramCategoryName
        public int OrderQty { get; set; }

        public string GrowerCode { get; set; }
        public string GrowerName { get; set; }
        public string CustomerPoNo { get; set; }
        public string OrderDescription { get; set; }
        public string PromotionalCode { get; set; }
        public string PhoneNumber { get; set; }

        public string GrowerOrderStatusCode { get; set; }
        public string GrowerOrderStatusName { get; set; }
        public string LowestSupplierOrderStatusCode { get; set; }
        public string LowestOrderLineStatusCode { get; set; }

        public Person PersonWhoPlacedOrder { get; set; }
        public string InvoiceNo { get; set; }


    }
}

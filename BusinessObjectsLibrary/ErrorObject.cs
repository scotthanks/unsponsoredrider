﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class ErrorObject
    {
        public int ErrorNumber { get; set; }
        public string Error { get; set; }
        public string ErrorLocation { get; set; }
        public string ErrorMessageUser { get; set; }
        public string ErrorMessageSystem { get; set; }
    }
}

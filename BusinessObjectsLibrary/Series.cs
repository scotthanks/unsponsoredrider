﻿using System;


namespace BusinessObjectsLibrary
{
    public class Series
    {

        public Series()
        {
        }

        public Series(DataObjectLibrary.Series series)
        {
            SeriesGuid = series.Guid;
            Code = series.Code;
            Name = series.Name;
            SpeciesGuid = series.SpeciesGuid;
            DescriptionHTMLGuid = series.DescriptionHTMLGUID;
            CultureLibraryLink = series.CultureLibraryLink;
        }

        public Guid SeriesGuid { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public Guid SpeciesGuid { get; set; }
        public Guid? DescriptionHTMLGuid { get; set; }
        public string CultureLibraryLink { get; set; }
                


    }
}

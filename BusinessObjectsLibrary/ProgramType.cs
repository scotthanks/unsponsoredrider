﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace BusinessObjectsLibrary
{
    public class ProgramType : IComparable<ProgramType>
    {
        public ProgramType()
        {
            ActiveProductForms = new List<ProductFormCategory>();
        }

        public ProgramType(DataObjectLibrary.ProgramType programType)
            :this()
        {
            Guid = programType.Guid;
            Code = programType.Code;
            Name = programType.Name;
        }

        public Guid Guid { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public List<ProductFormCategory> ActiveProductForms { get; set; }

        public override string ToString()
        {
            var result = new StringBuilder();

            result.AppendFormat("Code={0}", Code);
            result.Append(", ");
            result.AppendFormat("Name={0}", Name);

            return result.ToString();
        }

        public int CompareTo(ProgramType other)
        {
            return System.String.Compare(this.Name, other.Name, System.StringComparison.Ordinal);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class Catalogue
    {
        public Catalogue() { }

        public Guid Guid { get; set; }
        public bool IsLoaded { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public Guid[] FormsAvailable { get; set; }      //Not Populated till Data Available
    }
}

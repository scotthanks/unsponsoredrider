﻿using System;


namespace BusinessObjectsLibrary
{
    public class ShipWeek2
    {

        public ShipWeek2()
        {
        }

        public Guid Guid { get; set; }
        public int Year { get; set; }
        public int Week { get; set; }
        public int ContinuousWeekNumber { get; set; }
        public DateTime MondayDate { get; set; }
        public string FiscalYear { get; set; }
        public string ShipWeekCode { get; set; }

    }
}

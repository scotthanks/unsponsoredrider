﻿using System;


namespace BusinessObjectsLibrary
{
    public class AvailabilityItemAcrossWeeks
    {


        public string B2BStatus { get; set; }
        public String StatusMessage { get; set; }
        public String Program { get; set; }
        public String Species { get; set; }
        public String Variety { get; set; }
        public String SupplierIdentifier { get; set; }
        public int Week01Qty { get; set; }
        public int Week02Qty { get; set; }
        public int Week03Qty { get; set; }
        public int Week04Qty { get; set; }
        public int Week05Qty { get; set; }
        public int Week06Qty { get; set; }
        public int Week07Qty { get; set; }
        public int Week08Qty { get; set; }
        public int Week09Qty { get; set; }
        public int Week10Qty { get; set; }
        public int Week11Qty { get; set; }
        public int Week12Qty { get; set; }
        public int Week13Qty { get; set; }
        public int Week14Qty { get; set; }
        public int Week15Qty { get; set; }
        public int Week16Qty { get; set; }
        public int Week17Qty { get; set; }
        public int Week18Qty { get; set; }
        public int Week19Qty { get; set; }
        public int Week20Qty { get; set; }
        public int Week21Qty { get; set; }
        public int Week22Qty { get; set; }
        public int Week23Qty { get; set; }
        public int Week24Qty { get; set; }
        public int Week25Qty { get; set; }
        public int Week26Qty { get; set; }
        public int Week27Qty { get; set; }
        public int Week28Qty { get; set; }
        public int Week29Qty { get; set; }
        public int Week30Qty { get; set; }
        public int Week31Qty { get; set; }
        public int Week32Qty { get; set; }
        public int Week33Qty { get; set; }
        public int Week34Qty { get; set; }
        public int Week35Qty { get; set; }
        public int Week36Qty { get; set; }
        public int Week37Qty { get; set; }
        public int Week38Qty { get; set; }
        public int Week39Qty { get; set; }
        public int Week40Qty { get; set; }
        public int Week41Qty { get; set; }
        public int Week42Qty { get; set; }
        public int Week43Qty { get; set; }
        public int Week44Qty { get; set; }
        public int Week45Qty { get; set; }
        public int Week46Qty { get; set; }
        public int Week47Qty { get; set; }
        public int Week48Qty { get; set; }
        public int Week49Qty { get; set; }
        public int Week50Qty { get; set; }
        public int Week51Qty { get; set; }
        public int Week52Qty { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObjectsLibrary
{
    public class ProductFormCategory : ProductFormCategoryBase
    {
        public ProductFormCategory()
        {    
            this.ProductFormList = new List<ProductForm>();
        }

        public ProductFormCategory(DataObjectLibrary.ProductFormCategory tableRow)
            :this()
        {
            this.IsLoaded = true;
            this.Guid = tableRow.Guid;
            this.Code = tableRow.Code;
            this.Name = tableRow.Name;
            this.IsRooted = tableRow.IsRooted;
            this.IsActive = tableRow.IsActive;
            this.AvailabilityWeekCalculationMethod = tableRow.AvailabilityWeekCalcMethodLookup.Code;
        }
    }
}

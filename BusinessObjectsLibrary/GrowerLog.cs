﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class GrowerLog
    {
        public GrowerLog(DataObjectLibrary.EventLog eventLog)
        {
            Guid = eventLog.Guid;
            DateTime = eventLog.EventTime;
            LogTypeCode = eventLog.LogTypeLookup.Code;
            Text = eventLog.Comment;
        }

        public Guid Guid { get; set; }
        public DateTime DateTime { get; set; }
        public Guid GrowerGuid { get; set; }
        public Guid GrowerOrderGuid { get; set; }
        public Guid SupplierOrderGuid { get; set; }
        public Guid OrderLineGuid { get; set; }
        public string LogTypeCode { get; set; }
        public string Text { get; set; }
        public bool IsInternal { get; set; }
    }
}

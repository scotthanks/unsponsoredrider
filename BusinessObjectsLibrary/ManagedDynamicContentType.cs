﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class ManagedDynamicContentType
    {
        public bool IsLoaded { get; set; }
        public Guid Guid { get; set; }
        public string Html {get; set; }

    }
}

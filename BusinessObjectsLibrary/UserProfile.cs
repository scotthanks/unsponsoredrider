﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;

namespace BusinessObjectsLibrary
{
    public class UserProfile
    {
        //public UserProfile(DataObjectLibrary.UserProfile userProfileDataRow)
        //{
        //    UserId = userProfileDataRow.UserId;
        //    UserName = userProfileDataRow.UserName;
        //    EmailAddress = userProfileDataRow.EmailAddress;
        //    UserGuid = userProfileDataRow.UserGuid;
        //    GrowerGuid = userProfileDataRow.GrowerGuid;

        //}
        public UserProfile(int userId, string userName, string emailAddress, Guid userGuid, Guid growerGuid)
        {
            UserId = userId;
            UserName = userName;
            EmailAddress = emailAddress;
            UserGuid = userGuid;
            GrowerGuid = growerGuid;

        }
        
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public Guid UserGuid { get; set; }
        public Guid GrowerGuid { get; set; }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class SearchResultTemplateType
    {

        public bool IsLoaded { get; set; }
        public Guid Guid { get; set; }
        public bool IsLive { get; set; }
        public string Type { get; set; }
        public string Html { get; set; }
        public string Css { get; set; }
        public string Javascript { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }

    }
}

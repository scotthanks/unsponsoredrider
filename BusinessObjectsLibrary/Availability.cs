﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;

namespace BusinessObjectsLibrary
{
    public class Availability
    {
        public Availability()
        {
        }
       

        public List<ShipWeek> ShipWeekList { get; set; } 

        public List<Product> ProductList { get; set; }
    }
}

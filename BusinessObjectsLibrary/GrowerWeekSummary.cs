﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class GrowerWeekSummary
    {
        public int Year { get; set; }
        public int Week { get; set; }
        public string ShipWeekCode { get; set; }
        public int Carted { get; set; }
        public int Uncarted { get; set; }
        public int Total { get; set; }
    }
}

﻿using DataObjectLibrary;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObjectsLibrary
{
    public class GrowerOrder
    {
        public Guid OrderGuid { get; set; }
        public string OrderNo { get; set; }
        public DateTime DateEntered { get; set; }
        public string CustomerPoNo { get; set; }
        public string OrderDescription { get; set; }
        public string PromotionalCode { get; set; }
        public string OrderType { get; set; }
        public string ProgramTypeCode { get; set; }
        public string ProgramTypeName { get; set; }
        public string ProductFormCategoryName { get; set; }
        public string ProductFormCategoryCode { get; set; }
        public Guid GrowerCreditCardGuid { get; set; }
        public string NavigationProgramTypeCode { get; set; }
        public string NavigationProductFormCategoryCode { get; set; }

        public ShipWeek ShipWeek { get; set; }

        public Guid ShipWeekGuid { get; set; }

        public Grower Grower { get; set; }

        public Lookup PaymentType { get; set; }
        public CreditCard PaymentCreditCard { get; set; }
        public Guid? ShipToAddressGuid { get; set; }
        public Lookup GrowerOrderStatus { get; set; }

        public Person PersonWhoPlacedOrder { get; set; }

        public List<SupplierOrder> SupplierOrders;

        public List<GrowerOrderHistoryEvent> GrowerOrderHistoryEvents;
        public List<GrowerOrderFee> GrowerOrderFeeList;

        public GrowerOrder(bool bNew)
        { }
        public GrowerOrder()
        {
            SupplierOrders = new List<SupplierOrder>();
            GrowerOrderHistoryEvents = new List<GrowerOrderHistoryEvent>();
        }

        //ToDo: Rick fix or remove this constructor
        public GrowerOrder(DataObjectLibrary.GrowerOrder growerOrderTableObject)
            : this()
        {
            OrderGuid = growerOrderTableObject.Guid;
            OrderNo = growerOrderTableObject.OrderNo;
            DateEntered = growerOrderTableObject.DateEntered;
            CustomerPoNo = growerOrderTableObject.CustomerPoNo;
            PromotionalCode = growerOrderTableObject.PromotionalCode;
            OrderDescription = growerOrderTableObject.Description;
            OrderType = growerOrderTableObject.OrderTypeLookup.Name;
            ProgramTypeCode = growerOrderTableObject.ProgramType.Code;
            ProgramTypeName = growerOrderTableObject.ProgramType.Name;
            ProductFormCategoryCode = growerOrderTableObject.ProductFormCategory.Code;
            ProductFormCategoryName = growerOrderTableObject.ProductFormCategory.Name;
            NavigationProgramTypeCode = growerOrderTableObject.NavigationProgramTypeCode;
            NavigationProductFormCategoryCode = growerOrderTableObject.NavigationProductFormCategoryCode;


            
            GrowerCreditCardGuid = growerOrderTableObject.GrowerCreditCardGuid ?? Guid.Empty;

            //TODO: Put something here to reuse shipweek business objects?
            ShipWeek = new ShipWeek(growerOrderTableObject.ShipWeek);

            ShipWeekGuid = growerOrderTableObject.ShipWeekGuid;

            GrowerOrderStatus = new Lookup(growerOrderTableObject.GrowerOrderStatusLookup);

            Grower = new Grower(growerOrderTableObject.Grower);

            PersonWhoPlacedOrder = new Person(growerOrderTableObject.Person, growerOrderTableObject.Grower);

            PaymentType = new Lookup(growerOrderTableObject.PaymentTypeLookup, growerOrderTableObject.PaymentTypeLookup.ParentLookup.ChildLookupList);

            PaymentCreditCard = new CreditCard(growerOrderTableObject.GrowerCreditCard);
            ShipToAddressGuid = growerOrderTableObject.ShipToAddressGuid;

        }

        public string ToFormattedString(int tabLevel = 0)
        {
            var stringBuilder = new StringBuilder();

            string tabs = "";
            for (int i = 0; i < tabLevel; i++)
            {
                tabs += '\t';
            }

            stringBuilder.Append(tabs);
            stringBuilder.AppendLine("GROWER ORDER");
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Guid:{0}", OrderGuid);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("OrderNo:{0}", OrderNo);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("ProductFormCategoryName:{0}", ProductFormCategoryName);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("ProgramTypeName:{0}", ProgramTypeName);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("ShipWeek:{0}", ShipWeek);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Description:{0}", OrderDescription);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("CustomerPoNo:{0}", CustomerPoNo);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("PromotionalCode:{0}", PromotionalCode);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("OrderType:{0}", OrderType);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Grower.GrowerName:{0}", Grower.GrowerName);
            stringBuilder.AppendLine();

            foreach (var supplierOrder in SupplierOrders)
            {
                stringBuilder.AppendLine("{");
                stringBuilder.Append(supplierOrder.ToFormattedString(tabLevel + 1));
                stringBuilder.AppendLine("}");
            }

            foreach (var growerOrderHistoryEvent in GrowerOrderHistoryEvents)
            {
                stringBuilder.AppendLine("{");
                stringBuilder.Append(growerOrderHistoryEvent.ToFormattedString(tabLevel + 1));
                stringBuilder.AppendLine("}");                
            }

            return stringBuilder.ToString();
        }

      

    }
}
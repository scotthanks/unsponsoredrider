﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    class ProductOrderTransactionType
    {
        public enum OrderStatusEnum
        {
            Success,
            Added,
            Updated,
            Failed
        }

        public ProductOrderTransactionType()
        {
            OrderLineStatuses = new List<ProductOrderLineTransactionType>();
        }
        
        public OrderStatusEnum Status { get; set; }

        public IEnumerable<ProductOrderLineTransactionType> OrderLineStatuses { get; set; } 

        public class ProductOrderLineTransactionType
        {
            public enum OrderLineStatusEnum
            {
                Success,
                Adjusted,
                Failed,
                Unavailable,
            }

            public Guid ProductGuid { get; set; }
            public string Quantity { get; set; }
            public OrderLineStatusEnum Status { get; set; }
        }
    }
}

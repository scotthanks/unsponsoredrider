﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class GrowerOrderHistoryEvent
    {
        public GrowerOrderHistoryEvent(DataObjectLibrary.GrowerOrderHistory growerOrderHistoryTableObject)
        {
            Guid = growerOrderHistoryTableObject.Guid;
            EventTime = growerOrderHistoryTableObject.EventDate;
            Comment = growerOrderHistoryTableObject.Comment;
            IsInternal  = growerOrderHistoryTableObject.IsInternal;
            GrowerOrderGuid = growerOrderHistoryTableObject.GrowerOrderGuid;
            LogTypeLookupCode = growerOrderHistoryTableObject.LogTypeLookup.Code;
            PersonName = growerOrderHistoryTableObject.Person.FirstName + " " + growerOrderHistoryTableObject.Person.LastName;
            UserCode = growerOrderHistoryTableObject.Person.UserCode;
        }

        public Guid Guid { get; set; }
        public DateTime EventTime { get; set; }
        public string Comment { get; set; }
        public Guid GrowerOrderGuid { get; set; }
        public string LogTypeLookupCode { get; set; }
        public bool IsInternal { get; set; }
        public string PersonName { get; set; }
        public string UserCode { get; set; }

        public string ToFormattedString(int tabLevel = 0)
        {
            var stringBuilder = new StringBuilder();

            string tabs = "";
            for (int i = 0; i < tabLevel; i++)
            {
                tabs += '\t';
            }

            stringBuilder.Append(tabs);
            stringBuilder.AppendLine("EVENT LOG");
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Guid:{0}", Guid);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("EventTime: {0:dd-MMM-yy hh:mm:ss}", EventTime);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Comment: {0}", Comment);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("LogTypeLookupCode: {0}", LogTypeLookupCode);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("IsInternal: {0}", IsInternal);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("PersonName: {0}", PersonName);
            stringBuilder.AppendLine();

            return stringBuilder.ToString();
        }
    }
}

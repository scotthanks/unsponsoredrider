﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class Content
    {
        public long Id { get; set; }
        public Guid Guid { get; set; }
        public string ContentHTML { get; set; }
        public string CSS { get; set; }
        public bool IsLive { get; set; }
        public string Path { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
    }
}

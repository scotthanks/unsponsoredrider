﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace BusinessObjectsLibrary
{
    public class GrowerUser
    {

        public GrowerUser()
        {
        }

        public GrowerUser(DataObjectLibrary.Person personDataRow)
        {
            GrowerUserGuid = personDataRow.UserGuid.Value;
            FirstName = personDataRow.FirstName;
            LastName = personDataRow.LastName;
            Email = personDataRow.EMail;
            Phone = new PhoneNumber(personDataRow.PhoneAreaCode, personDataRow.Phone);
            CompanyRole = personDataRow.PersonTypeLookup.Name;
                
        }

        public Guid GrowerUserGuid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Utility.PhoneNumber Phone { get; set; }
        public string CompanyRole { get; set; }
        public string SitePermission { get; set; }
        public bool IsCaller { get; set; }
    }
}

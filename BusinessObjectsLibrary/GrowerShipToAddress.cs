﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;
using Utility;

namespace BusinessObjectsLibrary
{
    public class GrowerShipToAddress : Address
    {
        public GrowerShipToAddress()
            : base()
        {
        }

        public GrowerShipToAddress(DataObjectLibrary.GrowerAddress growerAddressDataRow)
            : base(growerAddressDataRow.Address)
        {
            GrowerAddressGuid = growerAddressDataRow.Guid;
            SpecialInstructions = growerAddressDataRow.SpecialInstructions;
            SellerCustomerID = growerAddressDataRow.SellerCustomerID;
            IsDefault = growerAddressDataRow.DefaultAddress;
            SellerCustomerID = growerAddressDataRow.SellerCustomerID;
            Phone = new PhoneNumber(growerAddressDataRow.PhoneAreaCode, growerAddressDataRow.Phone);
        }

        public Guid GrowerAddressGuid { get; set; }
        public string SellerCustomerID { get; set; }
        public string SpecialInstructions { get; set; }
        public PhoneNumber Phone { get; set; }
        public bool IsDefault { get; set; }
        public bool IsSelected { get; set; }
    }
}

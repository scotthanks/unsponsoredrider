﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class GeneticOwner
    {
        public GeneticOwner() { }

        public string Code { get; set; }
        public string Name { get; set; }
    }
}

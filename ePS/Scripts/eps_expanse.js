﻿
var eps_expanse = {};

eps_expanse.config = {
    containerClass:             "eps_expanse_containers",
    
    collapsedClass:             "eps_expanses_collapsed",
    expandedClass:              "eps_expanses_expanded",
    workingClass:               "eps_expanses_working",
    disabledClass:              "eps_expanses_disabled",
    
    headerBarsClass:            "eps_expanses_header_bars",
    
    displaysClass:              "eps_expanses_displays",
    
    statusBarsClass:            "eps_expanses_status_bars",
    
    defaultFooterText:           "Nothing to display.",
    expansionRate:              50/15,   //millisecons per pixel
    expansionRateBase:          350,
    expansionRateMin:           750,
};

eps_expanse.instances = [];

eps_expanse.state = {
    calculatedExpansionRate: function (containerId) {
        var displayHeight = eps_expanse.state.getDisplayElement(containerId).height || 45;
        var v = eps_expanse.config.expansionRateBase + (Math.ceil(displayHeight * eps_expanse.config.expansionRate) || 0);
        v = v < eps_expanse.config.expansionRateMin ? eps_expanse.config.expansionRateMin : v;
        return v;
    },

    getContainerId: function (childElement) {

        //parents = $(childElement).parents();
        //alert(parents.length);

        //var id = null;

        //id = $(childElement).parents("." + eps_expanse.config.containerClass).prop("id") || "??";
        return $(childElement).parents("." + eps_expanse.config.containerClass).prop("id") || null;
    },
    getDisplayElement: function (containerId) {
        return $("#" + containerId + " ." + eps_expanse.config.displaysClass);
    },
    getStatusBarElement: function (containerId) {
        return $("#" + containerId + " ." + eps_expanse.config.statusBarsClass);
    },
    setWorking: function (containerId) {
        var container = $("#" + containerId) || null;
        if (container == null) { return false; };
        container
            .removeClass()
            .addClass(eps_expanse.config.containerClass)
            .addClass(eps_expanse.config.workingClass);
        return true;
    },
    setCollapsed: function (containerId) {
        var container = $("#" + containerId) || null;
        if (container == null) { return false; };
        container
            .removeClass()
            .addClass(eps_expanse.config.containerClass)
            .addClass(eps_expanse.config.collapsedClass);
        return true;
    },
    expanded: function (containerId) {
        return $("#" + containerId).hasClass(eps_expanse.config.expandedClass);
    },
    collapsed: function (containerId) {
        return $("#" + containerId).hasClass(eps_expanse.config.collapsedClass);
    },
};

eps_expanse.init = function (containerId, onExpand, onCollapse, headerText, footerText) {
    //footerText is optional, defaults to defaultFooterText
   // alert("here");
    if (typeof containerId !== 'string' || containerId == null || containerId.length < 1) { return false; }
    var container = $("#" + containerId) || null;
    if (container == null) { return false; };

    var headerBar = $("<h3 />")
        .prop({
            "class": eps_expanse.config.headerBarsClass
        })
        .text(headerText || "??")
        .on("click", function () {
            var containerId = eps_expanse.state.getContainerId($(this));
            if (eps_expanse.state.expanded(containerId)) {
                onCollapse(containerId);
            } else if (eps_expanse.state.collapsed(containerId)) {
                onExpand(containerId);
            } else {
            };
        });

    //var upperContent = $("<div />")
    //    .prop({
    //        "class": eps_expanse.config.contentUpperClass
    //    })
    //    .html("<span>Upper content here.</span>");
    //var middleContent = $("<div />")
    //    .prop({
    //        "class": eps_expanse.config.contentMiddleClass
    //    })
    //    .html("<span>Middle content here.</span>");
    //var lowerContent = $("<div />")
    //    .prop({
    //        "class": eps_expanse.config.contentLowerClass
    //    })
    //    .html("<span>Lower content here.</span>");

    var statusBar = $("<h4 />")
        .prop({
            "class": eps_expanse.config.statusBarsClass
        })
        .html("<span>" + (footerText || eps_expanse.config.defaultFooterText) + "</span>");

    var display = $("<div />")
        .prop({
            "class": eps_expanse.config.displaysClass,
        });
        //.html("<span>Some content here.</span>");

        //.append([
        //    upperContent,
        //    middleContent,
        //    lowerContent,
        //    statusBar
        //]);

    container
        .removeClass()
        .addClass(eps_expanse.config.containerClass)
        .addClass(eps_expanse.config.collapsedClass)
        .append([
            headerBar,
            display,
            statusBar
        ]);

};

eps_expanse.collapse = function (containerId, duration, clearDisplay) {
    //alert("collapse");
    if (typeof containerId !== 'string' || containerId == null || containerId.length < 1) { return false; }
    var container = $("#" + containerId) || null;
    if (container == null) { return false; };
    container
        .removeClass()
        .addClass(eps_expanse.config.containerClass)
        .addClass(eps_expanse.config.workingClass);

    var display = eps_expanse.state.getDisplayElement(containerId);
    display.hide("blind", {}, (duration || eps_expanse.state.calculatedExpansionRate(containerId)), function () {
        if (clearDisplay) { eps_expanse.state.getDisplayElement(containerId).empty(); }; //conditionally clear content
        container.removeClass(eps_expanse.config.workingClass).addClass(eps_expanse.config.collapsedClass);
    });
};

eps_expanse.expand = function (containerId, duration) {
    //alert("expand");
    if (typeof containerId !== 'string' || containerId == null || containerId.length < 1) { return false; }
    var container = $("#" + containerId) || null;
    if (container == null) { return false; };
    var display = eps_expanse.state.getDisplayElement(containerId);

    //onExpand(containerId);

    display.show("blind", {}, (duration || eps_expanse.state.calculatedExpansionRate(containerId)), function () {
    container
        .removeClass()
        .addClass(eps_expanse.config.containerClass)
        .addClass(eps_expanse.config.expandedClass);
    });
};

eps_expanse.setStatus = function (containerId, message) {
    
eps_expanse.state.getStatusBarElement(containerId).text(message || "");

};
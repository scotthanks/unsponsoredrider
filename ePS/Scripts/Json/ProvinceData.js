﻿[
    { "Name": "Alberta", "Abbr": "AB" },
    { "Name": "British Columbia", "Abbr": "BC" },
    { "Name": "Manitoba", "Abbr": "MB" },
    { "Name": "New Brunswick", "Abbr": "NB" },
    { "Name": "Newfoundland &nbsp; Labrador", "Abbr": "NL" },
    { "Name": "Northwest Territory", "Abbr": "NT" },
    { "Name": "Nova Scotia", "Abbr": "NS" },
    { "Name": "Nunavut", "Abbr": "NU" },
    { "Name": "Ontario", "Abbr": "ON" },
    { "Name": "Prince Edward Island", "Abbr": "PE" },
    { "Name": "Quebec", "Abbr": "QC" },
    { "Name": "Saskatchewan", "Abbr": "SK" },
    { "Name": "Yukon", "Abbr": "YT" }
]


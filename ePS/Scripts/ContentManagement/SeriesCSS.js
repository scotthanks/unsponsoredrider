﻿ .sr_series_subtable {font-family:Helvetica,sans-serif; font-size:16px;padding:0px} 
.sr_series_type_displays {padding:0px; border-bottom:1px solid #737474;}
.sr_series_type_images {width:120px; height: 120px; margin:5px; padding:0px;}
.sr_series_header { vertical-align:top; font-family:Helvetica,sans-serif; font-size: 14px; }
.sr_series_catalog { font-family:Helvetica,sans-serif; font-size: 14px; }
.sr_series_result_name_spans { vertical-align:top; color:#287d87;font-family:Helvetica,sans-serif; font-size: 21px; }
.sr_series_result_name_spans2 { font-family:Helvetica,sans-serif; font-size: 16px; color:#9bc718}
.sr_series_result_name_spans3 { font-family:Helvetica,sans-serif; font-size: 16px;}
.sr_series_listingheader {font-weight:bold;font-family:Helvetica,sans-serif;font-size: 16px;} 
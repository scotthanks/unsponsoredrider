﻿
///////////////////////// PopUp Dialog ////////////////////////////////////////
var messageDialog = $("#dialog_message_ok_container").dialog({
    autoOpen: false,
    height: 300,
    width: 400,
    modal: true,
    title: "Message Box",
    buttons: {
        "Ok": function () {
            $(this).dialog("close");
        }
    },
    open: function (event, ui) {
        //alert("id: " + $(this).prop("id"));


        $('.ui-widget-overlay').bind('click', function () { messageDialog.dialog('close'); });
        //$('.ui-widget-overlay').bind('click', function () { $("#dialog_message_ok_container").dialog('close'); });

        //$(".ui-dialog-titlebar").append("<a href=\"#variants\" id=\"variants-button\"><img src=\"admin-images/variants-button.png\" alt=\"Variants\" /></a>");
    },
    close: function () {
        $("#dialog_message_ok_content").html('');
    }
});

var popUpMessage = function (content, heading, width, height) {

    //$("#dialog_message_ok_container").prop("title", heading);

    messageDialog.dialog('option', "title", heading);

    $("#dialog_message_ok_content").html(content);
    messageDialog.dialog("open");
};

//////////////////////////// Log In Dialog //////////////////////////////////
var frm;

(function(){
    var dialog1 = $("<div></div>");
    dialog1.prop({ "id": "sign_in_dialog", title: "Sign-In" });
    var div1 = $("<div></div>");
    div1.prop("id", "login_container");
    dialog1.append(div1);
    $("body").append(dialog1);
})();

var logInDialog = $("#sign_in_dialog").dialog({
    //appendTo: "#someElem",
    dialogClass: "loginDialogClass",
    autoOpen: false,
    show: { effect: 'fade', duration: 250 },
    hide: { effect: 'fade', duration: 500 },
    height: 425,
    width: 450,
    modal: true,
    closeOnEscape: true,
    resizable: false,
    open: function (event, ui) { alert("hi");},
    close: function (event, ui) {
        alert("hi");
        //alert("close event");
        $("#login_container").html('');
        updateSignInStatus();
    }
});
logInDialog.siblings('div.ui-dialog-titlebar').remove();
//logInDialog.attr('id', 'login_dialog');
//$('.loginDialogClass div.ui-dialog-titlebar').remove();
//$('#sign_in_dialog ui-dialog-titlebar').remove();
//$("#sign_in_dialog").siblings('div.ui-dialog-titlebar').remove();

////////////////////////// Simple Membership Tools ///////////////////////////////////////
var logOff = function () {
    //alert("logOff");
    var returnValue = false;
    $.ajax({
        type: 'post',
        url: '/test/LogOff',
        data: {},
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
        },
        success: function (response) {
            //alert("IsLoggedIn: " + response.IsLoggedIn);
            if (!response.IsLoggedIn) {
                returnValue = true;
            };
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + decodeURI(this.url) + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            //alert("logOff");
            if (returnValue) {
                window.location.href = "/";
            } else {
                alert("goodbye");
                updateSignInStatus();
            };
            return returnValue;
        }
    });
};
var updateSignInStatus = function () {
   
    console.log("updateSignInStatus");
    var SignInStatusDisplay = $("#mp_sign_in_display");
    var auth = getAuthentication();
    //alert(traverseObj(auth));
//    alert("Signed In: " + auth.FirstName);

    if (auth.LoggedIn) {
        console.log("logged in");
        $("#menu-item-signin").hide();
        $("#menu-item-signout").show();
        var linkUser = $("<a />");
        linkUser.attr("href", "/Account/Manage");
        linkUser.text(auth.FirstName);

        //var linkSignOut = $("<a></a>");
        //linkSignOut.attr("href", "JAVASCRIPT:logOff();");
        //linkSignOut.text("Sign Out");

        SignInStatusDisplay.empty().text("Welcome rider: ").append(linkUser);

    } else {
        console.log("not logged in");

        //var linkSignIn = $("<a />");
        //linkSignIn.attr("href", "JAVASCRIPT:basicLogin();");
        //linkSignIn.text("Sign In");
        SignInStatusDisplay.empty();

    };

};
var getLoginHtml = function (returnUrl) {
    //alert("hi - " + returnUrl);
    //if (returnUrl === null || returnUrl=="")
    //{
        returnUrl = "/Home";
    //}
   // alert("updated - " + returnUrl);
    $.ajax({
        type: "get",
        data: { returnUrl: "PopUp" },
        url: "/account/login",
        datatype: "html",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
            var loading = $("<img src=\"/Images/icons/icon_loading_bar_green.gif\" alt=\"loading icon\" title=\"Loading...\" />");
            $("#login_container").html(loading).delay(15);
        },
        success: function (response, textStatus, jqXHR) {
            $("#login_container").html(response);
            $('#popup_login_form:first *:input[type!=hidden]:first').focus();
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + decodeURI(this.url) + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            frm = $("#popup_login_form");
            frm.submit(function () {
                $.ajax({
                    type: frm.attr('method'),
                    url: frm.attr('action'),
                    data: frm.serialize(),
                    beforeSend: function (jqXHR, settings) {
                        //alert(traverseObj(data));
                    },
                    success: function (response) {
                        if (response.LoggedIn) {
                            //close dialog or proceed to ReturnUrl depending on returnUrl
                            if (typeof returnUrl !== "undefined" && typeof returnUrl === "string" && returnUrl != null && returnUrl != "") {
                                //proceed to ReturnUrl
                                window.location.replace(returnUrl);
                            }

                            else {
                                //close dialog
                                logInDialog.dialog("close");
                            };
                            

                            
                        }
                        else
                        {
                            //display failure message in dialog.
                            $("#popup_login_status_message").html("Oops! Your email or password is incorrect.<br />Need Help?");
                        };
                        //alert(JSON.stringify(response));
                    }
                });
                return false;
            });
        }
    });
};
var basicLogin = function () {
    alert('basicLogin');
    //alert(window.location.href);
    //var returnUrl = window.location.protocol + "//" + window.location.host
    var returnUrl = window.location.pathname + window.location.search;
    var url = "/Account/Login?ReturnUrl=" + encodeURIComponent(returnUrl);
    //alert(url);
    window.location.href = url;
    return false;
}
var popUpLogIn = function () {
    
    alert("in popUpLogIn");
    var authentication = getAuthentication();
    if (authentication.LoggedIn) {
        alert("Already");
       var content = "Already Logged In!";
        content += "<br />UserName: " + authentication.UserName;
        var heading = "Sign-In:";
        popUpMessage(content, heading);
    } else {

        alert("dialog open");
        logInDialog.dialog("option", "modal", true);
        logInDialog.dialog("option", "open", function (event, ui) {
            //alert("dialog open");
            getLoginHtml();
            $('.ui-widget-overlay').bind('click', function ()
            {
                logInDialog.dialog('close');
            });
        });
        logInDialog.dialog("open");
    };
};
var staticLogIn = function () {
   
    logInDialog.dialog("option", "modal", false);
    var returnUrl = eps_GetQueryStringValue("ReturnUrl");
    returnUrl = unescape(returnUrl);
    logInDialog.dialog("option", "open", function(event, ui) {
        getLoginHtml(returnUrl);
       
    });

    logInDialog.dialog("open");
};
var getAuthentication = function () {
    var authentication = {
        LoggedIn: false,
        IsAdministrator: false,
        UserName: "",
        FirstName: ""
    };
    $.ajax({
        type: 'post',
        async: false,        //synchronous return;
        url: '/test/TestMe',
        data: {},
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
        },
        success: function (response) {
            authentication.LoggedIn = response.LoggedIn;
            if (response.LoggedIn) { authentication.UserName = response.UserName; authentication.FirstName = response.FirstName; authentication.IsAdministrator = response.IsAdministrator; };
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + decodeURI(this.url) + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
        }
    });
    return authentication;
};

///////////////////////////////////////////////////////////////////////////////



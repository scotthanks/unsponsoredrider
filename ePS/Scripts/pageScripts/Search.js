﻿
var model = {};
model.ResponseData = {};
model.TemplateData = [];

model.config = {
    urlGetSearchResult: "/api/Search/Result/OpenSearch/",
    urlGetSearchResultTempate: "api/Search/Template/",
}; 

model.returnTemplate = function (templateGuid) {
    var returnTemplate = null;
    $.each(model.TemplateData, function (ndx, template) {
        if (template.TemplateGuid == templateGuid) { returnTemplate = template; return false; };
    });
    return returnTemplate;
};


model.getTemplates = function (callbacks) {
    var results = model.ResponseData.SearchResult;
    model.TemplateData = [];

    $.each(results.ResultRows, function (ndx, row) {
        if (model.returnTemplate(row.RowTypeTemplateGuid) == null) {
            //alert("bingo");
            $.ajax({
                async: false,
                datatype: "json",
                type: "get",
                url: model.config.urlGetSearchResultTempate + row.RowTypeTemplateGuid,
                beforeSend: function (jqXHR, settings) {
                    //$("#test_area_response_display").empty();
                    //$("#test_area_response_display").html(this.url);
                },
                success: function (data, textStatus, jqXHR) {
                    model.TemplateData.push(data.ResultTemplate || {});
                    //$("#test_area_response_display").text(traverseObj(data, true));
                    //alert(traverseObj(data, false));

                },
                //error: function (jqXHR, textStatus, errorThrown) {
                //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
                //},
                complete: function (jqXHR, textStatus) {
                    //viewModel.ProcessEventChain(callbacks);
                }
            });
        };
    });
    viewModel.ProcessEventChain(callbacks);
};


model.getAndOrReturnTemplate = function (guid, callbacks) {

    //alert("bingo");


    $.ajax({
        datatype: "json",
        type: "get",
        url: model.config.urlGetSearchResultTempate + guid,
        beforeSend: function (jqXHR, settings) {
            //$("#test_area_response_display").empty();

        },
        success: function (data, textStatus, jqXHR) {
            model.TemplateData.push(data.ResultTemplate || {});
            //$("#test_area_response_display").text(traverseObj(data, true));
            alert(traverseObj(data, false));

        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });


};


model.submitSearch = function (searchTerm, callbacks) {
    //alert(searchTerm);

    $.ajax({
        datatype: "json",
        type: "get",
        url: model.config.urlGetSearchResult,
        data: {
            "Term": searchTerm
        },
        beforeSend: function (jqXHR, settings) {
            model.ResponseData = {};
            $("#test_area_response_display").empty();

        },
        success: function (data, textStatus, jqXHR) {
            model.ResponseData = data || {};
            $("#test_area_response_display").html(traverseObj(data, true));

        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });


};

var viewModel = {};
viewModel.state = {
    searchTerm:null,
};

viewModel.init = function () {

    //alert("Initializing Term: " + viewModel.state.searchTerm);

    $("#eps_tools_search_input").val(viewModel.state.searchTerm);

    model.submitSearch(viewModel.state.searchTerm, [function () {
        //alert(traverseObj(model.ResponseData, false));

        if (model.ResponseData.Success) {
            //display result
            viewModel.displayResult();

        } else {
            //display failure message
            $("#search_result_failure_message").text(model.ResponseData.Message);

        };
    }]);

};


viewModel.publishCssToPageHead = function (callbacks) {

    $.each(model.TemplateData, function (ndx, template) {
        //alert(template.Css);
        //CSS only gets added once per template
        $('head').append('<style type="text/css" >' + template.Css + '</style>');
    });

    viewModel.ProcessEventChain(callbacks);
};


viewModel.displayResult = function () {
    model.getTemplates([
        viewModel.publishCssToPageHead,
        function () {
            var result = model.ResponseData.SearchResult;
            if (result.ResultCount >= 50)
            {
                $("#search_result_count_message").text("Showing 1-50 of " + result.ResultCount + " Results");
            }
            else
            {
                if (result.ResultCount > 0)
                {
                    $("#search_result_count_message").text("Showing 1-" + result.ResultCount + " of " + result.ResultCount + " Results");
                }
                else
                {
                    $("#search_result_count_message").text("No Results Found");
                  
                }
            }

            var display = $("#search_result_display");
            $.each(result.ResultRows, function (ndx, item) {


                var template = model.returnTemplate(item.RowTypeTemplateGuid);
                //var template = model.returnTemplate("a711b73b-d1ea-4b94-8f39-628c3ea24eb0");
                //var template = model.returnTemplate("08b041bc-65e9-4bd0-8f10-8b58ece7ce27");

                //alert(traverseObj(template, false));


                var div = $("<div />")
                    .html(template.Html);
                //div.html("Template Guid: " + item.RowTypeTemplateGuid);
                display.append(div);


               //alert(template.Javascript);
                var json = template.Javascript;
                //alert(json);
                eval('var parameters = ' + json);
                //alert(parameters);
                eval('var func = ' + parameters.init);
                //alert(func);
                func(item);

                //var func = function (data) {
                //    var rowId = eps_createNewGuid(); $('#sr_variety_type_template').prop({ 'id': rowId }); $('#' + rowId + ' .sr_variety_type_anchors').prop({ 'href': '/Catalog/Search?Category=' + data.CatalogProductFormCategoryCode + '&Form=' + data.CatalogProgramTypeCode }); $('#' + rowId + ' .sr_variety_type_images').prop({ 'src': data.ImageURI }); $('#' + rowId + ' .sr_result_name_spans').text(data.ResultName);  
                //};
                //func(item);


                

            });
        }
    ]);
};


/////////////// Utilities //////////////////////////////
viewModel.floatMessage = function (element, content, interval) {
    //floats message to right of element
    //if no element then does nothing
    if (typeof element === 'undefined') { return false; };

    var top, left;
    if (element == null) {
        //Leave values null
        top = null;
        left = null;
    }
    else if (element.length > 0) {
        var position = element.offset();
        //ToDo: code to center floating message vertically with element
        //assume floating message is 35 px tall
        top = Math.floor(position.top + ((element.height() - 35) / 2));
        left = Math.floor(position.left + element.width() + 30);
    };
    eps_tools.FloatingMessage(left, top, content, interval);
};
viewModel.ProcessEventChain = function (callbacks) {

    if (callbacks != undefined && callbacks.length > 0) {
        var callback = callbacks.shift();
        //alert(callback);
        callback(callbacks);
    }
};
//viewModel.ShipWeekCodeToShipWeekString = function (code) {
//    if (typeof code !== "string" || code == null || code.length != 6) { return ""; };
//    return code.substring(4, 6) + "/" + code.substring(0, 4);
//    //return code;
//};
////////////////////////////////////////////////////////




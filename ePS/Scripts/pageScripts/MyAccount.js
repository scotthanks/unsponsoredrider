﻿
var viewModel = {};

viewModel.config = {
    accordions: {
        headerBarsClickableClass: "header_bars_clickable",
        headerBarsClass: "header_bars",
        collapseExpandRate: 350,
        accordionDisplaysClass: "accordion_displays",
        orderAccountDisplayId: "order_account_display",
        companyPreferencesDisplayId: "company_preferences_display",
        userPreferencesDisplayId: "user_preferences_display",

    },
    icons: {
        WorkingSrc:'/images/layout/cart_header_pointer.png',
    },
};

viewModel.state = {
    acordions: {
        orderAccountExpanded: true,
        companyPreferencesExpanded: true,
        userPreferencesExpanded: true,
    },
};

viewModel.init = function () {
    //alert("init");

    $("." + viewModel.config.accordions.accordionDisplaysClass).data({ "expanded": true });

    $("." + viewModel.config.accordions.headerBarsClickableClass).off().on("click", function () {
        //alert("bingo");

        var headerBar = $(this);
        var display = $(headerBar).siblings("." + viewModel.config.accordions.accordionDisplaysClass);
        if ((display.length || 0) == 0) { return false; };
        var displayId = $(display[0]).prop("id");
        var isExpanded = $("#" + displayId).data("expanded");
        if (isExpanded) {
            viewModel.accordionCollapse(displayId);
        } else {
            viewModel.AccordionExpand(displayId);
        };
        //alert(displayId);
    });

};

viewModel.accordionCollapse = function (displayId) {

    //alert("collapse");
    var display = $("#" + displayId);
    if ((display.length || 0) == 0) { return false;};
    var headerBar = $(display[0]).siblings("." + viewModel.config.accordions.headerBarsClickableClass);
    if ((headerBar.length || 0) == 0) { return false; };
    display.hide("blind", {}, viewModel.config.accordions.collapseExpandRate, function () {
        //$(headerBar).css({ "background-image": "url('" + viewModel.config.icons.WorkingSrc + "')" });
        display.data({ "expanded": false });
    });


};

viewModel.AccordionExpand = function (displayId) {
    //alert("expand");
    var display = $("#" + displayId);
    if ((display.length || 0) == 0) { return false; };
    var headerBar = $(display[0]).siblings("." + viewModel.config.accordions.headerBarsClickableClass);
    if ((headerBar.length || 0) == 0) { return false; };
    display.show("blind", {}, viewModel.config.accordions.collapseExpandRate, function () {
        //$(headerBar).css({ "background-image": "url('" + viewModel.config.icons.WorkingSrc + "')" });
        display.data({ "expanded": true });
    });

};
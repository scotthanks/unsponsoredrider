﻿
var model = {};
model.LineOfCreditInfo = {};
model.RequestLineOfCreditResponse = {};
model.GetPaymentMethodResponse = {};
model.CreditCardsData = {};
model.stateData = [];
model.provinceData = [];
model.config = {
    urlLineOfCreditInfoApi: "/api/V2/Grower/LineOfCredit",
    urlRequestLineOfCreditApi: "/api/V2/Grower/RequestLineOfCredit/",
    urlPaymentMethodGetApi: "/api/V2/Grower/DefaultPaymentMethodLookup",
    urlPaymentMethodPutApi: "/api/V2/Grower/DefaultPaymentMethodLookup/",
    urlCreditCardsGetApi: "/api/V2/Grower/CreditCards",
    urlDefaultCreditCardPutApi: "/api/V2/Grower/DefaultCreditCard/",
    urlUpdateCreditCardPutApi: "/api/V2/Grower/UpdateCreditCard",
    urlDeleteCreditCardPutApi: "/api/V2/Grower/DeleteCreditCard",
    urlCardOnFileApi: '/api/CardOnFile',

};

//Generic Ajax Call - overides global error handler
model.ajaxCall = function (element, uri, httpVerb, data, onSuccess, callbacks) {
   // alert("ajaxCall");
    $.ajax({
        url: uri,
        dataType: "json",
        data: data,
        type: httpVerb,
    })
    .done(function (data, textStatus, jqXHR) {
       // alert(traverseObj(data, false));
       // alert(textStatus);
        if (data.Success == true)
        { onSuccess(data); }
        else
        {
          //  alert("error: " + data.Message);
            viewModel.floatMessage(element, "Error: " + data.Message, 2500);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        var errorMsg = "TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown;

       // alert("error: " + errorMsg);
        if (typeof element !== "undefined" & element != null) {

            viewModel.floatMessage(element, errorMsg, 4000);
        };
    })
    .always(function (jqXHR, textStatus) {
        if (textStatus == "success") {
            viewModel.ProcessEventChain(callbacks);
        } else {
        };
    });
};

model.getStateProvinceData = function (callback) {
    var url = '/Scripts/Json/StateData.js';
    var jqxhr = $.getJSON(url, function (data) {
        model.stateData = data;
      //  alert(traverseObj(model.stateData, false));
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        alert("TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown);
    })
    .done(
    function () {
        var url = '/Scripts/Json/ProvinceData.js';
        var jqxhr = $.getJSON(url, function (data) {
            model.provinceData = data;
           // alert(traverseObj(model.provinceData, false));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown);
        })
        .done(callback);

    });

};



var viewModel = {};
viewModel.config = {
    accountNameClass: "account_name_displays",
    lineOfCreditAmountId: "line_of_credit_amount_span",
    lineOfCreditRequestAmountId: "line_of_credit_request_input",
    requestLineOfCreditButtonId: "request_line_of_credit_button",
    actionButtonEnabledClass: "action_buttons",
    actionButtonDisabledClass: "action_buttons_disabled",

    methodsContainerId: "payment_methods_container",
    methodsDisplayId: "payment_methods_display",

    creditCardsContainerId: "credit_cards_container",
    creditCardsDisplayId: "credit_cards_display",
    creditCardValidationMessageElement: $("#credit_card_table_container .validation_messages"),

    creditCardRowTemplateId: "credit_card_row_template",
    creditCardTableBodyElement: $("#credit_card_table tbody"),
    creditCardRadioButtonsName: "credit_card_radio_buttons_name",

    buttonAddCardId: "add_new_card_button",

    lineOfCreditContainerId: "line_of_credit_container",
    lineOfCreditDisplayId: "line_of_credit_display",
    paymentMethodValidationMessageElement: $("#default_methods_radio_container .validation_messages"),
    paymentMethodsRadioName: "payment_methods",

    invoiceRadioElementId: "payment_method_invoice",
    creditCardRadioElementId: "payment_method_credit_card",
    invoiceRadioElement: $("#payment_method_invoice"),
    creditCardRadioElement: $("#payment_method_credit_card"),

    testLineOfCreditInfoButton: $("#test_line_of_credit_info"),

    Forms: {
        ClassName: "payment_options_page_forms",
        //NewAddressDisplayId: "new_address_form_display",
        AddressFormTemplateId: "address_form_template",
        AddressFormId: "new_address_form",
        AddressFormMessageClass: "ship_to_message",
        AddressFormInputBoxesClass: "inputTextBox",

        AddressSaveButtonId: "ship_to_button_save",
        AddressDeleteButtonId: "ship_to_button_delete",
        AddressCancelButtonId: "ship_to_button_cancel",
        AddressFormTemplate: null,
        AddressForm: null,

        ShipToGrowerShipToAddressGuidId: "hidden_ship_to_address_guid",
        ShipToGrowerGuidId: "hidden_grower_guid",
        ShipToNameId: "ship_to_name",
        ShipToStreetAddress1: "ship_to_street_address_1",
        ShipToStreetAddress2: "ship_to_street_address_2",
        ShipToCity: "ship_to_city",
        ShipToZipCode: "ship_to_zip_code",
        ShipToPhoneNumber: "ship_to_phone_number",
        ShipToCountryCodeName: "ship_to_country",
        ShipToStateCode: "ship_to_state_code",
        ShipToSpecialInstructions: "ship_to_special_instructions",

        CardOnFileFormAddTemplateId: "credit_card_add_form_template",
        CardOnFileFormId: "credit_card_form",
        CardOnFileFormMessageClass: "credit_card_message",
        CardOnFileFormInputBoxesClass: "inputTextBox",

        CardOnFileSendButtonId: "button_credit_card_create_add",
        CardOnFileCancelButtonId: "button_credit_card_create_cancel",

        CardOnFileFormTemplate: null,
        CardOnFileForm: null,

        CardOnFileDescription: "credit_card_description",

    },

};
viewModel.state = {
    pageMode: "all",

};
viewModel.init = function () {
    //alert("bingo");

    


    //State and Province Data
    model.getStateProvinceData();

    //initialize Methods Display
    var onExpandMethodsDisplay = function (containerId) {
       // alert("bingo");
        eps_expanse.state.setWorking(containerId);
        var onSuccess = function (data) {
            //alert(traverseObj(data, false));
            model.GetPaymentMethodResponse = data;
        };


        var uri = model.config.urlPaymentMethodGetApi;
        var element = model.config.paymentMethodValidationMessageElement;
        model.ajaxCall(element, uri, "get", {}, onSuccess, [function () {
            var target = null;
            $.each(model.GetPaymentMethodResponse.PaymentTypeList, function (ndx, item) {
                var element = null;
                if (item.Code == "Invoice") { var element = viewModel.config.invoiceRadioElement; };
                if (item.Code == "Credit") { var element = viewModel.config.creditCardRadioElement; };
                if (element != null) {
                    element.data({ "guid": item.Guid });
                    if (item.IsDefault) { target = element };
                };
                //$('input:radio[id='+   +'][id=test2]').prop('checked', true);
            });
            if (target != null) { target.prop('checked', true); };
        }]);


        var display = eps_expanse.state.getDisplayElement(containerId);
        eps_expanse.expand(containerId, null);
        eps_expanse.setStatus(containerId, "");
    };
    var onCollapseMethodsDisplay = function (containerId) {
        eps_expanse.state.setWorking(containerId);
        eps_expanse.collapse(containerId, null, false);
        eps_expanse.setStatus(containerId, "Choose Invoice or Credit Card");
    };
    eps_expanse.init(viewModel.config.methodsContainerId, onExpandMethodsDisplay, onCollapseMethodsDisplay, "Manage Payment Methods", "Choose Invoice or Credit Card");
    eps_expanse.state.getDisplayElement(viewModel.config.methodsContainerId).append($("#" + viewModel.config.methodsDisplayId));
  
    $("#" + viewModel.config.methodsDisplayId).css({ "display": "block" });
    if (viewModel.state.pageMode == "all" || viewModel.state.pageMode == "paymentmethods") {
        var containerId = viewModel.config.methodsContainerId;
        onExpandMethodsDisplay(containerId);
    };

    viewModel.definePaymentMethodRadioEvents();



    //initialize Cards Display
    var creditCardRowTemplate = $("#" + viewModel.config.creditCardRowTemplateId);
    //alert(creditCardRowTemplate.length);
    viewModel.config.creditCardRowClone = creditCardRowTemplate.clone();
    creditCardRowTemplate.remove();


    var onExpandCardsDisplay = function (containerId) {
        
        eps_expanse.state.setWorking(containerId);
        
        var onSuccess = function (data) {
            model.CreditCardsData = data;
        };
        var uri = model.config.urlCreditCardsGetApi;
        var element = model.config.creditCardsValidationMessageElement;
        model.CreditCardsData = {};
        model.ajaxCall(element, uri, "get", {}, onSuccess, [function () {
            viewModel.config.creditCardTableBodyElement.empty();
            $.each(model.CreditCardsData.CreditCards, function (ndx, card) {
                var rowClone = viewModel.config.creditCardRowClone.clone();
                rowClone.prop({ "id": card.Guid });
                var radioButton = rowClone.children("td:nth-child(1)").children("input.credit_card_radio_buttons");
                if(card.IsDefault == true)
                {
                    radioButton
                   .prop({
                       "id": radioButton.prop("id")+ndx,
                       "value": card.Guid,
                       "checked": true
                   });
                }
                else
                { radioButton
                    .prop({
                    "id": radioButton.prop("id")+ndx,
                    "value": card.Guid
                    });
                }
                rowClone.children("td:nth-child(2)").children("span.credit_card_last_four_digits").text(card.CardNumber);
                rowClone.children("td:nth-child(3)").children("input.credit_card_description_inputs").val(card.CardDescription);
                rowClone.children("td:nth-child(3)").children("input.credit_card_description_inputs").on("change", function () {
                    var offset = $(this).offset();
                    var cardGuid = rowClone.prop("id");
                    var desc = rowClone.children("td:nth-child(3)").children("input.credit_card_description_inputs").val();
                  //  alert("in function - " + cardGuid + " " + desc );
                    viewModel.UpdateCardOnFileDescription(cardGuid,desc);
                }
          );
                rowClone.children("td:nth-child(4)").children("a.credit_card_delete_links").on("click", function () {
                     var offset = $(this).offset();
                     var cardGuid = rowClone.prop("id");
                     //alert("in function - " + cardGuid);
                     viewModel.DisplayDeleteCardOnFileForm(cardGuid, { left: offset.left - $(window).scrollLeft(), top: offset.top - $(window).scrollTop() });
                }
                        );

                viewModel.config.creditCardTableBodyElement.append(rowClone);
            });
            viewModel.defineCreditCardsRadioEvents();
        }]);


        

        var display = eps_expanse.state.getDisplayElement(containerId);
        eps_expanse.expand(containerId, null);
        eps_expanse.setStatus(containerId, "");
    };
    var onCollapseCardsDisplay = function (containerId) {
        eps_expanse.state.setWorking(containerId);
        eps_expanse.collapse(containerId, null, false);
        eps_expanse.setStatus(containerId, "Add or Remove Cards Here");
    };
    eps_expanse.init(viewModel.config.creditCardsContainerId, onExpandCardsDisplay, onCollapseCardsDisplay, "Manage Credit Cards", "Add or Remove Cards Here");
    eps_expanse.state.getDisplayElement(viewModel.config.creditCardsContainerId).append($("#"+viewModel.config.creditCardsDisplayId));
    $("#" + viewModel.config.creditCardsDisplayId).css({ "display": "block" });
    if (viewModel.state.pageMode == "all" || viewModel.state.pageMode == "creditcards") {
        var containerId = viewModel.config.creditCardsContainerId;
        onExpandCardsDisplay(containerId);
    };



    //initialize LineOfCredit Display
    var onExpandLineOfCreditDisplay = function (containerId) {
        eps_expanse.state.setWorking(containerId);


        //get and display Line Of Credit Info
        var onSuccess = function (data) {
            if (data.IsAuthenticated && data.Success) {
                model.LineOfCreditInfo = data;
            } else {
                model.LineOfCreditInfo = {};
            };
            viewModel.accountInfoDisplaysRefresh();
            //$("." + viewModel.config.accountNameClass).text(model.LineOfCreditInfo.GrowerName || "??");
            //$("#" + viewModel.config.lineOfCreditAmountId).text("$ " + numeral(model.LineOfCreditInfo.CreditLimit).format('0,0.00'));
        };

        var element = $("#" + viewModel.config.lineOfCreditAmountId);
        var uri = model.config.urlLineOfCreditInfoApi;
        model.ajaxCall(element, uri, "get", {}, onSuccess, []);




        var display = eps_expanse.state.getDisplayElement(containerId);
        eps_expanse.expand(containerId, null);
        eps_expanse.setStatus(containerId, "");
    };
    var onCollapseLineOfCreditDisplay = function (containerId) {
        eps_expanse.state.setWorking(containerId);
        eps_expanse.collapse(containerId, null, false);
        eps_expanse.setStatus(containerId, "Request Line of Credit Here");
    };
    eps_expanse.init(viewModel.config.lineOfCreditContainerId, onExpandLineOfCreditDisplay, onCollapseLineOfCreditDisplay, "Manage Line of Credit", "Request Line of Credit Here");
    eps_expanse.state.getDisplayElement(viewModel.config.lineOfCreditContainerId).append($("#" + viewModel.config.lineOfCreditDisplayId));
    $("#" + viewModel.config.lineOfCreditDisplayId).css({"display":"block"});
    if (viewModel.state.pageMode == "all" || viewModel.state.pageMode == "lineofcredit") {
        var containerId = viewModel.config.lineOfCreditContainerId;
        onExpandLineOfCreditDisplay(containerId);
    };

    viewModel.RequestLineOfCreditWireUp();

    viewModel.DisplayDeleteCardOnFileForm = function (cardGuid, location) {
      
        var content = $('<span />').html("Card will be deleted. <br /><br />");
        eps_tools.OkCancelDialog({
                'title': '<span>Delete Card:</span>',
                'content': content,
                'modal': true,
                'onOk': function () {
                    viewModel.DeleteCardOnFile(cardGuid)
                },
                'locate': location,
            });
       // };
    };
    viewModel.DeleteCardOnFile = function (guid) {
        // alert("Delete: " + guid);
            var onSuccess = function () {
            viewModel.floatMessage($('.' + viewModel.config.creditCardsContainerId), 'Card Deleted', 2500);
        };

        var element = $("." + viewModel.config.creditCardsContainerId);
        var url = model.config.urlDeleteCreditCardPutApi;
        
        model.ajaxCall(element, url, "put", {"CreditCardGuid": guid}, onSuccess, []);
        
        var containerId = viewModel.config.creditCardsContainerId;
        onExpandCardsDisplay(containerId);
        
    };

    viewModel.UpdateCardOnFileDescription = function (guid,description) {
        var onSuccess = function () {
            viewModel.floatMessage($('.' + viewModel.config.creditCardsContainerId), 'Card Description Updated', 2500);
        };
        
        var element = $("." + viewModel.config.creditCardsContainerId);
        var url = model.config.urlUpdateCreditCardPutApi;
        //  alert(url);
        model.ajaxCall(element, url, "put", {
            "CreditCardGuid": guid,
            "CardDescription": description
        }, onSuccess, []);

       
    };

    ///////////////// Store and Delete Form Templates //////////////////

    viewModel.config.Forms.CardOnFileFormTemplate = $("#" + viewModel.config.Forms.CardOnFileFormAddTemplateId).clone(true); //store the card Form HTML Template
    $("#" + viewModel.config.Forms.CardOnFileFormAddTemplateId).remove();

    /////////////////// Test Buttons //////////////////////////
    $("#test_line_of_credit_info").button().on("click", function () {
        //alert("test_open_orders_detail_history_api");
        var target = $("#test_area_results_display").empty();

        var onSuccess = function (data) {
            model.LineOfCreditInfo = data;
        };

        var uri = model.config.urlLineOfCreditInfoApi;
        var element = model.config.testLineOfCreditInfoButton;
        model.ajaxCall(element, uri, "get", {}, onSuccess, [function () {
            target.append("<p>GET: " + uri + "<hr /></p>");
            target.append("<p>" + traverseObj(model.LineOfCreditInfo, true) + "</p>");
        }]);


    });

    $("#test_request_line_of_credit").button().on("click", function () {
        //alert("test_open_orders_detail_history_api");
        var target = $("#test_area_results_display").empty();

        var onSuccess = function (data) {
            model.RequestLineOfCreditResponse = data;
        };

        //var uri = model.config.urlRequestLineOfCreditApi;

        var amount = parseInt(numeral().unformat($("#" + viewModel.config.lineOfCreditRequestAmountId).val() || '0')) || 0;
        var uri = model.config.urlRequestLineOfCreditApi + amount;
        //var element = $("#test_request_line_of_credit");
        var element = $(this);
        //var data = { "RequestedAmount": numeral($("#" + viewModel.config.lineOfCreditRequestAmountId).val()).format('0') };
        model.ajaxCall(element, uri, "put", {}, onSuccess, [function () {
            target.append("<p>PUT: " + uri + "<hr /></p>");
            target.append("<p>" + traverseObj(model.RequestLineOfCreditResponse, true) + "</p>");
        }]);
    });

    $("#test_credit_card_info").button().on("click", function () {
        //alert("test_credit_card_info");

        var target = $("#test_area_results_display").empty();
        var onSuccess = function (data) {
            //alert(traverseObj(data, false));
            model.CreditCardsData = data;

        };
        var uri = model.config.urlCreditCardsGetApi;
        var element = model.config.creditCardsValidationMessageElement;
        model.ajaxCall(element, uri, "get", {}, onSuccess, [function () {
                target.append("<p>GET: " + uri + "<hr /></p>");
                target.append("<p>" + traverseObj(model.CreditCardsData, true) + "</p>");
        }]);
    });


    //////////////// Action Buttons ///////////////////////
    $("#" + viewModel.config.buttonAddCardId).on("click", function () {
        //var orderGuid = viewModel.state.displayedOrderGuid();
        viewModel.DisplayAddCardOnFileForm();
     });

    viewModel.DisplayAddCardOnFileForm = function () {
        //clone credit card form template and set id
        viewModel.config.Forms.CardOnFileForm = viewModel.config.Forms.CardOnFileFormTemplate.clone();                    //Copy the template and 
        viewModel.config.Forms.CardOnFileForm.prop({
            "id": viewModel.config.Forms.CardOnFileFormId,                                                             //change id to working id
        });

        //create and show address form
        eps_tools.FormSubmitDialog({
            "title": "Add Credit Card:",
            "content": viewModel.config.Forms.CardOnFileForm,
            "modal": true,

            //"size": {"width":555, "height":444},
        });

        $("#" + viewModel.config.Forms.CardOnFileFormId + " input:first").select();

        $("#" + viewModel.config.Forms.CardOnFileFormId + " input[type='radio'][name='credit_card_country']").off().on("change", function () {
            var country = $("#" + viewModel.config.Forms.CardOnFileFormId + " input[type='radio'][name='credit_card_country']:checked").val() || "";
            //alert(country);

            var select = $("#credit_card_state_code");
            select.empty();
            var arr = [];
            var lbl = $("#" + viewModel.config.Forms.CardOnFileFormId + ".state_province_selection_label");
            if (country == "ca") {
                arr = model.provinceData;
                lbl.text("Province");
            } else {
                arr = model.stateData;
                lbl.text("State");
            };
            $.each(arr, function (i, item) {
                var option = $("<option></option>");
                option.text(item.Abbr);
                option.prop("value", item.Abbr);
                select.append(option);
            });
        }).change();

        var messageBox = $("#" + viewModel.config.Forms.CardOnFileFormId + " ." + viewModel.config.Forms.CardOnFileFormMessageClass);
        // Set Add button event
        var sendButton = $("#" + viewModel.config.Forms.CardOnFileSendButtonId);
        //alert(sendButton.length);
        sendButton.off().on("click", function () {
            //CardOnFileMessageClass
            viewModel.CreateCardOnFile(messageBox);

            //  alert(viewModel.config.creditCardsContainerId);
            // var containerId = viewModel.config.creditCardsContainerId;
            //  onExpandCardsDisplay(containerId);
        });
        //alert(sendButton.length);
        /////////////////////////////////////////////////////////

        // Set Cancel button event
        $("#" + viewModel.config.Forms.CardOnFileCancelButtonId).off().on("click", function () {
            eps_tools.Dialog.close();
        });
        //alert(sendButton.length);
        /////////////////////////////////////////////////////////

        //Stop return key from closing form when pushed in text box
        var inputBoxes = $("#" + viewModel.Config.Forms.CardOnFileFormId + " ." + viewModel.Config.Forms.CardOnFileFormInputBoxesClass);
        //alert(inputBoxes.length);
        inputBoxes.off().on('keydown', function (evt) {
            evt.stopPropagation();
            //return false;              //false would stop browser from processing key

            //var messageBox = $("#" + viewModel.Config.Forms.CardOnFileFormId + " ." + viewModel.Config.Forms.CardOnFileFormMessageClass);

            messageBox.text('');
            return true;

        });

        //stop return key from closing form when pushed in textarea
        $("#ship_to_special_instructions").off().on('keydown', function (evt) {
            evt.stopPropagation();
            //return false;         //false would stop browser from processing key
            return true;
        });

    };

    viewModel.CreateCardOnFile = function (element) {
        //alert("CreateCardOnFile");
        var data = viewModel.ComposeCardOnFileDataFromForm();
       
        var callbacksToUpdateCardDisplay = function () {
            var containerId = viewModel.config.creditCardsContainerId;
            onExpandCardsDisplay(containerId);
         
        };
        model.WriteNewCardOnFile(data, element, [callbacksToUpdateCardDisplay]);
       
    };

    model.WriteNewCardOnFile = function (data, element, callbacks) {
        //element (optional) is message element to receive messages.

        var url = model.config.urlCardOnFileApi;

        var resultString = "Write New CardOnFile:\n";


        $.ajax({
            type: "PUT",
            url: url,
            data: data,
            datatype: "json",
            beforeSend: function (jqXHR, settings) {
                //alert("AJAX url: " + this.url);
                resultString += "url: " + this.url + "\n";
                resultString += "data: " + JSON.stringify(this.data) + "\n";
                //alert(resultString);
            },
            success: function (response, textStatus, jqXHR) {
                if (typeof response !== 'undefined' && response != null) {
                    //alert(JSON.stringify(response));
                    resultString += "RESPONSE: " + JSON.stringify(response) + "\n";
                    if (!response.IsAuthenticated) {
                        element.text("You must be logged-in to use this page.");
                    } else if (response.Success) {
                        //update element
                        element.text("Data updated.");
                        //close dialog
                        eps_tools.Dialog.close();


                    } else {
                        element.text(response.Message);
                    }
                };
            },
            //error: function (jqXHR, textStatus, errorThrown) {
            //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
            //},
            complete: function (jqXHR, textStatus) {
                 viewModel.ProcessEventChain(callbacks);
            }
        });

    };



    viewModel.ComposeCardOnFileDataFromForm = function () {
        var data = {};
        //sync the names to Api
        data.FirstName = $("#credit_card_first_name").val();
        data.LastName = $("#credit_card_last_name").val();
        data.CardNumber = $("#credit_card_number").val();
        data.ExpirationDate = $("#credit_card_expiration").val();
        data.Code = $("#credit_card_code").val();
        data.Description = $("#credit_card_description").val();
        data.StreetAddress1 = $("#credit_card_street_address_1").val();
        data.StreetAddress2 = $("#credit_card_street_address_2").val();
        data.City = $("#credit_card_city").val();
        data.StateCode = $("#credit_card_state_code").val();

        data.Country = $("#" + viewModel.config.Forms.CardOnFileFormId + " input[type='radio'][name='credit_card_country']:checked").val() || "";
        data.ZipCode = $("#credit_card_zip_code").val();
        data.PhoneNumber = $("#credit_card_phone_number").val();
        //data.FaxNumber = $("#credit_card_fax_number").val();

        //alert(traverseObj(data, false));
        return data;
    };




};


viewModel.defineCreditCardsRadioEvents = function () {
    //alert(viewModel.config.creditCardRadioButtonsName);
    $("input[name='" + viewModel.config.creditCardRadioButtonsName + "']")
    .off()      //first remove all events
    .on("change", function (e) {
        //alert("change");
        var element = viewModel.config.creditCardValidationMessageElement;

        var onSuccess = function (data) {
            //alert(traverseObj(data, false));
            if (data.Success) {
                viewModel.floatMessage(element, "Default credit card updated.", 2500);

                //ToDo: reset selected Credit Card
                //model.CreditCardsData = {};
                //alert(traverseObj(data, false));
            } else {
                viewModel.floatMessage(element, "An error has occured on our server.  Change may not have been recorded.", 4000);
            };
        };


        var guid = $(this).prop("value");
        //alert(guid);

        var uri = model.config.urlDefaultCreditCardPutApi + guid;
        //alert(uri);
        //var data = { "DefaultPaymentMethodLookupGuid": guid };
        model.ajaxCall(element, uri, "put", {}, onSuccess, [function () {
            //alert("bingo");
        }]);





    })
    .on("blur", function (e) {
    });
}


viewModel.definePaymentMethodRadioEvents = function () {
    $("input[name='" + viewModel.config.paymentMethodsRadioName + "']")
    .off()      //first remove all events
    .on("change", function (e) {
        //alert ("in function");
        var element = viewModel.config.paymentMethodValidationMessageElement;

        var onSuccess = function (data) {
            //alert(traverseObj(data, false));
            if (data.Success) {
                viewModel.floatMessage(element, "Default method updated.", 2500);
                model.GetPaymentMethodResponse = {};
               // alert(traverseObj(data, false));
            } else {
                viewModel.floatMessage(element, "An error has occured on our server.  Change may not have been recorded.", 4000);
            };
        };


        var guid = $(this).data("guid");
        var uri = model.config.urlPaymentMethodPutApi + guid;
        //alert(uri);
        //var data = { "DefaultPaymentMethodLookupGuid": guid };
        model.ajaxCall(element, uri, "put", {}, onSuccess, [function () {
            //alert("bingo");
        }]);





    })
    .on("blur", function (e) {
    });
}

viewModel.accountInfoDisplaysRefresh = function () {
    $("." + viewModel.config.accountNameClass).text(model.LineOfCreditInfo.GrowerName || "??");
    $("#" + viewModel.config.lineOfCreditAmountId).text("$ " + numeral(model.LineOfCreditInfo.CreditLimit).format('0,0.00'));

};

viewModel.RequestLineOfCreditWireUp = function () {
    //alert("RequestLineOfCreditWireUp");
    $("#" + viewModel.config.requestLineOfCreditButtonId).off().on("click", function () {
        //alert("request_line_of_credit_button");
        var thisButton = $(this);
        if (thisButton.hasClass(viewModel.config.actionButtonDisabledClass)) { return false; };//do nothing if disabled
        thisButton.removeClass().addClass(viewModel.config.actionButtonDisabledClass);//disable this button
        var amount = parseInt(numeral().unformat($("#" + viewModel.config.lineOfCreditRequestAmountId).val() || '0')) || 0;
        $("#" + viewModel.config.lineOfCreditRequestAmountId).val(numeral(amount).format('0'));

        var uri = model.config.urlRequestLineOfCreditApi + amount;
        //var element = $("#test_request_line_of_credit");
        var element = $(this);
        var onSuccess = function (data) {
            model.RequestLineOfCreditResponse = data;
        };

        model.RequestLineOfCreditResponse = {};
        model.ajaxCall(element, uri, "put", {}, onSuccess, [function () {
            //alert(traverseObj(model.RequestLineOfCreditResponse, false));
            //viewModel.RequestLineOfCreditWireUp();


            var result = {
                requestSuccess: model.RequestLineOfCreditResponse.Success,
                requestAuthenticated: model.RequestLineOfCreditResponse.IsAuthenticated,
                message: model.RequestLineOfCreditResponse.Message,

                infoSuccess: null,
                infoAuthenticated: null,
                requestedLineOfCredit: null,
                creditAppOnFile: null,
                creditAppStatus: null
            };


            //update Line Of Credit Info
            var onSuccess = function (data) {
                if (data.IsAuthenticated && data.Success) {
                    model.LineOfCreditInfo = data;
                } else {
                    model.LineOfCreditInfo = {};
                };
                viewModel.accountInfoDisplaysRefresh();

            };
            var element = $("#" + viewModel.config.requestLineOfCreditButtonId);
            var uri = model.config.urlLineOfCreditInfoApi;
            model.ajaxCall(element, uri, "get", {}, onSuccess, [function () { 
                result.infoSuccess = model.LineOfCreditInfo.Success || null;
                result.infoAuthenticated = model.LineOfCreditInfo.IsAuthenticated || null;
                result.requestedLineOfCredit = model.LineOfCreditInfo.RequestedLineOfCredit || null;
                result.creditAppOnFile = model.LineOfCreditInfo.CreditAppOnFile || null;
                result.creditAppStatus = model.LineOfCreditInfo.CreditAppStatusName|| null;

                var title = $('<span />').text("Request Pending:");
                var content = $("<div />");

                if (!result.requestAuthenticated || !result.infoAuthenticated) {
                    title.text("Error:");
                    var p = $("<p />")
                        .text("You must be authenticated.  Please log-in and try again.");
                    content.append(p);
                } else if (!result.requestSuccess || !result.infoSuccess) {
                    alert("bingo");
                    title.text("Error:");
                    var p = $("<p />")
                        .text("Error occured on our server.  Please try again.");
                    content.append(p);
                } else if ((result.message || "") != "") {
                    title.text("Error:");
                    var p = $("<p />")
                        .text(result.message);
                    content.append(p);
                } else if (result.creditAppOnFile) {
                    var p = $("<p />")
                        .text("Thank you. We have your information on file and will process your request for additional credit within 2 business days.");
                    content.append(p);
                } else {
                    var p = $("<p />")
                        .text("Thank you. In order to complete your request for a " + numeral(result.requestedLineOfCredit).format('$ 0,0.00') + " line of credit, ");
                    var p2 = $("<p />")
                        .text("please download Green-Fuse’s  ");
                   
                    var link = $("<a></a>")
                        .prop({
                            "href": "",
                            "target": "_blank",
                            "type": "application/pdf"
                        })
                        .css({"color": "#fff"})
                        .text("credit application");
                    p2.append(link, "");
                    var p3 = $("<p />")
                       .text(" and email it to confirmations@green-fuse.com or send it via fax to 888-820-8618.")
                    var p4 = $("<p />")
                       .text("Once applications have been received requests will be processed within 2 business days.")
                   
                    content.append(p, p2,p3,p4);

                };



                //content.append(traverseObj(result, true));
                eps_tools.OkDialog({ 'title': title, 'content': content, 'modal': true });
                thisButton.removeClass().addClass(viewModel.config.actionButtonEnabledClass);  //re-enable this button
            }]);


            
        }]);
    });
};



/////////////// Utilities //////////////////////////////
viewModel.floatMessage = function (element, content, interval) {
    //floats message to right of element
    //if no element then does nothing
    if (typeof element === 'undefined') { return false; };

    var top, left;
    if (element == null) {
        //Leave values null
        top = null;
        left = null;
    }
    else if (element.length > 0) {
        var position = element.offset();
        //ToDo: code to center floating message vertically with element
        //assume floating message is 35 px tall
        top = Math.floor(position.top + ((element.height() - 35) / 2));
        left = Math.floor(position.left + element.width() + 30);
    };
    eps_tools.FloatingMessage(left, top, content, interval);
};
viewModel.ProcessEventChain = function (callbacks) {
    //alert("ProcessEventChain");
    if (callbacks != undefined && callbacks.length > 0) {
        var callback = callbacks.shift();
        //alert(callback);
        callback(callbacks);
    }
};



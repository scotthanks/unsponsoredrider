﻿
///////////////////////////////////////////// Model ///////////////////////////////////////////////////
var model = new Object;
model.config = {
    sorts: ["Supplier", "Species", "Product", "Form", "Price", "Quantity"],
    sortedDirection: 1,    //0 -> unsorted, >0 -> ascending, <0 -> descending for currently sorted column
    sortedColumnNdx: 1,     //data comes from server sorted by Species
    sortFunctions: [
        function (a, b) {
            if (a.Supplier != b.Supplier) {
                if (a.Supplier > b.Supplier) { return 1 * model.config.sortedDirection; };
                return -1 * model.config.sortedDirection;
            };
            if (a.Species != b.Species) {
                if (a.Species > b.Species) { return 1 };
                return -1;
            };
            if (a.Product != b.Product) {
                if (a.Product > b.Product) { return 1 };
                return -1;
            };
            return 0;
        },
        function (a, b) {
            if (a.Species != b.Species) {
                if (a.Species > b.Species) { return 1 * model.config.sortedDirection; };
                return -1 * model.config.sortedDirection;
            };
            if (a.Product != b.Product) {
                if (a.Product > b.Product) { return 1 };
                return -1;
            };
            return 0;
        },
        function (a, b) {
            if (a.Product > b.Product) { return 1 * model.config.sortedDirection; };
            return -1 * model.config.sortedDirection;
            return 0;
        },
        function (a, b) {
            if (a.Form != b.Form) {
                if (a.Form > b.Form) { return 1 * model.config.sortedDirection; };
                return -1 * model.config.sortedDirection;
            };
            if (a.Species != b.Species) {
                if (a.Species > b.Species) { return 1 };
                return -1;
            };
            if (a.Product != b.Product) {
                if (a.Product > b.Product) { return 1 };
                return -1;
            };
            return 0;
        },
        function (a, b) {
            if (a.Price != b.Price) {
                return (a.Price - b.Price) * model.config.sortedDirection;
            };
            if (a.Species != b.Species) {
                if (a.Species > b.Species) { return 1 };
                return -1;
            };
            if (a.Product != b.Product) {
                if (a.Product > b.Product) { return 1 };
                return -1;
            };
            return 0;
        },
        function (a, b) {
            if (a.OrderQuantity != b.OrderQuantity) {
                return (a.OrderQuantity - b.OrderQuantity) * model.config.sortedDirection;
            };
            if (a.Species != b.Species) {
                if (a.Species > b.Species) { return 1 };
                return -1;
            };
            if (a.Product != b.Product) {
                if (a.Product > b.Product) { return 1 };
                return -1;
            };
            return 0;
        }
    ],
    urlAvailabilityApiCart: "/api/availabilityCart",
    urlAvailabilityApiOrder: "/api/availabilityOrder",
    urlGetOrderSummaries: "/api/Grower/GetGrowerOrderSummaries",
    urlShipWeeksApi: "/api/availabilityshipweeks",
    urlSupplierApi: "/api/supplier/-1",
    urlSpeciesApi: "/api/species/-1",
    urlSellerApi:"/api/seller/-1",
    urlOrderTotalsApi: "/api/Order/Totals",
    maxQuantity: 499999,
    //urlAddToCart: "api/availability",
    stateCookieName: 'epsavailselections',
    stateCookieDurationMinutes: 36 * 60,
};

model.state = {
    cookie: {
        category: "",
        form: "",
        seller: "",
        shipWeek: "",
        suppliers: "",
        species: "",
        filter: ""
},
    queryString: {
        isValid: function () {
            //true if category and form have string values
            if (this.category().length < 1) return false;
            if (this.form().length < 1) return false;
            return true;
        },             
        isFullVersion: function () {
            //alert(traverseObj(model.state.queryString, false));
            //alert('this.shipWeek().length: ' + this.shipWeek().length);

            //true if isValid and shipWeek, suppliers, and species have string values
            //console.log('valid: ', this.isValid());
            if (!this.isValid()) return false;
            //console.log('shipWeek', this.shipWeek());
            if (this.shipWeek().length != 7) return false;
            if (this.suppliers().length < 1) return false;
            if (this.species().length < 1) return false;
            return true;
        },       
        category: function () { return eps_GetQueryStringValue('Category'); },
        form: function () { return eps_GetQueryStringValue('Form'); },
        shipWeek: function () { return eps_GetQueryStringValue('ShipWeek'); },
        suppliers: function () { return eps_GetQueryStringValue('Suppliers'); },
        species: function () { return eps_GetQueryStringValue('Species'); },
        filter: function () { return eps_GetQueryStringValue('Filter') == 'true' ? true : false; },
        variety: function () { return eps_GetQueryStringValue('Variety'); },
        product: function () {return eps_GetQueryStringValue('Product'); },
        OrganicOnly: function () { return eps_GetQueryStringValue('OrganicOnly') == 'true' ? 'true' : 'false'; },
        go: function () { return eps_GetQueryStringValue('Go') == 'true' ? true : false; },

    },
    userSelections: {
        ShipWeek: "",
        Suppliers: "",
        Species: ""
    },
    hasRows: function () {
        return (model.state.rowsCount() > 0);
    },
    rowsCount: function () {
        if (model.rowData == null) { return 0; };
        //alert(viewModel.state.filterImmediateAvailability);
        if (viewModel.state.filterImmediateAvailability) { return model.countRowsWithAvailability(); } else { return model.rowData.length; };
    },
    orderTotals: {},
    seller: 'EPS',
    rebuildingRowDataIndex: false,
};

model.config.init = function () {
    //read current state cookie
    var value = viewModel.stateCookieRead();
    //console.log('stateCookieRead: ', value);
    if (value != null) {
        model.state.cookie.category = value.Category || null;
        model.state.cookie.form = value.Form || null;
        model.state.cookie.seller = value.Seller || null;
        model.state.cookie.shipWeek = value.ShipWeek || null;
        model.state.cookie.suppliers = value.Suppliers || null;
        model.state.cookie.species = value.Species || null;
        model.state.cookie.filter = value.Filter || null;
        model.state.cookie.organicOnly = value.OrganicOnly || null;
    } else {
        model.state.cookie.category = null;
        model.state.cookie.form = null;
        model.state.cookie.seller = null;
        model.state.cookie.shipWeek = null;
        model.state.cookie.suppliers = null;
        model.state.cookie.species = null;
        model.state.cookie.filter = null;
        model.state.cookie.organicOnly = null;
    };
    //alert(model.state.cookie.species);
   
};

model.rowDataIndex = [];        //Pointers to Filtered, Sorted Row Data
model.rowData = [];
model.allowOrdering = false;
model.showPriceData = false;
model.shipWeekHeadings = [];
model.shipWeekData = [];
model.supplierData = [];
model.speciesData = [];
//model.testGrowerVolumeLevels = {};
model.getSupplierData = function (callbacks) {
    //retrieve supplier data
    model.state.seller = "GFB";
    var targetRequest = $("#test_request_url");
    var targetResponse = $("#test_response_data");
    $.ajax({
        type: "get",
        data: {
            "PlantCategoryCode": model.state.queryString.category(),
            "ProductFormCode": model.state.queryString.form(),
            "SellerCode": model.state.seller
},
        url: model.config.urlSupplierApi,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //console.log('supplierData Request: ', this.url, this.data);
            if ((targetRequest.length || 0) > 0) {
                //targetRequest.text("AJAX REQUEST: " + decodeURI(this.url));
                
            };
        },
        success: function (response, textStatus, jqXHR) {
            //console.log('SupplierData Response: ', response);
            model.supplierData = response;
            if ((targetResponse.length || 0) > 0) {
                targetResponse.html("SERVER RESPONSE: <br />" + JSON.stringify(model.supplierData));
                
            };
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + decodeURI(this.url) + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });

};
model.getSpeciesData = function (callbacks) {
    //retrieve species data
    model.state.seller = "GFB";
    var targetRequest = $("#test_request_url");
    var targetResponse = $("#test_response_data");
    $.ajax({
        type: "get",
        data: {
            "PlantCategoryCode": model.state.queryString.category(),
            "ProductFormCode": model.state.queryString.form(),
            "SupplierCodes": viewModel.state.selections.getSelectedSuppliersList(),
            "SellerCode": model.state.seller

        },
        url: model.config.urlSpeciesApi,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //console.log('speciesData Request: ', this.url, this.data);
            if ((targetRequest.length || 0) > 0) {
                targetRequest.text("AJAX REQUEST: " + decodeURI(this.url));
            };
        },
        success: function (response, textStatus, jqXHR) {
            //console.log('SpeciesData Response: ', response);
            model.speciesData = response;
            if ((targetResponse.length || 0) > 0) {
                targetResponse.html("SERVER RESPONSE: <br />" + JSON.stringify(model.speciesData));
            };
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + decodeURI(this.url) + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });
};
model.getShipWeekData = function (callbacks) {
    //retrieve shipweek data
    var targetRequest = $("#test_request_url");
    var targetResponse = $("#test_response_data");
    $.ajax({
        type: "get",
        data: {
            "Category": model.state.queryString.category(),
            "Form": model.state.queryString.form(),
            "SupplierCodes": viewModel.state.selections.getSelectedSuppliersList(),
            "SpeciesCodes": viewModel.state.selections.getSelectedSpeciesList(),
            "ShipWeek": viewModel.state.shipWeekSelected.ShipWeekString
        },
        url: model.config.urlShipWeeksApi,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //console.log('shipWeekData Request: ', this.url, this.data);
            if ((targetRequest.length || 0) > 0) {
                targetRequest.text("AJAX REQUEST: " + decodeURI(this.url));
            };
        },
        success: function (response, textStatus, jqXHR) {
            //console.log('shipWeekData Response: ', response);
            model.shipWeekData = response;

            if (model.shipWeekData.SelectedShipWeek.Guid == "00000000-0000-0000-0000-000000000000") {
                var code = model.shipWeekData.SelectedShipWeek.Code;
               // alert(code);
                $.each(model.shipWeekData.ShipWeeksList, function (ndx, week) {
                    if (code == week.Code) {
                        model.shipWeekData.SelectedShipWeek = week;
                       // alert(week.Code);
                        return false;
                    }
                });
            };

            viewModel.state.shipWeekSelected = model.shipWeekData.SelectedShipWeek;


            if ((targetResponse.length || 0) > 0) {
                targetResponse.html("SERVER RESPONSE: <br />" + JSON.stringify(model.shipWeekData));
            };
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + decodeURI(this.url) + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });
};

model.getRowData = function (callbacks) {
    //Make ajax call to retrieve row data
    model.state.seller = "GFB";
    var targetRequest = $("#test_request_url");
    var targetResponse = $("#test_response_data");
 //   alert(viewModel.state.shipWeekSelected.ShipWeekString);
    var start = new Date();
    if ((targetRequest.length || 0) > 0) {
        $("#test_time_interval_data").text("?");
        $("#test_time_interval_data_working_image").show();
    };
    $("#get_row_data").prop('disabled', 'disabled');
    $.ajax({
        type: "get",
        timeout: "75000",
        data: {
            "Category": model.state.queryString.category(),
            "Form": model.state.queryString.form(),
            "Suppliers": viewModel.state.selections.getSelectedSuppliersList(),
            "Species": viewModel.state.selections.getSelectedSpeciesList(),
            "ShipWeek": viewModel.state.shipWeekSelected.ShipWeekString,
            "OrganicOnly": viewModel.state.filterOrganicOnly,
            "SellerCode": model.state.seller
},
        url: model.config.urlAvailabilityApiCart,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //console.log('getRowData Request: ', this.url, this.data);

            if ((targetRequest.length || 0) > 0) {
                targetRequest.text("AJAX REQUEST: " + decodeURI(this.url));
            };
            model.RowData = [];
            model.shipWeekHeadings = [];
            //alert("AJAX url: " + this.url);
        },
        success: function (response, textStatus, jqXHR) {
            console.log('getRowData Response: ', response);
            //alert(JSON.stringify(data));
            if ((targetResponse.length || 0) > 0) {
                targetResponse.html("SERVER RESPONSE: <br />" + traverseObj(response));
            };
            model.rowData = response.RowData;      //only save row data
            model.allowOrdering = response.AllowOrdering;
            model.showPriceData = response.ShowPriceData;
            model.shipWeekHeadings = response.ShipWeekHeadings;
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + decodeURI(this.url) + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            if ((targetRequest.length || 0) > 0) {
                $("#test_time_interval_data").text("Data: " + (new Date() - start) + " ms.");
                $("#test_row_count").text("| rows: " + model.state.rowsCount());
                $("#test_time_interval_data_working_image").hide();
            };
            $("#get_row_data").removeProp('disabled');
            viewModel.ProcessEventChain(callbacks);
        }
    });

};
model.totalOrderQuantity = function () {
    //returns integer count of totalOrderQuantity for all rows
    var total = 0;
    $.each(model.rowData, function (i, item) {
        total += item.OrderQuantity;
    });
    return total;
};

model.countRowsWithAvailability = function () {
    var count = 0;
    //var start = new Date();
    for (var i = 0; i < model.rowData.length; i++) { if (model.testRowAvailability(i)) count++; };
    //alert("["+(new Date() - start) + " ms]");
    return count;
};

model.refreshRowDataIndex = function () {
    //alert("refreshRowDataIndex");
    if (model.state.rebuildingRowDataIndex) { return false; };
    model.state.rebuildingRowDataIndex = true;
    var start = new Date();
    model.rowDataIndex = [];    //clear index
   // alert("one");
    for (var i = 0; i < model.rowData.length; i++){
        if (!viewModel.state.filterImmediateAvailability || model.testRowAvailability(i)) {
            model.rowDataIndex.push(i);
        };
    };
    var target = $("#test_response_data").empty();
    if ((target.length || 0) > 0) {
        target.append($("<p>ET: " + (new Date() - start) + " ms.</p>"));
        target.append($("<p>Count: " + (model.rowDataIndex.length || 0) + " rows.</p>"));
    };
    model.state.rebuildingRowDataIndex = false;
    return model.rowDataIndex.length || 0;
};

model.testRowAvailability = function (ndx) {
    var row = model.rowData[ndx] || null;
    if (row != null) {
        for (var i = 0; i < 5; i++) {
            if (row.DisplayCodes[i] == "OPEN" || row.Availabilities[i] > 0) { return true; };
        };
    };
    return false;
};

model.orderQuantityTotalsGet = function (onSuccess) {
    console.log(onSuccess);
    var targetRequest = $("#test_request_url");
    var targetResponse = $("#test_response_data");
    //Make ajax call to retrieve row data
    $.ajax({
        type: "get",
        data: {
            "GrowerOrderGuid": '00000000-0000-0000-0000-000000000000',
            "Category": model.state.queryString.category(),
            "Form": model.state.queryString.form(),
            "ShipWeekString": viewModel.state.shipWeekSelected.ShipWeekString
        },

        url: model.config.urlOrderTotalsApi,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            if ((targetRequest.length || 0) > 0) {
                targetRequest.text("AJAX REQUEST: " + decodeURI(this.url));
            };
            //alert("AJAX url: " + this.url);
        },
        success: function (data, textStatus, jqXHR) {
            if (typeof onSuccess === "function") {
                onSuccess(data);
            };
            //model.state.orderTotals = data;
            //if ((targetResponse.length || 0) > 0) {
            //    targetResponse.html("SERVER RESPONSE: <br />" + JSON.stringify(model.state.orderTotals));
            //};
            //alert(JSON.stringify(data));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + decodeURI(this.url) + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            //viewModel.ProcessEventChain(callbacks);
        }
    });

};


model.updateOrderQuantity = function (ndx, newQuantity) {
    //ToDo: Code to validate inputs before proceeding
    if (typeof ndx == 'undefined' || ndx == null || ndx < 0 || ndx >= model.rowData.length) { return false; };
    if (typeof newQuantity == 'undefined' || newQuantity == null || newQuantity < 0 || newQuantity > model.config.maxQuantity) { return false; };

    var v = parseInt(newQuantity);
    if (v == null || v < 0) { return false; };
    if (model.rowData[ndx].OrderQuantity == v) { return false; };
    model.rowData[ndx].OrderQuantity = v;
   // alert("Bingo a");
  
    return true;
};
model.rowDataNdxFromProductGuid = function (guid) {
    var returnValue = -1;
    if (guid == null || guid.length != 36) { return returnValue; };
    for (var i = 0; i < model.rowData.length; i++) {
        if (model.rowData[i].ProductGuid == guid) {
            returnValue = i;
            break;
        }
    }
    return returnValue;
};
model.returnAvailabilityIsUnlimited = function(ndx){
    if (typeof ndx == 'undefined' || ndx == null || ndx < 0 || ndx >= model.rowData.length) { return false; };
    var v = model.rowData[ndx].DisplayCodes[1];
    if (v == null) { return false; };
    return (v.toLowerCase() == "open");
};
model.returnAvailability = function(ndx){
    if (!model.isValidRowNdx) { return 0; };
    return parseInt(model.rowData[ndx].Availabilities[1]) || 0;
};
model.isValidRowNdx = function (ndx) {
    return (typeof ndx != 'undefined' && ndx != null && ndx >= 0 && ndx < model.rowData.length);
};
model.refreshDataRow = function (dataRow) {
    //ToDo: complete this
    if (dataRow === "undefined" || dataRow == null) { return false; };
    var ndx = model.rowDataNdxFromProductGuid(dataRow.ProductGuid);
    if (!model.isValidRowNdx(ndx)) { return false; };
    model.rowData[ndx].OrderQuantity = dataRow.OrderQuantity;
    model.rowData[ndx].IsCarted = dataRow.IsCarted;
    model.rowData[ndx].Price = dataRow.Price;
    for (var i = 0; i < 5; i++) {
        //model.rowData[ndx].Availabilities[i] = dataRow.Availabilities[i];
    };
};

model.sortOnDataColumn = function(ndx) {
    //ToDo: validate input
    if (model.config.sortedColumnNdx != ndx)  //if this is not the current sorted column
    {
        model.config.sortedDirection = 1;
    } else {
        //flip sort direction from current, default to ascending
        model.config.sortedDirection = model.config.sortedDirection > 0 ? -1 : 1;
    }
    var data = model.rowData;       //copy rowData
    //create sortFunction
    var sortFunction = model.config.sortFunctions[ndx];
    data.sort(sortFunction);        //sort data
    model.rowData = data;           //get sorted data

    //ToDo: Sort Criterion and Sort Action should be separate and result only in new rowDataIndex so actual sort could be done with subset of fields and much less data duplication
    model.refreshRowDataIndex();

    viewModel.displayGridRows(0);   //redisplay grid
    model.config.sortedColumnNdx = ndx;
    viewModel.resetSortableColumnHeadingClasses();
};

///////////////////////////////////////////////  View Model /////////////////////////////////////////
var viewModel = new Object;
viewModel.config = {
    grid: {
        colsHeader:     11,
        colsFooter:     11,
        colsData:       11,
        rowsHeader:     1,
        rowsData:       10,
        rowsFooter:     7,

        headerRowClassStem: "headerRow_",
        headerColClassStem: "headerCol_",
        dataRowIdStem: "dataRow_",
        dataColClassStem: "dataCol_",
        footerRowClassStem: "footerRow_",
        footerColClassStem: "footerCol_",
        shipWeekDisplayId:"ship_week_display_span",
        headingSortableClass: "grid_heading_sortable",
        headingSortableUnsortedClass: "grid_heading_sortable_unsorted",
        headingSortableAscendingClass: "grid_heading_sortable_ascending",
        headingSortableDescendingClass: "grid_heading_sortable_descending"

    },
    icons: {
        cart_checked: function () { return $("<img src=\"../Images/icons/icon_cart.png\" alt=\"\" title=\"\"/>"); },
        not_carted: function () { return $("<img src=\"../Images/icons/greencheck.png\" alt=\"\" title=\"\"/>"); },
        order_working: function () { return $("<img src=\"../Images/icons/loading_availability.gif\" alt=\"\" title=\"\"/>") }

    },
    searchDialog: {
        showoptions: { "direction": "left", "mode": "show" },
        hideoptions: { "direction": "left", "mode": "hide" },
        refineSearchGoId: "refine_search_go",
        refineSearchCloseId: "refine_search_close",
        refineSearchTabId: "refine_search_tab",
        refineSearchDialogId:"refine_search_dialog",
        refineSearchDialogHeaderId: "refine_search_dialog_header",
        filterAvailabilitySelectorId: "filter_availability_check_box",
        filterOrganicSelectorId: "filter_organic_check_box"
},

    totalResultsId: "total_results_span",

    //ToDo: Move these to grid section above
    orderQuantityInputClass: "order_quantity_inputs",
    orderQuantityNAClass: "order_quantity_NA",
    orderQuantityNoOrderingClass: "order_quantity_no_ordering",
    availabilityGridContainerId: "availability_grid_container",
    outerTableId: "outer_table",
    innerTableId: "inner_table",
    innerTableContainerId: "inner_table_container",
    orderQuantityTotalDisplayId: "order_total_span",
    orderQuantityCartedDisplayId: "order_carted_span",
    orderQuantityUncartedDisplayId: "order_uncarted_span",
    addToCartLinkId: "add_to_cart_link",
    cboAddToOrder: "cbo_Add_To_Order",
    addToOrderLinkId0: "add_to_order_link0",
    addToOrderLinkId1: "add_to_order_link1",
    addToOrderLinkId2: "add_to_order_link2",
    viewCartLinkId: "view_cart_link",

    supplierListDisplayId: "supplier_list_display",
    speciesListDisplayId: "species_list_display",
    shipWeekSelectorId: "ship_week_selector",
    shipWeekPriorId: "image_ship_week_prior",
    shipWeekNextId: "image_ship_week_next",
    shipWeekListContainerId: "ship_week_list_container",
    shipWeekListDisplayId: "ship_week_list_display",
    shipWeekListId: "ship_week_list",
    shipWeekAvailableClass: "_selection_available_ship_weeks",
    shipWeekSelectedClass: "_selection_selected_ship_weeks",
    selectorAllSuppliersId: "selector_all_suppliers",
    selectorAllSpeciesId: "selector_all_species",
    selectionAvailableClass: "selection_available",
    selectionSelectedClass: "selection_selected",
    selectionDisabledClass: "selection_disabled",

};
viewModel.state = {
    
    type: {
        stateObj: { 'stateObjName': 'stateObjValue' },
        title:null,
        url: null,
        queryString: null,
    },
    selections: {
        selectedSuppliersCount: function(){
            var list = $("#" + viewModel.config.supplierListDisplayId + " ul")
            list = list.children("li." + viewModel.config.selectionSelectedClass + ":not(#" + viewModel.config.selectorAllSuppliersId + ")");
            return (list.length) || 0;
        },
        selectedSpeciesCount: function () {
            var list = $("#" + viewModel.config.speciesListDisplayId + " ul")
            list = list.children("li." + viewModel.config.selectionSelectedClass + ":not(#" + viewModel.config.selectorAllSpeciesId + ")");
            return (list.length) || 0;
        },
        getSelectedCategoryCode: function () {
            //var value = model.state.queryString.category();
            //if (typeof value !== 'undefined' && value != null && value.length > 0) {
            //    return value;
            //} else {
            //    return null;
            //};
            return model.state.queryString.category();
        },
        getSelectedFormCode: function () {
            //var values = model.state.queryString.form();
            //if (typeof values !== 'undefined' && values != null && values.length > 0) {
            //    return values[0];
            //} else {
            //    return null;
            //};
            return model.state.queryString.form();
        },
        getSelectedSellerCode: function () {
            //var values = model.state.queryString.form();
            //if (typeof values !== 'undefined' && values != null && values.length > 0) {
            //    return values[0];
            //} else {
            //    return null;
            //};
            return "GFB";
        },
        getSelectedShipWeekString: function () {
            //ToDo: this needs to fetch from actual selection list



            return viewModel.state.shipWeekSelected.ShipWeekString;

        },
        getSelectedSuppliersList: function () {
            var listItems = $("#" + viewModel.config.supplierListDisplayId + " ul")
            //alert(listItems.length);
            listItems = listItems.children("li." + viewModel.config.selectionSelectedClass + ":not(#" + viewModel.config.selectorAllSuppliersId + ")");
            //alert(listItems.length);

            var s = "";
            $.each(listItems, function(){
                if (s != "") { s += ','; };
                s += $(this).data("code");
            });
            return s;
        },
        getSelectedSpeciesList: function () {
            var listItems = $("#" + viewModel.config.speciesListDisplayId + " ul");
            //alert(listItems.length);
            listItems = listItems.children("li." + viewModel.config.selectionSelectedClass + ":not(#" + viewModel.config.selectorAllSpeciesId + ")");
            //alert(listItems.length);

            var s = "";
            $.each(listItems, function () {
                if (s != "") { s += ','; };
                s += $(this).data("code");
            });
            return s;
        },
        getFilterImmediateAvailability: function() {
            return viewModel.state.filterImmediateAvailability;
        },
       
        getFilterOrganicOnly: function () {
            return viewModel.state.filterOrganicOnly;
        },
    
    shipWeekListComposed: false,
    supplierListComposed: false,
    speciesListComposed: false,
    filterImmediateAvailability: null,
    filterOrganicOnly: null,
    },
    composeHistoryState: function (go) {
        if (typeof go !== "boolean") { go = true; };
        var queryStr = "?";
        queryStr += "category=" + viewModel.state.selections.getSelectedCategoryCode();
        queryStr += "&form=" + viewModel.state.selections.getSelectedFormCode();
        queryStr += "&seller=" + viewModel.state.selections.getSelectedSellerCode();
        queryStr += "&filter=" + viewModel.state.selections.getFilterImmediateAvailability();
        if (viewModel.state.selections.getSelectedCategoryCode() == "EDB")
        {
            queryStr += "&OrganicOnly=" + viewModel.state.selections.getFilterOrganicOnly();
        }
        queryStr += "&shipweek=" + viewModel.state.selections.getSelectedShipWeekString();
        //console.log(viewModel.state.selections.getSelectedSuppliersList());
        queryStr += "&suppliers=" + viewModel.state.selections.getSelectedSuppliersList();
        queryStr += "&species=" + viewModel.state.selections.getSelectedSpeciesList();
        queryStr += "&go=" + go;

        return queryStr;
    },

    shipWeekListContainerElement: null,
    shipWeekSelected: { },  //this is set to shipweek data row by ship week list
    priorSpeciesSelected: [],
    gridPages: function(){
        var rows = model.rowData.length;
        var rowsPerPage = viewModel.config.grid.rowsData;
        if(rows <= 0) {return 0;};
        if (rows <= rowsPerPage) { return 1; };
        var pages = Math.floor(rows / rowsPerPage);
        if ((rows % rowsPerPage) > 0) { pages++ };
        return pages;
    },

    gridFirstRowDisplayed: 1,
    gridPageDisplayed: 0,

    gridPageIsDisplaying: false,
    orderQuantityBeingHandled: false,
    FooterActionsDisplayed: false,

    AddToOrderGuid: "None",
    AddToOrderNo: "None",
   
};


//

viewModel.codesCompile = function(arr) {
    var sortFunction = function (a, b) {
        //console.log(a[0], b[0]);

        if (a[0] < b[0]) {
            return -1;
        } else if (a[0] > b[0]) {
            return 1;
        } else {
            return 0;
        };

    };

    arr.sort(sortFunction);
    //console.log(arr);

    for (var i = arr.length - 1; i > 0; i--) { //travel backward so we don't delete index yet to be reached.
        var source = arr[i];
        var target = arr[i - 1];
        if (source[0] == target[0]) {
            //console.log(arr[i][0], arr[i - 1][0]);
            target[2] += (source[2]);
            arr.splice(i, 1);   //remove the later one
        };
    };

    var sortFunction2 = function (a, b) {
        return a[1] - b[1];
    };

    arr.sort(sortFunction2);

    //console.log(arr);
    return arr;


};
viewModel.varietyCodesArray = function() {
    //var obj = new {};

    var arr = [];

    $.each(model.rowDataIndex, function(indexRow, indexValue) {

        arr.push([model.rowData[indexValue].VarietyCode, indexRow, 1]);

        //console.log(model.rowData[indexValue].VarietyCode, indexRow);
    });
    //console.log(arr);

    return arr;
};
viewModel.productCodesArray = function () {
    //var obj = new {};

    var arr = [];

    $.each(model.rowDataIndex, function (indexRow, indexValue) {

        arr.push([model.rowData[indexValue].ProductCode, indexRow, 1]);

        //console.log(model.rowData[indexValue].VarietyCode, indexRow);
    });
    //console.log(arr);

    return arr;
};
viewModel.varietyCodeIndexGet = function (code) {
    var arr = viewModel.codesCompile(viewModel.varietyCodesArray());
    var index = -1;
    $.each(arr, function (ndx, val) {
        if (val[0] == code) {
            index = val[1];
            return false;
        } else {
            return true;
        };
    });
    return index;
};
viewModel.productCodeIndexGet = function (code) {
    var arr = viewModel.codesCompile(viewModel.productCodesArray());
    var index = -1;
    $.each(arr, function (ndx, val) {
        if (val[0] == code) {
            index = val[1];
            return false;
        } else {
            return true;
        };
    });
    return index;
};
viewModel.codesDisplay = function (arr) {
    arr = viewModel.codesCompile(arr);
    var s = '', rows = 0, codes = 0;
    $.each(arr, function (ndx, val) {
        codes++;
        rows += val[2];
    });
    s += "Check Sums: [";
    s += "codes = " + codes;
    s += "] - [rows =  " + rows + "]\n";
    s += 'index [count] code\n';
    $.each(arr, function (x, y) {
        var v = (y[1] + ' [' + y[2] + '] - ' + y[0] + '\n');
        //console.log(v);
        s += v;
    });

    alert(s);

};


var testHistoryGo = function(ndx) {
    //console.log(ndx);
    if (typeof ndx === 'undefined' || ndx == null) {
        //console.log(ndx);
        ndx = prompt("Value: ", 1);
        //console.log(ndx);
    }
    if (isNaN(ndx)) { ndx = 1; };
    ndx = parseInt(ndx);
    //console.log(ndx);

    //if (ndx > window.history.length) {
    //    ndx = window.history.length;};
    history.go(ndx);
};

var testHistoryView = function () {
    //console.log(history.state);
    //console.log(history);
};
var testHistoryPush = function () {

    viewModel.state.type.queryString = viewModel.state.composeHistoryState();
    history.pushState(viewModel.state.type.stateObj, viewModel.state.type.title, (viewModel.state.type.url || '') + viewModel.state.type.queryString || '');
};
var testHistoryReplace = function () {
    history.replaceState({ 'stateObjName': 'stateObjValue' }, "Testing Push State", "?category=VA&form=RC");
};
var testHistoryPopped = function () {
    var popped = ('state' in window.history && window.history.state !== null), initialURL = location.href;
    return popped;

    //http://stackoverflow.com/questions/6421769/popstate-on-pages-load-in-chrome
    //I am using History API for my web app and have one issue. I do Ajax calls to update some results on the page and use history.pushState() in order to update the browser's location bar without page reload. Then, of course, I use window.popstate in order to restore previous state when back-button is clicked.
    //The problem is well-known — Chrome and Firefox treat that popstate event differently. While Firefox doesn't fire it up on the first load, Chrome does. I would like to have Firefox-style and not fire the event up on load since it just updates the results with exactly the same ones on load. Is there a workaround except using History.js? The reason I don't feel like using it is — it needs way too many JS libraries by itself and, since I need it to be implemented in a CMS with already too much JS, I would like to minimize JS I am putting in it.
    //So, would like to know whether there is a way to make Chrome not fire up 'popstate' on load or, maybe, somebody tried to use History.js as all libraries mashed up together into one file.
};
var launchBeta = function () {
    var auth = getAuthentication();
    //alert("setting Beta2 1");
    localStorage.setItem("Beta2", "1");
    window.location.href = window.location.href.replace("Availability", "Availability2");
}


viewModel.init = function () {
   // alert("In Classic" + localStorage.getItem("Beta"));
    //if (localStorage.getItem("Beta2") === null || localStorage.getItem("Beta2") == "1") {
    launchBeta()
    //};


  

    
    window.onload = function (evt) {
       
        console.log('onload:', evt);
    };
    window.onpopstate = function (evt) {
        console.log('onpopstate:', evt);
        if (evt.state) {
            var url = window.location.href;
            console.log('url: ', url);
            window.location.href = url;
        };
    };

   

   // alert("hi");
    //model.config.init();
    viewModel.drawOuterGrid();

    //initialize new scroll bar
    viewModel.scrollBar.init();

    //init custom scrollbar
    $("#grid_scrollbar").slider({
        orientation: "vertical",
        range: false,
        min: 0,
        max: 100,
        value: 0,
        step: 1,
        change: function(e, ui) {
            var page = $("#grid_scrollbar").slider("option", "max") - ui.value;
            viewModel.displayGridRows(page);
        },
        slide: function (event, ui) {
            //var page = viewModel.state.gridPages() - ui.value;
            var page = $("#grid_scrollbar").slider("option", "max") - ui.value;
            viewModel.displayGridRows(page);
        }
    });

    //init filterImmediateAvailability
    viewModel.state.filterImmediateAvailability = viewModel.state.filterImmediateAvailability || true;
    //alert(typeof v);
    var v = eps_GetQueryStringValue("Filter");
    if (v) {
        //v has value convert it to boolean
        v = v == 'true';
    } else {
        //v has no value, default to true
        v = true;
    };
    viewModel.state.filterImmediateAvailability = (v);
    //alert(viewModel.state.filterImmediateAvailability);
    //init filterOrganicOnly
    //Only visible on Edibles
  //  alert(model.state.queryString.category());
    if (model.state.queryString.category() == "EDB")
    {
        $("#filter_organic_display").show();
        viewModel.state.filterOrganicOnly = viewModel.state.filterOrganicOnly || false;
        var v = eps_GetQueryStringValue("OrganicOnly");
        if (v) {
            //v has value convert it to boolean
            v = v == 'true';
        } else {
            //v has no value, default to false
            v = false;
        };
    }
    else
    { $("#filter_organic_display").hide(); }
    
/********************* Events for Ship Week DropDown ********************************/
    viewModel.state.shipWeekListContainerElement = $("#" + viewModel.config.shipWeekListContainerId);

    $("#" + viewModel.config.shipWeekSelectorId).on("mouseenter", function () {
        viewModel.locateShipWeekList();
    });

    $("#" + viewModel.config.shipWeekSelectorId).hover(
        function () {
            viewModel.state.shipWeekListContainerElement.css("display", "block");
        },
        function () {
            viewModel.state.shipWeekListContainerElement.css("display", "none");
        }
    );
    $("#" + viewModel.config.shipWeekListContainerId).hover(
        function () {
            viewModel.state.shipWeekListContainerElement.css("display", "block");
        },
        function () {
            viewModel.state.shipWeekListContainerElement.css("display", "none");
        }
    );

    $("#" + viewModel.config.shipWeekPriorId).on("click", function () {
        var self = $(this);
        $.each(model.shipWeekData.ShipWeeksList, function (ndx, item) {
            if (item.Guid == viewModel.state.shipWeekSelected.Guid) {

                if (ndx > 0) {
                    viewModel.state.shipWeekSelected = model.shipWeekData.ShipWeeksList[ndx - 1];
                    viewModel.getDataDisplayGrid();
                } else {
                    viewModel.floatMessage(self, "At first ship week now.", 2000);
                };

                return false;
            };
        });
    });
    $("#" + viewModel.config.shipWeekNextId).on("click", function () {
        var self = $(this);
        $.each(model.shipWeekData.ShipWeeksList, function (ndx, item) {
            if (item.Guid == viewModel.state.shipWeekSelected.Guid) {

                if (ndx < model.shipWeekData.ShipWeeksList.length - 1) {
                    viewModel.state.shipWeekSelected = model.shipWeekData.ShipWeeksList[ndx + 1];
                    viewModel.getDataDisplayGrid();
                } else {
                    viewModel.floatMessage(self, "At last ship week now.", 2000);

                };

                return false;
            };
        });

    });

/********************** Events for Search Dialog ***********************************/
    $("#" + viewModel.config.searchDialog.refineSearchTabId).on("click", function () {
        $("#" + viewModel.config.searchDialog.refineSearchTabId).effect('slide', viewModel.config.searchDialog.hideoptions, 500, function () { });
        $("#" + viewModel.config.searchDialog.refineSearchDialogId).effect('slide', viewModel.config.searchDialog.showoptions, 500, function () {
            //$("#" + viewModel.config.selectorAllSuppliersId).focus();
            //$(this).focus();
        });
    });

    $("#" + viewModel.config.searchDialog.refineSearchDialogId).on("blur", function () {
        $("#" + viewModel.config.searchDialog.refineSearchDialogId).effect('slide', viewModel.config.searchDialog.hideoptions, 500, function () { });
        $("#" + viewModel.config.searchDialog.refineSearchTabId).effect('slide', viewModel.config.searchDialog.showoptions, 500, function () { });
    });

    $("#" + viewModel.config.searchDialog.refineSearchCloseId).on("click", function () {
        $("#" + viewModel.config.searchDialog.refineSearchDialogId).effect('slide', viewModel.config.searchDialog.hideoptions, 500, function () { });
        $("#" + viewModel.config.searchDialog.refineSearchTabId).effect('slide', viewModel.config.searchDialog.showoptions, 500, function () { });
    });

    $("#" + viewModel.config.searchDialog.refineSearchGoId).on("click", function () {
        //viewModel.stateCookieWrite();
        viewModel.getDataDisplayGrid();
        $("#" + viewModel.config.searchDialog.refineSearchDialogId).effect('slide', viewModel.config.searchDialog.hideoptions, 500, function () { });
        $("#" + viewModel.config.searchDialog.refineSearchTabId).effect('slide', viewModel.config.searchDialog.showoptions, 500, function () { });
        testHistoryPush();
    });

    $("#" + viewModel.config.searchDialog.filterAvailabilitySelectorId)
        .off()
        .on("click", function () {
        viewModel.setFilterImmediateAvailability();
       


    });
    $("#" + viewModel.config.searchDialog.filterOrganicSelectorId)
        .off()
        .on("click", function () {
            viewModel.setFilterOrganicOnly();
     });
   

/***********************************************************************************/
    //Init and Show Search Dialog
    var sellerCode = localStorage.getItem("SellerCode");
   // if (sellerCode == null) {
        sellerCode = "GFB";
   // }
     model.state.seller = sellerCode;
  
    viewModel.initSearchInterface();

};
viewModel.headerCell = function (r, c) {
    return $("#" + viewModel.config.availabilityGridContainerId + " tr." + viewModel.config.grid.headerRowClassStem + r + " th." + viewModel.config.grid.headerColClassStem + c);
};
viewModel.footerCell = function (r, c) {
    return $("#" + viewModel.config.availabilityGridContainerId + " tr." + viewModel.config.grid.footerRowClassStem + r + " td." + viewModel.config.grid.footerColClassStem + c);
};

viewModel.initSearchInterface = function () {
    //reload Querystring in case this is a re-init
    model.config.init();

    //Note: model.Init should be called prior to this if a re-init
    viewModel.innerGridClear();
    model.getShipWeekData([
        viewModel.composeShipWeekList,
        model.getSupplierData,
        viewModel.composeSupplierList,
        //viewModel.selectAllSuppliers,
        model.getSpeciesData, viewModel.composeSpeciesList, //in case this is a re-init
        viewModel.filterImmediateAvailabilityInit,
        
        //viewModel.selectAllSpecies,
        //viewModel.getDataDisplayGrid
        function () {
            //$("#" + viewModel.config.searchDialog.refineSearchTabId).click();
            viewModel.searchDisplay();
            viewModel.displayShipWeekSelected();
            //$("#" + viewModel.config.grid.shipWeekDisplayId).text("Week: " + viewModel.state.shipWeekSelected.ShipWeekString);
            //update Selections per Querystring or State Cookie
            viewModel.updateSelections();
        }
    ]);

};

viewModel.filterImmediateAvailabilityInit = function (callbacks) {
    //alert("state=" + viewModel.state.filterImmediateAvailability);
    //note: this can be a re-init so don't go to cookie or url for value, that only happens once in init
    viewModel.setFilterImmediateAvailability(viewModel.state.filterImmediateAvailability);
    viewModel.ProcessEventChain(callbacks);
};

viewModel.setFilterImmediateAvailability = function(valu) {
    //valu is optional, boolean
    //if valu not specified, filter will be toggled
    var newValu = viewModel.state.filterImmediateAvailability || true;
    if (typeof valu === 'undefined') {
        newValu = !viewModel.state.filterImmediateAvailability;
    } else if (typeof valu === "boolean" && valu != viewModel.state.filterImmediateAvailability) {
        //alert("valu=" + valu);
        newValu = valu;
    }
    //alert(newValu);
    if (newValu != viewModel.state.filterImmediateAvailability) {
        //alert("toggling");
        var ctrl = $("#" + viewModel.config.searchDialog.filterAvailabilitySelectorId);
        if (ctrl.hasClass(viewModel.config.selectionSelectedClass)) {
            //alert("was selected")
            viewModel.state.filterImmediateAvailability = false;
            ctrl.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
        } else {
            //alert("now selected");
            viewModel.state.filterImmediateAvailability = true;
            ctrl.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
        };
    };

};

viewModel.setFilterOrganicOnly = function (valu) {
    //valu is optional, boolean
  //  alert("start valu=" + valu);
    //if valu not specified, filter will be toggled
    var newValu = viewModel.state.filterOrganicOnly || true;
    if (typeof valu === 'undefined') {
        newValu = !viewModel.state.filterOrganicOnly;
    } else if (typeof valu === "boolean" && valu != viewModel.state.filterOragnicOnly) {
       // alert("valu=" + valu);
        newValu = valu;
    }
  //  alert(newValu);
    if (newValu != viewModel.state.filterOrganicOnly) {
      //  alert("toggling");
        var ctrl = $("#" + viewModel.config.searchDialog.filterOrganicSelectorId);
        if (ctrl.hasClass(viewModel.config.selectionSelectedClass)) {
          //  alert("was selected")
            viewModel.state.filterOrganicOnly = false;
            ctrl.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
        } else {
          //  alert("now selected");
            viewModel.state.filterOrganicOnly = true;
            ctrl.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
        };
    };

};



viewModel.scrollBar = {
    settings: {
        max: 100,
        min: 0,
        value: 0,
        pageRows: viewModel.config.grid.rowsData
    }
};
viewModel.scrollBar.init = function () {
    var bar = $("#new_paging_scrollbar");
    var handle = $("#new_paging_scrollbar_handle");

    bar.off().on("click", null, { "name": "fred" }, function (e) {
        e.stopPropagation();

        //alert("click");

        var handle = $("#new_paging_scrollbar_handle");
        var handlePosition = handle.position();
        var handleTop = handlePosition.top;
        var handleBottom = handleTop + $("#new_paging_scrollbar_handle").outerHeight();

        //alert("click:\n" + e.data.name + "\ntop: " + this.offsetTop + "\nlocation: " + e.pageY + "\nHandle top: " + handleTop + "\nHandle bottom: " + handleBottom);
        var message = "";
        var row = viewModel.state.gridFirstRowDisplayed ;
        if (e.pageY < handleTop) {
            message = "scroll up one page";
            //row--;
            row -= viewModel.scrollBar.settings.pageRows;
            if (row < viewModel.scrollBar.settings.min) { row = viewModel.scrollBar.settings.min; };
        } else if (e.pageY > handleBottom) {
            message = "scroll down one page";
            //row++;
            row += viewModel.scrollBar.settings.pageRows;
            if (row > viewModel.scrollBar.settings.max) { row = viewModel.scrollBar.settings.max; };
        } else {
            message = "something went wrong";
            //do nothing
        };
        //alert(message);


        //viewModel.displayVarietyPage(row);
        viewModel.scrollBar.reset({ "value": row });
        //set focus on handle
        $("#new_paging_scrollbar_handle").focus();


    });

    handle.off().on("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
    })
        .off()
        .on("keydown", function (event) {

            switch (event.keyCode) {
                case $.ui.keyCode.HOME:
                    event.preventDefault();
                    event.stopPropagation();
                    //$("#new_scrollbar_up_button").click();
                    //viewModel.displayVarietyPage(1);
                    viewModel.scrollBar.reset({ "value": 1 });
                    this.focus();

                    break;
                case $.ui.keyCode.END:
                    event.preventDefault();
                    event.stopPropagation();

                    //$("#new_scrollbar_down_button").click();
                    viewModel.scrollBar.reset({ "value": viewModel.scrollBar.settings.max });
                    this.focus();

                    break;
                case $.ui.keyCode.PAGE_UP:
                    event.preventDefault();
                    event.stopPropagation();
                    var row = viewModel.state.gridFirstRowDisplayed;
                    row -= viewModel.scrollBar.settings.pageRows;
                    if (row < viewModel.scrollBar.settings.min) { row = viewModel.scrollBar.settings.min; };
                    //viewModel.displayVarietyPage(row);
                    viewModel.scrollBar.reset({ "value": row });
                    this.focus();
                    break;

                case $.ui.keyCode.PAGE_DOWN:
                    event.preventDefault();
                    event.stopPropagation();
                    var row = viewModel.state.gridFirstRowDisplayed;
                    row += viewModel.scrollBar.settings.pageRows;
                    if (row > viewModel.scrollBar.settings.max) { row = viewModel.scrollBar.settings.max; };
                    //viewModel.displayVarietyPage(row);
                    viewModel.scrollBar.reset({ "value": row });
                    this.focus();
                    break;

                case $.ui.keyCode.UP:
                case $.ui.keyCode.LEFT:
                    event.preventDefault();
                    event.stopPropagation();
                    var row = viewModel.state.gridFirstRowDisplayed;
                    row--;
                    if (row > viewModel.scrollBar.settings.max) { row = viewModel.scrollBar.settings.max; };
                    //viewModel.displayVarietyPage(row);
                    viewModel.scrollBar.reset({ "value": row });
                    this.focus();
                    break;

                case $.ui.keyCode.DOWN:
                case $.ui.keyCode.RIGHT:
                    event.preventDefault();
                    event.stopPropagation();
                    var row = viewModel.state.gridFirstRowDisplayed;
                    row++;
                    if (row > viewModel.scrollBar.settings.max) { row = viewModel.scrollBar.settings.max; };
                    //viewModel.displayVarietyPage(row);
                    viewModel.scrollBar.reset({ "value": row });
                    this.focus();
                    break;



                    //event.stopPropagation();
                    //event.preventDefault();
                    //alert("key: " + event.keyCode);
                    //if (!this._keySliding) {
                    //    this._keySliding = true;
                    //    $(event.target).addClass("ui-state-active");
                    //    allowed = this._start(event, index);
                    //    if (allowed === false) {
                    //        return;
                    //    }
                    //}
                    break;
            }

        })
        .draggable({
            containment: "parent",
            scroll: false,
            grid: [0, 45],
            start: viewModel.scrollBar.startEvent,
            drag: viewModel.scrollBar.dragEvent,
            stop: viewModel.scrollBar.stopEvent,
        });

    $("#new_scrollbar_up_button")
        .off()
        .on("click", function () {
            var row = viewModel.scrollBar.settings.value - 1;
            if (row < viewModel.scrollBar.settings.min) { row = viewModel.scrollBar.settings.min; };
            viewModel.scrollBar.reset({ "value": row });
            //};
            //set focus on handle
            $("#new_paging_scrollbar_handle").focus();

        });

    $("#new_scrollbar_down_button")
        .off()
        .on("click", function () {
            var row = viewModel.scrollBar.settings.value + 1
            if (row > viewModel.scrollBar.settings.max) { row = viewModel.scrollBar.settings.max; };
            viewModel.scrollBar.reset({ "value": row });
            //}
            //set focus on handle
            $("#new_paging_scrollbar_handle").focus();
        });

    handle.focus();
};
viewModel.scrollBar.reset = function (values) {
    //min must be >= 0
    if (typeof values.min === 'number' && Math.floor(values.min) >= 0) {
        this.settings.min = Math.floor(values.min);
    };
    //max must be >= min
    if (typeof values.max === 'number' && Math.floor(values.max) >= this.settings.min) {
        this.settings.max = Math.floor(values.max);
    };
    //value must be between min and max, inclusive
    if (typeof values.value === 'number' && Math.floor(values.value) >= this.settings.min && Math.floor(values.value) <= this.settings.max) {
        this.settings.value = Math.floor(values.value);
    };
    //Reset Buttons
    var upButton = $("#new_scrollbar_up_button");
    var downButton = $("#new_scrollbar_down_button");
    if (this.settings.value <= this.settings.min) {
        upButton.removeClass("new_scrollbar_up_button_enabled").addClass("new_scrollbar_up_button_disabled");
        downButton.removeClass("new_scrollbar_down_button_disabled").addClass("new_scrollbar_down_button_enabled");
    } else if (this.settings.value >= this.settings.max) {
        upButton.removeClass("new_scrollbar_up_button_disabled").addClass("new_scrollbar_up_button_enabled");
        downButton.removeClass("new_scrollbar_down_button_enabled").addClass("new_scrollbar_down_button_disabled");
    } else {
        upButton.removeClass("new_scrollbar_up_button_disabled").addClass("new_scrollbar_up_button_enabled");
        downButton.removeClass("new_scrollbar_down_button_disabled").addClass("new_scrollbar_down_button_enabled");
    };

    var bar = $("#new_paging_scrollbar");
    var handle = $("#new_paging_scrollbar_handle");

    if (model.state.rowsCount() == 0) {
        handle.css({ "height": bar.height(), "top": 0 }).draggable("disable");
    } else {
        //reset handle size
        handle.draggable("enable");
        var handleHeight = Math.floor(bar.height() / (this.settings.max - this.settings.min + 1));
        if (handleHeight < 35) { handleHeight = 35; };
        handle.css("height", handleHeight);

        //reset handle top and grid value
        var range = bar.height() - handle.outerHeight();
        var count = (this.settings.max - this.settings.min + 1);
        var increment = range / count;
        var targetTop = Math.round(increment * (this.settings.value - this.settings.min));
        handle.css("top", targetTop).draggable({ grid: [0, increment] });
    };

    if (this.settings.value != viewModel.state.gridFirstRowDisplayed) {
        //fire change event
        viewModel.scrollBar.changeEvent();
    };
};
viewModel.scrollBar.changeEvent = function () {
    if (this.settings.value != viewModel.state.gridFirstRowDisplayed) {
        //viewModel.displayVarietyPage(this.settings.value);

        viewModel.displayGridRows(this.settings.value);
    };
};
viewModel.scrollBar.dragEvent = function (event, ui) {

    var handleHeight = $("#new_paging_scrollbar_handle").outerHeight();
    var range = $("#new_paging_scrollbar").height() - handleHeight;
    var rowIncrement = range / (viewModel.scrollBar.settings.max - viewModel.scrollBar.settings.min + 1);

    var position = ui.position.top;
    //$("#test_scrollbar_position").text(position);

    var row = Math.floor(position / rowIncrement) + viewModel.scrollBar.settings.min;

    if (row != viewModel.state.gridFirstRowDisplayed) {

        //viewModel.displayVarietyPage(row);
        viewModel.scrollBar.reset({ "value": row });
    }

    //$("#test_scrollbar_row").text(row);

};
viewModel.scrollBar.startEvent = function (event, ui) {

};
viewModel.scrollBar.stopEvent = function (event, ui) {

};
viewModel.scrollBarScrollToVarietyCode = function (code) {
    if (typeof code === 'undefined' || code == null || typeof code !== "string") {
        return false;
    };
    var ndx = viewModel.varietyCodeIndexGet(code) || 0;
    if (ndx <= 0) { return false; };
    return viewModel.scrollBarScrollToIndex(ndx);
};
viewModel.scrollBarScrollToProductCode = function (code) {
    if (typeof code === 'undefined' || code == null || typeof code !== "string") {
        return false;
    };
    var ndx = viewModel.productCodeIndexGet(code) || 0;
    if (ndx <= 0) { return false; };
    return viewModel.scrollBarScrollToIndex(ndx);
};

viewModel.scrollBarScrollToIndex = function (ndx) {
    if (typeof ndx === 'undefined' || ndx == null || typeof ndx != 'number') { return false; };
    if (parseInt(ndx) <= 0) { return 0; };
    
    var handle = $('#new_paging_scrollbar_handle');
    var bar = $('#new_paging_scrollbar');
    var scr = {
        value: ndx,
        min: parseInt(handle.css('top')),
        max: bar.height() - handle.height(),
    };
    scr.span = (scr.max - scr.min);
    scr.top = scr.span * (ndx / viewModel.scrollBar.settings.max);

    //console.log(scr, viewModel.scrollBar.settings);
    viewModel.scrollBar.settings.value = scr.value;
    viewModel.displayGridRows(scr.value);
    handle.css({ 'top': scr.top });
    return scr.value;

//viewModel.scrollBar.settings.value = 120; viewModel.scrollBar.changeEvent();
};

viewModel.stateCookieWrite = function () {
    var value = {};
    value.Category = viewModel.state.selections.getSelectedCategoryCode();
    value.Form = viewModel.state.selections.getSelectedFormCode();
    value.ShipWeek = viewModel.state.selections.getSelectedShipWeekString();
    value.Suppliers = viewModel.state.selections.getSelectedSuppliersList();
    value.Species = viewModel.state.selections.getSelectedSpeciesList();
    value.Filter = viewModel.state.selections.getFilterImmediateAvailability();
    //alert(traverseObj(value, false));
    //duration, path, name and value
    eps_writeCookie({
        'duration': model.state.cookieDurationMinutes,
        'path': null,
        'name': model.state.cookieName,
        'value': JSON.stringify(value)
    });

};
viewModel.stateCookieRead = function () {
    var value = eps_readCookie(model.state.cookieName);
    value = JSON.parse(value);
    //console.log('cookie: ', value);
    //alert(JSON.stringify(value));
    //alert(traverseObj(value, false));
    return value;
}
viewModel.stateCookieRemove = function () {
    return eps_removeCookie(model.state.cookieName);
    //document.cookie = escape(model.state.cookieName) + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
viewModel.stateCookieIsValid = function() {
    var value = eps_readCookie(model.state.cookieName);
    //console.log('stateCookieIsValid: ', value);
    if (value == null) { return false; };
    if (value.Category == null) { return false; };
    if (value.Form == null) { return false; };
    if (value.ShipWeek == null) { return false; };
    if (value.Suppliers == null) { return false; };
    if (value.Species == null) { return false; };
    return true;
};

viewModel.searchDisplay = function()
{
    $("#" + viewModel.config.searchDialog.refineSearchTabId).effect('slide', viewModel.config.searchDialog.hideoptions, 500, function () { });
    $("#" + viewModel.config.searchDialog.refineSearchDialogId).effect('slide', viewModel.config.searchDialog.showoptions, 500, function () { });

};

viewModel.updateSelections = function () {
    
    model.config.init();
 //     alert(model.state.queryString.OrganicOnly());
    viewModel.setFilterOrganicOnly(model.state.queryString.OrganicOnly());

    if (model.state.queryString.isFullVersion()) {

        //console.log('initializing selections from query string.');
      
        model.getShipWeekData([
            viewModel.composeShipWeekList,
            model.getSupplierData,
            viewModel.composeSupplierList,
            model.getSpeciesData,
            viewModel.composeSpeciesList, //in case this is a re-init
            function () {
                var v = {
                    Category: model.state.queryString.category(),
                    Form: model.state.queryString.form(),
                    ShipWeek: model.state.queryString.shipWeek(),
                    Suppliers: model.state.queryString.suppliers(),
                    Species: model.state.queryString.species(),
                    Filter: model.state.queryString.filter(),
                    OrganicOnly: model.state.queryString.OrganicOnly(),
                    Variety: model.state.queryString.variety(),
                    Produt: model.state.queryString.product(),
                    Go: model.state.queryString.go()
                };
                viewModel.LoadSelections(v);
            }
            
        ]);

    } else {

        //if state cookie does not match Query String then retire it
        if (model.state.queryString.category() != model.state.cookie.category || model.state.queryString.form() != model.state.cookie.form) {
            //console.log('state cookie does not match, deleting it from response.');
            viewModel.stateCookieRemove();
        };

        var value = viewModel.stateCookieRead();
        if (value != null) {
            //alert('state Cookie rules');
            //console.log('using cookie value: ', value);
            viewModel.LoadSelections(value);
        }
        else
        {
            //Let defaults work
            //console.log('selections initialized from defaults.');
            //ToDo: should a call be made to LoadSelections
        };
    };
};

viewModel.LoadSelections = function (value) {
   // alert('loading selections');
    //console.log(value);
    //console.log('asterisk: ', ($.trim(value.Suppliers) == "*"));
  //  alert(value.OrganicOnly);
  //  viewModel.setFilterOrganicOnly(value.OrganicOnly);

    var suppliers = [];
    if ($.trim(value.Suppliers) == "*") {
        $.each(model.supplierData, function(i, item) { suppliers.push(item.Code); });
    } else {
        suppliers = value.Suppliers.split(",");
    };
    //console.log('suppliers: ', suppliers);
    var supplierList = $("#" + viewModel.config.supplierListDisplayId + " ul").children("li").not("#" + viewModel.config.selectorAllSuppliersId);
    var setAllSuppliers = true;
    $.each(supplierList, function (ndx, item) {
        var listItem = $(item);
        //alert(listItem.text());
        if ($.inArray(listItem.data("code"), suppliers) > -1) {
            listItem.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
        } else {
            listItem.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
            setAllSuppliers = false;
        };
    });
    if (setAllSuppliers) {
        $("#" + viewModel.config.selectorAllSuppliersId).removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
    };

    //Update Species based for new Supplier selections]
    model.getSpeciesData([viewModel.composeSpeciesList, function (callbacks) {
         //console.log('value', value);

        var species = [];
        if ($.trim(value.Species) == "*") {
            $.each(model.speciesData, function (i, item) { species.push(item.Code); });
        } else {
            species = value.Species.split(",");
        };
        //console.log('species: ', species);


        var speciesList = $("#" + viewModel.config.speciesListDisplayId + " ul").children("li").not("#" + viewModel.config.selectorAllSpeciesId);
        var setAllSpecies = true;
        $.each(speciesList, function (ndx, item) {
            var listItem = $(item);
            //alert(listItem.text());
            if ($.inArray(listItem.data("code"), species) > -1) {
                listItem.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
            } else {
                listItem.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
                setAllSpecies = false;
            };
        });
        if (setAllSpecies) {
            $("#" + viewModel.config.selectorAllSpeciesId).removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
        };
        viewModel.ProcessEventChain(callbacks);
    },
        model.getShipWeekData,
        viewModel.composeShipWeekList,
        function () {
            //set ship week per selection
            var shipWeekString = value.ShipWeek || '';
            //alert(shipWeekString);
            if (shipWeekString.length > 5) {
                $.each(model.shipWeekData.ShipWeeksList, function (index, item) {
                    if (item.ShipWeekString == shipWeekString) {
                        viewModel.state.shipWeekSelected = item;
                        viewModel.displayShipWeekSelected();
                        return false;
                    } else {
                        return true;
                    };
                });
            };
            //If only one supplier selected and one or more species then click go.
            //if (viewModel.state.selections.selectedSuppliersCount() == 1 && viewModel.state.selections.selectedSpeciesCount() > 0) {
            //    //alert('bingo');
            //    viewModel.getDataDisplayGrid();
            //};
            var autoGo = value.Go || false;
            if (autoGo && viewModel.state.selections.selectedSuppliersCount() > 0 && viewModel.state.selections.selectedSpeciesCount() > 0) {
                //$('#' + viewModel.config.searchDialog.refineSearchGoId).click();
                console.log('value: ', value);
                var scrollTo = null;
                var code = value.Variety || '';
                if (code != '') {
                    scrollTo = {
                        type:'variety',
                        code:code
                    };
                } else {
                    code = value.Product || '';
                    if (code != '') {
                        scrollTo = {
                            type:'product',
                            code:code
                        };
                    };
                };
                //console.log('one: ', scrollTo);
                viewModel.getDataDisplayGrid(scrollTo);

            };
        }
    ]);

};

viewModel.performColumnSort = function (sortName) {
    
    
    var ndx = $.inArray(sortName, model.config.sorts);
    model.sortOnDataColumn(ndx);

    //switch (ndx) {
    //    case 0:
    //        //x = "Today it's Sunday";
    //        //model.sortDataBySupplier();
    //        break;
    //    case 1:
    //        //model.sortDataBySpecies();
    //        model.sortOnDataColumn(ndx);
    //        break;
    //    case 2:
    //        model.sortDataByProduct();
    //        break;
    //    case 3:
    //        model.sortDataByForm();
    //        break;
    //    case 4:
    //        model.sortDataByPrice();
    //        break;
    //    case 5:
    //        model.sortDataByQuantity();
    //        break;
    //    default:
    //        alert("index not found" + ndx);

    //}


};
viewModel.displaySortableColumnHeadings = function () {
    viewModel.headerCell(0, 5).html('<span id="' + viewModel.config.grid.shipWeekDisplayId + '">??</span>');

    $.each(model.config.sorts, function (i, item) {
        var heading = $("<h5></h5>");

        heading.addClass(viewModel.config.grid.headingSortableClass);
        heading.addClass(viewModel.config.grid.headingSortableUnsortedClass);

        heading.prop("title", "Sort by " + item);
        heading.text(item);
        viewModel.headerCell(0, i).append(heading);
    });

    viewModel.resetSortableColumnHeadingClasses();

    $("." + viewModel.config.grid.headingSortableClass).on("click", function () {
        viewModel.performColumnSort($(this).text());
    });


};
viewModel.resetSortableColumnHeadingClasses = function () {
    $("." + viewModel.config.grid.headingSortableClass).removeClass(viewModel.config.grid.headingSortableAscendingClass).removeClass(viewModel.config.grid.headingSortableDescendingClass);
    viewModel.headerCell(0, model.config.sortedColumnNdx).children("." + viewModel.config.grid.headingSortableClass).addClass(
        function(){
            if (model.config.sortedDirection > 0) {
                return viewModel.config.grid.headingSortableAscendingClass;
            } else {
                return viewModel.config.grid.headingSortableDescendingClass;
            };
        });
};

viewModel.saveSpeciesSelections = function () {
    
};
viewModel.defineInputBoxEvents = function () {
    $('.' + viewModel.config.orderQuantityInputClass).on("keydown", function (e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        //alert(key);
        if (key == 13 || key == 9) {
            e.preventDefault();
            var guid = $(this).prop('id');
            //alert(guid);
            //ToDo: in Chrome both the keydown and the change events fire, debug this
            //alert('keydown');
            viewModel.handleOrderQuantityChange(guid);
            //$(this).change();
            //if (key == 9) {
                //$(input [id=guid]).next().focus();
                //go to next input box
                var nextGuid = viewModel.nextInputGuid(guid);
                //alert(nextGuid);
                $("#" + nextGuid).focus();
            //};
        }
    })
        .on("change", function (e) {
        //alert('change');
        var guid = $(this).prop('id');
        viewModel.handleOrderQuantityChange(guid);
    })
        .on("focus", function (e) {
        $(this).select();
    });

};


viewModel.displayGridRow = function (ndx, rowElement) {
    rowElement.empty();
    var imageOrganic = $('<img class="img_is_organic" src="/images/layout/USDA.jpg" alt="" />');
    var imagePW = $('<img class="img_is_PW" src="/images/layout/PW.jpg" alt="" />');
    var imageIncludesDelivery = $('<img class="img_includes_delivery" src="/images/layout/DeliveryIncluded.jpg" alt="" title="Price includes delivery." />');
    for (var c = 0; c < viewModel.config.grid.colsData; c++) {
        var col = $("<td></td>");
        col.prop("class", viewModel.config.grid.dataColClassStem + c);
        switch (c) {
            case 0:
                col.text(model.rowData[ndx].Supplier);
                break;
            case 1:
                col.text(model.rowData[ndx].Species);
                break;
            case 2:
               
                if (model.rowData[ndx].IsOrganic) {
                    //alert('huh?');
                    col.append(imageOrganic);
                };
                if (model.rowData[ndx].
                    IsPW) {
                   
                    col.append(imagePW);
                };

                var productSpan = $('<span />')
                    .text(model.rowData[ndx].Product)
                    .prop({ "title": ' ' + model.rowData[ndx].VarietyCode + ', ' + model.rowData[ndx].ProductCode })
                    //.prop({ "title": 'Bob'})
                    .data({
                        "VarietyCode": model.rowData[ndx].VarietyCode,
                        "ProductCode": model.rowData[ndx].ProductCode
                    });
                
                col.append(productSpan);

                break;
            case 3:
                col.text(model.rowData[ndx].Form);
                break;
            case 4:
                if (!model.showPriceData) {
                    col.text("- -");
                } else if (model.rowData[ndx].Price <= 0) {
                    col.text("TBD");
                } else {
                    var priceSpan = $('<span />');
                    if (model.rowData[ndx].Price > 100)
                    {
                        var round = Math.round;
                        priceSpan.text(round(model.rowData[ndx].Price).format('0,0.00'));
                    }
                    else
                    { priceSpan.text(numeral(model.rowData[ndx].Price).format('0,0.0000')); }
                    col.append(priceSpan);
                    if (model.rowData[ndx].IncludesDelivery) {
                        //alert('huh?');
                        col.append(imageIncludesDelivery);
                    };
                    //col.text(numeral(model.rowData[ndx].Price).format('0,0.0000'));
                };
                break;
            case 5:
                //alert(model.rowData[ndx].DisplayCode);
                if (model.rowData[ndx].DisplayCodes[1] == "NA") {
                //if (model.rowData[ndx].DisplayCode == "NA") {
                        var div = $("<div></div>");
                    div.addClass(viewModel.config.orderQuantityNAClass);
                    div.prop("id", model.rowData[ndx].ProductGuid);
                    div.html("NA");
                    col.append(div);
                } else if (!model.allowOrdering) {
                    var div = $("<div></div>");
                    div.addClass(viewModel.config.orderQuantityNoOrderingClass);
                    div.prop("id", model.rowData[ndx].ProductGuid);
                    div.html("- -");
                    col.append(div);
                //} else if (model.rowData[ndx].DisplayCodes[1] != "OPEN" && model.rowData[ndx].OrderQuantity == 0) {


                    //} else if (model.rowData[ndx].DisplayCode != "OPEN" && model.rowData[ndx].OrderQuantity == 0) {
                } else if (model.rowData[ndx].DisplayCodes[1] != "OPEN" && model.rowData[ndx].Availabilities[1] == 0) {
                    var div = $("<div></div>");
                    div.addClass(viewModel.config.orderQuantityNAClass);
                    div.prop("id", model.rowData[ndx].ProductGuid);
                    //div.html(model.rowData[ndx].DisplayCode + " : " + model.rowData[ndx].OrderQuantity);
                    div.html("-0-");
                    col.append(div);
                } else {
                    var input = $("<input type=\"number\"></input>");
                    input.addClass(viewModel.config.orderQuantityInputClass);
                    input.prop("id", model.rowData[ndx].ProductGuid);
                    //alert(model.rowData[ndx].ProductGuid);
                    input.prop("maxlength", '6');
                    input.val(model.rowData[ndx].OrderQuantity);
                    col.append(input);

                    if (model.rowData[ndx].OrderQuantity > 0) {
                        //this gets icon
                        if (model.rowData[ndx].IsCarted) {
                            col.append(viewModel.config.icons.cart_checked);
                        } else {
                            col.append(viewModel.config.icons.not_carted);
                        }
                    };
                };
                break;
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                if (model.rowData[ndx].DisplayCodes[c - 6] != "") {
                    col.text(model.rowData[ndx].DisplayCodes[c - 6]);
                } else {
                    col.text(numeral(model.rowData[ndx].Availabilities[c - 6]).format('0,0'));
                };
                break;
            default:
                col.text("cell(" + c + "," + ndx + ")");
        }
        rowElement.append(col);
    }
};

viewModel.displayGridRows = function (firstRow) {
    //firstRow not specified -> page Up
    //firstRow < 0 -> page down
    //firstRow > 0 -> got to that page

    if (viewModel.state.gridPageIsDisplaying) { return false; };
    viewModel.state.gridPageIsDisplaying = true;
    //Initialize Display Timing    
    var start = new Date();
    $("#test_time_interval_display").text("?");

    //$('.' + viewModel.config.orderQuantityInputClass).off("keydown").off("change");


    if (typeof (firstRow) == "undefined") {
        firstRow = viewModel.state.gridFirstRowDisplayed + 1;
    };

    if (firstRow < 0) {
        firstRow = viewModel.state.gridFirstRowDisplayed - 1;
    };

    if (firstRow < 0) { firstRow = 0; };
    if (firstRow >= model.state.rowsCount) { firstRow = model.state.rowsCount - 1; };  //grid is zero based

    
    
    //var rowNdx = firstRow;
    //cycle through grid elements
    for (var i = 0; i < viewModel.config.grid.rowsData; i++) {
        
        var el = $("#" + viewModel.config.grid.dataRowIdStem + i);
        var r = model.rowDataIndex[firstRow + i];


        if (model.isValidRowNdx(r)) {
            viewModel.displayGridRow(r, el);
        } else {
            el.empty();
        };
    };

    //capture first row
    viewModel.state.gridFirstRowDisplayed = firstRow;


    //define input box events
    viewModel.defineInputBoxEvents();
    
    //set focus on first input box
    //if (model.rowData.length > 0) {
    //    //ToDo: make this more robust - handle situation where no input boxes exist
    //    $('.' + viewModel.config.orderQuantityInputClass)[0].focus();
    //}



    //Display Elapsed Time
    $("#test_time_interval_display").text("| Page: " + viewModel.state.gridFirstRowDisplayed+"["+(new Date() - start) + " ms]");


    viewModel.state.gridPageIsDisplaying = false;



};

viewModel.displayShipWeekSelected = function () {
    $("#" + viewModel.config.grid.shipWeekDisplayId).text("Week: " + viewModel.state.shipWeekSelected.ShipWeekString);
}

viewModel.AdjustOrderQuantity = function (valu, max, min, mult) {

    var returnType = { "amount": valu, "message": null };

    var type = typeof valu;
    if (type === "undefined" || type !== "number" || valu <= 0) {
        returnType.amount = 0;
        return returnType;
    };
    var amt = valu;

    ///////////  Version 2 of Algorithm  /////////////////////
    //adjust multiple to at least 1
    if (mult == null || mult < 1) { mult = 1; };

    //adjust max to highest mult available
    max = Math.floor(max / mult) * mult;

    //adjust min to lowest multiple that meets minimum
    var v = mult;
    while (v < min) { v += mult; };
    min = v;

    if (min > max) {
        amt = 0;
        returnType.message = "Minimum order quantity not available!";
    } else {
        if (amt < min) {
            //raise amt to min
            amt = min;
        } else {
            if (amt > max) {
                //reduce amt to max
                amt = max;
                returnType.message = "Order quantity reduced to amount available.";
            } else {
                if (mult > 1) {
                    //adjust amt to nearest line item multiple
                    var v = Math.floor(amt / mult);
                    if ((amt % mult) >= (Math.floor(mult / 2))) { v++ };
                    amt = v * mult;
                };
            };
        };
    };
    /////////////////////////////////////////////////////////////////
    returnType.amount = amt;
    return returnType;
};

viewModel.handleOrderQuantityChange = function (guid) {
    
    if (guid == null || guid.length != 36) { return false; };
    var inputBox = $("#" + guid);   //get input element
    if (inputBox.length == 0) { return false; };  //no such element

    if (viewModel.state.orderQuantityBeingHandled) return false;    //avoid keydown and change events both firing

    viewModel.state.orderQuantityBeingHandled = true;

    var ndx = model.rowDataNdxFromProductGuid(guid);
    //var valu = parseInt(inputBox.val().replace(",", "")) || 0;

    var valu = inputBox.val();
    if (valu != null && valu != "") {
        //numeral.js cannot unformat empty or null string
        var round = Math.round;
        valu = parseInt(round(valu)) || 0;
    } else {
        valu = 0;
    };

    if (valu == model.rowData[ndx].OrderQuantity) {
        inputBox.val(valu);
        viewModel.state.orderQuantityBeingHandled = false;
        return false;
    };

    var result = viewModel.AdjustOrderQuantity(
        valu,
        (function () {
        //get max
        var max = model.returnAvailability(ndx);
        //if max is unlimited, set max = 4999999.
        if (model.returnAvailabilityIsUnlimited(ndx)) { max = 499999; };
        return max;
        })(),
        model.rowData[ndx].Min,
        model.rowData[ndx].Mult
    );
  
    //alert("amount: " + result.amount +", message: " + result.message);
    if (result.message !== null) {
        viewModel.floatMessage(inputBox, result.message, 2500);
    };
    var amt = result.amount;

    if (model.updateOrderQuantity(ndx, amt)) {
        //make call to update server
        viewModel.submitOrderQuantity(ndx);
    } else {
        //reformat input box
        if (amt < 0) { amt = 0; };
    };
  //  alert(amt);
    //format entry and display it
    inputBox.val(amt);
    
    viewModel.state.orderQuantityBeingHandled = false;
};

viewModel.floatMessage = function (element, content, interval) {
    //floats message to right of element
    if (typeof element !== 'undefined' && element != null || element.length > 0) {
        var position = element.offset();
        //ToDo: code to center floating message vertically with element
        //assume floating message is 35 px tall

        var top = position.top + ((element.height() - 35) /2);

        eps_tools.FloatingMessage(position.left + element.width() + 30, top, content, interval);
    };
};

viewModel.submitOrderQuantity = function (ndx) {
    //ToDo: code to Put Row and SessionGuid to Availability API
    
    if (!model.isValidRowNdx) { return false;};
    var rowData = model.rowData[ndx];

    var sessionGuid = rowData.ProductGuid;  //ToDo: fix this with real Session Guid
    //show working icon
    viewModel.showOrderIcon(rowData.ProductGuid, viewModel.config.icons.order_working);

    if (rowData.IsCarted) {
        //alert("update cart row#: " + ndx);

    } else {
        //alert("pre-cart row#: " + ndx);

    };
    
    var targetRequest = $("#test_request_url");
    var targetResponse = $("#test_response_data");
    //alert("Pre-cart row# " + ndx);

    $.ajax({
        url: model.config.urlAvailabilityApiCart,
        async: true,
        type: "put",
        data: {
            "SessionGuid": sessionGuid,
            "ShipWeekString": viewModel.state.shipWeekSelected.ShipWeekString,
            "RowData": rowData
        },
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            targetRequest.html("AJAX REQUEST: " + decodeURI(this.url) + " | Data Sent: " + decodeURI(this.data));
            //alert("AJAX url: " + this.url);
        },
        success: function (data, textStatus, jqXHR) {
            //console.log(data);
            //targetResponse.html("SERVER RESPONSE: <br />" + JSON.stringify(data));
            
            if(data.Success){
                model.refreshDataRow(data.RowData);
                viewModel.refreshOrderTotal();
            };
            
            //alert(JSON.stringify(data));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + decodeURI(this.url) + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            //reset icon
            viewModel.resetOrderIcon(rowData.ProductGuid);
            //viewModel.ProcessEventChain(callbacks);
        }
    });




};
viewModel.addToCart = function () {
    //var counts = model.OrderQuantityTotals();
    model.orderQuantityTotalsGet(function(counts) {
        if (counts.Success) {
            if (counts.Uncarted > 0) {
                //alert("Add " + counts.Uncarted + " items to your cart.");

                var targetRequest = $("#test_request_url");
                var targetResponse = $("#test_response_data");
                //Make ajax call to retrieve row data
                $.ajax({
                    type: "post",
                    data: {
                        "Category": model.state.queryString.category(),
                        "Form": model.state.queryString.form(),
                        "ShipWeekString": viewModel.state.shipWeekSelected.ShipWeekString
                    },
                    url: model.config.urlAvailabilityApiCart,
                    datatype: "json",
                    beforeSend: function (jqXHR, settings) {
                        targetRequest.html("AJAX REQUEST: " + decodeURI(this.url) + " | Data Sent: " + decodeURI(this.data));
                    },
                    success: function (response, textStatus, jqXHR) {
                        targetResponse.html("SERVER RESPONSE: <br />" + JSON.stringify(response));
                        if (response.Success) {
                            $.each(model.rowData, function (ndx, dataRow) {
                                if (dataRow.OrderQuantity > 0) { dataRow.IsCarted = true; };
                            });
                            viewModel.resetAllOrderIcons();
                           
                            viewModel.refreshOrderTotal();
                            //ToDo: Get location of Add-to-Cart button and locate floating message.

                            viewModel.floatMessage($("#" + viewModel.config.addToCartLinkId), 'Items added to your cart.', 2500);

                            //eps_tools.FloatingMessage(300, 300, 'Items added to your cart.', 2500);
                        } else {

                            eps_tools.OkDialog({ 'title': '<span>Error:</span>', 'content': '<br /><span>Add to Cart did not succeed!<br />&nbsp;</span>', 'modal': true });

                            //alert("Error: Add to Cart did not succeed!");
                        };

                        //alert(JSON.stringify(data));
                    },
                    //error: function (jqXHR, textStatus, errorThrown) {
                    //    alert("url: " + decodeURI(this.url) + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
                    //},
                    complete: function (jqXHR, textStatus) {
                        //viewModel.ProcessEventChain(callbacks);
                    }
                });
            } else {

                eps_tools.OkDialog({ 'title': '<span>Cart:</span>', 'content': '<br /><span>You have no uncarted items to add to your cart.<br />Enter order quantities and then submit to cart.<br />&nbsp;</span><br />', 'modal': true });
                //alert("You have no uncarted items to add to your cart.\nEnter order quantities and then re-submit to cart.");
            };
        };
    });

    
};
viewModel.addToOrder0 = function () {
    viewModel.state.AddToOrderGuid = $("#" + viewModel.config.cboAddToOrder).val();
    viewModel.state.AddToOrderNo = $("#" + viewModel.config.cboAddToOrder + " :selected").text();
    //alert( viewModel.state.AddToOrderNo);
      viewModel.addToOrder();

};

viewModel.addToOrder = function () {
    var growerOrderGuid = viewModel.state.AddToOrderGuid
    
  //  alert(growerOrderGuid);
    
    //var counts = model.OrderQuantityTotals();
    model.orderQuantityTotalsGet(function (counts) {
        if (counts.Success) {
            if (counts.Uncarted > 0) {
                //alert("Add " + counts.Uncarted + " items to your cart.");

                var targetRequest = $("#test_request_url");
                var targetResponse = $("#test_response_data");
                //Make ajax call to retrieve row data
                $.ajax({
                    type: "post",
                    data: {
                        "GrowerOrderGuid": growerOrderGuid,
                        "Category": model.state.queryString.category(),
                        "Form": model.state.queryString.form(),
                        "ShipWeekString": viewModel.state.shipWeekSelected.ShipWeekString
                    },
                    url: model.config.urlAvailabilityApiOrder,
                    datatype: "json",
                    beforeSend: function (jqXHR, settings) {
                        targetRequest.html("AJAX REQUEST: " + decodeURI(this.url) + " | Data Sent: " + decodeURI(this.data));
                    },
                    success: function (response, textStatus, jqXHR) {
                        targetResponse.html("SERVER RESPONSE: <br />" + JSON.stringify(response));
                        if (response.Success) {
                            //not carted
                            //$.each(model.rowData, function (ndx, dataRow) {
                            //    if (dataRow.OrderQuantity > 0) { dataRow.IsCarted = true; };
                            //});
                            viewModel.resetAllOrderIcons();
                           
                            viewModel.refreshOrderTotal();
                            
                            //  alert($("#" + viewModel.config.addToOrderLinkId0).data("orderGuid").orderguid);

                            viewModel.floatMessage($("#" + viewModel.config.addToOrderLinkId0), 'Items have been added to Order ' + viewModel.state.AddToOrderNo, 2500);

                            setTimeout(function () { window.location.href = '/MyAccount/Orders/open?OrderGuid=' + growerOrderGuid; }, 2500);
                           
                        } else {
                            eps_tools.OkDialog({ 'title': '<span>Error:</span>', 'content': '<br /><span>Add to Order did not succeed!<br />&nbsp;</span>', 'modal': true });
                        };
                    },
                    complete: function (jqXHR, textStatus) {
                    }
                });
            } else {

                eps_tools.OkDialog({ 'title': '<span>Add to Order:</span>', 'content': '<br /><span>You have no uncarted items to add to your order.<br />Enter order quantities and then Add to Order.<br />&nbsp;</span><br />', 'modal': true });
            };
        };
    });
};




    viewModel.nextInputGuid = function (guid) {
        if (guid == null || guid.length < 36) { return "" };
        if ($("#" + guid).length < 1) {return "" };

        var returnGuid = guid;
        var allInputs = $("." + viewModel.config.orderQuantityInputClass);
        $.each(
            allInputs, function(i, el){
                if (el.id == guid) {
                    //alert('bingo');
                    if ((i + 1) < allInputs.length) {
                        returnGuid = allInputs[i + 1].id;  //return next input box
                        //return returnInput;
                        //return allInputs[i + 1].id;  
                    } else {
                        returnGuid = allInputs[0].id;  //return first input box
                    };
                    return false;  //break out of $.each
                };
            });
        return returnGuid;
    };
    viewModel.orderTotalClear = function () {
        $("#" + viewModel.config.orderQuantityCartedDisplayId).text('');
        $("#" + viewModel.config.orderQuantityUncartedDisplayId).text('');
  
    };

    viewModel.refreshOrderTotal = function () {
        model.orderQuantityTotalsGet(function(counts) {
            if (counts.Success) {
                $("#" + viewModel.config.orderQuantityCartedDisplayId).text(numeral(counts.Carted).format('0,0'));
                $("#" + viewModel.config.orderQuantityUncartedDisplayId).text(numeral(counts.Uncarted).format('0,0'));
            //   alert("Bingo 3");
            };
        });


    };
    viewModel.refreshGrowerOrderList = function () {
        //alert("Hide or Show");
        model.growerGetOrderSumamries(function (summaries) {
        
            if (summaries.Success) {
                //alert(summaries.OrderCount);
                // alert(traverseObj(summaries));
                //ordersummeraies.GrowerOrderGuid
                if (summaries.OrderCount > 0) {
                    $("#" + viewModel.config.cboAddToOrder).empty();
                    $.each(summaries.OrderSummaries, function (i,item ) {
                      //  alert(item.OrderNo);
                        //  alert(item.GrowerOrderGuid);
                        var orderText = item.OrderNo;
                        if (item.CustomerPoNo != "")
                        {
                            orderText = orderText + " - " + item.CustomerPoNo;
                        }
                        $("#" + viewModel.config.cboAddToOrder)
                            .append($("<option></option>")
                                       .attr("value", item.GrowerOrderGuid)
                                       .text(orderText)
                                       );
                    });
                    $("#" + viewModel.config.addToOrderLinkId0).text("Add to Selected Order");
                    viewModel.ShowOrderLink(0);
                }
                else {
                    viewModel.HideOrderLink(0);
                }
                //if (summaries.OrderCount == 1) {
                //    //  alert("count 1");
                //    if (summaries.OrderSummaries[0].CustomerPoNo == ""){
                //    }
                //    else
                //    {
                //        $("#" + viewModel.config.addToOrderLinkId0).text("Add to Order " + summaries.OrderSummaries[0].OrderNo + " - " +  summaries.OrderSummaries[0].CustomerPoNo );
                //    }
                //    $("#" + viewModel.config.addToOrderLinkId0).data("order", { orderguid: summaries.OrderSummaries[0].GrowerOrderGuid, orderno: summaries.OrderSummaries[0].OrderNo });
                //    viewModel.ShowOrderLink(0);
                //    viewModel.HideOrderLink(1);
                //    viewModel.HideOrderLink(2);
                //}
                //if (summaries.OrderCount == 2) {
                //    //  alert("count 2");
                //    if (summaries.OrderSummaries[0].CustomerPoNo == "") {
                //        $("#" + viewModel.config.addToOrderLinkId0).text("Add to Order " + summaries.OrderSummaries[0].OrderNo);
                //    }
                //    else {
                //        $("#" + viewModel.config.addToOrderLinkId0).text("Add to Order " + summaries.OrderSummaries[0].OrderNo + " - " + summaries.OrderSummaries[0].CustomerPoNo);
                //    }
                //    $("#" + viewModel.config.addToOrderLinkId0).data("order", { orderguid: summaries.OrderSummaries[0].GrowerOrderGuid, orderno: summaries.OrderSummaries[0].OrderNo });
                //    viewModel.ShowOrderLink(0);
                //    if (summaries.OrderSummaries[1].CustomerPoNo == "") {
                //        $("#" + viewModel.config.addToOrderLinkId1).text("Add to Order " + summaries.OrderSummaries[1].OrderNo);
                //    }
                //    else {
                //        $("#" + viewModel.config.addToOrderLinkId1).text("Add to Order " + summaries.OrderSummaries[1].OrderNo + " - " + summaries.OrderSummaries[1].CustomerPoNo);
                //    }
                //    $("#" + viewModel.config.addToOrderLinkId1).data("order", { orderguid: summaries.OrderSummaries[1].GrowerOrderGuid, orderno: summaries.OrderSummaries[1].OrderNo });
                //    viewModel.ShowOrderLink(1);
        
                //    viewModel.HideOrderLink(2);
                //}
                //if (summaries.OrderCount > 2) {
                //    //  alert("count 3");
                //    if (summaries.OrderSummaries[0].CustomerPoNo == "") {
                //        $("#" + viewModel.config.addToOrderLinkId0).text("Add to Order " + summaries.OrderSummaries[0].OrderNo);
                //    }
                //    else {
                //        $("#" + viewModel.config.addToOrderLinkId0).text("Add to Order " + summaries.OrderSummaries[0].OrderNo + " - " + summaries.OrderSummaries[0].CustomerPoNo);
                //    }
                //    $("#" + viewModel.config.addToOrderLinkId0).data("order", { orderguid: summaries.OrderSummaries[0].GrowerOrderGuid, orderno: summaries.OrderSummaries[0].OrderNo });
                //    viewModel.ShowOrderLink(0);
                //    if (summaries.OrderSummaries[1].CustomerPoNo == "") {
                //        $("#" + viewModel.config.addToOrderLinkId1).text("Add to Order " + summaries.OrderSummaries[1].OrderNo);
                //    }
                //    else {
                //        $("#" + viewModel.config.addToOrderLinkId1).text("Add to Order " + summaries.OrderSummaries[1].OrderNo + " - " + summaries.OrderSummaries[1].CustomerPoNo);
                //    }
                //    $("#" + viewModel.config.addToOrderLinkId1).data("order", { orderguid: summaries.OrderSummaries[1].GrowerOrderGuid, orderno: summaries.OrderSummaries[1].OrderNo });
                //    viewModel.ShowOrderLink(1);
                //    if (summaries.OrderSummaries[2].CustomerPoNo == "") {
                //        $("#" + viewModel.config.addToOrderLinkId2).text("Add to Order " + summaries.OrderSummaries[2].OrderNo);
                //    }
                //    else {
                //        $("#" + viewModel.config.addToOrderLinkId2).text("Add to Order " + summaries.OrderSummaries[2].OrderNo + " - " + summaries.OrderSummaries[2].CustomerPoNo);
                //    }
                //    $("#" + viewModel.config.addToOrderLinkId2).data("order", { orderguid: summaries.OrderSummaries[2].GrowerOrderGuid, orderno: summaries.OrderSummaries[2].OrderNo });
                //    viewModel.ShowOrderLink(2);

               // }
           
            };
        });
    };

    viewModel.getDataDisplayGrid = function (scrollTo) {
        //scrollTo is optional variable, containing optional variety or product code 
        //console.log('two: ', scrollTo);

        viewModel.orderTotalClear();
        viewModel.innerGridClear();

        //display ship week selected
        viewModel.displayShipWeekSelected();
        //$("#" + viewModel.config.grid.shipWeekDisplayId).text("Week: " + viewModel.state.shipWeekSelected.ShipWeekString);

        //clear ship week headings
        for(var i = 0; i < 5; i++) {
            viewModel.headerCell(0, 6 + i).text('-');
        };

        var container = $("#" + viewModel.config.innerTableContainerId);
        container.empty();
        container.html('<img style="margin:0 0 0 235px;" src="/Images/icons/loading_page.gif" />');

        //old scrollbar
        var gridScrollbar = $("#grid_scrollbar_container");
        gridScrollbar.fadeOut('fast');

        //hide new scroll bar
        $("#new_paging_scrollbar_container").fadeOut('fast');

        model.getRowData([
            viewModel.innerGridDraw,
            function() {
                //if rows exist update State Cookie
                //alert('model.rowData.length = ' + model.rowData.length);
                //alert(model.state.hasRows());
                if (model.state.hasRows()) {
                    viewModel.stateCookieWrite();
                    if (typeof scrollTo === 'object' && scrollTo != null) {
                        //console.log('three: ', scrollTo);
                        if ((scrollTo.type || '').toLowerCase() == 'variety') {
                            viewModel.scrollBarScrollToVarietyCode(scrollTo.code||null);
                        } else if ((scrollTo.type || '').toLowerCase() == 'product') {
                            viewModel.scrollBarScrollToProductCode(scrollTo.code||null);
                        };
                    };
                };
            }
        ]);



    };
    viewModel.selectAllSuppliers = function (callbacks) {
        var listItems = $("#" + viewModel.config.supplierListDisplayId + " ul").children("li");
        var l = listItems.length;
        listItems.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
        viewModel.ProcessEventChain(callbacks);
    };
    viewModel.selectAllSpecies = function (callbacks) {
        var listItems = $("#" + viewModel.config.speciesListDisplayId + " ul").children("li");
        var l = listItems.length;
        listItems.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
        viewModel.ProcessEventChain(callbacks);

    };
    viewModel.ProcessEventChain = function (callbacks) {

        if (callbacks != undefined && callbacks.length > 0) {
            var callback = callbacks.shift();
            //alert(callback);
            callback(callbacks);
        }
    };

    viewModel.toggleAllSuppliers = function () {
        $("#" + viewModel.config.selectorAllSuppliersId).click();
    };
    viewModel.toggleAllSpecies = function () {
        $("#" + viewModel.config.selectorAllSpeciesId).click();
    };

    viewModel.drawOuterGrid = function () {
        var container = $("#" + viewModel.config.availabilityGridContainerId);



        //create outerTable
        var table = $("<table></table>");
        table.prop("id", viewModel.config.outerTableId);
        table.prop("cellpadding","0");
        table.prop("cellspacing", "0");
        //Draw Header Rows
        var thead = $("<thead></thead>");

        for (var r = 0; r < viewModel.config.grid.rowsHeader;r++){
            var row = $("<tr></tr>");
            row.addClass(viewModel.config.grid.headerRowClassStem + r);
            for (var c = 0; c < viewModel.config.grid.colsHeader;c++) {
                var col = $("<th></th>");
                //col.text("head("+r+", "+c+")");
                col.addClass(viewModel.config.grid.headerColClassStem + c);
                row.append(col);
            };
            thead.append(row);
        };

        table.append(thead);

        //Draw Grid Body
        var tbody = $("<tbody></tbody>");
        var row = $("<tr></tr>");
        var td = $("<td></td>");
        td.prop("colspan", viewModel.config.grid.colsData);

        //add inner table container
        var innerTableContainer = $("<div></div>");
        innerTableContainer.prop("id", viewModel.config.innerTableContainerId);
        //ajax_loading_lt_green_256.gif
        //ajax_loader_grey_128.gif
        //innerTableContainer.html('<img style="margin:75px 0 0 500px;" src="/Images/icons/working/ajax_loading_lt_green_256.gif" />');
        td.append(innerTableContainer);
        row.append(td);
        tbody.append(row);
        table.append(tbody);

        var tfoot = $("<tfoot></tfoot>");

        for (var r = 0; r < viewModel.config.grid.rowsFooter; r++) {
            var row = $("<tr></tr>");
            row.addClass(viewModel.config.grid.footerRowClassStem + r);
            for (var c = 0; c < viewModel.config.grid.colsFooter; c++) {
                var col = $("<td></td>");
                //col.text("foot(" + r + ", " + c + ")");

                col.addClass(viewModel.config.grid.footerColClassStem + c);
                row.append(col);
            };
            tfoot.append(row);
        };

        table.append(tfoot);

        container.append(table);    
    
        viewModel.displaySortableColumnHeadings();

    
    };
    viewModel.DisplayFooterActions = function () {

        if (!viewModel.state.FooterActionsDisplayed && model.allowOrdering) {
            //create spans to recieve Order Totals
            var span = $("<span></span>");
            span.prop("id", viewModel.config.orderQuantityCartedDisplayId);
            //span.text("Items in cart:")
            viewModel.footerCell(0, 4).prop("colspan", 4).text("Items in cart: ").append(span);

            var span = $("<span></span>");
            span.prop("id", viewModel.config.orderQuantityUncartedDisplayId);
            //span.text("Items not yet added to cart:")
            viewModel.footerCell(1, 4).prop("colspan", 4).text("Items not yet added to cart: ").append(span);
         

            //create span for add-to-cart link
            span = $("<span></span>");
            span.prop("id", viewModel.config.addToCartLinkId);
            span.text("Add to cart");
            span.on("click", viewModel.addToCart);
            viewModel.footerCell(2, 4).prop("colspan", 2).append(span);

            //create span for view cart link
            span = $("<span></span>");
            span.prop("id", viewModel.config.viewCartLinkId);
            span.text("View cart");
            span.on("click", function() {
                window.location.href = '/home/cart';
            });
            viewModel.footerCell(3, 4).prop("colspan", 2).append(span);

            //create span for Add-to-Order link
            span = $("<span></span>");
            span.prop("id", viewModel.config.addToOrderLinkId0);
            span.text("Add to Selected Order");
            span.on("click", viewModel.addToOrder0);
            viewModel.footerCell(4, 4).prop("colspan", 4).append(span);


            cboAddToOrder = $("<select></select>");
            cboAddToOrder.prop("id", viewModel.config.cboAddToOrder);
      
            viewModel.footerCell(4, 4).append(cboAddToOrder);

          

            ////create span for Add-to-Order link
            //span = $("<span></span>");
            //span.prop("id", viewModel.config.addToOrderLinkId1);
            //span.text("Add to Existing Order");
            //span.data("orderGuid", { orderguid: "guid 1" })
            //span.on("click", viewModel.addToOrder1);
            //viewModel.footerCell(5, 5).prop("colspan", 2).append(span);
     
            ////create span for Add-to-Order link
            //span = $("<span></span>");
            //span.prop("id", viewModel.config.addToOrderLinkId2);
            //span.text("Add to Existing Order");
            //span.data("orderGuid", { orderguid: "guid 1" })
            //span.on("click", viewModel.addToOrder2);
            //viewModel.footerCell(6, 5).prop("colspan", 2).append(span);

            viewModel.HideOrderLink(0);
            //viewModel.HideOrderLink(1);
            //viewModel.HideOrderLink(2);

            viewModel.state.FooterActionsDisplayed = true;
        }
    };

    viewModel.innerGridClear = function () {

        $("#" + viewModel.config.innerTableContainerId).empty();
        $("#" + viewModel.config.totalResultsId).html('&nbsp;');    //blank keeps grid from jumping during refresh

    };
    viewModel.innerGridDraw = function (callbacks) {

        viewModel.innerGridClear();

        var container = $("#" + viewModel.config.innerTableContainerId);

        var table = $("<table></table>");
        table.prop("id", viewModel.config.innerTableId);
        table.prop("cellpadding", "0");
        table.prop("cellspacing", "0");
        var thead = $("<thead></thead>");

        var tbody = $("<tbody></tbody>");
        table.append(thead);
        var max =    viewModel.config.grid.rowsData;
        if (max >model.state.rowsCount() ) {
            max = model.state.rowsCount();
        };

        for (var v = 0; v < viewModel.config.grid.rowsData; v++) {
            var row = $("<tr></tr>");
            row.prop("id", viewModel.config.grid.dataRowIdStem + v);
            tbody.append(row);
        };

        table.append(tbody);
        container.append(table);

        //display ship week headings
        $.each(model.shipWeekHeadings, function (i, item) {
            viewModel.headerCell(0, 6 + i).text(item);
        });

        //calc and show total rows found
        var totalRows = viewModel.state.filterImmediateAvailability ? model.countRowsWithAvailability() : model.rowData.length || 0;

        //console.log(totalRows);
        if (totalRows <= 0) {
            viewModel.searchDisplay();
            eps_tools.OkDialog({ 'title': '<span>No Records Found:</span>', 'content': "<br /><p>Your search criteria resulted in " + totalRows + " results.</p><p>Please uncheck the 'only view available products' option or change the week that you are searching to get availability listings.</p><p></p></span>", 'modal': true });
        };


        $("#" + viewModel.config.totalResultsId).text(numeral(totalRows).format('0,0') + " results");

        viewModel.DisplayFooterActions();

        //refresh the data row index - sorted and filtered
        model.refreshRowDataIndex();

        //show first page
        viewModel.displayGridRows(0);

        //calc and display Total
        viewModel.refreshOrderTotal();

        //Get the list to hide and show link
        viewModel.refreshGrowerOrderList();


        //ToDo: The gridScrollbar need not be reset, redisplayed every time a page is displayed
        $("#grid_scrollbar").slider("option", "max", viewModel.state.gridPages()-1);
        $("#grid_scrollbar").slider("option", "value", viewModel.state.gridPages()-1);
        $("#grid_scrollbar_container").fadeIn('fast');

        //viewModel.scrollBar.settings.value = 0;         //zero based
        //viewModel.scrollBar.settings.max = model.state.rowsCount()-1;   //note adjust for zero based

        //alert("count: " + model.state.rowsCount());

        viewModel.scrollBar.reset({ "value": 0, "max": model.state.rowsCount() - 1 });
        $("#new_paging_scrollbar_container").fadeIn('slow');


        viewModel.ProcessEventChain(callbacks);

    };

    viewModel.locateShipWeekList = function () {
        //called on init and when enter selector in case there has been a change of browser size
        var el = $("#" + viewModel.config.shipWeekSelectorId);
        var offset = el.offset();
        $("#" + viewModel.config.shipWeekListContainerId).css("left", offset.left - 4).css("top", offset.top + el.height() +7);
    };
    viewModel.displayShipWeekList = function () {
        $("#"+viewModel.config.shipWeekListContainerId).show('slow');
    };

    viewModel.composeShipWeekList = function (callbacks) {

        //alert('composeShipWeekList');
        var target = $("#" + viewModel.config.shipWeekListDisplayId);
        target.empty();
        var list = $("<ul />");

        $.each(model.shipWeekData.ShipWeeksList, function (index, value) {
            var li = $("<li></li>");
            li.prop("class", viewModel.config.shipWeekAvailableClass);
            li.prop("id", value.Guid);
            li.text(value.ShipWeekString)
            li.data("code", value.Code);
            //alert(value.Code);
            list.append(li);
            //$("#" + id).data("code", value.Code);
            //alert($("#" + id).data("code"));
        });
        target.append(list);
        var listItems = target.children("ul");
        listItems = listItems.children("li");

        listItems.on("click", function () {
            var thisItem = $(this);
            var id = thisItem.prop("id");
            //alert(id);
            $.each(model.shipWeekData.ShipWeeksList, function (index, item) {
                if (item.Guid == id) {
                    viewModel.state.shipWeekSelected = item;
                    return false;
                };
            });
            $("#" + viewModel.config.shipWeekListContainerId).hide();
            //alert("Selected ShipWeek is now: " + viewModel.state.shipWeekSelected.ShipWeekString);
            //$("tr.headerRow_0 th.headerCol_5").html(viewModel.state.shipWeekSelected.Week + "&nbsp;&nbsp;");
            //viewModel.displayShipWeekSelected();
            viewModel.getDataDisplayGrid();
        });
        viewModel.ProcessEventChain(callbacks);

    };

    viewModel.composeSupplierList = function (callbacks) {

        var target = $("#" + viewModel.config.supplierListDisplayId);
        target.empty();
        var list = $("<ul></ul>");
        var allSelector = $("<li></li>");
        allSelector.text("all");
        allSelector.prop("id", viewModel.config.selectorAllSuppliersId);
        allSelector.data("code", "");
        allSelector.prop("class", viewModel.config.selectionAvailableClass);
        list.append(allSelector);

        $.each(model.supplierData, function (index, value) {
            var li = $("<li></li>");

            //var className = viewModel.config.selectionAvailableClass;
            //if (!viewModel.state.supplierListComposed) { className = viewModel.config.selectionSelectedClass; };
            //li.prop("class", className);

            li.prop("class", viewModel.config.selectionAvailableClass);
            li.text(value.Name);
            li.data("code", value.Code);
            //alert(value.Code);
            list.append(li);
            //$("#" + id).data("code", value.Code);
            //alert($("#" + id).data("code"));
        });
        target.append(list);
        var listItems = target.children("ul");
        listItems = listItems.children("li");
        //alert(listItems.length);

        listItems.on("click", function () {
            var thisItem = $(this);
            var listItems = $("#" + viewModel.config.supplierListDisplayId + " ul").children("li"); //includes the select all item at [0]
            var selectedSuppliersCountBeginning = $("#" + viewModel.config.supplierListDisplayId + " ul").children("li." + viewModel.config.selectionSelectedClass).not("li#" + viewModel.config.selectorAllSuppliersId).length || 0;
            var allSuppliersSelected = false;
            if (thisItem.prop("id") == viewModel.config.selectorAllSuppliersId) {
                //select all suppliers was clicked;
                allSuppliersSelected = true;
                if (thisItem.hasClass(viewModel.config.selectionSelectedClass)) {
                    //deselecting all items
                    listItems.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
                } else {
                    //selecting all items
                    listItems.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
                };
            } else {
                if (thisItem.hasClass(viewModel.config.selectionSelectedClass)) {
                    //selected item was unselected
                    thisItem.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
                    $("#" + viewModel.config.selectorAllSuppliersId).removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
                } else {
                    //unselected item was selected
                    thisItem.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
                }
            }


            var selectedSuppliersCountEnding = $("#" + viewModel.config.supplierListDisplayId + " ul").children("li." + viewModel.config.selectionSelectedClass).not("li#" + viewModel.config.selectorAllSuppliersId).length || 0;
            if (allSuppliersSelected || (selectedSuppliersCountBeginning == 0 && selectedSuppliersCountEnding == 1)) {
                //this is the first one selected
                $("#" + viewModel.config.selectorAllSpeciesId).removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
            };
            if ($("#" + viewModel.config.selectorAllSpeciesId).hasClass(viewModel.config.selectionSelectedClass)) {
                //alert('bingo');
                model.getSpeciesData([viewModel.composeSpeciesList, viewModel.selectAllSpecies]); //suppler change with all species selected, reload species list with all selected
            } else {
                model.state.userSelections.species = viewModel.state.selections.getSelectedSpeciesList();       //archive the current species selections
                model.getSpeciesData([viewModel.composeSpeciesList, viewModel.selectSpecies]);                  //When suppler selections change, reload species list
            };
        });


        viewModel.state.supplierListComposed = true;
        viewModel.ProcessEventChain(callbacks);

    };

    viewModel.selectSpecies = function () {
        //This uses viewModel.config.userSelections.Species to reset the selection of species after a suppler is selected or deselected.
        //alert('viewModel.selectSpecies');

        var species = $("#" + viewModel.config.speciesListDisplayId + " ul").children("li");

        //alert('count: ' + species.length);

        if ((model.state.userSelections.species.length || 0) > 0) {
            var testString = "," + model.state.userSelections.species + ",";
            $.each(species, function (i, item) {
                //alert($(item).data("code"));
                var testCode = "," + $(item).data("code") + ",";
                if (testString.indexOf(testCode) >= 0) {
                    $(item).prop("class", viewModel.config.selectionSelectedClass);

                } else {
                    $(item).prop("class", viewModel.config.selectionAvailableClass);

                };
            });
        };

    };
    viewModel.HideOrderLink = function (i) {
        //  alert("in hide -- " + i);
        //  i = Math.round(i);
        // alert(i);

        if(i == 0)
        {
            // alert("hi 0");
            $("#" + viewModel.config.addToOrderLinkId0).hide();
            $("#" + viewModel.config.cboAddToOrder).hide();
            
        }
        //if(i == 1)
        //{
        //    // alert("hi 1");
        //    $("#" + viewModel.config.addToOrderLinkId1).hide();
        //}
        //if(i == 2)
        //{
        //    // alert("hi 2");
        //    $("#" + viewModel.config.addToOrderLinkId2).hide();
        //}

    }
    viewModel.ShowOrderLink = function (i) {
        // alert("in show -- " + i);
        //   id = Math.round(id);

        if(i == 0)
        {
            //   alert("sh 0");
            $("#" + viewModel.config.addToOrderLinkId0).show();
            $("#" + viewModel.config.cboAddToOrder).show();
        }
        //if(i == 1)
        //{
        //    //   alert("sh 1");
        //    $("#" + viewModel.config.addToOrderLinkId1).show();
        //}
        //if(i == 2)
        //{
        //    //  alert("sh 2");
        //    $("#" + viewModel.config.addToOrderLinkId2).show();
        //}
    }
  
    viewModel.composeSpeciesList = function (callbacks) {
        //alert('composing species list');
        var target = $("#" + viewModel.config.speciesListDisplayId);
        target.empty();
        var list = $("<ul></ul>");
        var allSelector = $("<li></li>");
        allSelector.text("all");
        allSelector.prop("id", viewModel.config.selectorAllSpeciesId);
        allSelector.data("code", "");
        allSelector.prop("class", viewModel.config.selectionAvailableClass);
        list.append(allSelector);
        //alert("bingo");

        $.each(model.speciesData, function (index, value) {
            var li = $("<li />");

            //var className = viewModel.config.selectionAvailableClass;
            //if (!viewModel.state.speciesListComposed) { className = viewModel.config.selectionSelectedClass; };
            //li.prop("class", className);

            li.prop("class", viewModel.config.selectionAvailableClass);
            li.text(value.Name)
            li.data("code", value.Code);
            //alert(value.Code);
            list.append(li);
            //$("#" + id).data("code", value.Code);
            //alert($("#" + id).data("code"));
        });
        target.append(list);
        var listItems = target.children("ul");
        listItems = listItems.children("li");
        //alert(listItems.length);

        listItems.on("click", function () {
            var thisItem = $(this);
            var listItems = $("#" + viewModel.config.speciesListDisplayId + " ul").children("li");
            if (thisItem.prop("id") == viewModel.config.selectorAllSpeciesId) {
                //alert("you clicked all species");
                if (thisItem.hasClass(viewModel.config.selectionSelectedClass)) {
                    listItems.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
                } else {
                    listItems.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
                };
            } else {

                if (thisItem.hasClass(viewModel.config.selectionSelectedClass)) {
                    thisItem.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
                    $("#" + viewModel.config.selectorAllSpeciesId).removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
                } else {
                    thisItem.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
                }
            }
        });

        viewModel.state.speciesListComposed = true;
        viewModel.ProcessEventChain(callbacks);

    };

    viewModel.showOrderIcon = function (guid, icon) {
        viewModel.clearOrderIcon(guid);
        $("#" + guid).after(icon).delay(10);
    };
    viewModel.clearOrderIcon = function (guid, icon) {
        var elements = $("#" + guid).siblings("img");
        //var i = elements.length;
        elements.remove();
        //$.remove(elements);
    };
    viewModel.resetOrderIcon = function (guid) {
        var ndx = model.rowDataNdxFromProductGuid(guid);
        if (!model.isValidRowNdx(ndx)) { return false; };
        viewModel.clearOrderIcon(guid);
        if (model.rowData[ndx].IsCarted) {
            viewModel.showOrderIcon(guid, viewModel.config.icons.cart_checked);
            return true;
        };
        if (model.rowData[ndx].OrderQuantity>0) {
            viewModel.showOrderIcon(guid, viewModel.config.icons.not_carted);
            return true;
        };

    };
    viewModel.resetAllOrderIcons = function () {
        var inputBoxes = $("." + viewModel.config.orderQuantityInputClass);
        $.each(inputBoxes, function (i, el) {
            //var guid = el.id
            viewModel.resetOrderIcon(el.id);
        });
    };

    viewModel.clearAllOrderIcons = function () {
        var elements = $("." + viewModel.config.orderQuantityInputClass).siblings("img").remove();
    };


    model.growerGetOrderSumamries = function (onSuccess) {
        console.log(onSuccess);
        var targetRequest = $("#test_request_url");
        var targetResponse = $("#test_response_data");
        //Make ajax call to retrieve row data
        $.ajax({
            type: "get",
            data: {
                "ShipWeekCode": viewModel.state.shipWeekSelected.ShipWeekString
            },

            url: model.config.urlGetOrderSummaries,
            datatype: "json",
            beforeSend: function (jqXHR, settings) {
                if ((targetRequest.length || 0) > 0) {
                    targetRequest.text("AJAX REQUEST: " + decodeURI(this.url));
                };
                //alert("AJAX url: " + this.url);
            },
            success: function (data, textStatus, jqXHR) {
                if (typeof onSuccess === "function") {
                    onSuccess(data);
                };
                //model.state.orderTotals = data;
                //if ((targetResponse.length || 0) > 0) {
                //    targetResponse.html("SERVER RESPONSE: <br />" + JSON.stringify(model.state.orderTotals));
                //};
                //  alert(JSON.stringify(data));
            },
            //error: function (jqXHR, textStatus, errorThrown) {
            //    alert("url: " + decodeURI(this.url) + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
            //},
            complete: function (jqXHR, textStatus) {
                //viewModel.ProcessEventChain(callbacks);
            }
        });

    };



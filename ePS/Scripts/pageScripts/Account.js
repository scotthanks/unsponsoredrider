﻿
var model = {};
model.lookupData = {};
model.registrationData = {};

model.stateData = [];
model.provinceData = [];
model.getStateProvinceData = function (callback) {
        var url = '/Scripts/Json/StateData.js';
        var jqxhr = $.getJSON(url, function (data) {
            model.stateData = data;
            //alert(traverseObj(model.stateData, false));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown);
        })
        .done(
        function () {
            var url = '/Scripts/Json/ProvinceData.js';
            var jqxhr = $.getJSON(url, function (data) {
                model.provinceData = data;
                //alert(traverseObj(model.provinceData, false));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                alert("TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown);
            })
            .done(callback);

        });

};



var viewModel = {};

viewModel.config = {
        registration_displayId: "registration_display",
        buttons: {
            lookupFormId: "lookup_form",
            registrationFormId: "registration_form",
            lookupFormSubmitButtonId: "lookup_form_submit_button",
            registrationFormSubmitButtonId: "create_account_button",
            registrationFormCancelButtonId: "cancel_account_button",
            copyCompanyPhoneId: "copy_from_company_phone",
            copyCompanyEmailId: "copy_from_company_email",

        },
        urls: {
            registrationApi: "/api/account/"
        }
    };

viewModel.init = function () {

/////////////////// Test Buttons ////////////////////////
    //$("#test_popup_message").button().on("click", function () {
    //    //alert("PopUp Message Button Pressed");
    //    viewModel.popUpMessage("Test Message", "Message Header", 250, 200);

    //});
    //$("#test_floating_message").button().on("click", function () {
    //    //alert("Button Pressed");
    //    x = 450; y = 550; msg = "hello there..."; interval = 1250;
    //    viewModel.floatMessageDefine(x, y, msg, interval)

    //});
    //$("#test_popup_confirmation").button().on("click", function () {
    //    //alert("Button Pressed");
    //    //x = 200; y = 350; msg = "hello there..."; interval = 2000;
    //    //viewModel.floatMessageDefine(x, y, msg, interval)
    //    viewModel.showPopUpDialog();

    //});
    //$("#test_set_country").button().on("click", function () {
    //    viewModel.setCountryValues();

    //});

    //$("#test_display_inputs").button().on("click", function () {
    //    viewModel.collectRegistrationData();
    //    var resultDisplay = $("#test_area_results_display");
    //    resultDisplay.empty();
    //    resultDisplay.html("<span>REGISTRATION DATA:</span><br>");
    //    resultDisplay.html(JSON.stringify(model.registrationData));
    //});

    ////////////////////////////////////////////////////////////

    //State and Province Data
    model.getStateProvinceData(viewModel.setCountryValues);

    //Input Masks
    $("#CompanyPhone").mask("(999) 999-9999");
    $("#PersonPhone").mask("(999) 999-9999");
    //$("#ZipCode").mask("99999");
    //$("#CompanyPhone").mask("(999) 999-9999", { completed: function () { alert("completed!"); } });
    //$("#PersonPhone").mask("(999) 999-9999", { completed: function () { alert("completed!"); } });

    $("input[type='radio'][name='country_code']").on("change", function () {
        viewModel.setCountryValues();
    });

    $("input[type='checkbox'][id='ClearOtherChoices']").on("change", function () {
        var wasChecked = $(this).prop("checked");
        $("input[type='checkbox'][id='SubscribeToNewsLetter']").prop("checked", !wasChecked);
        $("input[type='checkbox'][id='SubscribeToPromotions']").prop("checked", !wasChecked);
    });
    $("input[type='checkbox'][id='SubscribeToNewsLetter'], input[type='checkbox'][id='SubscribeToPromotions']").on("change", function () {
        var subChecked = $("input[type='checkbox'][id='SubscribeToNewsLetter']").prop("checked");
        var promoChecked = $("input[type='checkbox'][id='SubscribeToPromotions']").prop("checked");
        var clearChecked = (subChecked || promoChecked) ? false : true;
        $("input[type='checkbox'][id='ClearOtherChoices']").prop("checked", clearChecked);
    });

    $("#" + viewModel.config.buttons.registrationFormCancelButtonId).on("click", function () {
        var resultDisplay = $("#test_area_results_display");
        resultDisplay.text("Cancel Button Pressed...");
        window.history.back();
    });

    $("#" + viewModel.config.buttons.registrationFormSubmitButtonId).on("click", function () {
        $("#"+viewModel.config.buttons.registrationFormId).submit();
        //alert("Create Button Pressed\n...see result in test window below.");

    });

    viewModel.defineCopyCompanyInfoEvents();
    viewModel.defineItemValidationEvents();
};

viewModel.defineItemValidationEvents = function () {

    $("#CompanyName, #StreetAddress1, #City, #CompanyEmail, #FirstName, #LastName, #PersonEmail").on("blur", function () {
        //alert('bingo');
        var v = $(this).val();
        var label = $("label[for='" + $(this).prop('id') + "']");
        var className = "valid_fields";
        if (typeof v === 'undefined' || v == null || v.length < 1) {
            viewModel.floatMessage(label, "Reminder: this field is required", 2000);
            className = 'invalid_fields';
        };
        label.removeClass().addClass(className);
    });

    $("#CompanyPhone, PersonPhone").on("blur", function () {
        //Note:  Do not clear events for these because that clears masking as well
        var v = $(this).val();
        var label = $("label[for='" + $(this).prop('id') + "']");
        var className = "valid_fields";
        if (typeof v === 'undefined' || v == null || v.length <= 0) {
            viewModel.floatMessage(label, "Reminder: Company phone # is required", 2000);
            className = 'invalid_fields';
        };
        label.removeClass().addClass(className);
    });

    $("#ZipCode").on("blur", function () {
        var v = $(this).val().trim().replace(" ","");
        $(this).val(v);
        var label = $("label[for='" + $(this).prop('id') + "']");
        var className = "valid_fields";
        //if (typeof v === 'undefined' || v == null || v.length != 5) {
        if (typeof v === 'undefined' || v == null || v.length < 5) {
                viewModel.floatMessage(label, "Reminder: code is required", 2000);
            className = 'invalid_fields';
        };
        label.removeClass().addClass(className);
    });

    $("#Password").on("blur", function () {
        var v = $(this).val().trim().replace(" ", "");
        $(this).val(v);
        var label = $("label[for='" + $(this).prop('id') + "']");
        var className = "valid_fields";
        if (typeof v === 'undefined' || v == null || v.length < 6 || v.length > 12) {
            viewModel.floatMessage(label, "Reminder: 6 to 12 characters required.", 2000);
            className = 'invalid_fields';
        };
        label.removeClass().addClass(className);
        //$("#Password2").val('');
        //$("#Password2").blur();
    });

    $("#Password2").on("blur", function () {
        //alert('bingo');
        var v = $(this).val().trim().replace(" ", "");
        $(this).val(v);
        var label = $("label[for='" + $(this).prop('id') + "']");
        var className = "valid_fields";
        if (typeof v === 'undefined' || v == null || v.length <6 || v.length>12 ||v != $('#Password').val()) {
            if (v.length > 0) {
                viewModel.floatMessage(label, "Reminder: passwords must match.", 2000);
            }
            className = 'invalid_fields';
        };
        label.removeClass().addClass(className);
    });
};

viewModel.defineCopyCompanyInfoEvents = function () {
    $("#" + viewModel.config.buttons.copyCompanyPhoneId).on("click", function () {
        var companyPhone = $("#CompanyPhone").val();
        //alert(companyPhone);
        $("#PersonPhone").val(companyPhone);

    });
    $("#" + viewModel.config.buttons.copyCompanyEmailId).on("click", function () {
        var companyEmail = $("#CompanyEmail").val();
        //alert(companyEmail);
        $("#PersonEmail").val(companyEmail);
    });
};

viewModel.submitRegistrationForm = function () {
    //alert("submitRegistrationForm");


    viewModel.collectRegistrationData();

    //alert(JSON.stringify(model.registrationData));


    if (viewModel.validateRegistrationData()) {
        viewModel.submitRegistrationData();
    };
};

viewModel.collectRegistrationData = function () {
    var form = $("#" + viewModel.config.buttons.registrationFormId)

    var values = {};
    var textInputs = $("#" + viewModel.config.buttons.registrationFormId + " :input[type=text]")
    textInputs.each(function () {
        values[this.id] = $(this).val().trim();
    });

    var stateCode = $("#StateCode").val() || "";
    values["StateCode"] = stateCode;

    var country = $("#" + viewModel.config.buttons.registrationFormId + " input[type='radio'][name='country_code']:checked").val() || "";
    values["Country"] = country;

    var password = ($("#" + viewModel.config.buttons.registrationFormId + " input[type='password'][id='Password']").val() || "").trim();
    values["Password"] = password;
    var companyTypeCode = $("#" + viewModel.config.buttons.registrationFormId + " input[type='radio'][name='CompanyTypeCode']:checked").val() || "";
    values["CompanyTypeCode"] = companyTypeCode;
    var roleCode = $("#" + viewModel.config.buttons.registrationFormId + " input[type='radio'][name='RoleCode']:checked").val() || "";
    values["RoleCode"] = roleCode;

    
    values["SubscribeToNewsLetter"] = $("input[type='checkbox'][id='SubscribeToNewsLetter']").prop("checked");
    values["SubscribeToPromotions"] = $("input[type='checkbox'][id='SubscribeToPromotions']").prop("checked");

    //var subscribeToPromotions = $("#" + viewModel.config.buttons.registrationFormId + " input[type='checkbox'][id='SubscribeToPromotions']:checked").val() || "";
    //values["SubscribeToPromotions"] = (subscribeToPromotions == "on");

    model.registrationData = values;


};

viewModel.validateRegistrationData = function () {

    //return true;

    //#CompanyName, #StreetAddress1, #City, #CompanyEmail, #FirstName, #LastName, #PersonPhone, #PersonEmail"
    var isValid = true;
    var messages = [];
    viewModel.collectRegistrationData();
    //alert(traverseObj(model.registrationData, false));


    //Password Lengths
    var password = model.registrationData.Password;
    if (password.length < 6 || password.length > 12) {
        isValid = false;
        messages.push("<p>Password must be 6 to 12 characters!</p>");
    };

    //Password Match
    var password2 = $("#Password2").val().trim();
    if (password2 != model.registrationData.Password) {
        isValid = false;
        messages.push("<p>Passwords must match!</p>");
    };
    //Company Name
    if (model.registrationData.CompanyName.length < 1) {
        isValid = false;
        messages.push("<p>Company name is required!</p>");
    };
    //StreetAddress1
    if (model.registrationData.StreetAddress1.length < 1) {
        isValid = false;
        messages.push("<p>Street address is required!</p>");
    };
    //City Name
    if (model.registrationData.City.length < 1) {
        isValid = false;
        messages.push("<p>City name is required!</p>");
    };
    //Company Email
    if (model.registrationData.CompanyEmail.length < 1) {
        isValid = false;
        messages.push("<p>Company Email address is required!</p>");
    };
    //First Name
    if (model.registrationData.FirstName.length < 1) {
        isValid = false;
        messages.push("<p>First name is required!</p>");
    };
    //Last Name
    if (model.registrationData.LastName.length < 1) {
        isValid = false;
        messages.push("<p>Last name is required!</p>");
    };
    //Person Phone
    if (model.registrationData.PersonPhone == "") {
        isValid = false;
        messages.push("<p>Account Phone # is required!</p>");
    };
    //Company Name
    if (model.registrationData.PersonEmail.length < 5) {
        isValid = false;
        messages.push("<p>Account email address is required!</p>");
    };



    if (!isValid) {
        var msg = "<h3>Your request needs attention:</h3>";
        $.each(messages, function (i, item) {
            msg += item;
        });

        msg += "<p>Please fix these omissions and re-submit.</p>";
        eps_tools.OkDialog({ 'title': '<span>Error:</span>', 'content': msg, 'modal': true });
    };

    return isValid;






    //var d = {};
    //d.CompanName = $("#CompanyName").val().trim();
    //d.StreetAddress1 = $("#StreetAddress1").val().trim();
    //d.City = $("#City").val().trim();
    //d.CompanyEmail = $("#CompanyEmail").val().trim();
    //d.FirstName = $("#FirstName").val().trim();
    //d.LastName = $("#LastName").val().trim();
    //d.PersonPhone = $("#PersonPhone").val().trim();
    //d.PersonEmail = $("#PersonEmail").val().trim();
    //d.CompanyPhone = $("#CompanyPhone").val().trim();
    //d.ZipCode = $("#ZipCode").val().trim();
    //d.Password = $("#Password").val().trim();
    //d.Password2 = $("#Password2").val().trim();

    //alert(traverseObj(d,false));




};

viewModel.floatMessage = function (element, content, interval) {
    //floats message to right of element
    //if no element then does nothing
    if (typeof element !== 'undefined' && element != null && element.length > 0) {
        var position = element.offset();
        //ToDo: code to center floating message vertically with element
        //assume floating message is 35 px tall

        var top = Math.floor(position.top + ((element.height() - 35) / 2));
        var left = Math.floor(position.left + element.width() + 30);
        eps_tools.FloatingMessage(left, top, content, interval);
    };
};

viewModel.submitRegistrationData = function () {
    var resultDisplay = $("#test_area_results_display");
    //alert(traverseObj(model.registrationData));
    $.ajax({
        type: "put",
        data: model.registrationData,
        url: viewModel.config.urls.registrationApi,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            var requestMsg = $("<div><hr />AJAX REQUEST: [ "+this.type+" ] " + decodeURI(this.url) + "<br >DATA SENT: " +JSON.stringify(this.data) + "</div>");
            resultDisplay.append(requestMsg);
        },
        success: function (response, textStatus, jqXHR) {
            //alert(traverseObj(response,false));

            if (response.Success) {
                window.location.href = "/";
            } else {
                var content = "<h5>Problem occurred:</h5>";
                $.each(response.FormErrorList, function (i, item) {
                    content += "<p>" + i + ". Item: " + item.FieldName + ": " + item.Message + "</p>";
                });
                content += "<p>... please correct and resubmit.</p>";
                eps_tools.OkDialog({ 'title': '<span>Error:</span>', 'content': content, 'modal': true });
            };

            //resultDisplay.append($("<div><hr />AJAX RESPONSE: " + JSON.stringify(response) + "</div>"));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + decodeURI(this.url) + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
        }
    });
};


viewModel.collectLookupData = function () {
    var form = $("#" + viewModel.config.buttons.lookupFormId);
    var textInputs = $("#" + viewModel.config.buttons.lookupFormId + " :input[type=text]")
    var values = {};
    textInputs.each(function () {
        values[this.id] = $(this).val();
    });
    model.lookupData = values;
};
viewModel.validateLookupData = function () {

};

viewModel.submitLookupForm = function () {
    //alert("submitLookupForm");

    var resultDisplay = $("#test_area_results_display");
    var div = $("<div></div>");
    div.text("lookup form submitting...");
    resultDisplay.empty().html(div);

    viewModel.collectLookupData()
    viewModel.validateLookupData();






    div.text(JSON.stringify(model.lookupData));
    resultDisplay.append(div);
};


viewModel.setCountryValues = function () {
    var country = $("#" + viewModel.config.buttons.registrationFormId + " input[type='radio'][name='country_code']:checked").val() || "";

//    var companyTypeCode = $("#" + viewModel.config.buttons.registrationFormId + " input[type='radio'][name='CompanyTypeCode']:checked").val() || "";

    //alert(country);


    var select = $("#StateCode");
    select.empty();
    var arr = [];
    var lbl = $("#state_province_selection_label");
    if (country == "ca") {
        arr = model.provinceData;
        lbl.text("Province");
    } else {
        arr = model.stateData;
        lbl.text("State");
    };
    $.each(arr, function (i, item) {
        var option = $("<option></option>");
        option.text(item.Name);
        option.prop("value", item.Abbr);
        select.append(option);

    });

    //alert($("#StateCode").val());

}



﻿
model = {};

model.InvoicesData = {};    //ToDo: Change this to just the array

model.config = {
    api: {
        urlInvoicesDataApi: "/api/claims/",
    },
};

model.GetInvoiceData = function (callbacks) {
    //alert("bingo");
    $("#test_area_results_display_0, #test_area_results_display_1").empty();
    $.ajax({
        datatype: "json",
        url: model.config.api.urlInvoicesDataApi,
        type: "get",
        beforeSend: function (jqXHR, settings) {
            //clear current state
            $("#test_area_results_display_0").append("<span>URI: " + this.url + "</span>");
            model.InvoicesData = {};
        },
        success: function (response, textStatus, jqXHR) {
            model.InvoicesData = response;
            $("#test_area_results_display_1").append("<div>Result:<br />" + traverseObj(model.InvoicesData) + "</div>");
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {

            viewModel.processEventChain(callbacks);
        }
    });

};



viewModel = {}

viewModel.Init = function () {

    //////////////////// Test Buttons ///////////////////////////////
    $("#test_api_one").button().on("click", function () {

        model.GetInvoiceData();

    });


};



////////////////////// Utilities /////////////////////////
viewModel.processEventChain = function (callbacks) {

    if (typeof callbacks !== 'undefined' && callbacks != null && callbacks.length > 0) {
        var callback = callbacks.shift();
        //alert(callback);
        callback(callbacks);
    }
};

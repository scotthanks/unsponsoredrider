﻿
var model = {};
model.configData = {
    isConfigured: false,
    urlSupplierApi: '/api/supplier/',
    urlSpeciesApi: '/api/species/',
    urlVarietiesApi: '/api/varieties/',
    urlVarietyDetailPage: '/home/ProductDetail/',

    supplierContainerId: 'supplier_selection_container',
    speciesContainerId: 'species_selection_container',

    supplierListId: 'supplier_selection_list',
    speciesListId: 'species_selection_list',
    varietySetsDisplayId: 'variety_sets_display',
    varietySetClass: 'variety_set',
    varietyTileClass: 'variety_set_list_item',
    urlGetSupplierData: function () { return model.configData.urlSupplierApi + "-1?PlantCategoryCode=" + model.selectionState.plantCategoryCode + "&ProductFormCode=" + model.selectionState.productFormCode; },
    urlGetSpeciesData: function () { var url = model.configData.urlSpeciesApi + "-1?PlantCategoryCode=" + model.selectionState.plantCategoryCode + "&ProductFormCode=" + model.selectionState.productFormCode + "&SupplierCodes=" + model.selectionState.selectedSupplierCodes(); return url; },
    urlGetVarietySetsData: function () { return model.configData.urlVarietiesApi + "-1?PlantCategoryCode=" + model.selectionState.plantCategoryCode + "&ProductFormCode=" + model.selectionState.productFormCode + "&SupplierCodes=" + model.selectionState.selectedSupplierCodes() + "&SpeciesCodes=" + model.selectionState.selectedSpeciesCodes(); },
    urlGoToVarietyDetailPage: function (varietyCode) { return model.configData.urlVarietyDetailPage + "varietyDetailAvailability?plantCategoryCode=" + model.selectionState.plantCategoryCode + "&ProductFormCode=" + model.selectionState.productFormCode + "&supplierCodes=" + model.selectionState.selectedSupplierCodes() + "&varietyCode=" + varietyCode + "&shipWeek=30|2013"; }
};
model.selectionState = {
    plantCategoryCode: "",
    productFormCode: "",
    selectedSupplierCodes: function () {
        //Returns supplier codes currently selected
        var returnArray = [];
        var items = $("#" + model.configData.supplierListId + " li.selection_selected");
        $.each(items, function (i, value) { returnArray.push($(this).data("code")); });
        return returnArray;
    },
    selectedSpeciesCodes: function () {
        //Returns species codes currently selected
        var returnArray = [];
        var items = $("#" + model.configData.speciesListId + " li.selection_selected");
        $.each(items, function (i, value) { returnArray.push($(this).data("code")); });
        return returnArray;
    }
};
model.displayState = {
    DisplayPageId: "variety_sets_page",
    DisplayPageCountId: "variety_sets_page_count",
    DisplayVarietySetsCountId: "variety_sets_count",
    DisplayVarietiesCountId: "varieties_count",

    
    Columns: 8,
    Rows: 10,
    CurrentPage: 1,
    TotalPages: 5,
    VarietySetCount: 33,
    VarietiesCount: 400,
    Calc: function () {
        //model.VaritySetsData.length;
        this.VarietySetCount = (model.VaritySetsData.length || 0);
        this.TotalPages = Math.floor(this.VarietySetCount / 3); if (this.VarietySetCount % 3 > 0) { this.TotalPages++; };
        model.displayState.VarietiesCount = 0;
        $.each(model.VaritySetsData, function (i, set) {  //foreach variety Set
            $.each(set.SelectedVarieties, function (ii, variety){
                model.displayState.VarietiesCount++;
            });           
        });

    },
    Show: function () {
        //variety_sets_count
        $("#" + model.displayState.DisplayVarietiesCountId).text(model.displayState.VarietiesCount)
        $("#" + model.displayState.DisplayVarietySetsCountId).text(model.displayState.VarietySetCount)
        $("#" + model.displayState.DisplayPageId).text(model.displayState.CurrentPage)
        $("#" + model.displayState.DisplayPageCountId).text(model.displayState.TotalPages)
    }
};
model.SupplierData = [];
model.SpeciesData = [];
model.VaritySetsData = [];


var viewModel = {};

viewModel.config = function (plantCategoryCode, productFormCode) {
    if (plantCategoryCode != model.selectionState.plantCategoryCode || productFormCode != model.selectionState.productFormCode) {
        //if Selection State has changed then clear data
        model.SupplierData = [];
        model.SpeciesData = [];
        model.VaritySetsData = [];
    }
    //record new selection state
    model.selectionState.plantCategoryCode = plantCategoryCode;
    model.selectionState.productFormCode = productFormCode;
    viewModel.SetEventsToggleAll();
    model.configData.isConfigured = true;
};

viewModel.SelectedSupplierCodes = function () {
    var returnArray = [];
    var items = $("#" + model.configData.supplierListId + " li.selection_selected");
    $.each(items, function (i, value) { returnArray.push($(this).data("code")); });
    return returnArray;
};
viewModel.SelectedSpeciesCodes = function () {        //Returns supplier codes that are selected
    var returnArray = [];
    var items = $("#" +model.configData.speciesListId + " tr td.selection_selected");
    $.each(items, function (i, value) { returnArray.push($(this).data("code")); });
    return returnArray;
};

viewModel.loadAll = function () {
    viewModel.clearSupplierList([
        viewModel.clearSpeciesList,
        viewModel.clearVarietySetsDisplay,
        viewModel.deleteSupplierData,
        viewModel.retrieveSupplierData,
        viewModel.displaySupplierList,
        viewModel.deleteSpeciesData,
        viewModel.retrieveSpeciesData,
        viewModel.displaySpeciesList,
        viewModel.deleteVarietySetsData,
        viewModel.retrieveVarietySetsData,
        viewModel.displayVarietySets
    ]);
};
viewModel.reloadSpecies = function () {
    viewModel.clearSpeciesList([
        viewModel.clearVarietySetsDisplay,
        viewModel.deleteSpeciesData,
        viewModel.deleteVarietySetsData,
        viewModel.retrieveSpeciesData,
        viewModel.displaySpeciesList,
        viewModel.retrieveVarietySetsData,
        viewModel.displayVarietySets
    ]);
};

viewModel.reloadVarietySets = function () {
    viewModel.clearVarietySetsDisplay([
        viewModel.deleteVarietySetsData,
        viewModel.retrieveVarietySetsData,
        viewModel.displayVarietySets
    ]);
};



viewModel.retrieveSupplierData = function (callbacks) {
    //Note: takes variable number of arguments that are handled in ajax.complete event handler
    //alert("retrieving");
    //Make ajax call to retrieve supplier data
    $.ajax({
        datatype: "json",
        url: model.configData.urlGetSupplierData(),
        type: "get",
        beforeSend: function(jqXHR, settings){},
        success: function ( response, textStatus, jqXHR ) {
            model.SupplierData = response;
            //alert(JSON.stringify(model.SupplierData));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });
};
viewModel.deleteSupplierData = function (callbacks) {
    model.SupplierData = [];
    viewModel.ProcessEventChain(callbacks);

};
viewModel.displaySupplierList = function (callbacks) {
    //alert("data length:"+model.SupplierData.length);
    var targetList = $("#" + model.configData.supplierListId);
    targetList.empty();
    
    $.each(model.SupplierData, function (index, value) {
        var li = $("<li></li>");
        //var id = "breeder_code_" + value.Code;
        //li.prop("id", id);
        li.prop("class", "selection_selected");
        li.text(value.Name)
        li.data("code", value.Code);
        //alert(value.Code);
        targetList.append(li);
        //$("#" + id).data("code", value.Code);
        //alert($("#" + id).data("code"));
    });
    //setClickFunctions(model.configData.supplierListId);
    viewModel.SetEventsSelectItems(model.configData.supplierListId);
    viewModel.removeLoadingIcon($("#" + model.configData.supplierContainerId));
    viewModel.ProcessEventChain(callbacks);
};
viewModel.clearSupplierList = function (callbacks) {
    $("#" + model.configData.supplierListId).empty();
    viewModel.displayLoadingIcon($("#" + model.configData.supplierContainerId));

    viewModel.ProcessEventChain(callbacks);
};

viewModel.retrieveSpeciesData = function (callbacks) {
    //Make ajax call to retrieve species data
    //alert(model.configData.urlGetSpeciesData());
    $.ajax({
        datatype: "json",
        url: model.configData.urlGetSpeciesData(),
        type: "get",
        beforeSend: function (jqXHR, settings) { },
        success: function (response, textStatus, jqXHR) {
            model.SpeciesData = response;
            //alert(JSON.stringify(model.SpeciesData));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });

};
viewModel.deleteSpeciesData = function (callbacks) {
    model.SpeciesData = [];
    viewModel.ProcessEventChain(callbacks);
};
viewModel.displaySpeciesList = function (callbacks) {
    var targetList = $("#" + model.configData.speciesListId)
    targetList.empty();
    var data = model.SpeciesData;

    $.each(model.SpeciesData, function (index, value) {
        var li = $("<li></li>");
        //var id = "breeder_code_" + value.Code;
        //li.prop("id", id);
        li.prop("class", "selection_selected");
        li.text(value.Name)
        li.data("code", value.Code);
        //alert(value.Code);
        targetList.append(li);
        //$("#" + id).data("code", value.Code);
        //alert($("#" + id).data("code"));
    });

    viewModel.SetEventsSelectItems(model.configData.speciesListId);
    viewModel.removeLoadingIcon($("#" + model.configData.speciesContainerId));
    viewModel.ProcessEventChain(callbacks);
};
viewModel.clearSpeciesList = function (callbacks) {
    $("#" + model.configData.speciesListId).empty();
    viewModel.displayLoadingIcon($("#" + model.configData.speciesContainerId))

    viewModel.ProcessEventChain(callbacks);
};

viewModel.retrieveVarietySetsData = function (callbacks) {
    //alert("retrieveVarietySetsData");
    //Make ajax call to retrieve species data
    //alert(model.configData.urlGetVarietySetsData());
    $.ajax({
        datatype: "json",
        url: model.configData.urlGetVarietySetsData(),
        type: "get",
        beforeSend: function (jqXHR, settings) { },
        success: function (response, textStatus, jqXHR) {
            model.VaritySetsData = response;
            model.displayState.Calc();
            model.displayState.Show();
            //alert(JSON.stringify(model.VaritySetsData));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });

};
viewModel.deleteVarietySetsData = function (callbacks) {
    model.VaritySetsData = [];
    viewModel.ProcessEventChain(callbacks);
};
viewModel.displayVarietySets = function (callbacks) {
    //alert("displayVarietySets");
    //var s = JSON.stringify(model.VaritySetsData);
    //$("#" + model.configData.varietySetsDisplayId).text(s);
    var container = $("#" + model.configData.varietySetsDisplayId);
    container.empty();
    $.each(model.VaritySetsData, function (i, set) {  //foreach variety Set
        //alert(set.Code);
        //Create the Variety Display
        var varietySet = $("<div></div>");
        varietySet.prop("class", model.configData.varietySetClass)
        //create append label:
        var varietySetLabel = $("<h3></h3>");
        varietySetLabel.text(set.Name + ":");
        varietySet.append(varietySetLabel);

        var varietyList = $("<ul class=\"variety_set_list\"></ul>");
        $.each(set.SelectedVarieties, function (ii, variety) {  //foreach variety item
            var varietyTile = $("<li></li>");
            varietyTile.prop("class", model.configData.varietyTileClass);
            var image = $("<img alt=\"\"/>");
            //alert(variety.ImageUrl);
            image.prop("src", variety.ImageUrl);
            var p1 = $("<p></p>");
            var s = variety.Name || "-";
            p1.text(s.trim());
            var p2 = $("<p></p>");
            s = variety.GeneticOwnerName || "-";
            p2.text(s);
            varietyTile.append(image).append(p1).append(p2);

            varietyTile.on("click", function (event) {
                var s = model.configData.urlGoToVarietyDetailPage(variety.Code);
                alert("Development Note:\n\nRedirect to: " + s);
                window.location.href = s;
            });
            varietyList.append(varietyTile);
        });
        varietySet.append(varietyList);
        container.append(varietySet);
        if (i >= 2) { return false; };
    });
    viewModel.removeLoadingIcon($("#right_region_container"));
    viewModel.ProcessEventChain(callbacks);
};
viewModel.displayVarietySetsHOLD = function (callbacks) {
    //alert("displayVarietySets");
    //var s = JSON.stringify(model.VaritySetsData);
    //$("#" + model.configData.varietySetsDisplayId).text(s);
    var container = $("#" + model.configData.varietySetsDisplayId);
    container.empty();
    $.each(model.VaritySetsData, function (i, set) {  //foreach variety Set
        //alert(set.Code);
        //Create the Variety Display
        var varietySet = $("<div></div>");
        varietySet.prop("class", model.configData.varietySetClass)
        //create append label:
        var varietySetLabel = $("<h3></h3>");
        varietySetLabel.text(set.Name + ":");
        varietySet.append(varietySetLabel);

        $.each(set.SelectedVarieties, function (ii, variety) {  //foreach variety item
            var varietyTile = $("<div></div>");
            varietyTile.prop("class", model.configData.varietyTileClass)
            var image = $("<img alt=\"\"/>");
            //alert(variety.ImageUrl);
            image.prop("src", variety.ImageUrl);
            var line1 = $("<p></p>");
            line1.text(variety.Name);
            var line2 = $("<p></p>");
            line2.text(variety.GeneticOwnerName);
            varietyTile.append(image).append(line1).append(line2);

            varietyTile.on("click", function (event) {
                var s = model.configData.urlGoToVarietyDetailPage(variety.Code);
                //alert("Development Note:\n\nRedirect to: " + s);
                window.location.href = s;
            });
            varietySet.append(varietyTile);

        });

        container.append(varietySet);
        if (i >= 2) { return false; };
    });
    viewModel.removeLoadingIcon($("#right_region_container"));
    viewModel.ProcessEventChain(callbacks);
};
viewModel.clearVarietySetsDisplay = function (callbacks) {
    //alert("clearVarietySetsDisplay");
    $("#" + model.configData.varietySetsDisplayId).empty();
    viewModel.displayLoadingIcon($("#right_region_container"))
    viewModel.ProcessEventChain(callbacks);
};

viewModel.SetEventsToggleAll = function () {
    ////////////////////////////////// Select All /////////////////////////////////////////

    $("#supplier_selection_container .selection_select_all").on("click", function (event) {
        //alert("bingo");
        $("#supplier_selection_list .selection_selected, #supplier_selection_list .selection_available").removeClass("selection_available").addClass("selection_selected");

        //ToDo: only reload Species if suppliers selected < suppliers listed, otherwise, no change
        viewModel.reloadSpecies(); //trigger update of species list

        viewModel.SetEventsSelectItems("supplier_selection_list");
    });

    $("#species_selection_container .selection_select_all").on("click", function (event) {
        //alert("bingo");
        $("#species_selection_list .selection_selected, #species_selection_list .selection_available").removeClass("selection_available").addClass("selection_selected");

        //ToDo: only reload Varieites if species selected < species listed, otherwise no change
        viewModel.reloadVarietySets(); //trigger update of varieties 
        
        viewModel.SetEventsSelectItems("species_selection_list");
    });
    ////////////////////////////////// Clear All /////////////////////////////////////////
    $("#supplier_selection_container .selection_clear_all").on("click", function (event) {
        //alert("bingo");
        $("#supplier_selection_list .selection_selected, #supplier_selection_list .selection_available").removeClass("selection_selected").addClass("selection_available");
        viewModel.reloadSpecies(); //trigger update of species list
        viewModel.SetEventsSelectItems("supplier_selection_list");
    });
    $("#species_selection_container .selection_clear_all").on("click", function (event) {
        //alert("bingo");
        $("#species_selection_list .selection_selected, #species_selection_list .selection_available").removeClass("selection_selected").addClass("selection_available");
        viewModel.reloadVarietySets(); //trigger update of varieties 
        viewModel.SetEventsSelectItems("species_selection_list");
    });
};
viewModel.SetEventsSelectItems = function (containerId) {
    //alert(containerId + ", " + model.configData.speciesListId);
    containerId = ("" + containerId).trim();  //clear null, trim spaces
    var isSupplierSelection = (containerId == model.configData.supplierListId);
    var isSpeciesSelection = (containerId == model.configData.speciesListId);
    //alert(isSupplierSelection);

    //turn off all click events for selected and unselected elements in this container
    $("#" + containerId + " .selection_selected, #" + containerId + " .selection_available").off("click");

    //if set event for selected elements in this container
    $("#" + containerId + " .selection_selected").on("click", function (event) {
        $(this).removeClass("selection_selected").addClass("selection_available");
        if (isSupplierSelection) {
            viewModel.reloadSpecies();
        } else if (isSpeciesSelection) {
            viewModel.reloadVarietySets();
        };

        viewModel.SetEventsSelectItems(containerId);
    });

    //if event for unselected events in this container
    $("#" + containerId + " .selection_available").on("click", function (event) {
        $(this).removeClass("selection_available").addClass("selection_selected");
        if (isSupplierSelection) {
            viewModel.reloadSpecies();
        } else if (isSpeciesSelection) {
            viewModel.reloadVarietySets();
        };
        viewModel.SetEventsSelectItems(containerId);
    });



}

viewModel.ProcessEventChain = function (callbacks) {
    
    if (callbacks != undefined && callbacks.length > 0) {
        var callback = callbacks.shift();
        //alert(callback);
        callback(callbacks);
    }
};

viewModel.displayLoadingIcon = function (parentEl) {
    //Note loading icopns have class name and must be located where they won't be deleted, ie above or below element that is populated
    //var icon = $('<img alt="waiting"></img>');
    //icon.prop("src", "/Images/icon_loading_bar_green.gif");
    //element.append(icon);
    $(parentEl).children(".loadingIcons").css("display", "block");
};
viewModel.removeLoadingIcon = function (parentEl) {
    //Note loading icopns have class name and must be located where they won't be deleted, ie above or below element that is populated
    //var icon = $('<img alt="waiting"></img>');
    //icon.prop("src", "/Images/icon_loading_bar_green.gif");
    //element.append(icon);
    $(parentEl).children(".loadingIcons").css("display", "none");
};



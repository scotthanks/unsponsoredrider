﻿

var model = {};
model.data = {
    user: {
        FirstName: "Scott",
        LastName: "Pilgrim",
        PhoneNumber: "3334445555",
        Email: "scootp@mayflower.com",
        Password: "EWrite77",
    //    Password2: "EWrite77",
        CompanyRole: "Other",
        SitePermission: "CompanyAdministrator",
        
    },
    currentUser: {
        userGuid: "EWrite77",
        isAdmin: false,
        
    },
    users: [],
};

model.config = {
    
    urlUsers: "/api/user/growerusers",
    urlPostUser: "/api/user/post",
    urlDeleteUser: "/api/userdelete/put",
    urlUserRoleUpdate: "/api/userrole/put",
};

model.getData = function (element, url, onSuccess, callbacks) {
    var testDisplay = $("#test_area_results_display").empty();
    var p = $("<p />")
        .text("[GET] url : " + url);
    testDisplay.append(p, $("<hr />"));
    $.ajax({
        url: url,
        type: "get",
        dataType: "json",
        timeout: 10000,
    })
    .done(function (data, textStatus, jqXHR) {
        testDisplay.append(traverseObj(data));
        onSuccess(data);
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        var errorMsg = "URL: "+this.url+" data: "+this.data+ ", TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown;
        testDisplay.append(errorMsg);
        if (typeof element !== "undefined" & element != null) {
            viewModel.floatMessage(element, errorMsg, 4000);
        };
    })
    .always(callbacks);

};

model.postData = function (element, url, data, onSuccess, callbacks) {
    $.ajax({
        url: url,
        type: "post",
        dataType: "json",
        timeout: 10000,
        data: data,
    })
    .done(function (data, textStatus, jqXHR) {
        onSuccess(data);
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        var errorMsg = "TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown;
        $("#test_area_results_display").append(errorMsg);
        if (typeof element !== "undefined" & element != null) {
          //  alert("in fail");
            viewModel.floatMessage(element, errorMsg, 4000);
        };
    })
    .always(callbacks);

};





var viewModel = {};


viewModel.config = {
    pageMode: "all",
    buttonAddUserId: "add_user_button",
    addUsersContainerId: "add_users_container",
    manageUsersContainerId: "manage_users_container",
    templates: {
        addUserForm: {
            templateId: "user_input_container_template",
            id: "user_input_container",
            container$: null,
        },
        manageUsersTable: {
            templateId: "manage_users_grid_template",
            rowTemplateId: "manage_users_grid_data_row_template",
            id: "manage_users_grid",
            rowId: "manage_users_grid",
            grid$: null,
            row$: null,
        },
    },
    accordions: {
        addUserId: "add_user_accordion",
        manageUsersId: "manage_users_accordion",
        rateOfDisplay: function (accordionId) {
            var et = 500;
            var height = viewModel.config.accordions.display$(accordionId).height();
            et += Math.floor(height / 25 * 50);
            return et;
        },
        returnAccordion: function (headerBar$) {
            return headerBar$.parent("div");
        },
        container$: function (accordionId) { return $("#" + accordionId); },
        headerBar$: function (accordionId) { return $("#" + accordionId + " .accordion_header_bars"); },
        display$: function (accordionId) { return $("#" + accordionId + " .accordion_displays"); },
        displayDisabled$: function (accordionId) { return $("#" + accordionId + " .accordion_disabled_displays"); },
    },
    icons: {
        headerBarWorking: "../../Images/icons/Loading_Availability.gif",
        headerBarExpanded: "/images/layout/MyAccountArrowDown.png",
        headerBarCollapsed: "/images/layout/cart_header_pointer.png",
    },
    selectors: {
        orderSubstitutionsRadioDisplayId: "company_role_selection_container",
        orderSubstitutionsApprovedId: "order_substitutions_approved",
        orderSubstitutionsDeclinedId: "order_substitutions_declined",
        selectedClass: "selected",
        unselectedClass: "unselected",
        copmanyRoleRadioDisplayId: "company_role_radio_container",
        sitePermissionRadioDisplayId: "site_permissions_radio_container",
        
    },

};
viewModel.state = {
    accordions: {
        isExpanded: function (accordionId) {
            //alert(accordionId);
            var accordion$ = viewModel.config.accordions.container$(accordionId);
            //alert(viewModel.config.accordions.container$(accordionId).prop("class"));
            return (viewModel.config.accordions.container$(accordionId).hasClass("accordions_expanded"));
        },
    },
};

viewModel.init = function () {

    //Clone and Delete Add User Form Template
    viewModel.config.templates.addUserForm.container$ = $("#" + viewModel.config.templates.addUserForm.templateId).contents().clone(true);
    $("#" + viewModel.config.templates.addUserForm.templateId).remove();

    //Clone and Delete Manage Users Grid Row and Table Templates
    viewModel.config.templates.manageUsersTable.row$ = $("#" + viewModel.config.templates.manageUsersTable.rowTemplateId).clone(true);
    $("#" + viewModel.config.templates.manageUsersTable.rowTemplateId).remove();
    viewModel.config.templates.manageUsersTable.grid$ = $("#" + viewModel.config.templates.manageUsersTable.templateId).contents().clone(true);
    $("#" + viewModel.config.templates.manageUsersTable.templateId).remove();


    /////////////////////// Initialize Expanses /////////////////////////////

    model.getData(null, model.config.urlUsers, function (data) {
        if (data.Success) 
        {
            model.data.currentUser.userGuid = data.currentUserGuid;
            model.data.currentUser.isAdmin = data.isAdmin;
            model.data.users = data.growerUsers;
          
            eps_expanse.init(viewModel.config.addUsersContainerId, viewModel.addUserExpand, viewModel.addUserCollapse, "Add User");
            eps_expanse.setStatus(viewModel.config.addUsersContainerId, "Add User");
            eps_expanse.init(viewModel.config.manageUsersContainerId, viewModel.manageUsersExpand, viewModel.manageUsersCollapse, "Manage Users");
            eps_expanse.setStatus(viewModel.config.manageUsersContainerId, "Users: " + model.data.users.length);
            if (model.data.currentUser.isAdmin == false) {
                // alert("not admin");
                viewModel.floatMessage(null, "Only administrators on an account are able to add and manage users. <BR> Please contact the admin on your account to add the new user or have them give you admin privileges for your account.", 4000);
                //  eps_expanse.state.setCollapsed(containerId);
            }
            else
            {
                
                if (viewModel.config.pageMode == "all" || viewModel.config.pageMode == "add") {
                    viewModel.addUserExpand(viewModel.config.addUsersContainerId);

                };
               
                if (viewModel.config.pageMode == "all" || viewModel.config.pageMode == "manage") {
                    viewModel.manageUsersExpand(viewModel.config.manageUsersContainerId);
                };
            }
        }
        else {
            model.data.users = [];
            //  alert(model.data.users.length);
        }
    }, [])
};
   
    
//////////////////// Acton Buttons ///////////////////////////////
    
/////////////////////// Test Buttons ///////////////////////////////
    //$("#test_api_users").button().on("click", function () {
    //    var onSuccess = function (data) {
    //        //ToDo: Authentication
    //        //ToDo: Authorizaion as Administrator
    //        model.data.users = data;
    //    };
    //    model.getData($(this), model.config.urlUsers, onSuccess);
    //});
    //$("#test_data_users").button().on("click", function () {
    //    var testDisplay = $("#test_area_results_display").empty();
    //    var div = $("<div />")
    //        .html("Users Data:<hr />" + traverseObj(model.data.users));
    //    testDisplay.append(div);
    //});
    //$("#test_data_post").button().on("click", function () {
    //    var testDisplay = $("#test_area_results_display").empty();
    //    var div = $("<div />")
    //    var p1 = $("<p />").text("POST url: " + model.config.urlPostUser);
    //    var p2 = $("<p />").html("Data Sent: <br />" + traverseObj(model.data.user));
    //    div.append(p1, p2, $("<hr />"));
    //    testDisplay.append(div);

    //    var onSuccess = function (data) {
    //        //ToDo: Authentication
    //        //ToDo: Authorizaion as Administrator
    //        $("#test_area_results_display").append("Response:<br />" + traverseObj(data));
    //    };
    //    model.postData($(this), model.config.urlPostUser, model.data.user, onSuccess);

    //});

    // model.getData(null, model.config.urlUsers, function (data) {
    //     if (data.Success) 
    //     { 
    //         // alert(traverseObj(data));
    //         // alert(data.currentUserGuid);
    //         // alert(data.isAdmin);
    //         // alert(model.data.currentUser.userGuid);
    //         model.data.currentUser.userGuid = data.currentUserGuid;
    //         model.data.currentUser.isAdmin = data.isAdmin;
    //         //  alert("after");
    //         model.data.users = data.growerUsers; 
    //         ;
    //     } 
    //     else 
    //     { 
    //         model.data.users = []; 
    //     }
    // },[]
    //) };
//};



////////////////////// Selectors /////////////////////////////
viewModel.selectorContainerId = function (selectorElement) {
    return selectorElement.parents(".selection_group_container").prop("id") || null;
};
viewModel.selectiorType = function (containerId) {
    var type = null;
    if (($("#" + containerId + " .selection_group_multiple_displays span") || "").length > 0) {
        return "group";
    } else if (($("#" + containerId + " .selection_group_singular_displays span") || "").length > 0) {
        type = "radio";
    };
    return null;
};
viewModel.selectorGroupInit = function (containerId) {

    //$(".selection_group_singular_displays").off().on("change", function () {
    //    if (typeof changeHandler === "object" && changeHandler != null) {
    //        changeHandler();
    //    };
    //});

    $("#" + containerId + " .selection_group_multiple_displays span").off().on("click", function () {
        var self = $(this);
        if (self.hasClass("disabled")) { return false; };

        var wasSelected = self.hasClass("selected");
        var wasUnselected = self.hasClass("unselected");

        if (wasSelected) {
            self.removeClass().addClass("unselected");
        } else if (wasUnselected) {
            self.removeClass().addClass("selected");
        } else {
            self.removeClass().addClass("disabled");
        };

        
        //id = viewModel.selectorContainerId(self);
        //var result = viewModel.selectorGroupValue(id);
        //alert(JSON.stringify(result));

    });


};
viewModel.selectorRadioInit = function (containerId) {

    //$(".selection_group_singular_displays").off().on("change", function () {
    //    if (typeof changeHandler === "object" && changeHandler != null) {
    //        changeHandler();
    //    };
    //});
    $("#" + containerId + " .selection_group_singular_displays span").off().on("click", function () {
        var self = $(this);
        if (self.hasClass("disabled")) { return false; };
        if (self.hasClass("selected")) { return false; };

        //$(".selection_group_singular_displays").change();

        //alert(self.data("value"));

        if (self.hasClass("unselected")) {
            self.siblings(".selected").removeClass().addClass("unselected");
            self.removeClass().addClass("selected");
        } else {
            self.removeClass().addClass("disabled");
        };


        //id = viewModel.selectorContainerId(self);
        //var result = viewModel.selectorRadioValue(id);
        //alert(result);




    });



};
viewModel.selectorRadioValue = function (containerId) {
    var v = null;
    var choices = $("#" + containerId + " .selection_group_singular_displays span");
    $.each(choices, function (i, choice) {
        var el = $(choice);
        if (el.hasClass("selected")) {
            v = el.data("value");
            return false;
        };
    });
    return v;
};
viewModel.selectorGroupValue = function (containerId) {
    //alert(containerId);
    var v = [];
    var elements = $("#" + containerId + " .selection_group_multiple_displays span");
    
    //alert(elements.length);
    
    $.each(elements, function (i, item) {
        //alert("bingo");
        //alert($(item).hasClass("selected"));
        var el = $(item);
        if (el.hasClass("selected")) {
            //alert(el.data("value"));
            v.push(el.data("value"));
        };
    });
    return v;
};


viewModel.loadUserForm = function (accordionId) {
    var form = viewModel.config.templates.addUserForm.container$;
    //var accordion$ = viewModel.config.accordions.container$(accordionId);
    alert("Load form Data");
    var display$ = viewModel.config.accordions.display$(accordionId)
        .append(form);
    $("#FirstName").val(model.data.user.FirstName);
    $("#LastName").val(model.data.user.LastName);
    $("#FirstName").val(model.data.user.FirstName);
    $("#EmailAddress").val(model.data.user.Email);
    $("#PhoneNumber").val(model.data.user.PhoneNumber);


    //viewModel.accordionLoad(accordionId);
};
viewModel.accordionSetHeaderBarEvents = function (headerBar$, onLoad) {
   // alert("hi");
    if (typeof headerBar$ === 'undefined' || headerBar$ == null) { return false; };
    headerBar$
        .off()
        .on("click", function () {

            var accordionId = viewModel.config.accordions.returnAccordion($(this)).prop("id");

            //alert("expanded: " + viewModel.state.accordions.isExpanded(accordionId));
            if (viewModel.state.accordions.isExpanded(accordionId)) {
                //alert(accordionId);
                viewModel.accordionCollapse(accordionId);
            } else {

                //var content = onLoad(accordionId);
                //viewModel.accordionCollapse(accordionId);
                //viewModel.accordionLoad(accordionId, viewModel.loadUserForm(accordionId));

                viewModel.accordionExpand(accordionId, onLoad);
            };
        });
};

//////////////// Accordions //////////////////////////////
viewModel.setHeaderBarIcon = function (accordionId, iconSrc, callbacks) {
    var headerBar$ = viewModel.config.accordions.headerBar$(accordionId)
    headerBar$.css({ "background-image": "url('" + iconSrc + "')" });
    viewModel.processEventChain(callbacks);
};
viewModel.accordionEnable = function (accordionId, callbacks) {
    var accordion$ = viewModel.config.accordions.container$(accordionId);
    accordion$.removeClass().addClass("accordions_collapsed");
};
viewModel.accordionDisable = function (accordionId, message, callbacks) {
    var accordion$ = viewModel.config.accordions.container$(accordionId);
    var display$ = viewModel.config.accordions.displayDisabled$(accordionId);
    display$.html(message);
    accordion$.removeClass().addClass("accordions_disabled");
};
viewModel.accordionExpand = function (accordionId, onLoad, callbacks) {
    var accordion$ = viewModel.config.accordions.container$(accordionId);
    viewModel.setHeaderBarIcon(accordionId, viewModel.config.icons.headerBarWorking);
    viewModel.accordionEnable(accordionId);
    var display$ = viewModel.config.accordions.display$(accordionId);
    display$.hide();
    onLoad(accordionId);
    display$.show("blind", {}, viewModel.config.accordions.rateOfDisplay(accordionId), function () {
            viewModel.setHeaderBarIcon(accordionId, viewModel.config.icons.headerBarExpanded);
            accordion$.removeClass().addClass("accordions_expanded");
            viewModel.processEventChain(callbacks);
        });
};
viewModel.accordionCollapse = function (accordionId, callbacks) {
    //alert("collapse");
    var accordion$ = viewModel.config.accordions.container$(accordionId);
    viewModel.setHeaderBarIcon(accordionId, viewModel.config.icons.headerBarWorking);
    var display$ = viewModel.config.accordions.display$(accordionId);
    display$
        .hide("blind", {}, viewModel.config.accordions.rateOfDisplay(accordionId), function () {
            //alert(accordionId);
            viewModel.setHeaderBarIcon(accordionId, viewModel.config.icons.headerBarCollapsed);
            accordion$.removeClass().addClass("accordions_collapsed");
            display$.empty();
            viewModel.processEventChain(callbacks);
        })


};
viewModel.accordionClear = function (accordionId) {
    viewModel.config.accordions.displayDisabled$(accordionId).empty();
    viewModel.config.accordions.display$(accordionId).empty();
};
viewModel.accordionLoad = function (accordionId, content) {
    viewModel.config.accordions.display$(accordionId)
        .empty()
        .html(content);
};


////////////////// Add User //////////////////////////////////
viewModel.addUserExpand = function (containerId) {
    if (model.data.currentUser.isAdmin == false)
    {
       //alert("not admin");
       viewModel.floatMessage(null, "Only administrators on an account are able to add and manage users. <BR> Please contact the admin on your account to add the new user or have them give you admin privileges for your account.", 2500);
        //  eps_expanse.state.setCollapsed(containerId);
    }
    else
    {

        eps_expanse.state.setWorking(containerId);
        var display = eps_expanse.state.getDisplayElement(containerId);

        if ((display.children().length || 0) == 0) {
            var form = viewModel.config.templates.addUserForm.container$.clone(true);
            display.append(form);
        };
        //  alert("in add user expand - " + model.data.users.length);
        eps_expanse.setStatus(viewModel.config.manageUsersContainerId, "Users: " + model.data.users.length);
        eps_expanse.expand(containerId, null);
        $("#PhoneNumber").mask("(999) 999-9999");
        $("#" + viewModel.config.buttonAddUserId).on("click", function () {
            // alert(model.data.currentUser.isAdmin);
            if (model.data.currentUser.isAdmin == true) {
                viewModel.manageUsersAddUser();
            }
            else {
                // alert("User cannot add users. only Admin users can invite users.");
                viewModel.floatMessage(element, "User cannot add users. only Admin users can invite users.", 4000);
            }

        });

    }

};
viewModel.addUserCollapse = function (containerId) {
    eps_expanse.setStatus(viewModel.config.addUsersContainerId, "Add User");
    eps_expanse.collapse(containerId, null, false);
};
viewModel.validateUser = function () {

    // alert("in validate");
    //return true;

    //#CompanyName, #StreetAddress1, #City, #CompanyEmail, #FirstName, #LastName, #PersonPhone, #PersonEmail"
    var isValid = true;
    var messages = [];

    //alert(traverseObj(model.registrationData, false));


    //Password Lengths
    var password = model.data.user.Password;
    if (password.length < 6 || password.length > 12) {
        isValid = false;
        messages.push("<p>Password must be 6 to 12 characters</p>");
    };

    //Password Match
    var password2 = $("#ConfirmPassword").val().trim();
    if (password2 != model.data.user.Password) {
        isValid = false;
        messages.push("<p>Passwords must match</p>");
    };
    //Email

    if (model.data.user.Email.length < 1) {
        isValid = false;
        messages.push("<p>Email Address is required</p>");
    };

    //First Name
    if (model.data.user.FirstName.length < 1) {
        isValid = false;
        messages.push("<p>First name is required</p>");
    };
    //Last Name
    if (model.data.user.LastName.length < 1) {
        isValid = false;
        messages.push("<p>Last name is required</p>");
    };
    //Person Phone
    // alert(model.data.user.PersonPhone.length.ToString());
    if (model.data.user.PhoneNumber.length < 14) {
        isValid = false;
        messages.push("<p>Phone # not compelete</p>");
    };



    if (!isValid) {
        var msg = "<h3>Your request needs attention:</h3>";
        $.each(messages, function (i, item) {
            msg += item;
        });

        msg += "<p>Please fix these items and re-submit.</p>";
        eps_tools.OkDialog({ 'title': '<span>Not Ready to Add User:</span>', 'content': msg, 'modal': true });
    };

    //    alert("end validate " + isValid);
    return isValid;

};


///////////////// Manage Users ///////////////////////////////
viewModel.manageUsersExpand = function (containerId) {
    if (model.data.currentUser.isAdmin == false) {
        // alert("not admin");
        viewModel.floatMessage(null, "Only administrators on an account are able to add and manage users. <BR> Please contact the admin on your account to add the new user or have them give you admin privileges for your account.", 4000);
        //  eps_expanse.state.setCollapsed(containerId);
    }
    else {


        eps_expanse.state.setWorking(containerId);
        var display = eps_expanse.state.getDisplayElement(containerId).empty();
        eps_expanse.setStatus(viewModel.config.addUsersContainerId, "Add User");
        //  alert("in manage user expand - " + model.data.users.length);
        eps_expanse.setStatus(viewModel.config.manageUsersContainerId, "Users: " + model.data.users.length);

        model.getData(null, model.config.urlUsers, function (data) {
            if (data.Success) {

                model.data.currentUser.userGuid = data.currentUserGuid;
                model.data.currentUser.isAdmin = data.isAdmin;
                model.data.users = data.growerUsers;
                ;
            } else { model.data.users = []; };

        }, [function () {


            if (model.data.users.length <= 0) {
                eps_expanse.setStatus(viewModel.config.manageUsersContainerId, "Users: none");
                eps_expanse.state.setCollapsed(containerId);
            } else {

                var grid = viewModel.config.templates.manageUsersTable.grid$.clone(true);
                //alert(traverseObj(model.data.users, false));
                $.each(model.data.users, function (i, user) {
                    var row = viewModel.config.templates.manageUsersTable.row$.clone(true);
                    row.prop({
                        "id": user.GrowerUserGuid
                    });

                    //alert(user.GrowerUserGuid)

                    row.children("td.user_names").text(user.FirstName + " " + user.LastName)

                    //alert(row.children("td.site_preferences_radio_group_display").children("input[type='radio']").length);

                    var radioInputs = row.children("td.site_preferences_radio_group_display").children("input[type='radio']");
                    radioInputs
                        .prop({
                            "name": user.GrowerUserGuid
                        });

                    if (model.data.currentUser.isAdmin == false) {
                        radioInputs.attr('disabled', 'disabled');




                    }

                    row.children("td.delete_user_links").children("a.manage_user_delete_links").on("click", function () {
                        var offset = $(this).offset();
                        var userGuid = row.prop("id");
                        //   alert("Delete: " + model.data.currentUser.isAdmin);
                        if (model.data.currentUser.isAdmin == true) {
                            //alert("admin")
                            viewModel.DisplayDeleteUserForm(userGuid, { left: offset.left - $(window).scrollLeft(), top: offset.top - $(window).scrollTop() });
                        }
                    });

                    row.children("td.site_preferences_radio_group_display").children("input[type='radio'][value='" + user.SitePermission + "']").prop("checked", true);

                    grid.children("tbody").append(row);

                });
                display.append(grid);

                viewModel.manageUsersSetRadioEvents();
                eps_expanse.setStatus(viewModel.config.manageUsersContainerId, "Users: " + model.data.users.length);
                eps_expanse.expand(containerId, null);
            };

        }]);
    }
};
viewModel.manageUsersCollapse = function (containerId) {
    eps_expanse.state.setWorking(containerId);
    eps_expanse.collapse(containerId, null, true);
};

viewModel.manageUsersAddUser = function () {
    //alert("Add User");
    var url = model.config.urlPostUser;
    //alert(url);
    var data = viewModel.ComposeUserDataFromForm();
    var isvalid = viewModel.validateUser();
    if(isvalid == true)
    {
        var element = $("." + viewModel.config.manageUsersContainerId);

        var onSuccess = function (data) {
          //  alert(traverseObj(data));
            if (data.Success == true) {
                viewModel.floatMessage(element, "Thank you for adding a user to your account. <BR> The new user will be notified via email of their new log-in for orders.green-fuse.com", 4000);
                viewModel.manageUsersExpand(viewModel.config.manageUsersContainerId);
            }
            else {
             //   alert("in success with message Error: " + data.Message);
                viewModel.floatMessage(element, "Error: " + data.Message, 4000);
                
            }
        };

        model.postData(element, url, data,onSuccess, []);
    }
};
viewModel.manageUsersDeleteUser = function (userGuid) {
   // alert("TBD:\nRemove User: " + userGuid + "");
    var url = model.config.urlDeleteUser;
    // alert("TBD\nUser: " + userGuid + "\nValue: " + value);
 //   alert(url);
    var element = $("." + viewModel.config.manageUsersContainerId);
    var value = "CustomerAdministrator"; //take this dummy variable out, site permission not needed for this.
    model.ajaxCall(element, url, "put", { "GrowerUserGuid": userGuid, "SitePermission": value },
//  model.ajaxCall(element, url, "put", { "GrowerUserGuid": userGuid },
        function (data) {
           // alert("hi");
            if (data.Success == true) {
                viewModel.floatMessage(element, "User Deleted", 4000);
                viewModel.manageUsersExpand(viewModel.config.manageUsersContainerId);
            }
        }, []
    );

};
viewModel.manageUsersUpdateSitePermission = function (userGuid, value) {
   
    var url = model.config.urlUserRoleUpdate;
   // alert("TBD\nUser: " + userGuid + "\nValue: " + value);
   // alert(url);
    var element = $("." + viewModel.config.manageUsersContainerId);
   // alert(element.id);
    model.ajaxCall(element, url, "put", { "GrowerUserGuid": userGuid, "SitePermission": value },
        function (data) {
            //alert("hi");
            if (data.Success == false)
            {
                viewModel.manageUsersExpand(viewModel.config.manageUsersContainerId);
            }
        }, []
       
        );
    

};
viewModel.manageUsersSetRadioEvents = function () {
    $('.site_preferences_radio_group_display input[type="radio"]')
        .off()
        .on("change", function (event) {
            //alert("CHANGE EVENT!");
            //alert("rowid: " + $(this).closest("tr").prop("id"));
            //alert("value: " + $(this).prop("value"));
            viewModel.manageUsersUpdateSitePermission($(this).closest("tr").prop("id") || null, $(this).prop("value") || null);
        });
};
viewModel.DisplayDeleteUserForm = function (userGuid, location) {
    //var cardInfo = model.ReturnCreditCardInfo(userGuid);
   // alert("Delete: " + userGuid);
    
    var content = $('<span />').html("<br />");
    eps_tools.OkCancelDialog({
        'title': '<span>Delete User:</span>',
        'content': content,
        'modal': true,
        'onOk': function () {
            //alert("DELETE: " + userGuid);
            viewModel.manageUsersDeleteUser(userGuid)
        },
        'locate': location,
    });
    
};
viewModel.ComposeUserDataFromForm = function () {
    var data = {};
    //sync the names to Api

    model.data.user.FirstName = $("#FirstName").val();
    model.data.user.LastName = $("#LastName").val();
   
    model.data.user.Email = $("#EmailAddress").val();
    model.data.user.PhoneNumber = $("#PhoneNumber").val();
    model.data.user.Password = $("#Password").val();
    
   
    var companyRole = $("#" + viewModel.config.selectors.copmanyRoleRadioDisplayId + " input[type='radio'][name='company_role']:checked").val() || "";
  //  alert(companyRole);
    model.data.user.CompanyRole = companyRole;
  //  alert(model.data.user.CompanyRole);
    
    var roleCode = $("#" + viewModel.config.selectors.sitePermissionRadioDisplayId + " input[type='radio'][name='site_permission']:checked").val() || "";
  //  alert(roleCode);
    model.data.user.SitePermission = roleCode;
  //  alert(model.data.user.SitePermission);

 //   alert("here");
 //   alert(traverseObj(model.data.user), false);

    return model.data.user;
};


////////////////////// Utilities /////////////////////////
viewModel.processEventChain = function (callbacks) {
   // alert("in callbacks");
    if (typeof callbacks !== 'undefined' && callbacks != null && callbacks.length > 0) {
        var callback = callbacks.shift();
     //   alert(callback);
        callback(callbacks);
    }
};
viewModel.floatMessage = function (element, content, interval) {
    //floats message to right of element
    //if no element then does nothing
    if (typeof element === 'undefined') { return false; };

    var top, left;
    if (element == null) {
        //Leave values null
        top = null;
        left = null;
    }
    else if (element.length > 0) {
        var position = element.offset();
        //ToDo: code to center floating message vertically with element
        //assume floating message is 35 px tall
        top = Math.floor(position.top + ((element.height() - 35) / 2));
        left = Math.floor(position.left + element.width() + 30);
    };
    eps_tools.FloatingMessage(left, top, content, interval);
};
//Generic Ajax Call - overides global error handler
model.ajaxCall = function (element, uri, httpVerb, data, onSuccess, callbacks) {
    //alert("ajaxCall");
    $.ajax({
        url: uri,
        dataType: "json",
        data: data,
        type: httpVerb,
    })
    .done(function (data, textStatus, jqXHR) {
        // alert(traverseObj(data, false));
        // alert(textStatus);
        if (data.Success == true)
        { onSuccess(data); }
        else
        {
            viewModel.floatMessage(element, "Error: " + data.Message, 2500);
            onSuccess(data);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        var errorMsg = "TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown;
        if (typeof element !== "undefined" & element != null) {
            viewModel.floatMessage(element, errorMsg, 4000);
        };
    })
    .always(function (jqXHR, textStatus) {
         if (textStatus == "success") {
        //alert("hi there");
        viewModel.processEventChain(callbacks);
        } else {
        };
    });
};

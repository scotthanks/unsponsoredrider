﻿
var model = {};


model.TemplateData = [];
model.SearchResultTemplates = [];

model.config = {
    //urlGetSearchResult: "/api/Search/Result/OpenSearch/",
    urlGetSearchResultTemplates: "/api/Search/ResultTemplates/",
};




//model.returnTemplate = function (templateGuid) {
//    var returnTemplate = null;
//    $.each(model.TemplateData, function (ndx, template) {
//        if (template.TemplateGuid == templateGuid) { returnTemplate = template; return false; };
//    });
//    return returnTemplate;
//};


model.getTemplates = function (callbacks) {
    model.TemplateData = [];

    $.ajax({
        datatype: "json",
        type: "get",
        url: model.config.urlGetSearchResultTemplates,
        beforeSend: function (jqXHR, settings) {
            //$("#test_area_response_display").empty();

        },
        success: function (data, textStatus, jqXHR) {
            model.SearchResultTemplates = data.SearchResultTemplates || [];
            //$("#test_area_response_display").text(traverseObj(data, true));
            alert(traverseObj(data, false));

        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });
};





var viewModel = {};

viewModel.init = function () {
    //alert("bingo");

    model.getTemplates([
        function () {
            $("#editor_css_container").text(model.SearchResultTemplates[0].Css)

        }
    ]);
};




/////////////// Utilities //////////////////////////////
viewModel.floatMessage = function (element, content, interval) {
    //floats message to right of element
    //if no element then does nothing
    if (typeof element === 'undefined') { return false; };

    var top, left;
    if (element == null) {
        //Leave values null
        top = null;
        left = null;
    }
    else if (element.length > 0) {
        var position = element.offset();
        //ToDo: code to center floating message vertically with element
        //assume floating message is 35 px tall
        top = Math.floor(position.top + ((element.height() - 35) / 2));
        left = Math.floor(position.left + element.width() + 30);
    };
    eps_tools.FloatingMessage(left, top, content, interval);
};
viewModel.ProcessEventChain = function (callbacks) {

    if (callbacks != undefined && callbacks.length > 0) {
        var callback = callbacks.shift();
        //alert(callback);
        callback(callbacks);
    }
};
//viewModel.ShipWeekCodeToShipWeekString = function (code) {
//    if (typeof code !== "string" || code == null || code.length != 6) { return ""; };
//    return code.substring(4, 6) + "/" + code.substring(0, 4);
//    //return code;
//};
////////////////////////////////////////////////////////





﻿

var code= 
{ 
    init: function(data) {
        var rowId = eps_createNewGuid();
        //alert('bingo');
        //if (data.CatalogProductFormCategoryCode != '' && data.CatalogProgramTypeCode != '') {
            $('#sr_variety_type_template').prop({ 'id': rowId });
            $('#' + rowId + ' .sr_variety_type_anchors').prop(
            {
                'href': '/Catalog/Search?Category=' +  data.CatalogProductFormCategoryCode + '&Form=' + data.CatalogProgramTypeCode
            });
            $('#' + rowId + ' .sr_variety_type_images').prop(
            {
                'src': data.ImageURI
            });
            $('#' + rowId + ' .sr_result_name_spans').text(data.ResultName);
            $('#' + rowId + ' .sr_result_name_spans2').text(data.ResultName);
            $('#' + rowId + ' .sr_result_name_spans3').text(data.ResultName);
            $('#' + rowId + ' .sr_variety_subheader').text(data.SubHeaderText);
            $('#' + rowId + ' .sr_variety_catalog').prop(
            {
                'href': '/Catalog/Search?Category=' +  data.CatalogProductFormCategoryCode + '&Form=' + data.CatalogProgramTypeCode
            });
            //if ((data.Program1ProductCount || 0) > 0) {
                $('#' + rowId + ' .sr_variety_program_td1').text(data.Program1Name);
                $('#' + rowId + ' .sr_variety_avail_anchor1').prop(
                {
                    'href': '/Availability/Search?Category=' +  data.Program1ProgramTypeCode + '&Form=' + data.Program1ProductFormCategoryCode
                });
                $('#' + rowId + ' .sr_variety_avail_anchor1').text(data.Program1ProductCount + ' Products');
                $('#' + rowId + ' .sr_variety_species_anchor1').prop(
                {
                    'href': '/Availability/Search?Category=' +  data.Program1ProgramTypeCode + '&Form=' + data.Program1ProductFormCategoryCode
                });
                $('#' + rowId + ' .sr_variety_species_anchor1').text('All ' + data.Species1Name  + ' ' +  data.Program1Name);
                $('#' + rowId + ' .sr_variety_program_td2').text(data.Program2Name);
            //};
            //if ((data.Program2ProductCount || 0) > 0) {

                $('#' + rowId + ' .sr_variety_avail_anchor2').prop(
                {
                    'href': '/Availability/Search?Category=' + data.Program2ProgramTypeCode + '&Form=' + data.Program2ProductFormCategoryCode
                });
                $('#' + rowId + ' .sr_variety_avail_anchor2').text(data.Program2ProductCount + ' Products');
                $('#' + rowId + ' .sr_variety_species_anchor2').prop(
                {
                    'href': '/Availability/Search?Category=' + data.Program2ProgramTypeCode + '&Form=' + data.Program2ProductFormCategoryCode
                });
            //};
            //if ((data.Program3ProductCount || 0) > 0) {
                $('#' + rowId + ' .sr_variety_species_anchor2').text('All ' + data.Species2Name + ' ' + data.Program2Name);
                $('#' + rowId + ' .sr_variety_program_td3').text(data.Program3Name);
                $('#' + rowId + ' .sr_variety_avail_anchor3').prop(
                {
                    'href': '/Availability/Search?Category=' + data.Program3ProgramTypeCode + '&Form=' + data.Program3ProductFormCategoryCode
                });
                $('#' + rowId + ' .sr_variety_avail_anchor3').text(data.Program3ProductCount + ' Products');
                $('#' + rowId + ' .sr_variety_species_anchor3').prop({ 'href': '/Availability/Search?Category=' + data.Program3ProgramTypeCode + '&Form=' + data.Program3ProductFormCategoryCode });
                $('#' + rowId + ' .sr_variety_species_anchor3').text('All ' + data.Species3Name + ' ' + data.Program3Name);
            //};
        //}
    }
    , two: function(e) {alert(e.RowTypeTemplateGuid);}
}


var init = function(data) {
    var rowId = eps_createNewGuid();
    $('#sr_variety_type_template').prop({ 'id': rowId });
    $('#' + rowId + ' .sr_variety_type_anchors').prop(
    {
        'href': '/Catalog/Search?Category=' + data.CatalogProductFormCategoryCode + '&Form=' + data.CatalogProgramTypeCode
    });
    $('#' + rowId + ' .sr_variety_type_images').prop(
    {
        'src': data.ImageURI
    });
    $('#' + rowId + ' .sr_result_name_spans').text(data.ResultName);
    $('#' + rowId + ' .sr_result_name_spans2').text(data.ResultName);
    $('#' + rowId + ' .sr_result_name_spans3').text(data.ResultName);
    $('#' + rowId + ' .sr_variety_subheader').text(data.SubHeaderText);
    $('#' + rowId + ' .sr_variety_catalog').prop(
    {
        'href': '/Catalog/Search?Category=' + data.CatalogProductFormCategoryCode + '&Form=' + data.CatalogProgramTypeCode
    });
    $('#' + rowId + ' .sr_variety_program_td1').text(data.Program1Name);
    $('#' + rowId + ' .sr_variety_avail_anchor1').prop(
    {
        'href': '/Availability/Search?Category=' + data.Program1ProgramTypeCode + '&Form=' + data.Program1ProductFormCategoryCode
    });
    $('#' + rowId + ' .sr_variety_avail_anchor1').text(data.Program1ProductCount + ' Products');
    $('#' + rowId + ' .sr_variety_species_anchor1').prop(
    {
        'href': '/Availability/Search?Category=' + data.Program1ProgramTypeCode + '&Form=' + data.Program1ProductFormCategoryCode
    });
    $('#' + rowId + ' .sr_variety_species_anchor1').text('All ' + data.Species1Name + ' ' + data.Program1Name);
    $('#' + rowId + ' .sr_variety_program_td2').text(data.Program2Name);
    $('#' + rowId + ' .sr_variety_avail_anchor2').prop(
    {
        'href': '/Availability/Search?Category=' + data.Program2ProgramTypeCode + '&Form=' + data.Program2ProductFormCategoryCode
    });
    $('#' + rowId + ' .sr_variety_avail_anchor2').text(data.Program2ProductCount + ' Products');
    $('#' + rowId + ' .sr_variety_species_anchor2').prop(
    {
        'href': '/Availability/Search?Category=' + data.Program2ProgramTypeCode + '&Form=' + data.Program2ProductFormCategoryCode
    });
    $('#' + rowId + ' .sr_variety_species_anchor2').text('All ' + data.Species2Name + ' ' + data.Program2Name);
    $('#' + rowId + ' .sr_variety_program_td3').text(data.Program3Name);
    $('#' + rowId + ' .sr_variety_avail_anchor3').prop(
    {
        'href': '/Availability/Search?Category=' + data.Program3ProgramTypeCode + '&Form=' + data.Program3ProductFormCategoryCode
    });
    $('#' + rowId + ' .sr_variety_avail_anchor3').text(data.Program3ProductCount + ' Products');
    $('#' + rowId + ' .sr_variety_species_anchor3').prop({ 'href': '/Availability/Search?Category=' + data.Program3ProgramTypeCode + '&Form=' + data.Program3ProductFormCategoryCode });
    $('#' + rowId + ' .sr_variety_species_anchor3').text('All ' + data.Species3Name + ' ' + data.Program3Name);
};

var two = function(e) {
    alert(e.RowTypeTemplateGuid
    );
};




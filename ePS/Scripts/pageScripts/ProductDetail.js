﻿
        //ToDo: move this into the model
        function composeDataApi(){
            //productCategoryCode
            var url = SelectionScope.DataApi + "blahblah";
            $.ajax({
                url: url,
                data: {
                    PlantCategoryCode: SelectionScope.ProductCategoryCode,
                    ProductFormCode: SelectionScope.ProductFormCode,
                    SupplierCodes: SelectionScope.SupplierCodes,
                    VarietyCode: SelectionScope.VarietyCode,
                    ShipWeekString: SelectionScope.ShipWeekString

                },
                success: function () {
                    alert("success");
                },
                dataType: "json"
            });

            //$.get(SelectionScope.DataApi + '?productCategoryCode=boohoo', function (data) {
            //    //$('.result').html(data);
            //    alert('Get was performed.');
            //});



            return SelectionScope.DataApi;
        }

        //ToDo: Add a Working.gif to model and use it here
        function ajaxGet() {
            //alert("ajaxGet");
            var targetEt = $("#elapsed_time");
            var start = new Date();
            $.ajax({
                type: "get",
                url: "/api/ProductDetail/Detail",
                data: {
                    PlantCategoryCode: SelectionScope.ProductCategoryCode,
                    ProductFormCode: SelectionScope.ProductFormCode,
                    SupplierCodes: SelectionScope.SupplierCodes,
                    VarietyCode: SelectionScope.VarietyCode,
                    ShipWeekString: SelectionScope.ShipWeekString
                },
                timeout: 20000,
                dataType: "json",
                context: document.body,
                beforeSend: function (xhr) {
                    //xhr.overrideMimeType("text/plain; charset=x-user-defined");
                    model.workingState(true);
                },
                success: function (response) {
                    // successful request; do something with the data
                    //$('#json_response').empty();
                    //var jsonString = JSON.stringify(response);
                    //$("#json_response").html("JSON returned:<br />" + jsonString);
                    model.clearGrid();
                    model.InitializeGrid(response);
                    $('#input_0').focus().select();  //ToDo: move current focus into model and keep it current during life of grid
                },
                complete: function () {
                    var end = new Date();
                    targetEt.html("Milliseconds: " + (end - start));
                    model.workingState(false);
                    return false;
                }
                //error: function (xhr, textStatus, thrownError) {
                //    // failed request; give feedback to user
                //    //$('#ajax-panel').html('<p class="error"><strong>Oops!</strong> Try that again in a few moments.</p>');
                //    alert("Request failed: \nError: " + thrownError + "\n\nResponse:" + xhr.responseText + "\n\nStatus:" + textStatus);
                //}

            });
        }

        var initialData = [];
        //initialData.push({ Guid: "B528080F-90E6-4F57-A2CB-E469F163FB25", Product: "Gypsy White", OrderQuantity: 5500, Availability: [50000, 10000, 26000, 30000, 1900] });
        //initialData.push({ Guid: "C844D6C3-7D6A-4DF6-AF61-381463801F7E", Product: "Achillea #1", OrderQuantity: 0, Availability: [55000, 28500, 12000, 59000, 10000] });
        //initialData.push({ Guid: "79FD8E29-57D4-4E53-9297-FF6EBF148650", Product: "Achillea #3", OrderQuantity: 2800, Availability: [15000, 14250, 38500, 30000, 22000] });
        //initialData.push({ Guid: "0BCDEB35-2FCA-42B3-8119-0F031FEE18D3", Product: "Achillea #4", OrderQuantity: 0, Availability: [18000, 17600, 19500, 30000, 16500] });
        //initialData.push({ Guid: "BD3DBA2A-24DF-4806-92E5-F8BCF7120EBB", Product: "Achillea #5", OrderQuantity: 3000, Availability: [25000, 23000, 22250, 30000, 8000] });
        //initialData.push({ Guid: "36259B53-1C5C-4093-98B5-739C42C04917", Product: "Achillea #8", OrderQuantity: 0, Availability: [15000, 3850, 38500, 30000, 2200] });
        //initialData.push({ Guid: "0D7E5090-7D84-4D4B-B0AA-F8EC184B0092", Product: "Achillea #6", OrderQuantity: 0, Availability: [18000, 19000, 19500, 30000, 31000] });
        //initialData.push({ Guid: "69A6C3A2-36B0-4192-B439-1A4B37310F7E", Product: "Achillea #7", OrderQuantity: 3000, Availability: [25000, 23000, 22250, 30000, 7500] });

        var model = {}; //ToDo: Refactor this with clearer approach to managing data and gridview
        model.rowData = [];
        model.workingState = function (state) { var el = $("#ship_weeks_loading_icon"); if (state) { el.css("display", "block").delay(200); } else { el.css("display", "none"); } };
        model.createRow = function(i) {
            //ToDo: just get data that is to be displayed
            data = model.rowData;

            var rowDef = $("<tr></tr>");

            data[i].Guid = data[i].Guid.toLowerCase(); //make sure guids handled in lowercase for consistency
            //var guid = data[i].Guid;

            $(rowDef).attr("id", data[i].Guid);

            //guid = rowDef.id;
            //alert('val ' + guid);

            //Column 0
            var tdDef = $('<td class="col_Supplier"></td>');
            tdDef.text(data[i].SupplierName);
            tdDef.appendTo(rowDef);

            //Column 1
            tdDef = $('<td  class="col_plant_variety" >??</td>');
            $(tdDef).text(data[i].Product);
            tdDef.appendTo(rowDef);

            //Column 2
            tdDef = $('<td  class="col_product_form" >??</td>');
            $(tdDef).text(data[i].ProductForm);
            tdDef.appendTo(rowDef);

            //Column 3
            tdDef = $('<td  class="col_order_quantity" ></td>');
            var inputBox = $("<input type='text' value='0' class='order_quantity_inputs' />");
            $(inputBox).val(numeral(data[i].OrderQuantity).format('0,0'));
            $(inputBox).attr('id', "input_" + i);
            inputBox.appendTo(tdDef);
            tdDef.appendTo(rowDef);


            //Column 4
            tdDef = $('<td  class="col_week_less_1" >??</td>');
            $(tdDef).text(numeral(data[i].Availability[0]).format('0,0'));
            tdDef.appendTo(rowDef);

            //Column 5
            tdDef = $('<td  class="col_ship_week" >??</td>');
            $(tdDef).text(numeral(data[i].Availability[1]).format('0,0'));
            tdDef.appendTo(rowDef);

            //Column 6
            tdDef = $('<td  class="col_week_plus_1" >??</td>');
            $(tdDef).text(numeral(data[i].Availability[2]).format('0,0'));
            tdDef.appendTo(rowDef);

            //Column 7
            tdDef = $('<td  class="col_week_plus_2" >??</td>');
            $(tdDef).text(numeral(data[i].Availability[3]).format('0,0'));
            tdDef.appendTo(rowDef);

            //Column 8
            tdDef = $('<td  class="col_week_plus_3" >??</td>');
            $(tdDef).text(numeral(data[i].Availability[4]).format('0,0'));
            tdDef.appendTo(rowDef);

            //Column 9
            var tdDef = $('<td class="col_8"></td>');
            tdDef.appendTo(rowDef);

            //Append Row
            //rowDef.appendTo("#outer_grid tbody");
            rowDef.appendTo("#inner_grid tbody");

            //this.rowData.push(data[i]);

            //alert('rows: '+this.rowData.length);


        };
        model.clearGrid = function () {
            $("#inner_grid tbody").empty();
        };
        model.InitializeGrid = function (data) {
            model.rowData = data;
            beginIndex = 0;
            lastIndex = data.length;

            for (var i = beginIndex; i < lastIndex; i++) {
                model.createRow(i);
            }

            $("#grid_status").text(" " + model.rowData.length + " products");

            ////ToDo: Should this be moved back to row definition so subsequent appends will not create two delete buttons in existing rows.
            //var deleteButton = $("<input type='button' value='delete' class='table_delete_buttons' />");
            //deleteButton.click(function () {
            //    var guid = $(this).parent().parent().attr('id');
            //    //alert('delete: ' + guid);
            //    model.deleteRow(guid);
            //    return false;
            //});
            //$("#col_Supplier").empty();  //necessary to avoid duplicate buttons on future appends
            //deleteButton.appendTo(".col_Supplier");
            ///////////////////////////////////////////////////////////////////////////////////


            //var orderElement = $("#add_to_cart");
            $("#add_to_cart").on("click", function () {
                //alert("" + model.TotalOrderQuantity());
                if (model.TotalOrderQuantity() > 0) {
                    alert("where am I");
                    model.AjaxAddToCart();
                } else {
                    alert("Note: There are no Order Quantities to place in your cart.");
                };
            });


            $(".order_quantity_inputs").on("change", function () {
                //ToDo: This fires for each row during model.InitializeGrid
                //alert('bingo');

                //get particulars
                var guid = $(this).parent().parent().attr('id');
                var inputBox = $(this);
                var vint = parseInt(inputBox.val().replace(",", "")) || 0;

                //If value exceeds shipweek Availability, reset it to shipweek Availability, tell user
                
                var avail = model.readAvailabilty(guid, 0);  //no offset from shipweek
                if (vint > avail) {
                    alert("Warning: Value exceeds Availability for selected shipweek.\n\nReduced to product available.\n\nToDo: replace this alert with ePS Dialogue Box.\n");
                    vint = avail;
                }

                //format entry and display it
                inputBox.val(numeral(vint).format('0,0'));


                //save new value to data
                model.updateOrderQuantity(guid, vint);

                model.displayOrderQuantityTotal($("#total_order_quantity"));

            }).change();

            $('.order_quantity_inputs').keydown(function (e) {
                var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
                //alert(key);
                if (key == 13 || key == 9) {
                    e.preventDefault();
                    //var inputs = $(this).closest('form').find(':input:visible');
                    //inputs.eq(inputs.index(this) + 1).focus();
                    //$(".order_quantity_inputs").change();
                    //return false;

                    var targetId = $(this).attr('id');
                                        
                    s = parseInt(targetId.substr(targetId.lastIndexOf("_") + 1)) + 1;
                    if (s > model.rowData.length - 1) { s = 0; };
                    targetId = targetId.substr(0, targetId.lastIndexOf("_")+1) + s;
                    
                    //alert(targetId)

                    $("#" + targetId).focus().select();
                }

            });
        };
        model.deleteRow = function (guid) {
            //ToDo: code to validate guid coming in

            //find and delete dataRow
            for (var i = 0; i < model.rowData.length; i++) {
                if (model.rowData[i].Guid == guid) {
                    model.rowData.splice(i, 1);
                    break;
                }
            }
            //delete Table Row
            $("tr#" + guid).remove();
            //Update Total Display
            model.displayOrderQuantityTotal($("#total_order_quantity"));

        };
        model.updateOrderQuantity = function(guid, newQuantity){
            //ToDo: Code to validate inputs before proceeding
            var v = parseInt(newQuantity);
            for (var i = 0; i < model.rowData.length; i++) {
                if (model.rowData[i].Guid == guid) {
                    model.rowData[i].OrderQuantity = v;
                    break;
                }
            }
        };
        model.readAvailabilty = function (guid, offset) {
            //offset 0 = Ship Week
            //ToDo: Get offset working by changing data format to have shipweek be a property like OrderQuantity and to Availability arrays, one plus and one minus
            if (offset == 0) ndx = 1;

            for (var i = 0; i < model.rowData.length; i++) {
                if (model.rowData[i].Guid == guid) {
                    return model.rowData[i].Availability[ndx];
                    break;
                }
            }
            return 0;
        };
        model.TotalOrderQuantity = function () {
            //returns integer count of OrderQuantity for all rows
            var total = 0;
            var i = model.rowData.length;
            $.each(model.rowData, function (i, val) {
                //alert('bingo ' + val.OrderQuantity);
                total += val.OrderQuantity;
            });
            return total;
        };
        model.displayOrderQuantityTotal = function (elem) {
            if ($(elem).length > 0) {

                ////calculate new sum for column
                //var total = 0;
                //var i = model.rowData.length;
                //$.each(model.rowData, function (i, val) {
                //    //alert('bingo ' + val.OrderQuantity);
                //    total += val.OrderQuantity;
                //});

                ////write new sum for column to view
                //$(elem).text(numeral(total).format('0,0'));

                $(elem).text(numeral(model.TotalOrderQuantity()).format('0,0'));


            }


        };
        model.rowDataToJson = function () {
            //ToDo: Finish This
            var temp = { rowData: model.rowData };
            return JSON.stringify(temp);

        };
        model.sortDirection = 0;
        model.sortDataRows = function () {
            //flip sort direction from current, default to ascending
            //alert('bingo');
            model.sortDirection = model.sortDirection > 0 ? -1 : 1;
            //create sortFunction
            var sortFunction = function (a, b) {
                if (a.Product == b.Product) { return 0; };
                if (a.Product > b.Product) { return 1 * model.sortDirection; };
                return -1 * model.sortDirection;
            };

            //copy rowData and sort it
            var data = model.rowData;
            data.sort(sortFunction);
            //clear grid and delete rowData
            $("#inner_grid tbody").empty();
            model.rowData = [];
            //append sorted data
            model.InitializeGrid(data);
        };
        model.AjaxGetShipWeeks = function () {
            var targetEt = $("#elapsed_time");
            var start = new Date();
            $.ajax({
                type: "get",
                url: "/api/ShipWeek/GetShipWeeks",
                data: {
                    PlantCategoryCode: SelectionScope.ProductCategoryCode,
                    ProductFormCode: SelectionScope.ProductFormCode,
                    SupplierCodes: SelectionScope.SupplierCodes,
                    VarietyCode: SelectionScope.VarietyCode,
                    ShipWeekString: SelectionScope.ShipWeekString
                },
                dataType: "json",
                context: document.body,
                beforeSend: function (xhr) {
                    //xhr.overrideMimeType("text/plain; charset=x-user-defined");
                    model.workingState(true);
                },
                success: function (response) {
                    // successful request; do something with the data
                    $('#json_response').empty();
                    var jsonString = JSON.stringify(response);
                    $("#json_response").html("JSON returned:<br />" + jsonString);
                    //alert(response.SelectedShipWeek.ShipWeekString);
                    //alert(response.DisplayShipWeeks.length);

                    var targetSelect = $("#ship_week_select_list")
                    targetSelect.empty();
                    for (var i = 0; i < response.DisplayShipWeeks.length; i++) {
                        var option = $("<option></option>");
                        option.prop("id", response.DisplayShipWeeks[i].ShipWeekString);
                        option.text("week " + response.DisplayShipWeeks[i].Week + " of " + response.DisplayShipWeeks[i].Year);
                        if (response.DisplayShipWeeks[i].Week == response.SelectedShipWeek.Week && response.DisplayShipWeeks[i].Year == response.SelectedShipWeek.Year) {
                            option.prop("selected", true);
                        }
                        targetSelect.append(option);
                    }
                    targetSelect.off("change");  //ToDo: find out if this actually persists and can be moved to single occurance for past and future
                    targetSelect.on("change", function () {
                        var selectedOptionId = targetSelect.find(":selected").prop("id");
                        //alert("changed to " + selectedOptionId);
                        SelectionScope.ShipWeekString = selectedOptionId;
                        ajaxGet();
                    });

                    //model.clearGrid();
                    //model.InitializeGrid(response);
                    //$('#input_0').focus().select();  //ToDo: move current focus into model and keep it current during life of grid
                },
                complete: function () {
                    var end = new Date();
                    targetEt.html("Milliseconds: " + (end - start));
                    //model.workingState(false);
                    ajaxGet();
                    return false;
                }
                //error: function (jqXHR, textStatus) {
                //    // failed request; give feedback to user
                //    //$('#ajax-panel').html('<p class="error"><strong>Oops!</strong> Try that again in a few moments.</p>');
                //    alert("Request failed: " + textStatus);
                //}

            });
        }


        var viewModel = new Object;
        viewModel.DisplayDescriptionHtml = function (guid) {
            if (typeof guid == 'undefined' || guid == null || guid.length != 36) { return false; };
            //var url = "/api/content/" + guid;
            var url = "/api/content/";

            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {'id': guid},
                success: function (response) {
                    //alert(JSON.stringify(response));
                    $('#content_display_description').html(response.html);
                    

                    //alert('Load was performed.');
                }
            });

        };

        //alert("Product Detail: bingo");

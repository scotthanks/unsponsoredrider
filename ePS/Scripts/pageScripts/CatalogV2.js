﻿

var model = {};
model.configData = {
    isConfigured: false,
    urlSupplierApi: '/api/supplier/',
    urlSpeciesApi: '/api/species/',
    urlVarietiesApi: '/api/varieties/',
    urlVarietyDetailPage: '/home/ProductDetail/',

    urlProductDetailPartialPage: "/catalog/ProductDetail/",

    supplierContainerId: 'supplier_selection_container',
    speciesContainerId: 'species_selection_container',

    supplierListId: 'supplier_selection_list',
    speciesListId: 'species_selection_list',
    varietySetsDisplayId: 'variety_sets_display',
    varietySetClass: 'variety_set',
    varietyTileClass: 'variety_set_list_item',
    urlGetSupplierData: function () { return model.configData.urlSupplierApi + "-1?PlantCategoryCode=" + model.selectionState.plantCategoryCode + "&ProductFormCode=" + model.selectionState.productFormCode; },
    urlGetSpeciesData: function () { var url = model.configData.urlSpeciesApi + "-1?PlantCategoryCode=" + model.selectionState.plantCategoryCode + "&ProductFormCode=" + model.selectionState.productFormCode + "&SupplierCodes=" + model.selectionState.selectedSupplierCodes(); return url; },
    urlGetVarietySetsData: function () {
        return model.configData.urlVarietiesApi + "-1?PlantCategoryCode=" + model.selectionState.plantCategoryCode + "&ProductFormCode=" + model.selectionState.productFormCode + "&SupplierCodes=" + model.selectionState.selectedSupplierCodes() + "&SpeciesCodes=" + model.selectionState.selectedSpeciesCodes();
    },
    urlGetProductDetailPartialPage: function (varietyCode) {
        return model.configData.urlProductDetailPartialPage + "varietyDetailAvailability?plantCategoryCode=" + model.selectionState.plantCategoryCode + "&ProductFormCode=" + model.selectionState.productFormCode + "&supplierCodes=" + model.selectionState.selectedSupplierCodes() + "&varietyCode=" + varietyCode + "&shipWeek=30|2013";
    },
    urlGoToVarietyDetailPage: function (varietyCode) {
        return model.configData.urlVarietyDetailPage + "varietyDetailAvailability?plantCategoryCode=" + model.selectionState.plantCategoryCode + "&ProductFormCode=" + model.selectionState.productFormCode + "&supplierCodes=" + model.selectionState.selectedSupplierCodes() + "&varietyCode=" + varietyCode + "&shipWeek=30|2013";
    }
};
model.url = {
    mode: null,
    category: "",
    form: "",
    supplierCodes: [],
    speciesCodes: [],
    productCode: null, //used by both modes

    parse: function() {
        var location = window.location;
        //console.log('location', location);
        // make url lower case
        window.history.replaceState({}, '', location.href.replace(' ','').toLowerCase());   //replace url - no spaces and lowercase
        var path = location.pathname.replace(/(^[/\s]+)|([/\s]+$)/g, '').toLowerCase(); //remove leading and trailing slash
        //console.log('path', path);
        var pathParams = path.split("/");
        //console.log(pathParams[0]);
        var hash = location.hash.replace(/(^[#\s]+)|([/\s]+$)/g, '').toLowerCase(); //remove leading hash and trailing slash
        //console.log('hash', hash);
        var hashParams = hash.split("/");

        switch (pathParams[0]) {
        case 'catalog':
            model.url.mode = 'catalog';
            model.url.category = pathParams[1] || null;
            model.url.form = pathParams[2] || null;

            //Process Catalog Hash
            model.url.supplierCodes = (hashParams[0] || '') == '' ? [] : hashParams[0].toLowerCase().split(',') || [];
            model.url.speciesCodes = (hashParams[1] || '') == '' ? [] : hashParams[1].toLowerCase().split(',') || [];
            model.url.productCode = hashParams[2] || null;
            break;
        case 'product':
            model.url.mode = 'product';
            //Process Product Hash
            model.url.productCode = pathParams[1] || []; //produtCode is path, not hash in product mode
            break;
        default:
            //unrecognized call
        };
        //console.log('model.url', model.url);

        if (model.url.mode == 'catalog') {
            $('#test_url_mode').text('Mode: ' + model.url.mode).show();
            $('#test_url_category').text('Category: ' + model.url.category).show();
            $('#test_url_form').text('Form: ' + model.url.form).show();
            $('#test_url_suppliers').text('suppliers: ' + JSON.stringify(model.url.supplierCodes)).show();
            $('#test_url_species').text('species: ' + JSON.stringify(model.url.speciesCodes)).show();
            $('#test_url_product').text('Product: ' + model.url.productCode).show();

        } else if (model.url.mode == 'product') {
            $('#test_url_mode').text('Mode: ' + model.url.mode).show();
            $('#test_url_category').text('Category: ' + model.url.category).hide();
            $('#test_url_form').text('Form: ' + model.url.form).hide();
            $('#test_url_suppliers').text('Suppliers: ' + JSON.stringify(model.url.supplierCodes)).hide();
            $('#test_url_species').text('Species: ' + JSON.stringify(model.url.speciesCodes)).hide();
            $('#test_url_product').text('Product: ' + model.url.productCode).show();

        } else {

        }
    },

    supplierIsSelected: function(code) {
        //console.log(model.url.supplierCodes);
        if ((model.url.supplierCodes.length || 0) == 0 || model.url.supplierCodes[0] == '$') { return true; };
        return model.helpers.isStrInArr(code, model.url.supplierCodes);
    },
    speciesIsSelected: function (code) {
        if ((model.url.speciesCodes.length || 0) == 0 || model.url.speciesCodes[0] == '$') { return true; };
        return model.helpers.isStrInArr(code, model.url.speciesCodes);
    }
};

model.helpers = {
    isStrInArr: function (str, arr) {
        //Case Insensitive, returns true if found
        if (typeof str !== 'string' || str.trim() == '') { return false; };
        if ((arr.length || 0 ) == 0) { return false; };
        str = str.toLowerCase();
        var returnValue = false;
        $.each(arr, function (i, val) {
            //console.log(str);
            if (str == val.trim().toLowerCase()) { returnValue = true; };
        });
        return returnValue;
    }
};


model.selectionState = {

    mode: null,
    productCode:null,
    
    plantCategoryCode: "",
    plantCategoryName: "TBD",

    productFormCode: "",
    productFormName: "TBD",




    selectedSupplierCodes: function () {
        //Returns supplier codes currently selected
        var returnArray = [];
        var items = $("#" + model.configData.supplierListId + " li.selection_selected");
        $.each(items, function (i, value) { returnArray.push($(this).data("code")); });
        return returnArray;
    },
    selectedSpeciesCodes: function () {
        //Returns species codes currently selected
        var returnArray = [];
        var items = $("#" + model.configData.speciesListId + " li.selection_selected");
        $.each(items, function (i, value) { returnArray.push($(this).data("code")); });
        return returnArray;
    }
};


model.displayState = {
    Columns: 8,
    Rows: 10,
    CurrentPage: 1,
    TotalPages: 5,
    VarietySetCount: 33,
    VarietiesCount: 400,
    Calc: function () {
        //model.VarietySetsData.length;
        this.VarietySetCount = (model.VarietySetsData.length || 0);
        this.TotalPages = Math.floor(this.VarietySetCount / 3); if (this.VarietySetCount % 3 > 0) { this.TotalPages++; };
        model.displayState.VarietiesCount = 0;
        $.each(model.VarietySetsData, function (i, set) {  //foreach variety Set
            $.each(set.SelectedVarieties, function (ii, variety) {
                model.displayState.VarietiesCount++;
            });
        });

    },
};
model.SupplierData = [];
model.SpeciesData = [];
model.VarietySetsData = [];


var viewModel = {};
viewModel.config = {
    grid: {
        colsHeader: 11,
        colsFooter: 11,
        colsData: 11,
        rowsHeader: 1,
        rowsData: 10,
        rowsFooter: 3,

        headerRowClassStem: "headerRow_",
        headerColClassStem: "headerCol_",
        dataRowIdStem: "dataRow_",
        dataColClassStem: "dataCol_",
        footerRowClassStem: "footerRow_",
        footerColClassStem: "footerCol_",
        shipWeekDisplayId: "ship_week_display_span",
        headingSortableClass: "paging_heading_sortable",
        headingSortableUnsortedClass: "paging_heading_sortable_unsorted",
        headingSortableAscendingClass: "paging_heading_sortable_ascending",
        headingSortableDescendingClass: "paging_heading_sortable_descending"

    },
    varietyDisplay: {
        //DisplayCurrentRowId: "variety_display_current_row",
        //displayRowCountId: "variety_display_row_count",
        //displaySpeciesCountId: "variety_display_species_count",
        displayVarietiesCountId: "variety_display_varieties_count",

    },
    paging: {
        width: 5,
        height: 3,
        scrollbarContainerId: "paging_scrollbar_container",
        scrollbarId: "paging_scrollbar",
        scrollbarUpButtonId: "scrollbar_up_button",
        scrollbarDownButtonId: "scrollbar_down_button",
        statusDisplayId: "paging_status_display",
    }
}

viewModel.state = {

    paging: {
        active: false,
        currentRow: {},
        pageCount: 0,
        rowCount: 0,
        speciesCount: 0,
        varietiesCount: 0
    },
    shipWeekListComposed: false,
    supplierListComposed: false,
    speciesListComposed: false,
    shipWeekListContainerElement: null,
    shipWeekSelected: {},  //this is set to shipweek data row by ship week list
    priorSpeciesSelected: [],
    gridPages: function () {
        var rows = model.rowData.length;
        var rowsPerPage = viewModel.config.grid.rowsData;
        if (rows <= 0) { return 0; };
        if (rows <= rowsPerPage) { return 1; };
        var pages = Math.floor(rows / rowsPerPage);
        if ((rows % rowsPerPage) > 0) { pages++ };
        return pages;
    },
    gridPageDisplayed: 0,
    gridPageIsDisplaying: false,
    Show: function () {
        var s = "Model State:\n";
        s += "\nviewModel.state.paging.speciesCount: " + viewModel.state.paging.speciesCount;
        s += "\nviewModel.state.paging.varietiesCount: " + viewModel.state.paging.varietiesCount;
        s += "\nviewModel.state.paging.rowCount: " + viewModel.state.paging.rowCount;
        s += "\ncount / width: " + (viewModel.state.paging.varietiesCount / viewModel.config.paging.width);

        s += "\nviewModel.config.paging.width: " + viewModel.config.paging.width;


        s += '\nslider("option", "max"): ' + $("#" + viewModel.config.paging.scrollbarId).slider("option", "max");

        s += '\nslider("option", "value"): ' + $("#" + viewModel.config.paging.scrollbarId).slider("option", "value");

        //s += '\n.slider("option", "max") - ui.value + 1' + $("#" + viewModel.config.paging.scrollbarId).slider("option", "max") - ui.value + 1;


        //$("#" + viewModel.config.varietyDisplay.displayVarietiesCountId).text(model.displayState.VarietiesCount)
        //$("#" + viewModel.config.varietyDisplay.displaySpeciesCountId).text(model.displayState.VarietySetCount)
        //$("#" + viewModel.config.varietyDisplay.displayCurrentRowId).text(model.displayState.CurrentPage)
        //$("#" + viewModel.config.varietyDisplay.displayRowCountId).text(model.displayState.TotalPages)

        alert(s);
    }
}

viewModel.init = function() {
    //console.log('CatalogV2', 'init begin');

    window.addEventListener("hashchange", function () {
        //alert(window.location.hash);
        //console.log("Hash changed to", window.location.hash);
        model.url.parse();
        viewModel.clearSupplierList([
            viewModel.clearSpeciesList,
            viewModel.clearVarietySetsDisplay,
            viewModel.deleteSupplierData,
            viewModel.retrieveSupplierData,
            viewModel.displaySupplierList,
            viewModel.deleteSpeciesData,
            viewModel.retrieveSpeciesData,
            viewModel.displaySpeciesList,
            viewModel.deleteVarietySetsData,
            viewModel.retrieveVarietySetsData
        ]);

    });

    //model.selectionState should already be intialized.

    viewModel.SetEventsToggleAll();

    viewModel.clearSupplierList([
        viewModel.clearSpeciesList,
        viewModel.clearVarietySetsDisplay,
        viewModel.deleteSupplierData,
        viewModel.retrieveSupplierData,
        viewModel.displaySupplierList,
        viewModel.deleteSpeciesData,
        viewModel.retrieveSpeciesData,
        viewModel.displaySpeciesList,
        viewModel.deleteVarietySetsData,
        viewModel.retrieveVarietySetsData
        //, function() {
        //    console.log('model.VarietySetsData', model.VarietySetsData);
        //}
    ]);

    //init new scrollbar
    viewModel.scrollBar.init();

    //init scrollBar
    $("#" + viewModel.config.paging.scrollbarId).slider({
        orientation: "vertical",
        range: false,
        min: 0,
        max: 100,
        value: 0,
        step: 1,
        change: function (e, ui) {
            //alert('change');
            var row = $("#" + viewModel.config.paging.scrollbarId).slider("option", "max") - ui.value + 1;
            viewModel.displayVarietyPage(row);
        },
        slide: function (event, ui) {
            //alert('slide');
            var row = $("#" + viewModel.config.paging.scrollbarId).slider("option", "max") - ui.value + 1;
            viewModel.displayVarietyPage(row);
        },
        //stop: function (event, ui) {
        //    alert('stop');
        //}
    });


    var upButton = $("#" + viewModel.config.paging.scrollbarUpButtonId);
    upButton.off().on("click", function () {
        var min = $("#" + viewModel.config.paging.scrollbarId).slider("option", "min");
        var value = $("#" + viewModel.config.paging.scrollbarId).slider("option", "value");
        var newvalue = value + 1;
        if (newvalue > min) {
            $("#" + viewModel.config.paging.scrollbarId).slider('value', newvalue);  //reverse number
        };
        $("#" + viewModel.config.paging.scrollbarContainerId + " .ui-slider-handle").focus();
    });

    var downButton = $("#" + viewModel.config.paging.scrollbarDownButtonId);
    downButton.off().on("click", function () {
        var max = $("#" + viewModel.config.paging.scrollbarId).slider("option", "max");
        var value = $("#" + viewModel.config.paging.scrollbarId).slider("option", "value");
        var newvalue = value - 1;
        if (newvalue >= 0) {
            $("#" + viewModel.config.paging.scrollbarId).slider('value', newvalue); //reverse number
        };
        $("#" + viewModel.config.paging.scrollbarContainerId + " .ui-slider-handle").focus();
    });


    //console.log('CatalogV2', 'init complete');

};




viewModel.scrollBar = {
    settings: {
        max: 100,
        min: 1,
        value: 1,
        pageRows: viewModel.config.paging.height
    }
};

viewModel.scrollBar.init = function () {
    var bar = $("#" + "new_paging_scrollbar");
    var handle = $("#" + "new_paging_scrollbar_handle");

    bar.off().on("click", null, { "name": "fred" }, function (e) {
        e.stopPropagation();
        var handle = $("#new_paging_scrollbar_handle");
        var handlePosition = handle.position();
        var handleTop = handlePosition.top;
        var handleBottom = handleTop + $("#new_paging_scrollbar_handle").outerHeight();

        //alert("click:\n" + e.data.name + "\ntop: " + this.offsetTop + "\nlocation: " + e.pageY + "\nHandle top: " + handleTop + "\nHandle bottom: " + handleBottom);
        var message = "";
        var row = viewModel.state.paging.currentRow.number;
        if (e.pageY < handleTop) {
            message = "scroll up one page";
            //row--;
            row -= viewModel.scrollBar.settings.pageRows;
            if (row < viewModel.scrollBar.settings.min) { row = viewModel.scrollBar.settings.min; };
        } else if (e.pageY > handleBottom) {
            message = "scroll down one page";
            //row++;
            row += viewModel.scrollBar.settings.pageRows;
            if (row > viewModel.scrollBar.settings.max) { row = viewModel.scrollBar.settings.max; };
        } else {
            message = "something went wrong";
            //do nothing
        };
        //alert(message);


        viewModel.displayVarietyPage(row);
        viewModel.scrollBar.reset({ "value": row, "max": viewModel.state.paging.rowCount });
        //set focus on handle
        $("#new_paging_scrollbar_handle").focus();
    });

    handle.off().on("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
    })
        .off()
        .on("keydown", function (event) {

            switch (event.keyCode) {
                case $.ui.keyCode.HOME:
                    event.preventDefault();
                    event.stopPropagation();
                    //$("#new_scrollbar_up_button").click();
                    //viewModel.displayVarietyPage(1);
                    viewModel.scrollBar.reset({ "value": 1, "max": viewModel.state.paging.rowCount });
                    this.focus();

                    break;
                case $.ui.keyCode.END:
                    event.preventDefault();
                    event.stopPropagation();

                    //$("#new_scrollbar_down_button").click();
                    //viewModel.displayVarietyPage(viewModel.state.paging.rowCount);
                    viewModel.scrollBar.reset({ "value": viewModel.state.paging.rowCount, "max": viewModel.state.paging.rowCount });
                    this.focus();

                    break;
                case $.ui.keyCode.PAGE_UP:
                    event.preventDefault();
                    event.stopPropagation();
                    var row = viewModel.state.paging.currentRow.number;
                    row -= viewModel.scrollBar.settings.pageRows;
                    if (row < viewModel.scrollBar.settings.min) { row = viewModel.scrollBar.settings.min; };
                    //viewModel.displayVarietyPage(row);
                    viewModel.scrollBar.reset({ "value": row });
                    this.focus();
                    break;

                case $.ui.keyCode.PAGE_DOWN:
                    event.preventDefault();
                    event.stopPropagation();
                    var row = viewModel.state.paging.currentRow.number;
                    row += viewModel.scrollBar.settings.pageRows;
                    if (row > viewModel.scrollBar.settings.max) { row = viewModel.scrollBar.settings.max; };
                    //viewModel.displayVarietyPage(row);
                    viewModel.scrollBar.reset({ "value": row });
                    this.focus();
                    break;

                case $.ui.keyCode.UP:
                case $.ui.keyCode.LEFT:
                    event.preventDefault();
                    event.stopPropagation();
                    var row = viewModel.state.paging.currentRow.number;
                    row--;
                    if (row > viewModel.scrollBar.settings.max) { row = viewModel.scrollBar.settings.max; };
                    //viewModel.displayVarietyPage(row);
                    viewModel.scrollBar.reset({ "value": row });
                    this.focus();
                    break;

                case $.ui.keyCode.DOWN:
                case $.ui.keyCode.RIGHT:
                    event.preventDefault();
                    event.stopPropagation();
                    var row = viewModel.state.paging.currentRow.number;
                    row++;
                    if (row > viewModel.scrollBar.settings.max) { row = viewModel.scrollBar.settings.max; };
                    //viewModel.displayVarietyPage(row);
                    viewModel.scrollBar.reset({ "value": row });
                    this.focus();
                    break;



                    //event.stopPropagation();
                    //event.preventDefault();
                    //alert("key: " + event.keyCode);
                    //if (!this._keySliding) {
                    //    this._keySliding = true;
                    //    $(event.target).addClass("ui-state-active");
                    //    allowed = this._start(event, index);
                    //    if (allowed === false) {
                    //        return;
                    //    }
                    //}
                    break;
            }

        })
        .draggable({
            containment: "parent",
            scroll: false,
            grid: [0, 45],
            start: viewModel.scrollBar.startEvent,
            drag: viewModel.scrollBar.dragEvent,
            stop: viewModel.scrollBar.stopEvent,
        });

    $("#new_scrollbar_up_button")
        .off()
        .on("click", function () {
            var row = viewModel.scrollBar.settings.value - 1;
            if (row < viewModel.scrollBar.settings.min) { row = viewModel.scrollBar.settings.min; };
            //if (row != viewModel.state.paging.currentRow.number) {
            //    viewModel.displayVarietyPage(row);
            viewModel.scrollBar.reset({ "value": row });
            //};
            //set focus on handle
            $("#new_paging_scrollbar_handle").focus();

        });

    $("#new_scrollbar_down_button")
        .off()
        .on("click", function () {
            var row = viewModel.scrollBar.settings.value + 1
            if (row > viewModel.scrollBar.settings.max) { row = viewModel.scrollBar.settings.max; };
            //if (row != viewModel.state.paging.currentRow.number) {
            //    viewModel.displayVarietyPage(row);
            viewModel.scrollBar.reset({ "value": row });
            //}
            //set focus on handle
            $("#new_paging_scrollbar_handle").focus();
        });

    handle.focus();
};
viewModel.scrollBar.reset = function (values) {
    //min must be >= 0
    if (typeof values.min === 'number' && Math.floor(values.min) >= 0) {
        this.settings.min = Math.floor(values.min);
    };
    //max must be >= min
    if (typeof values.max === 'number' && Math.floor(values.max) >= this.settings.min) {
        this.settings.max = Math.floor(values.max);
    };
    //value must be between min and max, inclusive
    if (typeof values.value === 'number' && Math.floor(values.value) >= this.settings.min && Math.floor(values.value) <= this.settings.max) {
        this.settings.value = Math.floor(values.value);
    };
    //Reset Buttons
    var upButton = $("#new_scrollbar_up_button");
    var downButton = $("#new_scrollbar_down_button");
    if (this.settings.value <= this.settings.min) {
        upButton.removeClass("new_scrollbar_up_button_enabled").addClass("new_scrollbar_up_button_disabled");
        downButton.removeClass("new_scrollbar_down_button_disabled").addClass("new_scrollbar_down_button_enabled");
    } else if (this.settings.value >= this.settings.max) {
        upButton.removeClass("new_scrollbar_up_button_disabled").addClass("new_scrollbar_up_button_enabled");
        downButton.removeClass("new_scrollbar_down_button_enabled").addClass("new_scrollbar_down_button_disabled");
    } else {
        upButton.removeClass("new_scrollbar_up_button_disabled").addClass("new_scrollbar_up_button_enabled");
        downButton.removeClass("new_scrollbar_down_button_disabled").addClass("new_scrollbar_down_button_enabled");
    };

    var bar = $("#new_paging_scrollbar");
    var handle = $("#new_paging_scrollbar_handle");

    if (viewModel.state.paging.rowCount == 0) {
        handle.css({ "height": bar.height(), "top": 0 }).draggable("disable");
    } else {
        //reset handle size
        handle.draggable("enable");
        var handleHeight = Math.floor(bar.height() / (this.settings.max - this.settings.min + 1));
        if (handleHeight < 35) { handleHeight = 35; };
        handle.css("height", handleHeight);

        //reset handle top and grid value
        var range = bar.height() - handle.outerHeight();
        var count = viewModel.state.paging.rowCount;        //(this.settings.max - this.settings.min);
        var increment = range / count;
        var targetTop = Math.round(increment * (this.settings.value - this.settings.min));
        handle.css("top", targetTop).draggable({ grid: [0, increment] });
    };

    if (this.settings.value != viewModel.state.paging.currentRow.number) {
        //fire change event
        viewModel.scrollBar.changeEvent();
    };
};
viewModel.scrollBar.changeEvent = function () {
    if (this.settings.value != viewModel.state.paging.currentRow.number) {
        viewModel.displayVarietyPage(this.settings.value);
    };
};
viewModel.scrollBar.dragEvent = function (event, ui) {

    var handleHeight = $("#new_paging_scrollbar_handle").outerHeight();
    var range = $("#new_paging_scrollbar").height() - handleHeight;
    var rowIncrement = range / viewModel.state.paging.rowCount;

    var position = ui.position.top;
    $("#test_scrollbar_position").text(position);

    var row = Math.floor(position / rowIncrement) + viewModel.scrollBar.settings.min;

    if (row != viewModel.state.paging.currentRow.number) {

        viewModel.displayVarietyPage(row);
        viewModel.scrollBar.reset({ "value": row });
    }

    $("#test_scrollbar_row").text(row);

};
viewModel.scrollBar.startEvent = function (event, ui) {

};
viewModel.scrollBar.stopEvent = function (event, ui) {
    //adjust the handle to row it is showing

    //var top = ui.position.top;
    //var row = viewModel.state.paging.currentRow.number;
    //var handleHeight = $("#new_paging_scrollbar_handle").outerHeight();
    //var range = $("#new_paging_scrollbar").height() - handleHeight;
    //var rowIncrement = range / (viewModel.state.paging.rowCount - 1);
    //var targetTop = Math.floor(rowIncrement * (row - 1));
    //$("#new_paging_scrollbar_handle").css("top", targetTop);


    //alert(row + " x " + rowIncrement + " = " + targetTop);


};



viewModel.pagingSetRow = function (targetRow) {

    //$("#test_item_display").text(targetRow);

    viewModel.state.paging.currentRow = {};
    var row = 0;
    var setNdx = 0;
    var varietyNdx = 0;
    $.each(model.VarietySetsData, function (i, set) {
        setNdx = i;
        var len = set.SelectedVarieties.length;
        varietyNdx = 0;
        while (len > 0) {
            row++;
            if (row >= targetRow) { return false; };
            len = len - viewModel.config.paging.width;
            if (len < 0) { break; };
            varietyNdx += viewModel.config.paging.width;
        }
    });

    viewModel.state.paging.currentRow = {
        "number": row,
        "speciesOffset": setNdx,
        "varietiesOffset": varietyNdx
    };

    //if (viewModel.state.paging.currentRow.number == 0) { alert("bango!"); };
};

viewModel.pagingRefreshState = function () {
    //var target = $('#test_display').empty();
    var speciesCount = model.VarietySetsData.length || 0;
    var varietiesCount = 0
    var rowCount = 0
    for (var i = 0; i < speciesCount; i++) {
        var len = model.VarietySetsData[i].SelectedVarieties.length || 0;
        varietiesCount += len;
        var rows = Math.floor(len / viewModel.config.paging.width);
        if (len % viewModel.config.paging.width > 0) { rows++; };
        //target.append("<p>" + model.VarietySetsData[i].Name + ": " + len + " -> " + rows + "</p>");
        rowCount += rows;
    };
    //set state values
    viewModel.state.paging.pageCount = Math.floor(rowCount / viewModel.config.paging.height);
    if ((rowCount % viewModel.config.paging.height) > 0) viewModel.state.paging.pageCount++;
    viewModel.state.paging.rowCount = rowCount;
    viewModel.state.paging.speciesCount = speciesCount;
    viewModel.state.paging.varietiesCount = varietiesCount;

    //alert(varietiesCount);


    $("#" + viewModel.config.paging.scrollbarId).slider("option", "max", viewModel.state.paging.rowCount - 1);
    $("#" + viewModel.config.paging.scrollbarId).slider("option", "value", viewModel.state.paging.rowCount - 1);

    //reset new scroll bar
    viewModel.scrollBar.reset({ "value": 1, "min": 1, "max": viewModel.state.paging.rowCount });




    //viewModel.displayVarietyPage(1);

    //variety_display_species_count
    $("#" + viewModel.config.varietyDisplay.displayVarietiesCountId).text(viewModel.state.paging.varietiesCount)
    //$("#" + viewModel.config.varietyDisplay.displaySpeciesCountId).text(viewModel.state.paging.speciesCount)
    //$("#" + viewModel.config.varietyDisplay.displayRowCountId).text(viewModel.state.paging.rowCount)



};

viewModel.displayVarietyPage = function (rowNum) {
    //alert("displayVarietyPage rowNum: " + rowNum);
    if (viewModel.state.paging.active) { return false; };
    viewModel.state.paging.active.active = true;
    var new_scrollbarContainer = $("#new_paging_scrollbar_container").css("display", "none");
    var scrollbarContainer = $("#" + viewModel.config.paging.scrollbarContainerId).css("display", "none");
    var statusDisplay = $("#" + viewModel.config.paging.statusDisplayId).css("display", "none");


    viewModel.pagingSetRow(rowNum);
    //$("#" + viewModel.config.varietyDisplay.displayCurrentRowId).text(viewModel.state.paging.currentPage)
    $("#variety_display_current_row").text(viewModel.state.paging.currentRow.number);
    viewModel.displayVarietySets();

    statusDisplay.css("display", "block");
    //scrollbarContainer.css("display", "block");
    if (viewModel.state.paging.rowCount != 0) {
        new_scrollbarContainer.css("display", "block");
    };
    viewModel.state.paging.active = false;
    return true;
};


//viewModel.SelectedSupplierCodes = function () {
//    var returnArray = [];
//    var items = $("#" + model.configData.supplierListId + " li.selection_selected");
//    $.each(items, function (i, value) { returnArray.push($(this).data("code")); });
//    return returnArray;
//};
//viewModel.SelectedSpeciesCodes = function () {        //Returns supplier codes that are selected
//    var returnArray = [];
//    var items = $("#" + model.configData.speciesListId + " tr td.selection_selected");
//    $.each(items, function (i, value) { returnArray.push($(this).data("code")); });
//    return returnArray;
//};

viewModel.reloadSpecies = function () {
    viewModel.clearSpeciesList([
        viewModel.clearVarietySetsDisplay,
        viewModel.deleteSpeciesData,
        viewModel.deleteVarietySetsData,
        viewModel.retrieveSpeciesData,
        viewModel.displaySpeciesList,
        viewModel.retrieveVarietySetsData,
        //viewModel.displayVarietySets
    ]);
};
viewModel.reloadVarietySets = function () {
    viewModel.clearVarietySetsDisplay([
        viewModel.deleteVarietySetsData,
        viewModel.retrieveVarietySetsData,
        //viewModel.displayVarietySets
    ]);
};


viewModel.retrieveSupplierData = function (callbacks) {
    //Note: takes variable number of arguments that are handled in ajax.complete event handler
    //alert("retrieving");
    //Make ajax call to retrieve supplier data
    $.ajax({
        datatype: "json",
        url: model.configData.urlGetSupplierData(),
        type: "get",
        beforeSend: function (jqXHR, settings) { },
        success: function (response, textStatus, jqXHR) {
            model.SupplierData = response;
            //alert(JSON.stringify(model.SupplierData));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });
};
viewModel.deleteSupplierData = function (callbacks) {
    model.SupplierData = [];
    viewModel.ProcessEventChain(callbacks);

};
viewModel.displaySupplierList = function (callbacks) {
    //alert("data length:"+model.SupplierData.length);
    var targetList = $("#" + model.configData.supplierListId);
    targetList.empty();

    $.each(model.SupplierData, function (index, value) {
        var classValue = model.url.supplierIsSelected(value.Code) ? "selection_selected" : "selection_available";
        //console.log("code", value.Code, "classValue", classValue);
        var li = $("<li></li>")
        .prop("class", classValue)
        .text(value.Name)
        .data("code", value.Code);
        //alert(value.Code);
        targetList.append(li);
        //$("#" + id).data("code", value.Code);
        //alert($("#" + id).data("code"));
    });
    //setClickFunctions(model.configData.supplierListId);
    viewModel.SetEventsSelectItems(model.configData.supplierListId);
    viewModel.removeLoadingIcon($("#" + model.configData.supplierContainerId));
    viewModel.ProcessEventChain(callbacks);
};
viewModel.clearSupplierList = function (callbacks) {
    $("#" + model.configData.supplierListId).empty();
    viewModel.displayLoadingIcon($("#" + model.configData.supplierContainerId));

    viewModel.ProcessEventChain(callbacks);
};

viewModel.retrieveSpeciesData = function (callbacks) {
    //Make ajax call to retrieve species data
    //alert(model.configData.urlGetSpeciesData());
    $.ajax({
        datatype: "json",
        url: model.configData.urlGetSpeciesData(),
        type: "get",
        beforeSend: function (jqXHR, settings) { },
        success: function (response, textStatus, jqXHR) {
            model.SpeciesData = response;
            //alert(JSON.stringify(model.SpeciesData));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });

};
viewModel.deleteSpeciesData = function (callbacks) {
    model.SpeciesData = [];
    viewModel.ProcessEventChain(callbacks);
};
viewModel.displaySpeciesList = function (callbacks) {
    var targetList = $("#" + model.configData.speciesListId)
    targetList.empty();
    var data = model.SpeciesData;
    $.each(model.SpeciesData, function (index, value) {
        var classValue = model.url.speciesIsSelected(value.Code) ? "selection_selected" : "selection_available";
        var li = $("<li></li>")
            .prop("class", classValue)
            .text(value.Name)
            .data("code", value.Code);
        targetList.append(li);
    });
    viewModel.SetEventsSelectItems(model.configData.speciesListId);
    viewModel.removeLoadingIcon($("#" + model.configData.speciesContainerId));
    viewModel.ProcessEventChain(callbacks);
};
viewModel.clearSpeciesList = function (callbacks) {
    $("#" + model.configData.speciesListId).empty();
    viewModel.displayLoadingIcon($("#" + model.configData.speciesContainerId))
    viewModel.ProcessEventChain(callbacks);
};

viewModel.retrieveVarietySetsData = function (callbacks) {
    //alert("retrieveVarietySetsData");
    //Make ajax call to retrieve species data
    //alert(model.configData.urlGetVarietySetsData());
    $.ajax({
        datatype: "json",
        url: model.configData.urlGetVarietySetsData(),
        type: "get",
        beforeSend: function (jqXHR, settings) { },
        success: function (response, textStatus, jqXHR) {
            model.VarietySetsData = response;

            //model.displayState.Calc();
            //model.displayState.Show();

            viewModel.pagingRefreshState();

            //alert(JSON.stringify(model.VarietySetsData));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });

};

viewModel.displayVarietySets = function (callbacks) {
    var container = $("#" + model.configData.varietySetsDisplayId);
    container.empty();

    var row = 0;
    var varietiesOffset = viewModel.state.paging.currentRow.varietiesOffset;    //offset for initial row
    for (var speciesNdx = viewModel.state.paging.currentRow.speciesOffset; speciesNdx < viewModel.state.paging.speciesCount; speciesNdx++) {
        var heading = $('<h3 />');
        var txt = model.VarietySetsData[speciesNdx].Name;
        if (varietiesOffset > 0) { txt += " - continued"; };
        heading.text(txt);
        container.append(heading);
        for (var varietyNdx = varietiesOffset ; varietyNdx < model.VarietySetsData[speciesNdx].SelectedVarieties.length; varietyNdx += viewModel.config.paging.width) {
            var varietyList = $("<ul class=\"variety_set_list\"></ul>");
            for (var imageOffset = 0; imageOffset < viewModel.config.paging.width && (varietyNdx + imageOffset) < model.VarietySetsData[speciesNdx].SelectedVarieties.length ; imageOffset++) {
                var variety = model.VarietySetsData[speciesNdx].SelectedVarieties[varietyNdx + imageOffset];
                var varietyTile = $("<li></li>");
                varietyTile.prop("class", model.configData.varietyTileClass);
                varietyTile.data("varietyCode", variety.Code);
                var image = $("<img alt=\"\"/>");
                //alert(variety.ImageUrl);
                image.prop("src", variety.ImageUrl);
                var p1 = $("<p></p>");
                var s = variety.Name || "-";
                p1.text(s.trim());
                var p2 = $("<p></p>");
                s = variety.GeneticOwnerName || " ";
                p2.text(s);
                varietyTile.append(image).append(p1).append(p2);
                varietyList.append(varietyTile);
            };
            container.append(varietyList);
            row++;
            if (row >= viewModel.config.paging.height) {
                //alert('row:'+row );
                break;
            };
        };
        varietiesOffset = 0;
        if (row >= viewModel.config.paging.height) {
            //alert('row:'+row );
            break;
        };
    };

    $("." + model.configData.varietyTileClass).on("click", function (event) {
        var varietyCode = $(this).data("varietyCode");
        viewModel.displayVarietyDetailDialog(varietyCode);
    });


    viewModel.removeLoadingIcon($("#right_region_container"));
    viewModel.ProcessEventChain(callbacks);

}

viewModel.deleteVarietySetsData = function (callbacks) {
    model.VarietySetsData = [];
    viewModel.ProcessEventChain(callbacks);
};
viewModel.displayVarietySetsHOLD = function (callbacks) {
    //alert("displayVarietySets");
    //var s = JSON.stringify(model.VarietySetsData);
    //$("#" + model.configData.varietySetsDisplayId).text(s);
    var container = $("#" + model.configData.varietySetsDisplayId);
    container.empty();
    $.each(model.VarietySetsData, function (i, set) {  //foreach variety Set
        //alert(set.Code);
        //Create the Variety Display
        var varietySet = $("<div></div>");
        varietySet.prop("class", model.configData.varietySetClass)
        //create append label:
        var varietySetLabel = $("<h3></h3>");
        varietySetLabel.text(set.Name + ":");
        varietySet.append(varietySetLabel);

        $.each(set.SelectedVarieties, function (ii, variety) {  //foreach variety item
            var varietyTile = $("<div></div>");
            varietyTile.prop("class", model.configData.varietyTileClass)
            var image = $("<img alt=\"\"/>");
            //alert(variety.ImageUrl);
            image.prop("src", variety.ImageUrl);
            var line1 = $("<p></p>");
            line1.text(variety.Name);
            var line2 = $("<p></p>");
            line2.text(variety.GeneticOwnerName);
            varietyTile.append(image).append(line1).append(line2);

            varietyTile.on("click", function (event) {
                var s = model.configData.urlGoToVarietyDetailPage(variety.Code);
                //alert("Development Note:\n\nRedirect to: " + s);
                window.location.href = s;
            });
            varietySet.append(varietyTile);

        });

        container.append(varietySet);
        if (i >= 2) { return false; };
    });
    viewModel.removeLoadingIcon($("#right_region_container"));
    viewModel.ProcessEventChain(callbacks);
};
viewModel.clearVarietySetsDisplay = function (callbacks) {
    //alert("clearVarietySetsDisplay");

    $("#new_paging_scrollbar_container").css("display", "none");
    $("#" + viewModel.config.paging.scrollbarContainerId).css("display", "none");
    $("#" + viewModel.config.paging.scrollbarContainerId).css("display", "none");
    $("#" + model.configData.varietySetsDisplayId).empty();


    viewModel.displayLoadingIcon($("#right_region_container"))
    viewModel.ProcessEventChain(callbacks);
};

viewModel.displayVarietyDetailDialog = function (varietyCode) {

    //var s = model.configData.urlGoToVarietyDetailPage(varietyCode);
    var uri = model.configData.urlGetProductDetailPartialPage(varietyCode);
    //alert("Development Note:\n\nUri: " + uri);
    //$.get(uri, function (data) {
    //    //alert(data);

    //    var iframe = $("<iframe />");
    //    iframe.prop({
    //        "src":uri
    //    });

    //    eps_tools.StandbyDialog({
    //        'title': '<span>Detail View</span>',
    //        'content': iframe,
    //        'modal': true
    //    });
    //});


    //eps_tools.StandbyDialog({
    //    'title': '<span>Product Detail</span>',
    //    'content': '<iframe style = "width:700px; height: 500px;" src="' + uri + '"></iframe>',
    //    'modal': true
    //});
    eps_tools.StandbyDialog({
        'title': '<span>Product Detail</span>',
        'content': '<iframe style = "width:650px; height: 450px; overflow: hidden; background: #fff; " src="' + uri + '"></iframe>',
        'modal': true
    });


};





viewModel.SetEventsToggleAll = function () {
    ////////////////////////////////// Select All /////////////////////////////////////////

    $("#supplier_selection_container .selection_select_all").on("click", function (event) {
        //alert("bingo");
        $("#supplier_selection_list .selection_selected, #supplier_selection_list .selection_available").removeClass("selection_available").addClass("selection_selected");

        //ToDo: only reload Species if suppliers selected < suppliers listed, otherwise, no change
        viewModel.reloadSpecies(); //trigger update of species list

        viewModel.SetEventsSelectItems("supplier_selection_list");
    });

    $("#species_selection_container .selection_select_all").on("click", function (event) {
        //alert("bingo");
        $("#species_selection_list .selection_selected, #species_selection_list .selection_available").removeClass("selection_available").addClass("selection_selected");

        //ToDo: only reload Varieites if species selected < species listed, otherwise no change
        viewModel.reloadVarietySets(); //trigger update of varieties 

        viewModel.SetEventsSelectItems("species_selection_list");
    });
    ////////////////////////////////// Clear All /////////////////////////////////////////
    $("#supplier_selection_container .selection_clear_all").on("click", function (event) {
        //alert("bingo");
        $("#supplier_selection_list .selection_selected, #supplier_selection_list .selection_available").removeClass("selection_selected").addClass("selection_available");
        viewModel.reloadSpecies(); //trigger update of species list
        viewModel.SetEventsSelectItems("supplier_selection_list");
    });
    $("#species_selection_container .selection_clear_all").on("click", function (event) {
        //alert("bingo");
        $("#species_selection_list .selection_selected, #species_selection_list .selection_available").removeClass("selection_selected").addClass("selection_available");
        viewModel.reloadVarietySets(); //trigger update of varieties 
        viewModel.SetEventsSelectItems("species_selection_list");
    });
};
viewModel.SetEventsSelectItems = function (containerId) {
    //alert(containerId + ", " + model.configData.speciesListId);
    containerId = ("" + containerId).trim();  //clear null, trim spaces
    var isSupplierSelection = (containerId == model.configData.supplierListId);
    var isSpeciesSelection = (containerId == model.configData.speciesListId);
    //alert(isSupplierSelection);

    //turn off all click events for selected and unselected elements in this container
    $("#" + containerId + " .selection_selected, #" + containerId + " .selection_available").off("click");

    //if set event for selected elements in this container
    $("#" + containerId + " .selection_selected").on("click", function (event) {
        $(this).removeClass("selection_selected").addClass("selection_available");
        if (isSupplierSelection) {
            viewModel.reloadSpecies();
        } else if (isSpeciesSelection) {
            viewModel.reloadVarietySets();
        };

        viewModel.SetEventsSelectItems(containerId);
    });

    //if event for unselected events in this container
    $("#" + containerId + " .selection_available").on("click", function (event) {
        $(this).removeClass("selection_available").addClass("selection_selected");
        if (isSupplierSelection) {
            viewModel.reloadSpecies();
        } else if (isSpeciesSelection) {
            viewModel.reloadVarietySets();
        };
        viewModel.SetEventsSelectItems(containerId);
    });



}

viewModel.ProcessEventChain = function (callbacks) {

    if (callbacks != undefined && callbacks.length > 0) {
        var callback = callbacks.shift();
        //alert(callback);
        callback(callbacks);
    }
};

viewModel.displayLoadingIcon = function (parentEl) {
    //Note loading icopns have class name and must be located where they won't be deleted, ie above or below element that is populated
    //var icon = $('<img alt="waiting"></img>');
    //icon.prop("src", "/Images/icon_loading_bar_green.gif");
    //element.append(icon);
    $(parentEl).children(".loadingIcons").css("display", "block");
};
viewModel.removeLoadingIcon = function (parentEl) {
    //Note loading icopns have class name and must be located where they won't be deleted, ie above or below element that is populated
    //var icon = $('<img alt="waiting"></img>');
    //icon.prop("src", "/Images/icon_loading_bar_green.gif");
    //element.append(icon);
    $(parentEl).children(".loadingIcons").css("display", "none");
};





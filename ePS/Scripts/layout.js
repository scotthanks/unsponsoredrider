﻿
///////////////////////////////// Bread Crumbs /////////////////////////////////////
var breadCrumbModel = new Object;
//breadCrumbModel.IsInitialized = false;
breadCrumbModel.IsDisplayed = false;
breadCrumbModel.Config = {
    DisplayElement: $("#bread_crumbs"),
    ItemSeparator: "<span>&nbsp;&gt;&nbsp;</span>",
    SeparatorCSS: "transparent url('/Images/icons/bread_crumb_separator_2_16.png') no-repeat 0px -2px",
    ItemClassName: "main_nav_items"
};

breadCrumbModel.data = [];  //Array of objects
breadCrumbModel.ClearDisplay = function () {
    $(breadCrumbModel.Config.DisplayElement).empty();
    breadCrumbModel.IsDisplayed = false;
};

breadCrumbModel.Display = function (data) {
    if (data == null) { return false; };
    if (data.length == 0) { return false; };
    var list = $("<ul style=\"display:inline; list-style: none; list-style-type: none; margin:0; padding:0; \"></ul>");
    $.each(data, function (ndx, item) {
        var listItem = $("<li style=\"display:inline; margin:0; padding:0 5px 0 20px; background:transparent; \"></li>");
        if (ndx > 0) { $(listItem).css("background", breadCrumbModel.Config.SeparatorCSS); }

        if (item.Type.toLowerCase() == "anchor" && item.State.toLowerCase() == "active") {
            var anchor = $("<a></a>");
            //alert('bingo');
            $(anchor).text(item.Text);
            var properties = new Object;
            if (item.id != null && item.id != "") { properties.id = item.id; } // Optionally include the id

            anchor.prop("class", breadCrumbModel.Config.ItemClassName);

            //properties.class = breadCrumbModel.Config.ItemClassName;  //Include the class name
            properties.title = item.Title;  //Include Title Text
            properties.href = item.Href;    //Include href value
            $(anchor).prop(properties);


            $(listItem).append(anchor);
        };

        if (item.Type.toLowerCase() == "span" || item.State.toLowerCase() == "inactive") {
            var span = $("<span></span>");
            //alert('bingo');
            $(span).text(item.Text);
            var properties = { };
            //var properties = { 'class': className, 'id': id, 'title': title };
            if (item.id != null && item.id != "") { properties.id = item.id; }// Optionally include the id

            span.prop("class", breadCrumbModel.Config.ItemClassName);
            //properties.class = breadCrumbModel.Config.ItemClassName;  //Include the class name

            properties.title = item.Title;  //Include Title Text
            $(span).prop(properties);
            $(listItem).append(span);

        };
        $(list).append(listItem);
    });
    $(breadCrumbModel.Config.DisplayElement).append(list);
    breadCrumbModel.IsDisplayed = true;
   
};

//////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////  Selection Menu  /////////////////////////////////////////////


var selectionMenuModel = new Object;
selectionMenuModel.Config = {
    IsComposed: false,
    urlSelectionMenuApi: "/api/SelectionMenu",
    urlAvailabilitySearch: "/Availability/Search",
    urlCatalogSearch: "/Catalog/Search",

};
selectionMenuModel.data = [];
selectionMenuModel.Init = function () {
   // alert("start");
    //selectionMenuModel.LoadThenCompose();
    //PageState2.NavigationLinkAvailaibility.hover(
    //    function () {
    //        PageState2.Init(PageState2.NavigationLinkAvailaibility, selectionMenuModel.Config.urlAvailabilitySearch);
    //        PageState2.CategorySelectionDisplay.css("display", "block");
    //        //PageState2.CategorySelectionDisplay.fadeIn(300);
    //    },
    //    function () {
            
    //        PageState2.CategorySelectionDisplay.css("display", "none");
    //        //PageState2.CategorySelectionDisplay.fadeOut(100);
    //    }
    //);
    //PageState2.NavigationLinkCatalog.hover(
    //    function () {
    //        PageState2.Init(PageState2.NavigationLinkCatalog, selectionMenuModel.Config.urlCatalogSearch);
    //        PageState2.CategorySelectionDisplay.css("display", "block");
    //        //PageState2.CategorySelectionDisplay.fadeIn(300);
    //    },
    //    function () {
    //        PageState2.CategorySelectionDisplay.css("display", "none");
    //        //PageState2.CategorySelectionDisplay.fadeOut(100);
    //    }
    //);
    //PageState2.CategorySelectionDisplay.hover(
    //    function () {
    //        PageState2.CategorySelectionDisplay.css("display", "block");
    //        //$(this).doTimeout('hover', 250, 'addClass', 'hover');

    //    },
    //    function () {
    //        PageState2.CategorySelectionDisplay.css("display", "none");
    //        //$(this).doTimeout('hover', 250, 'removeClass', 'hover');
    //        PageState2.FormSelectionDisplay.hide();

    //    }
    //);
    //PageState2.FormSelectionDisplay.hover(
    //    function () {
    //        PageState2.FormSelectionDisplay.css("display", "block");
    //        PageState2.CategorySelectionDisplay.css("display", "block");
    //        $("#" + $.data(this, 'SelectedCategory', selectedCategoryId)).removeClass("select_category_list").addClass("select_category_list_selected");
    //        //PageState2.SetCategoryFocus();

    //    },
    //    function () {
    //        PageState2.FormSelectionDisplay.css("display", "none");
    //        PageState2.CategorySelectionDisplay.css("display", "none");
    //    }
    //);

};



var loginModel = {};
loginModel.forgottenPassword = function () {
    //close login popup
    logInDialog.dialog("close");
    //get request form
    $.get('/account/ResetPasswordRequestForm', function(form) {
         //show request form
        eps_tools.FormSubmitDialog({
            "title": "Forgotten Password:",
            "content": form,
            "modal": true,
        });
        //popUpLogIn.close();
        $("#reset_password_request_form #email_address_input").focus();
    });

};




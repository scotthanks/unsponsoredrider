﻿
eps_tools = {};

//ToDo: and replace login popup with FormSubmitDialog
//ToDo: Add Icon method for setting icon image

eps_tools.OkDialog = function (settings) {

    //settings values are title, content and modal

    var $action = $('<input id="Button1" type="button" value="OK" />');
    $action.on("click", function () {
        eps_tools.Dialog.close();
    });

    var modal = (function () {
        if (typeof settings.modal === 'undefined' || settings.modal == null || settings.modal == true) {
            return true;
        }
        return false;
    }());

    eps_tools.Dialog.open({
        'title': settings.title || null,
        'content': settings.content || null,
        //'action': typeof settings.onClose === "function" ? settings.onClose : $action,
        'action': $action,
        'modal': modal
    });

};

eps_tools.OkCancelDialog = function (settings) {

    //settings values are title, content, butttonText, locate and modal

    /////////// Handle Buttons //////////////
    var butttonText = {
        okText: "OK",
        cancelText: "Cancel",
    };
    if (typeof settings !== "undefined" && settings != null && typeof settings.buttonText !== "undefined" && settings.buttonText != null) {
        if (typeof settings.buttonText.okText === "string") { butttonText.okText = settings.buttonText.okText; };
        if (typeof settings.buttonText.cancelText === "string") { butttonText.cancelText = settings.buttonText.cancelText; };
    };
    var $actions = $('<span />');
    var $ok = $('<input />')
        .prop({
            "id": "eps_ok_cancel_dialog_button_ok",
            "type": "button",
            "value": butttonText.okText,
        })
        .on("click", function () {
            eps_tools.Dialog.close();
            if (settings.onOk !== 'undefined' && settings.onOk != null) { settings.onOk(); };
        });

    var $cancel = $('<input />')
        .prop({
            "id": "eps_ok_cancel_dialog_button_cancel",
            "type": "button",
            "value": butttonText.cancelText
        })
        .on("click", function () {
            eps_tools.Dialog.close();
            if (settings.onCancel !== 'undefined' && settings.onCancel != null) { settings.onCancel(); };
        });
   $actions.append($cancel, $ok);
    ///////////////////////////////////////////////////////

   var modal = (function () {
        if (typeof settings.modal === 'undefined' || settings.modal == null || settings.modal == true) {
            return true;
        }
        return false;
    }());




    eps_tools.Dialog.open({
        'title': settings.title || null,
        'content': settings.content || null,
        'action': $actions,
        'locate': settings.locate,
        'modal': modal
    });

};

eps_tools.StandbyDialog = function (settings) {

    //settings values are title, content and modal


    //var $action = $('<input id="Button1" type="button" value="OK" />');
    //$action.on("click", function () {
    //    eps_tools.Dialog.close();
    //});

    //var modal = (function () {
    //    if (typeof settings.modal === 'undefined' || settings.modal == null || settings.modal == true) {
    //        return true;
    //    }
    //    return false;
    //}());

    if (typeof settings !== 'undefined' && settings != null && typeof settings.size !== 'undefined' && settings.size != null) {

        

    };




    eps_tools.Dialog.open({
        'title': settings.title || null,
        'content': settings.content || null,
        'action': null,
        'modal': true
    });

};

eps_tools.FormSubmitDialog = function (settings) {
    //alert("FormSubmitDialog");
    //settings values are title, content and modal


    //var $action = $('<input id="Button1" type="button" value="Submit" />');
    //$action.on("click", function () {
    //    eps_tools.Dialog.close();
    //});

    var modal = (function () {
        if (typeof settings.modal === 'undefined' || settings.modal == null || settings.modal == true) {
            return true;
        }
        return false;
    }());

    eps_tools.Dialog.open({
        'title': settings.title || null,
        'content': settings.content || null,
        'action': null,     //clear action from prior uses of dialog
        "size": settings.size || null,
        "locate": settings.locate || null,
        'modal': modal
    });

};

eps_tools.HoverMessage = function (element) {
    if (typeof element === "undefined" || element == null) { return false; };  //must have an element to hover around
    var method = {};
    var id = element.data("hoverMessageContainerId") || "";
    if (id == "") {
        //this element does not have a hover message so wireup one
        method.id = eps_createNewGuid();
        $container = $("<div />").prop({ "id": method.id, "class": "eps_hover_message_containers" });
        var el = $(element) || null; //begin with the element itself
        if (el != null) {
            el.data({ "hoverMessageContainerId": method.id });  //add the element to this
        };
        $("body").append($container);
    } else {
        method.id = id;
    };
    method.element = element;
    //method.id = (id == "") ? eps_createNewGuid() : id;
    

    method.name = "Fred Flintsone";
    method.test = function () {
        alert(this.name);
    };
    method.load = function (content) {
        //alert(this.id);
        //var $container = $("#" + this.id).empty();      //empty incase this is a reload
        var $display = $("<div />").prop({ "class": "eps_hover_message_displays" });
        var $contents = $("<div />").prop({ "class": "eps_hover_message_contents" });
        $contents.html(content);
        $display.append($contents);
        $container.append($display);
        return method;
    }
    method.display = function (position)
    {
        //alert("position: " + position + "\nid: " + this.id);

        var x = 0, y = 0;
        switch (position)
        {
           
            case 4: //hover message goes immediately to left of element
                //var position = element.offset();
                //var top = Math.floor(position.top + ((element.height() - 35) / 2));
                //var left = Math.floor(position.left + element.width() + 30);
                var pos = this.element.offset();

                x = Math.floor(pos.left - $container.width() - 10);

                y = Math.floor(pos.top + ((this.element.height() - 35) / 2));

                //y += 100;
                break;
            default:
    //alert("here");
                //center horizontally in viewport
                x = Math.max($(window).width() - $container.outerWidth(), 0) / 2;
                x += $(window).scrollLeft();
                // Center vertically in the viewport
                y = $(window).height();
                y = y - $container.outerHeight();
                y = Math.max(y / 2, 0);
                y += $(window).scrollTop();
        }
        
        $container.css({
            "left": x,
            "top": y,
        });
        $("#" + this.id).fadeIn("fast");


        //if (typeof offsetX === "undefined" || typeof offsetX !== "number" || offsetX == null || offsetX == 0) {
        //} else if (offsetX < 0) {

        //} else if (offsetX > 0) {

        //};
        //if (typeof offsetY === "undefined" || typeof offsetY !== "number" || offsetY == null || offsetY == 0) {
        //} else if (offsetY < 0) {

        //} else if (offsetY > 0) {

        //};
        //alert(x + ", " + y);

    };

    method.remove = function () {
        $("#" + this.id).remove();
        $(this.element).data({ "hoverMessageContainerId": "" });
    };

    return method;

};

eps_tools.floatMessage = function (element, content, interval) {
    //floats message to right of element
    //if no element then does nothing
    if (typeof element === 'undefined') { return false; };
    var top; var left;
    if (element == null) {
        //set values null
        top = null;
        left = null;
        //alert("element == null");
    }
    else if (element.length > 0) {
        var position = element.offset();
        //ToDo: code to center floating message vertically with element
        //assume floating message is 35 px tall
        top = Math.floor(position.top + ((element.height() - 35) / 2));
        left = Math.floor(position.left + element.width() + 30);
    };
    eps_tools.FloatingMessage(left, top, content, interval);
};
eps_tools.FloatingMessage = function (x, y, content, interval) {
    var $container = $("<div class=\"eps_floating_message_containers\" />");
    var $display = $("<div class=\"eps_floating_message_displays\" />");
    var $contents = $("<div class=\"eps_floating_message_contents\" />");

    $contents.html(content);
    $display.append($contents);
    $container.append($display);
    $("body").append($container);


    if (x == null) {
        var x = Math.max($(window).width() - $container.outerWidth(), 0) / 2;
        x += $(window).scrollLeft();
    };
    if (y == null) {
        // Center the modal in the viewport
        y = $(window).height();
        y = y - $container.outerHeight();
        y = Math.max(y / 2, 0);
        y += $(window).scrollTop();
    };
    //alert(x + ", " + y);

    $container.css({
        "left": x,
        "top": y,
    });
    $container.fadeIn("fast").delay(interval).fadeOut(1000).remove("slow");
};

eps_tools.Dialog = (function () {


    var method = {},
        overlayId = 'eps_dialog_overlay',
        containerId = "eps_dialog_container",
        displayId = "eps_dialog_display",
        titleId = "eps_dialog_title",
        contentId = "eps_dialog_content",
        actionId = "eps_dialog_action",
        closeId = "eps_dialog_close";

        var $overlay = $('<div />').prop({ "id": overlayId });
        var $container = $('<div />').prop({ "id": containerId });
        var $display = $('<div />').prop({ "id": displayId });
        var $title = $('<div />').prop({ "id": titleId });
        var $content = $('<div />').prop({ "id": contentId });
        var $action = $('<div />').prop({ "id": actionId });
        var $close = $('<div />').prop({ "id": closeId });

        $container.on("keydown", function (e) {
            e.stopPropagation();
            var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
            //ToDo: split enter and escape, enter should trigger action, escape trigger close;
            if (key == 13 || key == 27) {
                e.preventDefault();
                method.close();
            }
        });

        $close.on("click", function () {
            method.close();
        });

        $display.append($title, $content, $action);
        $container.append($display, $close);

        $(document).ready(function () {
            $('body').append($overlay, $container);
        });

        //alert("bingo " + ($("#eps_dialog_container").length || -1));


        method.open = function (settings) {

            var modal = (function () {
                if (typeof settings.modal === 'undefined' || settings.modal == null || settings.modal == true) {
                    return true;
                }
                return false;
            }());

            if (modal) {
                //$overlay.css({"width":"100%", "height":"100%"});
                $overlay.fadeIn("slow");
                $overlay.on("click", function () {
                    method.close();
                });
            };
            $title.html(settings.title);
            $content.html(settings.content);
            $action.html(settings.action);

            method.size(settings.size);

            method.locate(settings.locate);
            $container.fadeIn("slow").focus();
        };

        method.close = function () {
            $overlay.fadeOut(500);
            $container.fadeOut(500, function () {
                $title.empty();
                $content.empty();
                $action.empty();
            });
        };

    // Set the size of the modal dialog
        method.size = function (size) {
            if (typeof size === 'undefined' || size == null) { return false; };

            //if (typeof size.width === 'undefined' || typeof size.height === 'undefined') { return false; };
            //if (typeof size.width !== 'number' || typeof size.height !== 'number') { return false; };
            //if (size.width < 50) { size.width = 50 };
            //if (size.height < 30) { size.width = 30 };

            $content.css({
                "width": size.width || 50,
                "height": size.height || 30
            });
            return true;
        };
    
        method.locate = function (locate) {
            var top, left;

            // Center the modal in the viewport
            top = Math.max($(window).height() - $container.outerHeight(), 0) / 2;
            //top += $(window).scrollTop();
            left = Math.max($(window).width() - $container.outerWidth(), 0) / 2;
            //left += $(window).scrollLeft();

            //set location if settings provided
            if (typeof locate !== 'undefined' && locate != null) {
                if (typeof locate.top !== 'undefined' && locate.top != null) { top = locate.top; };
                if (typeof locate.left !== 'undefined' && locate.left != null) { left = locate.left; };
            };

            //set css to location
            $container.css({
                top: top + $(window).scrollTop(),
                left: left + $(window).scrollLeft()
            });
        };

    return method;


}());



var reportRunStart = function () {
    // alert("in reportRunStart");
    //$('#reset').prop('disabled', true);
    //$('#submit').prop('disabled', true);
    //$('.run-report').addClass('collapse');
    //$('.please-wait').removeClass('collapse');
   // alert("hi start");
    $("#RunReport").text("Please Wait...");
};

var reportRunEnd = function () {
    // alert("in reportRunEnd");
    //$('#reset').prop('disabled', false);
    //$('#submit').prop('disabled', false);
    //$('.run-report').removeClass('collapse');
    //$('.please-wait').addClass('collapse');
    //$('#criteria').collapse('hide');
 //   alert("hi end");
    var xFrameId = $('#xViewer > iframe:eq(0)').attr('id');
    var xFrame = $('#' + xFrameId);
    var xFrameHeight = xFrame.contents().height();
    // alert(xFrameHeight);
    document.getElementById(xFrameId).style.height = xFrameHeight + 50 + "px";
    var xFrameWidth = xFrame.contents().width();
    // var xFrameWidth = "2064px"

    document.getElementById(xFrameId).style.width = xFrameWidth + 50 + "px";
    //  alert(document.getElementById(xFrameId).style.width);
    console.log(xFrameId + " " + xFrameHeight);
    console.log(xFrameId + " " + xFrameWidth);
    // $('#showHideCriteria').scrollTop(0);
    //$('html, body').animate({ scrollTop: $('#showHideCriteria').offset().top }, "fast");
    $("#RunReport").text("Run Report");

};
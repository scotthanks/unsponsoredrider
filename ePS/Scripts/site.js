﻿

$(document).ajaxError(function (event, jqxhr, settings, exception) {
    var msg = "Verb: " + settings.type + "\nURL: " + settings.url + "\nStatus: " + jqxhr.status + "\nError: " + exception;
    //alert(msg);

    //if (jqXHR.status === 0) {
    //    alert('Not connect.\n Verify Network.');
    //} else if (jqXHR.status == 404) {
    //    alert('Requested page not found. [404]');
    //} else if (jqXHR.status == 500) {
    //    alert('Internal Server Error [500].');
    //} else if (exception === 'parsererror') {
    //    alert('Requested JSON parse failed.');
    //} else if (exception === 'timeout') {
    //    alert('Time out error.');
    //} else if (exception === 'abort') {
    //    alert('Ajax request aborted.');
    //} else {
    //    alert('Uncaught Error.\n' + jqXHR.responseText);
    //}

    console.log(msg);

});

var model2 = {};
model2.ProcessEventChain = function (callbacks) {
    if (callbacks != undefined && callbacks.length > 0) {
        var callback = callbacks.shift();
        //alert(callback);
        callback(callbacks);
    }
};

//Generic Ajax Call - overides global error handler
model2.ajaxCall2 = function (element, uri, httpVerb, data, onSuccess, callbacks) {
    //alert("ajaxCall");
    $.ajax({
        url: uri,
        dataType: "json",
        data: data,
        type: httpVerb,
    })
    .done(function (data, textStatus, jqXHR) {
       //  alert(traverseObj(data, false));
        // alert(textStatus);
       // if (data.Success == true)
       // {
            onSuccess(data);
      //  }
        //else
        //{
        //    viewModel.floatMessage(element, "Error: " + data.Message, 2500);
        //    onSuccess(data);
        //}
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
      //  alert("model 2 error: " + textStatus + errorThrown);
        var errorMsg = "TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown;
        //if (typeof element !== "undefined" & element != null) {
        //    viewModel.floatMessage(element, errorMsg, 4000);
        //};
    })
    .always(function (jqXHR, textStatus) {
        //if (textStatus == "success") {
        //   // alert("hi there");
        model2.ProcessEventChain(callbacks);
        //} else {
        //};
    });
};

if (!String.prototype.trim) {
    //code for trim
    String.prototype.trim = function () {
        if (typeof this === 'undefined' || this == null) { return ''};
        return this.replace(/^\s+|\s+$/g, '');
    };

    //String.prototype.ltrim = function () { return this.replace(/^\s+/, ''); };

    //String.prototype.rtrim = function () { return this.replace(/\s+$/, ''); };

    //String.prototype.fulltrim = function () { return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g, '').replace(/\s+/g, ' '); };
}

/////////////////////// Helper Functions /////////////////////////////////////

//Create new guid
function eps_createNewGuid() {

 return   'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });

}


//Read values from Query String

function eps_GetQueryStringValue(key) {
    //Note: this is case sensitive
    //key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    //var regex = new RegExp("[\\?&]" + key + "=([^&#]*)"), results = regex.exec(document.location.search);
    //return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));

    //Note: this returns an array, so if multiple instances are sent - as in an array - they will all be returned
    //Note: this is not case sensitive
    var re = new RegExp('(?:\\?|&)' + key + '=(.*?)(?=&|$)', 'gi');
    var r = [], m;
    while ((m = re.exec(document.location.search)) != null) {
        //alert(m[0] + " | " + m[1]);
        r.push(m[1]);
    };

    //alert(r.length);
    //r = r.length == 0 ? "" : r[0];

    r = r.length == 0 ? "" : String(r[0]);
    //alert("|" + r + "|" + typeof r);
    return r;



};


// deprecated way to read querystring, use one above
function getQueryStringValue(key) {
    var re = new RegExp('(?:\\?|&)' + key + '=(.*?)(?=&|$)', 'gi');
    var r = [], m;
    while ((m = re.exec(document.location.search)) != null) r.push(m[1]);
    return r;
};

var traverseObj = function (obj, useHtml) {
    var returnStr = "<br />";
    if (typeof useHtml === 'undefined' || useHtml) {
        //alert("bingo!");
        //Leave returnStr as is
    } else if (!useHtml) {
        returnStr = "\n";
    };
    if (obj == null) return "null";
    var s = "";
    var offset = 0;

    var traverse = function (o) {
        if (o == 'null') { alert('[[null error traversing object ]]'); } else {
            //s += "..<br />";
            offset += 3;
            $.each(o, function (key, item) {
                if (item == null) {
                    for (var i = 0; i < offset; i++) { s += "." };
                    s += key + ": " + 'null';
                    s += returnStr;
                } else {

                    var type = typeof item;
                    //s += key + ": " + type;;
                    if (type === "object") {
                        //alert(key + ": " + type);
                        if (typeof key === 'string') {
                            for (var i = 0; i < offset; i++) { s += "." };
                            s += key + ":" + returnStr;
                        };
                        //alert(key + ": " + o);
                        traverse(item);
                    } else {
                        for (var i = 0; i < offset; i++) { s += "." };
                        s += key + ": " + item;
                        s += returnStr;
                    };

                };



            });
            offset -= 3;
        };
    };

    traverse(obj);
    return s;

};

function eps_writeCookie(inputs) {
    //inputs: duration(minutes), path, name and value
    //note: pass null or empty string for path to create local cookie
    if (typeof inputs.duration === 'number') {
        //alert(traverseObj(inputs, false));
        var date = new Date();
        date.setTime(date.getTime() + (inputs.duration * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else { var expires = ""; }
    if (typeof inputs.path === 'string' && inputs.path != '') {
        var path = "; path=" + inputs.path;
    } else {
        var path = "";
    };
    document.cookie = escape(inputs.name) + "=" + escape(inputs.value) + expires + path;
}
function eps_readCookie(name) {
    var nameEQ = escape(name) + "=";

  //  alert(document.cookie.toString());

    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return unescape(c.substring(nameEQ.length, c.length));
    }
    return null;
}
function eps_removeCookie(name) {
    //Important: returns bool indicating whether cookie is gone
    //alert(escape(name));
    document.cookie = escape(name) + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    //cookies created at session level can not be deleted.
    //if (readCookie(name) == null) { return true; } else { return false; };

}
function getSellerList(email,callbacks) {
  //  alert("here 1");
    var duration = 36 * 60;
  
    var data = { email: email };
    //var data = { };
    //alert("here 3");

    var onSuccess = function (data) {
        if (data == null)
        {
            alert("error");

        }
        else
        {
          //  alert("in success");
            //alert(traverseObj(data));
            var x = data.Sellers;
            var sellerList = JSON.stringify(x)
           // alert(cookieData);
            localStorage.setItem("SellerList", sellerList);
           

            $.each(x, function (i, item) 
            {
                //   alert(data.length);
                if (x.length == 1 && i==0) {
                    sellerCode = item.Code;
                    sellerCode = "GFB";
                    localStorage.setItem("SellerCode", sellerCode);              
                }
            })
        }
    };

    var uri = "/api/Grower/GetSellers";
    var element = $("#seller_select_div");



    model2.ajaxCall2(element, uri, "get", data, onSuccess, callbacks);


}

function getMenuUL(email,callbacks) {
    var sellerCode = localStorage.getItem("SellerCode");
    var isBeta = localStorage.getItem("Beta2");
   // alert(isBeta);
   // if (sellerCode == null)
   // {
        sellerCode = "GFB";
   // }
  //  alert("getting Menu for: " + email + " and sellerCode: " + sellerCode);
    var data = {
        email: email,
        sellerCode: sellerCode,
        isBeta: isBeta
};
    
   
    var onSuccess = function (data) {
        if (data == null) {
            alert("error");
        }
        else {
            var x = data.MenuUL;
            var menuData = JSON.stringify(x);
            localStorage.setItem("MenuUL", menuData);   
        }
        

    };

    var uri = "/api/Grower/GetMenuUL";
    var element = $("#menu");



    model2.ajaxCall2(element, uri, "get", data, onSuccess, callbacks);


}
function setMenuUL()
{
   // alert("setMenuUL");
    var auth = getAuthentication();
 
    if (auth.LoggedIn)
    { 
        var x = localStorage.getItem("MenuUL");
    //    alert(x);
        var data = JSON.parse(x);
      //  alert(data);
        if (data != null)
        {
            $("#menu").html(data);
        }
    }
   
}


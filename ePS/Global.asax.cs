﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using DataServiceLibrary;
using BusinessObjectServices;
using WebMatrix.WebData;
using System.Web.SessionState;
using System.Configuration;
using ePS.Models;

namespace ePS
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

   
    public class MvcApplication : System.Web.HttpApplication
    {
        const string SUB_DOMAIN_PRODUCTION = "www";
        const string SUB_DOMAIN_DEMO = "demo";
        const string SUB_DOMAIN_DEVELOPMENT = "dev";

        const string USER_ACTION_LOG_TABLE_NAME_WWW = "trafficlog002";
        const string USER_ACTION_LOG_TABLE_NAME_DEMO = "trafficlogdemo002";
        const string USER_ACTION_LOG_TABLE_NAME_DEV = "trafficlogdev002";

       
        protected void Application_PostAuthorizeRequest()
        {
            if (HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.StartsWith("~/api"))
            {
                HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
            }
        }
        private void _requireAccessCode() {
                //Require Access Code
                const string ACCESS_CODE_VALUE = "369";
                const string ACCESS_CODE_KEY = "AccessCode";
                const string ACCESS_COOKIE_NAME = "AccessCookie";
                const double ACCESS_COOKIE_DURATION_HOURS = 72d;
                
                if (true)   //used to switch this on and off during testing
                {
                    //if (!Request.RawUrl.ToLower().Contains(HEALTH_TEST_FILE_NAME.ToLower()))
                    //{
                    //    var s = "bingo";
                        
                    //}   
                        //Init HttpService
                        var httpService = new HttpService();

                        //if user is authenticated, they must have access
                        if (Request.IsAuthenticated)
                        {
                            //get or create cookie 
                            var accessCookie = httpService.ReturnCookie(Request, ACCESS_COOKIE_NAME);
                            if (accessCookie == null)
                            {
                                accessCookie = new HttpCookie(ACCESS_COOKIE_NAME, ACCESS_CODE_VALUE);
                                accessCookie.Path = "/";
                            };
                            //extend cookie life
                            accessCookie.Expires = DateTime.Now.AddHours(ACCESS_COOKIE_DURATION_HOURS);
                            httpService.AddCookie(Response, accessCookie);
                        }
                        else
                        {
                            //read accessCode if any supplied
                            var accessCode = Request.QueryString[ACCESS_CODE_KEY];
                            //Get Access Cookie
                            var accessCookie = httpService.ReturnCookie(Request, ACCESS_COOKIE_NAME);
                            //Check for valid Access Cookie
                            if (accessCookie != null && accessCookie.Value == ACCESS_CODE_VALUE)
                            {
                                //extend expiration of access cookie
                                accessCookie.Expires = DateTime.Now.AddHours(ACCESS_COOKIE_DURATION_HOURS);
                                httpService.AddCookie(Response, accessCookie);
                                //var path = string.Format(@"{0}?returnUrl={1}", SITE_AUTHENTICATION_PATH, Request.Url.PathAndQuery);
                                //Response.Redirect(Request.Url.AbsoluteUri);
                            }
                            //check for Valid Access Code
                            else if (accessCode != null && accessCode != string.Empty && accessCode == ACCESS_CODE_VALUE)
                            {
                                //create Access Cookie
                                accessCookie = new HttpCookie(ACCESS_COOKIE_NAME, ACCESS_CODE_VALUE);
                                httpService.AddCookie(Response, accessCookie);
                                accessCookie.Expires = DateTime.Now.AddHours(ACCESS_COOKIE_DURATION_HOURS);
                                accessCookie.Path = "/";
                                var path = Request.Url.AbsoluteUri;             //.Substring(0,Request.Url.PathAndQuery.LastIndexOf("/"));
                                var i = path.LastIndexOf("?");
                                if (i > 0) { path = path.Substring(0, i); };
                                Response.Redirect(path);

                            }
                            else
                            {
                                //If no Access Cookie and No Access Code return Error
                                Request.RequestContext.HttpContext.Response.StatusCode = 401;
                            };
                        
                    };
                }


        }

        private void _logUserAction(string tableName)
        {
            //Log User to Table Storage
            try
            {
                if(Request.RawUrl != "/test/TestMe" && Request.RawUrl.Substring(0,9) != "/bundles/")
                {
                    var service = new LogUserAction();
                    service.WriteToTable(Request, Response, tableName);
                }
               
            }
            catch { }
            //ToDo: catch should log error
        }

        protected void Application_Start()
        {
            //set Application Variable SubDomain from setting in web.config
            Application["SubDomain"] = ConfigurationManager.AppSettings["SubDomain"].ToLower();
            
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            EventLogTableService.SetLoggingUtilityDelegate();
            WebSecurity.InitializeDatabaseConnection("MembershipDb", "UserProfile", "UserId", "UserName", autoCreateTables: false);

            LookupTableService.Initialize();
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

        }

        protected void Application_AuthenticateRequest() {
            
            var subDomain = (Application["SubDomain"] != null) ? (string)Application["SubDomain"] : string.Empty;

            var tableName = subDomain == SUB_DOMAIN_PRODUCTION ? USER_ACTION_LOG_TABLE_NAME_WWW : subDomain == SUB_DOMAIN_DEMO ? USER_ACTION_LOG_TABLE_NAME_DEMO : USER_ACTION_LOG_TABLE_NAME_DEV;
            _logUserAction(tableName);

            //if (subDomain == SUB_DOMAIN_PRODUCTION) {
            //} else if (subDomain == SUB_DOMAIN_DEMO) {
            //} else if (subDomain == SUB_DOMAIN_DEVELOPMENT) {
            //} else {
            //};
        
        }

        protected void Application_BeginRequest()
        {
            var subDomain = (Application["SubDomain"] != null) ? (string)Application["SubDomain"] : string.Empty;

            if (subDomain == SUB_DOMAIN_PRODUCTION) {
            } else {
                _requireAccessCode();
            };

            //Init HttpService
            var httpService = new HttpService();
            var sessionCookie = httpService.ReturnOrCreateSessionCookie(Request);
            httpService.AddCookie(Response, sessionCookie);

            // GmB Disable caching
            //HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            //HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
            //HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            //HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //HttpContext.Current.Response.Cache.SetNoStore();

        }
    }
}
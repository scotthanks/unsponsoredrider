﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Configuration;
using ePS.Filters;

using BusinessObjectsLibrary;
using BusinessObjectServices;

using ePS.Models;

namespace ePS.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        const string SUB_DOMAIN_DEVELOPMENT = "dev";

        public ActionResult EarlyIE()
        {
            return View();
        }


//        public ActionResult TestPage()
//        {
//            ViewBag.Title = "Test Page";
//            ViewBag.Message = "Test Page is only for development.";
//            ViewBag.SessionId = Session.SessionID;
//            ViewBag.IsNewSession = Session.IsNewSession;
//            ViewBag.ToString = Session.ToString();
            
//            //HttpWebRequest myReq = (HttpWebRequest)Request;

//            //var v = ViewState["arrayListInViewState"];

//            var request = Request;
//            var form = request.Form;

//            var v = Request.Form["state_guid"] ?? "n/a"; ;

//            @ViewBag.StateGuid = string.Format("value: {0}", v);




//            //Reading Cookies
//            List<string> cookieList = new List<string>();
//            HttpCookieCollection requestCookiesCollection = Request.Cookies;
            
//            foreach (string cookieName in requestCookiesCollection.AllKeys) {
//                var cookie = requestCookiesCollection[cookieName];
//                //cookieString += "<br />";
//                cookieList.Add(string.Format(" {0}: ", cookieName));
//                if( cookie.Values.Count > 0){
//                    string[] valueKeys = cookie.Values.AllKeys;
//                    foreach(string key in valueKeys){
//                        var cookieKeyValuePair = cookie.Values[key];
//                        cookieList.Add(string.Format(" [ {0} ]-[ {1} ]", key, cookieKeyValuePair));
//                    }
//                }
//            }
            
//            ViewBag.CookieDescriptionsArray = cookieList.ToArray();

//            //Writing Cookies
////ToDo: Rewrite to test for StateGuid Cookie and only extend when it already exists
//            HttpCookie stateCookie = new HttpCookie("SessionState");
//            stateCookie.Path = "/";
//            stateCookie.Expires = DateTime.Now.AddSeconds(12d);
//            stateCookie["Guid"] = Guid.NewGuid().ToString();
//            stateCookie["PlantCategoryKey"] = "annuals";
//            stateCookie["PlantFormKey"] = "unrooted";
//            Response.Cookies.Add(stateCookie);

//            //HttpCookie tempCookie = new HttpCookie("TempCookie");
//            //tempCookie.Path = "/";
//            //tempCookie.Expires = DateTime.Now.AddDays(365d);
//            //tempCookie["Value_1"] = "TempCookieValue";
//            //tempCookie["Value_2"] = "TempCookieValue";
//            //Response.Cookies.Add(tempCookie);
            

//            //Killing Cookies
            
//            //Response.Cookies.Add(new HttpCookie("ACookieToKill", "SomeValueHere"));
            
//            //string nameOfCookieToKill = "ACookieToKill";
//            //HttpCookie cookieToKill = Request.Cookies[nameOfCookieToKill] ?? null;
//            //if (cookieToKill != null)
//            //{
//            //    cookieToKill.Expires = DateTime.Now.AddDays(-1d);
//            //    Response.Cookies.Add(cookieToKill);
//            //}



//            //Changing Existing Cookie's Values
//            string nameOfCookieToChange = "TempCookie";
//            HttpCookie cookieToChange = Request.Cookies[nameOfCookieToChange] ?? null;
//            if (cookieToChange != null)
//            {
//                cookieToChange.Expires = DateTime.Now.AddDays(1d);
//                cookieToChange["Value_1"] = "ANewValueNow";
//                Response.Cookies.Add(cookieToChange);
//            }



//            //int loop1, loop2;
//            //HttpCookieCollection MyCookieColl;
//            //HttpCookie MyCookie;

//            //MyCookieColl = Request.Cookies;

//            //// Capture all cookie names into a string array.
//            //String[] arr1 = MyCookieColl.AllKeys;

//            //// Grab individual cookie objects by cookie name.
//            //for (loop1 = 0; loop1 < arr1.Length; loop1++)
//            //{
//            //    MyCookie = MyCookieColl[arr1[loop1]];
//            //    Response.Write("Cookie: " + MyCookie.Name + "<br>");
//            //    Response.Write("Secure:" + MyCookie.Secure + "<br>");

//            //    //Grab all values for single cookie into an object array.
//            //    String[] arr2 = MyCookie.Values.AllKeys;

//            //    //Loop through cookie Value collection and print all values.
//            //    for (loop2 = 0; loop2 < arr2.Length; loop2++)
//            //    {
//            //        Response.Write("Value" + loop2 + ": " + Server.HtmlEncode(arr2[loop2]) + "<br>");
//            //    }
//            //}







//            return View();
//        }

        public ActionResult Index()
        {
            ViewBag.HeaderDisplay = "none";

          
            return View();
        }


        //[Authorize]
        //public ActionResult ProductDetail(string id, string plantCategoryCode, string productFormCode, string supplierCodes, string varietyCode, string shipWeek)
        //{
        //    //Note: shipWeek by convention is ##|#### = 30|2013
            
            
        //    //string productCategoryCode = id ?? string.Empty;
        //    //Note: id is ProductCategory
        //    //ToDo: This may not be the right call - breeder vs supplier
        //    //ToDo: currently defaulting to these values
        //    //productCategoryCode = "VA";
        //    //productFormCode = "RC";
        //    //suppier = "Breeder";
        //    //species = "Calibrachoa";
        //    /////////////////////////////////////////

        //    //Note productForm matches the submitted data and must be kept the same, it contains the Product Form Code per Database
        //    //id = id ?? string.Empty;
        //    ViewBag.Id = id;
        //    ViewBag.Title = "Product Detail Page";
        //    ViewBag.Message = "ProductDetail Application";

        //    var model = new ProductDetailViewModel(plantCategoryCode, productFormCode, supplierCodes, varietyCode, shipWeek);

        //    return View("ProductDetailView", model);
        //}

        public ActionResult Catalogue(string id, string productForm)
        {
            //If id and productForm are defined then go to CatalogueSearch_2, else CatalogueSearch_1
            id = id ?? string.Empty;

            if (productForm == null)
            { //This is Catalogue Level 1
                ViewBag.Id = id;
                ViewBag.Title = "Catalogue Page 1";
                ViewBag.Message = "Catalogue Application Page";
                var model = new CatalogueSearch_1PageModel(id);
                return View("CatalogueSearch_1", model);
            }
            else
            { //This is Catalogue Level 2
                var model = new CatalogueSearch_2PageModel(id, productForm);

                ViewBag.Title = "Catalogue Page 2";
                ViewBag.Message = "Catalogue Application Page";
                return View("CatalogueSearch_2", model);
            }
        }
        
        [RequireHttps]
        [Authorize]
        public ActionResult Cart()
        {
            ViewBag.Title = "Shopping Cart";
            ViewBag.Message = "Your shopping cart";
            ViewBag.ShowTestInfo = (SUB_DOMAIN_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());
            return View();
        }


        public ActionResult About()
        {

            return View();


        }

    }
}

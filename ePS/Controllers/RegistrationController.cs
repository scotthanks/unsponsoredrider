﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ePS.Controllers
{
    public class RegistrationController : Controller
    {
        const string SUB_DOMAIN_DEVELOPMENT = "dev";

        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Account()
        {
            ViewBag.ShowTestInfo = (SUB_DOMAIN_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());

            return View();
        }

    }
}

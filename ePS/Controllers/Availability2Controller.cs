﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using BusinessObjectsLibrary;
using BusinessObjectServices;
using System.Web.UI.WebControls;
using ePS.Models;

 
namespace ePS.Controllers
{
    //[RequireHttps]
    public class Availability2Controller : Controller
    {
        const string SUB_DOMAIN_DEVELOPMENT = "dev";
        private string reportServer = "";
        private Uri reportServerURI;
        private string SSRSUserName = "";
        private string SSRSPassword = "";
        private string SSRSDomain = "";
        private string reportPath = "";

        private bool SetSSRSParamters()
        {
            var appSettingsReader = new System.Configuration.AppSettingsReader();
            reportServer = (string)appSettingsReader.GetValue("SSRSReportServer", typeof(string));
            reportServerURI = new Uri(reportServer);
            reportPath = (string)appSettingsReader.GetValue("SSRSReportPath", typeof(string));
            SSRSUserName = (string)appSettingsReader.GetValue("SSRSUserName", typeof(string));
            SSRSPassword = (string)appSettingsReader.GetValue("SSRSPassword", typeof(string));
            SSRSDomain = (string)appSettingsReader.GetValue("SSRSDomain", typeof(string));
            return true;
        }
        public ActionResult Index()
        {
            var service = new ContentService();
            var content = service.GetStaticContentType("/Availability2/");
            ViewBag.ContentType = content;
            return View();
        }
        private ReportViewer reportViewer = new ReportViewer()
        {
            ProcessingMode = ProcessingMode.Remote,
            SizeToReportContent = true,
            Width = Unit.Pixel(1094),
            Height = Unit.Pixel(300),
            AsyncRendering = false,
            ShowParameterPrompts = false,
            ZoomMode = ZoomMode.FullPage,
            BackColor = System.Drawing.Color.White
        };
        public ActionResult CurrentAvailability()
        {
           
            return View();
        }
        public ActionResult CurrentAvailability2()
        {

            return View();
        }


        [HttpPost]
        [ActionName("RunCurrentAvailabilityReport")]
        public PartialViewResult RunCurrentAvailabilityReport(
           string cboSupplier,
           string cboProductForm
          )
        {
            string reportName = "";
            reportName = "AvailabilityGFPublic";
            
           
            bool bRet = SetSSRSParamters();

            //Set up Viewer
            reportViewer.ServerReport.ReportPath = reportPath + reportName;
            reportViewer.ServerReport.ReportServerUrl = reportServerURI;

            //Set up Credentials
            IReportServerCredentials irsc = new CustomReportCredentials(SSRSUserName, SSRSPassword, SSRSDomain);
            reportViewer.ServerReport.ReportServerCredentials = irsc;

            //Set up Parameters
            List<ReportParameter> reportParameters = new List<ReportParameter>();

            reportParameters.Add(new ReportParameter("pSupplierGuidList", cboSupplier));
            reportParameters.Add(new ReportParameter("pProductFormCategoryGuidList", cboProductForm));
            reportViewer.ServerReport.SetParameters(reportParameters);

            //Run the report
            reportViewer.ServerReport.Refresh();

            //Return Viewer
            ViewBag.ReportViewer = reportViewer;
            ViewBag.ReportToShow = true;
            return PartialView("~/Views/Shared/_ReportViewer.cshtml");
        }


        //[Authorize]
        public ActionResult Search(string category, string form)
        {
            //browser sniffing stinks, but we do it
            var browserName = Request.Browser.Browser;
            var browserVersion = Request.Browser.MajorVersion;

            if (browserName == "IE" && browserVersion <= 8) {
                return RedirectToAction("EarlyIE", "Home");
            };

            //Logic of State Management:
            // 1. If valid Category/Form specified in Query String, proceed
            // 2. ELseIf Category or Form is invalid, redirect using default values

            const string DEFAULT_CATEGORY_CODE = "VA";  //ToDo: Move this to configuration
            const string DEFAULT_FORM_CODE = "RC";      //ToDo: Move this to configuration

            category = category ?? string.Empty;
            form = form ?? string.Empty;
            //Validate Category Code and Form Code

            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var service = new AvailabilityService(status);
            string productFormName = "";
            string categoryName = "";

            if (!service.IsValidCategoryCode(category, out categoryName) || !service.IsValidFormCode(form, out productFormName))
            {
                //Use Default Values
                category = DEFAULT_CATEGORY_CODE;
                form = DEFAULT_FORM_CODE;
                return RedirectToAction("Search", "Availability2", new { Category = category, Form = form });
            };

            //Michael, how do I get the names back to the UI to display in the Breadcrumb (where it now says "search"

            ViewBag.Message = "This page is under construction.";
            ViewBag.Title = "Availability Search | Green Fuse Botanicals";
            ViewBag.ShowTestInfo = (SUB_DOMAIN_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());

            //override turn-off of Test Info
            ViewBag.ShowTestInfo = false;  
            
            return View("Availability2");
        }


    }
}

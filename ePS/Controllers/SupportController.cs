﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using BusinessObjectsLibrary;
using BusinessObjectServices;

using ePS.Models;

namespace ePS.Controllers
{
    public class SupportController : Controller
    {
        //
        // GET: /Support/

        public ActionResult Index()
        {
            ViewBag.Title = "Support Page";

            var service = new ContentService();
            var content = service.GetStaticContentType("/support/");
            ViewBag.ContentType = content;


            return View();
        }

        public ActionResult Confirmation()
        {

            ViewBag.Title = "Support Page";
            ViewBag.Message = "This page is under construction.";

            var service = new ContentService();
            var content = service.GetStaticContentType("/support/confirmation/");
            ViewBag.ContentType = content;


            return View();

        }


    }
}

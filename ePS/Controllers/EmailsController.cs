﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using BusinessObjectsLibrary;
using BusinessObjectServices;

//using AttributeRouting.Web.Mvc;

using ePS.Models;

namespace ePS.Controllers
{
    public class EmailsController : Controller
    {
        
        public ActionResult Index()
        {
            ViewBag.Title = "Emails Page";
            ViewBag.Message = "The Emails page.";

            var service = new ContentService();
            var content = service.GetStaticContentType("/Emails/");
            ViewBag.ContentType = content;

            return View();
        }

        //[GET("emails/{id}")]
        public ActionResult Link(string id)
        {
            ViewBag.Title = "Emails Page";
            ViewBag.Message = "An Emails page.";

            var path = id ?? string.Empty;
            while (path.StartsWith("/")) { path = path.Substring(1); };
            while (path.EndsWith("/")) { path = path.Substring(0, path.Length - 1); };
            path = string.Format("/Emails/Link/{0}/", path);


            var service = new ContentService();
            var content = service.GetStaticContentType(path);
            if (content.IsLoaded)
            {
                ViewBag.ContentType = content;
                return View("index");
            }
            else
            {
                return HttpNotFound("Page not found.");
            };
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessObjectServices;
using BusinessObjectsLibrary;

namespace ePS.Controllers.CatalogueAPI
{
    public class CatalogueController : ApiController
    {
        // GET api/catalogue
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/catalogue/5
        public int Get(string id)
        {
            int returnValue = -1;

            switch (id.ToLower()) {
                case "currentshipweek":
                    //ToDo: Code here to fetch current week
                    var service = new ShipWeekService();
                    returnValue = service.CurrentShipWeekNumber;

                    break;
                default:
                    break;
            }
            return returnValue;
        }

        // POST api/catalogue
        public void Post([FromBody]string value)
        {
        }

        // PUT api/catalogue/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/catalogue/5
        public void Delete(int id)
        {
        }
    }
}

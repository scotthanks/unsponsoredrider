﻿using AttributeRouting.Web.Http;
using ePS.Controllers.api;
using ePS.Models;

namespace ePS.Controllers.CatalogueAPI
{
    public class ProductOrderController : ApiControllerBase
    {
        [POST("api/ProductOrder")]
        [POST("api/V1/ProductOrder")]
        [POST("api/V2/Order/Cart")]
        public ProductOrderApiPostResponseModel Post(ProductOrderApiPostRequestModel request)
        {
            return new ProductOrderApiPostResponseModel(GetCurrentUserGuid(), request);
        }
    }
}
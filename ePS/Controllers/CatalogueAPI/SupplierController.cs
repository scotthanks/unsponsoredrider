﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ePS.Models;
using BusinessObjectsLibrary;
using BusinessObjectServices;
using ePS.Controllers.api;





namespace ePS.Controllers.CatalogueAPI
{
    public class SupplierController : ApiControllerBase
    {
        // GET api/supplier/5
        public IEnumerable<Supplier> Get(int id, string PlantCategoryCode, string ProductFormCode,string SellerCode)
        {
            //Note: for now this ignores value of id but some value there is necessary to call this api
            //ToDo: Replace Breeder and BreederServices with Supplier and SupplierServices
            var service = new SupplierService();
            var userguid = GetCurrentUserGuid();
            var sellerCode = "GFB";
            var suppliers = service.SelectSuppliers(userguid, PlantCategoryCode, ProductFormCode, sellerCode);
            return suppliers;
        }

    }
}

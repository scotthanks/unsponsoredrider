﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ePS.Models;
using BusinessObjectsLibrary;
using BusinessObjectServices;
using ePS.Controllers.api;





namespace ePS.Controllers.CatalogueAPI
{
    public class SellerController : ApiControllerBase
    {
        // GET api/seller/5
        public string Get()
        {
           
            
            var userGuid = GetCurrentUserGuid();
            var status = new StatusObject(userGuid,false);
            var service = new SellerService(status);
            var sellerCode = "GFB";
            sellerCode = service.GetSellerForUser();
            sellerCode = "GFB";
            return sellerCode;
        }

    }
}

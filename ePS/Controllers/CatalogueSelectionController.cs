﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ePS.Controllers
{
    public class CatalogueSelectionController : ApiController
    {
        // GET api/catalogueselection
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/catalogueselection/breeders
        public string Get(string id, string category, string plantForm)
        {
            const string BREEDERS_REQUEST_TYPE = "breeders";
            const string SPECIES_REQUEST_TYPE = "species";
            const string PRODUCTS_REQUEST_TYPE = "products";

            id = (id ?? string.Empty).ToLower();
            
            switch(id)
            {
                case BREEDERS_REQUEST_TYPE:

                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            };

            return "value";
        }

        // POST api/catalogueselection
        public void Post([FromBody]string value)
        {
        }

        // PUT api/catalogueselection/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/catalogueselection/5
        public void Delete(int id)
        {
        }
    }
}

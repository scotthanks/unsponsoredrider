﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using BusinessObjectsLibrary;
using BusinessObjectServices;

using ePS.Models;

 
namespace ePS.Controllers
{
    //[RequireHttps]
    public class AvailabilityController : Controller
    {
        const string SUB_DOMAIN_DEVELOPMENT = "dev";

        public ActionResult Index()
        {
            var service = new ContentService();
            var content = service.GetStaticContentType("/Availability/");
            ViewBag.ContentType = content;
            return View();
        }

       

        [Authorize]
        public ActionResult Search(string category, string form)
        {
            //browser sniffing stinks, but we do it
            var browserName = Request.Browser.Browser;
            var browserVersion = Request.Browser.MajorVersion;

            if (browserName == "IE" && browserVersion <= 8) {
                return RedirectToAction("EarlyIE", "Home");
            };

            //Logic of State Management:
            // 1. If valid Category/Form specified in Query String, proceed
            // 2. ELseIf Category or Form is invalid, redirect using default values

            const string DEFAULT_CATEGORY_CODE = "VA";  //ToDo: Move this to configuration
            const string DEFAULT_FORM_CODE = "RC";      //ToDo: Move this to configuration

            category = category ?? string.Empty;
            form = form ?? string.Empty;
            //Validate Category Code and Form Code

            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var service = new AvailabilityService(status);
            string productFormName = "";
            string categoryName = "";

            if (!service.IsValidCategoryCode(category, out categoryName) || !service.IsValidFormCode(form, out productFormName))
            {
                //Use Default Values
                category = DEFAULT_CATEGORY_CODE;
                form = DEFAULT_FORM_CODE;
                return RedirectToAction("Search", "Availability", new { Category = category, Form = form });
            };

            //Michael, how do I get the names back to the UI to display in the Breadcrumb (where it now says "search"

            ViewBag.Message = "This page is under construction.";
            ViewBag.Title = "Availability Search | Green-Fuse";
            ViewBag.ShowTestInfo = (SUB_DOMAIN_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());

            //override turn-off of Test Info
            ViewBag.ShowTestInfo = false;  
            
            return View("AvailabilitySearch");
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ePS.Models;
using ePS.Models.GrowerApi;
using AttributeRouting.Web.Http;
using System.Web;


using ePS.Models.SelectionMenu;

namespace ePS.Controllers.api
{
    public class SelectionMenuController : ApiController
    {
     

        [Route("api/SelectionMenu")]
        public SelectionMenuModel Get()
        {
            var model = new Models.SelectionMenu.SelectionMenuModel();
            return model;
        }
        

    }
}

﻿using System;

using System.Web.Http;
using ePS.Models.OrderLineApi;
using ePS.Types;

namespace ePS.Controllers.api
{
    public class OrderLineController : ApiControllerBase
    {

   


        //  [PUT("api/V2/Order/UpdateOrderLineQuantity", RouteName = "UpdateOrderLineQuantity"), System.Web.Http.HttpPut]
        [HttpPut]
        [Route("api/OrderLine")]
        public UpdateOrderLineQuantityResponseModel UpdateOrderLineQuantity([FromBody] UpdateOrderLineQuantityRequestModel request)
        {
            //CSingleTone.Instance.theCounter += 1;
            var x = new UpdateOrderLineQuantityResponseModel(GetCurrentUserGuid(), request);
            //request.Messages.
            //request.Messages = request.Messages + " " + CSingleTone.Instance.theCounter.ToString();
            return x;
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using System.Configuration;


using System.Web.Mvc;
using BusinessObjectsLibrary;
using BusinessObjectServices;
using ePS.Models;

namespace ePS.Controllers.api
{
    public class ResetPasswordTokenController : ApiController
    {

        private ePS.Models.UserProfile _UserProfileFromEmailAddress(string userEmailAddress)
        {
            var userProfile = new ePS.Models.UserProfile();
            try
            {
                using (UsersContext db = new UsersContext())
                {
                    userProfile = db.UserProfiles.FirstOrDefault(u => u.EmailAddress.ToLower() == userEmailAddress.ToLower());
                    return userProfile;
                }
            }
            catch (Exception e)
            {
                //var s = e.ToString();
                return new ePS.Models.UserProfile();
            }
        }

        // POST api/resetpasswordtoken
        [ValidateAntiForgeryToken]
        public object Post([FromUri] string EmailAddress)
        {
            //Antiforgery Token must have been valid to get here



            //ToDo: Move constants into configuration
            //const string PRODUCTION_SERVER_NAME = "PROD";
            //string subDomain = ConfigurationManager.AppSettings["SubDomain"].ToLower();
            string URI_STEM = "http://orders.green-fuse.com/MyAccount/ResetPassword";     
            const int TOKEN_EXPIRATION_MINUTES = 60 * 8;
            const string EMAIL_TEXT_SUBJECT = "Reset your Green Fuse Botanicals password";
            const string EMAIL_ADDRESS_FROM = "confirmations@green-fuse.com";
            const string EMAIL_ADDRESS_CC = "";
            const string EMAIL_ADDRESS_BCC = "ScottH@ePlantSource.com";
            const string EMAIL_ADDRESS_BCC2 = "confirmations@green-fuse.com";

            //set defaults
            var message = string.Empty;
            var success = true;

            //validate email address
            string emailAddress = EmailAddress ?? string.Empty;
            var util = new RegexUtilities();
            success = util.IsValidEmail(emailAddress);
            if (!success)
            {
                message += "You must enter a valid email address!";
            }
            else
            {

                //Now verify email address

                var profile = _UserProfileFromEmailAddress(emailAddress);

                success = (profile != null && profile.EmailAddress != null && profile.EmailAddress.ToLower() == emailAddress.ToLower());

                if (!success)
                {
                    message = string.Format("If {0} was found then a link to reset the password has been emailed.", emailAddress);
                }
                else
                {
                    //Create a Token
                    string confirmationToken = string.Empty;
                    try
                    {
                        confirmationToken = WebMatrix.WebData.WebSecurity.GeneratePasswordResetToken(profile.UserName, TOKEN_EXPIRATION_MINUTES);
                    }
                    catch { }
                    success = (confirmationToken != null && confirmationToken != string.Empty);
                    if (!success)
                    {
                        message = string.Format("Unable to reset passord related to {0}.", emailAddress);
                    }
                    else
                    {
                        //Create a link

                        string passwordResetUri = string.Format("{0}/{1}", URI_STEM, confirmationToken);
                        string passwordResetlink = string.Format("<a href='{0}'>open</a>", passwordResetUri);

                        //success = false;
                        //message = passwordResetlink;

                        //Embed link in email message
                        var emailMessage = new StringBuilder("<p>Hi</p>");
                        emailMessage.Append("<p>Can't remember your password?  No Problem.</p>");
                        emailMessage.AppendFormat("<p>Please {0} this link in your browser:<br />{1}.", passwordResetlink, passwordResetUri);
                        emailMessage.Append("<p>This will reset your password and give you the opportunity to update it.</p>");
                        emailMessage.Append("<p>Thanks!<br />Green Fuse Botanicals</p>");
                        emailMessage.Append("<p></p>");
                        //emailMessage.AppendFormat("<br />These links will expire in {0} minutes.", TOKEN_EXPIRATION_MINUTES);

                        var emptyGuid = new Guid("00000000-0000-0000-0000-000000000000");
                        var status = new StatusObject(emptyGuid, false);
                        var emailService = new EmailQueueService(status);
                        var bRet = emailService.AddEmailtoQueue(EMAIL_TEXT_SUBJECT, emailMessage.ToString(), 0, "Reset Password", "confirmations@green-fuse.com", emailAddress, "", "", emptyGuid, 0, 0, 0);

                    };

                };
            };
            return new
            {
                Success = success,
                Message = message
            };
        }

        // PUT api/resetpasswordtoken/<token value>
        public object Put([FromUri]string Token, [FromUri] string Password)
        {
            var message = string.Empty;

            //validate password and return message
            var success = (Password != null && Password.Length >= 6 && Password.Length <= 12);
            if (!success)
            {
                message = "Password invalid: Enter string between 6 and 12 characters";
            }
            else
            {
                success = WebMatrix.WebData.WebSecurity.ResetPassword(Token, Password);
                if (!success)
                {
                    message = "Unexpected Error: Unable to reset password.";
                };

            };

            var response = new
            {
                Success = success,
                Message = message
            };
            return response;
        }

    }
}

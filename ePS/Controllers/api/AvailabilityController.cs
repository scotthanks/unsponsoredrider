﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;


//using ePS.Filters;
using ePS.Models;

using ePS.Models.AvailabilityApi;
using AttributeRouting.Web.Http;
using AdminAppApiLibrary;


namespace ePS.Controllers.api
{
    public class AvailabilityController : ApiControllerBase
    {
        //[GET("api/Availability/GetGrowerVolumeLevels")]
        //public GetGrowerVolumeLevelsResponseModel GetVolumeLevels([FromUri] GetGrowerVolumeLevelsRequestModel request)
        //{
        //    return new GetGrowerVolumeLevelsResponseModel(GetCurrentUserGuid(), request);
        //}

        
        //[GET("api/AvailabilityCart")]
        //[GET("api/V1/Availability")]
        //[GET("api/V2/Availability")]

        //public GetAvailabilityResponseModel Get([FromUri] GetAvailabilityRequestModel availabilityRequest)
        //{
        //    return new GetAvailabilityResponseModel(GetCurrentUserGuid(), availabilityRequest);
        //}
     //   [GET("api/AvailabilityCount")]
        [HttpGet]
        [Route("api/Availability/AvailabilityCount")]
        public GetAvailabilityCountResponseModel GetAvailabilityCount([FromUri] string ProgramTypeCode, string Category, string ProductFormCategoryCode, string Form, string Suppliers, string Species, string ShipWeek, string OrganicOnly, string AvailableOnly, string SellerCode)
        {
            var availabilityRequest = new GetAvailabilityRequestModel();
            availabilityRequest.ProgramTypeCode = ProgramTypeCode;
            availabilityRequest.Category = Category;
            availabilityRequest.ProductFormCategoryCode = ProductFormCategoryCode;
            availabilityRequest.Form = Form;
            availabilityRequest.Suppliers = Suppliers;
            availabilityRequest.Species = Species;
            availabilityRequest.ShipWeek = ShipWeek;
            availabilityRequest.OrganicOnly = OrganicOnly;
            availabilityRequest.AvailableOnly = AvailableOnly;
            availabilityRequest.SellerCode = SellerCode;


       

            return new GetAvailabilityCountResponseModel(GetCurrentUserGuid(), availabilityRequest);
        }
        //[GET("api/AvailabilityByPage")]
        [HttpGet] 
        [Route("api/Availability/AvailabilityByPage")]
        public GetAvailabilityByPageResponseModel GetAvailabilityByPage([FromUri] string Category, string Form, string Suppliers, string Species, string ShipWeek,string OrganicOnly, string AvailableOnly, string SortOption, int PageSize, int PageNumber, string SellerCode)
        {

            var availabilityRequest = new GetAvailabilityByPageRequestModel();
            availabilityRequest.Category = Category;
            availabilityRequest.Form = Form;
            availabilityRequest.Suppliers = Suppliers;
            availabilityRequest.Species = Species;
            availabilityRequest.ShipWeek = ShipWeek;
            availabilityRequest.OrganicOnly = OrganicOnly;
            availabilityRequest.AvailableOnly = AvailableOnly;
            availabilityRequest.SortOption = SortOption;
            availabilityRequest.PageSize = PageSize;
            availabilityRequest.PageNumber = PageNumber;
            availabilityRequest.SellerCode = SellerCode;





            var x = new GetAvailabilityByPageResponseModel(GetCurrentUserGuid(), availabilityRequest);
            return x;
        }
        //[GET("api/Reports/GetCurrentAvailabilityReportData"), System.Web.Http.HttpGet]
        [HttpGet]
        [Route("api/Availability/GetCurrentAvailabilityReportData")]
        public CurrentAvailabilityReportGet GetCurrentAvailabilityReportGet(string Report)
        {
            var userName = User.Identity.Name;
            Guid userGuid = new Guid();
            var model = new CurrentAvailabilityReportGet(userGuid);
            return model;

        }
        //[GET("api/Reports/GetNextShipWeekCodes"), System.Web.Http.HttpGet]
        [HttpGet]
        [Route("api/Availability/GetNextShipWeekCodes")]
        public NextShipWeekCodesGet GetNextShipWeekCodes(string Week)
        {
            var userName = User.Identity.Name;
            Guid userGuid = new Guid();
            var model = new NextShipWeekCodesGet(userGuid);
            return model;

        }
        //[GET("api/V2/Availability/SubstituteList", RouteName = "AvailabilityV1SubstituteList")]
        //[GET("api/V2/Availability/SubstituteList"), System.Web.Http.HttpGet]
        //public GetSubstitutesResponseModel GetSubstituteList([FromUri] GetSubstitutesRequestModel request)
        //{
        //    return new GetSubstitutesResponseModel(GetCurrentUserGuid(), request);
        //}

        //[GET("api/Availability/SupplierProducts"), System.Web.Http.HttpGet]
        //public SupplierProductsResponse SupplierProducts([FromUri] Guid supplierOrderGuid)
        //{
        //    var model = new SupplierProductsModel(GetCurrentUserGuid(), supplierOrderGuid);
        //    return model.Response;
        //}

        //[PUT("api/AvailabilityCart")]
        //[PUT("api/V1/Availability")]
        //[PUT("api/V2/Availability/AddOrUpdateOrderLineQuantity")]
        //public PutResponseModel Put([FromBody]PutRequestModel request)
        //{
        //    var putmodel = new PutResponseModel(GetCurrentUserGuid(), request,false);
        //    return putmodel;
        //}
        [HttpPut]
        [Route("api/Availability")]
        public PutResponseModel Put2([FromBody]PutRequestModel request)
        {
            //var request = new PutRequestModel();
            //request.SessionGuid = new Guid(SessionGuid);
            //request.ShipWeekString = ShipWeekString;
            //request.SellerCode = SellerCode;
            //"SessionGuid": sessionGuid,
            //"ShipWeekString": shipWeekCode,
            //"SellerCode": "GFB",



            var putmodel = new PutResponseModel(GetCurrentUserGuid(), request, true);
            return putmodel;
        }

        //[POST("api/AvailabilityCart")]
        //[POST("api/V1/Availability")]
        //[POST("api/V2/Availability")]
        //public PostResponseModel Post([FromBody]PostRequestModel request)
        //{
        //    var postmodel =  new PostResponseModel(GetCurrentUserGuid(), request,false);
        //    return postmodel;
        //}
        [HttpPost]
        [Route("api/Availability")]
        public PostResponseModel Post2(  string GrowerOrderGuid,string Category, string Form, string ShipWeekString, string AddToOrder)
        {
            var request = new PostRequestModel();
            request.GrowerOrderGuid = GrowerOrderGuid;
            request.Category = Category;
            request.Form = Form;
            request.ShipWeekString = ShipWeekString;
            if(AddToOrder == "true")
            {
                request.AddToOrder = true;
            }
            else
            {
                request.AddToOrder = false;
            }
           

            var postmodel = new PostResponseModel(GetCurrentUserGuid(), request, true);

           

            return postmodel;
        }
        //[POST("api/AvailabilityOrder")]
        //public PostResponseModel PostOrder([FromBody]PostRequestModel request)
        //{
        //    request.AddToOrder = true;
        //    return new PostResponseModel(GetCurrentUserGuid(), request,false);
        //}
        //[GET("api/Availability/GetCategoryAndFormNames")]
        [HttpGet]
        [Route("api/Availability/GetCategoryAndFormNames")]
        public GetCategoryAndFormNamesResponseModel GetCategoryAndFormNames([FromUri] string CategoryCode, string FormCode)
        {
            var request = new GetCategoryAndFormNamesRequestModel();
            request.CategoryCode = CategoryCode;
            request.FormCode = FormCode;
            return new GetCategoryAndFormNamesResponseModel(GetCurrentUserGuid(), request);
        }
    }
}

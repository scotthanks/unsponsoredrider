﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ePS.Models.AvailabilityShipWeeksApi;

namespace ePS.Controllers.api
{
    public class AvailabilityShipWeeksController : ApiControllerBase
    {

      

        [HttpGet]
        [Route("api/AvailabilityShipWeeks")]
        public GetResponseModel GetShipWeeks([FromUri] string Category, string Form, string SupplierCodes, string SpeciesCodes, string ShipWeek)
        {

            var request = new GetRequestModel();
            request.Category = Category;
            request.Form = Form;
            request.SupplierCodes = SupplierCodes;
            request.SpeciesCodes = SpeciesCodes;
            request.ShipWeek = ShipWeek;
   


            var model = new GetResponseModel(GetCurrentUserGuid(), request);
            return model;
        }
    }
}

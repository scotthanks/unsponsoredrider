﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using BusinessObjectServices;

namespace ePS.Controllers.api
{
    public class ContentController : ApiController
    {
        //// GET api/content
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        
        
        
        // GET api/content/5
        public object Get(Guid id)
        {

            var service = new ContentService();
            var dynamicContent = service.GetDynamicContentType(id);

            

            var response = new { 
                success = true,
                html = dynamicContent.Html

            };

   

            return response;

        }

        
        
        
        
        
        //// POST api/content
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/content/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/content/5
        //public void Delete(int id)
        //{
        //}
    }
}

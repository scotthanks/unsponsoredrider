﻿
using System.Collections.Generic;

using System.Web.Http;

using BusinessObjectsLibrary;
using BusinessObjectServices;

namespace ePS.Controllers.api
{
    public class SpeciesController : ApiControllerBase
    {
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }




        // GET api/species/5
        public IEnumerable<Species> Get(int id, string PlantCategoryCode, string ProductFormCode, string SupplierCodes,string SellerCode)
        {
            SellerCode = "GFB";
            if (SupplierCodes != null)
            {
                string[] supplierCodeArray = SupplierCodes.Split(',');

                var userGuid = GetCurrentUserGuid();
                var status = new StatusObject(userGuid, true);
                var service = new SpeciesService(status);
                var list = service.SelectSpecies(SellerCode,PlantCategoryCode, ProductFormCode, supplierCodeArray);
                return list;
            }
            else
            {
                List<Species> list = new List<Species>();
                return list;
            }            
        }

        // POST api/species
        public void Post([FromBody]string value)
        {
        }

        // PUT api/species/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/species/5
        public void Delete(int id)
        {
        }
    }
}

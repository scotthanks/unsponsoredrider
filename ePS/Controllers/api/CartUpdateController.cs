﻿
using System.Web.Http;


using ePS.Models.CartUpdateApi;


namespace ePS.Controllers.api
{
    public class CartUpdateController : ApiControllerBase
    {
        //private Guid _UserGuid(string userHandle)
        //{
        //    var userGuid = new Guid();

        //    using (UsersContext db = new UsersContext())
        //    {
        //        UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == userHandle.ToLower());
        //        userGuid = user.UserGuid;
        //    }

        //    return userGuid;
        //}
        [HttpPut]
        [Route("api/CartUpdate")]
        public CartOrderUpdateResponseModel PutOrderUpdateFromCart(CartOrderUpdateRequestModel request)
        {
           // var userHandle = string.Empty;
            //var userGuid = Guid.Empty;
            //if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            //{
            //    userHandle = System.Web.HttpContext.Current.User.Identity.Name;  //set to users Login
            //    userGuid = this._UserGuid(userHandle);
            //}
                       

            var responseModel = new CartOrderUpdateResponseModel(GetCurrentUserGuid(), request);

            return responseModel;
        }

    }
}

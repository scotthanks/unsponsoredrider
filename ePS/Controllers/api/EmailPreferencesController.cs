﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;
using ePS.Models.EmailPreferencesApi;

namespace ePS.Controllers.api
{
    [Authorize]
    public class EmailPreferencesController : ApiControllerBase
    {
        private bool IsAccountAdministrator
        {
            //ToDo: Replace this by adding GetCurrentUserRole to ApiControllerBase once Account Admin Role is working
            get
            {
                //Determine if authenticated user has Account Admin Role

                var userGuid = GetCurrentUserGuid();
                var personService = new BusinessObjectServices.PersonService();
                var person = personService.GetPersonFromUser(userGuid);
                var role = person.UserRole;
                //ToDo: The above currently does not return a UserRole
                //either enable UserRole if that is where this information should be gotten
                //or replace above code to determine if user is Admin

                //ToDo: This his hard-coded till above code is operational
                return true;

            }
        }


        [GET("api/EmailPreferences/GetEmailPreferences", RouteName = "GetEmailPreferences")]
        public IEnumerable<EmailPreferenceType> GetEmailPreferences()
        {
            var isAccountAdministrator = this.IsAccountAdministrator;
            //response.EmailPreferences.IsAccountAdministrator = isAccountAdministrator;

            var list = new List<EmailPreferenceType>();

            //ToDo: Instantiate service to get Email Preferences

            //ToDo: Populate list of Email Preferences
            list.Add(new EmailPreferenceType()
            {
                Name="Fred Flintstone",
                Email="Fred@BedrockQuarry.net",
                IsAccountAdministrator = isAccountAdministrator
            });

            //Convert list to IEnumerable type and return it
            return list;
        }

        [AcceptVerbs("POST")]
        [POST("api/EmailPreferences/Update", RouteName = "UpdateEmailPreferences")]
        public PostResponseModel PostUpdate(PostRequestModel request)
        {

            var response = new PostResponseModel(GetCurrentUserGuid(), request);
            return response;
        }


        [AcceptVerbs("PUT")]
        [PUT("api/EmailPreferences/Create", RouteName = "PutCreateEmailPreference")]
        public PutResponseModel PutCreate(PutRequestModel request)
        {
            var response = new PutResponseModel(GetCurrentUserGuid(), request);
            return response;
        }

        [AcceptVerbs("DELETE")]
        [DELETE("api/EmailPreferences/Delete/{emailGuid}", RouteName = "DeleteEmailPreferences")]
        public object Delete(Guid emailGuid)
        {
            //Note: This action takes a valid Guid and deletes Email Preferences for that Guid

            var success = (emailGuid != Guid.Empty);
            if (success)
            {
                //ToDo: Code to Delete data, returning a boolean indicating success
                success = true;  //hard coded for now
            }

            var response = new
            {
                Success = success,
                IsAuthenticated = true,
                this.IsAccountAdministrator,
                EmailGuid = emailGuid
            };
            return response;

        }






    }
}

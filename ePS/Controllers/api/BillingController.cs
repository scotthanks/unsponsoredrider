﻿using AttributeRouting.Web.Http;
using ePS.Models.AdminAppAccountingApi;
using ePS.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ePS.Controllers.api
{
    public class BillingController : ApiController
    {
        //[GET("api/Billing", RouteName = "Billing")]
        //[GET("api/V1/Billing/{RequestType:alpha}", RouteName = "Billing1")]
        //[GET("api/V2/Billing/billings/{RequestType:alpha}", RouteName = "Billing2")]
        //void Action2()
        //{
        //}

        [GET("api/Billing/Action", RouteName = "Billing3")]
        [GET("api/V1/Billing/Action/{RequestType:alpha}", RouteName = "Billing4")]
        [GET("api/V2/Billing/list/{RequestType:alpha}", RouteName = "Billing5")]
        public void Action()
        {

        }

    }
}

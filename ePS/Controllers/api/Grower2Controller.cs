﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;


//using ePS.Filters;
using ePS.Models;

using ePS.Models.Grower2Api;
using AttributeRouting.Web.Http;
using AdminAppApiLibrary;



namespace ePS.Controllers.api
{
    public class Grower2Controller : ApiControllerBase
    {



        [HttpGet]
        [Route("api/Grower2/GrowerTotals")]
        public GetGrowerTotalsResponseModel GetGrowerTotals([FromUri] string ProgramTypeCode, string ProductFormCategoryCode)
        {
            var request = new GetGrowerTotalsRequestModel();
            request.ProgramTypeCode = ProgramTypeCode;
            request.ProductFormCategoryCode = ProductFormCategoryCode;

            return new GetGrowerTotalsResponseModel(GetCurrentUserGuid(), request);

        }

        [HttpGet]
        [Route("api/Grower2/TotalsPerWeek")]
        public GetGrowerTotalsPerWeekResponseModel GetGrowerTotalsPerWeek([FromUri] string ProgramTypeCode2, string ProductFormCategoryCode2)
        {
            
            var request = new GetGrowerTotalsRequestModel();
            request.ProgramTypeCode = ProgramTypeCode2;
            request.ProductFormCategoryCode = ProductFormCategoryCode2;

            return new GetGrowerTotalsPerWeekResponseModel(GetCurrentUserGuid(), request);

        }

        [HttpGet]
        [Route("api/Grower2/GetGrowerOrderSummaries")]
        public GetGrowerOrderSummariesResponseModel GetGrowerOrderSummaries([FromUri] string ShipWeekCode, string SellerCode)
        {
       
            var request = new GetGrowerOrderGetSummariesRequestModel();
            request.ShipWeekCode = ShipWeekCode;
            request.SellerCode = SellerCode;
  
       
            return new GetGrowerOrderSummariesResponseModel(GetCurrentUserGuid(), request);

        }
    }
}

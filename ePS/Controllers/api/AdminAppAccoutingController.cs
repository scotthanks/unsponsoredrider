﻿using AdminAppApiLibrary;
using AttributeRouting.Web.Http;
using ePS.Models.AdminAppAccountingApi;
using ePS.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ePS.Controllers.api
{

    public class AdminAppAccoutingController : ApiControllerBase
    {
        ////[GET("api/AdminAppAccouting/GetBillingList", RouteName = "GetBillingList")]
        ////public BillingSummaryResponse GetBillingList([FromUri] BillingSummaryRequest request)
        ////{
        ////    var model = new BillingListResponseModel(GetCurrentUserGuid(), request);
        ////    return model.Response;
        ////}

        //[GET("api/AdminAppAccouting/GetInvoicedList", RouteName = "GetInvoicedList"), System.Web.Http.HttpGet]
        //public InvoicedSummaryResponse GetInvoicedList([FromUri] InvoicedSummaryRequest request)
        //{
        //    var model = new InvoicedListResponseModel(GetCurrentUserGuid(), request);
        //    return model.Response;
        //}

        //// TBD - not used
        ////[GET("api/AdminAppAccouting/GetUnappliedPayments", RouteName = "GetUnappliedPayments"), System.Web.Http.HttpGet]
        ////public UnappliedPaymentsResponse GetUnappliedPayments([FromUri] GetUnappliedPaymentsRequest request)
        ////{
        ////    var model = new UnappliedPaymentsResponseModel(GetCurrentUserGuid(), request);
        ////    return model.Response;
        ////}

        //[GET("api/AdminAppAccouting/GetLedgerTransactions", RouteName = "GetLedgerTransactions"), System.Web.Http.HttpGet]
        //public LedgerTransactionsResponse GetLedgerTransactions([FromUri] LedgerTransactionsRequest request)
        //{
        //    var model = new LedgerTransactionsResponseModel(GetCurrentUserGuid(), request);
        //    return model.Response;
        //}

        //[GET("api/AdminAppAccouting/InvoiceLedgerEntry", RouteName = "InvoiceLedgerEntry"), System.Web.Http.HttpGet]
        //public InvoiceLedgerEntryResponse InvoiceLedgerEntry([FromUri] InvoiceEntryRequest request)
        //{
        //    Guid orderGuid = new Guid(request.OrderGuid);
        //    var model = new InvoiceLedgerEntryResponseModel(GetCurrentUserGuid(), orderGuid);
        //    return model.Response;
        //}

        //[GET("api/AdminAppAccouting/GetAccountBalance", RouteName = "GetAccountBalance"), System.Web.Http.HttpGet]
        //public AccountBalanceResponse GetAccountBalance([FromUri] AccountBalanceRequest request)
        //{
        //    var model = new AccountBalanceResponseModel(GetCurrentUserGuid(), request);
        //    return model.Response;
        //}

        ////[GET("api/AdminAppAccouting/Suppliers", RouteName = "Suppliers"), System.Web.Http.HttpGet]
        ////public SuppliersResponse Suppliers()
        ////{
        ////    var model = new SuppliersResponseModel(GetCurrentUserGuid());
        ////    return model.Response;
        ////}

        //[GET("api/AdminAppAccouting/SupplierInvoices", RouteName = "SupplierInvoices"), System.Web.Http.HttpGet]
        //public SupplierInvoicesResponse SupplierInvoices([FromUri] Guid SupplierGuid)
        //{
        //    var model = new SupplierInvoicesResponseModel(GetCurrentUserGuid(), SupplierGuid);
        //    return model.Response;
        //}

        ////[GET("api/AdminAppAccouting/SupplierOrders", RouteName = "SupplierOrders"), System.Web.Http.HttpGet]
        ////public SupplierOrdersResponse SupplierOrders([FromUri] Guid SupplierGuid)
        ////{
        ////    var model = new SupplierOrdersResponseModel(GetCurrentUserGuid(), SupplierGuid);
        ////    return model.Response;
        ////}


        //[POST("api/AdminAppAccouting/BillOrder", RouteName = "BillOrder")]
        //public BillOrderResponse BillOrder([FromBody] BillOrderRequest request)
        //{
        //    var model = new BillOrderResponseModel(GetCurrentUserGuid(), request);
        //    return model.Response;
        //}


        //[POST("api/AdminAppAccouting/CreditCardPayment", RouteName = "CreditCardPayment")]
        //public CreditCardPaymentResponse CreditCardPayment([FromBody] CreditCardPaymentRequest request)
        //{
        //    var model = new CreditCardPaymentResponseModel(GetCurrentUserGuid(), request);
        //    return model.Response;
        //}

        //[POST("api/AdminAppAccouting/SaveInvoiceDocument", RouteName = "SaveInvoiceDocument")]
        //public SaveInvoiceDocumentResponse SaveInvoiceDocument([FromBody] SaveInvoiceDocumentRequest request)
        //{
        //    var model = new SaveInvoiceDocumentResponseModel(GetCurrentUserGuid(), request);
        //    return model.Response;
        //}

        //[POST("api/AdminAppAccouting/CheckPayment", RouteName = "CheckPayment")]
        //public CheckPaymentResponse CheckPayment([FromBody] CheckPaymentRequest request)
        //{
        //    var model = new CheckPaymentResponseResponseModel(GetCurrentUserGuid(), request);
        //    return model.Response;
        //}

        //[POST("api/AdminAppAccouting/LedgerTransaction", RouteName = "LedgerTransaction")]
        //public LedgerTransactionResponse LedgerTransaction([FromBody] LedgerTransactionRequest request)
        //{
        //    var model = new LedgerTransactionResponseModel(GetCurrentUserGuid(), request);
        //    return model.Response;
        //}


        //[POST("api/AdminAppAccouting/CreateUnappliedPayment", RouteName = "CreateUnappliedPayment"), System.Web.Http.HttpPost]
        //public CreateUnappliedPaymentResponse CreateUnappliedPayment([FromBody] CreateUnappliedPaymentRequest request)
        //{
        //    var model = new CreateUnappliedPaymentResponseModel(GetCurrentUserGuid(), request);
        //    return model.Response;
        //}




    }
}

﻿using System;

using System.Web.Http;
using ePS.Models.OrderApi;
using ePS.Types;

namespace ePS.Controllers.api
{
    public class OrderController : ApiControllerBase
    {

       // [GET("api/Order/SupplierOrderFees/{supplierOrderGuid:guid}", RouteName = "GetSupplierOrderFees")]
        [HttpGet]
        [Route("api/Order/SupplierOrderFees")]
        public ePS.Models.OrderApi.GetResponseSupplierFeeModel GetSupplierOrderFees(string supplierOrderGuid)
        {
            //var id = Guid.NewGuid();
            var request = new GetRequestSupplierFeeModel() { SupplierOrderGuid = new Guid(supplierOrderGuid) };

             return new ePS.Models.OrderApi.GetResponseSupplierFeeModel(GetCurrentUserGuid(), request);

        }

   //     [GET("api/Order/GrowerOrderFees/{growerOrderGuid:guid}", RouteName = "GetGrowerOrderFees")]
        [HttpGet]
        [Route("api/Order/GrowerOrderFees")]
        public ePS.Models.OrderApi.GetResponseGrowerFeeModel GetGrowerOrderFees(string growerOrderGuid)
        {
            //var id = Guid.NewGuid();
            var request = new GetRequestGrowerFeeModel() { GrowerOrderGuid = new Guid(growerOrderGuid) };
            return new ePS.Models.OrderApi.GetResponseGrowerFeeModel(GetCurrentUserGuid(), request);

        }
        

        
        //[GET("api/Order", RouteName = "OrderGetSummaryV1a")]
        //[GET("api/V1/Order/{RequestType:alpha}", RouteName = "OrderGetSummaryV1b")]
        //public GetResponseSummaryModel Get([FromUri] GetSummaryRequestModel summaryRequest, string requestType)
        //{
        //    if (summaryRequest.RequestType == "MyPlacedOrders")
        //    {
        //        summaryRequest.RequestType = GetSummaryRequestModel.RequestTypeEnum.MyOpenOrders.ToString();
        //    }
        //    else if (summaryRequest.RequestType == "AllPlacedOrders")
        //    {
        //        summaryRequest.RequestType = GetSummaryRequestModel.RequestTypeEnum.AllOpenOrders.ToString();
        //    }

            
        //    //return new GetResponseSummaryModel(GetCurrentUserGuid(), summaryRequest);
        //        var model = new GetResponseSummaryModel(GetCurrentUserGuid(), summaryRequest);
        //        return model;
           
        //}
       
      //  [GET("api/Order/DetailHistory", RouteName = "OrderGetDetailHistory")]
        [HttpGet]
        [Route("api/Order/DetailHistory")]
        public GetOrderHistoryResponseModel GetDetailHistory([FromUri] string OrderGuid)
        {
            var theGuid = new Guid(OrderGuid);
            //ToDo: Scott, the line of code creating the var theGuid will convert any faulty guid passed in to an empty guid
            //an empty guid breaks the GetOrderHistoryResponseModel.  It should be able to handle an empty guid (a faulty guid coming in)
            //Moreover, it is questionable to create a new GetOrderHistoryRequestModel with no inputs as you create a new GetOrderHistoryResponseModel, why not create it inside rather than as a parameter here
            return new GetOrderHistoryResponseModel(GetCurrentUserGuid(), theGuid, new GetOrderHistoryRequestModel());
           
        }

       // [GET("api/V2/Order/Summary/MyShoppingCartOrders")]
        [HttpGet]
        [Route("api/Order/MyShoppingCartOrders")]
        public GetCartResponseModel GetMyShoppingCartOrders(string TheType)
        {
            return new GetCartResponseModel(GetCurrentUserGuid());
        }

       // [GET("api/V2/Order/Summary/MyOpenOrders")]
        [HttpGet]
        [Route("api/Order/MyOpenOrders")]
        public GetOpenOrderSummary GetMyOpenOrders(string TheType2)
        {
       
            var getOpenOrderSummary =  new GetOpenOrderSummary(GetCurrentUserGuid());
            return getOpenOrderSummary;
        }

    //    [GET("api/V2/Order/Summary/MyShippedOrders")]
        [HttpGet]
        [Route("api/Order/MyShippedOrders")]
        public GetShippedOrderSummary GetMyShippedOrders(string TheType3)
        {
            var getShippedOrderSummary = new GetShippedOrderSummary(GetCurrentUserGuid());
            return getShippedOrderSummary;
        }

       // [GET("api/V2/Order/Summary/MyCancelledOrders")]
        [HttpGet]
        [Route("api/Order/MyCancelledOrders")]
        public GetCancelledOrderSummary GetMyCancelledOrders(string TheType4)
        {
            var getCancelledOrderSummary = new GetCancelledOrderSummary(GetCurrentUserGuid());
            return getCancelledOrderSummary;
        }


        //Old way
        //[GET("api/Order/Totals", RouteName = "OrderGetTotals")]
        //public object Get([FromUri] string category, string form, string shipWeeks)
        //{
        //    //ToDo: Rick you may want to revise the name I gave this to fit your convention
        //    //ToDo: Rick please populate these values and add any other values that are relevent to an order over its entire life
        //    int carted = 555;
        //    int uncarted = 999;

        //    //ToDo: Rick, you may want to use a Request Object, Response Object and Defined Type 
        //    var response = new {
        //        Carted = carted,
        //        Uncarted = uncarted,
        //        Total = carted + uncarted
        //    };
            
        //    return response;
        //}

        //new way
        
        [HttpGet]
        [Route("api/Order/Totals")]
        public GetResponseGrowerOrderTotalsModel GetGrowerOrderTotals([FromUri] string GrowerOrderGuid, string Category, string Form, string ShipWeekString)
        {
            //public GetResponseGrowerOrderTotalsModel GetGrowerOrderTotals([FromUri] Guid GrowerOrderGuid,string Category, string Form, string ShipWeekString)
            //var id = Guid.NewGuid();
            //var request = new GetRequestGrowerOrderTotalsModel() {GrowerOrderGuid = GrowerOrderGuid, Category = Category, Form = Form, ShipWeekString = ShipWeekString };


            var request = new GetRequestGrowerOrderTotalsModel();
            request.GrowerOrderGuid = new Guid(GrowerOrderGuid);
            request.Category = Category;
            request.Form = Form;
            request.ShipWeekString = ShipWeekString;
   

            return new GetResponseGrowerOrderTotalsModel(GetCurrentUserGuid(), request);

        }




        //[GET("api/Order/DetailData", RouteName = "OrderGetDetailV1")]
        //[HttpGet]
        //[Route("api/Order/DetailData")]
        //public GetResponseDetailModel Get([FromUri] GetRequestDetailModel request, Guid orderGuid)
        //{

        //    var model = new GetResponseDetailModel(GetCurrentUserGuid(), request);
        //    return model;
        //}
        //  [GET("api/Order/OrderPDF")] 
        //AttributeRouting
        //public HttpResponseMessage GetPdfRecordData([FromUri] Guid OrderGuid)
        //{
        //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "value");
        //    var x = new GetOrderPDFResponseModel(GetCurrentUserGuid(), OrderGuid);

        //    MemoryStream ms = x.OrderPDF;
        //    response.Content = new ByteArrayContent(ms.ToArray());
        //    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
        //    ms.Close();
        //    return response;
        //    //MemoryStream ms2 = x.OrderPDF;

        //    //using (FileStream file = new FileStream("c:\\temp\\confirmation.pdf", FileMode.Create, System.IO.FileAccess.Write))
        //    //{
        //    //    byte[] bytes = new byte[ms2.Length];
        //    //    ms2.Read(bytes, 0, (int)ms2.Length);
        //    //    file.Write(bytes, 0, bytes.Length);
        //    //    ms.Close();
        //    //}

        //}
        //   [GET("api/Order/OrderPDF")] //AttributeRouting

        //public GetOrderPDFResponseModel GetPdfRecordData([FromUri] string OrderGuid2)
        //{

        //    var x = new GetOrderPDFResponseModel(GetCurrentUserGuid(), new Gujid(OrderGuid));   
        //    return x;
        //} 


        //TODO: Depricated
        //[GET("api/Order/DetailData", RouteName = "OrderGetDetailV1a")]
        //[GET("api/V1/Order/DetailData", RouteName = "OrderGetDetailV1b")]
        [HttpGet]
        [Route("api/Order/DetailData")]
        public GrowerOrderType GetV1([FromUri] string OrderGuid3)
        {
            var detailRequest = new GetDetailRequestModel();
            detailRequest.OrderGuid = new Guid(OrderGuid3);
            var model = new GetDetailResponseModel(GetCurrentUserGuid(), detailRequest);
            return model.GrowerOrder;
        }

        ////[GET("api/V2/Order/Detail", RouteName = "OrderGetDetailV2Request")]
        ////[GET("api/V2/Order/Detail/{orderGuid:guid}", RouteName = "OrderGetDetailV2OrderGuid")]
        //[HttpGet]
        //[Route("api/Order/DetailData")]
        //public GetResponseDetailModelV2 GetV2([FromUri] string OrderGuid3)
        //{
        //    var request = new GetRequestDetailModelV2();
        //    request.OrderGuid = new Guid(OrderGuid3);
        //    request.RequestType = "load";



        //    return new GetResponseDetailModelV2(GetCurrentUserGuid(), request);
        //}

        //[PUT("api/Order", RouteName = "OrderPutV1a")]
        //[PUT("api/V1/Order", RouteName = "OrderPutV1b")]
        //public PutResponseModel PutV1([FromBody] PutRequestModel request)
        //{

        //    return new PutResponseModel(GetCurrentUserGuid(), request);
        //}

        //   [PUT("api/V2/Order/{GrowerOrderGuid:guid}/PlaceOrder", RouteName = "OrderPutV2GrowerOrderPlaceOrder")]


        //public PutResponseModel PutV2PlaceOrder(Guid growerOrderGuid)
        //{
        //    var request = new PutRequestModel()
        //        {
        //            GrowerOrderGuid = growerOrderGuid,
        //            OrderTransitionTypeLookupCode = "PlaceOrder"
        //        };

        //    return new PutResponseModel(GetCurrentUserGuid(), request);
        //}

        //[PUT("api/V2/Order", RouteName = "OrderPutV2Request")]
        [HttpPut]
        [Route("api/Order")]
        public PlaceOrderResponseModel PutV2Request([FromBody] PlaceOrderRequestModel request)
        {
            return new PlaceOrderResponseModel(GetCurrentUserGuid(), request);
        }


        ////  [PUT("api/V2/Order/UpdateOrderLineQuantity", RouteName = "UpdateOrderLineQuantity"), System.Web.Http.HttpPut]
        //[HttpPut]
        //[Route("api/Order")]
        //public UpdateOrderLineQuantityResponseModel UpdateOrderLineQuantity([FromBody] UpdateOrderLineQuantityRequestModel request)
        //{
        //    //CSingleTone.Instance.theCounter += 1;
        //    var x = new UpdateOrderLineQuantityResponseModel(GetCurrentUserGuid(), request);
        //    //request.Messages.
        //    //request.Messages = request.Messages + " " + CSingleTone.Instance.theCounter.ToString();
        //    return x;
        //}


        //[PUT("api/V2/Order/GrowerOrder/{GrowerOrderGuid:guid}", RouteName = "OrderPutV2bGrowerOrder")]
        //public PutResponseModel PutV2GrowerOrder(Guid growerOrderGuid, [FromBody] PutRequestModel request)
        //{
        //    request.GrowerOrderGuid = growerOrderGuid;

        //    return new PutResponseModel(GetCurrentUserGuid(), request);
        //}

        //[PUT("api/V2/Order/SupplierOrder/{SupplierOrderGuid:guid}", RouteName = "OrderPutV2SupplierOrderGuid")]
        //public PutResponseModel PutV2SupplierOrder(Guid supplierOrderGuid, [FromBody] PutRequestModel request)
        //{
        //    request.SupplierOrderGuid = supplierOrderGuid;

        //    return new PutResponseModel(GetCurrentUserGuid(), request);
        //}

        //[PUT("api/V2/Order/OrderLine/{OrderLineGuid:guid}", RouteName = "OrderPutV2OrderLine")]
        //public PutResponseModel PutV2OrderLine(Guid orderLineGuid, [FromBody] PutRequestModel request)
        //{
        //    request.OrderLineGuid = orderLineGuid;

        //    return new PutResponseModel(GetCurrentUserGuid(), request);
        //}

        //[PUT("api/V2/Order/UpdatePrices", RouteName = "OrderPutUpdatePricesV2")]
        //public PutPriceUpdateResponseModel Put([FromBody] PutPriceUpdateRequestModel request)
        //{
        //    return new PutPriceUpdateResponseModel(GetCurrentUserGuid(), request);
        //}

        //[PUT("api/V2/Order/Substitute", RouteName = "OrderSubstituteV2"), System.Web.Http.HttpPut]
        //public SubstituteResponseModel Substitute([FromBody] SubstituteRequestModel request)
        //{
        //    return new SubstituteResponseModel(GetCurrentUserGuid(), request);
        //}




        //[POST("api/Order/AddOrUpdateOrderLine", RouteName = "AddOrUpdateOrderLine"), System.Web.Http.HttpPost]
        //public AddOrUpdateOrderLineResponseModel AddOrUpdateOrderLine(AddOrUpdateOrderLineRequestModel request)
        //{
        //    return new AddOrUpdateOrderLineResponseModel(GetCurrentUserGuid(), request);
        //}


        //[POST("api/V2/Order/GrowerOrderLog", RouteName = "OrderPostGrowerOrderLogV2")]
        //public PostGrowerOrderLogResponseModel Post([FromBody] PostGrowerOrderLogRequestModel growerOrderLogRequest)
        //{
        //    return new PostGrowerOrderLogResponseModel(GetCurrentUserGuid(), growerOrderLogRequest,"Add");
        //}


        //[POST("api/V2/Order/GrowerOrderLogDelete", RouteName = "OrderDeleteGrowerOrderLogV2"), System.Web.Http.HttpPost]
        //public DeleteGrowerOrderLogResponseModel DeleteLog([FromBody]  DeleteGrowerOrderLogRequestModel growerOrderHistoryLogRequest)
        //{
        //   // var growerOrderHistoryLogRequest = new DeleteGrowerOrderLogRequestModel();
        //   // growerOrderHistoryLogRequest.GrowerOrderHistoryGuid = growerOrderLogRequest.GrowerOrderGuid;
        //    return new DeleteGrowerOrderLogResponseModel(GetCurrentUserGuid(), growerOrderHistoryLogRequest);
        //}

        //[POST("api/V2/Order/GrowerOrderLogEdit", RouteName = "OrderEditGrowerOrderLogV2"), System.Web.Http.HttpPost]
        //public EditGrowerOrderLogResponseModel EditLog([FromBody]  EditGrowerOrderLogRequestModel growerOrderLogRequest)
        //{
        //    // var growerOrderHistoryLogRequest = new DeleteGrowerOrderLogRequestModel();
        //    // growerOrderHistoryLogRequest.GrowerOrderHistoryGuid = growerOrderLogRequest.GrowerOrderGuid;
        //    return new EditGrowerOrderLogResponseModel(GetCurrentUserGuid(), growerOrderLogRequest);
        //}

    }
}
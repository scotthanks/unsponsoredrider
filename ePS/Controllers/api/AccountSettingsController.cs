﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;

using System.Web.Http;
using AttributeRouting.Web.Http;
using ePS.Models;
using ePS.Models.AccountSettingsApi;

using BusinessObjectServices;

namespace ePS.Controllers.api
{
    [Authorize]
    public class AccountSettingsController : ApiControllerBase
    {

        private bool IsAccountAdministrator
        {
            //ToDo: Replace this by adding GetCurrentUserRole to ApiControllerBase once Account Admin Role is working
            get
            {
                //Determine if authenticated user has Account Admin Role

                var userGuid = GetCurrentUserGuid();
                var personService = new BusinessObjectServices.PersonService();
                var person = personService.GetPersonFromUser(userGuid);
                var role = person.UserRole;
                //ToDo: The above currently does not return a UserRole
                //either enable UserRole if that is where this information should be gotten
                //or replace above code to determine if user is Admin

                //ToDo: This his hard-coded until above code is operational
                return true;

            }
        }

        [GET("api/AccountSettings/GetUserIsAccountAdministrator", RouteName = "GetUserIsAccountAdministrator")]
        public object GetUserIsAccountAdministrator([FromUri]Guid id)
        {
            //NOTE: no models needed for this, anonamous object returned.
            //Could not get here unless authenticated
            var success = id != Guid.Empty; //must have a valid user Guid to get valid answer
            var isAuthenticated = false;
            if (success) { isAuthenticated = (GetCurrentUserGuid() != Guid.Empty); }
            var response = new
            {
                IsAuthenticated = isAuthenticated,
                Success = success,
                IsAccountAdministrator
            };
            return response;
        }


        [GET("api/AccountSettings/GetCurrentAccountSettings", RouteName = "GetCurrentAccountSettings")]
        public GetResponseModel GetCurrentAccountSettings()
        {
            //Note: This takes no parameters and returns the Account Settings based on the currently authenticated user
            var request = new GetRequestModel()
            {
                AccountGuid = GetCurrentUserGuid()
            };
            
            var response = new GetResponseModel(GetCurrentUserGuid(), request);
            var isAccountAdministrator = this.IsAccountAdministrator;
            response.AccountSettings.IsAccountAdministrator = isAccountAdministrator;
            return response;
        }
        
        [GET("api/AccountSettings/GetUserAccountSettings", RouteName = "GetUserAccountSettings")]
        public GetResponseModel GetUserAccountSettings([FromUri]GetRequestModel request)
        {
            //Note: This action takes a valid Account Guid and returns Account Settings for that Account
            //ToDo: There is a problem with this to straighten out.  The Guid coming in here is supposed to be Account Guid, not User Guid.
            //  This action uses the GetResponse as the one based on current user.  Maybe it should have one based on Account Guid, not User Guid.
            var response = new GetResponseModel(GetCurrentUserGuid(), request);
            var isAccountAdministrator = this.IsAccountAdministrator;
            response.AccountSettings.IsAccountAdministrator = isAccountAdministrator;
            
            return response;
        }




        [AcceptVerbs("POST")]
        [POST("api/AccountSettings/Update", RouteName = "PostUpdateAccountSettings")]
        public PostResponseModel PostUpdate(PostRequestModel request)
        {

            var response = new PostResponseModel(GetCurrentUserGuid(), request);
            return response;
        }


        [AcceptVerbs("PUT")]
        [PUT("api/AccountSettings/Create/", RouteName = "PutCreateAccountSettings")]
        public PutResponseModel PutCreate(PutRequestModel request)
        {
            
            
            var response = new PutResponseModel(GetCurrentUserGuid(), request);
            
            return response;
        }

        [AcceptVerbs("DELETE")]
        [DELETE("api/AccountSettings/Delete/{accountGuid}", RouteName = "DeleteAccountSettings")]
        public object Delete(Guid accountGuid)
        {
            //    //Note: This action takes a valid Account Guid and deletes Account Settings for that Account

            var success = (accountGuid != Guid.Empty);
            if (success) {
                //ToDo: Code to Delete Account Settings, returning a boolean
                success = true;  //value resulting from delete process
            }

            var response = new
            {
                Success = success,
                IsAuthenticated = true,
                this.IsAccountAdministrator,
                AccountGuid = accountGuid
            };
            return response;

        }

    }
}

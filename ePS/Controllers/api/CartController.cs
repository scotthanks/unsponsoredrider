﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;

using ePS.Models;
using ePS.Models.CartApi;
using ePS.Types;

namespace ePS.Controllers.api
{
    
    public class CartController : ApiControllerBase
    {
        //[POST("api/Cart", RouteName = "CartPostV1a")]
        //[POST("api/V1/Cart", RouteName = "CartPostV1b")]
        //[POST("api/V2/Cart/UpdateOrDeleteOrderLines", RouteName = "CartPostV2")]
        [HttpPut]
        [Route("api/Cart")]
        public PostResponseModel PutCartUpdate(PostRequestModel request)
        {
            return new PostResponseModel(GetCurrentUserGuid(), request);
        }

        //[GET("api/Cart/DetailData", RouteName = "CartGetV1aDetail")]
        //[GET("api/V1/Cart/DetailData", RouteName = "CartGetV1bDetail")]
        [HttpGet]
        [Route("api/Cart/DetailData")]
        public GrowerOrderType GetV1Detail([FromUri] string OrderGuid)
        {
            var request = new GetRequestDetailModel();
            request.OrderGuid =  new Guid(OrderGuid);
            request.RequestType = "DetailData";
            var model = new GetResponseDetailModel(GetCurrentUserGuid(), request);
            return model.GrowerOrder;
        }

        //[GET("api/V2/Cart/GrowerOrder/{GrowerOrderGuid:guid}/Detail", RouteName = "CartGetV2Detail")]
        //public GetResponseDetailModel GetV2Detail(Guid growerOrderGuid)
        //{
        //    var request = new GetRequestDetailModel()
        //        {
        //            OrderGuid = growerOrderGuid,
        //            RequestType = "DetailData"
        //        };

        //    return new GetResponseDetailModel(GetCurrentUserGuid(), request);
        //}

        //[GET("api/Cart/SummaryData", RouteName = "CartGetV1aSummary")]
        //[GET("api/V1/Cart/SummaryData", RouteName = "CartGetV1bSummary")]
        //[GET("api/V2/Cart/Summary", RouteName = "CartGetV2Summary")]
        //public GetResponseSummaryModel GetV1Summary([FromUri] GetRequestSummaryModel request)
        //{
        //    request.RequestType = "SummaryData";
        //    return new GetResponseSummaryModel(GetCurrentUserGuid(), request);
        //}

        //[PUT("api/Cart/{GrowerOrderGuid:guid}", RouteName = "CartPutV1a")]
        //[PUT("api/V1/Cart/{GrowerOrderGuid:guid}", RouteName = "CartPutV1b")]
        //[PUT("api/V2/Cart/PlaceOrder/{GrowerOrderGuid:guid}", RouteName = "CartPutPlaceOrderV2a")]
        //public PutResponseModel Put(Guid growerOrderGuid, string userCode)
        //{
        //    return new PutResponseModel(GetCurrentUserGuid(), orderGuid: growerOrderGuid);
        //}

        ////TODO: Add userCode to this API as a parameter.
        //[DELETE("api/Cart/{GrowerOrderGuid:guid}", RouteName = "CartDeleteV1a")]
        //[DELETE("api/V1/Cart/{GrowerOrderGuid:guid}", RouteName = "CartDeleteV1b")]
        //[DELETE("api/V2/Cart/Delete/GrowerOrder/{GrowerOrderGuid:guid}", RouteName = "CartDeleteV2")]
        //public DeleteResponseModel Delete(Guid growerOrderGuid)
        //{
        //    //TODO: Get Rid of the "Unknown" user code (by adding a parm).
        //    return new DeleteResponseModel(GetCurrentUserGuid(), userCode: "Unknown", growerOrderGuid: growerOrderGuid);
        //}
        //[GET("api/Cart/GetPromoCode", RouteName = "GetPromoCode")]
        //public GetPromoCodeResponseModel GetPromoCode([FromUri] GetPromoCodeRequestModel request)
        //{
        //    //public GetResponseGrowerOrderTotalsModel GetGrowerOrderTotals([FromUri] Guid GrowerOrderGuid,string Category, string Form, string ShipWeekString)
        //    //var id = Guid.NewGuid();
        //    //var request = new GetRequestGrowerOrderTotalsModel() {GrowerOrderGuid = GrowerOrderGuid, Category = Category, Form = Form, ShipWeekString = ShipWeekString };
        //    return new GetPromoCodeResponseModel(GetCurrentUserGuid(), request);

        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;
using ePS.Models.GrowerApi;

namespace ePS.Controllers.api
{
    public class OrderPreferencesController : ApiControllerBase
    {

        [GET("api/OrderPreferences/AllowSubstitutions")]
        public object Get()
        {
            GetAllowSubstitutionsResponseModel oResponse = new GetAllowSubstitutionsResponseModel(GetCurrentUserGuid());
            return oResponse;
        }

        [PUT("api/OrderPreferences/{NewValue:bool}")]
        public object Put(bool newValue)
        {
            var currentValue = newValue;
            var AllowSubstitutionsRequest = new PutAllowSubstitutionsRequestModel();
           
            PutAllowSubstitutionsResponseModel  oResponse = new PutAllowSubstitutionsResponseModel(GetCurrentUserGuid(), currentValue, AllowSubstitutionsRequest);
            return oResponse;
        }

    }
}

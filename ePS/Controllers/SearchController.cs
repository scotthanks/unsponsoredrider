﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ePS.Controllers
{
    public class SearchController : Controller
    {
        const string SUB_DOMAIN_DEVELOPMENT = "dev";

        public ActionResult Index( string  SearchTerm)
        {
            ViewBag.Title = "eCommerce for the professional grower | Search Result";
            ViewBag.SearchTerm = SearchTerm ?? string.Empty;
            ViewBag.ShowTestInfo = (SUB_DOMAIN_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());
            return View("Search");
        }


        public ActionResult Editor() {

            var runningInDev = (SUB_DOMAIN_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());

            if (!runningInDev) { return HttpNotFound("Editor is only available in Development."); };
        
            return View();
        
        }

    }
}

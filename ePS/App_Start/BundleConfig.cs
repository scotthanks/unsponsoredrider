﻿using System.Web;
using System.Web.Optimization;

namespace ePS
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            
            /////// Page Bundles  ////////////
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        new string[] {
                        "~/Scripts/modernizr.custom.*"
                        }));
            
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            new string[] {
            //            "~/Scripts/modernizr-*"
            //            }));

            bundles.Add(new ScriptBundle("~/bundles/PageScripts/Availability").Include(
                        new string[] {
                            "~/Scripts/numeral.min.js",
                            "~/Scripts/PageScripts/AvailabilitySearch.js"
                        }));

            bundles.Add(new ScriptBundle("~/bundles/PageScripts/Availability3").Include(
            new string[] {
                            
                          "~/Scripts/numeral.min.js",
                            "~/Scripts/jquery.scrollTo.js",
                            "~/ViewModels/Availability3.js",
                           
                            "~/Scripts/chosen.jquery.js"
                            //,
                            //"~/Scripts/chosen.proto.js"
                            //"~/Scripts/MultiSelect.js",
                           // "~/Scripts/multiselect.css"
                            //,
                            //"~/Scripts/jquery-1.11.3.min.js",
                            //"~/Scripts/jquery.mobile-1.4.5.min.js"
                        }));


            bundles.Add(new ScriptBundle("~/bundles/PageScripts/Catalog").Include(
                        new string[] {
                            "~/Scripts/numeral.min.js",
                            "~/Scripts/PageScripts/CatalogSearch.js"
                        }));

            bundles.Add(new ScriptBundle("~/bundles/PageScripts/OrdersPDF").Include(
                        new string[] {
                            "~/ViewModels/OrdersPDF.js"
                        }));
            bundles.Add(new ScriptBundle("~/bundles/PageScripts/Orders").Include(
                        new string[] {
                            "~/Scripts/numeral.min.js",
                            "~/Scripts/jquery.maskedinput.min.js",
                            "~/Scripts/eps_expanse.js",
                            //"~/Scripts/jspdf/jspdf.debug.js",
                            "~/ViewModels/Orders.js",
                            "~/ViewModels/OrdersOpen.js",
                            "~/ViewModels/OrdersShipped.js",
                            "~/ViewModels/OrdersCancelled.js"
                        }));

            bundles.Add(new ScriptBundle("~/bundles/PageScripts/Cart").Include(
                        new string[] {
                            "~/Scripts/numeral.min.js",
                            "~/Scripts/jquery.maskedinput.min.js",
                            "~/ViewModels/Cart2.js"
                        }));

            bundles.Add(new ScriptBundle("~/bundles/PageScripts/TransactionSummary").Include(
                        new string[] {
                            "~/Scripts/numeral.min.js",
                            "~/Scripts/jquery.maskedinput.min.js",
                            "~/Scripts/PageScripts/TransactionSummary.js"
                        }));

            bundles.Add(new ScriptBundle("~/bundles/PageScripts/PaymentOptions").Include(
                        new string[] {
                            "~/Scripts/numeral.min.js",
                            "~/Scripts/eps_expanse.js",
                            //"~/Scripts/jquery.maskedinput.min.js",
                            "~/Scripts/PageScripts/PaymentOptions.js"
                        }));
            bundles.Add(new ScriptBundle("~/bundles/PageScripts/ManageAddresses").Include(
                        new string[] {
                            "~/Scripts/numeral.min.js",
                            "~/Scripts/eps_expanse.js",
                            //"~/Scripts/jquery.maskedinput.min.js",
                            "~/Scripts/PageScripts/ManageAddresses.js"
                        }));
            bundles.Add(new ScriptBundle("~/bundles/PageScripts/Users").Include(
                        new string[] {
                            "~/Scripts/jquery.maskedinput.min.js",
                            "~/Scripts/numeral.min.js",
                            "~/Scripts/eps_expanse.js",
                            "~/Scripts/PageScripts/Users.js"
                        }));
            bundles.Add(new ScriptBundle("~/bundles/PageScripts/AccountSettings").Include(
                      new string[] {
                            "~/Scripts/jquery.maskedinput.min.js",
                            "~/Scripts/numeral.min.js",
                            "~/Scripts/eps_expanse.js",
                            "~/Scripts/PageScripts/AccountSettings.js"
                        }));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        new string[] {
                            "~/Scripts/jquery-{version}.js",
                            "~/Scripts/jquery-{version}.intellisense.js"
                        }));

            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            new string[] {
            //                "~/Scripts/jquery-{version}.js"
            //            }));

            bundles.Add(new ScriptBundle("~/bundles/siteJs").Include(
                        new string[] {
                            "~/Scripts/jquery.hoverIntent.js",
                            "~/Scripts/site.js",
                            "~/Scripts/layout.js",
                            "~/Scripts/SimpleMembership.js",
                            "~/Scripts/eps_tools.js"
                        //   , "~/Scripts/eps_search.js"
                        }));
            



            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js",
            //            "~/Scripts/jquery-{version}.intellisense.js",
            //            "~/Scripts/jquery-migrate-{version}.js"
            //            ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        new string[] {
                            "~/Scripts/jquery-ui-{version}.js"      
                        }));

            
            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.unobtrusive*",
            //            "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive-ajax.min.js",
                        "~/Scripts/jquery.validate.js"
                        ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
            

            //have not been able to figure out why this will not work
            bundles.Add(new StyleBundle("~/bundles/css").Include(
                        new string[] {
                            "~/Content/styles/site.css",
                            "~/Content/styles/layout.css",
                            "~/Content/styles/PopUpLogIn.css",
                            "~/Content/styles/eps_tools.css"
                           //, "~/Content/styles/eps_search.css"
                    }));

            //BundleTable.EnableOptimizations = true;
            BundleTable.EnableOptimizations = false;

        }
    }
}
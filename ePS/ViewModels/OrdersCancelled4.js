﻿
model.cancelled.SummaryData = [];
model.cancelled.DetailData = [];
model.cancelled.OrderHistoryData = {};


model.cancelled.config = {
    urlOrderApi: '/api/order',
    urlGetSummaryData: "/api/Order/MyCancelledOrders",
    urlGetDetailData: function (guid) { return model.cancelled.config.urlOrderApi + "/DetailData?OrderGuid3=" + guid },
};

model.cancelled.state = {
    currentGrowerOrderGuid: function () { return model.cancelled.DetailData.OrderGuid || ""; },
    growerOrdersCount: function () {
        return model.cancelled.SummaryData.length || 0;
    },
};

model.cancelled.getSummaryData = function (callbacks) {
    $.ajax({
        datatype: "json",
        url: model.cancelled.config.urlGetSummaryData + "?TheType4=Cancel",
        type: "get",
        beforeSend: function (jqXHR, settings) {
            model.cancelled.SummaryData = [];
        },
        success: function (response, textStatus, jqXHR) {
            model.cancelled.SummaryData = response.SummaryData || [];
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            vModel.ProcessEventChain(callbacks);
        }
    });
};
model.cancelled.getDetailData = function (guid, callbacks) {
    model.cancelled.DetailData = {};
    model.cancelled.DetailData.OrderGuid = guid;
    $.ajax({
        datatype: "json",
        url: model.cancelled.config.urlGetDetailData(guid),
        type: "get",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
        },
        success: function (response, textStatus, jqXHR) {
            model.cancelled.DetailData = response;
            //alert(JSON.stringify(model.cancelled.DetailData));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            //alert(callbacks);
            vModel.ProcessEventChain(callbacks);
        }
    });


};


model.cancelled.returnSupplierOrder = function (supplierOrderGuid) {
    if (typeof supplierOrderGuid !== 'string' || supplierOrderGuid.length != 36) { return false; }
    var supplierOrder = null;
    $.each(model.cancelled.DetailData.SupplierOrders, function (i, order) {
        if (order.SupplierOrderGuid == supplierOrderGuid) { supplierOrder = order; return false };
    });
    return supplierOrder;
};

model.cancelled.ReturnSelectedTagRatio = function (supplierOrderGuid) {
    if (typeof supplierOrderGuid !== 'string' || supplierOrderGuid.length != 36) { return false; }
    var selectedTagRatio = null;
    var supplierOrder = model.cancelled.returnSupplierOrder(supplierOrderGuid);
    if (supplierOrder == null) { return null; };

    $.each(supplierOrder.TagRatioList, function (i, tagRatio) {
        //    //alert(tag.Guid);
        if (tagRatio.IsSelected) {
            selectedTagRatio = tagRatio.Name;
            return false;
        } else if (selectedTagRatio == null) {
            if (i == 0 || tagRatio.IsDefault) { selectedTagRatio = tagRatio.Name; };
        };
    });


    return selectedTagRatio;
};

model.cancelled.ReturnSelectedShipMethod = function (supplierOrderGuid) {
    if (typeof supplierOrderGuid !== 'string' || supplierOrderGuid.length != 36) { return false; }
    var selectedShipMethod = null;
    var supplierOrder = model.cancelled.returnSupplierOrder(supplierOrderGuid);
    if (supplierOrder == null) { return null; };

    $.each(supplierOrder.ShipMethodList, function (i, shipMethod) {
        //alert(shipMethod.Name);
        if (shipMethod.IsSelected) {
            selectedShipMethod = shipMethod.Name;
            return false;
        } else if (selectedShipMethod == null) {
            if (i == 0 || shipMethod.IsDefault) { selectedShipMethod = shipMethod.Name; };
        };

    });

    return selectedShipMethod;
};



vModel.cancelled.config = {
    containerId: "cancelled_orders_summary_container",
    summaryGridRowClass: "summary_grid_rows",

    summaryGrid: {
        id: "cancelled_order_summary_grid",
        rowClass: "order_summary_grid_rows",
        Columns: 8,     //ToDo: move to state and calculate from template during init
        detailRowId: "order_detail_row",
        detailContainerId: "cancelled_order_detail_container",
    },

    detailDisplay: {
        templateId: "cancelled_orders_detail_display_template",
        template: null,
        id: "cancelled_orders_detail_display",
        headerBarClass: "header_bars",
        cancelledOrderDetailHeaderBarId: "cancelled_order_detail_header_bar",

        orderStatusId: "cancelled_order_status_span",
        orderNumberId: "cancelled_order_number_span",
        purchaseOrderSpanId: "cancelled_order_purchase_order_span",
        orderDescriptionSpanId: "cancelled_order_description_span",

       
        ShipToIdStem: "ship_to_span_",
        PaymentMethodIdStem: "payment_method_span_",
        cancelledOrderHistoryContainerId: "cancelled_orders_history_container",

        totalTraysSpanId: "grower_order_total_trays_span",

        totalCuttingsSpanId: "grower_order_total_cuttings_span",
        totalCuttingsCostSpanId: "grower_order_total_cuttings_cost_span",
        orderTotalsGrid: {
            id: "shipped_orders_grower_order_totals_table",

            growerOrderFeesRowTemplateId: "shipped_orders_grower_order_fees_row_template",
            growerOrderFeesRowTemplate$: null,

            growerOrderFeeInfoSpans: "grower_order_fee_info_icons",
            growerOrderFeeStatusDescription: "grower_order_fee_status_description",
            growerOrderFeeTypeDescription: "grower_order_fee_type_description",
            growerOrderFeeSpansClass: "grower_order_fee_spans",

            totalTraysSpanId: "grower_order_total_trays_span",
            totalCuttingsSpanId: "grower_order_total_cuttings_span",
            totalCuttingsCostSpanId: "grower_order_total_cuttings_cost_span",


        },

    },

    SupplierOrders: {
        templateId: "cancelled_orders_supplier_orders_template",
        template: null,
        ContainerId: "cancelled_order_supplier_orders_container",

        displaysClass: "supplier_order_displays",
        SupplierNameSpanClass: "supplier_name_span",

        tagRatioDisplayClass: "supplier_order_tag_ratio_display",
        tagRatioSelectedClass: "tagRatioSelectedSpans",
        shipMethodDisplayClass: "supplier_order_ship_method_display",
        shipMethodSelectedClass: "shipMethodSelectedSpans",


        LineItemGrid: {
            ClassName: "line_item_grids",
            rowClass: "line_item_grid_rows",
            columns: 8,




            //OrderQuantityLockedClass: "line_item_grid_order_quantity_locked_inputs",
            //OrderQuantityInputClass: "line_item_grid_order_quantity_inputs",

            //orderTraysTotalClass: "supplier_order_tray_totals",
            //orderQuantityTotalClass: "supplier_order_quantity_totals",
            //orderLineStatusesClass: "order_line_statuses",

            //OrderQuantityImgClass: "line_item_grid_order_quantity_imgs",

            //priceTotalsClass: "supplier_order_price_totals",

            //IdStemHeaderRows: "headerRow_",
            //ClassStemHeaderCols: "headerCol_",
            //IdStemFooterRows: "footerRow_",
            //ClassStemFooterCols: "footerCol_",
            //ShowHeadings: true,
            //Headings: ["Species", "Product", "Trays--", "Quantity", "Unit Price", "Price", "Status", "Cancel"],   //deprecated
            //ColumnWidths: ["12.5%", "12.5%", "12.5%", "12.5%", "12.5%", "12.5%", "12.5%", "12.5%"],
            //FooterRows: 1,
            //ColsClassStemHeader: "headerCol_",
            //ColumnClassStemBody: "bodyCol_",
            //ColumnClassStemFooter: "footerCol_",
            //ToggleRemoveSpanClass: "toggle_remove_spans",
            //ToggleRemoveSelectedClass: "toggle_remove_selected",
            //ToggleLockedRowClass: "toggle_locked_selected",
            //removeEnabledRowsClass: "remove_enabled_rows",



            //cancellableRows: "cancellable_rows",
            //uncancelledRows: "un_cancelled_rows",
            //cancelledRows: "cancelled_rows",
            //lockedForChangesRows: "locked_for_changes_rows",
            //lockedForReductionRows: "locked_for_reduction_rows",
            //cancelSpansClass: "cancel_spans_class",


        },
    },

};

vModel.cancelled.state = {
    displayedOrderGuid: function () { return $("#" + vModel.cancelled.config.summaryGrid.detailContainerId).data("orderGuid") || ""; },
};

vModel.cancelled.init = function () {

    ////Clone and delete supplier order dynamic cost row from detail display template
    //var dynamicCostRowsTemplate = $("#" + vModel.cancelled.config.SupplierOrders.LineItemGrid.dynamicCostRowsTemplateId);
    ////console.log(dynamicCostRowsTemplate);
    //vModel.cancelled.config.SupplierOrders.LineItemGrid.dynamicCostRowsTemplate$ = dynamicCostRowsTemplate.clone(true); //store the dynamic cost rows template
    //dynamicCostRowsTemplate.remove();
    ////console.log(vModel.shipped.config.SupplierOrders.LineItemGrid.dynamicCostRowsTemplate$);

    ////Clone and delete grower order dynamic cost row from detail display template
    //var growerOrderFeesRowTemplate = $("#" + vModel.cancelled.config.detailDisplay.orderTotalsGrid.growerOrderFeesRowTemplateId);
    //vModel.cancelled.config.detailDisplay.orderTotalsGrid.growerOrderFeesRowTemplate$ = growerOrderFeesRowTemplate.clone(true); //store the dynamic row template
    //growerOrderFeesRowTemplate.remove();
    ////alert(vModel.shipped.config.detailDisplay.orderTotalsGrid.growerOrderFeesRowTemplate$.length);

    //Clone and Delete Supplier Order Template From Cancelled Order Detail Template
    var supplierOrdertemplate = $("#" + vModel.cancelled.config.SupplierOrders.templateId)
    //supplierOrdertemplate.find("." + vModel.cancelled.config.SupplierOrders.LineItemGrid.ClassName + " tbody").empty();
  
    vModel.cancelled.config.SupplierOrders.template = supplierOrdertemplate.clone(true);;
    supplierOrdertemplate.remove();

    //Clone and delete detail display template
    var detailDisplayTemplate = $("#" + vModel.cancelled.config.detailDisplay.templateId);
    detailDisplayTemplate.prop({ "id": vModel.cancelled.config.detailDisplay.id });
    vModel.cancelled.config.detailDisplay.template = detailDisplayTemplate.clone(true);
    detailDisplayTemplate.remove();


    var onExpandCancelledSummary = function (containerId) {
       // alert("expand");
        eps_expanse.state.setWorking(containerId);
        var display = eps_expanse.state.getDisplayElement(containerId).empty();
        model.cancelled.getSummaryData([function () {
         //   alert("get Summary");
            if (model.cancelled.state.growerOrdersCount() == 0) {
                //park display
                eps_expanse.setStatus(containerId, "Cancelled Orders: 0");
                eps_expanse.state.setCollapsed(containerId);
            } else {
                var table = vModel.config.ordersSummaryTable$.clone(true);
                table.prop({
                    "id": "cancelled_orders_summary_grid"
                });
                display.html(table);
                table.children("tbody").replaceWith(vModel.cancelled.composeSummaryRows());
                eps_expanse.setStatus(containerId, "Cancelled Orders: " + model.cancelled.state.growerOrdersCount());
                eps_expanse.expand(containerId, null);
            };
        }]);
    };
    var onCollapseCancelledSummary = function (containerId) {
       // alert("collaspse");
        eps_expanse.state.setWorking(containerId);
        eps_expanse.collapse(containerId, null, true);
    };
    eps_expanse.init(vModel.cancelled.config.containerId, onExpandCancelledSummary, onCollapseCancelledSummary, "Cancelled Orders", "Searching...");

    if (vModel.config.PageMode == "all" || vModel.config.PageMode == "cancelled") {
        var containerId = vModel.cancelled.config.containerId;
        onExpandCancelledSummary(containerId);
    } else{

        model.cancelled.getSummaryData([function () {
            eps_expanse.setStatus(vModel.cancelled.config.containerId, "Cancelled Orders: " + model.cancelled.SummaryData.length);
        }]);
    };
};

vModel.cancelled.composeSummaryRows = function () {
   // alert("compose");
    var tbody = $("<tbody />");
    $.each(model.cancelled.SummaryData, function (i, item) {
        tbody.append(vModel.composeSummaryRow(item));
    });
    vModel.cancelled.defineSummaryRowEvents(tbody);

    return tbody;


};

vModel.cancelled.defineSummaryRowEvents = function (tbody) {
   // alert(tbody.length);
    var rows = tbody.children("tr." + vModel.cancelled.config.summaryGrid.rowClass);
  //  alert(rows.length);
    $(rows).on("click", function () {
        var orderGuid = $(this).prop("id");
        vModel.cancelled.RemoveDetail([
            function () {
                vModel.cancelled.insertDetailDisplay(orderGuid);
            }
        ]);
    });
};

vModel.cancelled.RemoveDetail = function (callbacks) {
    //clear detail display
    var currentDetailContainer = $("#" + vModel.cancelled.config.summaryGrid.detailContainerId)
    var currentlyDisplayedOrderGuid = currentDetailContainer.data("orderGuid") || "";
    //alert("currentlyDisplayedOrderGuid: " + currentlyDisplayedOrderGuid);
    if (currentlyDisplayedOrderGuid != "") {
        var displayRow = $("#" + vModel.cancelled.config.summaryGrid.detailRowId);
        var detailDisplay = $("#" + vModel.cancelled.config.detailDisplay.id);

        detailDisplay.hide("blind", {}, 500, function () {
            //restore row icon
            $("#" + currentlyDisplayedOrderGuid + " td.td_0").css("background-image", "url('/Images/icons/ArrowRightCart.gif')");
            detailDisplay.remove();
            displayRow.remove();
        });
    };
    //clear data
    model.cancelledDetailData = {};
    //alert(callbacks);
    vModel.ProcessEventChain(callbacks);
};

vModel.cancelled.insertDetailDisplay = function (orderGuid) {


    var currentDetailContainer = $("#" + vModel.cancelled.config.summaryGrid.detailContainerId)
    var currentlyDisplayedOrderGuid = currentDetailContainer.data("orderGuid") || "";
    if (orderGuid != currentlyDisplayedOrderGuid) {
        $("#" + orderGuid + " td.td_0").css("background-image", "url('/Images/icons/Loading_Availability.gif')");
        //alert("row clicked [ Order Guid: " + orderGuid + " ]");

        //insert row in grid spanning all columns
        var row = $("<tr></tr>")
            .prop({ "id": vModel.cancelled.config.summaryGrid.detailRowId })
            .css({ "display": "none", "border": "none" });

        var td = $("<td></td>")
            .prop({ "colspan": vModel.cancelled.config.summaryGrid.Columns });

        var div = $("<div />")
            .prop({ "id": vModel.cancelled.config.summaryGrid.detailContainerId })
            .data("orderGuid", orderGuid);

        td.append(div);
        row.append(td);

        row.insertAfter("#" + orderGuid);
        model.cancelled.getDetailData(orderGuid, [function () {
            //alert("Orders: " + model.cancelled.state.growerOrdersCount());

            vModel.cancelled.displayDetail(row);

        }]);

    };



};

vModel.cancelled.displayDetail = function (row) {
    var target = $("#" + vModel.cancelled.config.summaryGrid.detailContainerId);
    var detailDisplay = vModel.cancelled.config.detailDisplay.template.clone(true);
    target.append(detailDisplay);
    vModel.cancelled.refreshDetailDisplay();
    //detailRow = $("#" + vModel.cancelled.config.summaryGrid.detailRowId);
    row.fadeIn("slow");
}

vModel.cancelled.refreshDetailDisplay = function () {
    

    $("#" + vModel.cancelled.config.detailDisplay.cancelledOrderDetailHeaderBarId).text(
    (model.cancelled.DetailData.ProgramTypeName || null) + " - " +
    (model.cancelled.DetailData.ProductFormCategoryName || null) + " - Week " +
    vModel.ShipWeekCodeToShipWeekString(model.cancelled.DetailData.ShipWeekCode)
    );


    //set status:
    $("#" + vModel.cancelled.config.detailDisplay.orderStatusId).text(model.cancelled.DetailData.GrowerOrderStatusName || '');
    //set order number
    $("#" + vModel.cancelled.config.detailDisplay.orderNumberId).text(model.cancelled.DetailData.OrderNo || '');
    //set PO
    $("#" + vModel.cancelled.config.detailDisplay.purchaseOrderSpanId).text(model.cancelled.DetailData.CustomerPoNo || '');
    //set Description
    $("#" + vModel.cancelled.config.detailDisplay.orderDescriptionSpanId).text(model.cancelled.DetailData.OrderDescription || '');


    //set ship-to
    var selectedShipTo = (function () {
        var shipToAddresses = model.cancelled.DetailData.ShipToAddresses || [];
        var shipToAddress = null;
        for (var i = 0, len = shipToAddresses.length; i < len; i++) {
            shipToAddress = shipToAddresses[i];  //take first address found
            if (shipToAddresses[i].IsSelected) {
                return shipToAddresses[i];  //object found return it
            } else if (shipToAddresses[i].IsDefault) {
                shipToAddress = shipToAddresses[i];  //update default to this
            };
        }
        return shipToAddress; //not found
    })();

  //  alert(selectedShipTo.Name);

    if (selectedShipTo !== null) {
        $("#" + vModel.cancelled.config.detailDisplay.ShipToIdStem + "0").text(selectedShipTo.Name || '');
        //address line 1
        var t = $("#" + vModel.cancelled.config.detailDisplay.ShipToIdStem + "1");
        var v = selectedShipTo.StreetAddress1 || null;
        if (v != null) { t.text(v); } else { t.remove(); };
        //address line 2
        t = $("#" + vModel.cancelled.config.detailDisplay.ShipToIdStem + "2");
        v = selectedShipTo.StreetAddress2 || null;
        if (v != null) {
            t.text(v);
        } else {
            $(t).parents("tr:first").remove();
        };
        //city, state zip        
        $("#" + vModel.cancelled.config.detailDisplay.ShipToIdStem + "3").text(
            (selectedShipTo.City || null) + ", " +
            (selectedShipTo.StateCode || null) + " " +
            (selectedShipTo.ZipCode || null)

            );
        $("#" + vModel.cancelled.config.detailDisplay.ShipToIdStem + "4").text(selectedShipTo.Country || null);
    }


    //set payment method
    var selectedPaymentMethod = (function () {
        var method = {};
        method.Type = null;
        method.Card = null;
        $.each(model.cancelled.DetailData.PaymentTypeList, function (i, type) {
            if ((type.IsDefault && method.Type == null) || type.IsSelected) {
                method.Type = type.DisplayName;
            };
        });
        if (method.Type == "Credit Card") {
            $.each(model.cancelled.DetailData.CreditCards, function (i, card) {
                if ((card.IsDefault && method.Card == null) || card.IsSelected) {
                    method.Card = card.CardType + " ending: " + card.CardNumber
                };
            });
        };
        return method; //not found
    })();
    if (selectedPaymentMethod != null) {
        //alert(selectedPaymentMethod.Type);
        $("#" + vModel.cancelled.config.detailDisplay.PaymentMethodIdStem + "0").text(selectedPaymentMethod.Type || '');
        $("#" + vModel.cancelled.config.detailDisplay.PaymentMethodIdStem + "1").text(selectedPaymentMethod.Card || '');
    };


    //Initialize Order History
    var onExpandOrderHistory = function (containerId) {
        eps_expanse.state.setWorking(containerId);
        eps_expanse.setStatus(containerId, "Loading ...");
        var display = eps_expanse.state.getDisplayElement(containerId).empty();



        var orderGuid = vModel.cancelled.state.displayedOrderGuid();
        var onSuccess = function (data) {
            model.cancelled.OrderHistoryData = data;
        };

        var uri = model.config.urlGetDetailHistoryData(orderGuid);
        model.cancelled.OrderHistoryData = {};
        model.ajaxCall(null, uri, "get", onSuccess, [function () {
            var historyData = model.cancelled.OrderHistoryData.eventList || [];
            vModel.composeHistoryData(display, orderGuid, historyData);
            eps_expanse.setStatus(containerId, "Changes: " + historyData.length);
            eps_expanse.expand(containerId, null);
        }]);

    };
    var onCollapseOrderHistory = function (containerId) {
        eps_expanse.state.setWorking(containerId);
        eps_expanse.collapse(containerId, null, true);
        eps_expanse.setStatus(containerId, "...");
    };
    eps_expanse.init(vModel.cancelled.config.detailDisplay.cancelledOrderHistoryContainerId, onExpandOrderHistory, onCollapseOrderHistory, "Order History", "...");


    //Clear Supplier Orders from Detail Display
    $("#" + vModel.cancelled.config.SupplierOrders.containerId).empty();



    //Add Supplier Orders
    $.each(model.cancelled.DetailData.SupplierOrders, function (i, order) {
        var clone = vModel.cancelled.config.SupplierOrders.template.clone(true);
        clone.prop({
            "id": order.SupplierOrderGuid
        });
        clone.find("." + vModel.cancelled.config.SupplierOrders.SupplierNameSpanClass).text(order.SupplierName);

        ////// Tag Ratio ////////
        var tagRatioDisplay = $(clone).find("." + vModel.cancelled.config.SupplierOrders.tagRatioDisplayClass)
        $(tagRatioDisplay).children("." + vModel.cancelled.config.SupplierOrders.tagRatioSelectedClass).text(model.cancelled.ReturnSelectedTagRatio(order.SupplierOrderGuid));

        //// Ship Method /////
        var shipMethodDisplay = $(clone).find("." + vModel.cancelled.config.SupplierOrders.shipMethodDisplayClass)
        $(shipMethodDisplay).children("." + vModel.cancelled.config.SupplierOrders.shipMethodSelectedClass).text(model.cancelled.ReturnSelectedShipMethod(order.SupplierOrderGuid));

        ///// Line Item Grid: Body Section /////
        var theDiv = $("<div />");
        var thrDivProperties = { "class": "line_item_grids" };
        theDiv.prop(thrDivProperties);



        var orderDiv = $("<div />");
        var orderDivProperties = { "class": "cart_line_item" };
        orderDiv.prop(orderDivProperties);

        vModel.addSupplierOrderLinesToDiv(orderDiv, order);


        theDiv.append(orderDiv);

        var productTotalsDiv = $("<div />");
        var productTotalsDivProperties = { "id": "supplier_totals_count" };
        productTotalsDiv.prop(productTotalsDivProperties);

        var ul = $("<ul>");
        var ulProperties = { "id": "supplier_totals_count_list" };
        ul.prop(ulProperties);


        var li = $("<li />");
        var span = $('<span />');
        span.prop({
            "id": order.SupplierOrderGuid + "_supplier_order_tray_totals",
            "class": "supplier_order_tray_totals",
        });
        li.text("Total Trays: ");
        li.append(span);
        ul.append(li);

        var li = $("<li />");
        var span = $('<span />');
        span.prop({
            "id": order.SupplierOrderGuid + "_supplier_order_quantity_totals", "class": "supplier_order_quantity_totals",
        });
        li.text("Total Qty: ");
        li.append(span);
        ul.append(li);


        productTotalsDiv.append(ul);
        //theDiv.append(productTotalsDiv);

        var supplierSubtotalsDiv = $("<div />");
        var supplierSubtotalsDivProperties = { "id": "supplier_subtotals" };
        supplierSubtotalsDiv.prop(supplierSubtotalsDivProperties);

        var ul = $("<ul>");
        var ulProperties = {
            "id": "supplier_product_subtotal",
            "class": "supplier_subtotal_boxes"
        };
        ul.prop(ulProperties);

        var li = $("<li />");
        var liProperties = { "class": "left" };
        var span = $('<span />');
        span.prop({
            "class": "supplier_name_span2",
            "id": order.SupplierOrderGuid + "_supplier_name_span"
        });
        li.append(span);
        ul.append(li);

        var li = $("<li />");
        var liProperties = { "class": "right" };
        var span = $('<span />');
        span.prop({
            "id": order.SupplierOrderGuid + "_supplier_order_product_cost_totals"
            , "class": "supplier_order_product_cost_totals",
        });
        li.append(span);
        ul.append(li);
        supplierSubtotalsDiv.append(ul);


        $(clone).children("#supplier_fees").remove();
        var supplieFeesDiv = $("<div />");
        var supplieFeesDivProperties = { "id": order.SupplierOrderGuid + "_supplier_fees" };
        supplieFeesDiv.prop(supplieFeesDivProperties);
        // supplieFeesDiv.html("<p>hi Scott</p>");
        supplierSubtotalsDiv.append(supplieFeesDiv);


        $(clone).children("#supplier_totals").remove();
        var supplieSubtotalsDiv = $("<div />");
        var supplieSubtotalsDivProperties = { "id": order.SupplierOrderGuid + "_supplier_totals" };
        supplieSubtotalsDiv.prop(supplieSubtotalsDivProperties);
        // supplieFeesDiv.html("<p>hi Scott</p>");
        supplierSubtotalsDiv.append(supplieSubtotalsDiv);



        $(clone).children("." + vModel.cancelled.config.SupplierOrders.LineItemGrid.ClassName).replaceWith(theDiv);
        $(clone).children("#supplier_totals_count").replaceWith(productTotalsDiv);
        $(clone).children("#supplier_subtotals").replaceWith(supplierSubtotalsDiv);



        $("#" + vModel.cancelled.config.SupplierOrders.ContainerId).append(clone);
    });

    vModel.refreshTotalsCascade("cancelled", null);  //null value means refresh all supplier orders
    $("#" + model.cancelled.state.currentGrowerOrderGuid() + " td.td_0").css("background-image", "url('/Images/icons/ArrowDownCart.gif')");
   


};





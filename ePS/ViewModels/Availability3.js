﻿
///////////////////////////////////////////// Model ///////////////////////////////////////////////////
var model = new Object;
model.config = {
  
    sorts: ["Supplier", "Species", "Product", "Form", "Price", "Quantity"],
    sortedDirection: 1,    //0 -> unsorted, >0 -> ascending, <0 -> descending for currently sorted column
    sortedColumnNdx: 1,     //data comes from server sorted by Species
    sortFunctions: [
        function (a, b) {
            if (a.Supplier != b.Supplier) {
                if (a.Supplier > b.Supplier) { return 1 * model.config.sortedDirection; };
                return -1 * model.config.sortedDirection;
            };
            if (a.Species != b.Species) {
                if (a.Species > b.Species) { return 1 };
                return -1;
            };
            if (a.Product != b.Product) {
                if (a.Product > b.Product) { return 1 };
                return -1;
            };
            return 0;
        },
        function (a, b) {
            if (a.Species != b.Species) {
                if (a.Species > b.Species) { return 1 * model.config.sortedDirection; };
                return -1 * model.config.sortedDirection;
            };
            if (a.Product != b.Product) {
                if (a.Product > b.Product) { return 1 };
                return -1;
            };
            return 0;
        },
        function (a, b) {
            if (a.Product > b.Product) { return 1 * model.config.sortedDirection; };
            return -1 * model.config.sortedDirection;
            return 0;
        },
        function (a, b) {
            if (a.Form != b.Form) {
                if (a.Form > b.Form) { return 1 * model.config.sortedDirection; };
                return -1 * model.config.sortedDirection;
            };
            if (a.Species != b.Species) {
                if (a.Species > b.Species) { return 1 };
                return -1;
            };
            if (a.Product != b.Product) {
                if (a.Product > b.Product) { return 1 };
                return -1;
            };
            return 0;
        },
        function (a, b) {
            if (a.Price != b.Price) {
                return (a.Price - b.Price) * model.config.sortedDirection;
            };
            if (a.Species != b.Species) {
                if (a.Species > b.Species) { return 1 };
                return -1;
            };
            if (a.Product != b.Product) {
                if (a.Product > b.Product) { return 1 };
                return -1;
            };
            return 0;
        },
        function (a, b) {
            if (a.OrderQuantity != b.OrderQuantity) {
                return (a.OrderQuantity - b.OrderQuantity) * model.config.sortedDirection;
            };
            if (a.Species != b.Species) {
                if (a.Species > b.Species) { return 1 };
                return -1;
            };
            if (a.Product != b.Product) {
                if (a.Product > b.Product) { return 1 };
                return -1;
            };
            return 0;
        }
    ],
    //urlAvailabilityApiCart: "/api/Availability/AvailabilityCart2",
    urlAvailabilityApiCart: "/api/Availability",
    urlAvailabilityByPageApiCart: "/api/Availability/AvailabilityByPage",
    urlAvailabilityCount: "/api/Availability/AvailabilityCount",
    urlAvailabilityApiOrder: "/api/availability",
    urlGetOrderSummaries: "/api/Grower2/GetGrowerOrderSummaries",
    urlShipWeeksApi: "/api/AvailabilityShipweeks",
    urlSupplierApi: "/api/supplier/-1",
    urlSpeciesApi: "/api/species/-1",
    urlSellerApi:"/api/seller/-1",
    urlGrowerTotalsApi: "/api/Grower2/GrowerTotals",
    urlGrowerTotalsPerWeekApi: "/api/Grower2/TotalsPerWeek",
    maxQuantity: 999999,
    pageSize: 25000,
    nextPageToLoad: 1,
    displayPageSize: 100,
    nextPageToDisplay: 1,
    refineSearchExpandedId: "refine_search_expanded",
    //refineSearchDialogId: ""
    //urlAddToCart: "api/availability",
  //  stateCookieName: 'epsavailselections',
  //  stateCookieDurationMinutes: 36 * 60,
};

model.state = {
//    cookie: {
//        category: "",
//        form: "",
//        seller: "",
//        shipWeek: "",
//        suppliers: "",
//        species: "",
//        filter: ""
//},
    loggedIn: false,
    queryString: {
        isValid: function () {
            //true if category and form have string values
            if (this.category().length < 1) return false;
            if (this.form().length < 1) return false;
            return true;
        },             
        isFullVersion: function () {
            //alert(traverseObj(model.state.queryString, false));
            //alert('this.shipWeek().length: ' + this.shipWeek().length);

            //true if isValid and shipWeek, suppliers, and species have string values
            //console.log('valid: ', this.isValid());
            if (!this.isValid()) return false;
            //console.log('shipWeek', this.shipWeek());
            if (this.shipWeek().length != 7) return false;
            if (this.suppliers().length < 1) return false;
            if (this.species().length < 1) return false;
            return true;
        },       
        category: function () { return eps_GetQueryStringValue('Category'); },
        form: function () { return eps_GetQueryStringValue('Form'); },
        shipWeek: function () { return eps_GetQueryStringValue('ShipWeek'); },
        suppliers: function () { return eps_GetQueryStringValue('Suppliers'); },
        species: function () { return eps_GetQueryStringValue('Species'); },
        filter: function () {
          //  alert(eps_GetQueryStringValue('Filter'));
            if (eps_GetQueryStringValue('Filter') === null || eps_GetQueryStringValue('Filter') == "") {
                return true;
            }   
            var x = eps_GetQueryStringValue('Filter') == 'true' ? true : false;
            return x

        },
        variety: function () { return eps_GetQueryStringValue('Variety'); },
        product: function () {return eps_GetQueryStringValue('Product'); },
        OrganicOnly: function () { return eps_GetQueryStringValue('OrganicOnly') == 'true' ? 'true' : 'false'; },
        go: function () {
           // alert(eps_GetQueryStringValue('Go'));
            if (eps_GetQueryStringValue('Go') === null || eps_GetQueryStringValue('Go') == "") {
                return false;
            }
            var x = eps_GetQueryStringValue('Go') == 'true' ? true : false;
            return x

           
        },

    },
    userSelections: {
        ShipWeek: "",
        Suppliers: "",
        Species: ""
    },   
    growerTotals: {},
    growerTotalsPerWeek: {},
    seller: 'EPS',
    rebuildingRowDataIndex: false,
};

model.config.init = function () {
    //alert("hi model.config.init");
    //read current state cookie
    //var value = viewModel.stateCookieRead();
    ////console.log('stateCookieRead: ', value);
    //if (value != null) {
    //    model.state.cookie.category = value.Category || null;
    //    model.state.cookie.form = value.Form || null;
    //    model.state.cookie.seller = value.Seller || null;
    //    model.state.cookie.shipWeek = value.ShipWeek || null;
    //    model.state.cookie.suppliers = value.Suppliers || null;
    //    model.state.cookie.species = value.Species || null;
    //    model.state.cookie.filter = value.Filter || null;
    //    model.state.cookie.organicOnly = value.OrganicOnly || null;
    //} else {
    //    model.state.cookie.category = null;
    //    model.state.cookie.form = null;
    //    model.state.cookie.seller = null;
    //    model.state.cookie.shipWeek = null;
    //    model.state.cookie.suppliers = null;
    //    model.state.cookie.species = null;
    //    model.state.cookie.filter = null;
    //    model.state.cookie.organicOnly = null;
    //};
    //alert(model.state.cookie.species);
    model.state.CurrentSort = "Species";
};

model.rowDataIndex = [];        //Pointers to Filtered, Sorted Row Data
model.rowData = [];
model.rowDataCount = 0;
model.allowOrdering = false;
model.showPriceData = false;
model.shipWeekHeadings = [];
model.shipWeekData = [];
model.supplierData = [];
model.speciesData = [];
model.getSupplierData = function (callbacks) {
    //retrieve supplier data
    model.state.seller = "GFB";
    $.ajax({
        type: "get",
        data: {
            "PlantCategoryCode": model.state.queryString.category(),
            "ProductFormCode": model.state.queryString.form(),
            "SellerCode": model.state.seller
},
        url: model.config.urlSupplierApi,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //console.log('supplierData Request: ', this.url, this.data);

        },
        success: function (response, textStatus, jqXHR) {
            //console.log('SupplierData Response: ', response);
            model.supplierData = response;
        },
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });

};
model.getSpeciesData = function (callbacks) {
    console.log("getSpeciesData");
    //retrieve species data
    model.state.seller = "GFB";
    $.ajax({
        type: "get",
        data: {
            "PlantCategoryCode": model.state.queryString.category(),
            "ProductFormCode": model.state.queryString.form(),
            "SupplierCodes": viewModel.state.selections.getSelectedSuppliersList(),
            "SellerCode": model.state.seller
        },
        url: model.config.urlSpeciesApi,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //console.log('speciesData Request: ', this.url, this.data);

        },
        success: function (response, textStatus, jqXHR) {
            console.log('getSpeciesData Response');
            model.speciesData = response;
           // alert(traverseObj(model.speciesData));

        },

        complete: function (jqXHR, textStatus) {
            console.log('getSpeciesData complete');
            viewModel.ProcessEventChain(callbacks);
        }
    });
};
model.getShipWeekData = function (callbacks) {

   
    
    var SupplierCodes = viewModel.state.selections.getSelectedSuppliersList();
    if (SupplierCodes == ""){
        SupplierCodes = "*"
    }
   // alert(SupplierCodes);

    var SpeciesCodes = viewModel.state.selections.getSelectedSpeciesList();
    if (SpeciesCodes == "") {
        SpeciesCodes = "*"
    }
   // alert(SpeciesCodes);

    var ShipWeek = "202048"
    if (typeof viewModel.state.shipWeekSelected.ShipWeekString !== 'undefined') {
        ShipWeek = viewModel.state.shipWeekSelected.ShipWeekString;
    }

    
   // alert(ShipWeek);


    //retrieve shipweek data
    $.ajax({
        type: "get",
        data: {
            "Category": model.state.queryString.category(),
            "Form": model.state.queryString.form(),
            "SupplierCodes": SupplierCodes,
            "SpeciesCodes": SpeciesCodes,
            "ShipWeek": ShipWeek
            
        },
        url: model.config.urlShipWeeksApi,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
         console.log('shipWeekData Request: ', this.url, this.data);
        },
        success: function (response, textStatus, jqXHR) {
            //console.log('shipWeekData Response: ', response);
            model.shipWeekData = response;
            var x = model.state.queryString.shipWeek();
            if (x !== null) {
                var compare = x.substr(3, 4);
                compare += x.substr(0, 2);
               
            }
            
            if (model.shipWeekData.SelectedShipWeek.Guid == "00000000-0000-0000-0000-000000000000") {
                var code = model.shipWeekData.SelectedShipWeek.Code;
               // alert("code = " + code);
                if (compare > code)
                {
                    code = compare;
                }
                $.each(model.shipWeekData.ShipWeeksList, function (ndx, week) {
                    if (code == week.Code) {
                        model.shipWeekData.SelectedShipWeek = week;
                        return false;
                    }
                });
            };
            
            viewModel.state.shipWeekSelected = model.shipWeekData.SelectedShipWeek;
          //  alert(viewModel.state.shipWeekSelected.ShipWeekString);
        },
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });
};
model.getRowDataCount = function (callbacks) {
    //Make ajax call to retrieve row data
    //alert("get Count")
    model.state.seller = "GFB";
    $("#row_data_count").prop('disabled', 'disabled');
    $.ajax({
        type: "get",
        timeout: "75000",
        data: {
            "ProgramTypeCode": "VA",
            "Category": model.state.queryString.category(),
            "ProductFormCategoryCode": "URC",
            "Form": model.state.queryString.form(),
            "Suppliers": viewModel.state.selections.getSelectedSuppliersList(),
            "Species": viewModel.state.selections.getSelectedSpeciesList(),
            "ShipWeek": viewModel.state.shipWeekSelected.ShipWeekString,
            "OrganicOnly": viewModel.state.filterOrganicOnly,
            "AvailableOnly": viewModel.state.filterImmediateAvailability,
            "SellerCode" : model.state.seller
        },

        url: model.config.urlAvailabilityCount,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //console.log('getRowData Request: ', this.url, this.data);
            model.rowDataCount = 0;
        },
        success: function (response, textStatus, jqXHR) {
            console.log('getRowDataCount Response: ', response);
        //    alert(JSON.stringify(response));
            model.rowDataCount = response.RowDataCount;
           
        },
        complete: function (jqXHR, textStatus) {
            $("#row_data_count").removeProp('disabled');
            viewModel.ProcessEventChain(callbacks);
        }
    });

};

model.getRowDataByPage = function (callbacks) {
    //Make ajax call to retrieve row data
    model.state.seller = "GFB";
    var shipweek = "48|2020";
    if (typeof viewModel.state.shipWeekSelected.ShipWeekString !== 'undefined' && viewModel.state.shipWeekSelected.ShipWeekString !== null)  {
        shipweek = viewModel.state.shipWeekSelected.ShipWeekString;
    }
  
   // alert(shipweek);

    $("#get_row_data").prop('disabled', 'disabled');
    $.ajax({
        type: "get",
        timeout: "75000",
        data: {
            "Category": model.state.queryString.category(),
            "Form": model.state.queryString.form(),
            "Suppliers": viewModel.state.selections.getSelectedSuppliersList(),
            "Species": viewModel.state.selections.getSelectedSpeciesList(),
            "ShipWeek": shipweek,
            "OrganicOnly": viewModel.state.filterOrganicOnly,
            "AvailableOnly": viewModel.state.filterImmediateAvailability,
            "SortOption": "Species",
            "PageSize": model.config.pageSize,
            "PageNumber": model.config.nextPageToLoad,
            "SellerCode" : model.state.seller

        },
        url: model.config.urlAvailabilityByPageApiCart,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //console.log('getRowData Request: ', this.url, this.data);
            model.RowData = [];
            model.shipWeekHeadings = [];
        },
        success: function (response, textStatus, jqXHR) {
           // alert("return");
            console.log('getRowDataByPage Response: ', response);
           // alert(JSON.stringify(response));
            model.rowData = response.RowData;      //only save row data
            model.allowOrdering = response.AllowOrdering;
            model.showPriceData = response.ShowPriceData;
            model.shipWeekHeadings = response.ShipWeekHeadings;
        },
        complete: function (jqXHR, textStatus) {
            $("#get_row_data").removeProp('disabled');
            viewModel.ProcessEventChain(callbacks);
        }
    });

};




model.refreshRowDataIndex = function () {
    //alert("refreshRowDataIndex");
    if (model.state.rebuildingRowDataIndex) { return false; };
    model.state.rebuildingRowDataIndex = true;
    var start = new Date();
    model.rowDataIndex = [];    //clear index
   // alert("one");
    for (var i = 0; i < model.rowData.length; i++){
        if (!viewModel.state.filterImmediateAvailability || model.testRowAvailability(i)) {
            model.rowDataIndex.push(i);
        };
    };
    var target = $("#test_response_data").empty();
    if ((target.length || 0) > 0) {
        target.append($("<p>ET: " + (new Date() - start) + " ms.</p>"));
        target.append($("<p>Count: " + (model.rowDataIndex.length || 0) + " rows.</p>"));
    };
    model.state.rebuildingRowDataIndex = false;
    return model.rowDataIndex.length || 0;
};

model.testRowAvailability = function (ndx) {
    var row = model.rowData[ndx] || null;
    if (row != null) {
        for (var i = 0; i < 5; i++) {
            if (row.DisplayCodes[i] == "OPEN" || row.Availabilities[i] > 0) { return true; };
        };
    };
    return false;
};

model.growerTotalsGet = function (onSuccess) {

     
    var ProgramTypeCode = model.state.queryString.category();
    var ProductFormCategoryCode = model.state.queryString.form();
    //Make ajax call to retrieve row data
    $.ajax({
        type: "get",
        data: {         
            "ProgramTypeCode": ProgramTypeCode,
            "ProductFormCategoryCode": ProductFormCategoryCode
        },

        url: model.config.urlGrowerTotalsApi,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
        },
        success: function (data, textStatus, jqXHR) {
               
                model.state.growerTotals = data;
               // alert(traverseObj(model.state.growerTotals));
                onSuccess();
            
        },
        complete: function (jqXHR, textStatus) {
            //viewModel.ProcessEventChain(callbacks);
        }
    });
};
model.growerTotalsPerWeekGet = function (onSuccess) {

    //Make ajax call to retrieve row data
    $.ajax({
        type: "get",
        data: {
            "ProgramTypeCode2": model.state.queryString.category(),
            "ProductFormCategoryCode2": model.state.queryString.form()
        },

        url: model.config.urlGrowerTotalsPerWeekApi,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
        },
        success: function (data, textStatus, jqXHR) {

            model.state.growerTotalsPerWeek = data;
            console.log(model.state.growerTotalsPerWeek);
            onSuccess();

        },
        complete: function (jqXHR, textStatus) {
            //viewModel.ProcessEventChain(callbacks);
        }
    });
};
model.updateOrderQuantity = function (ndx, newQuantity,i) {
    console.log(traverseObj(model.rowData[ndx])); // alert(traverseObj(model.rowData[ndx]));
    console.log("column: " + i);
    console.log("updateOrderQuantity1 ndx= " + ndx + "newQuantity= " + newQuantity);
    //ToDo: Code to validate inputs before proceeding
    if (typeof ndx == 'undefined' || ndx == null || ndx < 0 || ndx >= model.rowData.length) { return false; };
    console.log("updateOrderQuantity2");
    if (typeof newQuantity == 'undefined' || newQuantity == null || newQuantity < 0 || newQuantity > model.config.maxQuantity) { return false; };
    console.log("updateOrderQuantity3");
    var v = parseInt(newQuantity);
    if (v == null || v < 0) { return false; };
    console.log("updateOrderQuantity4");
    
    console.log("model.rowData[ndx].OrderQuantities[" + i + "]= " + model.rowData[ndx].OrderQuantities[i] + "  v = " + v);
    if (model.rowData[ndx].OrderQuantities[i] == v) { return false; };
    console.log("updateOrderQuantity5");

    model.rowData[ndx].OrderQuantities[i] = v;

    model.rowData[ndx].WeekOffset = i;
  //   alert("Bingo " +  model.rowData[ndx].WeekOffset);
  
    return true;
};
model.rowDataNdxFromProductGuid = function (guid) {
    var returnValue = -1;
    if (guid == null || guid.length != 36) { return returnValue; };
    for (var i = 0; i < model.rowData.length; i++) {
        if (model.rowData[i].ProductGuid == guid) {
            returnValue = i;
            break;
        }
    }
    return returnValue;
};
model.returnAvailabilityIsUnlimited = function (ndx, shipweek) {
    if (typeof ndx == 'undefined' || ndx == null || ndx < 0 || ndx >= model.rowData.length) { return false; };
    var v = model.rowData[ndx].DisplayCodes[shipweek];
    if (v == null) { return false; };
    return (v.toLowerCase() == "open");
};
model.returnAvailability = function (ndx, shipweek) {
   
    if (!model.isValidRowNdx)
    {
        return 0;
    };

    //alert(shipweek);
    return parseInt(model.rowData[ndx].Availabilities[shipweek]) || 0;
};
model.isValidRowNdx = function (ndx) {
    return (typeof ndx != 'undefined' && ndx != null && ndx >= 0 && ndx < model.rowData.length);
};
model.refreshDataRow = function (dataRow) {
    //ToDo: complete this
    //if (dataRow === "undefined" || dataRow == null) { return false; };
    //var ndx = model.rowDataNdxFromProductGuid(dataRow.ProductGuid);
    //if (!model.isValidRowNdx(ndx)) { return false; };
    //model.rowData[ndx].OrderQuantity = dataRow.OrderQuantity;
    //model.rowData[ndx].IsCarted = dataRow.IsCarted;
    //model.rowData[ndx].Price = dataRow.Price;
    //for (var i = 0; i < 5; i++) {
    //    //model.rowData[ndx].Availabilities[i] = dataRow.Availabilities[i];
    //};
};

model.sortOnDataColumn = function(ndx) {
   // alert("in sort");
    //ToDo: validate input
   // if (model.config.sortedColumnNdx != ndx)  //if this is not the current sorted column
   // {
    model.config.sortedDirection = 1;
  //  } else {
        //flip sort direction from current, default to ascending
 //       model.config.sortedDirection = model.config.sortedDirection > 0 ? -1 : 1;
    //   }
        
    var data = model.rowData;       //copy rowData
    //create sortFunction
    var sortFunction = model.config.sortFunctions[ndx];
    data.sort(sortFunction);        //sort data
    model.rowData = data;           //get sorted data

    //ToDo: Sort Criterion and Sort Action should be separate and result only in new rowDataIndex so actual sort could be done with subset of fields and much less data duplication
    model.refreshRowDataIndex();
     
    var scrollTo = model.state.queryString.variety();
    if (scrollTo == "")
    { scrollTo = "none"; }
  //  alert("scrollTo = " + scrollTo);
    viewModel.displayGridRows(true, false, true, scrollTo);   //redisplay grid
    model.config.sortedColumnNdx = ndx;
  //  viewModel.resetSortableColumnHeadingClasses();
};
model.growerGetOrderSumamries = function (onSuccess) {
    console.log(onSuccess);
    model.state.seller = "GFB";
    //Make ajax call to retrieve row data
    $.ajax({
        type: "get",
        data: {
            "ShipWeekCode": "ALLALL",
            "SellerCode": model.state.seller
        },

        url: model.config.urlGetOrderSummaries,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //if (
            //    (targetRequest.length || 0) > 0) {
            //    targetRequest.text("AJAX REQUEST: " + decodeURI(this.url));
            //};
            //alert("AJAX url: " + this.url);
        },
        success: function (data, textStatus, jqXHR) {
            if (typeof onSuccess === "function") {
                onSuccess(data);
            };

        },
        complete: function (jqXHR, textStatus) {
            //viewModel.ProcessEventChain(callbacks);
        }
    });

};



///////////////////////////////////////////////  View Model /////////////////////////////////////////
var viewModel = new Object;
//alert("hi");
viewModel.config = {
    grid: {
        colsHeader:     11,
        colsFooter:     11,
        colsData:       11,
        rowsHeader:     1,
        rowsData:       10,
        rowsFooter:     7,

        headerRowClassStem: "headerRow_",
        headerColClassStem: "headerCol_",
        dataRowIdStem: "dataRow_",
        dataColClassStem: "dataCol_",
        footerRowClassStem: "footerRow_",
        footerColClassStem: "footerCol_",
        shipWeekDisplayId:"ship_week_display_span",
        headingSortableClass: "grid_heading_sortable",
        headingSortableUnsortedClass: "grid_heading_sortable_unsorted",
        headingSortableAscendingClass: "grid_heading_sortable_ascending",
        headingSortableDescendingClass: "grid_heading_sortable_descending"

    },
    icons: {
        cart_checked: function () { return $("<img src=\"../Images/icons/icon_cart.png\" alt=\"\" title=\"\"/>"); },
        not_carted: function () { return $("<img src=\"../Images/icons/greencheck.png\" alt=\"\" title=\"\"/>"); },
        order_working: function () { return $("<img src=\"../Images/icons/loading_availability.gif\" alt=\"\" title=\"\"/>") }

    },
    searchDialog: {
        showoptions: { "direction": "left", "mode": "show" },
        hideoptions: { "direction": "left", "mode": "hide" },
        refineSearchGoId: "refine_search_go",
        refineSearchGoId2: "refine_search_go2",
        refineSearchCloseId: "refine_search_close",
        refineSearchTabId: "refine_search_tab",
        refineSearchDialogId:"refine_search_dialog",
        refineSearchDialogHeaderId: "refine_search_dialog_header",
        filterAvailabilitySelectorId: "filter_availability_check_box",
        filterOrganicSelectorId: "filter_organic_check_box",
        supplierHeaderId: "supplier_header",
        refineSearchExpandedId: "refine_search_expanded",
        speciesHeaderId: "species_header",
        refineSpeciesExpandedId: "refine_species_expanded"
},

    totalResultsId: "total_results_span",

    //ToDo: Move these to grid section above
    orderQuantityInputClass: "order_quantity_inputs",
    orderQuantityNAClass: "order_quantity_NA",
    orderQuantityNoOrderingClass: "order_quantity_no_ordering",
    availabilityGridContainerId: "availability_grid_container",
    outerTableId: "outer_table",
    innerTableId: "inner_table",
    innerTableContainerId: "inner_table_container",
    orderQuantityTotalDisplayId: "order_total_span",
    cartedQuantityDisplayId: "pending_total_span",
    precartQuantityDisplayId: "precart_total_span",
    cartedQuantityDisplayId2: "pending_total_span2",
    precartQuantityDisplayId2: "precart_total_span2",
    addToCartButtonID1: "add_to_cart_button",
    addToCartButtonID2: "add_to_cart_button2",
    addToCartButtonID3: "add_to_cart_button3",
    viewCartButtonID1: "view_cart_button",
    viewCartButtonID2: "view_cart_button2",
    viewCartButtonID3: "view_cart_button3",
    
   // addToCartLinkId: "add_to_cart_link",
    viewCartLinkId: "view_cart_link",
    cboAddToOrder: "cbo_Add_To_Order",
    
    addToOrderDiv: "addToOrderDiv",
    addToOrderLinkId: "add_to_order_link",
    addToOrderList: "add_to_order_List",
  
   
   
    supplierListDisplayId: "refine_supplier_list",
    speciesListDisplayId: "refine_species_list",
    shipWeekSelectorId: "ship_week_selector",
    shipWeekPriorId: "image_ship_week_prior",
    shipWeekNextId: "image_ship_week_next",
    shipWeekListContainerId: "ship_week_list_container",
    shipWeekListDisplayId: "ship_week_list_display",
    shipWeekListId: "ship_week_list",
    shipWeekAvailableClass: "_selection_available_ship_weeks",
    shipWeekSelectedClass: "_selection_selected_ship_weeks",
    selectorAllSuppliersId: "selector_all_suppliers",
    selectorAllSpeciesId: "selector_all_species",
    selectionAvailableClass: "selection_available",
    selectionSelectedClass: "selection_selected",
    selectionDisabledClass: "selection_disabled",

};
viewModel.state = {
    
    type: {
        stateObj: { 'stateObjName': 'stateObjValue' },
        title:null,
        url: null,
        queryString: null,
    },
    selections: {
        selectedSuppliersCount: function(){
            var list = $("#" + viewModel.config.supplierListDisplayId + " ul")
            list = list.children("li." + viewModel.config.selectionSelectedClass + ":not(#" + viewModel.config.selectorAllSuppliersId + ")");
            return (list.length) || 0;
        },
        selectedSpeciesCount: function () {
            var list = $("#" + viewModel.config.speciesListDisplayId + " ul")
            list = list.children("li." + viewModel.config.selectionSelectedClass + ":not(#" + viewModel.config.selectorAllSpeciesId + ")");
            return (list.length) || 0;
        },
        getSelectedCategoryCode: function () {
            //var value = model.state.queryString.category();
            return model.state.queryString.category();
        },
        getSelectedFormCode: function () {
            //var values = model.state.queryString.form();
            return model.state.queryString.form();
        },
        getSelectedSellerCode: function () {
            //var values = model.state.queryString.form();
            return "GFB";
        },
        getSelectedShipWeekString: function () {
            //ToDo: this needs to fetch from actual selection list
            return viewModel.state.shipWeekSelected.ShipWeekString;

        },
        getSelectedSuppliersList: function () {
            var listItems = $("#" + viewModel.config.supplierListDisplayId + " ul")
           // alert(listItems.length);
            listItems = listItems.children("li." + viewModel.config.selectionSelectedClass + ":not(#" + viewModel.config.selectorAllSuppliersId + ")");
          //  alert(listItems.length);

            var s = "";
            $.each(listItems, function(){
                if (s != "") { s += ','; };
                s += $(this).data("code");
            });
            return s;
        },
        getSelectedSpeciesList: function () {
            var listItems = $("#" + viewModel.config.speciesListDisplayId + " ul");
            //alert(listItems.length);
            listItems = listItems.children("li." + viewModel.config.selectionSelectedClass + ":not(#" + viewModel.config.selectorAllSpeciesId + ")");
            //alert(listItems.length);

            var s = "";
            $.each(listItems, function () {
                if (s != "") { s += ','; };
                s += $(this).data("code");
            });
            return s;
        },
        getFilterImmediateAvailability: function() {
            return viewModel.state.filterImmediateAvailability;
        },
       
        getFilterOrganicOnly: function () {
            return viewModel.state.filterOrganicOnly;
        },
    
    shipWeekListComposed: false,
    supplierListComposed: false,
    speciesListComposed: false,
    filterImmediateAvailability: null,
    filterOrganicOnly: null,
    },
    composeHistoryState: function (go) {
        if (typeof go !== "boolean") { go = true; };
        var queryStr = "?";
        queryStr += "category=" + viewModel.state.selections.getSelectedCategoryCode();
        queryStr += "&form=" + viewModel.state.selections.getSelectedFormCode();
        queryStr += "&seller=" + viewModel.state.selections.getSelectedSellerCode();
        queryStr += "&filter=" + viewModel.state.selections.getFilterImmediateAvailability();
        if (viewModel.state.selections.getSelectedCategoryCode() == "EDB")
        {
            queryStr += "&OrganicOnly=" + viewModel.state.selections.getFilterOrganicOnly();
        }
        queryStr += "&shipweek=" + viewModel.state.selections.getSelectedShipWeekString();
        //console.log(viewModel.state.selections.getSelectedSuppliersList());
        queryStr += "&suppliers=" + viewModel.state.selections.getSelectedSuppliersList();
        queryStr += "&species=" + viewModel.state.selections.getSelectedSpeciesList();
        queryStr += "&go=" + go;

        return queryStr;
    },

    shipWeekListContainerElement: null,
    shipWeekSelected: { },  //this is set to shipweek data row by ship week list
    priorSpeciesSelected: [],
    gridPages: function(){
        var rows = model.rowData.length;
        var rowsPerPage = viewModel.config.grid.rowsData;
        if(rows <= 0) {return 0;};
        if (rows <= rowsPerPage) { return 1; };
        var pages = Math.floor(rows / rowsPerPage);
        if ((rows % rowsPerPage) > 0) { pages++ };
        return pages;
    },

    gridFirstRowDisplayed: 1,
    gridPageDisplayed: 0,

    gridPageIsDisplaying: false,
    orderQuantityBeingHandled: false,
    FooterActionsDisplayed: false,

    AddToOrderGuid: "None",
    AddToOrderNo: "None",
    CurrentSort: "Species",
   
};


//

viewModel.codesCompile = function(arr) {
    var sortFunction = function (a, b) {
        //console.log(a[0], b[0]);

        if (a[0] < b[0]) {
            return -1;
        } else if (a[0] > b[0]) {
            return 1;
        } else {
            return 0;
        };

    };

    arr.sort(sortFunction);
    //console.log(arr);

    for (var i = arr.length - 1; i > 0; i--) { //travel backward so we don't delete index yet to be reached.
        var source = arr[i];
        var target = arr[i - 1];
        if (source[0] == target[0]) {
            //console.log(arr[i][0], arr[i - 1][0]);
            target[2] += (source[2]);
            arr.splice(i, 1);   //remove the later one
        };
    };

    var sortFunction2 = function (a, b) {
        return a[1] - b[1];
    };

    arr.sort(sortFunction2);

    //console.log(arr);
    return arr;


};
viewModel.varietyCodesArray = function() {
    //var obj = new {};

    var arr = [];

    $.each(model.rowDataIndex, function(indexRow, indexValue) {

        arr.push([model.rowData[indexValue].VarietyCode, indexRow, 1]);

        //console.log(model.rowData[indexValue].VarietyCode, indexRow);
    });
    //console.log(arr);

    return arr;
};
viewModel.productCodesArray = function () {
    //var obj = new {};

    var arr = [];

    $.each(model.rowDataIndex, function (indexRow, indexValue) {

        arr.push([model.rowData[indexValue].ProductCode, indexRow, 1]);

        //console.log(model.rowData[indexValue].VarietyCode, indexRow);
    });
    //console.log(arr);

    return arr;
};
viewModel.varietyCodeIndexGet = function (code) {
    var arr = viewModel.codesCompile(viewModel.varietyCodesArray());
    var index = -1;
    $.each(arr, function (ndx, val) {
        if (val[0] == code) {
            index = val[1];
            return false;
        } else {
            return true;
        };
    });
    return index;
};
viewModel.productCodeIndexGet = function (code) {
    var arr = viewModel.codesCompile(viewModel.productCodesArray());
    var index = -1;
    $.each(arr, function (ndx, val) {
        if (val[0] == code) {
            index = val[1];
            return false;
        } else {
            return true;
        };
    });
    return index;
};
viewModel.codesDisplay = function (arr) {
    arr = viewModel.codesCompile(arr);
    var s = '', rows = 0, codes = 0;
    $.each(arr, function (ndx, val) {
        codes++;
        rows += val[2];
    });
    s += "Check Sums: [";
    s += "codes = " + codes;
    s += "] - [rows =  " + rows + "]\n";
    s += 'index [count] code\n';
    $.each(arr, function (x, y) {
        var v = (y[1] + ' [' + y[2] + '] - ' + y[0] + '\n');
        //console.log(v);
        s += v;
    });

    alert(s);

};

var historyPush = function () {
   // alert("start historyPush");
    viewModel.state.type.queryString = viewModel.state.composeHistoryState();
   // alert("viewModel.state.type.queryString");
    history.pushState(viewModel.state.type.stateObj, viewModel.state.type.title, (viewModel.state.type.url || '') + viewModel.state.type.queryString || '');
  //  alert("end historyPush");
};
var testHistoryGo = function(ndx) {
    //console.log(ndx);
    if (typeof ndx === 'undefined' || ndx == null) {
        //console.log(ndx);
        ndx = prompt("Value: ", 1);
        //console.log(ndx);
    }
    if (isNaN(ndx)) { ndx = 1; };
    ndx = parseInt(ndx);
    //console.log(ndx);

    //if (ndx > window.history.length) {
    //    ndx = window.history.length;};
    history.go(ndx);
};

var testHistoryView = function () {
    //console.log(history.state);
    //console.log(history);
};

var testHistoryReplace = function () {
    history.replaceState({ 'stateObjName': 'stateObjValue' }, "Testing Push State", "?category=VA&form=RC");
};
var testHistoryPopped = function () {
    var popped = ('state' in window.history && window.history.state !== null), initialURL = location.href;
    return popped;

    //http://stackoverflow.com/questions/6421769/popstate-on-pages-load-in-chrome
    //I am using History API for my web app and have one issue. I do Ajax calls to update some results on the page and use history.pushState() in order to update the browser's location bar without page reload. Then, of course, I use window.popstate in order to restore previous state when back-button is clicked.
    //The problem is well-known — Chrome and Firefox treat that popstate event differently. While Firefox doesn't fire it up on the first load, Chrome does. I would like to have Firefox-style and not fire the event up on load since it just updates the results with exactly the same ones on load. Is there a workaround except using History.js? The reason I don't feel like using it is — it needs way too many JS libraries by itself and, since I need it to be implemented in a CMS with already too much JS, I would like to minimize JS I am putting in it.
    //So, would like to know whether there is a way to make Chrome not fire up 'popstate' on load or, maybe, somebody tried to use History.js as all libraries mashed up together into one file.
};
//var launchClassic = function () {
//    var auth = getAuthentication();
//   // alert("setting beta 0");
//    localStorage.setItem("Beta2", "0");
   
//    window.location.href = window.location.href.replace("Availability2", "Availability");
//}
viewModel.init = function () {
   // alert("In Beta" +localStorage.getItem("Beta"));
    //if (localStorage.getItem("Beta2") == "0") {
    //    launchClassic()
    //};
   // alert("In Init");
    
   
    window.onload = function (evt) {
       
        console.log('onload:', evt);
    };
    window.onpopstate = function (evt) {
       // alert("what");
        console.log('onpopstate:', evt);
        if (evt.state) {
            var url = window.location.href;
            console.log('url: ', url);
            window.location.href = url;
        };
    };

    //Hide all of the ordering stuff if not logged in
    if ($("#loggedInSpan").val() == "false") {
        $("#" + viewModel.config.addToCartButtonID1).hide();
        $("#" + viewModel.config.addToCartButtonID2).hide();
        $("#" + viewModel.config.addToCartButtonID3).hide();
        $("#" + viewModel.config.viewCartButtonID1).hide();
        $("#" + viewModel.config.viewCartButtonID2).hide();
        $("#" + viewModel.config.viewCartButtonID3).hide();
        $("#divPreCart1").hide();
        $("#divPreCart2").hide();
        $("#divPreCart3").hide();
        $("#divPending1").hide();
        $("#divPending2").hide();
        $("#divPending3").hide();
        

    //    $("#resultsFooterRow").hide();


    }
    else {
        model.state.loggedIn = "true";
    }


    //init filterImmediateAvailability
    viewModel.state.filterImmediateAvailability = viewModel.state.filterImmediateAvailability || true;
    
    //alert(typeof v);
    var v = eps_GetQueryStringValue("Filter");
    if (v) {
        //v has value convert it to boolean
        v = v == 'true';
    } else {
        //v has no value, default to true
        v = true;
    };
    viewModel.state.filterImmediateAvailability = (v);
    //Only visible on Edibles
    if (model.state.queryString.category() == "EDB")
    {
        $("#filter_organic_display").show();
        var v = eps_GetQueryStringValue("OrganicOnly");
        if (v=== null || v =="" || v =='true' ) { 
            v = true; 
        }
        else
        {
            v = false;
        }
        viewModel.state.filterOrganicOnly = v;
        viewModel.setFilterOrganicOnly();
    }
    else
    {
        
        $("#filter_organic_display").hide();
        viewModel.state.filterOrganicOnly = false;
        viewModel.setFilterOrganicOnly();
    }

    $("#" + viewModel.config.searchDialog.refineSearchGoId).on("click", function () {
        viewModel.getDataDisplayGrid();
        historyPush();
     });
    $("#" + viewModel.config.searchDialog.refineSearchGoId2).on("click", function () {
        viewModel.getDataDisplayGrid();
        historyPush();
    });

    /************events for select all and clear all *************/
    $("#supplierAll").on("click", function () {
        viewModel.toggleAllSuppliers(true, [function () {
           // alert("get");
           //Put back in for Auto Display viewModel.getDataDisplayGrid("");
        }]);
    });
    $("#supplierClear").on("click", function () {
        viewModel.toggleAllSuppliers(false,[]);
    });
    $("#speciesAll").on("click", function () {
        viewModel.toggleAllSpecies(true,[]);
    });
    $("#speciesClear").on("click", function () {
        viewModel.toggleAllSpecies(false,[]);
    });

    /************events for Load More *************/
    $("#load_more_results_button").on("click", function () {    
            viewModel.loadMoreResults();
    });
    /************events for Add to Cart *************/
    $("#add_to_cart_button").on("click", function () {
        //alert("add clicked");
        viewModel.addToCart("add_to_cart_button");
    });
    $("#add_to_cart_button2").on("click", function () {
        //alert("add clicked 2");
        viewModel.addToCart("add_to_cart_button2");
    });
    $("#add_to_cart_button3").on("click", function () {
        //alert("add clicked 3");
        viewModel.addToCart("add_to_cart_button3");
    });

  
/********************* Events for Ship Week DropDown ********************************/
    viewModel.state.shipWeekListContainerElement = $("#" + viewModel.config.shipWeekListContainerId);

    $("#" + viewModel.config.shipWeekSelectorId).on("mouseenter", function () {
        viewModel.locateShipWeekList();
    });

    $("#" + viewModel.config.shipWeekSelectorId).hover(
        function () {
            viewModel.state.shipWeekListContainerElement.css("display", "block");
        },
        function () {
            viewModel.state.shipWeekListContainerElement.css("display", "none");
        }
    );
    $("#" + viewModel.config.shipWeekListContainerId).hover(
        function () {
            viewModel.state.shipWeekListContainerElement.css("display", "block");
        },
        function () {
            viewModel.state.shipWeekListContainerElement.css("display", "none");
        }
    );

    $("#" + viewModel.config.shipWeekPriorId).on("click", function () {
        var self = $(this);
        self = $("#weekCol1");
        $.each(model.shipWeekData.ShipWeeksList, function (ndx, item) {
            if (item.Guid == viewModel.state.shipWeekSelected.Guid) {
                if (ndx > 0) {
                    viewModel.state.shipWeekSelected = model.shipWeekData.ShipWeeksList[ndx - 1];
                    model.shipWeekData.SelectedShipWeek = model.shipWeekData.ShipWeeksList[ndx - 1];
                    viewModel.getDataDisplayGrid("");
                    historyPush();
                } else {
                    viewModel.floatMessage(self, "At first ship week now.", 2000);
                };
                return false;
            };
        });
    });
    $("#" + viewModel.config.shipWeekNextId).on("click", function () {
        var self = $(this);
        self = $("#weekCol1");
        $.each(model.shipWeekData.ShipWeeksList, function (ndx, item) {
            if (item.Guid == viewModel.state.shipWeekSelected.Guid) {
                if (ndx < model.shipWeekData.ShipWeeksList.length - 1) {
                  //  alert(traverseObj(viewModel.state.shipWeekSelected));
                    viewModel.state.shipWeekSelected = model.shipWeekData.ShipWeeksList[ndx + 1];
                    model.shipWeekData.SelectedShipWeek = model.shipWeekData.ShipWeeksList[ndx + 1];
                    viewModel.getDataDisplayGrid("");
                    historyPush();
                } else {
                    viewModel.floatMessage(self, "At last ship week now.", 2000);
                };
                return false;
            };
        });

    });

/***********************************************************************************/
    //Events for Check boxes
    $("#" + viewModel.config.searchDialog.filterAvailabilitySelectorId)
    .off()
    .on("click", function () {
        viewModel.setFilterImmediateAvailability();
    });
    $("#" + viewModel.config.searchDialog.filterOrganicSelectorId)
        .off()
        .on("click", function () {
            viewModel.state.filterOrganicOnly = !viewModel.state.filterOrganicOnly;
            viewModel.setFilterOrganicOnly();
        });

    /***********************************************************************************/
    //Events for sort
    $("#cboSort").on("change", function () {
       // alert(this.value);
        model.state.CurrentSort = this.value;
        model.config.nextPageToDisplay = 1
        
        viewModel.performColumnSort(); //also displays grid
    });
    
/***********************************************************************************/
    //Init and Show Search Dialog
    var sellerCode = localStorage.getItem("SellerCode");
  //  if (sellerCode == null) {
        sellerCode = "GFB";
   // }
     model.state.seller = sellerCode;
    
   
    //calc and display Totals
   // alert("calling refreshOrderTotal");
    viewModel.refreshOrderTotal();
    viewModel.initSearchInterface();
    //clears grid
    
    viewModel.displayGridRows(true, true, true, "none");
};
viewModel.headerCell = function (r, c) {
    return $("#" + viewModel.config.availabilityGridContainerId + " tr." + viewModel.config.grid.headerRowClassStem + r + " th." + viewModel.config.grid.headerColClassStem + c);
};
viewModel.footerCell = function (r, c) {
    return $("#" + viewModel.config.availabilityGridContainerId + " tr." + viewModel.config.grid.footerRowClassStem + r + " td." + viewModel.config.grid.footerColClassStem + c);
};

viewModel.initSearchInterface = function () {
    //reload Querystring in case this is a re-init
    model.config.init();
   // model.state.sh
    //Note: model.Init should be called prior to this if a re-init
  
    
    
    // alert(viewModel.state.shipWeekSelected.ShipWeekString);
    // alert(model.shipWeekData.SelectedShipWeek.ShipWeekString);
    //
    //  alert(model.state.queryString.shipWeek);
   //  alert(viewModel.state.shipWeekSelected.ShipWeekString);


    model.getShipWeekData([
        viewModel.composeShipWeekList,
        model.getSupplierData,
        viewModel.composeSupplierList,
        viewModel.selectURLSupplierItems,
        viewModel.filterImmediateAvailabilityInit,
        // function () { alert(viewModel.state.shipWeekSelected.ShipWeekString); }
        //Put back in for auto display   function () {
            //Put back in for auto display   viewModel.toggleAllSuppliers(true, [function () {
             //   alert("get");
             //Put back in for auto display   viewModel.getDataDisplayGrid("");
         //Put back in for auto display     }]);

      //Put back in for auto display   }
    ]);

  
};
viewModel.runQueryOption = function (callbacks) {
    go = model.state.queryString.go();
   
    if (go == true) {
        viewModel.getDataDisplayGrid();
    }
    viewModel.ProcessEventChain(callbacks);
};
viewModel.selectURLSupplierItems  = function (callbacks) {
    //  alert("set suppliers");
    var checkAll = false;
    var suppliers = [];
    suppliers = model.state.queryString.suppliers();
    if ($.trim(suppliers) == "*") {
        checkAll = true;
        //$.each(model.supplierData, function (i, item) { suppliers.push(item.Code); });
    } else {
        suppliers = suppliers.split(",");
    };
    console.log('suppliers: ', suppliers);
    var supplierList = $("#" + viewModel.config.supplierListDisplayId + " ul").children("li").not("#" + viewModel.config.selectorAllSuppliersId);
    //var setAllSuppliers = true;
    $.each(supplierList, function (ndx, item) {
        var listItem = $(item);
       // alert(listItem.text());
        if (checkAll==true || $.inArray(listItem.data("code"), suppliers) > -1) {
            listItem.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
        } else {
            listItem.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
            //setAllSuppliers = false;
        };
    });
  model.getSpeciesData([viewModel.composeSpeciesList,
        viewModel.selectURLSpeciesItems,
        viewModel.runQueryOption,
    ]);              


    viewModel.ProcessEventChain(callbacks);
}
viewModel.selectURLSpeciesItems = function (callbacks) {
  //  alert("in selectURLSpeciesItems");
    var species = [];
    species = model.state.queryString.species()
   // alert(species);
    if ($.trim(species) == "*") {
        $.each(model.speciesData, function (i, item) { species.push(item.Code); });
    } else {
        species = species.split(",");
    };
    //  console.log('species: ', species);
    var speciesList = $("#" + viewModel.config.speciesListDisplayId + " ul").children("li").not("#" + viewModel.config.selectorAllSpeciesId);

    $.each(speciesList, function (ndx, item) {
        var listItem = $(item);
        if ($.inArray(listItem.data("code"), species) > -1) {
    //        alert("selected: " + listItem.data("code"));
            listItem.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
        } else {
  //          alert("not selected: " + listItem.data("code"));
            listItem.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
        };
    });

    viewModel.ProcessEventChain(callbacks);
}

viewModel.filterImmediateAvailabilityInit = function (callbacks) {
  //  alert("state=" + viewModel.state.filterImmediateAvailability);
    viewModel.state.filterImmediateAvailability = model.state.queryString.filter();
  
    //note: this can be a re-init so don't go to cookie or url for value, that only happens once in init
    viewModel.setFilterImmediateAvailability(viewModel.state.filterImmediateAvailability);
    viewModel.ProcessEventChain(callbacks);
};

viewModel.setFilterImmediateAvailability = function(valu) {
    //valu is optional, boolean
    //if valu not specified, filter will be toggled
    var newValu = viewModel.state.filterImmediateAvailability || true;
    if (typeof valu === 'undefined') {
        newValu = !viewModel.state.filterImmediateAvailability;
    } else if (typeof valu === "boolean" && valu != viewModel.state.filterImmediateAvailability) {
        //alert("valu=" + valu);
        newValu = valu;
    }
    if (newValu != viewModel.state.filterImmediateAvailability) {
        var ctrl = $("#" + viewModel.config.searchDialog.filterAvailabilitySelectorId);
        if (ctrl.hasClass(viewModel.config.selectionSelectedClass)) {
            //alert("was selected")
            viewModel.state.filterImmediateAvailability = false;
            ctrl.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
        } else {
            //alert("now selected");
            viewModel.state.filterImmediateAvailability = true;
            ctrl.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
        };
    };
};

viewModel.setFilterOrganicOnly = function () {
   
    var newValu = viewModel.state.filterOrganicOnly 
    var ctrl = $("#" + viewModel.config.searchDialog.filterOrganicSelectorId);
    if (newValu == true) {
        ctrl.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
    }
    else {
        ctrl.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
    }
   
    
};

//viewModel.stateCookieWrite = function () {
//    var value = {};
//    value.Category = viewModel.state.selections.getSelectedCategoryCode();
//    value.Form = viewModel.state.selections.getSelectedFormCode();
//    value.ShipWeek = viewModel.state.selections.getSelectedShipWeekString();
//    value.Suppliers = viewModel.state.selections.getSelectedSuppliersList();
//    value.Species = viewModel.state.selections.getSelectedSpeciesList();
//    value.Filter = viewModel.state.selections.getFilterImmediateAvailability();
//    //alert(traverseObj(value, false));
//    //duration, path, name and value
//    eps_writeCookie({
//        'duration': model.state.cookieDurationMinutes,
//        'path': null,
//        'name': model.state.cookieName,
//        'value': JSON.stringify(value)
//    });
//};
//viewModel.stateCookieRead = function () {
//    var value = eps_readCookie(model.state.cookieName);
//    value = JSON.parse(value);
//    //alert(JSON.stringify(value));
//    return value;
//}
//viewModel.stateCookieRemove = function () {
//    return eps_removeCookie(model.state.cookieName);
//    //document.cookie = escape(model.state.cookieName) + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
//}
//viewModel.stateCookieIsValid = function() {
//    var value = eps_readCookie(model.state.cookieName);
//    //console.log('stateCookieIsValid: ', value);
//    if (value == null) { return false; };
//    if (value.Category == null) { return false; };
//    if (value.Form == null) { return false; };
//    if (value.ShipWeek == null) { return false; };
//    if (value.Suppliers == null) { return false; };
//    if (value.Species == null) { return false; };
//    return true;
//};

//viewModel.updateSelections = function () {
//    model.config.init();
// //     alert(model.state.queryString.OrganicOnly());
//    viewModel.setFilterOrganicOnly(model.state.queryString.OrganicOnly());

//    if (model.state.queryString.isFullVersion()) {
//        //console.log('initializing selections from query string.');
//        model.getShipWeekData([
//            viewModel.composeShipWeekList,
//            model.getSupplierData,
//            viewModel.composeSupplierList,
//            model.getSpeciesData,
//            viewModel.composeSpeciesList, //in case this is a re-init
//            function () {
//                var v = {
//                    Category: model.state.queryString.category(),
//                    Form: model.state.queryString.form(),
//                    ShipWeek: model.state.queryString.shipWeek(),
//                    Suppliers: model.state.queryString.suppliers(),
//                    Species: model.state.queryString.species(),
//                    Filter: model.state.queryString.filter(),
//                    OrganicOnly: model.state.queryString.OrganicOnly(),
//                    Variety: model.state.queryString.variety(),
//                    Produt: model.state.queryString.product(),
//                    Go: model.state.queryString.go()
//                };
//                viewModel.LoadSelections(v);
//            }         
//        ]);
//    } else {
//        //if state cookie does not match Query String then retire it
//        if (model.state.queryString.category() != model.state.cookie.category || model.state.queryString.form() != model.state.cookie.form) {
//            //console.log('state cookie does not match, deleting it from response.');
//            viewModel.stateCookieRemove();
//        };

//        var value = viewModel.stateCookieRead();
//        if (value != null) {
//            //alert('state Cookie rules');
//            //console.log('using cookie value: ', value);
//            viewModel.LoadSelections(value);
//        };
//    };
//};

//viewModel.LoadSelections = function (value) {
//    alert("here");
//    var suppliers = [];
//    if ($.trim(value.Suppliers) == "*") {
//        $.each(model.supplierData, function(i, item) { suppliers.push(item.Code); });
//    } else {
//        suppliers = value.Suppliers.split(",");
//    };
//    //console.log('suppliers: ', suppliers);
//    var supplierList = $("#" + viewModel.config.supplierListDisplayId + " ul").children("li").not("#" + viewModel.config.selectorAllSuppliersId);
//    var setAllSuppliers = true;
//    $.each(supplierList, function (ndx, item) {
//        var listItem = $(item);
//        //alert(listItem.text());
//        if ($.inArray(listItem.data("code"), suppliers) > -1) {
//            listItem.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
//        } else {
//            listItem.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
//            setAllSuppliers = false;
//        };
//    });
//    if (setAllSuppliers) {
//        $("#" + viewModel.config.selectorAllSuppliersId).removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
//    };

//    //Update Species based for new Supplier selections]
//    model.getSpeciesData([viewModel.composeSpeciesList, function (callbacks) {
//         //console.log('value', value);
//        var species = [];
//        if ($.trim(value.Species) == "*") {
//            $.each(model.speciesData, function (i, item) { species.push(item.Code); });
//        } else {
//            species = value.Species.split(",");
//        };
//        //console.log('species: ', species);
//        var speciesList = $("#" + viewModel.config.speciesListDisplayId + " ul").children("li").not("#" + viewModel.config.selectorAllSpeciesId);
//        var setAllSpecies = true;
//        $.each(speciesList, function (ndx, item) {
//            var listItem = $(item);
//            //alert(listItem.text());
//            if ($.inArray(listItem.data("code"), species) > -1) {
//                listItem.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
//            } else {
//                listItem.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
//                setAllSpecies = false;
//            };
//        });
//        if (setAllSpecies) {
//            $("#" + viewModel.config.selectorAllSpeciesId).removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
//        };
//        viewModel.ProcessEventChain(callbacks);
//    },
//        model.getShipWeekData,
//        viewModel.composeShipWeekList,
//        function () {
//            //set ship week per selection
//            var shipWeekString = value.ShipWeek || '';
//            //alert(shipWeekString);
//            if (shipWeekString.length > 5) {
//                $.each(model.shipWeekData.ShipWeeksList, function (index, item) {
//                    if (item.ShipWeekString == shipWeekString) {
//                        viewModel.state.shipWeekSelected = item;
//                        viewModel.displayShipWeekSelected();
//                        return false;
//                    } else {
//                        return true;
//                    };
//                });
//            };
//            //If only one supplier selected and one or more species then click go.
//            //if (viewModel.state.selections.selectedSuppliersCount() == 1 && viewModel.state.selections.selectedSpeciesCount() > 0) {
//            //    //alert('bingo');
//            //    viewModel.getDataDisplayGrid();
//            //};
//            var autoGo = value.Go || false;
//            if (autoGo && viewModel.state.selections.selectedSuppliersCount() > 0 && viewModel.state.selections.selectedSpeciesCount() > 0) {
//                //$('#' + viewModel.config.searchDialog.refineSearchGoId).click();
//                console.log('value: ', value);
//                var scrollTo = null;
//                var code = value.Variety || '';
//                if (code != '') {
//                    scrollTo = {
//                        type:'variety',
//                        code:code
//                    };
//                } else {
//                    code = value.Product || '';
//                    if (code != '') {
//                        scrollTo = {
//                            type:'product',
//                            code:code
//                        };
//                    };
//                };
//                //console.log('one: ', scrollTo);
//                //alert("we got here?");
//                viewModel.getDataDisplayGrid(scrollTo);
//            };
//        }
//    ]);   
//};

viewModel.performColumnSort = function () {
    // alert("in sort " + model.state.CurrentSort)

    var ndx = $.inArray(model.state.CurrentSort, model.config.sorts);
   // alert(ndx);
    model.sortOnDataColumn(ndx);
};

viewModel.defineInputBoxEvents = function () {
    $('.' + viewModel.config.orderQuantityInputClass).on("keydown", function (e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
       // alert(key);
        if (key == 13 || key == 9) {
            e.preventDefault();
            //alert(traverseObj($(this).data('boxData')));
            var guid = $(this).prop('id');
            //alert(guid);
            //ToDo: in Chrome both the keydown and the change events fire, debug this
           // alert('keydown');


            viewModel.handleOrderQuantityChange($(this));


            $(this).change();
            if (key == 9) {
                console.log("tab key");
               // $(input [id=guid]).next().focus();
                //go to next input box

            //    alert(guid);
                var nextGuid = viewModel.nextInputGuid(guid);
              //  alert(nextGuid);
                $("#" + nextGuid).focus();
             //   alert("focus done");
            };
            if (e.keyCode == 13) {
                console.log("enter key"); 
                var checkColumnNo = 0;
                var columnNo = parseInt(guid.substr(guid.length - 1));
                var nextGuid = guid;
                for (i = 0; i < 500; i++) {
                    nextGuid = viewModel.nextInputGuid(nextGuid);
                    checkColumnNo = parseInt(nextGuid.substr(nextGuid.length - 1));
                    if (checkColumnNo==columnNo)
                    {
                        $("#" + nextGuid).focus();
                        break;
                    }
                   
                }

               
            
            }
        }
    })
        .on("change", function (e) {
          //  alert('change');
            console.log("change event");
            viewModel.handleOrderQuantityChange($(this));
    })
        .on("focus", function (e) {
        $(this).select();
    });

};


viewModel.displayGridRow = function (ndx, rowElement) {
    //rowElement.empty();
   // alert(traverseObj(model.rowData[ndx]));
    var imageOrganic = $('<img class="img_is_organic" src="/images/layout/USDA.jpg" alt="" />');
    var imagePW = $('<img class="img_is_PW" src="/images/layout/PW.jpg" alt="" />');
    var imageIncludesDelivery = $('<img class="img_includes_delivery" src="/images/layout/DeliveryIncluded.jpg" alt="" title="Price includes delivery." />');
    //var row = $("<div class='resultsRow' id='" + model.rowData[ndx].ProductGuid + "'/>");
    var row = $("<div id='resultsRow' name='" + model.rowData[ndx].VarietyCode + "' />");
    var productInfoColumn1 = $("<div class='productInfoColumn1' />");
    productInfoColumn1.append("<span class='title'>Product:</span> " + model.rowData[ndx].Product + "</br>");
    productInfoColumn1.append("<span class='title'>Species:</span> " + model.rowData[ndx].Species + "</br>");
    productInfoColumn1.append("<span class='title'>Supplier:</span> " + model.rowData[ndx].Supplier );
    if (model.rowData[ndx].IsPW)
    {
        productInfoColumn1.append(imagePW);
    };
    if (model.rowData[ndx].IsOrganic)
    {
        productInfoColumn1.append(imageOrganic);
    };
    productInfoColumn1.append("</br>");
    row.append(productInfoColumn1);

    var priceDisplay = "";
    var productInfoColumn2 = $("<div class='productInfoColumn2' />");
    productInfoColumn2.append(" <span class='title'>Form:</span>" + model.rowData[ndx].Form + "</br>");
    if (!model.showPriceData)
    {
        priceDisplay = "- -";
    }
    else if (model.rowData[ndx].Price <= 0)
    {
        priceDisplay = "TBD";
    }
    else
    {
            if (model.rowData[ndx].Price > 100)
            { 
                priceDisplay = model.rowData[ndx].Price.toFixed(2); 
            }
            else
            {
                priceDisplay = model.rowData[ndx].Price.toFixed(4);
            }
    };
    //alert(model.state.loggedIn);
    //hide price when not logged in
    if (model.state.loggedIn == "true")
    {
        productInfoColumn2.append("<span class='title'>Price:</span>" + priceDisplay);
    }
    if (model.rowData[ndx].IncludesDelivery)
    {
        productInfoColumn2.append(imageIncludesDelivery);
    };
    row.append(productInfoColumn2);
   // alert(traverseObj(model.rowData[ndx]));

    for (var i = 0; i < 4; i++) {
        var weekColumn = $("<div class='weekColumn' />");
     //  alert(model.rowData[ndx].DisplayCodes[i]);
        if (model.rowData[ndx].DisplayCodes[i] != "") {
            weekColumn.append(model.rowData[ndx].DisplayCodes[i] + "</br>");
        } else {
            weekColumn.append(model.rowData[ndx].Availabilities[i].toFixed(0) + "</br>");
        };


        if (model.rowData[ndx].DisplayCodes[i] == "NA") {

        } else if (!model.allowOrdering) {
           
        } else if (model.rowData[ndx].DisplayCodes[i] != "OPEN" && model.rowData[ndx].Availabilities[i] == 0) {
        
        } else {
            var input = $("<input id='" + model.rowData[ndx].ProductGuid + '_' + i + "' type=\"number\"></input>");
            input.addClass(viewModel.config.orderQuantityInputClass);
            input.data('boxData', {
                productGuid: model.rowData[ndx].ProductGuid,
                shipWeekCode: i
            });

            input.prop("maxlength", '6');
            input.val(model.rowData[ndx].OrderQuantities[i]);
            weekColumn.append(input);

            if (model.rowData[ndx].OrderQuantities[i] > 0) {
                //this gets icon
                if (model.rowData[ndx].IsCarteds[i]) {
                    weekColumn.append(viewModel.config.icons.cart_checked);
                } else {
                    weekColumn.append(viewModel.config.icons.not_carted);
                }
            };
        }
        row.append(weekColumn);
    }
    rowElement.append(row);
};

viewModel.displayGridRows = function (firstRow,firstrun, incrementNextPageToDisplay,scrollTo) {

  //  alert("displayGridRows");
    //display ship week headings
   // alert(traverseObj(model.shipWeekHeadings));
    $.each(model.shipWeekHeadings, function (i, item) {
        // alert(i);
        $("#weekCol" + (i + 1)).html("<img src='../Images/Availability/icon_week.png' />" + item)
    });



    var el = $("#resultsRows");
    var r = 1;
    if (firstRow == true) {
       
        if  (model.rowData.length == 0 && firstrun == false)
        {
            //viewModel.searchDisplay();
            eps_tools.OkDialog({ 'title': '<span>No Products Found:</span>', 'content': "<br /><p>Your search resulted in " + model.rowData.length + " results.</p><p>Please uncheck the 'only view available products' option or change the week that you are searching to get availability listings.</p><p></p></span>", 'modal': true });          
        }
        el.empty();
        //     $("#load_more_results_button").css('visibility', 'visible');
        $("#load_more_results_button").css('visibility', 'hidden');

    }
  //  alert((model.config.nextPageToDisplay * model.config.displayPageSize) + "   " + model.rowData.length);
  //  alert(model.config.nextPageToDisplay);
    for (var i = ((model.config.nextPageToDisplay - 1) * model.config.displayPageSize) ; i < model.config.nextPageToDisplay * model.config.displayPageSize; i++) {
    //    for (var i = 0; i < model.rowData.length; i++) {
            //alert(traverseObj(model.rowData[i]));
        //if (model.isValidRowNdx(r)) {
            if (i < model.rowData.length)
            {
                viewModel.displayGridRow(i, el);
            }
    };
    if(model.rowData.length > (model.config.nextPageToDisplay * model.config.displayPageSize)- 1 )
    {
       // alert("visible");
        $("#load_more_results_button").css('visibility', 'visible');
    }
    else {
       // alert("hidden");
       $("#load_more_results_button").css('visibility', 'hidden');
    }
 

    if (incrementNextPageToDisplay == true) {
        //   alert("increment nextPageToDisplay");
        model.config.nextPageToDisplay += 1;
    }
    ////define input box events
    viewModel.defineInputBoxEvents();

    //scrolling
    if(scrollTo != "none")
    {
        // GmB you may want to put a timeout callback in case your returned data is delayed
     //   alert("about to scroll");
        $.scrollTo('[name="' + scrollTo + '"]', 1000, { easing: 'easeInOutExpo', offset: -100, 'axis': 'y' });
    }
   
};

viewModel.displayShipWeekSelected = function () {
    $("#" + viewModel.config.grid.shipWeekDisplayId).text("Week: " + viewModel.state.shipWeekSelected.ShipWeekString);
}

viewModel.AdjustOrderQuantity = function (valu, max, min, mult) {

    var returnType = { "amount": valu, "message": null };

    var type = typeof valu;
    if (type === "undefined" || type !== "number" || valu <= 0) {
        returnType.amount = 0;
        return returnType;
    };
    var amt = valu;

    //adjust multiple to at least 1
    if (mult == null || mult < 1) { mult = 1; };

    //adjust max to highest mult available
    max = Math.floor(max / mult) * mult;

    //adjust min to lowest multiple that meets minimum
    var v = mult;
    while (v < min) { v += mult; };
    min = v;

    if (min > max) {
        amt = 0;
        returnType.message = "Minimum order quantity not available!";
    } else {
        if (amt < min) {
            //raise amt to min
            amt = min;
        } else {
            if (amt > max) {
                //reduce amt to max
                amt = max;
                returnType.message = "Order quantity reduced to amount available.";
            } else {
                if (mult > 1) {
                    //adjust amt to nearest line item multiple
                    var v = Math.floor(amt / mult);
                    if ((amt % mult) >= (Math.floor(mult / 2))) { v++ };
                    amt = v * mult;
                };
            };
        };
    };

    returnType.amount = amt;
    return returnType;
};

viewModel.handleOrderQuantityChange = function (inputBox) {
    
    
    var guid = inputBox.data('boxData').productGuid;
    var shipweek = inputBox.data('boxData').shipWeekCode;


    var orderWeek = model.shipWeekHeadings[shipweek];
   // console.log(traverseObj(inputBox.data('boxData')));
  //  console.log(model.shipWeekHeadings);
    console.log(viewModel.state.shipWeekSelected);
    console.log("orderWeek:" + orderWeek);

    if (
        (shipweek == 2 && model.shipWeekHeadings[shipweek] == "1")
        ||
        (shipweek == 3 && (model.shipWeekHeadings[shipweek] == "1" || model.shipWeekHeadings[shipweek] == "2"))
        )
    {
        console.log("top");
        var year = 0;
        year = parseInt(viewModel.state.shipWeekSelected.Year, 10) + 1
        console.log("year:" + year.toString);
        var shipWeekCode = model.shipWeekHeadings[shipweek] + "|" + year.toString();
    }
    else 
    {
    
        if (shipweek == 0 && model.shipWeekHeadings[shipweek + 1] == "1") {
            console.log("middle");
            var year = 0;
            year = parseInt(viewModel.state.shipWeekSelected.Year, 10) - 1
            console.log("year:" + year.toString);

            var shipWeekCode = model.shipWeekHeadings[shipweek] + "|" + year.toString();
        }
        else
        {
            console.log("bottom");
            var shipWeekCode = model.shipWeekHeadings[shipweek] + "|" + viewModel.state.shipWeekSelected.Year;
        }
    }
    //viewModel.state.shipWeekSelected.ShipWeekString
    console.log(shipWeekCode);
    
    if (guid == null || guid.length != 36) { return false; };
    
    if (inputBox.length == 0) { return false; };  //no such element

    if (viewModel.state.orderQuantityBeingHandled) return false;    //avoid keydown and change events both firing
  //  console.log("before: " + viewModel.state.orderQuantityBeingHandled + " shipweek= " + shipWeekCode);
    viewModel.state.orderQuantityBeingHandled = true;
  //  console.log("after: " + viewModel.state.orderQuantityBeingHandled + " value= " + inputBox.val());

    var ndx = model.rowDataNdxFromProductGuid(guid);
    //var valu = parseInt(inputBox.val().replace(",", "")) || 0;

    var valu = inputBox.val();
    if (valu != null && valu != "") {
        //numeral.js cannot unformat empty or null string
        var round = Math.round;
        valu = parseInt(round(valu)) || 0;
    } else {
        valu = 0;
    };
    console.log("Got here: " + ndx);
    if (valu == model.rowData[ndx].OrderQuantities[shipweek]) {
        inputBox.val(valu);
        viewModel.state.orderQuantityBeingHandled = false;
        return false;
    };
    console.log("Got to AdjustOrderQuantity: " + ndx);
    var result = viewModel.AdjustOrderQuantity(
        valu,
        (function () {
        //get max
        var max = model.returnAvailability(ndx, shipweek);
        //if max is unlimited, set max = 4999999.
        if (model.returnAvailabilityIsUnlimited(ndx, shipweek)) { max = 499999; };
        return max;
        })(),
        model.rowData[ndx].Min,
        model.rowData[ndx].Mult 
    );
  
   // alert("amount: " + result.amount +", message: " + result.message);
    if (result.message !== null) {
        viewModel.floatMessage(inputBox, result.message, 2500);
    };
    var amt = result.amount;
 //   console.log("adjusted: " + amt);
    if (model.updateOrderQuantity(ndx, amt, shipweek)) {
        //make call to update server
        console.log("Submit to server");
        viewModel.submitOrderQuantity(ndx, inputBox, shipWeekCode);
    } else {
        console.log("Not Submitted to server");
        //reformat input box
        if (amt < 0) { amt = 0; };
    };
  //  alert(amt);
    //format entry and display it
    inputBox.val(amt);
    
    viewModel.state.orderQuantityBeingHandled = false;
};

//viewModel.floatMessage = function (element, content, interval) {
//    //floats message to right of element
//    if (typeof element !== 'undefined' && element != null || element.length > 0) {
//        var position = element.offset();
//        //assume floating message is 35 px tall
//        var top = position.top + ((element.height() - 35) /2);
//        eps_tools.FloatingMessage(position.left + element.width() + 30, top, content, interval);
//    };
//};

viewModel.submitOrderQuantity = function (ndx, inputBox,shipWeekCode) {
    if (!model.isValidRowNdx) { return false;};
    var rowData = model.rowData[ndx];

    var sessionGuid = rowData.ProductGuid;  //ToDo: fix this with real Session Guid
    //show working icon
    viewModel.showOrderIcon(inputBox, viewModel.config.icons.order_working);
    //alert(localStorage.getItem("SellerCode"));
    $.ajax({
        url: model.config.urlAvailabilityApiCart,
        async: true,
        type: "put",
        data: {
            "SessionGuid": sessionGuid,
            "ShipWeekString": shipWeekCode,
            "SellerCode": "GFB",
            "RowData": rowData
        },
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
        },
        success: function (data, textStatus, jqXHR) {
            if(data.Success){
                model.refreshDataRow(data.RowData);
                viewModel.refreshOrderTotal();
            };
        },
        complete: function (jqXHR, textStatus) {
            //reset icon
            viewModel.resetOrderIcon(rowData.ProductGuid, inputBox);
        }
    });
};
viewModel.addToCart = function (el_id) {
    var uncarted = model.state.growerTotals.Uncarted;
    var category = model.state.queryString.category();
    var form = model.state.queryString.form();
   
    //alert("about to call " + model.config.urlAvailabilityApiCart + " " + category + " " + form);
    if (uncarted > 0) {
        //Make ajax call to retrieve row data
        $.ajax({
            type: "POST",
            //data: {
           // //    "GrowerOrderGuid": "hi",
            //    "Category": category,
            //    "Form": form,
            //    "ShipWeekString": "ALL",
            //    "AddToOrder": "False"
            //},
        
            url: model.config.urlAvailabilityApiCart + "?GrowerOrderGuid=hi&Category=" + category + "&Form=" + form + "&ShipWeekString=ALL&AddToOrder=false",
            datatype: "json",
            beforeSend: function (jqXHR, settings) {
            },
            success: function (response, textStatus, jqXHR) {
                // targetResponse.html("SERVER RESPONSE: <br />" + JSON.stringify(response));
                if (response.Success) {
                            
                    $.each(model.rowData, function (ndx, dataRow) {
                        $.each(model.shipWeekHeadings, function (i, item) {
                            if (dataRow.OrderQuantities[i] > 0)
                            {
                                dataRow.IsCarteds[i] = true;
                            };
                        });
                    });
                           
                    //Update Icons
                    viewModel.setAllOrderIconsToCart();

                    //Update Order totals on top and bottom
                    viewModel.refreshOrderTotal();
                        
                    //display message
                    viewModel.floatMessage($("#" + el_id), 'Added ' + uncarted + ' items to your cart.', 2500);                  
                } else
                {
                    eps_tools.OkDialog({ 'title': '<span>Error:</span>', 'content': '<br /><span>Add to Cart did not succeed!<br />&nbsp;</span>', 'modal': true });
                };
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    } else {

        eps_tools.OkDialog({ 'title': '<span>Cart:</span>', 'content': '<br /><span>You have no uncarted items to add to your cart.<br />Enter order quantities and then submit to cart.<br />&nbsp;</span><br />', 'modal': true });
    };  
};


viewModel.addToOrder = function (i) {
    console.log("adding to order: " + viewModel.state.AddToOrderNo);
    var growerOrderGuid = viewModel.state.AddToOrderGuid;
  //  alert("not added");
   // alert("Guid: " + viewModel.state.AddToOrderGuid);
    //var counts = model.OrderQuantityTotals();
    var shipweek = parseInt(viewModel.state.shipWeekSelected.ShipWeekString.substr(3, 4) + "" + viewModel.state.shipWeekSelected.ShipWeekString.substr(0, 2), 10);
    var shipweekString = "";
    //alert(shipweek);
    
    var uncarted = 0;
    var weekOffset = 2;
    for (var j = 0; j < model.shipWeekData.ShipWeeksList.length; ++j) {
      //  alert("i= " + i + "j =  " + j + "model.shipWeekData.ShipWeeksList[j].ShipWeekString: " + model.shipWeekData.ShipWeeksList[j].ShipWeekString + "viewModel.state.shipWeekSelected.ShipWeekString: " + viewModel.state.shipWeekSelected.ShipWeekString);
        if (model.shipWeekData.ShipWeeksList[j].ShipWeekString == viewModel.state.shipWeekSelected.ShipWeekString) {
           
            var shipweekForOrder = model.shipWeekData.ShipWeeksList[j + i - weekOffset];
               
            shipweek = parseInt(shipweekForOrder.ShipWeekString.substr(3, 4) + "" + shipweekForOrder.ShipWeekString.substr(0, 2), 10);
          //  alert(shipweek);
            shipweekString = shipweekForOrder.ShipWeekString;
         //   alert(shipweekString);
            
            break;
        };
           
       
    }
  
    console.log(shipweek);
    console.log(shipweekString);
 
    
    console.log(model.state.growerTotalsPerWeek.WeekSummary);
    $.each(model.state.growerTotalsPerWeek.WeekSummary, function (j, item) {
        console.log(item.Uncarted);
        console.log(item.ShipWeekCode);
        console.log(shipweek);
        if (parseInt(item.ShipWeekCode, 10) == shipweek) {

            uncarted = item.Uncarted;
        }
    });
  //  alert(uncarted);
  //  alert(shipweekString);
    
    if (uncarted > 0) {
        $.ajax({
            type: "POST",
            //data: {
            //    "GrowerOrderGuid": growerOrderGuid,
            //    "Category": model.state.queryString.category(),
            //    "Form": model.state.queryString.form(),
            //    "ShipWeekString": shipweekString,
            //    "AddToOrder": "true"
            //},
            url: model.config.urlAvailabilityApiOrder + "?GrowerOrderGuid=" + growerOrderGuid + "&Category=" + model.state.queryString.category() + "&Form=" + model.state.queryString.form() + " &ShipWeekString=" + shipweekString + "&AddToOrder=true"  ,
            datatype: "json",
            beforeSend: function (jqXHR, settings) {
            },
            success: function (response, textStatus, jqXHR) {
               // alert(response.Success);
                if (response.Success) {

                  //  viewModel.resetAllOrderIcons();                      
                  //  viewModel.refreshOrderTotal();
                    viewModel.floatMessage($("#" + viewModel.config.addToOrderLinkId + i), 'Items have been added to Order ' + viewModel.state.AddToOrderNo, 2000);
                  //  setTimeout(function () { window.location.href = '/MyAccount/Orders/open?OrderGuid=' + growerOrderGuid; }, 2000);
                    viewModel.clearPrecartIcons(shipweekString, i);
                    viewModel.showOrderIcon($("#" + viewModel.config.addToOrderLinkId + i), viewModel.config.icons.not_carted);
                    viewModel.refreshOrderTotal();
                }
                else
                {
                    eps_tools.OkDialog({ 'title': '<span>Error:</span>', 'content': '<br /><span>Add to Order did not succeed!<br />&nbsp;</span>', 'modal': true });
                };
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    }
    else
    {
        eps_tools.OkDialog({ 'title': '<span>Add to Order:</span>', 'content': '<br /><span>You have no uncarted items to add to your order.<br />Enter order quantities and then Add to Order.<br />&nbsp;</span><br />', 'modal': true });
    };
};

viewModel.nextInputGuid = function (guid) {
   // alert('nextInputGuid');
    if (guid == null || guid.length < 36) { return "" };
    if ($("#" + guid).length < 1) {return "" };
   
    var returnGuid = guid;
    var allInputs = $("." + viewModel.config.orderQuantityInputClass);
    $.each(
        allInputs, function(i, el){
            if (el.id == guid) {
         //       alert('bingo');
                if ((i + 1) < allInputs.length) {
                    returnGuid = allInputs[i + 1].id;  //return next input box
                    //return returnInput;
                    //return allInputs[i + 1].id;  
                } else {
                    returnGuid = allInputs[0].id;  //return first input box
                };
                return false;  //break out of $.each
            };
        });
    return returnGuid;
};
viewModel.clearPrecartIcons = function (shipweekString,i) {
   // alert(shipweekString);
   // alert(i);
    for (var j = 0 ; j < model.rowData.length; j++) {
      //  alert(model.rowData[j].OrderQuantities[i - 1]);
       
        if (model.rowData[j].IsCarteds[i - 1] == false && model.rowData[j].OrderQuantities[i - 1] != 0) {
         //   alert("setting to 0");
            model.rowData[j].OrderQuantities[i - 1] = 0;
        }
    };
    var pageCount = model.config.nextPageToDisplay
    model.config.nextPageToDisplay = 1
    var el = $("#resultsRows");
    el.empty();
  //  alert("model.config.nextPageToDisplay = " + model.config.nextPageToDisplay);
    for (var k = 1; k < pageCount; k++) {
       // alert("k = " + k);
        viewModel.displayGridRows(false, false, false,"none");
    }
    model.config.nextPageToDisplay = pageCount
   
};
viewModel.orderTotalClear = function () {
    $("#" + viewModel.config.cartedQuantityDisplayId).text('0');
    $("#" + viewModel.config.precartQuantityDisplayId).text('0');
    $("#" + viewModel.config.cartedQuantityDisplayId2).text('0');
    $("#" + viewModel.config.precartQuantityDisplayId2).text('0');

};

viewModel.refreshWeekTotals = function () {
    model.growerTotalsPerWeekGet(function () {
        // alert(traverseObj(model.state.growerTotalsPerWeek));
        $("#weekCol1Span").text(0);
        $("#weekCol2Span").text(0);
        $("#weekCol3Span").text(0);
        $("#weekCol4Span").text(0);

        if (typeof viewModel.state.shipWeekSelected.ShipWeekString !== 'undefined' && viewModel.state.shipWeekSelected.ShipWeekString !== null) {
           
        }
        else {
            viewModel.state.shipWeekSelected.ShipWeekString = "48|2020"
        }
      
        var shipweek = parseInt(viewModel.state.selections.getSelectedShipWeekString().substr(3, 4) + "" + viewModel.state.selections.getSelectedShipWeekString().substr(0, 2), 10);
      //  alert(shipweek);

        var tempShipWeek = shipweek.toString();
        var newshipweek = shipweek;
        var oldShipweek = shipweek;
        if (tempShipWeek.substr(4, 2) == "51" || tempShipWeek.substr(4, 2) == "52") {
            newshipweek = parseInt(tempShipWeek.substr(0, 4), 10) + 1
            newshipweek = (newshipweek * 100);
         //   alert(newshipweek);
        }
        if (tempShipWeek.substr(4, 2) == "01" ) {
            oldShipweek = parseInt(tempShipWeek.substr(0, 4), 10) - 1
            oldShipweek = (oldShipweek * 100);
         //   alert(oldShipweek);
        }
        $.each(model.state.growerTotalsPerWeek.WeekSummary, function (i, item) {
              // alert(i);
              // alert(traverseObj(item));
            if (tempShipWeek.substr(4, 2) == "01")
            {
                if (parseInt(item.ShipWeekCode, 10) == (oldShipweek + 52)) {
                    $("#weekCol1Span").text(item.Total);
                }
            }
            else {
                if (parseInt(item.ShipWeekCode, 10) == (shipweek - 1)) {
                    $("#weekCol1Span").text(item.Total);
                }
            }
            
            if (parseInt(item.ShipWeekCode, 10) == shipweek)
            {
                $("#weekCol2Span").text(item.Total);
            }
           
          
            if (tempShipWeek.substr(4, 2) == "52")
            {
                if (parseInt(item.ShipWeekCode, 10) == (newshipweek + 1)) 
                {
                    $("#weekCol3Span").text(item.Total);
                }        
            }
            else {
                if (parseInt(item.ShipWeekCode, 10) == (shipweek + 1)) {
                    $("#weekCol3Span").text(item.Total);
                }
            }

            if (tempShipWeek.substr(4, 2) == "52")
            {
                if (parseInt(item.ShipWeekCode, 10) == (newshipweek + 2)) {
                    $("#weekCol4Span").text(item.Total);
                }
            }
            else
            {
                if (tempShipWeek.substr(4, 2) == "51")
                {
                    if (parseInt(item.ShipWeekCode, 10) == (newshipweek + 1)) {
                        $("#weekCol4Span").text(item.Total);
                    }
                }
                else {
                    if (parseInt(item.ShipWeekCode, 10) == (shipweek + 2)) {
                        $("#weekCol4Span").text(item.Total);
                    }
                }
            }
        });
    });

    viewModel.HideOrderLink(1);
    viewModel.HideOrderLink(2);
    viewModel.HideOrderLink(3);
    viewModel.HideOrderLink(4);

    //add call to get orders for all weeks
    model.growerGetOrderSumamries(function (summaries) 
    {
        var added1 = false;
        var added2 = false;
        var added3 = false;
        var added4 = false;
        var shipWeekGuid1 = "";
        var shipWeekGuid2 = "";
        var shipWeekGuid3 = "";
        var shipWeekGuid4 = "";
       
        //alert(traverseObj(summaries.OrderSummaries));
        // alert(traverseObj(model.shipWeekData.SelectedShipWeek));
        if (summaries.Success) {
            if (summaries.OrderCount > 0) {
                $.each(model.shipWeekData.ShipWeeksList, function (ndx, item) {
                    if (item.Guid == model.shipWeekData.SelectedShipWeek.Guid) {
                     //   alert(traverseObj(model.shipWeekData.ShipWeeksList));
                        //   alert(traverseObj(model.shipWeekData.ShipWeeksList[ndx]));
                        if (ndx > 0) {
                            //to do....how to handle behind 1st week?
                              shipWeekGuid1 = model.shipWeekData.ShipWeeksList[ndx - 1].Guid;
                        }
                     
                        shipWeekGuid2 = model.shipWeekData.ShipWeeksList[ndx].Guid;
                        shipWeekGuid3 = model.shipWeekData.ShipWeeksList[ndx + 1].Guid;
                        shipWeekGuid4 = model.shipWeekData.ShipWeeksList[ndx + 2].Guid;

                    };
                });

             //   alert(shipWeekGuid1 + " " + shipWeekGuid2 + " " + shipWeekGuid3 + " " + shipWeekGuid4);

                $.each(summaries.OrderSummaries, function (i, item) {
                    if (shipWeekGuid1 == item.ShipWeekGuid) {
                        if (added1 == false) {
                            viewModel.addAddToOrderSpan(1);
                            added1 = true;
                        };

                        viewModel.addItemToOrderList(1, item);
                    };
                    if (shipWeekGuid2 == item.ShipWeekGuid) {
                        if (added2 == false) {
                            viewModel.addAddToOrderSpan(2);
                            added2 = true;
                        };

                        viewModel.addItemToOrderList(2, item);
                    };
                    if (shipWeekGuid3 == item.ShipWeekGuid) {
                        if (added3 == false) {
                            viewModel.addAddToOrderSpan(3);
                            added3 = true;
                        };

                        viewModel.addItemToOrderList(3, item);
                    };
                    if (shipWeekGuid4 == item.ShipWeekGuid) {
                        if (added4 == false) {
                            viewModel.addAddToOrderSpan(4);
                            added4 = true;
                        };

                        viewModel.addItemToOrderList(4, item);
                    };
                });
                //   Show the links for items with orders
                if (added1 == true) {
                    viewModel.ShowOrderLink(1);
                   
                };
                if (added2 == true) {
                    viewModel.ShowOrderLink(2);
                    
                };
                if (added3 == true) {
                    viewModel.ShowOrderLink(3);
                };
                if (added4 == true) {
                    viewModel.ShowOrderLink(4);
                };
            };   
        };
    }); 
};
viewModel.addAddToOrderSpan = function (i) {
 
    var span = $("<span></span>");
    span.append("add to order");
    span.prop("id", viewModel.config.addToOrderLinkId + i);
    span.text("Add to Order");
    //span.data("orderGuid", { orderguid: "guid 1" });
    span.on("click", function () {
        viewModel.showOrderIcon($("#" + viewModel.config.addToOrderLinkId + i), viewModel.config.icons.order_working);
        viewModel.state.AddToOrderGuid = $("#" + viewModel.config.addToOrderList + i).val();
        viewModel.state.AddToOrderNo = $("#" + viewModel.config.addToOrderList + i + " :selected").text();
        viewModel.addToOrder(i);
        
    });

    $("#" + viewModel.config.addToOrderDiv + i).html(span);
    var combo  = $("<select></select>");
    combo.prop("id", viewModel.config.addToOrderList + i);
    combo.prop("style", "width:100px");
    $("#" + viewModel.config.addToOrderDiv + i).append(combo);
    //$("#" + viewModel.config.addToOrderList + i).empty();
};

viewModel.addItemToOrderList = function (i,item) {
   // alert("addItemToOrderList");
   // alert(item.GrowerOrderGuid);

    var orderText = item.OrderNo;
   // alert(item.ShipToCity);
    if (item.CustomerPoNo != "") {
        orderText = orderText + " - " + item.CustomerPoNo;
    }
    if (item.ShipToCity != "") {
        orderText = orderText + " - " + item.ShipToCity;
    }
  //  alert(orderText);
    $("#" + viewModel.config.addToOrderList + i).append($("<option></option>")
                   .attr("value", item.GrowerOrderGuid)
                   .text(orderText)
                   );
  //  alert("addItemToOrderList end");
}

viewModel.refreshOrderTotal = function () {
    model.growerTotalsGet(function () {
      //  alert("refreshOrderTotal");
        var round = Math.round;
        $("#" + viewModel.config.cartedQuantityDisplayId).text(round(model.state.growerTotals.Carted));
        $("#" + viewModel.config.precartQuantityDisplayId).text(round(model.state.growerTotals.Uncarted));
        $("#" + viewModel.config.cartedQuantityDisplayId2).text(round(model.state.growerTotals.Carted));
        $("#" + viewModel.config.precartQuantityDisplayId2).text(round(model.state.growerTotals.Uncarted));
            
    });

    if (viewModel.state.shipWeekSelected.ShipWeekString != null)
    {
        viewModel.refreshWeekTotals();
        
    }

};


viewModel.getDataDisplayGrid = function (scrollTo) {
    //alert("getDataDisplayGrid")

    model.getRowDataCount([function () {
        //  alert(model.rowDataCount)
        $("#rowCount").html(model.rowDataCount + " Results");
            
    }
    ]);
       
  
    var container =$("#resultsRows");
    container.empty();
    container.html('<img style="margin:0 0 0 235px;" src="/Images/icons/loading_page.gif" />');

    model.getRowDataByPage([
        function() {
            //alert("in call back: " + model.RowData.length);
            //alert(model.state.CurrentSort);
            model.config.nextPageToDisplay = 1;
            if (model.rowData === null) {
                //viewModel.searchDisplay();
                eps_tools.OkDialog({ 'title': '<span>No Products Found:</span>', 'content': "<br /><p>Your search resulted in " + 0 + " results.</p><p>Please uncheck the 'only view available products' option or change the week that you are searching to get availability listings.</p><p></p></span>", 'modal': true });
                var el = $("#resultsRows");
                el.empty();
            }
            else {
                viewModel.performColumnSort();
                //   viewModel.displayGridRows(true,false,true,"none");


                viewModel.refreshWeekTotals();
            }
           
        }
    ]);



};


viewModel.ProcessEventChain = function (callbacks) {

    if (callbacks != undefined && callbacks.length > 0) {
        var callback = callbacks.shift();
        //alert(callback);
        callback(callbacks);
    }
};

viewModel.toggleAllSuppliers = function (checked,callbacks) {
    if (checked === true) {
        var listItems = $("#" + viewModel.config.supplierListDisplayId + " ul").children("li");
        listItems.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
        model.getSpeciesData([viewModel.composeSpeciesList,function () {
            viewModel.toggleAllSpecies(true, []);
            viewModel.ProcessEventChain(callbacks);
        }]);
    }
    else {
        var listItems = $("#" + viewModel.config.supplierListDisplayId + " ul").children("li");
        listItems.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
    }
       
       
};

viewModel.toggleAllSpecies = function (checked, callbacks) {
    console.log("toggleAllSpecies");
    if (checked == true) {
        var listItems = $("#" + viewModel.config.speciesListDisplayId + " ul").children("li");
        listItems.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
    }
    else {
        var listItems = $("#" + viewModel.config.speciesListDisplayId + " ul").children("li");
        listItems.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
    }
    viewModel.ProcessEventChain(callbacks);
};
    
viewModel.allSpeciesSelected = function () {
    var allSelected = true;
    var listItems = $("#speciesUL").children("li");
   // alert(listItems.length);
    $.each(listItems, function (index, value) {
    //    alert(index);
        // alert(value.id);
     //   .classList.contains(cls);
        if (value.classList.contains(viewModel.config.selectionAvailableClass)){
            allSelected = false;
     //       alert("not selected");
            return false;
        }
    });

   
    return allSelected;
};


viewModel.locateShipWeekList = function () {
    //called on init and when enter selector in case there has been a change of browser size
    var el = $("#" + viewModel.config.shipWeekSelectorId);
    var offset = el.offset();
    $("#" + viewModel.config.shipWeekListContainerId).css("left", offset.left - 4).css("top", el.height());
};
viewModel.displayShipWeekList = function () {
    $("#"+viewModel.config.shipWeekListContainerId).show('slow');
};

viewModel.composeShipWeekList = function (callbacks) {
    var target = $("#" + viewModel.config.shipWeekListDisplayId);
    target.empty();
    var list = $("<ul />");

    $.each(model.shipWeekData.ShipWeeksList, function (index, value) {
        var li = $("<li></li>");
        li.prop("class", viewModel.config.shipWeekAvailableClass);
        li.prop("id", value.Guid);
        li.text(value.ShipWeekString)
        li.data("code", value.Code);
        list.append(li);
    });
    target.append(list);
    var listItems = target.children("ul");
    listItems = listItems.children("li");

    listItems.on("click", function () {
        var thisItem = $(this);
        var id = thisItem.prop("id");
        $.each(model.shipWeekData.ShipWeeksList, function (index, item) {
            if (item.Guid == id) {
                viewModel.state.shipWeekSelected = item;
                model.shipWeekData.SelectedShipWeek = item;
                return false;
            };
        });
        $("#" + viewModel.config.shipWeekListContainerId).hide();
        viewModel.getDataDisplayGrid();
        historyPush();
    });
    viewModel.ProcessEventChain(callbacks);
};

viewModel.composeSupplierList = function (callbacks) {
    var target = $("#" + viewModel.config.supplierListDisplayId);
    target.empty();
    var list = $("<ul></ul>");
      
    $.each(model.supplierData, function (index, value) {
        var li = $("<li></li>");
        li.prop("class", viewModel.config.selectionAvailableClass);
        li.text(value.Name);
        li.data("code", value.Code);
        list.append(li);
    });
    target.append(list);
    var listItems = target.children("ul");
    listItems = listItems.children("li");

    listItems.on("click", function () {
        var thisItem = $(this);
        var listItems = $("#" + viewModel.config.supplierListDisplayId + " ul").children("li"); //includes the select all item at [0]
        var selectedSuppliersCountBeginning = $("#" + viewModel.config.supplierListDisplayId + " ul").children("li." + viewModel.config.selectionSelectedClass).not("li#" + viewModel.config.selectorAllSuppliersId).length || 0;
        var allSuppliersSelected = false;
            
        if (thisItem.hasClass(viewModel.config.selectionSelectedClass)) {
            //selected item was unselected
            thisItem.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
            $("#" + viewModel.config.selectorAllSuppliersId).removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
        } else {
            //unselected item was selected
            thisItem.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
        }

        var selectedSuppliersCountEnding = $("#" + viewModel.config.supplierListDisplayId + " ul").children("li." + viewModel.config.selectionSelectedClass).not("li#" + viewModel.config.selectorAllSuppliersId).length || 0;
        if (allSuppliersSelected || (selectedSuppliersCountBeginning == 0 && selectedSuppliersCountEnding == 1)) {
            //this is the first one selected
            $("#" + viewModel.config.selectorAllSpeciesId).removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
        };
        var allSpecies = true;
        allSpecies = viewModel.allSpeciesSelected();
      
        if (allSpecies == true) {
            console.log("selecting all");
          //  model.state.userSelections.species = viewModel.state.selections.getSelectedSpeciesList();       //archive the current species selections
            model.getSpeciesData([viewModel.composeSpeciesList, viewModel.selectAllSpecies]);                 //When suppler selections change, reload species list
  //          model.getSpeciesData([viewModel.composeSpeciesList([viewModel.selectSpecies(true)])]);  //suppler change with all species selected, reload species list with all selected
        } else {
            console.log("not selecting all");
            model.state.userSelections.species = viewModel.state.selections.getSelectedSpeciesList();       //archive the current species selections
            model.getSpeciesData([viewModel.composeSpeciesList, viewModel.selectSpecies]);                  //When suppler selections change, reload species list
        };
        model.config.nextPageToDisplay = 1;
    //   Put back in for auto display viewModel.getDataDisplayGrid("");
    });

    viewModel.state.supplierListComposed = true;

    viewModel.ProcessEventChain(callbacks);

};
viewModel.selectAllSpecies = function () {
    viewModel.toggleAllSpecies(true, []);
};

viewModel.selectSpecies = function () {
   
   // console.log("selectSpecies");
    var species = $("#" + viewModel.config.speciesListDisplayId + " ul").children("li");
   
    if ((model.state.userSelections.species.length || 0) > 0)
    {
       // console.log("not all");
        var testString = "," + model.state.userSelections.species + ",";
        $.each(species, function (i, item) {
          //  console.log("each");
            var testCode = "," + $(item).data("code") + ",";
            if (testString.indexOf(testCode) >= 0) {
                $(item).prop("class", viewModel.config.selectionSelectedClass);
            } else {
                $(item).prop("class", viewModel.config.selectionAvailableClass);
            };
        });
    }
    else {
     //   console.log("bottom all");
        viewModel.toggleAllSpecies(true, []);
    };

};
viewModel.HideOrderLink = function (i) {   
        $("#" + viewModel.config.addToOrderDiv + i).hide();
};
viewModel.ShowOrderLink = function (i) {
    $("#" + viewModel.config.addToOrderDiv + i).show();
    $("#" + viewModel.config.addToOrderList + i).chosen({
        noneSelectedText: "Select Order",
        selectedList: 1,
        disable_search: true
    });
    $('.chzn-drop').css({ "width": "300px" });

};
  
viewModel.composeSpeciesList = function (callbacks) {
   // console.log("composeSpeciesList");
    var target = $("#" + viewModel.config.speciesListDisplayId);
    target.empty();
    var list = $("<ul id='speciesUL'></ul>");
    $.each(model.speciesData, function (index, value) {
    //    console.log("composeSpeciesList each");
        var li = $("<li />");
        li.prop("class", viewModel.config.selectionAvailableClass);
        li.prop("id", value.Guid);
        li.text(value.Name)
        li.data("code", value.Code);
        list.append(li);
     //   console.log("composeSpeciesList each done");
    });
    target.append(list);
    var listItems = target.children("ul");
    listItems = listItems.children("li");

    listItems.on("click", function () {
     //   console.log("composeSpeciesList compose click");
        var thisItem = $(this);
        var listItems = $("#" + viewModel.config.speciesListDisplayId + " ul").children("li");
        if (thisItem.prop("id") == viewModel.config.selectorAllSpeciesId) {
            //alert("you clicked all species");
            if (thisItem.hasClass(viewModel.config.selectionSelectedClass)) {
                listItems.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
            } else {
                listItems.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
            };
        } else {

            if (thisItem.hasClass(viewModel.config.selectionSelectedClass)) {
                thisItem.removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
                $("#" + viewModel.config.selectorAllSpeciesId).removeClass(viewModel.config.selectionSelectedClass).addClass(viewModel.config.selectionAvailableClass);
            } else {
                thisItem.removeClass(viewModel.config.selectionAvailableClass).addClass(viewModel.config.selectionSelectedClass);
            }
        }
        model.config.nextPageToDisplay = 1;
        //Put back in for Auto Display   viewModel.getDataDisplayGrid("");
    });

    viewModel.state.speciesListComposed = true;
  //  console.log("composeSpeciesList callbacks");
    viewModel.ProcessEventChain(callbacks);
};

viewModel.showOrderIcon = function (inputBox, icon) {
    viewModel.clearOrderIcon(inputBox);
    inputBox.after(icon).delay(10);
};
viewModel.clearOrderIcon = function (inputBox) {
    var elements = inputBox.siblings("img");
    elements.remove();
};
viewModel.resetOrderIcon = function (guid, inputBox) {
    var ndx = model.rowDataNdxFromProductGuid(guid);
    if (!model.isValidRowNdx(ndx)) { return false; };
    viewModel.clearOrderIcon(inputBox);

    var shipweek = inputBox.data('boxData').shipWeekCode
    // alert(shipweek);
    if (model.rowData[ndx].IsCarteds[shipweek]) {
        viewModel.showOrderIcon(inputBox, viewModel.config.icons.cart_checked);
        return true;
    };
    if (model.rowData[ndx].OrderQuantities[shipweek]>0) {
        viewModel.showOrderIcon(inputBox, viewModel.config.icons.not_carted);
        return true;
    };
};
viewModel.setAllOrderIconsToCart = function () {
    var inputBoxes = $("." + viewModel.config.orderQuantityInputClass);
    $("."+ viewModel.config.orderQuantityInputClass).each(function () {
        var theID = this.id;     
        var valu = $("#" + theID).val();     
        if (valu != null && valu != "") {
            //numeral.js cannot unformat empty or null string
            var round = Math.round;
            valu = parseInt(round(valu)) || 0;
        } else {
            valu = 0;
        };
        if (valu > 0)
        {
            viewModel.showOrderIcon($("#" + theID), viewModel.config.icons.cart_checked);
        }
    });
};

viewModel.clearAllOrderIcons = function () {
    var elements = $("." + viewModel.config.orderQuantityInputClass).siblings("img").remove();
};

viewModel.loadMoreResults = function () {
    viewModel.displayGridRows(false, false, true,"none");
};

viewModel.floatMessage = function (element, content, interval) {
    //floats message to right of element
    if (typeof element !== 'undefined' && element != null || element.length > 0) {
        var position = element.offset();
        //ToDo: code to center floating message vertically with element
        //assume floating message is 35 px tall

        var top = position.top + ((element.height() - 35) / 2);

        eps_tools.FloatingMessage(position.left + element.width() + 30, top, content, interval);
    };
};
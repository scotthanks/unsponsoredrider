﻿
model.open.SummaryData = [];
model.open.DetailData = {};
model.open.OrderHistoryData = {};

model.open.config = {
    urlOrderApi: '/api/order',
    urlGetSummaryData: "/api/Order/MyOpenOrders",
    urlGetDetailData: function (guid) { return model.open.config.urlOrderApi + "/DetailData?OrderGuid3=" + guid; },
    urlUpdateOrderQuantityApi: '/api/OrderLine',
    //urlDeleteOrder: function (guid) { return model.open.config.urlOrderApi + "/" + guid; },
    urlOrderPDF: '/api/order/OrderPDF?OrderGuid2=',
    urlGetDetailHistoryData: function (guid) { return model.open.config.urlOrderApi + "/DetailHistory?OrderGuid=" + guid; },
};

model.open.state = {
    currentGrowerOrderGuid: function () { return model.open.DetailData.OrderGuid || ""; },
    growerOrdersCount: function () {
        return model.open.SummaryData.length || 0;
    },
   
};

model.open.getSummaryData = function (callbacks) {
    //alert("open getSummaryData");
    $.ajax({
        datatype: "json",
        url: model.open.config.urlGetSummaryData + "?TheType2=Open",
        type: "get",
        beforeSend: function (jqXHR, settings) {
            model.open.SummaryData = [];
        },
        success: function (response, textStatus, jqXHR) {
            model.state.isReadOnly = response.IsReadOnly;
            model.open.SummaryData = response.SummaryData || [];
        },
        complete: function (jqXHR, textStatus) {
            vModel.ProcessEventChain(callbacks);
           
        }
    });
   
};
model.open.GetDetailData = function (guid, callbacks) {
    model.open.DetailData = {};
    model.open.DetailData.OrderGuid = guid;
    $.ajax({
        datatype: "json",
        url: model.open.config.urlGetDetailData(guid),
        type: "get",
        beforeSend: function (jqXHR, settings) {
        },
        success: function (response, textStatus, jqXHR) {
            console.log(response);
            model.open.DetailData = response;
 
        },
        complete: function (jqXHR, textStatus) {
            vModel.ProcessEventChain(callbacks);
        }
    });
};

model.open.writeOrderDescription = function (data, element, callbacks) {
    if (typeof data.OrderDescription === "undefined" || (data.OrderDescription || "") == (model.open.DetailData.OrderDescription || "")) {
        return false;
    };
    //alert("data: " + traverseObj(data, false));
    $.ajax({
        type: "post",
        url: model.open.config.urlCartUpdateApi,
        data: data,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
        },
        success: function (response, textStatus, jqXHR) {
            if (typeof response !== 'undefined' && response != null) {
                //ToDo: this interface stuff should be handled by a callback
                if (typeof element !== 'undefined' && element != null && element.length > 0) {
                    if (!response.IsAuthenticated) {
                        eps_tools.floatMessage(element, "You must be logged-in to use this page.", 2000);
                    } else if (response.Success) {
                        var ndx = model.open.getGrowerOrderNdx(data.GrowerOrderGuid);
                        model.open.SummaryData[ndx].OrderDescription = data.OrderDescription;

                        eps_tools.floatMessage(element, "Data updated.", 2000);
                    } else {
                        eps_tools.floatMessage(element, "Update Data: failed.", 2000);
                    }
                };
            };
        },
        complete: function (jqXHR, textStatus) {
            //alert(resultString);
            vModel.ProcessEventChain(callbacks);
        }
    });
};
model.open.getGrowerOrderNdx = function (guid) {
    var ndx = null;
    $.each(model.open.SummaryData, function (i, item) {
        if (item.OrderGuid == guid) {
            ndx = i;
        }
    });
    return ndx;
};
model.open.GetOrderLineFromGuid = function (orderLineGuid) {
    if (typeof orderLineGuid === 'undefined' || orderLineGuid == null || orderLineGuid.length != 36) { return null; };
    if (model.open.DetailData == null || model.open.DetailData.SupplierOrders.length == 0) { return false; };
    var orderLine = null;
    $.each(model.open.DetailData.SupplierOrders, function (i, order) {

        $.each(order.OrderLines, function (ndx, line) {
            if (line.Guid == orderLineGuid) {
                orderLine = line;
            }
        });
    });
    return orderLine;
};
model.open.changeOrderLineQuantity = function (guid, newQuantity) {
    var orderLine = model.open.GetOrderLineFromGuid(guid);
    if (orderLine != null) {
        orderLine.QuantityOrdered = newQuantity;
    };
};
////ToDo: This is deprecated, delete it
//model.open.getOpenOrderHistory = function (callbacks) {
//    //alert("getOpenOrderHistory");
//    model.open.OrderHistoryData = { "Order History": "TBD" };

//    vModel.ProcessEventChain(callbacks);
//};
model.open.returnSupplierOrder = function (supplierOrderGuid) {
    if (typeof supplierOrderGuid !== 'string' || supplierOrderGuid.length != 36) { return false; }
    var supplierOrder = null;
    $.each(model.open.DetailData.SupplierOrders, function (i, order) {
        if (order.SupplierOrderGuid == supplierOrderGuid) { supplierOrder = order; return false };
    });
    return supplierOrder;
};
model.open.ReturnSelectedTagRatio = function (supplierOrderGuid) {
    if (typeof supplierOrderGuid !== 'string' || supplierOrderGuid.length != 36) { return false; }
    var selectedTagRatio = null;
    var supplierOrder = model.open.returnSupplierOrder(supplierOrderGuid);
    if (supplierOrder == null) { return null; };

    $.each(supplierOrder.TagRatioList, function (i, tagRatio) {
        if (tagRatio.IsSelected) {
            selectedTagRatio = tagRatio.Name;
            return false;
        } else if (selectedTagRatio == null) {
            if (i == 0 || tagRatio.IsDefault) { selectedTagRatio = tagRatio.Name; };
        };
    });
    return selectedTagRatio;
};
model.open.ReturnSelectedShipMethod = function (supplierOrderGuid) {
    if (typeof supplierOrderGuid !== 'string' || supplierOrderGuid.length != 36) { return false; }
    var selectedShipMethod = null;
    var supplierOrder = model.open.returnSupplierOrder(supplierOrderGuid);
    if (supplierOrder == null) { return null; };

    $.each(supplierOrder.ShipMethodList, function (i, shipMethod) {
        if (shipMethod.IsSelected) {
            selectedShipMethod = shipMethod.Name;
            return false;
        } else if (selectedShipMethod == null) {
            if (i == 0 || shipMethod.IsDefault) { selectedShipMethod = shipMethod.Name; };
        };
    });
    return selectedShipMethod;
};
model.open.ReturnSupplierOrderGuid = function (orderLineGuid) {
    if (orderLineGuid == null || orderLineGuid.length < 36) { return "" };
    if ($("#" + orderLineGuid).length < 1) { return "" };
    var returnGuid = "";
    $.each(model.open.DetailData.SupplierOrders, function (i, order) {
        $.each(order.OrderLines, function (i, line) {
            if (line.Guid == orderLineGuid) {
                returnGuid = order.SupplierOrderGuid;
            };
        });
    });
    return returnGuid;
};



////////////////////////////// vModel //////////////////////////////////////

vModel.open.config = {
    ordersContainerId: "open_orders_summary_container",

    //openOrdersDisplayId: 'open_orders_display',

    OrderButtonClass: "order_buttons",
    DeleteButtonClass: "delete_buttons",
    OrderHeadingStem: "order_heading_",      //Gets Order Guid Appended

    

    Buttons: {
        actionButtonsClass: "action_buttons",
        actionButtonsDisabledClass: "action_buttons_disabled",
    },


    summaryGrid: {
        id: "open_order_summary_grid",
        rowClass: "order_summary_grid_rows",
        Columns: 8,     //ToDo: move to state and calculate from template during init
        detailRowId: "order_detail_row",
        detailContainerId: "open_order_detail_container",
    },

    detailDisplay: {
        id: "open_orders_detail_display",

        templateId: "open_orders_detail_display_template",
        template: null,

        headerBarClass: "header_bars",
        openOrderDetailHeaderBarId: "open_order_detail_header_bar",
        OrderStatusId: "open_order_status_span",
        OrderNumberId: "open_order_number_span",

        openOrderHistoryContainerId: "open_orders_history_container",
        PurchaseOrderInput: "open_order_purchase_order_input",
        OrderDescriptionInput: "open_order_description_input",

        ShipToTableId: "ship_to_table",
        ShipToIdStem: "ship_to_span_",

        PaymentMethodIdStem: "payment_method_span_",

        ///////////////grower order totals /////////////////
        TotalCuttingsSpanId: "open_grower_order_total_cuttings_span",
        TotalCuttingsCostSpanId: "open_grower_order_total_cuttings_cost_span",
        OrderDescriptionsClass: "order_descriptions",

        orderTotalsGrid: {
            id: "open_orders_grower_order_totals_table",

            growerOrderFeesRowTemplateId: "open_orders_grower_order_fees_row_template",
            growerOrderFeesRowTemplate$: null,

            growerOrderFeeInfoSpans: "grower_order_fee_info_icons",
            growerOrderFeeStatusDescription: "grower_order_fee_status_description",
            growerOrderFeeTypeDescription: "grower_order_fee_type_description",
            growerOrderFeeSpansClass: "grower_order_fee_spans",

            totalTraysSpanId: "grower_order_total_trays_span",
            totalCuttingsSpanId: "grower_order_total_cuttings_span",
            totalCuttingsCostSpanId: "grower_order_total_cuttings_cost_span",
        },

        ButtonContinueShoppingId: "continue_shopping_button",
        ButtonCancelAllId: "cancel_order_button",

    },

    SupplierOrders: {
        TemplateId: "open_orders_supplier_orders_template",
        template: null,

        ContainerId: "open_order_supplier_orders_container",
        DisplaysClass: "supplier_order_displays",
        SupplierNameSpanClass: "supplier_name_span",

        tagRatioDisplayClass: "supplier_order_tag_ratio_display",
        tagRatioSelectedClass: "tagRatioSelectedSpans",
        shipMethodDisplayClass: "supplier_order_ship_method_display",
        shipMethodSelectedClass: "shipMethodSelectedSpans",

        LineItemGrid: {
            ClassName: "line_item_grids",
            RowClass: "line_item_grid_rows",

            dynamicCostRowsTemplateId: "open_orders_dynamic_cost_rows_template",
            dynamicCostRowsTemplate$: null,

            subTotalRowsClass: "product_cost_rows",
            dynamicCostRowsClass: "dynamic_cost_rows",
            totalCostRowsClass: "total_cost_rows",

            supplierOrderFeeInfoSpans: "supplier_order_fee_info_icons",
            supplierOrderFeeStatusDescription: "supplier_order_fee_status_description",
            supplierOrderFeeTypeDescription: "supplier_order_fee_type_description",
            lineItemFooterCostsClass: "line_item_footer_costs",
            supplierOrderTotalCostsClass: "supplier_order_total_costs",

            OrderQuantityLockedClass: "line_item_grid_order_quantity_locked_inputs",
            OrderQuantityInputClass: "line_item_grid_order_quantity_inputs",
            OrderQuantityTotalClass: "supplier_order_quantity_totals",
            orderLineStatusesClass: "order_line_statuses",

            OrderQuantityImgClass: "line_item_grid_order_quantity_imgs",

            PriceTotalsClass: "supplier_order_product_cost_totals",

            IdStemHeaderRows: "headerRow_",
            ClassStemHeaderCols: "headerCol_",
            IdStemFooterRows: "footerRow_",
            ClassStemFooterCols: "footerCol_",
            Columns: 8,
            ShowHeadings: true,
            Headings: ["Species", "Product", "Trays--", "Quantity", "Unit Price", "Price", "Status", "Cancel"],   //deprecated
            ColumnWidths: ["12.5%", "12.5%", "12.5%", "12.5%", "12.5%", "12.5%", "12.5%", "12.5%"],
            FooterRows: 1,
            ColsClassStemHeader: "headerCol_",
            ColumnClassStemBody: "bodyCol_",
            ColumnClassStemFooter: "footerCol_",
            ToggleRemoveSpanClass: "toggle_remove_spans",
            ToggleRemoveSelectedClass: "toggle_remove_selected",
            ToggleLockedRowClass: "toggle_locked_selected",
            removeEnabledRowsClass: "remove_enabled_rows",

            trayTotalsClass: "supplier_order_tray_totals",
            supplierNameTotalClass: "supplier_name_span_total",

            cancellableRows: "cancellable_rows",
            uncancelledRows: "un_cancelled_rows",
            cancelledRows: "cancelled_rows",
            lockedForChangesRows: "locked_for_changes_rows",
            lockedForReductionRows: "locked_for_reduction_rows",
            cancelSpansClass: "cancel_spans_class",
        }
    },

};
vModel.open.state = {
    displayedOrderGuid: function () { return $("#" + vModel.open.config.summaryGrid.detailContainerId).data("orderGuid") || ""; },
    detailDisplay$: function () {
        return $("#" + vModel.open.config.detailDisplay.id) || null;
    },
};


vModel.open.init = function () {
   
    ////////////////ACTION BUTTONS
    $("#" + vModel.open.config.detailDisplay.ButtonContinueShoppingId).on("click", function () {
        var link = vModel.continueShoppingLink();
       
       
        window.location.href = link;
    });

    $("#" + vModel.open.config.detailDisplay.ButtonCancelAllId).on("click", function () {
        $.each(model.open.DetailData.SupplierOrders, function (i, order) {
            $.each(order.OrderLines, function (i, orderLine) {
                alert(orderLine.Guid);
            });
        });
    });


    $("#get_confirmation_button").on("click", function () {
        
        var theURL = $("#get_confirmation_button").attr('href');
       // alert(theURL);
        $.ajax({
            url: theURL, //or your url
            type: 'HEAD',
            success: function (data) {
               // alert('exists');
                $("#get_confirmation_span").hide();
                
            },
            error: function (data) {
                //alert('does not exist');
                $("#get_confirmation_span").show();
               
            },
        })

        //var orderGuid = vModel.open.state.displayedOrderGuid();
    //   // alert(orderGuid);
    //    var link = "/MyAccount/OrdersPDF?OrderGuid=" + orderGuid;
    ////    alert(link);
    //    window.open(link, "_blank")
         
        //vModel.GetPDF(orderGuid);
      
    });

    ////Clone and Delete Supplier Order Template From Open Order Detail Template
    var supplierOrdertemplate = $("#" + vModel.open.config.SupplierOrders.TemplateId)
    supplierOrdertemplate.find("." + vModel.open.config.SupplierOrders.LineItemGrid.ClassName + " tbody").empty();
    vModel.open.config.SupplierOrders.template = supplierOrdertemplate.clone(true);;
    supplierOrdertemplate.remove();

    //Clone and delete detail display template
    var detailDisplayTemplate = $("#" + vModel.open.config.detailDisplay.templateId);
    detailDisplayTemplate.prop({ "id": vModel.open.config.detailDisplay.id });
    vModel.open.config.detailDisplay.template = detailDisplayTemplate.clone(true);
    detailDisplayTemplate.remove();

    var onExpandOpenSummary = function (containerId) {
        eps_expanse.state.setWorking(containerId);
        var display = eps_expanse.state.getDisplayElement(containerId).empty();
        model.open.getSummaryData([function () {
            if (model.open.state.growerOrdersCount() == 0) {
                eps_expanse.setStatus(containerId, "Open Orders: 0");
                eps_expanse.state.setCollapsed(containerId);
            } else {
                var table = vModel.config.ordersSummaryTable$.clone(true);
                table.prop({
                    "id": "open_orders_summary_grid"
                });
                display.html(table);
                table.children("tbody").replaceWith(vModel.open.composeSummaryRows());
                eps_expanse.setStatus(containerId, "Open Orders: " + model.open.state.growerOrdersCount());
                eps_expanse.expand(containerId, null);
            };
        }]);
    };
    var onCollapseOpenSummary = function (containerId) {
        eps_expanse.state.setWorking(containerId);
        eps_expanse.collapse(containerId, null, true);
    };
    eps_expanse.init(vModel.open.config.ordersContainerId, onExpandOpenSummary, onCollapseOpenSummary, "Open Orders", "Searching...");
    if (vModel.config.PageMode == "all" || vModel.config.PageMode == "open") {
        var containerId = vModel.open.config.ordersContainerId;
        onExpandOpenSummary(containerId);
    } else {
        model.open.getSummaryData([function () {
            eps_expanse.setStatus(vModel.open.config.ordersContainerId, "Open Orders: " + model.open.SummaryData.length);
        }]);       
    };
};
vModel.open.composeSummaryRows = function () {
    var tbody = $("<tbody />");
    $.each(model.open.SummaryData, function (i, item) {
        tbody.append(vModel.composeSummaryRow(item));
    });
    vModel.open.defineSummaryRowEvents(tbody);
    return tbody;
};

vModel.open.updateSummaryRowPO = function (orderGuid, customerPO) {
    $('#' + orderGuid + "_CustomerPoNo").html(customerPO);
}
vModel.open.updateSummaryRowDescription = function (orderGuid, description) {
    $('#' + orderGuid + "_OrderDescription").html(description);
}
vModel.open.defineSummaryRowEvents = function (tbody) {
    var rows = tbody.children("tr." + vModel.open.config.summaryGrid.rowClass);
    $(rows).on("click", function () {
        var orderGuid = $(this).prop("id");
        vModel.open.RemoveDetail([
            function () {               
                vModel.open.insertDetailDisplay(orderGuid);             
            }
        ]);    
    });
};
vModel.open.insertDetailDisplay = function (orderGuid) {
    var currentDetailContainer = $("#" + vModel.open.config.summaryGrid.detailContainerId)
    var currentlyDisplayedOrderGuid = currentDetailContainer.data("orderGuid") || "";
    if (orderGuid != currentlyDisplayedOrderGuid) {
        $("#" + orderGuid + " td.td_0").css("background-image", "url('/Images/icons/Loading_Availability.gif')");
      //  alert("row clicked [ Order Guid: " + orderGuid + " ]");

        //insert row in grid spanning all columns
        var row = $("<tr></tr>")
            .prop({ "id": vModel.open.config.summaryGrid.detailRowId })
            .css({ "display": "none", "border": "none" });

        var td = $("<td></td>")
            .prop({ "colspan": vModel.open.config.summaryGrid.Columns });

        var div = $("<div />")
            .prop({ "id": vModel.open.config.summaryGrid.detailContainerId })
            .data("orderGuid", orderGuid);

        td.append(div);
        row.append(td);

        row.insertAfter("#" + orderGuid);
        model.open.GetDetailData(orderGuid, [function () {
            vModel.open.DisplayDetail(row);
        }]);
    };
};
vModel.open.DisplayDetail = function (row) {
    var target = $("#" + vModel.open.config.summaryGrid.detailContainerId);
    var detailDisplay = vModel.open.config.detailDisplay.template.clone(true);
    
    target.append(detailDisplay);
    vModel.open.RefreshDetailDisplay();
    row.fadeIn("slow");
};
vModel.open.RemoveDetail = function (callbacks) {
    //clear detail display
    var currentDetailContainer = $("#" + vModel.open.config.summaryGrid.detailContainerId)
    var currentlyDisplayedOrderGuid = currentDetailContainer.data("orderGuid") || "";
    if (currentlyDisplayedOrderGuid != "") {
        var displayRow = $("#" + vModel.open.config.summaryGrid.detailRowId);
        var detailDisplay = $("#" + vModel.open.config.detailDisplay.id);

        detailDisplay.hide("blind", {}, 500, function () {
            //restore row icon
            $("#" + currentlyDisplayedOrderGuid + " td.td_0").css("background-image", "url('/Images/icons/ArrowRightCart.gif')");

            detailDisplay.remove();
            displayRow.remove();
        });
    };
    //clear data
    model.open.DetailData = {};
    //alert(callbacks);
    vModel.ProcessEventChain(callbacks);
};
vModel.open.RefreshDetailDisplay = function () {
    var url = "https://epsstorage.blob.core.windows.net/gfconfirmations/" + model.open.DetailData.OrderNo + "_" + model.open.DetailData.OrderGuid + ".pdf";
    $("#get_confirmation_button").attr("href", url);
    //set heading
    var poString = "";
    if (model.open.DetailData.CustomerPoNo != null)
    {
        poString = " - " + model.open.DetailData.CustomerPoNo ;
    }
    $("#" + vModel.open.config.detailDisplay.openOrderDetailHeaderBarId).text(
        "Week " + vModel.ShipWeekCodeToShipWeekString(model.open.DetailData.ShipWeekCode) + " - " +
        (model.open.DetailData.OrderNo || null) + 
        poString
        );
    //set status:
    $("#" + vModel.open.config.detailDisplay.OrderStatusId).text(model.open.DetailData.GrowerOrderStatusName || '');
    //set order number
    $("#" + vModel.open.config.detailDisplay.OrderNumberId).text(model.open.DetailData.OrderNo || '');
    //set PO
    $("#" + vModel.open.config.detailDisplay.PurchaseOrderInput).val(model.open.DetailData.CustomerPoNo || '');
    vModel.open.wireUpPurchaseOrderInput();
    //set Description
    $("#" + vModel.open.config.detailDisplay.OrderDescriptionInput).val(model.open.DetailData.OrderDescription || '');
    vModel.open.wireUpOrderDescriptionInput();
    //set ship-to
    var selectedShipTo = (function () {
        var shipToAddresses = model.open.DetailData.ShipToAddresses || [];
        var shipToAddress = null;
        for (var i = 0, len = shipToAddresses.length; i < len; i++) {
            shipToAddress = shipToAddresses[i];  //take first address found
            if (shipToAddresses[i].IsSelected) {
                return shipToAddresses[i];  //object found return it
            } else if (shipToAddresses[i].IsDefault) {
                shipToAddress = shipToAddresses[i];  //update default to this
            };
        }
        return shipToAddress; //not found
    })();
    if (selectedShipTo !== null) {
        $("#" + vModel.open.config.detailDisplay.ShipToIdStem + "0").text(selectedShipTo.Name || '');
        //address line 1
        var t = $("#" + vModel.open.config.detailDisplay.ShipToIdStem + "1");
        var v = selectedShipTo.StreetAddress1 || null;
        if (v != null) { t.text(v); } else { t.remove(); };
        //address line 2
        t = $("#" + vModel.open.config.detailDisplay.ShipToIdStem + "2");
        v = selectedShipTo.StreetAddress2 || null;
        if (v != null) {
            t.text(v);
        } else {
            $(t).parents("tr:first").remove();
        };
        //city, state zip        
        $("#" + vModel.open.config.detailDisplay.ShipToIdStem + "3").text(
            (selectedShipTo.City || null) + ", " +
            (selectedShipTo.StateCode || null) + " " +
            (selectedShipTo.ZipCode || null)

            );
        $("#" + vModel.open.config.detailDisplay.ShipToIdStem + "4").text(selectedShipTo.Country || null);
    }
    //set payment method
    var selectedPaymentMethod = (function () {
        var method = {};
        method.Type = null;
        method.Card = null;
        $.each(model.open.DetailData.PaymentTypeList, function (i, type) {
            if ((type.IsDefault && method.Type == null) || type.IsSelected) { method.Type = type.DisplayName; };
        });
        if (method.Type == "Credit Card") {
            $.each(model.open.DetailData.CreditCards, function (i, card) {
                if ((card.IsDefault && method.Card == null) || card.IsSelected) {
                    method.Card = card.CardType + " ending: " + card.CardNumber
                };
            });
        };
        return method; //not found
    })();
    if (selectedPaymentMethod != null) {
        $("#" + vModel.open.config.detailDisplay.PaymentMethodIdStem + "0").text(selectedPaymentMethod.Type || '');
        $("#" + vModel.open.config.detailDisplay.PaymentMethodIdStem + "1").text(selectedPaymentMethod.Card || '');
    };

    //Initialize Order History
    var onExpandOrderHistory = function (containerId) {
        eps_expanse.state.setWorking(containerId);
        eps_expanse.setStatus(containerId, "Loading ...");
        var display = eps_expanse.state.getDisplayElement(containerId).empty();
        var orderGuid = vModel.open.state.displayedOrderGuid();
        var onSuccess = function (data) {
            model.open.OrderHistoryData = data;
        };

        var uri = model.config.urlGetDetailHistoryData(orderGuid);
        model.open.OrderHistoryData = {};
        model.ajaxCall(null, uri, "get", onSuccess, [function () {
            var historyData = model.open.OrderHistoryData.eventList || [];
            vModel.composeHistoryData(display, orderGuid, historyData);
            eps_expanse.setStatus(containerId, "Changes: " + historyData.length);
            eps_expanse.expand(containerId, null);
        }]);
    };
    var onCollapseOrderHistory = function (containerId) {
        eps_expanse.state.setWorking(containerId);
        eps_expanse.collapse(containerId, null, true);
        eps_expanse.setStatus(containerId, "...");
    };

    eps_expanse.init(vModel.open.config.detailDisplay.openOrderHistoryContainerId, onExpandOrderHistory, onCollapseOrderHistory, "Order History", "...");

    //Clear Supplier Orders from Detail Display
    $("#" + vModel.open.config.SupplierOrders.ContainerId).empty();

    //Add Supplier Orders
    $.each(model.open.DetailData.SupplierOrders, function (i, order) {
        var clone = vModel.open.config.SupplierOrders.template.clone(true);
        clone.prop({
            "id": order.SupplierOrderGuid
        });
        clone.find("." + vModel.open.config.SupplierOrders.SupplierNameSpanClass).text(order.SupplierName);

        ////// Tag Ratio ////////
        var tagRatioDisplay = $(clone).find("." + vModel.open.config.SupplierOrders.tagRatioDisplayClass)
        $(tagRatioDisplay).children("." + vModel.open.config.SupplierOrders.tagRatioSelectedClass).text(model.open.ReturnSelectedTagRatio(order.SupplierOrderGuid));

        //// Ship Method /////
        var shipMethodDisplay = $(clone).find("." + vModel.open.config.SupplierOrders.shipMethodDisplayClass)
        $(shipMethodDisplay).children("." + vModel.open.config.SupplierOrders.shipMethodSelectedClass).text(model.open.ReturnSelectedShipMethod(order.SupplierOrderGuid));

        ///// Line Item Grid: Body Section /////
        var theDiv = $("<div />");
        var thrDivProperties = { "class": "line_item_grids" };
        theDiv.prop(thrDivProperties);
  

  
        var orderDiv = $("<div />");
        var orderDivProperties = { "class": "cart_line_item" };
        orderDiv.prop(orderDivProperties);

        vModel.addSupplierOrderLinesToDiv(orderDiv, order);
       
        
        theDiv.append(orderDiv);

        var productTotalsDiv = $("<div />");
        var productTotalsDivProperties = { "id": "supplier_totals_count" };
        productTotalsDiv.prop(productTotalsDivProperties);

        var ul = $("<ul>");
        var ulProperties = {"id": "supplier_totals_count_list" };
        ul.prop(ulProperties);
        
      
        var li = $("<li />");
        var span = $('<span />');
        span.prop({
            "id": order.SupplierOrderGuid + "_supplier_order_tray_totals",
            "class": "supplier_order_tray_totals",
        });
        li.text("Total Trays: ");
        li.append(span);
        ul.append(li);                    
 
        var li = $("<li />");
        var span = $('<span />');
        span.prop({
            "id": order.SupplierOrderGuid + "_supplier_order_quantity_totals", "class": "supplier_order_quantity_totals",
        });
        li.text("Total Qty: ");
        li.append(span);
        ul.append(li);                    


        productTotalsDiv.append(ul);
        //theDiv.append(productTotalsDiv);

        var supplierSubtotalsDiv = $("<div />");
        var supplierSubtotalsDivProperties = { "id": "supplier_subtotals" };
        supplierSubtotalsDiv.prop(supplierSubtotalsDivProperties);

        var ul = $("<ul>");
        var ulProperties = { "id": "supplier_product_subtotal",
            "class" : "supplier_subtotal_boxes"};
        ul.prop(ulProperties);

        var li = $("<li />");
        var liProperties = { "class": "left" };
        var span = $('<span />');
        span.prop({
            "class": "supplier_name_span2",
            "id": order.SupplierOrderGuid + "_supplier_name_span"
        });
        li.append(span);
        ul.append(li);

        var li = $("<li />");
        var liProperties = { "class": "right" };
        var span = $('<span />');
        span.prop({
            "id": order.SupplierOrderGuid + "_supplier_order_product_cost_totals"
            , "class": "supplier_order_product_cost_totals",
        });   
        li.append(span);
        ul.append(li);  
        supplierSubtotalsDiv.append(ul);

        
        $(clone).children("#supplier_fees").remove();
        var supplieFeesDiv = $("<div />");
        var supplieFeesDivProperties = { "id": order.SupplierOrderGuid +  "_supplier_fees" };
        supplieFeesDiv.prop(supplieFeesDivProperties);
       // supplieFeesDiv.html("<p>hi Scott</p>");
        supplierSubtotalsDiv.append(supplieFeesDiv);
       
        
        $(clone).children("#supplier_totals").remove();
        var supplieSubtotalsDiv = $("<div />");
        var supplieSubtotalsDivProperties = { "id": order.SupplierOrderGuid + "_supplier_totals" };
        supplieSubtotalsDiv.prop(supplieSubtotalsDivProperties);
        // supplieFeesDiv.html("<p>hi Scott</p>");
        supplierSubtotalsDiv.append(supplieSubtotalsDiv);

      

        $(clone).children("." + vModel.open.config.SupplierOrders.LineItemGrid.ClassName).replaceWith(theDiv);
        $(clone).children("#supplier_totals_count").replaceWith(productTotalsDiv);
        $(clone).children("#supplier_subtotals").replaceWith(supplierSubtotalsDiv);

        

        $("#" + vModel.open.config.SupplierOrders.ContainerId).append(clone);
    });

    vModel.refreshTotalsCascade("open",null);  //null value means refresh all supplier orders
    vModel.open.defineInputBoxEvents();
    $("#" + model.open.state.currentGrowerOrderGuid() + " td.td_0").css("background-image", "url('/Images/icons/ArrowDownCart.gif')");
    vModel.open.setRowStatusAndEvents();
}
vModel.open.wireUpOrderDescriptionInput = function () {
    $("#" + vModel.open.config.detailDisplay.OrderDescriptionInput)
        .off()
        .on("change", function () {
            var self = $(this);
            orderGuid = vModel.open.state.displayedOrderGuid();
            description = self.val().trim();
            if (!model.state.orderDescriptionUpdating) {
                model.state.orderDescriptionUpdating = true;         
                var data = {
                    GrowerOrderGuid: orderGuid,
                    FieldName : "OrderDescription",
                    OrderDescription: description
                };
                model.writeOrderData(data, self, [function () {
                    //alert("change");
                    model.state.orderDescriptionUpdating = false;
                }]);
                vModel.open.updateSummaryRowDescription(orderGuid, description)
            };
        })
        .on("keydown", function (e) {
            var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
            //alert("key: " + key);
            //if (key == 13 || key == 9) {
            if (key == 13) {
                e.preventDefault();
                var self = $(this);
                orderGuid = vModel.open.state.displayedOrderGuid();
                description = self.val().trim();
                if (!model.state.orderDescriptionUpdating) {               
                    var data = {
                        GrowerOrderGuid: orderGuid,
                        FieldName: "OrderDescription",
                        OrderDescription: description
                    };
                    model.writeOrderData(data, self, [function () {
                        model.state.orderDescriptionUpdating = false;
                    }]);
                    vModel.open.updateSummaryRowDescription(orderGuid, description)
                };
            };
        })
        .on("focus", function (e) {
        });

};
vModel.open.wireUpPurchaseOrderInput = function () {
    $("#" + vModel.open.config.detailDisplay.PurchaseOrderInput)
        .off()
        .on("change", function () {
            var self = $(this);
            orderGuid = vModel.open.state.displayedOrderGuid();
            customerPoNo = self.val().trim();
            if (!model.state.orderPurchaseOrderUpdating) {
                model.state.orderPurchaseOrderUpdating = true;
                var data = {
                    GrowerOrderGuid: orderGuid,
                    FieldName: "CustomerPoNo",
                    CustomerPONO: customerPoNo
                };
                model.writeOrderData(data, self, [function () {
                    model.state.orderPurchaseOrderUpdating = false;
                }]);
                vModel.open.updateSummaryRowPO(orderGuid, customerPoNo)
            };
        })
        .on("keydown", function (e) {
            var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
            //alert("key: " + key);
            //if (key == 13 || key == 9) {
            if (key == 13) {
                e.preventDefault();
                var self = $(this);
                orderGuid = vModel.open.state.displayedOrderGuid();
                customerPoNo = self.val().trim();
                if (!model.state.orderPurchaseOrderUpdating) {                  
                    var data = {
                        GrowerOrderGuid: orderGuid,
                        FieldName: "CustomerPoNo",
                        CustomerPONO: customerPoNo
                    };
                    model.writeOrderData(data, self, [function () {
                        model.state.orderPurchaseOrderUpdating = false;
                    }]);
                    vModel.open.updateSummaryRowPO(orderGuid, customerPoNo)
                };
            };
        })
        .on("focus", function (e) {
        });      
};



vModel.open.setRowStatusAndEvents = function (orderLineGuid) {
    //if orderLineGuid is undefined then all rows will be set
    var type = typeof orderLineGuid;
    if (type !== "undefined" && type !== "string" && orderLineGuid.length != 36) { return false; };
    var rows = null;
    if (type === "undefined") {
        rows = $("#" + vModel.open.config.SupplierOrders.ContainerId + " ." + vModel.open.config.SupplierOrders.LineItemGrid.ClassName + " tbody tr");
    } else {
        rows = $("." + vModel.open.config.SupplierOrders.LineItemGrid.ClassName + " tbody tr#" + orderLineGuid);
        //alert("bingo");
    };

   // console.log(rows);

    //alert("rows: " + rows.length || 0);
    $.each(rows, function (i, item) {
        //alert("bingo");
        var row = $(item)
            .removeClass().addClass(vModel.open.config.SupplierOrders.LineItemGrid.RowClass);
        var guid = row.prop("id");
        //alert("guid: " + guid);

        console.log(guid);
        var line = model.open.GetOrderLineFromGuid(guid);
        console.log(line);

        var span = $('tr#' + guid + " td span." + vModel.open.config.SupplierOrders.LineItemGrid.cancelSpansClass)
            .off();

        if (line.IsLockedForAllChanges) {
            row.toggleClass(vModel.open.config.SupplierOrders.LineItemGrid.lockedForChangesRows);
            span.prop({
                "title": "Quantity locked for changes.",
            })
        } else if (line.IsLockedForReduction) {
            row.toggleClass(vModel.open.config.SupplierOrders.LineItemGrid.lockedForReductionRows);
            span.prop({
                "title": "Quantity locked for reductions.",
            })
        } else if (line.QuantityOrdered == 0) {
            row.toggleClass(vModel.open.config.SupplierOrders.LineItemGrid.cancelledRows);
            span.prop({
                "title": "Restore this line.",
            })
            .off()
            .on("click", function () {
                var rowElement = $(this).closest("tr");
                var guid = rowElement.prop("id");
                //alert("cancelled row: " + guid);
                rowElement.removeClass(vModel.open.config.SupplierOrders.LineItemGrid.cancelledRows).addClass(vModel.open.config.SupplierOrders.LineItemGrid.uncancelledRows);
                var inputBox = $("tr#" + guid + " td input." + vModel.open.config.SupplierOrders.LineItemGrid.OrderQuantityInputClass);
                $(inputBox).val(0).focus();     //.change()
                //$(this).off("click");

            });
        } else {
            row.toggleClass(vModel.open.config.SupplierOrders.LineItemGrid.cancellableRows);
            span.prop({
                "title": "Reduce quantity to zero.",
            })
            .off()
            .on("click", function () {
                var rowElement = $(this).closest("tr");
                var guid = rowElement.prop("id");
                //alert("cancellable row: " + guid);
                var inputBox = $("tr#" + guid + " td input." + vModel.open.config.SupplierOrders.LineItemGrid.OrderQuantityInputClass);
                //if (inputBox.length == 0) { return false; };  //no such element
                $(inputBox).val(0).change();
            });
        };
    });
};
/////////////// Order Line Quantity Code ///////////////
vModel.open.defineInputBoxEvents = function () {
    $('.' + vModel.open.config.SupplierOrders.LineItemGrid.OrderQuantityInputClass)
        .on("keydown", function (e) {
            var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
            if (key == 13) {
                e.preventDefault();
                var guid = $(this).parents("ul").prop('id');
                //note: Chrome and FireFox fire keydown and change events, this is handled by code downstream               
                vModel.open.handleOrderQuantityChange(guid);
            }
        })
        .on("change", function (e) {
            var guid = $(this).parents("ul").prop('id');
            vModel.open.handleOrderQuantityChange(guid);
        })
        .on("focus", function (e) {
            $(this).select();
        });
};
vModel.open.handleOrderQuantityChange = function (orderLineGuid) {
   // alert("orderLineGuid: " + orderLineGuid);
   // if (vModel.open.state.orderQuantityBeingHandled) return false;    //avoid keydown and change events both firing
    if (orderLineGuid == null || orderLineGuid.length != 36) { return false; };
  
    //get input box or return false
    var inputBox = $("#" + orderLineGuid + "_QtyInput");
    if (inputBox.length == 0) {
        return false; //no such element
    }; 

    var orderLineData = model.open.GetOrderLineFromGuid(orderLineGuid);
    if (orderLineData == null) { return false; };

    //validation complete, work begins - dispable this method
    //vModel.open.state.orderQuantityBeingHandled = true;

    var currentQuantityOrdered = orderLineData.QuantityOrdered;

    //get flags
    var lockedForReduction = orderLineData.IsLockedForReduction;
    var lockedForAllChanges = orderLineData.IsLockedForAllChanges;
    //get value requested
    var valu = inputBox.val();
    if (valu != null && valu != "") {
        var round = Math.round;
        valu = parseInt(round(valu)) || 0;
    } else {
        valu = 0;
    };
    //process value vs. flags and current quantity
    var noChange = false;
    var msg = null;
    if (valu == currentQuantityOrdered) { noChange = true; }
    else if (lockedForAllChanges) { noChange = true; msg = "Locked for all changes"; }
    else if (valu < currentQuantityOrdered && lockedForReduction && !lockedForAllChanges) { noChange = true; msg = "Locked for reduction"; };

    //handle no change
    if (noChange) {
        inputBox.val(numeral(currentQuantityOrdered).format('0,0'));
        vModel.open.showQuantityCheckIcon(orderLineGuid);
        if (msg != null) { eps_tools.floatMessage(inputBox, msg, 2000); }
        //vModel.open.state.orderQuantityBeingHandled = false;
        return false;
    };

    //update server
    vModel.open.showQuantityUpdatingIcon(orderLineGuid);
    //compose data
    var data = [{
        GrowerOrderGuid: model.open.state.currentGrowerOrderGuid(),
        OrderLineGuid: orderLineGuid,
        Quantity: valu,
        OriginalQuantity: currentQuantityOrdered,
        ProductDescription: orderLineData.ProductDescription,
    }];
    console.log(data);
    var serverResponse = null;

    $.ajax({
        datatype: "json",
        url: model.open.config.urlUpdateOrderQuantityApi,
        type: "PUT",
        data: { OrderLineUpdates: data },
        context: document.body,
        beforeSend: function (jqXHR, settings) {
            //alert(JSON.stringify(this.data));
        },
        success: function ( response, textStatus, jqXHR) {
            serverResponse = response;
        },
        fail: function (response, textStatus) {
            var errorMsg = "url: " + model.open.config.urlUpdateOrderQuantityApi + "<br />status: " + textStatus + "<br />error:  Update Data: failed.";
            eps_tools.floatMessage(inputBox, errorMsg, 4000);
            vModel.open.clearOrderIcon(orderLineGuid);
            //vModel.open.state.orderQuantityBeingHandled = false;
        },
        complete: function (jqXHR, textStatus) {
            var statuses = serverResponse.OrderLineStatuses;
            var orderLineGuid = serverResponse.OrderLineStatuses[0].OrderLineGuid;
            if (!serverResponse.IsAuthenticated) {
                //user not authenticated
                eps_tools.floatMessage(inputBox, "You must be logged-in to use this page.", 2000);
            }
            else if (serverResponse.Success && serverResponse.Message != "") {
                if (currentQuantityOrdered != serverResponse.OrderLineStatuses[0].QuantityOnOrder)
                {
                    $("#" + orderLineGuid + "_Status").text("Grower Edit");
                    
                }
                //success with server message about outcome
                eps_tools.floatMessage(inputBox, serverResponse.Message, 2000);
            } else if (serverResponse.Success) {
               
                //success, no message
              //  eps_tools.floatMessage(inputBox, "Order Updated.", 2000);
                eps_tools.floatMessage(inputBox, serverResponse.Message, 2000);

            } else if (sserverResponse.Message != "") {
               
                //not successful with server message
                eps_tools.floatMessage(inputBox, serverResponse.Message, 2000);
            } else {
                //not successful, no server message
                eps_tools.floatMessage(inputBox, "Unable to change value.", 2000);
            };

            model.open.changeOrderLineQuantity(orderLineGuid, serverResponse.OrderLineStatuses[0].QuantityOnOrder);
            //update input box
            inputBox.val(numeral(serverResponse.OrderLineStatuses[0].QuantityOnOrder).format('0,0'));
            //get data and row
            var line = model.open.GetOrderLineFromGuid(orderLineGuid);
            var row = $("#" + orderLineGuid);

            //update trays
            var trays = (line.QuantityOrdered / line.Multiple) || 0;
         //   alert(trays);
            $("#" + orderLineGuid + "_Trays").text(trays);
            //row.children(".td_3").text(numeral(trays).format('0,0'));

            //update extension
            var extPrice = numeral(line.Price * line.QuantityOrdered).format('0,0.00');
           // alert($("#" + orderLineGuid + "_EXT").text());
           // alert(extPrice);
            $("#" + orderLineGuid + "_EXT").text(extPrice);
            //alert($("#" + orderLineGuid + "_EXT").text());

            //row.children(".td_6").children(".extension_price_span").text(numeral(line.Price * line.QuantityOrdered).format('0,0.00'));

            //update Avail
            var currentAvail = $("#" + orderLineGuid + "_Avail").text();
         //   alert(currentAvail + " " + orderLineGuid);
            if (isNaN(currentAvail)) {
                $("#" + orderLineGuid + "_Avail").text(currentAvail);
            }
            else {
                $("#" + orderLineGuid + "_Avail").text(currentAvail - (line.QuantityOrdered - currentQuantityOrdered));
            }
            
            //row.children(".td_6").children(".extension_price_span").text(numeral(line.Price * line.QuantityOrdered).format('0,0.00'));

            vModel.open.showQuantityCheckIcon(orderLineGuid);
            vModel.open.setRowStatusAndEvents(orderLineGuid);

            vModel.refreshTotalsCascade("open",null, [function () {
                //vModel.open.state.orderQuantityBeingHandled = false;
            }]);
        }
    });

};
vModel.open.showOrderIcon = function (orderLineGuid, src) {
    var imgElement = $("#" + orderLineGuid + "_IMG");
    if (typeof imgElement !== 'undefined' && imgElement != null && imgElement.length > 0) {
        imgElement.prop("src", src);
    };
};
vModel.open.clearOrderIcon = function (orderLineGuid) {
     vModel.open.showOrderIcon(orderLineGuid, vModel.config.icons.DotClearSrc);
};
vModel.open.showQuantityUpdatingIcon = function (orderLineGuid) {
    vModel.open.showOrderIcon(orderLineGuid, vModel.config.icons.AjaxWorkingSrc);
};
vModel.open.showQuantityCheckIcon = function (orderLineGuid) {
    vModel.open.showOrderIcon(orderLineGuid, vModel.config.icons.ItemCheckedSrc);
};
vModel.continueShoppingLink = function () {

    var link = "/Availability2/search"             //VA&Form=URC&Suppliers=PAC,HMA&species=ABU,ALT,ATR&ShipWeek=38|2014";
  
    //add category value to link
    link += "?category=" + (model.open.DetailData.NavigationProgramTypeCode || "??");
   // link += "?category="

    //add form value to link
    link += "&form=" + (model.open.DetailData.NavigationProductFormCategoryCode || "??");

    //compose suppliers and species list
    var suppliers = "";
    var species = "";
    $.each(model.open.DetailData.SupplierOrders, function (ndx, order) {
        if (ndx > 0) { suppliers += "," };
       // alert(traverseObj(order, false));
       // alert(order.SupplierCode);
        suppliers += order.SupplierCode || "";
        $.each(order.OrderLines, function (i, orderLine) {
            var code = orderLine.SpeciesCode || "";
            var str = "," + species + ",";
            if (str.indexOf("," + code + ",") == -1) {
                if (species.length > 0) { species += "," };
                species += code
            };
        });
    });
    //add suppliers to link
    link += "&suppliers=" + suppliers;
    //add species list to link
    link += "&species=" + species;

    //add shipweek value to link
    var shipWeekCode = model.open.DetailData.ShipWeekCode || "";
    if (shipWeekCode.length == 6) {
        var shipWeekString = shipWeekCode.substr(4);
        shipWeekString += "|" + shipWeekCode.substr(0, 4);
        link += "&ShipWeek=" + shipWeekString;
    };
    link += "&go=true";
    return link;
};

vModel.GetPDF = function (orderGuid) {
    alert(model.open.config.urlOrderPDF + orderGuid);
    $.ajax({
        url: model.open.config.urlOrderPDF + orderGuid,
        //async: true,
        type: "get",
        data: {
            //"SessionGuid": sessionGuid,
            //"ShipWeekString": viewModel.state.shipWeekSelected.ShipWeekString,
            //"RowData": rowData
        },
        datatype: "json",
        beforeSend: function (jqXHR, settings) {

        },
        success: function (data, textStatus, jqXHR) {
            //console.log(data);
            //targetResponse.html("SERVER RESPONSE: <br />" + JSON.stringify(data));

            //if (data.Success) {
            alert("success");
            alert(traverseObj(data));
            alert(data.urlPDF);
            //var blob = new Blob([data]);
            var link = document.createElement('a');
            link.href = data.urlPDF;
            link.download = "Confirmation";
            link.click();
            //<a href="https://epsstorage.blob.core.windows.net/invoicepdf/I-000909.pdf" download="MyFile">Download it!</a>
            //var file = new Blob([data], { type: 'application/pdf' });
            //var fileURL = URL.createObjectURL(file);
            //window.open(fileURL);
            //};

            //alert(JSON.stringify(data));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + decodeURI(this.url) + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            //reset icon
            //viewModel.resetOrderIcon(rowData.ProductGuid);
            //viewModel.ProcessEventChain(callbacks);
        }
    });

};
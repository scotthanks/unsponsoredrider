﻿

var model = {};

model.open = {};
model.shipped = {};
model.cancelled = {};



//model.DetailHistoryData = {};

model.config = {
    urlOrderDetailHistoryApi: '/api/order/detailhistory',

    urlGetDetailHistoryData: function (guid) { return model.config.urlOrderDetailHistoryApi + "?OrderGuid=" + guid },

    urlCartUpdateApi: '/api/cartupdate',
    //urlOrderApi: '/api/order',


};
model.state = {
    orderDescriptionUpdating: false,
    orderPurchaseOrderUpdating: false,
    isReadOnly: false,
};

//Generic Ajax Call
model.ajaxCall = function (element, uri, httpVerb, onSuccess, callbacks) {
    $.ajax({
        url: uri,
        dataType: "json",
        type: httpVerb,
    })
    .done(function (data, textStatus, jqXHR) {
        //alert(traverseObj(data, false));
        onSuccess(data);
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        //alert("failure");
        var errorMsg = "TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown;
        //testDisplay.append(errorMsg);
        //if (typeof element !== "undefined") {
        eps_tools.floatMessage(element, errorMsg, 5000);
        //};
    })
    .always(function () {
        vModel.ProcessEventChain(callbacks);
    });

};

model.writeOrderData = function (data, element, callbacks) {
    //ported over from cart, this uses cartupdate api
    //element is solely for positioning floating message
   // alert("hi");
    var resultString = "Write Order Data:\n";

    $.ajax({
        type: "put",
        url: model.config.urlCartUpdateApi,
        data: data,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
            //resultString += "url: " + this.url + "\n";
            //resultString += "data: " + JSON.stringify(this.data) + "\n";
        },
        success: function (response, textStatus, jqXHR) {
            if (typeof response !== 'undefined' && response != null) {
                //alert(JSON.stringify(response));
                resultString += "RESPONSE: " + JSON.stringify(response) + "\n";
                //ToDo: this interface stuff should be handled by a callback
                if (typeof element !== 'undefined' && element != null && element.length > 0) {
                    if (!response.IsAuthenticated) {
                        eps_tools.floatMessage(element, "You must be logged-in to use this page.", 2000);
                        //eps_tools.OkDialog({ 'title': '<span>Error:</span>', 'content': '<br /><span>You must be logged-in to use this page.<br />&nbsp;</span>', 'modal': true });
                    } else if (response.Success) {
                        eps_tools.floatMessage(element, "Data updated.", 2000);
                        //eps_tools.OkDialog({ 'title': '<span>Cart:</span>', 'content': '<br /><span>Data updated.<br />&nbsp;</span>', 'modal': true });
                    } else {
                        eps_tools.floatMessage(element, "Update Data: failed.", 2000);
                        //eps_tools.OkDialog({ 'title': '<span>Error:</span>', 'content': '<br /><span>Update Data: failed.<br />&nbsp;</span>', 'modal': true });
                    }
                };

            };
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            //alert(resultString);
            //alert(callbacks);
            vModel.ProcessEventChain(callbacks);
        }
    });
};


model.getData = function (element, url, onSuccess, onFailure, callbacks) {
    //var testDisplay = $("#test_area_results_display").empty();
    //var p = $("<p />")
    //    .text("[GET] url : " + url);
    //testDisplay.append(p, $("<hr />"));
    $.ajax({
        type: "get",
        url: url,
        dataType: "json",
    })
    .done(function (data, textStatus, jqXHR) {
        //testDisplay.append(traverseObj(data));
        onSuccess(data);
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        //var errorMsg = "TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown;
        //testDisplay.append(errorMsg);
        //if (typeof element !== "undefined" & element != null) {
        //    vModel.floatMessage(element, errorMsg, 4000);
        //};
        onFailure(jqXHR, textStatus, errorThrown);

    })
    .always(function () {
        vModel.ProcessEventChain(callbacks);
    });

};


model.getGrowerOrderFees = function (section,orderGuid,callbacks) {
    var onSuccess = function (data) {
        if (data.Success) {
            switch (section) {
                case "open":
                    model.open.DetailData.GrowerOrderFees = data.GrowerOrderFees || [];
                    break;
                case "shipped":
                    model.shipped.DetailData.GrowerOrderFees = data.GrowerOrderFees || [];
                    break;
                case "cancelled":
                    model.cancelled.DetailData.GrowerOrderFees = data.GrowerOrderFees || [];
                    break;
                default:
                    break;
            }
           
        } else {
            eps_tools.FloatingMessage(null, null, "<p>Error: Server error while retrieving Grower Order fees.</p>", 4000);
        };
    };

    var onFailure = function (jqXHR, textStatus, errorThrown) {
        eps_tools.FloatingMessage(null, null, "<p>Error: " + errorThrown + "</p>", 3500);
    };

    var callAfter = [function () {
        vModel.ProcessEventChain(callbacks)
    }];
    var url = "/api/Order/GrowerOrderFees?growerOrderGuid=" + orderGuid;
    model.getData($(this), url, onSuccess, onFailure, callAfter);

};
model.returnGrowerOrderFees = function (section) {
    switch (section) {
        case "open":
            return model.open.DetailData.GrowerOrderFees || [];
            break;
        case "shipped":
            return model.shipped.DetailData.GrowerOrderFees || [];
            break;
        case "cancelled":
            return model.cancelled.DetailData.GrowerOrderFees || [];
            break;
        default:
            break;
    }
 
};




model.getSupplierOrderFees = function (section,supplierOrderGuid, callbacks) {
   
    var onSuccess = function (data) {
        if (data.Success) {
            var fees = data.SupplierOrderFees || [];
            if (fees == null || fees == "") { fees = [] };
            switch (section) {
                case "open":
                    model.open.returnSupplierOrder(supplierOrderGuid).SupplierOrderFees = fees;
                    break;
                case "shipped":
                    model.shipped.returnSupplierOrder(supplierOrderGuid).SupplierOrderFees = fees;
                    break;
                case "cancelled":
                    model.cancelled.returnSupplierOrder(supplierOrderGuid).SupplierOrderFees = fees;
                    break;
                default:
                    break;
            }
            
        } else {
            //Error on Server
            eps_tools.FloatingMessage(null, null, "<p>Error: Server error while retrieving Supplier Order fees.</p>", 4000);
        };
    };
    var onFailure = function (jqXHR, textStatus, errorThrown) {
        eps_tools.FloatingMessage(null, null, "<p>Error: " + errorThrown + "</p>", 3500);
    };
    var callAfter = [function () {
        vModel.ProcessEventChain(callbacks)
    }];
    var url = "/api/Order/SupplierOrderFees?supplierOrderGuid=" + supplierOrderGuid;
    model.getData($(this), url, onSuccess, onFailure, callAfter);
};
model.returnSupplierOrderFees = function (section, supplierOrderGuid) {
    switch (section) {
        
        case "open":
            return model.open.returnSupplierOrder(supplierOrderGuid).SupplierOrderFees || [];
            break;
        case "shipped":
            return model.shipped.returnSupplierOrder(supplierOrderGuid).SupplierOrderFees || [];
            break;
        case "cancelled":
            return model.cancelled.returnSupplierOrder(supplierOrderGuid).SupplierOrderFees || [];
            break;
        default:
    }
  
    
};

var vModel = {};
//var vModel.open = {};
vModel.open = {};
vModel.shipped = {};
vModel.cancelled = {};



    


vModel.config = {
    PageMode: "all",
    headerBarsClass: "header_bars",
    headerBarsClickableClass: "accordion_header_bars",
    accordionDisplaysClass: "accordion_displays",

    ordersSummaryTable$: null,

    OrderTrayTotal: 0,
    OrderCuttingTotal: 0,
    OrderCostTotal: 0.0,
    OrderQuantityImgClass: "line_item_grid_order_quantity_imgs",

    icons: {
        order_workingSrc: "../../Images/icons/Loading_Availability.gif",
        order_working: function () { return $("<img src=\"\" alt=\"" + vModel.config.icons.order_workingSrc + "\" title=\"\"/>"); },

        ItemCheckedSrc: "/images/layout/green_check.png",
        ItemLockedSrc: "/images/icons/locks/icon_locked_16.png",
        AjaxWorkingSrc: "/images/layout/timer.png",
        DotClearSrc: "/images/icons/mag_glass_plus.png"

    },

    orderHistoryTable: {
        templateId: "order_history_table_template",
        id: "order_history_table",      //assigned at runtime
        template: null,
        rowTemplateId: "order_history_row_template",
        rowClass: "order_history_table_rows",
        rowTemplate: null,
        timeStampClass: "order_history_date_spans",
        personNameClass: "order_history_person_spans",
        commentClass: "order_history_description_spans",

    },

};

vModel.init = function () {
    //Clone and remove order summary template
    var ordersSummaryTemplate = $("#orders_summary_grid_template");
    vModel.config.ordersSummaryTable$ = ordersSummaryTemplate.clone(true);
    ordersSummaryTemplate.remove();

    //Clone and delete order history table template and order history row template
    var orderHistoryRowTemplate = $("#" + vModel.config.orderHistoryTable.rowTemplateId);
    orderHistoryRowTemplate.removeAttr("id");
    vModel.config.orderHistoryTable.rowTemplate = orderHistoryRowTemplate.clone(true);

    //alert(orderHistoryRowTemplate.length);
    orderHistoryRowTemplate.remove();
    //alert(vModel.config.orderHistoryTable.rowTemplate.length);

    var orderHistoryTableTemplate = $("#" + vModel.config.orderHistoryTable.templateId);
    orderHistoryTableTemplate.removeAttr("id");
    vModel.config.orderHistoryTable.template = orderHistoryTableTemplate.clone(true);

    //alert(orderHistoryTableTemplate.length);
    orderHistoryTableTemplate.remove();
    //alert(vModel.config.orderHistoryTable.template.length);




    vModel.open.init();
    vModel.shipped.init();
    vModel.cancelled.init();
  
   

};

vModel.setHeaderBarIcon = function (accordionId, iconSrc, callbacks) {
    var accordion$ = $("#" + accordionId);
    var headerBar$ = accordion$.children(".accordion_header_bars");
    headerBar$.css({ "background-image": "url('" + iconSrc + "')" });

    vModel.ProcessEventChain(callbacks);
};

vModel.disableAccordion = function (accordionId, message) {
    //alert("disabling: " + accordionId);

    var accordion = $("#" + accordionId);
    var header = accordion.children("." + vModel.config.headerBarsClickableClass);
    if ((header.length || 0) == 0) { return false; };

    var display = accordion.children("." + vModel.config.accordionDisplaysClass);
    display.hide();
    header.removeClass(vModel.config.headerBarsClickableClass).addClass(vModel.config.headerBarsClass);
    //$("#" + vModel.cancelled.config.cancelledOrdersHeaderBarId).off();

    var p = $("<p />")
    .prop({})
    .text(message);
    accordion.append(p);

};

vModel.composeHistoryData = function (display, orderGuid, historyData) {
    //toggle between history to display and none
    if (historyData.length > 0) {

        var table = vModel.config.orderHistoryTable.template.clone(true);
        table.prop({ "id": orderGuid });

        $.each(historyData, function (ndx, item) {
            var row = vModel.config.orderHistoryTable.rowTemplate.clone(true);
            row.prop({ "id": item.Guid });
            //ToDo: insert data into row
            var date = new Date(item.EventTime);
            //var displayDate = date.toLocaleDateString(date) + " " + date.toLocaleTimeString(date);
            var displayDate = date.toLocaleString(date);
            row.children("td.td_0").children("span." + vModel.config.orderHistoryTable.timeStampClass).text(displayDate);
            row.children("td.td_1").children("span." + vModel.config.orderHistoryTable.personNameClass).text(item.PersonName);
            row.children("td.td_2").children("span." + vModel.config.orderHistoryTable.commentClass).text(item.Comment);
            table.append(row);
        });
        display.append(table);
    } else {
        var div = $("<div />");
        div.html("<p>No items found.</p>");
        display.append(div);
    };
};
vModel.composeSummaryRow = function (item) {
    var row = $("<tr />")
    .prop({
        "id": item.OrderGuid,
        "class": "order_summary_grid_rows"
    });

    for (var i = 0; i < 8; i++) {
        var td = $("<td />")
        .prop({ "class": "td_" + i });
        switch (i) {
            case 1:
                td.text(item.OrderNo);
                break;
            case 2:
                td.text(item.ProgramTypeName + "; " + item.ProductFormCategoryName);
                break;
            case 3:
                var shipWeek = item.ShipWeekString || "";
                if (shipWeek.length != 7) shipWeek = "00|0000";
                td.text(shipWeek.substring(0, 2) + "/" + shipWeek.substring(3, 7));
                break;
            case 4:
                td.text(item.CustomerPoNo || '');
                break;
            case 5:
                td.text(item.OrderDescription || '');
                break;
            case 6:
                td.text(numeral(item.OrderQty).format('0,0'));
                break;
            case 7:
                td.text(item.GrowerOrderStatusName || null);
                break;
            default:
                td.html("<span>&nbsp;&nbsp;&nbsp;</span>");   //ie 9 stutters on click
        }
        row.append(td);
    };

    return row;

};

vModel.refreshSupplierOrderFeesAndTotals = function (section, supplierOrderGuid, callbacks) {
    switch (section) {
        case "open":
            var supplierOrder = model.open.returnSupplierOrder(supplierOrderGuid);
            break;
        case "shipped":
            var supplierOrder = model.shipped.returnSupplierOrder(supplierOrderGuid);
            break;
        case "cancelled":
            var supplierOrder = model.cancelled.returnSupplierOrder(supplierOrderGuid);
            break;
        default:
            break;
    }
  
    var supplierOrderTotalTrays = 0;
    var supplierOrderTotalCuttings = 0;
    var supplierOrderTotalPrice = 0.00;

    $.each(supplierOrder.OrderLines, function (i, orderLine) {
        supplierOrderTotalTrays += (orderLine.QuantityOrdered / orderLine.Multiple) || 0;
        supplierOrderTotalCuttings += orderLine.QuantityOrdered;
        supplierOrderTotalPrice += (orderLine.QuantityOrdered * orderLine.Price);
    });
    vModel.config.OrderTrayTotal += supplierOrderTotalTrays;
    vModel.config.OrderCuttingTotal += supplierOrderTotalCuttings;
    //total Price done later

    $("#" + supplierOrder.SupplierOrderGuid + "_supplier_name_span").text(supplierOrder.SupplierName + " Subtotal");
    $("#" + supplierOrder.SupplierOrderGuid + "_supplier_order_tray_totals").text(numeral(supplierOrderTotalTrays).format('0,0'));
    $("#" + supplierOrder.SupplierOrderGuid + "_supplier_order_quantity_totals").text(numeral(supplierOrderTotalCuttings).format('0,0'));
    $("#" + supplierOrder.SupplierOrderGuid + "_supplier_order_product_cost_totals").text(numeral(supplierOrderTotalPrice).format('$ 0,0.00'));



    //get total costs row
    var totalCostsRow = $("#" + supplierOrder.SupplierOrderGuid + "_open_orders_dynamic_cost_rows_template");

    //initialize total costs with subtotal
    var totalCosts = supplierOrderTotalPrice;

    var feeList = []
    var supplierFeeDiv = $("<div />");
    var supplierFeeDivProperties = { "id": supplierOrder.SupplierOrderGuid + "_supplier_fees" };
    supplierFeeDiv.prop(supplierFeeDivProperties);
    switch (section) {
        case "open":
            feeList = model.returnSupplierOrderFees("open", supplierOrderGuid) || [];
            break;
        case "shipped":
            feeList = model.returnSupplierOrderFees("shipped", supplierOrderGuid) || [];
            break;
        case "camcelled":
            feeList = model.returnSupplierOrderFees("camcelled", supplierOrderGuid) || [];
            break;
        default:
            break;
    }
  
    
    $.each(feeList, function (ndx, fee) {
        var ul = $("<ul>");
        var ulProperties = {
            "id": "supplier_fee",
            "class": "supplier_subtotal_boxes"
        };
        ul.prop(ulProperties);


        totalCosts += fee.Amount;
        var dynamicCostsRow = totalCostsRow.clone(true);
        dynamicCostsRow
            .prop({ "id": fee.Guid });

        var infoIcon
        switch (section) {
            case "open":
                infoIcon = dynamicCostsRow.find("span." + vModel.open.config.SupplierOrders.LineItemGrid.supplierOrderFeeInfoSpans);
                break;
            case "shippped":
                infoIcon = dynamicCostsRow.find("span." + vModel.shipped.config.SupplierOrders.LineItemGrid.supplierOrderFeeInfoSpans);
                break;
            case "cancelled":
                infoIcon = dynamicCostsRow.find("span." + vModel.cancelled.config.SupplierOrders.LineItemGrid.supplierOrderFeeInfoSpans);
                break;
            default:
                break;
        }
       
        var description = fee.FeeDescription || "";
        description = description.trim();
        if (description == "") {
            $(infoIcon).remove();
        } else {
            $(infoIcon)
            .off()
            .on({
                mouseenter: function () {
                    eps_tools.HoverMessage($(this)).load(description).display(4);    //0 means display left of element
                },
                mouseleave: function () {
                    eps_tools.HoverMessage($(this)).remove();
                }
            });
        };
        var statusToDisplay = fee.SupplierOrderFeeStatusDescription || "";
        if (statusToDisplay.trim().toLowerCase() === "actual" || statusToDisplay.trim().toLowerCase() === "tbd" || statusToDisplay.trim().toLowerCase() === "incl.") { statusToDisplay = ""; };
        switch (section) {
            case "open":
                dynamicCostsRow.find("span." + vModel.open.config.SupplierOrders.LineItemGrid.supplierOrderFeeStatusDescription).text(statusToDisplay);
                dynamicCostsRow.find("span." + vModel.open.config.SupplierOrders.LineItemGrid.supplierOrderFeeTypeDescription).text(fee.SupplierOrderFeeTypeDescription);
                break;
            case "shipped":
                dynamicCostsRow.find("span." + vModel.shipped.config.SupplierOrders.LineItemGrid.supplierOrderFeeStatusDescription).text(statusToDisplay);
                dynamicCostsRow.find("span." + vModel.shipped.config.SupplierOrders.LineItemGrid.supplierOrderFeeTypeDescription).text(fee.SupplierOrderFeeTypeDescription);
                break;
            case "cacncelled":
                dynamicCostsRow.find("span." + vModel.cacncelled.config.SupplierOrders.LineItemGrid.supplierOrderFeeStatusDescription).text(statusToDisplay);
                dynamicCostsRow.find("span." + vModel.cacncelled.config.SupplierOrders.LineItemGrid.supplierOrderFeeTypeDescription).text(fee.SupplierOrderFeeTypeDescription);
                break;
            default:
                break;
        }
       
        var feeToDisplay = numeral(fee.Amount).format('$ 0,0.00');
        if (fee.SupplierOrderFeeStatusCode.toLowerCase() === "included" || fee.SupplierOrderFeeStatusCode.toLowerCase() === "tbd" || fee.SupplierOrderFeeStatusCode.toLowerCase() === "na") {
            feeToDisplay = fee.SupplierOrderFeeStatusCode;
        };
        //dynamicCostsRow.find("span." + vModel.open.config.SupplierOrders.LineItemGrid.lineItemFooterCostsClass).text(feeToDisplay);

        //dynamicCostsRow.insertBefore(totalCostsRow)

        var li = $("<li />");
        var liProperties = { "class": "left" };
        var span = $('<span />');
        span.prop({
            "class": "supplier_order_fee_info_icons"
        });
        var img = $('<img />');
        img.prop({
            "id": fee.Guid + "_IMG",
            "src": "/Images/icons/icon_information_circle_green_24.png",
            "class": vModel.config.OrderQuantityImgClass,
            "alt": "",
            "style": "display:inline-block; vertical-align:text-bottom; width:20px; height:20px; margin: 0; padding:0"
        });
        span.append(img);
        li.append(span);
        ul.append(li);

        var li = $("<li />");
        var liProperties = { "class": "left" };
        var span = $('<span />');
        span.prop({
            "class": "supplier_order_fee_status_description"
        });
        span.text(statusToDisplay);
        //alert("statusToDisplay = " + statusToDisplay);
        li.append(span);
        ul.append(li);

        var li = $("<li />");
        var liProperties = { "class": "left" };
        var span = $('<span />');
        span.prop({
            "class": "supplier_order_fee_type_description"
        });
        span.text(fee.SupplierOrderFeeTypeDescription);
        //alert("statusToDisplay = " + statusToDisplay);
        li.append(span);
        ul.append(li);

        var li = $("<li />");
        var liProperties = { "class": "right" };
        var span = $('<span />');
        span.prop({
            "class": "supplier_order_fee_amount"
        });
        span.text(numeral(fee.Amount || 0).format('$ 0,0.00'));
        //alert("statusToDisplay = " + statusToDisplay);
        li.append(span);
        ul.append(li);

        supplierFeeDiv.append(ul);
    });


    $("#" + supplierOrderGuid + "_supplier_fees").replaceWith(supplierFeeDiv);

    var supplierTotalDiv = $("<div />");
    var supplierTotalDivProperties = { "id": supplierOrder.SupplierOrderGuid + "_supplier_totals" };
    supplierTotalDiv.prop(supplierTotalDivProperties);

    var ul = $("<ul>");
    var ulProperties = {
        "id": "supplier_total",
        "class": "supplier_subtotal_boxes"
    };
    ul.prop(ulProperties);

    var li = $("<li />");
    var liProperties = { "class": "left" };
    var span = $('<span />');
    span.prop({
        "class": "supplier_name_span2"
    });
    
    span.text(supplierOrder.SupplierName + " Total");
    //alert("statusToDisplay = " + statusToDisplay);
    li.append(span);
    ul.append(li);

    var li = $("<li />");
    var liProperties = { "class": "right" };
    var span = $('<span />');
    span.prop({
        "class": "supplier_order_total_costs"
    });
    // alert(totalCosts);
    vModel.config.OrderCostTotal += totalCosts;
    span.text(numeral(totalCosts || 0).format('$ 0,0.00'));
    //alert("statusToDisplay = " + statusToDisplay);
    li.append(span);
    ul.append(li);

    supplierTotalDiv.append(ul);

    $("#" + supplierOrderGuid + "_supplier_totals").replaceWith(supplierTotalDiv);


    vModel.ProcessEventChain(callbacks);
}


vModel.refreshGrowerOrderFeesAndTotals = function (section, orderGuid,callbacks) {


    var growerFeeDiv = $("<div />");
    var growerFeeDivProperties = { "id": orderGuid + "_grower_fees" };
    growerFeeDiv.prop(growerFeeDivProperties);



    var feeList = model.returnGrowerOrderFees(section) || [];
    $.each(feeList, function (ndx, fee) {

        var ul = $("<ul>");
        var ulProperties = {
            "id": "grower_fee",
            "class": "grower_subtotal_boxes"
        };
        ul.prop(ulProperties);


        var description = fee.FeeDescription || "";
        description = description.trim();

        var feeStatusDescription = fee.GrowerOrderFeeStatusDescription || "";


        var li = $("<li />");
        var liProperties = { "class": "right" };
        var span = $('<span />');
        span.prop({
            "class": "grower_order_fee_info_icons"
        });
        var img = $('<img />');
        img.prop({
            "id": fee.Guid + "_IMG",
            "src": "/Images/icons/icon_information_circle_green_24.png",
            "class": vModel.config.OrderQuantityImgClass,
            "alt": "",
            "style": "display:inline-block; vertical-align:text-bottom; width:20px; height:20px; margin: 0; padding:0"
        });
        span.append(img);
        li.append(span);
        ul.append(li);

        var li = $("<li />");
        var liProperties = { "class": "right" };
        var span = $('<span />');
        span.prop({
            "class": "grower_order_fee_status_description"
        });
        span.text(feeStatusDescription);
        //alert("statusToDisplay = " + statusToDisplay);
        li.append(span);
        ul.append(li);

        var li = $("<li />");
        var liProperties = { "class": "right" };
        var span = $('<span />');
        span.prop({
            "class": "grower_order_fee_type_description"
        });
        span.text(fee.GrowerOrderFeeTypeDescription || "");
        //alert("statusToDisplay = " + statusToDisplay);
        li.append(span);
        ul.append(li);

        var li = $("<li />");
        var liProperties = { "class": "right" };
        var span = $('<span />');
        span.prop({
            "class": "grower_order_fee_amount"
        });
        span.text(numeral(fee.Amount || 0).format('$ 0,0.00'));
        vModel.config.OrderCostTotal += fee.Amount;

        //alert("statusToDisplay = " + statusToDisplay);
        li.append(span);
        ul.append(li);

        growerFeeDiv.append(ul);

        //   alert("end of fee");


    });

    $("#grower_fees").replaceWith(growerFeeDiv);

    //  growerOrderFeeRowTemplate.remove();
    // supplierOrderTotalPrice
    //Now do the total row
    $("#grower_order_total_trays_span").text(numeral(vModel.config.OrderTrayTotal).format('0,0'));
    $("#grower_order_total_cuttings_span").text(numeral(vModel.config.OrderCuttingTotal).format('0,0'));
    $("#grower_order_total_cuttings_cost_span").text(numeral(vModel.config.OrderCostTotal).format('$ 0,0.00'));

    vModel.ProcessEventChain(callbacks);
};

vModel.addSupplierOrderLinesToDiv = function (orderDiv, order) {
    //Add Order Lines
    $.each(order.OrderLines, function (ndx, line) {
        var alink = $('<a>');
        var alinkProps = { "class": "order_delete_links", "text": "DELETE" };

        var row = $("<ul>");
        var rowProperties = { "id": line.Guid, "class": "line_item_grid_rows" };
        row.data({ "SupplierOrderGuid": order.SupplierOrderGuid });  //used by toggle remove row function
        row.prop(rowProperties);


        for (var i = 0; i <= 8; i++) {
            var td = $("<li />");
            //var columnProperties = { "class": "td_" + i };
            //  var columnProperties = { "class": "order_info_content" };

            //  alert(traverseObj(line));
            //   td.prop(columnProperties);
            switch (i) {
                case 0:
                    var span = $("<span />");
                    span.prop({
                        "class": "order_label",
                    });
                    span.text("Species:");
                    td.append(span);
                    var span = $("<span />");
                    span.prop({
                        "class": "order_content",
                    });
                    span.text(line.SpeciesName);
                    td.append(span);
                    break;
                case 1:
                    var span = $("<span />");
                    span.prop({
                        "class": "order_label",
                    });
                    span.text("Product:");
                    td.append(span);
                    var span = $("<span />");
                    span.prop({
                        "class": "order_content",
                    });
                    span.text(line.ProductDescription);
                    td.append(span);
                    break;
                case 2:
                    var span = $("<span />");
                    span.prop({
                        "class": "order_label",
                    });
                    span.text("Form:");
                    // td.append(span);
                    var span2 = $("<span />");
                    span2.prop({
                        "class": "order_content",
                    });
                    span2.text(line.ProductFormName);
                    // alert(line.ProductFormName);
                    td.append(span, span2);
                    break;
                case 3:
                    var span = $("<span />");
                    span.prop({
                        "class": "order_label",
                    });
                    span.text("Trays:");
                    td.append(span);
                    var span = $("<span />");
                    span.prop({
                        "id": line.Guid + "_Trays",
                        "class": "order_content",
                    });
                    span.text((line.QuantityOrdered / line.Multiple) || 0);
                    td.append(span);
                    break;
                case 4:
                    var span = $("<span />");
                    span.prop({
                        "class": "order_label",
                    });
                    span.text("Unit Price:");
                    td.append(span);

                    var thePrice = "";
                    if (numeral(line.Price) > 100) {
                        thePrice = numeral(line.Price).format('0,0.00');
                    }
                    else {
                        thePrice = numeral(line.Price).format('0,0.0000');
                    }

                    var span = $("<span />");
                    span.prop({
                        "class": "order_content",
                    });
                    span.text(thePrice);
                    td.append(span);
                    break;
                case 5:
                    var span = $("<span />");
                    span.prop({
                        "class": "order_label",
                    });
                    span.text("Qty:");
                    td.append(span);


                    var iconSrc = vModel.config.icons.ItemCheckedSrc;
                    if (model.state.isReadOnly == false) {
                        if (line.IsLockedForAllChanges) {
                            var orderInput = $("<span />")
                                .prop({
                                    "class": "line_item_grid_order_quantity_locked_inputs",
                                    "title": "Quantity is locked for changes.",
                                    "style": "width:75px",
                                })
                                .text(numeral(line.QuantityOrdered).format('0,0')
                                );
                            iconSrc = vModel.config.icons.ItemLockedSrc;
                            td.append(orderInput);
                        } else {

                            var orderInput = $("<input type=\"text\" />")
                                .prop({
                                    "value": numeral(line.QuantityOrdered).format('0,0'),
                                    "class": "line_item_grid_order_quantity_inputs",
                                    "style": "width:75px",
                                    "id": line.Guid + "_QtyInput"
                                });
                            td.append(orderInput);
                        };
                    }
                    else {//read only
                        var orderInput = $("<span />")
                             .prop({
                                 "class": "line_item_grid_order_quantity_locked_inputs",
                                 "title": "Read Only.",
                                 "style": "width:75px",
                             })
                             .text(numeral(line.QuantityOrdered).format('0,0')
                             );
                        iconSrc = vModel.config.icons.ItemLockedSrc;
                        td.append(orderInput);
                    }
                    var img = $('<img />');
                    img.prop({
                        "id": line.Guid + "_IMG",
                        "src": iconSrc,
                        "class": "line_item_grid_order_quantity_imgs",
                        "alt": ""
                    });

                    var span = $('<span />');
                   // span.prop({
                       // "class": vModel.config.SupplierOrders.LineItemGrid.OrderQuantitySpanClass,
                   // });
                    span.append(img);

                    td.append(span);
                    break;
                case 6:
                    var span = $("<span />");
                    span.prop({
                        "class": "order_label",
                    });
                    span.text("Avail:");
                    td.append(span);




                    var columnProperties = {
                        //"class": "order_info_content",
                        "id": "avail"
                    };
                    td.prop(columnProperties);
                    var availText = "";
                    switch (line.AvailabilityTypeCode) {
                        case "OPEN":
                            availText = "OPEN";
                            break;
                        case "NA":
                            availText = "NA";
                            break;

                        case "AVAIL":
                            availText = line.AvailableQty;
                            break;
                        default:
                            availText = ""
                            break;
                    }


                    var span = $('<span />');
                    span.prop({
                        "id": line.Guid + "_Avail",
                        "class": "order_content",
                    });
                    span.text(availText);
                    td.append(span);

                    break;


                case 7:
                    var span = $("<span />");
                    span.prop({
                        "class": "order_label",
                    });
                    span.text("EXT:");
                    td.append(span);


                    var extPrice = "";
                    if (line.Price <= 0) {
                        extPrice = 'TBD';
                    }
                    else {
                        extPrice = numeral(line.Price * line.QuantityOrdered).format('0,0.00');
                    }
                    var span = $('<span />');
                    span.prop({
                        "id": line.Guid + "_EXT",
                        "class": "order_content",
                    });
                    span.text(extPrice);
                    td.append(span);
                    break;
                case 8:
                    var span = $("<span />");
                    span.prop({
                        "class": "order_label",
                       
                    });
                    span.text("Status:");
                    td.append(span);
                    var span = $('<span />');
                    span.prop({
                        "class": "order_content",
                        "id": line.Guid + "_Status",
                    });
                    span.text(line.OrderLineStatusName);
                    td.append(span);
                    break;
                  
                default:
                    td.html("<span>???</span>");
            }
            row.append(td);
        };

        orderDiv.append(row);
    });
}


vModel.refreshTotalsCascade = function (section,supplierOrderGuid, callbacks) {
    
    vModel.config.OrderTrayTotal = 0;
    vModel.config.OrderCuttingTotal = 0;
    vModel.config.OrderCostTotal = 0.00;

    //alert(supplierOrderGuid);
    if (typeof supplierOrderGuid === "undefined") { return false };
    if (typeof supplierOrderGuid === "string" && supplierOrderGuid.length != 36) { return false };
    var supplierOrderGuidsArray = [];
    
    var currentOrderGuid = "";
    switch (section) {
        case "open":
            if (supplierOrderGuid === null) {
                $.each(model.open.DetailData.SupplierOrders, function (ndx, order) {
                    supplierOrderGuidsArray.push(order.SupplierOrderGuid);
                });

            } else {
                supplierOrderGuidsArray = [supplierOrderGuid];
            };

            currentOrderGuid = model.open.state.currentGrowerOrderGuid();
            break;
        case "shipped":
            if (supplierOrderGuid === null) {
                $.each(model.shipped.DetailData.SupplierOrders, function (ndx, order) {
                    supplierOrderGuidsArray.push(order.SupplierOrderGuid);
                });

            } else {
                supplierOrderGuidsArray = [supplierOrderGuid];
            };

            currentOrderGuid = model.shipped.state.currentGrowerOrderGuid();
            break;
        case "cancelled":
            if (supplierOrderGuid === null) {
                $.each(model.cancelled.DetailData.SupplierOrders, function (ndx, order) {
                    supplierOrderGuidsArray.push(order.SupplierOrderGuid);
                });

            } else {
                supplierOrderGuidsArray = [supplierOrderGuid];
            };

            currentOrderGuid = model.cancelled.state.currentGrowerOrderGuid();
            break;
        default:
            break;
    }
   

    $.each(supplierOrderGuidsArray, function (ndx, guid) {
        model.getSupplierOrderFees(section, guid, [function () {
            vModel.refreshSupplierOrderFeesAndTotals(section, guid, [function () {
                if (supplierOrderGuidsArray.length == (ndx + 1)) {
                    model.getGrowerOrderFees(section, currentOrderGuid, [function () {
                        vModel.refreshGrowerOrderFeesAndTotals(section, currentOrderGuid, [function () {
                            vModel.ProcessEventChain(callbacks);
                        }]);
                    }]);
                };
            }]);
        }]);
    });

};
///////////////// Utilities //////////////////////////////
//vModel.floatMessage = function (element, content, interval) {
//    //floats message to right of element
//    //if no element then does nothing
//    if (typeof element === 'undefined') { return false; };
//    var top; var left;
//    if (element == null) {
//        //set values null
//        top = null;
//        left = null;
//        //alert("element == null");
//    }
//    else if (element.length > 0) {
//        var position = element.offset();
//        //ToDo: code to center floating message vertically with element
//        //assume floating message is 35 px tall
//        top = Math.floor(position.top + ((element.height() - 35) / 2));
//        left = Math.floor(position.left + element.width() + 30);
//    };
//    eps_tools.FloatingMessage(left, top, content, interval);
//};
vModel.ProcessEventChain = function (callbacks) {
    if (callbacks != undefined && callbacks.length > 0) {
        var callback = callbacks.shift();
        //alert(callback);
        callback(callbacks);
    }
};
vModel.ShipWeekCodeToShipWeekString = function (code) {
    if (typeof code !== "string" || code == null || code.length != 6) { return ""; };
    return code.substring(4, 6) + "/" + code.substring(0, 4);
    //return code;
};
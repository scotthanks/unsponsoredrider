﻿
var model = {};
model.data = {

};

model.config = {


};
model.LoadProduct = function (productID) {
    $("#sProductID").html(productID);
    viewModel.GetProductData(productID);
};


var viewModel = {};
viewModel.config = {

  
};



viewModel.Init = function () {
    var productID = "hi";
    productID = eps_GetQueryStringValue('ProductID');
    model.LoadProduct(productID);

    $("#button_AddToCart").on("click", function () {
        alert("to do");
    });
};


viewModel.GetProductData = function (productID) {
    var imageLink = "";
    var productName = "";
    var nonMemberPrice = "";
    var memberprice = "";
    switch (productID) {
        case "1":
            imageLink = "Images/catalogue/Men Endurance Max Short Sleeve Jersey (Pro Fit).jpg";
            productName = "Men Endurance Max Short Sleeve Jersey (Pro Fit)";
            nonMemberPrice = "$132.00";
            memberprice = "$102.00";
            break;
        case "2":
            imageLink = "Images/catalogue/Women Endurance Max Short Sleeve Jersey (Pro Fit).jpg";
            productName = "Women Endurance Max Short Sleeve Jersey (Pro Fit)";
            nonMemberPrice = "$132.00";
            memberprice = "$102.00";
            break;
        case "3":
            imageLink = "Images/catalogue/Men Signature Bib Short.jpg";
            productName = "Men Signature Bib Short";
            nonMemberPrice = "$147.30";
            memberprice = "$117.30";
            break;
        case "4":
            imageLink = "Images/catalogue/Women Signature Bib Short.jpg";
            productName = "Women Signature Bib Short";
            nonMemberPrice = "$147.30";
            memberprice ="$117.30";
            break;
        case "5":
            imageLink = "Images/catalogue/Men Endurance Max Short Sleeve Jersey (Pro Fit).jpg";
            productName = "Men Endurance Max Short Sleeve Jersey (Pro Fit)";
            nonMemberPrice = "$126.90";
            memberprice = "$96.90";
            break;
        case "6":
            imageLink = "Images/catalogue/Women Endurance Max Short Sleeve Jersey (Pro Fit).jpg";
            productName = "Women Endurance Max Short Sleeve Jersey (Pro Fit)";
            nonMemberPrice = "$126.90";
            memberprice = "$96.90";
            break;
        case "7":
            imageLink = "Images/catalogue/Men Element Pocket Shell.jpg";
            productName = "Men Element Pocket Shell";
            nonMemberPrice = "$152.40";
            memberprice = "$122.40";
            break;
        default:
            break;
    }
    $("#sProductName").html(productName);
    $("#sNonMemberPrice").html(nonMemberPrice);
    $("#sMemberPrice").html(memberprice);
    $("#imgProduct").attr('src', "../" + imageLink);
   
}

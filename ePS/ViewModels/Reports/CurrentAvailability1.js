﻿
var model = {};
model.data = {
    supplierCodes: [],
    reportData: [],
    shipweekHeaders: [],
};

model.config = {


    urlReport: "/api/Availability/GetCurrentAvailabilityReportData",
    urlShipWeekHeaders: "/api/Availability/GetNextShipWeekCodes",


};



model.loadReport = function (supplierLocationCode) {
//    alert("go");
    var url = model.config.urlReport + "?Report=Avail";
    var element = $("#" + viewModel.config.reportTableId);
    var data = {};

    $.ajax({
        type: "Get",
        data: data,
        url: url,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //console.log('speciesData Request: ', this.url, this.data);

        },
        success: function (response, textStatus, jqXHR) {
           // alert("back form ajax2");
            console.log('loadReport Response');
           
           // alert(traverseObj(response));
            model.data.reportData = response.ReportData;
            viewModel.RefreshReportDisplay(model.data.reportData, model.data.shipweekHeaders);
        },

        complete: function (jqXHR, textStatus) {
            console.log('loadReport complete');
           
        }
    });

};

model.loadShipWeekHeaders = function (supplierLocationCode) {
  //  alert("In loadShipWeekHeaders: " + supplierLocationCode);
    var url = model.config.urlShipWeekHeaders + "?Week=Hi";
    var element = $("#" + viewModel.config.reportTableId);
    var data = {};
  
    $.ajax({
        type: "Get",
        data: data,
        url: url,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //console.log('speciesData Request: ', this.url, this.data);

        },
        success: function (response, textStatus, jqXHR) {
           // alert(traverseObj(response));
            console.log('loadShipWeekHeaders Response');
            model.data.shipweekHeaders = response.ShipWeekCodes;
            model.loadReport(supplierLocationCode);

        },

        complete: function (jqXHR, textStatus) {
            console.log('getSpeciesData complete');
          
        }
    });
};

var viewModel = {};
viewModel.config = {

    reportDivId: "report_table_div",
    reportTableId: "reportTable",

};



viewModel.Init = function () {

    $("#get_report_button").on("click", function () {
     //   alert("click");
        model.loadShipWeekHeaders("GFB");
    });

    $("#export_button").on('click', function () {
        var timestamp = "hi";

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        timestamp = yyyy + '_' + mm + '_' + dd + '_'  ;
        
        var filename = "GreenFuse_Availability_" + timestamp;
        $("#reportTable").tableToCSV(filename);
    });
};



viewModel.RefreshReportDisplay = function (data, shipWeekHeaders) {

   // alert(traverseObj(data));
    var tbody = $("#" + viewModel.config.reportTableId + " tbody");

    tbody.empty();


    var row = $("<tr />");
    var rowProperties = { "class": "reportdata_table_rows" };
    row.prop(rowProperties);

    row.append('<th style="vertical-align:top;align:center">Program</th>"');
    row.append('<th style="vertical-align:top;align:center">Species</th>"');
    row.append('<th style="vertical-align:top;align:center">Variety</th>"');
    row.append('<th style="vertical-align:top;align:center">Supplier ID</th>"');
    $.each(shipWeekHeaders, function (i, item) {
        row.append('<th style="vertical-align:top;align:center;padding: 0px 5px 0px 10px;">' + item.ShipWeekCode + '</th>"');
    });

    tbody.append(row);

    $.each(data, function (i, item) {
        var row = $("<tr />");
        row.prop(rowProperties);

        var cell = $("<td />");
        cell.append(item.Program);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.Species);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.Variety);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.SupplierIdentifier);
        row.append(cell);


        cell = $("<td />");
        cell.append(item.Week01Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week02Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week03Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week04Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week05Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week06Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week07Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week08Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week09Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week10Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week11Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week12Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week13Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week14Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week15Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week16Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week17Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week18Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week19Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week20Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week21Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week22Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week23Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week24Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week25Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week26Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week27Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week28Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week29Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week30Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week31Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week32Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week33Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week34Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week35Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week36Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week37Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week38Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week39Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week40Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week41Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week42Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week43Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week44Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week45Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week46Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week47Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week48Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week49Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week50Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week51Qty);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week52Qty);
        row.append(cell);

        tbody.append(row);

    });


};



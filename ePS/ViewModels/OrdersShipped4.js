﻿model.shipped.SummaryData = [];
model.shipped.DetailData = {};
model.shipped.OrderHistoryData = {};

model.shipped.config = {
    urlOrderApi: '/api/order',
    urlGetSummaryData: "/api/Order/MyShippedOrders",
    urlGetDetailData: function (guid) { return model.shipped.config.urlOrderApi + "/DetailData?OrderGuid3=" + guid },
};

model.shipped.state = {
    currentGrowerOrderGuid: function () { return model.shipped.DetailData.OrderGuid || ""; },
    growerOrdersCount: function () {
        return model.shipped.SummaryData.length || 0;
    },
};

model.shipped.getSummaryData = function (callbacks) {
    $.ajax({
        datatype: "json",
        url: model.shipped.config.urlGetSummaryData + "?TheType3=Shipped",
        type: "get",
        beforeSend: function (jqXHR, settings) {
            model.shipped.SummaryData = [];
        },
        success: function (response, textStatus, jqXHR) {
            model.shipped.SummaryData = response.SummaryData || [];
        },
        complete: function (jqXHR, textStatus) {
            vModel.ProcessEventChain(callbacks);
        }
    });
};
model.shipped.getDetailData = function (guid, callbacks) {
    model.shipped.DetailData = {};
    model.shipped.DetailData.OrderGuid = guid;
    $.ajax({
        datatype: "json",
        url: model.shipped.config.urlGetDetailData(guid),
        type: "get",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
        },
        success: function (response, textStatus, jqXHR) {
            model.shipped.DetailData = response;
            //alert(JSON.stringify(model.shipped.DetailData));
        },
        complete: function (jqXHR, textStatus) {
            //alert(callbacks);
            vModel.ProcessEventChain(callbacks);
        }
    });


};

model.shipped.GetShippedOrderHistory = function (callbacks) {
    //alert("getShippedOrderHistory");
    model.shipped.OrderHistoryData = { "Order History": "TBD" };
    vModel.ProcessEventChain(callbacks);
};

model.shipped.returnSupplierOrder = function (supplierOrderGuid) {
    if (typeof supplierOrderGuid !== 'string' || supplierOrderGuid.length != 36) { return false; }
    var supplierOrder = null;
    $.each(model.shipped.DetailData.SupplierOrders, function (i, order) {
        if (order.SupplierOrderGuid == supplierOrderGuid) { supplierOrder = order; return false };
    });
    //alert(traverseObj(supplierOrder));
    return supplierOrder;
};

model.shipped.ReturnSelectedTagRatio = function (supplierOrderGuid) {
    if (typeof supplierOrderGuid !== 'string' || supplierOrderGuid.length != 36) { return false; }
    var selectedTagRatio = null;
    var supplierOrder = model.shipped.returnSupplierOrder(supplierOrderGuid);
    if (supplierOrder == null) { return null; };

    $.each(supplierOrder.TagRatioList, function (i, tagRatio) {
        //    //alert(tag.Guid);
        if (tagRatio.IsSelected) {
            selectedTagRatio = tagRatio.Name;
            return false;
        } else if (selectedTagRatio == null) {
            if (i == 0 || tagRatio.IsDefault) { selectedTagRatio = tagRatio.Name; };
        };
    });


    return selectedTagRatio;
};

model.shipped.ReturnSelectedShipMethod = function (supplierOrderGuid) {
    if (typeof supplierOrderGuid !== 'string' || supplierOrderGuid.length != 36) { return false; }
    var selectedShipMethod = null;
    var supplierOrder = model.shipped.returnSupplierOrder(supplierOrderGuid);
    if (supplierOrder == null) { return null; };

    $.each(supplierOrder.ShipMethodList, function (i, shipMethod) {
        //alert(shipMethod.Name);
        if (shipMethod.IsSelected) {
            selectedShipMethod = shipMethod.Name;
            return false;
        } else if (selectedShipMethod == null) {
            if (i == 0 || shipMethod.IsDefault) { selectedShipMethod = shipMethod.Name; };
        };

    });

    return selectedShipMethod;
};


vModel.shipped.config = {
    containerId: "shipped_orders_summary_container",

   

    summaryGrid: {
        id: "shipped_order_summary_grid",
        RowClass: "order_summary_grid_rows",
        Columns: 8,     //ToDo: move to state and calculate from template during init
        detailRowId: "order_detail_row",
        detailContainerId: "shipped_order_detail_container",
    },

    detailDisplay: {
        templateId: "shipped_orders_detail_display_template",
        template: null,
        id: "shipped_orders_detail_display",
        headerBarClass: "header_bars",
        shippedOrderDetailHeaderBarId: "shipped_order_detail_header_bar",

        orderStatusId: "shipped_order_status_span",
        orderNumberId: "shipped_order_number_span",
        purchaseOrderSpanId: "shipped_order_purchase_order_span",
        orderDescriptionSpanId: "shipped_order_description_span",

        shipToId: "shipped_order_ship_to_span",

        paymentMethodId: "shipped_order_payment_method_span",

        shippedOrderHistoryContainerId: "shipped_orders_history_container",

        //ShipToTableId: "ship_to_table",
        ShipToIdStem: "ship_to_span_",

        PaymentMethodIdStem: "payment_method_span_",

        totalCuttingsSpanId: "shipped_grower_order_total_cuttings_span",
        totalCuttingsCostSpanId: "shipped_grower_order_total_cuttings_cost_span",
        //OrderDescriptionsClass: "order_descriptions",

    

       

        ///////////////grower order totals /////////////////
        TotalCuttingsSpanId: "shipped_grower_order_total_cuttings_span",
        TotalCuttingsCostSpanId: "shipped_grower_order_total_cuttings_cost_span",
        OrderDescriptionsClass: "order_descriptions",

        orderTotalsGrid: {
            id: "shipped_orders_grower_order_totals_table",

            growerOrderFeesRowTemplateId: "shipped_orders_grower_order_fees_row_template",
            growerOrderFeesRowTemplate$: null,

            growerOrderFeeInfoSpans: "grower_order_fee_info_icons",
            growerOrderFeeStatusDescription: "grower_order_fee_status_description",
            growerOrderFeeTypeDescription: "grower_order_fee_type_description",
            growerOrderFeeSpansClass: "grower_order_fee_spans",

            totalTraysSpanId: "grower_order_total_trays_span",
            totalCuttingsSpanId: "grower_order_total_cuttings_span",
            totalCuttingsCostSpanId: "grower_order_total_cuttings_cost_span",


        },
        //////////////////////////////////


    },

    SupplierOrders: {
        templateId: "shipped_orders_supplier_orders_template",
        template: null,
        ContainerId: "shipped_order_supplier_orders_container",

        displaysClass: "supplier_order_displays",
        SupplierNameSpanClass: "supplier_name_span",

        tagRatioDisplayClass: "supplier_order_tag_ratio_display",
        tagRatioSelectedClass: "tagRatioSelectedSpans",
        shipMethodDisplayClass: "supplier_order_ship_method_display",
        shipMethodSelectedClass: "shipMethodSelectedSpans",

        //vModel.shipped.config.supplierOrders.lineItemGrid.trayTotalsClass

        LineItemGrid: {
            ClassName: "line_item_grids",
            RowClass: "line_item_grid_rows",
            columns: 8,

            //////
            dynamicCostRowsTemplateId: "shipped_orders_dynamic_cost_rows_template",
            dynamicCostRowsTemplate$: null,


            trayTotalsClass: "supplier_order_tray_totals",

            subTotalRowsClass: "product_cost_rows",
            dynamicCostRowsClass: "dynamic_cost_rows",
            totalCostRowsClass: "total_cost_rows",


            supplierOrderFeeInfoSpans: "supplier_order_fee_info_icons",
            supplierOrderFeeStatusDescription: "supplier_order_fee_status_description",
            supplierOrderFeeTypeDescription: "supplier_order_fee_type_description",
            lineItemFooterCostsClass: "line_item_footer_costs",
            supplierOrderTotalCostsClass: "supplier_order_total_costs",

          




        },
    },
};

vModel.shipped.state = {
    displayedOrderGuid: function () { return $("#" + vModel.shipped.config.summaryGrid.detailContainerId).data("orderGuid") || ""; },
    detailDisplay$: function () {
        return $("#" + vModel.shipped.config.detailDisplay.id) || null;
    },
};





vModel.shipped.init = function () {


    //Clone and delete supplier order dynamic cost row from detail display template
    var dynamicCostRowsTemplate = $("#" + vModel.shipped.config.SupplierOrders.LineItemGrid.dynamicCostRowsTemplateId);
    //console.log(dynamicCostRowsTemplate);
    vModel.shipped.config.SupplierOrders.LineItemGrid.dynamicCostRowsTemplate$ = dynamicCostRowsTemplate.clone(true); //store the dynamic cost rows template
    dynamicCostRowsTemplate.remove();
    //console.log(vModel.shipped.config.SupplierOrders.LineItemGrid.dynamicCostRowsTemplate$);

    //Clone and delete grower order dynamic cost row from detail display template
    var growerOrderFeesRowTemplate = $("#" + vModel.shipped.config.detailDisplay.orderTotalsGrid.growerOrderFeesRowTemplateId);
    vModel.shipped.config.detailDisplay.orderTotalsGrid.growerOrderFeesRowTemplate$ = growerOrderFeesRowTemplate.clone(true); //store the dynamic row template
    growerOrderFeesRowTemplate.remove();
    //alert(vModel.shipped.config.detailDisplay.orderTotalsGrid.growerOrderFeesRowTemplate$.length);


    //Clone and Delete Supplier Order Template From Shipped Order Detail Template
    var supplierOrdertemplate = $("#" + vModel.shipped.config.SupplierOrders.templateId)
    //alert(supplierOrdertemplate.length);
    //alert(vModel.shipped.config.SupplierOrders.LineItemGrid.className);
    //alert(supplierOrdertemplate.find("." + vModel.shipped.config.SupplierOrders.LineItemGrid.className + " tbody").children().length);
    //supplierOrdertemplate.find("." + vModel.shipped.config.SupplierOrders.LineItemGrid.className + " tbody").empty();
    //alert(supplierOrdertemplate.find("." + vModel.shipped.config.SupplierOrders.LineItemGrid.className + " tbody").children().length);
    vModel.shipped.config.SupplierOrders.template = supplierOrdertemplate.clone(true);;
    //alert(vModel.shipped.config.SupplierOrders.template.children().length);
    supplierOrdertemplate.remove();

    //Clone and delete detail display template
    var detailDisplayTemplate = $("#" + vModel.shipped.config.detailDisplay.templateId);
    detailDisplayTemplate.prop({ "id": vModel.shipped.config.detailDisplay.id });
    vModel.shipped.config.detailDisplay.template = detailDisplayTemplate.clone(true);
    detailDisplayTemplate.remove();



    var onExpandShippedSummary = function (containerId) {
        eps_expanse.state.setWorking(containerId);
        var display = eps_expanse.state.getDisplayElement(containerId).empty();
        model.shipped.getSummaryData([function () {
            if (model.shipped.state.growerOrdersCount() == 0) {
                //park display
                eps_expanse.setStatus(containerId, "Shipped Orders: 0");
                eps_expanse.state.setCollapsed(containerId);
            } else {
                var table = vModel.config.ordersSummaryTable$.clone(true);
                table.prop({
                    "id": "shipped_orders_summary_grid"
                });
                display.html(table);
                table.children("tbody").replaceWith(vModel.shipped.composeSummaryRows());
                eps_expanse.setStatus(containerId, "Shipped Orders: " + model.shipped.state.growerOrdersCount());
                eps_expanse.expand(containerId, null);
            };
        }]);
    };
    var onCollapseShippedSummary = function (containerId) {
        eps_expanse.state.setWorking(containerId);
        eps_expanse.collapse(containerId, null, true);
    };
    eps_expanse.init(vModel.shipped.config.containerId, onExpandShippedSummary, onCollapseShippedSummary, "Shipped Orders", "Searching...");

    if (vModel.config.PageMode == "all" || vModel.config.PageMode == "shipped") {
        var containerId = vModel.shipped.config.containerId;
        onExpandShippedSummary(containerId);
    } else {

        model.shipped.getSummaryData([function () {
            eps_expanse.setStatus(vModel.shipped.config.containerId, "Shipped Orders: " + model.shipped.SummaryData.length);
        }]);
    };
};

vModel.shipped.composeSummaryRows = function () {

    var tbody = $("<tbody />");
    $.each(model.shipped.SummaryData, function (i, item) {
        tbody.append(vModel.composeSummaryRow(item));
    });
    vModel.shipped.defineSummaryRowEvents(tbody);

    return tbody;


};

vModel.shipped.defineSummaryRowEvents = function (tbody) {
    var rows = tbody.children("tr." + vModel.shipped.config.summaryGrid.RowClass);
    $(rows).on("click", function () {
        var orderGuid = $(this).prop("id");
        vModel.shipped.RemoveDetail([
            function () {
                vModel.shipped.insertDetailDisplay(orderGuid);
            }
        ]);
    });
};


vModel.shipped.RemoveDetail = function (callbacks) {
    //clear detail display
    var currentDetailContainer = $("#" + vModel.shipped.config.summaryGrid.detailContainerId)
    var currentlyDisplayedOrderGuid = currentDetailContainer.data("orderGuid") || "";
    //alert("currentlyDisplayedOrderGuid: " + currentlyDisplayedOrderGuid);
    if (currentlyDisplayedOrderGuid != "") {
        var displayRow = $("#" + vModel.shipped.config.summaryGrid.detailRowId);
        var detailDisplay = $("#" + vModel.shipped.config.detailDisplay.id);

        detailDisplay.hide("blind", {}, 500, function () {
            //restore row icon
            $("#" + currentlyDisplayedOrderGuid + " td.td_0").css("background-image", "url('/Images/icons/ArrowRightCart.gif')");
            detailDisplay.remove();
            displayRow.remove();
        });
    };
    //clear data
    model.shippedDetailData = {};
    //alert(callbacks);
    vModel.ProcessEventChain(callbacks);
};

vModel.shipped.insertDetailDisplay = function (orderGuid) {
    //alert("Grower Order Guid" + orderGuid);


    var currentDetailContainer = $("#" + vModel.shipped.config.summaryGrid.detailContainerId)
    var currentlyDisplayedOrderGuid = currentDetailContainer.data("orderGuid") || "";
    if (orderGuid != currentlyDisplayedOrderGuid) {
        $("#" + orderGuid + " td.td_0").css("background-image", "url('/Images/icons/Loading_Availability.gif')");
        //alert("row clicked [ Order Guid: " + orderGuid + " ]");

        //insert row in grid spanning all columns
        var row = $("<tr></tr>")
            .prop({ "id": vModel.shipped.config.summaryGrid.detailRowId })
            .css({ "display": "none", "border": "none" });

        var td = $("<td></td>")
            .prop({ "colspan": vModel.shipped.config.summaryGrid.Columns });

        var div = $("<div />")
            .prop({ "id": vModel.shipped.config.summaryGrid.detailContainerId })
            .data("orderGuid", orderGuid);

        td.append(div);
        row.append(td);

        row.insertAfter("#" + orderGuid);
        model.shipped.getDetailData(orderGuid, [function () {
            //alert("Orders: " + model.shipped.state.growerOrdersCount());

            vModel.shipped.displayDetail(row);
        }]);
    };
};

vModel.shipped.displayDetail = function (row) {
    var target = $("#" + vModel.shipped.config.summaryGrid.detailContainerId);
    var detailDisplay = vModel.shipped.config.detailDisplay.template.clone(true);
    target.append(detailDisplay);
    vModel.shipped.refreshDetailDisplay();
    //detailRow = $("#" + vModel.shipped.config.summaryGrid.detailRowId);
    row.fadeIn("slow");
}

vModel.shipped.refreshDetailDisplay = function () {
    //set heading
    var poString = "";
    if (model.shipped.DetailData.CustomerPoNo != null)
    {
        poString = " - " + model.shipped.DetailData.CustomerPoNo ;
    }
    $("#" + vModel.shipped.config.detailDisplay.openOrderDetailHeaderBarId).text(
        "Week " + vModel.ShipWeekCodeToShipWeekString(model.shipped.DetailData.ShipWeekCode) + " - " +
        (model.shipped.DetailData.OrderNo || null) + 
        poString
        );
    //set status:
    $("#" + vModel.shipped.config.detailDisplay.orderStatusId).text(model.shipped.DetailData.GrowerOrderStatusName || '');
    //set order number
    $("#" + vModel.shipped.config.detailDisplay.orderNumberId).text(model.shipped.DetailData.OrderNo || '');
    //set PO
    $("#" + vModel.shipped.config.detailDisplay.purchaseOrderSpanId).text(model.shipped.DetailData.CustomerPoNo || '');
   // vModel.shipped.wireUpPurchaseOrderInput();
    //set Description
    $("#" + vModel.shipped.config.detailDisplay.orderDescriptionSpanId).text(model.shipped.DetailData.OrderDescription || '');
  //  vModel.shipped.wireUpOrderDescriptionInput();
    //set ship-to
    var selectedShipTo = (function () {
        var shipToAddresses = model.shipped.DetailData.ShipToAddresses || [];
        var shipToAddress = null;
        for (var i = 0, len = shipToAddresses.length; i < len; i++) {
            shipToAddress = shipToAddresses[i];  //take first address found
            if (shipToAddresses[i].IsSelected) {
                return shipToAddresses[i];  //object found return it
            } else if (shipToAddresses[i].IsDefault) {
                shipToAddress = shipToAddresses[i];  //update default to this
            };
        }
        return shipToAddress; //not found
    })();
    if (selectedShipTo !== null) {
        $("#" + vModel.shipped.config.detailDisplay.ShipToIdStem + "0").text(selectedShipTo.Name || '');
        //address line 1
        var t = $("#" + vModel.shipped.config.detailDisplay.ShipToIdStem + "1");
        var v = selectedShipTo.StreetAddress1 || null;
        if (v != null) { t.text(v); } else { t.remove(); };
        //address line 2
        t = $("#" + vModel.shipped.config.detailDisplay.ShipToIdStem + "2");
        v = selectedShipTo.StreetAddress2 || null;
        if (v != null) {
            t.text(v);
        } else {
            $(t).parents("tr:first").remove();
        };
        //city, state zip        
        $("#" + vModel.shipped.config.detailDisplay.ShipToIdStem + "3").text(
            (selectedShipTo.City || null) + ", " +
            (selectedShipTo.StateCode || null) + " " +
            (selectedShipTo.ZipCode || null)

            );
        $("#" + vModel.shipped.config.detailDisplay.ShipToIdStem + "4").text(selectedShipTo.Country || null);
    }
    //set payment method
    var selectedPaymentMethod = (function () {
        var method = {};
        method.Type = null;
        method.Card = null;
        $.each(model.shipped.DetailData.PaymentTypeList, function (i, type) {
            if ((type.IsDefault && method.Type == null) || type.IsSelected) {
                method.Type = type.DisplayName;            
            };
        });
        if (method.Type == "Credit Card") {
            $.each(model.shipped.DetailData.CreditCards, function (i, card) {
                if ((card.IsDefault && method.Card == null) || card.IsSelected) {
                    method.Card = card.CardType + " ending: " + card.CardNumber
                };
            });
        };
        return method; //not found
    })();
    if (selectedPaymentMethod != null) {
        //alert(selectedPaymentMethod.Type);
        $("#" + vModel.shipped.config.detailDisplay.PaymentMethodIdStem + "0").text(selectedPaymentMethod.Type || '');
        $("#" + vModel.shipped.config.detailDisplay.PaymentMethodIdStem + "1").text(selectedPaymentMethod.Card || '');
    };

    //Initialize Order History
    var onExpandOrderHistory = function (containerId) {
        eps_expanse.state.setWorking(containerId);
        eps_expanse.setStatus(containerId, "Loading ...");
        var display = eps_expanse.state.getDisplayElement(containerId).empty();
        var orderGuid = vModel.shipped.state.displayedOrderGuid();
        var onSuccess = function (data) {
            model.open.OrderHistoryData = data;
        };

        var uri = model.config.urlGetDetailHistoryData(orderGuid);
        model.shipped.OrderHistoryData = {};
        model.ajaxCall(null, uri, "get", onSuccess, [function () {
            var historyData = model.shipped.OrderHistoryData.eventList || [];
            vModel.composeHistoryData(display, orderGuid, historyData);
            eps_expanse.setStatus(containerId, "Changes: " + historyData.length);
            eps_expanse.expand(containerId, null);
        }]);
    };
    var onCollapseOrderHistory = function (containerId) {
        eps_expanse.state.setWorking(containerId);
        eps_expanse.collapse(containerId, null, true);
        eps_expanse.setStatus(containerId, "...");
    };

    eps_expanse.init(vModel.shipped.config.detailDisplay.openOrderHistoryContainerId, onExpandOrderHistory, onCollapseOrderHistory, "Order History", "...");

    //Clear Supplier Orders from Detail Display
    $("#" + vModel.shipped.config.SupplierOrders.ContainerId).empty();

    //Add Supplier Orders
    $.each(model.shipped.DetailData.SupplierOrders, function (i, order) {
        var clone = vModel.shipped.config.SupplierOrders.template.clone(true);
        clone.prop({
            "id": order.SupplierOrderGuid
        });
      
        clone.find("." + vModel.shipped.config.SupplierOrders.SupplierNameSpanClass).text(order.SupplierName);

        ////// Tag Ratio ////////
        var tagRatioDisplay = $(clone).find("." + vModel.shipped.config.SupplierOrders.tagRatioDisplayClass)
        $(tagRatioDisplay).children("." + vModel.shipped.config.SupplierOrders.tagRatioSelectedClass).text(model.shipped.ReturnSelectedTagRatio(order.SupplierOrderGuid));

        //// Ship Method /////
        var shipMethodDisplay = $(clone).find("." + vModel.shipped.config.SupplierOrders.shipMethodDisplayClass)
        $(shipMethodDisplay).children("." + vModel.shipped.config.SupplierOrders.shipMethodSelectedClass).text(model.shipped.ReturnSelectedShipMethod(order.SupplierOrderGuid));

        ///// Line Item Grid: Body Section /////
        var theDiv = $("<div />");
        var thrDivProperties = { "class": "line_item_grids" };
        theDiv.prop(thrDivProperties);
  

  
        var orderDiv = $("<div />");
        var orderDivProperties = { "class": "cart_line_item" };
        orderDiv.prop(orderDivProperties);

        vModel.addSupplierOrderLinesToDiv(orderDiv, order);
        
        theDiv.append(orderDiv);

        var productTotalsDiv = $("<div />");
        var productTotalsDivProperties = { "id": "supplier_totals_count" };
        productTotalsDiv.prop(productTotalsDivProperties);

        var ul = $("<ul>");
        var ulProperties = {"id": "supplier_totals_count_list" };
        ul.prop(ulProperties);
        
      
        var li = $("<li />");
        var span = $('<span />');
        span.prop({
            "id": order.SupplierOrderGuid + "_supplier_order_tray_totals",
            "class": "supplier_order_tray_totals",
        });
        li.text("Total Trays: ");
        li.append(span);
        ul.append(li);                    
 
        var li = $("<li />");
        var span = $('<span />');
        span.prop({
            "id": order.SupplierOrderGuid + "_supplier_order_quantity_totals", "class": "supplier_order_quantity_totals",
        });
        li.text("Total Qty: ");
        li.append(span);
        ul.append(li);                    


        productTotalsDiv.append(ul);
        //theDiv.append(productTotalsDiv);

        var supplierSubtotalsDiv = $("<div />");
        var supplierSubtotalsDivProperties = { "id": "supplier_subtotals" };
        supplierSubtotalsDiv.prop(supplierSubtotalsDivProperties);

        var ul = $("<ul>");
        var ulProperties = { "id": "supplier_product_subtotal",
            "class" : "supplier_subtotal_boxes"};
        ul.prop(ulProperties);

        var li = $("<li />");
        var liProperties = { "class": "left" };
        var span = $('<span />');
        span.prop({
            "class": "supplier_name_span2",
            "id": order.SupplierOrderGuid + "_supplier_name_span"
        });
        li.append(span);
        ul.append(li);

        var li = $("<li />");
        var liProperties = { "class": "right" };
        var span = $('<span />');
        span.prop({
            "id": order.SupplierOrderGuid + "_supplier_order_product_cost_totals"
            , "class": "supplier_order_product_cost_totals",
        });   
        li.append(span);
        ul.append(li);  
        supplierSubtotalsDiv.append(ul);

        
        $(clone).children("#supplier_fees").remove();
        var supplieFeesDiv = $("<div />");
        var supplieFeesDivProperties = { "id": order.SupplierOrderGuid +  "_supplier_fees" };
        supplieFeesDiv.prop(supplieFeesDivProperties);
        // supplieFeesDiv.html("<p>hi Scott</p>");
        supplierSubtotalsDiv.append(supplieFeesDiv);
       
        
        $(clone).children("#supplier_totals").remove();
        var supplieSubtotalsDiv = $("<div />");
        var supplieSubtotalsDivProperties = { "id": order.SupplierOrderGuid + "_supplier_totals" };
        supplieSubtotalsDiv.prop(supplieSubtotalsDivProperties);
        supplierSubtotalsDiv.append(supplieSubtotalsDiv);

      
        $(clone).children("." + vModel.shipped.config.SupplierOrders.LineItemGrid.ClassName).replaceWith(theDiv);
        $(clone).children("#supplier_totals_count").replaceWith(productTotalsDiv);
        $(clone).children("#supplier_subtotals").replaceWith(supplierSubtotalsDiv);

       
        $("#" + vModel.shipped.config.SupplierOrders.ContainerId).append(clone);
    });

    vModel.refreshTotalsCascade("shipped",null);  //null value means refresh all supplier orders
    $("#" + model.shipped.state.currentGrowerOrderGuid() + " td.td_0").css("background-image", "url('/Images/icons/ArrowDownCart.gif')");

};





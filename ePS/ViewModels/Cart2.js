﻿
/////////////////////////// Cart Model Config ///////////////////////////////
var model = {};
model.configData = {
    isConfigured: false,
    urlCartApi: '/api/cart',
    urlCartOrderUpdateApi: '/api/cartUpdate',
    urlOrderApi: '/api/order',
    urlPromoCodeApi: '/api/Cart/GetPromoCode',
    urlShipToAddressApi: '/api/shiptoaddress',
    urlCardOnFileApi: '/api/CardOnFile',
    urlUpdateCreditCardPutApi: "/api/V2/Grower/UpdateCreditCard",
    urlDeleteCreditCardPutApi: "/api/V2/Grower/DeleteCreditCard",
    urlGetShipToAddresses: function (growerOrderGuid) { return model.configData.urlShipToAddressApi + "/" + growerOrderGuid + "?mode=GrowerOrderGuid"; },
    urlGetCardsOnFile: function (growerOrderGuid) { return model.configData.urlCardOnFileApi + "/" + growerOrderGuid + "?mode=GrowerOrderGuid"; },
    urlGetSummaryData: '/api/Order/MyShoppingCartOrders',
    urlDeleteOrder: function (guid) { return model.configData.urlCartApi + "/" + guid; },
    urlGetDetailData: function (guid) { return model.configData.urlCartApi + "/DetailData?OrderGuid=" + guid; }

};
model.state = {
    currentGrowerOrderGuid: function () { return model.DetailData.GrowerOrder.OrderGuid || ""; },
    selectedAddressGuid: null,
    paymentMethodSelected: function () {
        returnValue = viewModel.Config.DetailDisplay.PaymentMethodDefaultValue;  //default from code
        $.each(model.DetailData.GrowerOrder.PaymentTypeList, function (i, method) {
            if (method.IsSelected) {
                returnValue = method.Code;
                return false;   //finished
            } else if (method.IsDefault) {
                returnValue = method.Code;  //default from data
            };
        });
        return returnValue;
    },
    promoName: "",
    isReadOnly: false,
};

model.stateData = [];
model.provinceData = [];
model.SummaryData = [];
model.DetailData = {};


//////////////////////////Cart View Model Config ///////////////////////////

var viewModel = {};

viewModel.Config = {
    OpenOrdersDisplayId: 'cart_orders_display',
    ///// Todo: consolidate these to one class for styling /////
    OrderButtonClass: "order_buttons",
    ContinueButtonClass: "continue_buttons",
    DeleteButtonClass: "delete_buttons",
    ////////////////////////////////////////////////////////////

    SupplierOrderClone: null,
    Forms: {
        ClassName: "cart_page_forms",
        //NewAddressDisplayId: "new_address_form_display",
        AddressFormTemplateId: "address_form_template",
        AddressFormId: "new_address_form",
        AddressFormMessageClass: "ship_to_message",
        AddressFormInputBoxesClass: "inputTextBox",

        AddressSaveButtonId: "ship_to_button_save",
        AddressDeleteButtonId: "ship_to_button_delete",
        AddressCancelButtonId: "ship_to_button_cancel",
        AddressFormTemplate: null,
        AddressForm: null,

        ShipToGrowerShipToAddressGuidId: "hidden_ship_to_address_guid",
        ShipToGrowerGuidId: "hidden_grower_guid",
        ShipToNameId: "ship_to_name",
        ShipToStreetAddress1: "ship_to_street_address_1",
        ShipToStreetAddress2: "ship_to_street_address_2",
        ShipToCity: "ship_to_city",
        ShipToZipCode: "ship_to_zip_code",
        ShipToPhoneNumber: "ship_to_phone_number",
        ShipToCountryCodeName: "ship_to_country",
        ShipToStateCode: "ship_to_state_code",
        ShipToSpecialInstructions: "ship_to_special_instructions",

        CardOnFileFormAddTemplateId: "credit_card_add_form_template",
        CardOnFileFormId: "credit_card_form",
        CardOnFileFormMessageClass: "credit_card_message",
        CardOnFileFormInputBoxesClass: "inputTextBox",

        CardOnFileSendButtonId: "button_credit_card_create_add",
        CardOnFileCancelButtonId: "button_credit_card_create_cancel",

        CardOnFileFormTemplate: null,
        CardOnFileForm: null,

        CardOnFileDescription: "credit_card_description",

    },
    Icons: {
        ItemCheckedSrc: "/images/layout/green_check.png",
        AjaxWorkingSrc: "/images/layout/timer.png",
        DotClearSrc: "/images/icons/mag_glass_plus.png"

    },
    SummaryGrid: {
        Id: "order_summary_grid",
        RowClass: "order_summary_grid_rows",
        Columns: 5,
        ColumnWidths: ["4%", "24%", "24%", "24%", "24%"],
        Headings: [" ", "Week", "Category", "Form", "Total Units"],
        ShowHeadings: true,
        DetailRowId: "grower_order_row",
        DetailContainerId: "grower_order_container"
    },
    DetailDisplay: {

        Id: "order_detail_display",
        headerBarClass: "header_bars",
        TextInputDisplaysClass: "text_input_displays",

        //OrderDescriptionClass: "input_set_displays",

        OrderDescriptionDisplayId: "order_description_display",
        PurchaseOrderInputId: "purchase_order_input",
        OrderDescriptionInput: "order_description_input",
        PromotionalCodeInput: "promotional_code_input",
        PromotionalDescriptionID: "promotional_description_label",





        ShipToTableId: "ship_to_table",
        ShipToEditLinkClass: "ship_to_edit_links",

        ShipToRadioButtonsName: "ship_to_radio_buttons_name",
        ShipToRadioButtonsClass: "ship_to_radio_buttons",

        PaymentInformationContainerId: "payment_information_container",


        PaymentMethodDefaultValue: "Invoice",


        PaymentMethodSelectionDisplayId: "payment_method_selection_display",
        PaymentMethodRadioButtonsName: "payment_method_radio_buttons_name",
        PaymentMethodRadioButtonsClass: "payment_method_radio_buttons",


        CreditCardDisplayId: "credit_card_display",
        CreditCardTableId: "credit_card_table",
        CreditCardDescriptionsClass: "credit_card_description_inputs",
        CreditCardEditLinksClass: "credit_card_edit_links",
        CreditCardDeleteLinksClass: "credit_card_delete_links",
        CreditCardRadioButtonsName: "credit_card_radio_buttons_name",
        CreditCardRadioButtonsClass: "credit_card_radio_buttons",

        AgreeToTermsCheckboxId: "agree_to_terms_checkbox",
        CreditCardRadioButtonsIdStem: "credit_card_selection_",

        orderTotalsGrid: {
            id: "order_totals_table",

            growerOrderFeesRowTemplateId: "grower_order_fees_row_template",
            growerOrderFeesRowTemplate$: null,

            growerOrderFeeInfoSpans: "grower_order_fee_info_icons",
            growerOrderFeeStatusDescription: "grower_order_fee_status_description",
            growerOrderFeeTypeDescription: "grower_order_fee_type_description",
            growerOrderFeeSpansClass: "grower_order_fee_spans",

            totalTraysSpanId: "grower_order_total_trays_span",
            totalCuttingsSpanId: "grower_order_total_cuttings_span",
            totalCuttingsCostSpanId: "grower_order_total_cuttings_cost_span",


        },
        ButtonDisplayId: "button_display",
        ButtonOrderId: "order_button",
        ButtonContinueShoppingId: "continue_shopping_button",
        ButtonDeleteOrderId: "delete_order_button",
        ButtonAddAddressId: "add_new_address_button",
        ButtonAddCardId: "add_new_card_button",
        //ButtonAddCardSecureId: "add_new_card_button_secure",

    },
    SupplierOrders: {
        TemplateId: "supplier_orders_template",
        ContainerId: "supplier_orders_container",
        DisplaysClass: "supplier_order_displays",
        SupplierNameSpanClass: "supplier_name_span",

        //DEPRECATED DropdownDisplaysClass: "supplier_order_dropdown_containers",,
        DropdownDisplaysClass: "supplier_order_dropdown_containers_new",

        //DEPRECATED tagRatioSelectorDisplayClass: "supplier_order_selector_display_left",
        tagRatioSelectorDisplayClass: "supplier_order_selector_display_upper",
        tagRatioSelectedDisplayClass: "tag_ratio_selected_spans",
        tagRatioSelectorClass: "tag_ratio_selectors",

        //DEPRECATED shipMethodSelectorDisplayClass: "supplier_order_selector_display_right",
        shipMethodSelectorDisplayClass: "supplier_order_selector_display_lower",
        shipMethodSelectedDisplayClass: "ship_method_selected_spans",
        shipMethodSelectorClass: "shipment_method_selectors",

        LineItemGrid: {
            ClassName: "line_item_grids",
            RowClass: "line_item_grid_rows",

            OrderQuantityInputClass: "line_item_grid_order_quantity_inputs",
            OrderQuantitySpanClass: "line_item_grid_order_quantity_spans",
            OrderQuantityImgClass: "line_item_grid_order_quantity_imgs",


            dynamicCostRowsTemplateId: "dynamic_cost_rows_template",
            dynamicCostRowsTemplate$: null,



            subTotalRowsClass: "product_cost_rows",
            dynamicCostRowsClass: "dynamic_cost_rows",
            totalCostRowsClass: "total_cost_rows",


            supplierOrderFeeInfoSpans: "supplier_order_fee_info_icons",
            supplierOrderFeeStatusDescription: "supplier_order_fee_status_description",
            supplierOrderFeeTypeDescription: "supplier_order_fee_type_description",
            lineItemFooterCostsClass: "line_item_footer_costs",
            supplierOrderTotalCostsClass: "supplier_order_total_costs",

            OrderQuantityTotalClass: "supplier_order_quantity_totals",
            trayTotalsClass: "supplier_order_tray_totals",
            PriceTotalsClass: "supplier_order_product_cost_totals",
            UpdateLinksClass: "supplier_order_update_links",
            IdStemHeaderRows: "headerRow_",
            ClassStemHeaderCols: "headerCol_",
            IdStemFooterRows: "footerRow_",
            ClassStemFooterCols: "footerCol_",
            Columns: 7,
            ShowHeadings: true,
            Headings: ["Species", "Product", "Trays--", "Form", "Quantity", "Unit Price", "Price", "Remove"],   //deprecated
            ColumnWidths: ["10%", "20%", "12.5%", "12.5%", "15", "10%", "10%", "10%"],
            FooterRows: 1,
            ColsClassStemHeader: "headerCol_",
            ColumnClassStemBody: "bodyCol_",
            ColumnClassStemFooter: "footerCol_",
            ToggleRemoveSpanClass: "toggle_remove_spans",
            ToggleRemoveSelectedClass: "toggle_remove_selected",

        },

    }
};




model.getData = function (element, url, onSuccess, onFailure, callbacks) {
    //var testDisplay = $("#test_area_results_display").empty();
    //var p = $("<p />")
    //    .text("[GET] url : " + url);
    //testDisplay.append(p, $("<hr />"));
    $.ajax({
        type: "get",
        url: url,
        dataType: "json",
    })
    .done(function (data, textStatus, jqXHR) {
        //testDisplay.append(traverseObj(data));
        onSuccess(data);
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        //var errorMsg = "TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown;
        //testDisplay.append(errorMsg);
        //if (typeof element !== "undefined" & element != null) {
        //    viewModel.floatMessage(element, errorMsg, 4000);
        //};
        onFailure(jqXHR, textStatus, errorThrown);

    })
    .always(function () {
        viewModel.ProcessEventChain(callbacks);
    });

};


model.GetDetailData = function (guid, callbacks) {
    model.DetailData = [];
    if (typeof guid === 'undefined' || guid == null) {
        viewModel.ProcessEventChain(callbacks);
        return false;
    };
    $.ajax({
        datatype: "json",
        url: model.configData.urlGetDetailData(guid),
        type: "get",
        beforeSend: function (jqXHR, settings) {
            //clear current state
            model.DetailData = {};
        },
        success: function (response, textStatus, jqXHR) {
            model.DetailData.GrowerOrder = response;
            //ToDo: complete this
            //if (response.IsAuthenticated) {
            //    model.DetailData = response.DetailData[0];
            //} else {
            //    //ToDo: Handle response.IsAuthenticated == false
            //    alert("you must be logged in to view Order Detail");
            //}
            //  alert(JSON.stringify(model.DetailData));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {

            viewModel.ProcessEventChain(callbacks);
        }
    });
};
model.getSummaryData = function (callbacks) {
    //Note: takes optional parameter that is array of callbacks to execute in order after completion
    //Make ajax call to retrieve summary data
    $.ajax({
        datatype: "json",
        url: model.configData.urlGetSummaryData + "?TheType=Cart",
        type: "get",
        beforeSend: function (jqXHR, settings) {
            model.SummaryData = [];
            //alert("AJAX url: " + this.url);
        },
        success: function (response, textStatus, jqXHR) {
            if (response.IsAuthenticated) {
                //   alert(response.IsReadOnly);
                model.SummaryData = response.SummaryData;
                model.state.isReadOnly = response.IsReadOnly;
            } else {
                //ToDo: Handle response.IsAuthenticated == false
                alert("You must be logged-in to view your cart.");
            }
            //  alert(JSON.stringify(model.SummaryData));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {

            viewModel.ProcessEventChain(callbacks);
        }
    });
};

model.getStateProvinceData = function (callback) {
    var url = '/Scripts/Json/StateData.js';
    var jqxhr = $.getJSON(url, function (data) {
        model.stateData = data;
        //alert(traverseObj(model.stateData, false));
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        alert("TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown);
    })
    .done(
    function () {
        var url = '/Scripts/Json/ProvinceData.js';
        var jqxhr = $.getJSON(url, function (data) {
            model.provinceData = data;
            //alert(traverseObj(model.provinceData, false));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown);
        })
        .done(callback);

    });

};
model.getPromoName = function (promoCode, callbacks) {
    // alert("in function -" + promoCode + "-");
    if (promoCode.length == 0) {

        model.state.promoName = "";
        var element2 = $('#' + viewModel.Config.DetailDisplay.PromotionalDescriptionID);
        element2.text(model.state.promoName);
    }
    else {

        $.ajax({
            type: "get",
            url: model.configData.urlPromoCodeApi,
            data: {
                "PromoCode": promoCode,
                "PromoName": "hi"
            },
            datatype: "json",
            beforeSend: function (jqXHR, settings) {
                // alert("AJAX url: " + this.url);
            },
            success: function (response, textStatus, jqXHR) {


                // alert(traverseObj(response));
                model.state.promoName = response.PromoName;

                var element2 = $('#' + viewModel.Config.DetailDisplay.PromotionalDescriptionID);
                // alert("theName " + model.state.promoName);
                element2.text(model.state.promoName);

                viewModel.ProcessEventChain(callbacks);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
            },
            complete: function (jqXHR, textStatus) {
                // alert("end: " + model.state.promoName);
            }
        });
    }

};

model.PlaceOrder = function (guid) {
    $.ajax({
        type: "PUT",
        url: model.configData.urlOrderApi,
        data: viewModel.CompileOrderData(),
        //added by Scott June 2016
        //data: JSON.stringify(viewModel.CompileOrderData()),
        // contentType:"application/json; charset=utf-8",

        datatype: "json",



        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
        },
        success: function (response, textStatus, jqXHR) {


            if (!response.IsAuthenticated) {
                //Handle response.IsAuthenticated == false

                //alert("You must be logged-in to place order.");

                eps_tools.OkDialog({ 'title': '<span>Alert:</span>', 'content': '<br /><span>You must be logged-in to place order.<br />&nbsp;</span>', 'modal': true });

            } else if (response.Success) {
                //alert("Server API Response:\nPut request processed!\nOrderGuid: " + response.OrderGuid + "\nItems Ordered: " + response.ProductsOrdered);

                eps_tools.OkDialog({ 'title': '<span>Thank You!</span>', 'content': '<br /><span>Your order is placed. Please check your inbox for an email confirmation.<br />&nbsp;</span>', 'modal': true });

                //ToDo: Consider just using GetSummary Data 
                viewModel.RemoveDetailDisplay();
                model.getSummaryData([viewModel.DisplaySummaryGrid]);
            } else {
                //alert("Order failed.");
                eps_tools.OkDialog({ 'title': '<span>Error:</span>', 'content': '<br /><span>Place Order: failed.<br />&nbsp;</span>', 'modal': true });
            }
            //alert(JSON.stringify(model.SummaryData));



        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
        }
    });

};
model.DeleteOrder = function (guid, callbacks) {
    $.ajax({
        datatype: "json",
        url: model.configData.urlDeleteOrder(guid),
        type: "delete",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
        },
        success: function (response, textStatus, jqXHR) {
            //ToDo: Need Confirmation Dialog 
            if (!response.IsAuthenticated) {
                //ToDo: Handle response.IsAuthenticated == false
                alert("You must be logged-in to delete order."); //ToDo: Replace this with floatMessage or Dialog Box
            } else if (response.Success) {


                //alert("Items Deleted from Cart!\nOrderGuid: " + response.OrderGuid);    //ToDo: Replace this with floatMessage or Dialog Box

                viewModel.RemoveDetailDisplay();
                model.getSummaryData([viewModel.DisplaySummaryGrid]);
                eps_tools.OkDialog({ 'title': '<span>Cart:</span>', 'content': '<br /><span>Grower Order has been deleted.<br />&nbsp;</span>', 'modal': true });


            } else {

                eps_tools.OkDialog({ 'title': '<span>Error:</span>', 'content': '<br /><span>Delete Order: failed!<br />&nbsp;</span>', 'modal': true });

                //alert("Delete failed.");    //ToDo: Replace this with floatMessage or Dialog Box
            }
            //alert(JSON.stringify(model.SummaryData));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);

        }
    });

};
model.WriteOrderData = function (data, element, callbacks) {

    //element is solely for positioning floating message

    var resultString = "Write Order Data:\n";

    $.ajax({
        type: "PUT",
        url: model.configData.urlCartOrderUpdateApi,
        data: data,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
            resultString += "url: " + this.url + "\n";
            resultString += "data: " + JSON.stringify(this.data) + "\n";
        },
        success: function (response, textStatus, jqXHR) {
            if (typeof response !== 'undefined' && response != null) {
                //alert(JSON.stringify(response));
                resultString += "RESPONSE: " + JSON.stringify(response) + "\n";
                //ToDo: this interface stuff should be handled by a callback
                if (typeof element !== 'undefined' && element != null && element.length > 0) {
                    if (!response.IsAuthenticated) {
                        viewModel.floatMessage(element, "You must be logged-in to use this page.", 2000);
                        //eps_tools.OkDialog({ 'title': '<span>Error:</span>', 'content': '<br /><span>You must be logged-in to use this page.<br />&nbsp;</span>', 'modal': true });
                    } else if (response.Success) {
                        //viewModel.floatMessage(element, "Data updated.", 2000);
                        //eps_tools.OkDialog({ 'title': '<span>Cart:</span>', 'content': '<br /><span>Data updated.<br />&nbspO;</span>', 'modal': true });
                    } else {
                        viewModel.floatMessage(element, "Update Data: failed.", 2000);
                        //eps_tools.OkDialog({ 'title': '<span>Error:</span>', 'content': '<br /><span>Update Data: failed.<br />&nbsp;</span>', 'modal': true });
                    }
                };

            };
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            //alert(resultString);
            viewModel.ProcessEventChain(callbacks);
        }
    });
};

//Credit Card Stuff
model.ReturnCreditCardInfo = function (guid) {
    //alert(guid);
    if (typeof guid === 'undefined' || guid == null || guid.length != 36) { return null; };
    if (model.DetailData.GrowerOrder.CreditCards == null || model.DetailData.GrowerOrder.CreditCards.length == 0) { return null; };
    var items = $.grep(model.DetailData.GrowerOrder.CreditCards, function (item, i) {
        return item.Guid == guid;
    });

    if (items.length > 0) {
        return items[0];
    } else {
        return null;
    };

};
model.RefreshCardOnFileData = function (growerOrderGuid, callbacks) {
    // alert("RefreshCardOnFileData--" + growerOrderGuid);
    $.ajax({
        datatype: "json",
        url: model.configData.urlGetCardsOnFile(growerOrderGuid),
        type: "get",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
        },
        success: function (response, textStatus, jqXHR) {
            //write new data into Detail Data
            //   alert("REfresh   " + traverseObj(response.CreditCards));
            model.DetailData.GrowerOrder.CreditCards = response.CreditCards;
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {

            viewModel.ProcessEventChain(callbacks);
        }
    });
};
model.WriteNewCardOnFile = function (data, element, callbacks) {
    //element (optional) is message element to receive messages.

    var url = model.configData.urlCardOnFileApi;

    var resultString = "Write New CardOnFile:\n";


    $.ajax({
        type: "PUT",
        url: url,
        data: data,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
            resultString += "url: " + this.url + "\n";
            resultString += "data: " + JSON.stringify(this.data) + "\n";
            //alert(resultString);
        },
        success: function (response, textStatus, jqXHR) {
            if (typeof response !== 'undefined' && response != null) {
                //alert(JSON.stringify(response));
                resultString += "RESPONSE: " + JSON.stringify(response) + "\n";
                if (!response.IsAuthenticated) {
                    element.text("You must be logged-in to use this page.");
                } else if (response.Success) {
                    //update element
                    element.text("Data updated.");
                    //close dialog
                    eps_tools.Dialog.close();
                    //refresh card on file - selecting new one
                    //model.ReturnCreditCardInfo(response.CardGuid);
                    viewModel.RefreshCardOnFileDisplay(response.CardGuid);
                } else {
                    element.text(response.Message);
                }
            };
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            //alert(resultString);
            viewModel.ProcessEventChain(callbacks);
        }
    });

};
model.DeleteCardOnFile = function (guid, callbacks) {

    // alert("Delete: " + guid);
    var url = model.configData.urlDeleteCreditCardPutApi;
    //  alert(url);

    $.ajax({
        type: "put",
        data: {
            "CreditCardGuid": guid

        },
        url: url,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {

        },
        success: function (response, textStatus, jqXHR) {

            //  targetResponse.html("SERVER RESPONSE: <br />" + JSON.stringify(response));
            if (response.Success) {

                // alert(viewModel.Config.DetailDisplay.CreditCardDescriptionsClass)
                //    
                viewModel.floatMessage($('.' + viewModel.Config.DetailDisplay.CreditCardDescriptionsClass), 'Card Deleted', 2500);
                vieModel.RefreshCardOnFileDisplay(null);


            } else {
                viewModel.floatMessage($('.' + viewModel.Config.DetailDisplay.CreditCardDescriptionsClass), 'Error: Card Not Deleted', 2500);
            };
        },
        complete: function (jqXHR, textStatus) {
            // alert("bongo");
        }
    });



    viewModel.ProcessEventChain(callbacks);
};

model.UpdateCardOnFileDescription = function (values, callbacks) {
    var url = model.configData.urlUpdateCreditCardPutApi;
    //  alert(url);

    $.ajax({
        type: "put",
        data: {
            "CreditCardGuid": values.guid,
            "CardDescription": values.description
        },
        url: url,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {

        },
        success: function (response, textStatus, jqXHR) {

            //  targetResponse.html("SERVER RESPONSE: <br />" + JSON.stringify(response));
            if (response.Success) {

                // alert(viewModel.Config.DetailDisplay.CreditCardDescriptionsClass)
                //    
                viewModel.floatMessage($('.' + viewModel.Config.DetailDisplay.CreditCardDescriptionsClass), 'Card Description Updated', 2500);


            } else {
                viewModel.floatMessage($('.' + viewModel.Config.DetailDisplay.CreditCardDescriptionsClass), 'Error: Card Description Not Updated', 2500);
            };
        },
        complete: function (jqXHR, textStatus) {
            // alert("bongo");
        }
    });





};

//ShipTo stuff
model.UpdateShipToAddressData = function (growerOrderGuid, callbacks) {

    var url = model.configData.urlGetShipToAddresses(growerOrderGuid);
    $.ajax({
        datatype: "json",
        url: url,
        type: "get",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
        },
        success: function (response, textStatus, jqXHR) {
            //write new data into Detail Data
            model.DetailData.GrowerOrder.ShipToAddresses = response.ShipToAddressList;
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {

            viewModel.ProcessEventChain(callbacks);
        }
    });

};
model.WriteShipToAddressNew = function (data, element, callbacks) {

    //element (optional) is message element to receive messages.

    var resultString = "Write New ShipTo Address:\n";
    $.ajax({
        type: "POST",
        url: model.configData.urlShipToAddressApi,
        data: data,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
            resultString += "url: " + this.url + "\n";
            resultString += "data: " + JSON.stringify(this.data) + "\n";
        },
        success: function (response, textStatus, jqXHR) {
            if (typeof response !== 'undefined' && response != null) {
                //alert(JSON.stringify(response));
                resultString += "RESPONSE: " + JSON.stringify(response) + "\n";
                if (!response.IsAuthenticated) {
                    element.text("You must be logged-in to use this page.");
                } else if (response.Success) {
                    //update element
                    element.text("Data updated.");
                    //close dialog
                    eps_tools.Dialog.close();
                    //refresh ship-to addresses - selecting new one
                    viewModel.RefreshShipToDisplay(response.GrowerAddressGuid, response.AddressGuid)
                } else {
                    element.text(response.Message);
                }
            };
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            //alert(resultString);
            viewModel.ProcessEventChain(callbacks);
        }
    });
};
model.WriteShipToAddressUpdate = function (data, element, callbacks) {

    //element (optional) is message element to receive messages.
    //alert(element.length);
    var resultString = "Write New ShipTo Address:\n";
    $.ajax({
        type: "PUT",
        url: model.configData.urlShipToAddressApi,
        data: data,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
            resultString += "url: " + this.url + "\n";
            resultString += "data: " + JSON.stringify(this.data) + "\n";
        },
        success: function (response, textStatus, jqXHR) {
            if (typeof response !== 'undefined' && response != null) {
                //alert(JSON.stringify(response));
                resultString += "RESPONSE: " + JSON.stringify(response) + "\n";
                if (!response.IsAuthenticated) {
                    element.text("You must be logged-in to use this page.");
                } else if (response.Success) {
                    //update element
                    element.text("Data updated.");
                    //close dialog
                    eps_tools.Dialog.close();
                    //refresh ship-to addresses - selecting new one
                    viewModel.RefreshShipToDisplay(response.GrowerAddressGuid, response.AddressGuid)
                } else {
                    element.text(response.Message);
                }
            };
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            //alert(resultString);
            viewModel.ProcessEventChain(callbacks);
        }
    });
};
model.WriteShipToAddressDelete = function (addressGuid, element, callbacks) {
    //element (optional) is message element to receive messages.
    var resultString = "Write New ShipTo Address:\n";
    $.ajax({
        type: "DELETE",
        url: model.configData.urlShipToAddressApi,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
            this.url += "/" + addressGuid;
            resultString += "url: " + this.url + "\n";
        },
        success: function (response, textStatus, jqXHR) {
            if (typeof response !== 'undefined' && response != null) {
                //alert(JSON.stringify(response));
                resultString += "RESPONSE: " + JSON.stringify(response) + "\n";
                if (!response.IsAuthenticated) {
                    element.text("You must be logged-in to use this page.");
                } else if (response.Success) {
                    //update element
                    element.text("Data updated.");
                    //close dialog
                    eps_tools.Dialog.close();
                    //refresh ship-to addresses - selecting new one
                    viewModel.RefreshShipToDisplay(response.GrowerAddressGuid, response.AddressGuid)
                } else {
                    element.text(response.Message);
                }
            };
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            //alert(resultString);
            viewModel.ProcessEventChain(callbacks);
        }
    });

};

//Supplier Order Stuff
model.SupplierOrdersExists = function () {
    if (!model.GrowerOrderExists) { return false; };
    if (typeof model.DetailData.GrowerOrder.SupplierOrders === 'undefined' || model.DetailData.GrowerOrder.SupplierOrders == null) { return false; };
    return true;
};
model.SupplierOrderCount = function () {
    if (!model.SupplierOrdersExists) { return 0; };
    return model.DetailData.GrowerOrder.SupplierOrders.length;
};
model.OrderLineCount = function (supplierOrderGuid) {
    if (typeof supplierOrderGuid === 'undefined' || supplierOrderGuid == null || supplierOrderGuid.length != 36) { return 0; };
    if (!model.SupplierOrdersExists) { return 0; };
    var returnValue = 0;
    $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, order) {
        if (order.SupplierOrderGuid == supplierOrderGuid) {
            if (typeof order.OrderLines !== 'undefined' && order.OrderLines != null) {
                returnValue = order.OrderLines.length;
            };
            return false;  //exit loop
        }
    });
    return returnValue;
};
model.GetSupplierOrderFromGuid = function (supplierOrderGuid) {
    if (typeof supplierOrderGuid === 'undefined' || supplierOrderGuid == null || supplierOrderGuid.length != 36) { return null; };
    if (model.DetailData == null || model.DetailData.GrowerOrder.SupplierOrders.length == 0) { return false; };

    var items = $.grep(model.DetailData.GrowerOrder.SupplierOrders, function (item, i) {
        return item.SupplierOrderGuid == supplierOrderGuid;
    });

    if (items.length > 0) {
        return items[0];
    } else {
        return null;
    };

};
model.ReturnSupplierOrderGuid = function (orderLineGuid) {
    //alert("in ReturnSupplierOrderGuid " + orderLineGuid);
    if (orderLineGuid == null || orderLineGuid.length < 36) { return "" };
    // if ($("#" + orderLineGuid).length < 1) { return "" };
    var returnGuid = "";
    $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, order) {
        $.each(order.OrderLines, function (i, line) {
            if (line.Guid == orderLineGuid) {
                returnGuid = order.SupplierOrderGuid;
            };
        });
    });
    return returnGuid;
};


model.getSupplierOrderFees = function (supplierOrderGuid, callbacks) {
    var onFailure = function (jqXHR, textStatus, errorThrown) {
        eps_tools.FloatingMessage(null, null, "<p>Error: " + errorThrown + "</p>", 3500);
    };
    var onSuccess = function (data) {
        //alert(traverseObj(data, false));
        if (data.Success) {
            //alert(traverseObj(data, false));
            var fees = data.SupplierOrderFees || [];
            if (fees == null || fees == "") { fees = [] };
            model.GetSupplierOrderFromGuid(supplierOrderGuid).SupplierOrderFees = fees;
            // alert(traverseObj(fees));
        } else {
            //Error on Server
            eps_tools.FloatingMessage(null, null, "<p>Error: Server error while retrieving Supplier Order fees.</p>", 4000);
        };
    };
    var callAfter = [function () {
        viewModel.ProcessEventChain(callbacks)
    }];
    //  alert(supplierOrderGuid);
    var url = "/api/Order/SupplierOrderFees?supplierOrderGuid=" + supplierOrderGuid;
    model.getData($(this), url, onSuccess, onFailure, callAfter);
};
model.returnSupplierOrderFees = function (supplierOrderGuid) {
    return model.GetSupplierOrderFromGuid(supplierOrderGuid).SupplierOrderFees || [];
};
model.getGrowerOrderFees = function (callbacks) {
    // alert("Get Order Fees");
    var onSuccess = function (data) {
        // alert(traverseObj(data, false));
        if (data.Success) {
            model.DetailData.GrowerOrder.GrowerOrderFees = data.GrowerOrderFees || [];
        } else {
            //Error on Server
            eps_tools.FloatingMessage(null, null, "<p>Error: Server error while retrieving Grower Order fees.</p>", 4000);
        };
    };

    var onFailure = function (jqXHR, textStatus, errorThrown) {
        eps_tools.FloatingMessage(null, null, "<p>Error: " + errorThrown + "</p>", 3500);
    };

    var callAfter = [function () {
        viewModel.ProcessEventChain(callbacks)
    }];

    //alert("Getting Fee Data: " + viewModel.state.displayedOrderGuid());
    var url = "/api/Order/GrowerOrderFees?growerOrderGuid=" + viewModel.state.displayedOrderGuid();
    model.getData($(this), url, onSuccess, onFailure, callAfter);

};
model.returnGrowerOrderFees = function () {
    return model.DetailData.GrowerOrder.GrowerOrderFees || [];
};



//These apply to old FeeList not new Fees Array
model.returnSupplierOrderFeeList = function (supplierOrderGuid) {
    var supplierOrder = model.GetSupplierOrderFromGuid(supplierOrderGuid) || null;
    var feeList = supplierOrder.SupplierOrderFeeList || null;
    return feeList;
};
model.replaceSupplierOrderFeeList = function (supplierOrderGuid, supplierOrderFeeList) {
    var supplierOrder = model.GetSupplierOrderFromGuid(supplierOrderGuid) || null;
    var feeList = supplierOrder.SupplierOrderFeeList || null;
    feeList.remove();
    supplierOrder.SupplierOrderFeeList = supplierOrderFeeList
};

model.GetSummaryItemFromOrderGuid = function (orderGuid) {
    if (typeof orderGuid === 'undefined' || orderGuid == null || orderGuid.length != 36) { return null; };
    if (model.SummaryData == null || model.SummaryData.length == 0) { return false; };

    var items = $.grep(model.SummaryData, function (item, i) {
        return item.OrderGuid == orderGuid;
    });

    if (items.length > 0) {
        return items[0];
    } else {
        return null;
    };

};
model.ReturnOrderLinePrice = function (guid) {
    var valu = 0.00;
    $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, order) {
        $.each(order.OrderLines, function (i, line) {
            if (line.Guid == guid) {
                valu = line.Price;
            };
        });
    });
    return valu;
};
model.ReturnOrderLineQuantityOrdered = function (guid) {
    var valu = 0;
    $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, order) {
        $.each(order.OrderLines, function (i, line) {
            if (line.Guid == guid) {
                valu = line.QuantityOrdered;
            };
        });
    });
    return valu;
};
model.GetOrderLineFromGuid = function (orderLineGuid) {
    if (typeof orderLineGuid === 'undefined' || orderLineGuid == null || orderLineGuid.length != 36) { return null; };
    if (model.DetailData == null || model.DetailData.GrowerOrder.SupplierOrders.length == 0) { return false; };
    var orderLine = null;
    $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, order) {

        $.each(order.OrderLines, function (i, line) {
            if (line.Guid == orderLineGuid) {
                orderLine = line;
                return false;       //exit loop
            }
        });
        if (orderLine != null) { return false; }

    });

    return orderLine;


};
model.RemoveSupplierOrderDataItem = function (supplierOrderGuid) {
    if (typeof supplierOrderGuid === 'undefined' || supplierOrderGuid == null || supplierOrderGuid.length != 36) { return null; };
    if (!model.SupplierOrdersExists) { return false; };

    $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, order) {
        if (order.SupplierOrderGuid == supplierOrderGuid) {
            model.DetailData.GrowerOrder.SupplierOrders.splice(i, 1);
            return false;  //exit loop
        }
    });
};
model.RemoveOrderLineDataItem = function (orderLineGuid) {
    if (typeof orderLineGuid === 'undefined' || orderLineGuid == null || orderLineGuid.length != 36) { return null; };
    if (model.DetailData == null || model.DetailData.GrowerOrder.SupplierOrders.length == 0) { return false; };
    if (model.SupplierOrderCount() <= 0) { return false; };
    var returnValue = false;
    $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, order) {
        $.each(order.OrderLines, function (i, line) {
            if (line.Guid == orderLineGuid) {
                order.OrderLines.splice(i, 1);
                returnValue = true;
                return false;  //exit loop
            }
        });
    });
    return returnValue;
};

//GrowerOrderStuff
model.GrowOrderExists = function () {
    if (typeof model.DetailData === 'undefined' || model.DetailData == null || typeof model.DetailData.GrowerOrder === 'undefined' || model.DetailData.GrowerOrder == null) { return false; };
    return true;
};



viewModel.state = {
    CustomerPoNo: function () { return $("#" + viewModel.Config.DetailDisplay.PurchaseOrderInputId).val(); },
    OrderDescription: function () { return $("#" + viewModel.Config.DetailDisplay.OrderDescriptionInput).val(); },
    PromotionalCode: function () { return $("#" + viewModel.Config.DetailDisplay.PromotionalCodeInput).val(); },
    PromotionalDescription: function () { return $("#" + viewModel.Config.DetailDisplay.PromotionalDescriptionID).text(); },
    //PromotionalDescription: function () { return "hi" },
    ShipToAddressGuid: function () { return $('input:radio[name=' + viewModel.Config.DetailDisplay.ShipToRadioButtonsName + ']:checked').val(); },
    PaymentTypeLookupCode: function () { return $('input:radio[name=' + viewModel.Config.DetailDisplay.PaymentMethodRadioButtonsName + ']:checked').val(); },

    CardOnFileGuid: function () { return $('input:radio[name=' + viewModel.Config.DetailDisplay.CreditCardRadioButtonsName + ']:checked').val(); },

    AgreeToTerms: function () { return $("#" + viewModel.Config.DetailDisplay.AgreeToTermsCheckboxId).is(':checked'); },
    OrderTransitionTypeLookupCode: "PlaceOrder",
    PaymentMethodInvoiceSelected: function () { return (viewModel.state.PaymentTypeLookupCode() == 'Invoice'); },
    PaymentMethodCreditCardSelected: function () { return (viewModel.state.PaymentTypeLookupCode() == 'Credit'); },
    SupplierOrders: function () {
        var orderGuids = [];
        $.each($("." + viewModel.Config.SupplierOrders.DisplaysClass), function (i, element) {
            var supplierOrderGuid = $(element).prop("id");
            orderGuids.push({
                "SupplierOrderGuid": supplierOrderGuid,
                "TagRatioCode": $(element).find("." + viewModel.Config.SupplierOrders.tagRatioSelectorClass).find('option:selected').prop("value"),
                "ShipMethodCode": $(element).find("." + viewModel.Config.SupplierOrders.shipMethodSelectorClass).find('option:selected').prop("value")
            });
        });
        return orderGuids;
    },

    orderQuantityBeingHandled: false,

    DetailIsDisplayed: function () {
        var count = $("#" + viewModel.Config.SummaryGrid.DetailRowId).length || 0;
        if (count > 0) {
            return true;
        } else {
            return false;
        };
    },
    displayedOrderGuid: function () { return $("#" + viewModel.Config.SummaryGrid.DetailContainerId).data("orderGuid") || ""; },

    DetailDisplaySummaryData: function () {
        var value = null;
        if (viewModel.state.DetailIsDisplayed) {
            $.each(model.SummaryData, function (i, order) {
                if (order.OrderGuid == viewModel.state.displayedOrderGuid()) {
                    value = order;
                    return false;
                }
            });
        };
        return value;
    },

    UpdatesPending: function () {
        var count = 0;
        
        
        $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, order) {
           
            if (typeof order.OrderLines !== 'undefined' && order.OrderLines != null) {

                for (j = 0; j < order.OrderLines.length; j++) {
                    // alert(traverseObj(order.OrderLines));                     
                    if (viewModel.ReturnOrderQuantityEntered(order.OrderLines[j].Guid) == 0) {
                        count += 1;
                    }
                }
            };
        });
        return count;
    },
    promoName: "",

};

viewModel.Init = function () {
  //  alert("init1");
 //   Masked Inputs
    $("#ship_to_phone_number").mask("(999) 999-9999");
    var sellerCode = localStorage.getItem("SellerCode");
   // if (sellerCode == null) {
        sellerCode = "GFB";
   // }
    if (sellerCode == "GFB")
    {
        $("#EPSTerms").show();
        $("#DMOTerms").hide();
    }
    else
    {
        $("#DMOTerms").show();
        $("#EPSTerms").hide();
    }
   // id="EPSTerms" name="EPSTerms"

    ///////////////// Store and Delete Form Templates //////////////////
    viewModel.Config.Forms.AddressFormTemplate = $("#" + viewModel.Config.Forms.AddressFormTemplateId).clone(true); //store the Address Form HTML Template
    $("#" + viewModel.Config.Forms.AddressFormTemplateId).remove();

    viewModel.Config.Forms.CardOnFileFormTemplate = $("#" + viewModel.Config.Forms.CardOnFileFormAddTemplateId).clone(true); //store the Address Form HTML Template
    $("#" + viewModel.Config.Forms.CardOnFileFormAddTemplateId).remove();

    var dynamicCostRowsTemplate = $("#" + viewModel.Config.SupplierOrders.LineItemGrid.dynamicCostRowsTemplateId);
    viewModel.Config.SupplierOrders.LineItemGrid.dynamicCostRowsTemplate$ = dynamicCostRowsTemplate.clone(true); //store the dynamic cost rows template
    dynamicCostRowsTemplate.remove();

    var growerOrderFeesRowTemplate = $("#" + viewModel.Config.DetailDisplay.orderTotalsGrid.growerOrderFeesRowTemplateId);
    viewModel.Config.DetailDisplay.orderTotalsGrid.growerOrderFeesRowTemplate$ = growerOrderFeesRowTemplate.clone(true); //store the dynamic row template
    growerOrderFeesRowTemplate.remove();
   
    //////////////// Action Buttons ///////////////////////
    $("#" + viewModel.Config.DetailDisplay.ButtonAddCardId).on("click", function () {
        if (model.state.isReadOnly == false) {
            viewModel.DisplayAddCardOnFileForm();
        }
        else {
            viewModel.floatMessage($("#" + viewModel.Config.DetailDisplay.ButtonAddCardId), "You are not currently set up as a user that can add cards.", 4000);
        }
    });
    $("#" + viewModel.Config.DetailDisplay.ButtonAddAddressId).on("click", function () {
        if (model.state.isReadOnly == false) {
            var orderGuid = viewModel.state.displayedOrderGuid();
            viewModel.DisplayAddShipToAddressForm();
        }
        else {
            viewModel.floatMessage($("#" + viewModel.Config.DetailDisplay.ButtonAddAddressId), "You are not currently set up as a user that can add addresses.", 4000);
        }
    });
    $("#" + viewModel.Config.DetailDisplay.ButtonOrderId).on("click", function () {
        if (model.state.isReadOnly == false) {
            var orderGuid = viewModel.state.displayedOrderGuid();
            //alert('Place Order: '+orderGuid);
            window.scrollTo(0, 0);

            var validation = viewModel.OrderValidation();
            if (validation.IsValid) {
                eps_tools.StandbyDialog({ 'title': '<span>We are processing your order:</span>', 'content': '<br /><span>This will take just a moment. Please don\'t leave this page, you are almost done.<br />&nbsp;</span>' });
                model.PlaceOrder(orderGuid);
            } else {
                eps_tools.OkDialog({
                    "title": "Alert:",
                    "content": validation.Message,
                    "modal": true
                });

            };
        }
        else
        {
            viewModel.floatMessage($("#" + viewModel.Config.DetailDisplay.ButtonOrderId), "You are not currently set up as a user that can place orders.", 4000);
        }
    });
    $("#" + viewModel.Config.DetailDisplay.ButtonContinueShoppingId).on("click", function () {
        var link = viewModel.continueShoppingLink();
     //   alert(link);
        window.location.href = link;
    });
    $("#" + viewModel.Config.DetailDisplay.ButtonDeleteOrderId).on("click", function () {
        if (model.state.isReadOnly == false)
        {
            var orderGuid = viewModel.state.displayedOrderGuid();
            window.scrollTo(0, 0);
            model.DeleteOrder(orderGuid);
        }
        else
        {
            viewModel.floatMessage($("#" + viewModel.Config.DetailDisplay.ButtonDeleteOrderId), "You are not currently set up as a user that can Delete orders.", 4000);
        }
    });
   
    /////////////////////////////////////////////////////////////////

 
    //Clone and Delete Supplier Order Template From Cart Detail Template
    viewModel.Config.SupplierOrdersClone = $("#" + viewModel.Config.SupplierOrders.TemplateId).clone();
  //  $(viewModel.Config.SupplierOrdersClone).find("." + viewModel.Config.SupplierOrders.LineItemGrid.ClassName + " tbody").empty();
    $("#" + viewModel.Config.SupplierOrders.ContainerId).empty();

    ///////////////// Initialize Summary view ////////////////////
  
    model.getSummaryData([viewModel.DisplaySummaryGrid]);
   
    //////////////////// Test Buttons ///////////////////////////////
    $("#test_show_order_data").button().on("click", function () {
        var target = $("#test_area_results_display")
        target.empty();
        var p = $("<p>Order Data:<br /></p>");
        p.append("<p>" + traverseObj(viewModel.CompileOrderData()) + "</p>");
        p.append("<hr /><p>" + JSON.stringify(viewModel.CompileOrderData()) + "</p>");
        target.html(p);
    });
    $("#test_show_detail_data").button().on("click", function () {


        //alert("bingo: test_show_detail_data");

        var target = $("#test_area_results_display")
        target.empty();
        var p = $("<p>Detail Data:<br /></p>");
        p.append("<p>" + traverseObj(model.DetailData) + "</p>");
        p.append("<hr /><p>" + JSON.stringify(model.DetailData) + "</p>");

        target.html(p);

    });
    $("#test_summary_api").button().on("click", function () {
        //alert('summary');
        model.getSummaryData([function () {
            var target = $("#test_area_results_display")
            target.empty();
            var p = $("<p>Summary Data:<br /></p>");
            p.append("<p>Count: " + model.SummaryData.length + "</p>");
            p.append("<span>" + traverseObj(model.SummaryData) + "</span>");

            target.html(p);


        }]);

    });
    $("#test_detail_api").button().on("click", function () {


        model.GetDetailData(viewModel.state.displayedOrderGuid(), [function () {
            //alert('detail: ' + viewModel.state.displayedOrderGuid());
            var target = $("#test_area_results_display")
            target.empty();
            var p = $("<p>Detail Data:<br /></p>");
            p.append("<p>OrderGuid: " + viewModel.state.displayedOrderGuid() + "</p>");
            p.append("<p>" + traverseObj(model.DetailData) + "</p>");
            p.append("<hr /><p>" + JSON.stringify(model.DetailData) + "</p>");

            target.html(p);
        }]);

    });
    $("#test_supplier_order_fees_api").button().on("click", function () {
        //alert("test_supplier_order_fees_api");

        var target = $("#test_area_results_display").empty();
        target.append("<p>Supplier Orders Count: " + model.DetailData.GrowerOrder.SupplierOrders.length + "</p>");

        $.each(model.DetailData.GrowerOrder.SupplierOrders, function (ndx, supplierOrder) {
            //alert("bingo");
            var url = "/api/Order/SupplierOrderFees?supplierOrderGuid=" + supplierOrder.SupplierOrderGuid;
            target.append("<p>Get: " + url + "</p><hr />");
            model.getSupplierOrderFees(supplierOrder.SupplierOrderGuid, [function () {
                target.append("<p>" + supplierOrder.SupplierOrderGuid + ":<br />" + traverseObj(model.returnSupplierOrderFees(supplierOrder.SupplierOrderGuid) || [], true) + "</p><hr />");
            }]);
        });
    });
    $("#test_grower_order_fees_api").button().on("click", function () {
        //alert("test_supplier_order_fees_api");
        var target = $("#test_area_results_display").empty();
        var url = "/api/Order/GrowerOrderFees?growerOrderGuid=" + viewModel.state.displayedOrderGuid();
        target.append("<p>Get: " + url + "</p><hr />");
        model.getGrowerOrderFees([function(){
            target.append("<p>Grower Order Fees:<br />" + traverseObj(model.returnGrowerOrderFees() || [], true) + "</p><hr />");
        }]);
    });
    $("#test_model_fee_data").button().on("click", function () {
        //alert("test_model_fee_data");
        var target = $("#test_area_results_display").empty();
        $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, supplierOrder) {
            var feeArray = supplierOrder.SupplierOrderFees || [];
            var p = $("<p />")
            .text("SupplierOrder [ " + supplierOrder.SupplierOrderGuid + " ] feeArray.length: " + feeArray.length);
            var div1 = $("<div />")
                .html(traverseObj(feeArray, true));
            target.append(p, div1, "<hr />");
        });
        
        var feeArray = model.DetailData.GrowerOrder.GrowerOrderFees || [];
        var p = $("<p />")
        .text("Grower order feeArray.length: " + feeArray.length);
        var div1 = $("<div />")
            .html(traverseObj(feeArray, true));
        target.append(p, div1, "<hr />");
    });
    $("#test_supplier_order_refresh_totals").button().on("click", function () {
        //alert("test_supplier_order_refresh_totals");
        $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, order) {
            viewModel.refreshSupplierOrderTotals(order.SupplierOrderGuid, [function () {
            }]);

        });
    });
    $("#test_grower_order_refresh_totals").button().on("click", function () {
        //alert("test_supplier_order_refresh_totals");
        viewModel.refreshGrowerOrderTotals()
    });
    /////////////////////////////////////////////////////////////////

    //State and Province Data
    model.getStateProvinceData();

    //Warn on page exit if updates are pending
    window.onbeforeunload = function (evt) {
        //var bob = true;
        var count = viewModel.state.UpdatesPending();
        if (count > 0) {
            return "You have " + count + " line(s) marked to remove but still pending update.";
        };
    }



};

viewModel.continueShoppingLink = function () {

    var link = "/Availability2/search"             //VA&Form=URC&Suppliers=PAC,HMA&species=ABU,ALT,ATR&ShipWeek=38|2014";
   // alert(traverseObj(model.DetailData.GrowerOrder));
    //add category value to link
    link += "?category=" + (model.DetailData.GrowerOrder.NavigationProgramTypeCode || "??");

    //add form value to link
    link += "&form=" + (model.DetailData.GrowerOrder.NavigationProductFormCategoryCode || "??");

    //compose suppliers and species list
    var suppliers = "";
    var species = "";
    $.each(model.DetailData.GrowerOrder.SupplierOrders, function (ndx, order) {
        if (ndx > 0) { suppliers += "," };
        suppliers += order.SupplierCode || "";

        $.each(order.OrderLines, function (i, orderLine) {
            var code = orderLine.SpeciesCode || "";
            var str = "," + species + ",";
            

            if (str.indexOf("," + code + ",") ==-1) {
                if (species.length > 0) { species += "," };
                species += code
            };
        });
    });
    //add suppliers to link
    link += "&suppliers=" + suppliers;
    //add species list to link
    link += "&species=" + species;

    //add shipweek value to link
    var shipWeekCode = model.DetailData.GrowerOrder.ShipWeekCode || "";
    if (shipWeekCode.length == 6) {
        var shipWeekString = shipWeekCode.substr(4);
        shipWeekString += "|" + shipWeekCode.substr(0, 4);
        link += "&ShipWeek=" + shipWeekString;
    };
    link += "&go=true";
  //  alert(link);
    return link;
};
viewModel.DisplayAddCardOnFileForm = function () {
    //clone credit card form template and set id
    viewModel.Config.Forms.CardOnFileForm = viewModel.Config.Forms.CardOnFileFormTemplate.clone();                    //Copy the template and 
    viewModel.Config.Forms.CardOnFileForm.prop({
        "id": viewModel.Config.Forms.CardOnFileFormId,                                                             //change id to working id
    });
    //alert(viewModel.Config.Forms.CardOnFileForm);
    //create and show address form
    eps_tools.FormSubmitDialog({
        "title": "Add Credit Card:",
        "content": viewModel.Config.Forms.CardOnFileForm,
        "modal": true,
    });

    $("#" + viewModel.Config.Forms.CardOnFileFormId + " input:first").select();

    $("#" + viewModel.Config.Forms.CardOnFileFormId + " input[type='radio'][name='credit_card_country']").off().on("change", function () {
        var country = $("#" + viewModel.Config.Forms.CardOnFileFormId + " input[type='radio'][name='credit_card_country']:checked").val() || "";
        var select = $("#credit_card_state_code");
        select.empty();
        var arr = [];
        var lbl = $("#" + viewModel.Config.Forms.CardOnFileFormId + ".state_province_selection_label");
        if (country == "ca") {
            arr = model.provinceData;
            lbl.text("Province");
        } else {
            arr = model.stateData;
            lbl.text("State");
        };
        $.each(arr, function (i, item) {
            var option = $("<option></option>");
            option.text(item.Abbr);
            option.prop("value", item.Abbr);
            select.append(option);
        });
    }).change();

    var messageBox = $("#" + viewModel.Config.Forms.CardOnFileFormId + " ." + viewModel.Config.Forms.CardOnFileFormMessageClass);
    // Set Add button event
    var sendButton = $("#" + viewModel.Config.Forms.CardOnFileSendButtonId);
    sendButton.off().on("click", function () {
        viewModel.CreateCardOnFile(messageBox);
    });
    /////////////////////////////////////////////////////////

    // Set Cancel button event
    $("#" + viewModel.Config.Forms.CardOnFileCancelButtonId).off().on("click", function () {
        eps_tools.Dialog.close();
    });
    /////////////////////////////////////////////////////////

    //Stop return key from closing form when pushed in text box
    var inputBoxes = $("#" + viewModel.Config.Forms.CardOnFileFormId + " ." + viewModel.Config.Forms.CardOnFileFormInputBoxesClass);
    //alert(inputBoxes.length);
    inputBoxes.off().on('keydown', function (evt) {
        evt.stopPropagation();
        //return false;              //false would stop browser from processing key
        messageBox.text('');
        return true;

    });

    //stop return key from closing form when pushed in textarea
    $("#ship_to_special_instructions").off().on('keydown', function (evt) {
        evt.stopPropagation();
        //return false;         //false would stop browser from processing key
        return true;
    });

};
viewModel.DisplayDeleteCardOnFileForm = function (cardGuid, location) {
    var cardInfo = model.ReturnCreditCardInfo(cardGuid);
    //alert("Delete: " + cardGuid);
    if (cardInfo != null) {
        var content = $('<span />').html("Card: " + cardInfo.CardNumber + "<br />" + cardInfo.CardDescription + "<br /><br />");
        eps_tools.OkCancelDialog({
            'title': '<span>Delete Card:</span>',
            'content': content,
            'modal': true,
            'onOk': function () {
                viewModel.DeleteCardOnFile(cardInfo.Guid)
            },
            'locate': location,
        });
    };
};
viewModel.ComposeCardOnFileDataFromForm = function () {
    var data = {};
    //sync the names to Api
    data.FirstName = $("#credit_card_first_name").val();
    data.LastName = $("#credit_card_last_name").val();
    data.CardNumber = $("#credit_card_number").val();
    data.ExpirationDate = $("#credit_card_expiration").val();
    data.Code = $("#credit_card_code").val();
    data.Description = $("#credit_card_description").val();
    data.StreetAddress1 = $("#credit_card_street_address_1").val();
    data.StreetAddress2 = $("#credit_card_street_address_2").val();
    data.City = $("#credit_card_city").val();
    data.StateCode = $("#credit_card_state_code").val();

    data.Country = $("#" + viewModel.Config.Forms.CardOnFileFormId + " input[type='radio'][name='credit_card_country']:checked").val() || "";
    data.ZipCode = $("#credit_card_zip_code").val();
    data.PhoneNumber = $("#credit_card_phone_number").val();
    //alert(traverseObj(data, false));
    return data;
};
viewModel.CreateCardOnFile = function (element) {
    //alert("CreateCardOnFile");
    var data = viewModel.ComposeCardOnFileDataFromForm();
    model.WriteNewCardOnFile(data, element);
};
viewModel.RefreshCardOnFileDisplay = function (cardGuid) {
    //set cardGuid as selected card
    var d = {};
    d.GrowerOrderGuid = viewModel.state.displayedOrderGuid();
    d.FieldName = "CardOnFileGuid";
    d.CardOnFileGuid = cardGuid;
    model.WriteOrderData(d, null, [function () {
        //refresh Card On File Data
        model.RefreshCardOnFileData(viewModel.state.displayedOrderGuid(), [function () {
            viewModel.FillCreditCardTable();
        }]);
    }]);
};
viewModel.DeleteCardOnFile = function (guid) {
    //alert("Delete: " + guid);
    model.DeleteCardOnFile(guid, function () {
        model.ReturnCreditCardInfo();
    });
    model.RefreshCardOnFileData(viewModel.state.displayedOrderGuid(), [function () {
        viewModel.FillCreditCardTable();
    }]);
};
viewModel.UpdateCardOnFileDescription = function (element) {

    var values = {}
    values.guid = element.data("guid");
    values.description = element.val();
    model.UpdateCardOnFileDescription(values, function () {
        element.focus();
    });

};
viewModel.DisplayAddShipToAddressForm = function () {
    //clone address form template and set id
    viewModel.Config.Forms.AddressForm = viewModel.Config.Forms.AddressFormTemplate.clone();                    //Copy the template and 
    viewModel.Config.Forms.AddressForm.prop({
        "id": viewModel.Config.Forms.AddressFormId,                                                             //change id to working id
    });

    //create and show address form
    eps_tools.FormSubmitDialog({
        "title": "Ship-To Address:",
        "content": viewModel.Config.Forms.AddressForm,
        "modal": true,
    });
    //remove delete button
    $("#" + viewModel.Config.Forms.AddressDeleteButtonId).remove();

    //focus on first field
    $("#" + viewModel.Config.Forms.AddressFormId + " input:first").select();

    //set country change event
    $("#" + viewModel.Config.Forms.AddressFormId + " input[type='radio'][name='" + viewModel.Config.Forms.ShipToCountryCodeName + "']").off().on("change", function () {
        var country = $("#" + viewModel.Config.Forms.AddressFormId + " input[type='radio'][name='ship_to_country']:checked").val() || "";
        //alert(country);

        var select = $("#ship_to_state_code");
        select.empty();
        var arr = [];
        var lbl = $("#" + viewModel.Config.Forms.AddressFormId + ".state_province_selection_label");
        if (country == "ca") {
            arr = model.provinceData;
            lbl.text("Province");
        } else {
            arr = model.stateData;
            lbl.text("State");
        };
        $.each(arr, function (i, item) {
            var option = $("<option></option>");
            option.text(item.Abbr);
            option.prop("value", item.Abbr);
            select.append(option);
        });
    }).change();

    //get reference to message box
    var messageBox = $("#" + viewModel.Config.Forms.AddressFormId + " ." + viewModel.Config.Forms.AddressFormMessageClass);

    // Set send button event
    var sendButton = $("#" + viewModel.Config.Forms.AddressSaveButtonId);
    sendButton.off().on("click", function () {
        viewModel.CreateAddress(messageBox);
    });

    // Set Cancel button event
    $("#" + viewModel.Config.Forms.AddressCancelButtonId).off().on("click", function () {
        eps_tools.Dialog.close();
    });

    //Stop return key from closing form when pushed in text box
    var inputBoxes = $("#" + viewModel.Config.Forms.AddressFormId + " ." + viewModel.Config.Forms.AddressFormInputBoxesClass);

    //clear and reset input box keydown event
    inputBoxes.off().on('keydown', function (evt) {
        evt.stopPropagation();
        messageBox.text('');
        return true;
    });

    //stop return key from closing form when pushed in textarea
    $("#ship_to_special_instructions").off().on('keydown', function (evt) {
        evt.stopPropagation();
        //return false;         //false would stop browser from processing key
        return true;
    });

};
viewModel.DisplayEditShipToAddressForm = function (addressGuid) {
    //alert(addressGuid);

    //get current values
    var addressData = $.grep(model.DetailData.GrowerOrder.ShipToAddresses, function (n, i) {
        return n.Guid == addressGuid;
    });
    if (typeof addressData === 'undefined' || addressData == null || addressData <= 0) { return false; };
    addressData = addressData[0];

    //clone address form template and set id
    viewModel.Config.Forms.AddressForm = viewModel.Config.Forms.AddressFormTemplate.clone();                    //Copy the template and 
    viewModel.Config.Forms.AddressForm.prop({
        "id": viewModel.Config.Forms.AddressFormId,                                                             //change id to working id
    });
    //create and show address form
    eps_tools.FormSubmitDialog({
        "title": "Ship-To Address:",
        "content": viewModel.Config.Forms.AddressForm,
        "modal": true,
    });

    //set country change event and fire it
    $("#" + viewModel.Config.Forms.AddressFormId + " input[type='radio'][name='" + viewModel.Config.Forms.ShipToCountryCodeName + "']").off().on("change", function () {
        var country = $("#" + viewModel.Config.Forms.AddressFormId + " input[type='radio'][name='ship_to_country']:checked").val() || "";
        //alert(country);

        var select = $("#ship_to_state_code");
        select.empty();
        var arr = [];
        var lbl = $("#" + viewModel.Config.Forms.AddressFormId + ".state_province_selection_label");
        if (country == "ca") {
            arr = model.provinceData;
            lbl.text("Province");
        } else {
            arr = model.stateData;
            lbl.text("State");
        };
        $.each(arr, function (i, item) {
            var option = $("<option></option>");
            option.text(item.Abbr);
            option.prop("value", item.Abbr);
            select.append(option);
        });
    }).change();


    //load currentData
    var countryCode = "usa";
    if (addressData.Country == 'Canada') { countryCode = "ca"; };
    $("#" + viewModel.Config.Forms.AddressFormId + " input[type='radio'][name='" + viewModel.Config.Forms.ShipToCountryCodeName + "']").value = countryCode;
    $("#" + viewModel.Config.Forms.ShipToStateCode).val(addressData.StateCode || '');
    $.each($("#" + viewModel.Config.Forms.AddressFormId +" .inputTextBox"), function (ndx, textBox) {
        if (textBox.id == viewModel.Config.Forms.ShipToNameId) {
            textBox.value = addressData.Name || '';
        } else if (textBox.id == viewModel.Config.Forms.ShipToStreetAddress1) {
            textBox.value = addressData.StreetAddress1 || '';
        } else if (textBox.id == viewModel.Config.Forms.ShipToStreetAddress2) {
            textBox.value = addressData.StreetAddress2 || '';
        } else if (textBox.id == viewModel.Config.Forms.ShipToCity) {
            textBox.value = addressData.City || '';
        } else if (textBox.id == viewModel.Config.Forms.ShipToZipCode) {
            textBox.value = addressData.ZipCode || '';
        } else if (textBox.id == viewModel.Config.Forms.ShipToPhoneNumber) {
            textBox.value = addressData.PhoneNumber || '';
        } else {
            //alert(textBox.id);
        };
    });
    $("#" + viewModel.Config.Forms.ShipToSpecialInstructions).val(addressData.SpecialInstructions || '');

    //ToDo: Debug this and get it to work
    $("#ship_to_phone_number").mask("(999) 999-9999");


    //get reference to message box
    var messageBox = $("#" + viewModel.Config.Forms.AddressFormId + " ." + viewModel.Config.Forms.AddressFormMessageClass);

    // Set send button event
    var sendButton = $("#" + viewModel.Config.Forms.AddressSaveButtonId);
    sendButton.off().on("click", function () {
        viewModel.UpdateAddress(addressGuid, messageBox);
    });

    // Set delete button event
    if (addressData.IsDefault) {
        $("#" + viewModel.Config.Forms.AddressDeleteButtonId).off().hide();
    } else {
        $("#" + viewModel.Config.Forms.AddressDeleteButtonId).off().on("click", function () {
            viewModel.DeleteAddress(addressGuid, messageBox);
        });
    };

    // Set Cancel button event
    $("#" + viewModel.Config.Forms.AddressCancelButtonId).off().on("click", function () {
        eps_tools.Dialog.close();
    });

    //Stop return key from closing form when pushed in text box
    var inputBoxes = $("#" + viewModel.Config.Forms.AddressFormId + " ." + viewModel.Config.Forms.AddressFormInputBoxesClass);

    //clear and reset input box keydown event
    inputBoxes.off().on('keydown', function (evt) {
        evt.stopPropagation();
        messageBox.text('');
        return true;
    });

    //stop return key from closing form when pushed in textarea
    $("#ship_to_special_instructions").off().on('keydown', function (evt) {
        evt.stopPropagation();
        //return false;         //false would stop browser from processing key
        return true;
    });

    //focus on first field
    $("#" + viewModel.Config.Forms.AddressFormId + " input:first").select();

};
viewModel.ComposeShipToAddressDataFromForm = function (addressGuid) {
    var data = {};

    if (typeof addressGuid !== 'undefined' && addressGuid != null && addressGuid.length == 36) {
        data.GrowerShipToAddressGuid = addressGuid;
    };
    data.Name = $("#ship_to_name").val();
    data.StreetAddress1 = $("#ship_to_street_address_1").val();
    data.StreetAddress2 = $("#ship_to_street_address_2").val();
    data.City = $("#ship_to_city").val();
    data.StateCode = $("#ship_to_state_code").val();
    data.Country = $("#" + viewModel.Config.Forms.AddressFormId + " input[type='radio'][name='ship_to_country']:checked").val() || "";
    data.ZipCode = $("#ship_to_zip_code").val();
    data.PhoneNumber = $("#ship_to_phone_number").val();
    data.SpecialInstructions = $("#ship_to_special_instructions").val();
    //alert(traverseObj(data, false));
    return data;
};
viewModel.DeleteAddress = function (addressGuid, element) {
    //alert('SubmitDeleteAddress');
    model.WriteShipToAddressDelete(addressGuid, element);
};
viewModel.UpdateAddress = function (addressGuid, element) {
    //alert('SubmitUpdateAddressForm');
    var data = viewModel.ComposeShipToAddressDataFromForm(addressGuid);
    model.WriteShipToAddressUpdate(data, element);
};
viewModel.CreateAddress = function (element) {
    //alert('SubmitNewAddressForm');
    var data = viewModel.ComposeShipToAddressDataFromForm();
    model.WriteShipToAddressNew(data, element);
};
viewModel.RefreshShipToDisplay = function (growerAddressGuid, addressGuid) {
    //set new address as selected
    var d = {};
    d.GrowerOrderGuid = viewModel.state.displayedOrderGuid();
    d.FieldName = "ShipToAddressGuid";
    d.ShipToAddressGuid = growerAddressGuid;
    model.WriteOrderData(d, null, [function () {
        //refresh ShipTo Address Data
        var growerOrderGuid = viewModel.state.displayedOrderGuid();
        model.UpdateShipToAddressData(growerOrderGuid, [function () {
            //redisplay addresseses
            var tableDiv = $("#" + viewModel.Config.DetailDisplay.ShipToTableId);
            viewModel.FillAddressTable(tableDiv);
            //Define events
            viewModel.defineShipToAddressRadioEvents();
        }]);
    }]);
};
viewModel.DisplaySummaryGrid = function (callbacks) {
    var target = $("#" + viewModel.Config.OpenOrdersDisplayId);
    target.empty();
    var grid = $("<DIV></DIV>");
    var gridProperties = { "id": viewModel.Config.SummaryGrid.Id};
    grid.prop(gridProperties);

    $.each(model.SummaryData, function (i, item) {
      
        var row = $("<ul>");
        var rowProperties = { "id": item.OrderGuid, "class": viewModel.Config.SummaryGrid.RowClass };
        row.prop(rowProperties);

        for (var i = 0; i < viewModel.Config.SummaryGrid.Columns; i++) {
            var li = $("<li>")
            switch (i)
            {
                case 0:
                    li = $("<img>")
                    var liProperties = { "id": "IMG_" + item.OrderGuid, "src": "/Images/Icons/mag_glass_plus.png" };
                    li.prop(liProperties);
                    break;
                case 1:
                    var liProperties = { "id": "order_week" };
                    li.prop(liProperties);
                    li.append(item.ShipWeekString);
                    break;
                case 2:
                    li.append("CATEGORY: " + item.ProgramCategoryName);
                    break;
                case 3:
                    li.append("FORM: "+ item.ProductFormName);
                    break;
                case 4:
                    li.append("QUANTITY: " + numeral(item.OrderQty).format('0,0'));
                    break;
                default:
            }
            row.append(li);
        };
        grid.append(row);

    });
    target.append(grid);
    viewModel.defineSummaryRowEvents();
    viewModel.ProcessEventChain(callbacks);
};
viewModel.defineSummaryRowEvents = function () {

    $("ul." + viewModel.Config.SummaryGrid.RowClass).on("click", function () {
      
        var orderGuid = $(this).prop("id");
       // alert(orderGuid);
        var currentDetailContainer = $("#" + viewModel.Config.SummaryGrid.DetailContainerId)
        var currentlyDisplayedOrderGuid = currentDetailContainer.data("orderGuid") || "";
        var displayRow = $("#" + viewModel.Config.SummaryGrid.DetailRowId)

        if (currentlyDisplayedOrderGuid != "") {
            var liCurrProperties = { "id": "IMG_" + currentlyDisplayedOrderGuid,"src" : "/Images/icons/mag_glass_plus.png" };      
            $("#" + "IMG_" + currentlyDisplayedOrderGuid).prop(liCurrProperties);
        }

        viewModel.RemoveDetailDisplay();

        if (orderGuid != currentlyDisplayedOrderGuid) {
            //displayRow.hide("blind").delay(1000).remove();
           // var liCurrProperties = { "id": "IMG_" + currentlyDisplayedOrderGuid, "src": "/Images/icons/Loading_Availability.gif" };
          //  $("#" + "IMG_" + currentlyDisplayedOrderGuid).prop(liCurrProperties);

            //insert row in grid spanning all columns
            var row = $("<ul>");
            var rowProperties = { "id": viewModel.Config.SummaryGrid.DetailRowId };
            row.prop(rowProperties);

            var td = $("<li>")

            var div = $("<div />");
            var divProperties = { "id": viewModel.Config.SummaryGrid.DetailContainerId };
            div.data("orderGuid", orderGuid);
            div.prop(divProperties);

            td.append(div);
            row.append(td);

            row.insertAfter("#" + orderGuid);

            div.show("blind");
            //show detail data in this area
            model.GetDetailData(orderGuid, [viewModel.DisplayDetail]);
        };

    });

};

viewModel.DisplayDetail = function () {
    var target = $("#" + viewModel.Config.SummaryGrid.DetailContainerId);
    var detailDisplay = $("#" + viewModel.Config.DetailDisplay.Id);
    target.append(detailDisplay);
    detailDisplay.fadeIn("slow");
    viewModel.LoadDetailDisplay();
};

viewModel.LoadDetailDisplay = function () {

    //load input fields
    $("#" + viewModel.Config.DetailDisplay.PurchaseOrderInputId).val(model.DetailData.GrowerOrder.CustomerPoNo);
    $("#" + viewModel.Config.DetailDisplay.OrderDescriptionInput).val(model.DetailData.GrowerOrder.OrderDescription);
    $("#" + viewModel.Config.DetailDisplay.PromotionalCodeInput).val(model.DetailData.GrowerOrder.PromotionalCode);
    if (model.state.isReadOnly == true) {
        // alert("hi 1" + model.state.isReadOnly);
        $("#" + viewModel.Config.DetailDisplay.PurchaseOrderInputId).prop({ "disabled": "disabled", })
        $("#" + viewModel.Config.DetailDisplay.OrderDescriptionInput).prop({ "disabled": "disabled", })
        $("#" + viewModel.Config.DetailDisplay.PromotionalCodeInput).prop({ "disabled": "disabled", })
    }

    //fill Address Table
    var tableDiv = $("#" + viewModel.Config.DetailDisplay.ShipToTableId);
    viewModel.FillAddressTable(tableDiv);

    //Set default Payment Selection - ToDo: rewrite this to create radio buttons from data rather than assume they map
    var paymentMethodValue = model.state.paymentMethodSelected();

    var radioButton = $("input:radio[name='" + viewModel.Config.DetailDisplay.PaymentMethodRadioButtonsName + "'][value='" + paymentMethodValue + "']");
    radioButton.prop('checked', 'checked');
    
    if (model.state.isReadOnly == true) {
        radioButton2 = $("input:radio[name='" + viewModel.Config.DetailDisplay.PaymentMethodRadioButtonsName + "'][value='Invoice']")
        radioButton3 = $("input:radio[name='" + viewModel.Config.DetailDisplay.PaymentMethodRadioButtonsName + "'][value='Credit']")
        radioButton2.prop({ "disabled": "disabled", })
        radioButton3.prop({ "disabled": "disabled", })
        $("#" + viewModel.Config.DetailDisplay.AgreeToTermsCheckboxId).prop({ "disabled": "disabled", }) 
    }
    //fill Credit Card Table
    viewModel.FillCreditCardTable();

    //Clear Supplier Orders from Detail Display
    $("#" + viewModel.Config.SupplierOrders.ContainerId).empty();

    //Add Supplier Orders
    $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, order) {
        var clone = viewModel.Config.SupplierOrdersClone.clone(true);
        clone.prop({
            "id": order.SupplierOrderGuid
        });
        clone.find("." + viewModel.Config.SupplierOrders.SupplierNameSpanClass).text(order.SupplierName);

        ///// Tag Ratio Dropdown /////
        var tagRatioSelector = $(clone).find("." + viewModel.Config.SupplierOrders.tagRatioSelectorClass);
        tagRatioSelector.empty();
        var selectionValues = { "First": "", "Default": "", "Selected": "" };
        $.each(order.TagRatioList, function (i, tag) {
            //capture the code of the tag to be selected
            if (i == 0) { selectionValues.First = tag.Code; };
            if (tag.IsSelected) { selectionValues.Selected = tag.Code; };
            if (tag.IsDefault) { selectionValues.Default = tag.Code; };
            var option = $('<option />').prop({ "value": tag.Code });
            option.text(tag.DisplayName);
            tagRatioSelector.append(option);
        });
        //set selected attribute of selectedOption
        var selectedCode = "";
        if (selectionValues.Selected != null && selectionValues.Selected != "") {
            selectedCode = selectionValues.Selected;
        } else {
            if (selectionValues.Default != null && selectionValues.Default != "") {
                selectedCode = selectionValues.Default;
            } else {
                if (selectionValues.First != null && selectionValues.First != "") {
                    selectedCode = selectionValues.First;
                };
            };
        };
        if (selectedCode != "") {
            tagRatioSelector.children("option[value='" + selectedCode + "']").prop({ "selected": "selected" });
            //display selected tag ratio
            var tagRatioDisplayValue = tagRatioSelector.children("option[value='" + tagRatioSelector.val() + "']").text();
            tagRatioSelector.siblings("." + viewModel.Config.SupplierOrders.tagRatioSelectedDisplayClass).text(tagRatioDisplayValue);
        };
        if (model.state.isReadOnly == true) { tagRatioSelector.prop("disabled", "disabled"); }

        ///// Ship Method Dropdown /////
        var shipMethodSelector = $(clone).find("." + viewModel.Config.SupplierOrders.shipMethodSelectorClass);
        shipMethodSelector.empty();
        var selectionValues = {"First":"", "Default":"","Selected":""};
        $.each(order.ShipMethodList, function (i, method) {
            //capture selectionValues
            if (i == 0) { selectionValues.First = method.Code; };
            if (method.IsSelected) { selectionValues.Selected = method.Code; };
            if (method.IsDefault) { selectionValues.Default = method.Code; };
            var option = $('<option />').prop({ "value": method.Code });
            option.text(method.DisplayName);
            shipMethodSelector.append(option);
        });

        var selectedCode = "";
        if (selectionValues.Selected != null && selectionValues.Selected != "") {
            selectedCode = selectionValues.Selected;
        } else {
            if (selectionValues.Default != null && selectionValues.Default != "") {
                selectedCode = selectionValues.Default;
            } else {
                if (selectionValues.First != null && selectionValues.First != "") {
                    selectedCode = selectionValues.First;
                };
            };
        };
              
        if (selectedCode != "") {
            //set selected attribute of selectedOption
            shipMethodSelector.children("option[value='" + selectedCode + "']").prop({ "selected": "selected" });

            //show selected ship method
            var shipMethodDisplayValue = shipMethodSelector.children("option[value='" + shipMethodSelector.val() + "']").text();
            shipMethodSelector.siblings("." + viewModel.Config.SupplierOrders.shipMethodSelectedDisplayClass).text(shipMethodDisplayValue);
        };
        if (model.state.isReadOnly == true) { shipMethodSelector.prop("disabled", "disabled"); }

        ///// Line Item Grid: Body Section /////
        var theDiv = $("<div />");
      //  tbody = $(viewModel.Config.SupplierOrdersClone).find("." + viewModel.Config.SupplierOrders.LineItemGrid.ClassName + " tbody");
        $.each(order.OrderLines, function (ndx, line) {
            var row = $("<ul>");
         //   alert(traverseObj(line));

            var rowProperties = { "id": line.Guid, "class": viewModel.Config.SupplierOrders.LineItemGrid.RowClass };
            row.data({ "SupplierOrderGuid": order.SupplierOrderGuid });  //used by toggle remove row function
            row.prop(rowProperties);
           // alert("hi");
            for (var i = 0; i < viewModel.Config.SupplierOrders.LineItemGrid.Columns; i++) {
                var td = $("<li>")
                var columnProperties = { "class": "td_" + i };
                //if (viewModel.Config.SupplierOrders.LineItemGrid.ColumnWidths[i] != "") {
                //    td.css("width", viewModel.Config.SupplierOrders.LineItemGrid.ColumnWidths[i]);
                //  };
                td.prop(columnProperties);

                switch (i) {
                    case 0:
                        var lblSpan = $('<span class="order_label" />')
                          .text("Species:");
                        
                        var span = $('<span />')
                         .prop({
                             "id": "SPECIES_" + line.Guid
                         })
                        .text(line.SpeciesName);
                        td.html(lblSpan);
                        td.append(span);

                        break;
                    case 1:
                        var lblSpan = $('<span class="order_label" />')
                        .text("Variety:");

                        var span = $('<span />')
                            .prop({
                                "id": "VAR_" + line.Guid
                            })
                            .text(line.ProductDescription);
                        td.html(lblSpan);
                        td.append(span);
                        break;
                    case 2:
                        var lblSpan = $('<span class="order_label" />')
                            .text("Form:");

                        var span = $('<span />')
                            .prop({
                                "id": "FROM_" + line.Guid
                            })
                            .text(line.ProductFormName);

                        td.html(lblSpan);
                        td.append(span);
                        break;
                    case 3:
                        var lblSpan = $('<span class="order_label" />')
                           .text("Trays:");

                        var span = $('<span />')
                            .prop({
                                                      
                                "id": "TRAY_" + line.Guid
                            })
                           .text((line.QuantityOrdered / line.Multiple) || -1);

                        td.html(lblSpan);
                        td.append(span);

                        break;
                    case 4:
                        var lblSpan = $('<span class="order_label" />')
                           .text("Qty:");

                        var orderInput = $("<input type=\"number\" />");
                    //    alert(line.QuantityOrdered);
                        //alert(numeric(line.QuantityOrdered));
                        if (model.state.isReadOnly == false) {
                            orderInput.prop({
                                "value": line.QuantityOrdered,
                                "class": viewModel.Config.SupplierOrders.LineItemGrid.OrderQuantityInputClass,
                                "id": "INPUT_" + line.Guid,
                                "maxlength": "6",

                            });
                        }
                        else
                        {
                            orderInput.prop({
                                "value": line.QuantityOrdered,
                                "class": viewModel.Config.SupplierOrders.LineItemGrid.OrderQuantityInputClass,
                                "id": "INPUT_" + line.Guid,
                                "maxlength": "6",
                                "disabled" : "disabled",
                                });
                        }

                        var img = $('<img />');
                        img.prop({
                            "src": viewModel.Config.Icons.ItemCheckedSrc,
                            //  "class": viewModel.Config.SupplierOrders.LineItemGrid.OrderQuantityImgClass,
                            "id" : "IMG_" + line.Guid,
                            "alt": ""
                        });

                        td.html(lblSpan);
                   //     td.append(span);

                        td.append(orderInput, img);

                        break;
                    case 5:
                        var lblSpan = $('<span class="order_label" />')
                            .text("Price:");

                        var thePrice = "";
                        if (numeral(line.Price) > 100)
                        { 
                            thePrice = numeral(line.Price).format('0,0.00'); 
                        }
                        else
                        {
                            thePrice = numeral(line.Price).format('0,0.0000'); 
                        }                
            
            
                        var span = $('<span />')
                            .prop({
                                "id": "PRICE_" + line.Guid
                            })
                            .text(thePrice);

                        td.html(lblSpan);
                        td.append(span);

                        break;
                    case 6:
                        var lblSpan = $('<span class="order_label" />')
                            .text("Ext:");

                        var span = $('<span />')
                            .prop({
                                "class": "extension_price_span",
                                "id": "EXTPRICE_" + line.Guid
                            })
                            .text(numeral(line.Price * line.QuantityOrdered).format('0,0.00'));

                        td.html(lblSpan);
                        td.append(span);
                        break;
                   
                       

                    default:
                        td.html("<span>???</span>");
                }
                row.append(td);
            };
            theDiv.append(row);
        });
        $(clone).children("." + viewModel.Config.SupplierOrders.LineItemGrid.ClassName).children("div").replaceWith(theDiv);  //line_item_grids
        
        $("#" + viewModel.Config.SupplierOrders.ContainerId).append(clone);

        model.getSupplierOrderFees(order.SupplierOrderGuid, [function () {
            viewModel.refreshSupplierOrderTotals(order.SupplierOrderGuid, [function () {
                model.getGrowerOrderFees([function () {
                   viewModel.refreshGrowerOrderTotals();
                 }]);
            }]);
        }]);

    }); //Add Supplier Orders

 //   viewModel.DefineRemoveButtonEvents();
    viewModel.definePurchaseOrderInputBoxEvents();
    viewModel.defineOrderDescriptionInputBoxEvents();
    viewModel.definePromotionalCodeInputBoxEvents();
    viewModel.defineShipToAddressRadioEvents();
    viewModel.defineCardOnFileRadioEvents();
    viewModel.definePaymentTypeRadioEvents();
    viewModel.defineAgreeToTermsCheckboxEvent();
    viewModel.DefinePickListEvents();
    viewModel.defineInputBoxEvents();
 //   viewModel.defineUpdateLinkEvents();


    var liCurrProperties = { "id": "IMG_" + model.state.currentGrowerOrderGuid(), "src": "/Images/icons/mag_glass_minus.png" };
    $("#" + "IMG_" + model.state.currentGrowerOrderGuid()).prop(liCurrProperties);

    var theCode = viewModel.state.PromotionalCode();

    model.getPromoName(theCode, [function () {
        //this checks to make sure reused single use codes are removed
        var element = $('#' + viewModel.Config.DetailDisplay.PromotionalCodeInput);
        if (model.state.promoName == "This Promo Code has already been used") {
            element.val("");
            var theCode = viewModel.CompileData_PromotionalCode();
            model.WriteOrderData(theCode, element, [function () {
              //  alert("promo Code refresh fees");
                model.getGrowerOrderFees([function () {
                    viewModel.refreshGrowerOrderTotals();
                }]);
            }]);
        }
    }]);
 
       
}
viewModel.RemoveDetailDisplay = function (callbacks) {
    var displayRow = $("#" + viewModel.Config.SummaryGrid.DetailRowId)
    if (displayRow != null && displayRow.length > 0) {
        var detailDisplay = $("#" + viewModel.Config.DetailDisplay.Id);
        detailDisplay.css("display", "none");
        $("body").append(detailDisplay);
        displayRow.remove();
        model.DetailData = {};
    };
    viewModel.ProcessEventChain(callbacks);
};

viewModel.OrderValidation = function () {
    var validation = {
        IsValid: true,  //presume success, any test can set to false
        Message:""
    };

    var orderData = viewModel.CompileOrderData();

    //Code to Validate Payment Method
    if (viewModel.state.PaymentMethodInvoiceSelected()) {
        //ToDo: Validate Credit Line
    } else if (viewModel.state.PaymentMethodCreditCardSelected()) {
        var cardsOnFile = model.DetailData.GrowerOrder.CreditCards.length || 0;
        if (cardsOnFile <= 0) {
            validation.IsValid = false;
            validation.Message += "<p>Payment Method = Credit Card requires a card on file.</p>";

        };
    }

    //Code to Validate Acceptance of Terms
    if (orderData.AgreeToTerms != true) {
        validation.IsValid = false;
        validation.Message += "<p>Please Agree to Terms (see Payment Information section).</p>";
    };

    //Code to Validate no updates pending
    var count = viewModel.state.UpdatesPending();
    if (count > 0) {
        validation.IsValid = false;
        validation.Message += "<p>You have pending updates.  Uncheck rows marked to remove or click update link to remove those rows from order.</p>";
    };


    if (!validation.IsValid) {
        validation.Message = "<p>Order needs attention:</p>" + validation.Message;
        validation.Message += "<p>Edit and resubmit your order....</p>";
    };
    return validation;

};
///////////// Data Update Code ////////////////////////

viewModel.CompileOrderData = function () {
    var d = {};
    d.GrowerOrderGuid = viewModel.state.displayedOrderGuid();
    //d.CustomerPoNo = viewModel.state.CustomerPoNo();
    //d.OrderDescription = viewModel.state.OrderDescription();
    //d.ShipToAddressGuid = viewModel.state.ShipToAddressGuid();
    //d.PaymentTypeLookupCode = viewModel.state.PaymentTypeLookupCode();
    //d.CardOnFileGuid = viewModel.state.CardOnFileGuid();
    d.AgreeToTerms = viewModel.state.AgreeToTerms();
    d.OrderTransitionTypeLookupCode = viewModel.state.OrderTransitionTypeLookupCode;
    //d.SupplierOrders = viewModel.state.SupplierOrders();
    return d;
};

viewModel.CompileData_PurchaseOrderNumber = function () {
    var d = {};
    d.GrowerOrderGuid = viewModel.state.displayedOrderGuid();
    d.FieldName = "CustomerPoNo";
    d.CustomerPoNo = viewModel.state.CustomerPoNo();
    return d;
};
viewModel.CompileData_OrderDescription = function () {
    var d = {};
    d.GrowerOrderGuid = viewModel.state.displayedOrderGuid();
    d.FieldName = "OrderDescription";
    d.OrderDescription = viewModel.state.OrderDescription();
    return d;
};
viewModel.CompileData_PromotionalCode = function () {
    var d = {};
    d.GrowerOrderGuid = viewModel.state.displayedOrderGuid();
    d.FieldName = "PromotionalCode";
    d.PromotionalCode = viewModel.state.PromotionalCode();
    return d;
};
viewModel.CompileData_ShipToAddress = function () {
    var d = {};
    d.GrowerOrderGuid = viewModel.state.displayedOrderGuid();
    d.FieldName = "ShipToAddressGuid";
    d.ShipToAddressGuid = viewModel.state.ShipToAddressGuid();
    return d;
};
viewModel.CompileData_CardOnFile = function () {
    var d = {};
    d.GrowerOrderGuid = viewModel.state.displayedOrderGuid();
    d.FieldName = "CardOnFileGuid";
    d.CardOnFileGuid = viewModel.state.CardOnFileGuid();
    return d;
};
viewModel.CompileData_PaymentType = function () {
    var d = {};
    d.GrowerOrderGuid = viewModel.state.displayedOrderGuid();
    d.FieldName = "PaymentTypeLookupCode";
    d.PaymentTypeLookupCode = viewModel.state.PaymentTypeLookupCode();
    return d;
};
viewModel.CompileData_AgreeToTerms = function () {
    var d = {};
    d.GrowerOrderGuid = viewModel.state.displayedOrderGuid();
    d.FieldName = "AgreeToTerms";
    d.AgreeToTerms = viewModel.state.AgreeToTerms();
    return d;
};
viewModel.CompileData_TagRatioSelected = function (supplierOrderGuid, tagRatioLookupCode) {
    //Different from others
    //first: this is supplier order not grower order
    //second: values passed in rather than retrieved by model.state functions
    var d = {};
    d.SupplierOrderGuid = supplierOrderGuid;
    d.FieldName = "TagRatioLookupCode";
    d.TagRatioLookupCode = tagRatioLookupCode;
    return d;
};
viewModel.CompileData_ShipMethodSelected = function (supplierOrderGuid, shipMethodLookupCode) {
    //Different from others
    //first: this is supplier order not grower order
    //second: values passed in rather than retrieved by model.state functions
    var d = {};
    d.SupplierOrderGuid = supplierOrderGuid;
    d.FieldName = "ShipMethodLookupCode";
    d.ShipMethodLookupCode = shipMethodLookupCode;
    return d;
};
viewModel.definePurchaseOrderInputBoxEvents = function () {
 //Important to first turn all events off because Chrome accumulates them between detail page refreshes.
    $('#' + viewModel.Config.DetailDisplay.PurchaseOrderInputId)
    .off()      //first remove all events
    .on("change", function (e) {
        //alert('change');
        model.WriteOrderData(viewModel.CompileData_PurchaseOrderNumber(), $(this));
    })
    .on("focus", function (e) {
        $(this).select();
    })
    .on("blur", function (e) {

    });
}
viewModel.defineOrderDescriptionInputBoxEvents = function () {
    $('#' + viewModel.Config.DetailDisplay.OrderDescriptionInput)
    .off()      //first remove all events
    .on("change", function (e) {
        //alert('change');
        model.WriteOrderData(viewModel.CompileData_OrderDescription(), $(this));
    })
    .on("focus", function (e) {
        $(this).select();
    })
    .on("blur", function (e) {

    });
}
viewModel.definePromotionalCodeInputBoxEvents = function () {
    var element = $('#' + viewModel.Config.DetailDisplay.PromotionalCodeInput);
    element
    .off()     
    .on("change", function (e) {
        var theCode = viewModel.CompileData_PromotionalCode();
       // alert("in change before " + model.state.promoName + " code = " + theCode.PromotionalCode);
        viewModel.DisplayPromoName(theCode.PromotionalCode,[function ()  {
         //  alert("in change after " + model.state.promoName);
            if (model.state.promoName != "This Promo Code has already been used") {
                model.WriteOrderData(theCode, $(this), [function () {
                    model.getGrowerOrderFees([function () {
                        viewModel.refreshGrowerOrderTotals();
                    }]);
                }]);
            }
        }])
    })
    .on("focus", function (e) {
        $(this).select();
    })
    .on("blur", function (e) {

    });

}
viewModel.defineShipToAddressRadioEvents = function () {
    
    $("input[name='" + viewModel.Config.DetailDisplay.ShipToRadioButtonsName + "']")
    .off()      //first remove all events
    .on("change", function (e) {
       
        model.WriteOrderData(viewModel.CompileData_ShipToAddress(), $(this));
    })
    .on("blur", function (e) {
    });

}
viewModel.definePaymentTypeRadioEvents = function () {
    $("input[name='" + viewModel.Config.DetailDisplay.PaymentMethodRadioButtonsName + "']")
    .off()      //first remove all events
    .on("change", function (e) {
        model.WriteOrderData(viewModel.CompileData_PaymentType(), $(this), [viewModel.FillCreditCardTable]);
    })
    .on("blur", function (e) {
    });
}
viewModel.defineCardOnFileRadioEvents = function () {
    $("input[name='" + viewModel.Config.DetailDisplay.CreditCardRadioButtonsName + "']")
    .off()      //first remove all events
    .on("change", function (e) {
        model.WriteOrderData(viewModel.CompileData_CardOnFile(), $(this));
    })
    .on("blur", function (e) {
    });
}
viewModel.defineCardOnFileDescriptionInputEvents = function () {
    $('.' + viewModel.Config.DetailDisplay.CreditCardDescriptionsClass)
    .off()      //first remove all events
        .on("keydown", function (e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        if (key == 13) {
            e.preventDefault();
            viewModel.UpdateCardOnFileDescription($(this));
        };
    })
    .on("change", function (e) {
        viewModel.UpdateCardOnFileDescription($(this));
    })
    .on("focus", function (e) {
        $(this).select();
    })
    .on("blur", function (e) {

    });

};
viewModel.defineAgreeToTermsCheckboxEvent = function () {
    $("#" + viewModel.Config.DetailDisplay.AgreeToTermsCheckboxId)
    .off()      //first remove all events
    .on("click", function (e) {
        model.WriteOrderData(viewModel.CompileData_AgreeToTerms(), $('div.check_box_containers label'));
    })
    .on("blur", function (e) {

    });
}
///////////// Supplier Order Pick Lists //////////////////
viewModel.DefinePickListEvents = function () {
    $("." + viewModel.Config.SupplierOrders.shipMethodSelectorClass)
        .off()
        .on("change", function () {
        var selectionDisplay = $(this).siblings("." + viewModel.Config.SupplierOrders.shipMethodSelectedDisplayClass);
        var supplierOrderGuid = $(this).parents("." + viewModel.Config.SupplierOrders.DisplaysClass).prop("id");
        var text = $(this).children("option[value='" + $(this).val() + "']").text();
        selectionDisplay.text(text);

        var data = viewModel.CompileData_ShipMethodSelected(supplierOrderGuid, $(this).val());

        var callbacksToUpdateFeesAndTotals = function () {
          //  alert("here");
            model.getSupplierOrderFees(supplierOrderGuid, [function () {
                viewModel.refreshSupplierOrderTotals(supplierOrderGuid, [function () {
                    model.getGrowerOrderFees([function () {
                        viewModel.refreshGrowerOrderTotals();
                    }]);
                }]);
            }]);
        };
        model.WriteOrderData(data, selectionDisplay, [callbacksToUpdateFeesAndTotals]);
    });

    $("." + viewModel.Config.SupplierOrders.tagRatioSelectorClass)
        .off()
        
        .on("change", function () {
            var selectionDisplay = $(this).siblings("." + viewModel.Config.SupplierOrders.tagRatioSelectedDisplayClass);
            var supplierOrderGuid = $(this).parents("." + viewModel.Config.SupplierOrders.DisplaysClass).prop("id");
            var text = $(this).children("option[value='" + $(this).val() + "']").text();
            selectionDisplay.text(text);

            var data = viewModel.CompileData_TagRatioSelected(supplierOrderGuid, $(this).val());

            var callbacksToUpdateFeesAndTotals = function () {
               // alert("here 2");
                model.getSupplierOrderFees(supplierOrderGuid, [function () {
                //    alert("here 3");
                    viewModel.refreshSupplierOrderTotals(supplierOrderGuid, [function () {
                    model.getGrowerOrderFees([function () {
                        viewModel.refreshGrowerOrderTotals();
                    }]);
                }]);
            }]);
        };
        model.WriteOrderData(data, selectionDisplay, [callbacksToUpdateFeesAndTotals]);
    });

};

/////////////// Order Line Quantity Code ///////////////
viewModel.defineInputBoxEvents = function () {
   // alert("define");
    $('.' + viewModel.Config.SupplierOrders.LineItemGrid.OrderQuantityInputClass).on("keydown", function (e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
       // alert(key);
        var orderLineGuid = $(this).prop('id');
        orderLineGuid = orderLineGuid.substring(6);
        if (key == 13 || key == 9) {
            e.preventDefault();
            var inputBox = $(this);
            viewModel.handleOrderQuantityChange(orderLineGuid, inputBox);
            if (key == 9) {
               // alert("start before" + orderLineGuid);
                var nextGuid = viewModel.nextOrderLineGuid(orderLineGuid);
               // alert("start after" + nextGuid);
                $("#INPUT_" + nextGuid).focus();
            };
        } else if ((key >= 48 && key <= 57) || (key >= 96 && key <= 105)){
            //allow these keys
            //keyboard num keys: 48 to 57
            //numpad num keys: 96 to 105
            // viewModel.clearOrderIcon(orderLineGuid);
            //
        
        } else if (((key >= 33 && key <= 40) || key == 8 || key == 46 ) ) {
            //allow these keys
            //action keys: 33 to 40
            //backspace: 8
            //delete: 46
        } else if (key == 27) {
            //allow this key
            //escape: 27
            e.preventDefault();         //ie resets value automatically, others do not

            //$(this).val('');
            var orderLineGuid = $(this).parents("ul").prop('id');
          //  alert(orderLineGuid);
          //  alert("here 1");
            var orderLineData = model.GetOrderLineFromGuid(orderLineGuid);
            var currentQuantityOrdered = orderLineData.QuantityOrdered;

            var currentQuantityOrdered = numeral(orderLineData.QuantityOrdered).format('0,0') || "";

            $(this).val(currentQuantityOrdered);
            var s = $(this).val();
            viewModel.showQuantityCheckIcon(orderLineGuid);

        } else {
            //ignore all other keys
            e.preventDefault();
            //alert(key);
        };
    })
        .on("change", function (e) {
         //   alert('change');
            var guid = $(this).parents("ul").prop('id');
           // alert(guid);
            var inputBox = $(this);
            viewModel.handleOrderQuantityChange(guid, inputBox);
        })
        .on("focus", function (e) {
            $(this).select();
        })
        .on("blur", function (e) {
            var inputBox = $(this);
            var orderLineGuid = inputBox.parents("ul").prop('id');
            var orderLineData = model.GetOrderLineFromGuid(orderLineGuid);
            var currentQuantityOrdered = orderLineData.QuantityOrdered;

            var valu = inputBox.val();
            if (valu != null && valu != "") {
                var round = Math.round;
                valu = parseInt(round(valu)) || 0;
                
            } else {
                valu = 0;
            };

            //handle no change
            if (valu == currentQuantityOrdered) {
               // alert("no change")
                inputBox.val(valu);
                viewModel.showQuantityCheckIcon(orderLineGuid);
            };
        });
};
viewModel.handleOrderQuantityChange = function (orderLineGuid,inputBox) {
   // alert(valu);
    if (orderLineGuid == null || orderLineGuid.length != 36) { return false; };
    if (viewModel.state.orderQuantityBeingHandled)
    {
       // alert("being handled");
        return false
    };    //avoid keydown and change events both firing
    viewModel.state.orderQuantityBeingHandled = true;


    var supplierOrderGuid = model.ReturnSupplierOrderGuid(orderLineGuid);
  //  alert(supplierOrderGuid);

    //could pick up input bx by line guid too
    if (inputBox.length == 0) {
       // alert("no length");
        return false;
    };  //no such element

    var orderLineData = model.GetOrderLineFromGuid(orderLineGuid);
   // alert(traverseObj(orderLineData));
    var currentQuantityOrdered = orderLineData.QuantityOrdered;

    var valu = inputBox.val();
    if (valu != null && valu != "") {
        var round = Math.round;
        valu = parseInt(round(valu)) || 0;
        
    } else {
        valu = 0;
    };

    //handle no change
    if (valu == currentQuantityOrdered) {
       // alert("no change");
        inputBox.val(valu);
        viewModel.showQuantityCheckIcon(orderLineGuid);
        viewModel.state.orderQuantityBeingHandled = false;
        return false;
    };

    var result = viewModel.AdjustOrderQuantity(
        valu,
        (function () {
            //get max
            var max = orderLineData.AvailableQty;
            //if max is unlimited, set max = 999999.
            if (orderLineData.AvailabilityTypeCode == "OPEN") { max = 99999; };
            return max;
        })(),
        orderLineData.Minimum,
        orderLineData.Multiple
    );

//    alert(traverseObj(result));
    if (result.message !== null) {
        var position = inputBox.offset();
   //     alert(result.message);
        viewModel.floatMessage(inputBox, result.message, 2500);
    };
    var amt = result.amount; 

    ////////////////////////////////////

   // if (amt == 0 && orderLineData.QuantityOrdered > 0) {
        //Special case - setting to zero means clicking remoive

     //   inputBox.val(orderLineData.QuantityOrdered);
    //    var removeButton = $("#REMOVEIMG_" + orderLineGuid);
    //    $(removeButton).click();
    //    viewModel.state.orderQuantityBeingHandled = false;

   /// } else {

    var serverResponse = viewModel.submitOrderQuantity([
        {
            "Guid": orderLineGuid,
            "Price": model.ReturnOrderLinePrice(orderLineGuid),
            "Quantity": amt,
    }
    ], [function () {
       // alert(orderLineData.QuantityOrdered);
        //reset input value
        inputBox.val(orderLineData.QuantityOrdered);
        //reset trays count
        var tray = $("#TRAY_" + orderLineGuid);
        tray.text(numeral((orderLineData.QuantityOrdered / orderLineData.Multiple) || -1).format('0,0'));
        //reset price
        var priceExt = $("#EXTPRICE_" + orderLineGuid);
        priceExt.text(numeral((orderLineData.QuantityOrdered * orderLineData.Price) || -1).format('0,0.00'));
      
       // alert(supplierOrderGuid);
        if (supplierOrderGuid != null && supplierOrderGuid != ""){
        //    alert("in here" + supplierOrderGuid);
            model.getSupplierOrderFees(supplierOrderGuid, [function () {
                viewModel.refreshSupplierOrderTotals(supplierOrderGuid, [function () {
                    model.getGrowerOrderFees([function () {
                        viewModel.refreshGrowerOrderTotals();
                    }]);
                }]);
            }]);
        }
        viewModel.state.orderQuantityBeingHandled = false;
    }]);
  //  };

}
viewModel.ReturnOrderQuantityEntered = function (orderLineGuid) {   
    var inputBox = $("#INPUT_" + orderLineGuid);
    var valu = inputBox.val();
    if (valu != null && valu != "") {
        var round = Math.round;
        valu = parseInt(round(valu)) || 0;
    } else {
        valu = 0;
    };
    return valu;
};
viewModel.AdjustOrderQuantity = function (valu, max, min, mult) {
    var returnType = { "amount": valu, "message": null };

    var type = typeof valu;
    if (type === "undefined" || type !== "number" || valu <= 0) {
        returnType.amount = 0;
        return returnType;
    };
    var amt = valu;

    ///////////  Version 2 of Algorithm  /////////////////////
    //adjust multiple to at least 1
    if (mult == null || mult < 1) { mult = 1; };

    //adjust max to highest mult available
    max = Math.floor(max / mult) * mult;

    //adjust min to lowest multiple that meets minimum
    var v = mult;
    while (v < min) { v += mult; };
    min = v;

    if (min > max) {
        amt = 0;
        returnType.message = "Minimum order quantity not available!";
    } else {
        if (amt < min) {
            //raise amt to min
            amt = min;
        } else {
            if (amt > max) {
                //reduce amt to max
                amt = max;
                returnType.message = "Order quantity reduced to amount available.";
            } else {
                if (mult > 1) {
                    //adjust amt to nearest line item multiple
                    var v = Math.floor(amt / mult);
                    if ((amt % mult) >= (Math.floor(mult / 2))) { v++ };
                    amt = v * mult;
                };
            };
        };
    };
    /////////////////////////////////////////////////////////////////
    returnType.amount = amt;
    return returnType;
};
viewModel.submitOrderQuantity = function (cartOrderLineUpdates, callbacks) {

    $.each(cartOrderLineUpdates, function (ndx, item) {
        viewModel.showQuantityUpdatingIcon(item.Guid);
    });
    var serverResponse = null;
    var target = $("#test_area_results_display")
    target.empty();

    $.ajax({
        url: model.configData.urlCartApi,
        async: true,       //not async in order to return value
        type: "PUT",
        data: { CartOrderLineUpdates: cartOrderLineUpdates },
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
          //  target.append("<p>AJAX REQUEST: " + decodeURI(this.url) + " | Data Sent: " + decodeURI(this.data) + "</p>");
        },
        success: function (response, textStatus, jqXHR) {
         //   target.append("<hr /><p>SERVER RESPONSE: <br />" + traverseObj(response) + "</p>");
            serverResponse = response;
        },
        complete: function (jqXHR, textStatus) {
            $.each(cartOrderLineUpdates, function (ndx, item) {
                viewModel.showQuantityCheckIcon(item.Guid);
            });
            if (serverResponse != null) {
                $.each(serverResponse.OrderLineStatuses, function (ndx, item) {
                    var supplierOrderGuid = model.ReturnSupplierOrderGuid(item.OrderLineGuid);
                    var orderLineData = model.GetOrderLineFromGuid(item.OrderLineGuid);
                    orderLineData.QuantityOrdered = item.Quantity;

                    if (item.Quantity <= 0) {
                        $("#" + item.OrderLineGuid).remove();
                        model.RemoveOrderLineDataItem(item.OrderLineGuid);
                    } else {

                    }
                 //   alert(traverseObj(item));
                    if (item.Message != null && item.Message != "") {
                    var inputBox = $("#INPUT_" + item.OrderLineGuid);
                    viewModel.floatMessage(inputBox, serverResponse.Message, 2500);
                    };
                    if (model.OrderLineCount(supplierOrderGuid) == 0) {
                        model.RemoveSupplierOrderDataItem(supplierOrderGuid);

                        $("#" + supplierOrderGuid).remove();
                        if (model.SupplierOrderCount() <= 0) {
                            viewModel.RemoveDetailDisplay();
                            model.getSummaryData([viewModel.DisplaySummaryGrid, function () {
                                eps_tools.OkDialog({ 'title': '<span>Update:</span>', 'content': '<br /><span>Grower Order removed.</span><br />', 'modal': true });
                            }]);
                        } else {
                            eps_tools.OkDialog({ 'title': '<span>Update:</span>', 'content': '<br /><span>Supplier Order removed.</span><br />', 'modal': true });
                        };
                    };
                });
            };
            viewModel.ProcessEventChain(callbacks);
        }
    });
};
viewModel.nextOrderLineGuid = function (orderLineGuid) {

    if (orderLineGuid == null || orderLineGuid.length < 36) { return "" };
    if ($("#" + orderLineGuid).length < 1) { return "" };


   // var supplierOrderGuid = model.ReturnSupplierOrderGuid(orderLineGuid);
   // if (supplierOrderGuid == '') { return ''; };
    var returnGuid= orderLineGuid;
   
    var bNext = false;
   // alert("start 6");
    $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, order) {
    //    if (order.SupplierOrderGuid == supplierOrderGuid) {
        if (typeof order.OrderLines !== 'undefined' && order.OrderLines != null) {
            for (j = 0; j < order.OrderLines.length; j++) {
                if (bNext == true)
                {
                    returnGuid = order.OrderLines[j].Guid;
                    bNext=false;  
                }
                //alert(traverseObj(order.OrderLines));                     
                if (order.OrderLines[j].Guid == orderLineGuid) {
                    bNext = true;
                }
            }
        };        
       // }
    });
    return returnGuid;
};
viewModel.showOrderIcon = function (orderLineGuid, src) {
    var imgElement = $("#IMG_" + orderLineGuid);
    if (typeof imgElement !== 'undefined' && imgElement != null && imgElement.length > 0) {
        imgElement.prop("src", src);
    };
};
viewModel.clearOrderIcon = function (orderLineGuid) {
    viewModel.showOrderIcon(orderLineGuid, viewModel.Config.Icons.DotClearSrc);
};
viewModel.showQuantityUpdatingIcon = function (orderLineGuid) {
    viewModel.showOrderIcon(orderLineGuid, viewModel.Config.Icons.AjaxWorkingSrc);
};
viewModel.showQuantityCheckIcon = function (orderLineGuid) {
    viewModel.showOrderIcon(orderLineGuid, viewModel.Config.Icons.ItemCheckedSrc);
};

////////////// Update Link Code //////////////////////////////
    //viewModel.defineUpdateLinkEvents = function () {
    //    $("." + viewModel.Config.SupplierOrders.LineItemGrid.UpdateLinksClass).off().on("click", function () {
    //        var supplierOrderGuid = $(this).parents("div." + viewModel.Config.SupplierOrders.DisplaysClass).prop("id");
    //        viewModel.handleUpdateClick(supplierOrderGuid);
    //    });
    //};
    //viewModel.handleUpdateClick = function (supplierOrderGuid) {
    //   // alert("update Click   " + supplierOrderGuid);
    //    //validate guid
    //    if (supplierOrderGuid == null || supplierOrderGuid.length != 36) { return false; };

    //    var orderLineUpdates = viewModel.CompileUpdateData(supplierOrderGuid).Updates;
    //    if (orderLineUpdates != null && orderLineUpdates.length > 0) {
    //        viewModel.submitOrderQuantity(orderLineUpdates);
    //    } else {
    //        eps_tools.OkDialog({ 'title': '<span>Update:</span>', 'content': '<br /><span>No row(s) marked to remove.</span><br />', 'modal': true });
    //    };
    //};
    //viewModel.CompileUpdateData = function (supplierOrderGuid) {
    //    var d = {};
    //    d.SupplierOrderGuid = supplierOrderGuid;
    //    d.Updates = []

    //    $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, order) {
    //        if (order.SupplierOrderGuid == supplierOrderGuid) {
    //            if (typeof order.OrderLines !== 'undefined' && order.OrderLines != null) {

    //                for (j = 0; j < order.OrderLines.length; j++) {
    //                    // alert(traverseObj(order.OrderLines));                     
    //                    if (viewModel.ReturnOrderQuantityEntered(order.OrderLines[j].Guid) == 0) {
    //                        var price = model.ReturnOrderLinePrice(order.OrderLines[j].Guid);
    //                        d.Updates.push({ "Guid": order.OrderLines[j].Guid, "Price": price, "Quantity": 0 });
    //                    }
    //                }
    //            };
    //            return false;  //exit loop
    //        }
    //    });

    //    return d;
    //};
//////////////////////////////////////////////////////////////
    viewModel.refreshSupplierOrderTotals = function (supplierOrderGuid, callbacks) {
       
        var supplierOrderTotalTrays = 0;
        var supplierOrderTotalCuttings = 0;
        var supplierOrderTotalPrice = 0.00;
        var totalSupplierCost = 0.00;

        $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, order) {
            if (order.SupplierOrderGuid == supplierOrderGuid) {
                if (typeof order.OrderLines !== 'undefined' && order.OrderLines != null) {
                    for (j = 0; j < order.OrderLines.length; j++) {
                       // alert(traverseObj(order.OrderLines));                     
                        if (viewModel.ReturnOrderQuantityEntered(order.OrderLines[j].Guid) != 0) {
                            if ( order.OrderLines[j].Multiple > 0){
                                supplierOrderTotalTrays += order.OrderLines[j].QuantityOrdered / order.OrderLines[j].Multiple
                            }
                            supplierOrderTotalCuttings += order.OrderLines[j].QuantityOrdered;
                            supplierOrderTotalPrice += order.OrderLines[j].QuantityOrdered * order.OrderLines[j].Price;
                        }
                    }
                    totalSupplierCost += supplierOrderTotalPrice;
                };
                return false;  //exit loop
            }
        });
   
        var supplierOrderSubTotalRow = $("#" + supplierOrderGuid);
        supplierOrderSubTotalRow.find("." + viewModel.Config.SupplierOrders.LineItemGrid.trayTotalsClass).text(numeral(supplierOrderTotalTrays).format('0,0'));
        supplierOrderSubTotalRow.find("." + viewModel.Config.SupplierOrders.LineItemGrid.OrderQuantityTotalClass).text(numeral(supplierOrderTotalCuttings).format('0,0'));
        supplierOrderSubTotalRow.find("." + viewModel.Config.SupplierOrders.LineItemGrid.PriceTotalsClass).text(numeral(supplierOrderTotalPrice).format('$ 0,0.00'));
       
        var supplierTotalCostDiv = supplierOrderSubTotalRow.find("#supplier_fee")
        var feeList = model.returnSupplierOrderFees(supplierOrderGuid) || [];
      //  alert(traverseObj(feeList));
        var feeDiv = $("<div id='supplier_fee'/>");
        $.each(feeList, function (ndx, fee) {
            totalSupplierCost += fee.Amount;

            //Make the Row
            var row = $("<ul class='supplier_subtotal_boxes'>");
            var rowProperties = { "id": fee.Guid};
            row.prop(rowProperties);

            //Item for icon and description
            var li1 = $("<li>");
            var li1Properties = { "class": "right" };
            li1.prop(li1Properties);

            var span = "";
            var description = fee.FeeDescription || "";
            description = description.trim();
            if (description != "") {
                li1
                .off()
                .on({
                    mouseenter: function () {
                        eps_tools.HoverMessage($(this)).load(description).display(0);    //0 means display left of element
                    },
                    mouseleave: function () {
                        eps_tools.HoverMessage($(this)).remove();
                    }
                });

                 span = "<span class='supplier_order_fee_info_icons'>&nbsp;<img style='display:inline-block; vertical-align:text-bottom; width:20px; height:20px; margin: 0; padding:0;' src='/Images/icons/icon_information_circle_green_24.png' alt='' />&nbsp;</span>";
                 li1.html('').append(span);
            };
            row.append(li1);
            
            //Item for Fee Type
            var li2 = $("<li>");
            var li2Properties = { "class": "left"};
            li2.prop(li2Properties);
            li2.text(fee.SupplierOrderFeeTypeDescription || "");
            row.append(li2);
            
            //Item for Fee Amount
            var feeToDisplay = numeral(fee.Amount).format('$ 0,0.00');
            if (fee.SupplierOrderFeeStatusCode.toLowerCase() === "included" || fee.SupplierOrderFeeStatusCode.toLowerCase() === "tbd" || fee.SupplierOrderFeeStatusCode.toLowerCase() === "na") {
                feeToDisplay = fee.SupplierOrderFeeStatusCode;
            };

            var li3 = $("<li>");
            var li3Properties = { "class": "right" };
            li3.prop(li3Properties);
            li3.text(feeToDisplay || "");
            row.append(li3);

            feeDiv.append(row);
        });
        supplierTotalCostDiv.replaceWith(feeDiv);

        supplierOrderSubTotalRow.find("." + viewModel.Config.SupplierOrders.LineItemGrid.supplierOrderTotalCostsClass).text(numeral(totalSupplierCost).format('$ 0,0.00'));

        $("#" + supplierOrderGuid).data({
            "supplierOrderTotalTrays": supplierOrderTotalTrays,
            "supplierOrderTotalCuttings": supplierOrderTotalCuttings,
            "supplierOrderTotalPrice": supplierOrderTotalPrice,
            "supplierOrderTotalCost": totalSupplierCost
        });
       
        viewModel.ProcessEventChain(callbacks);
    };
    viewModel.refreshGrowerOrderTotals = function () {
       // alert("refreshGrowerOrderTotals");
        var growerOrderTotalTrays = 0;
        var growerOrderTotalCuttings = 0;
        var growerOrderTotalCost = 0.00;

        $.each(model.DetailData.GrowerOrder.SupplierOrders, function (i, supplierOrder) {
            var data = $("#" + supplierOrder.SupplierOrderGuid).data();
            //alert(data.supplierOrderTotalCost);
            growerOrderTotalTrays += data.supplierOrderTotalTrays || 0;
            growerOrderTotalCost += data.supplierOrderTotalCost || 0;
            growerOrderTotalCuttings += data.supplierOrderTotalCuttings || 0;
        });
      //  alert("here 1");
        var growerFeesDiv = $("#grower_fees")
        var feeList = model.returnGrowerOrderFees() || [];
        //alert(traverseObj(feeList));

        var feeDiv = $("<div id='grower_fees'>");
        $.each(feeList, function (ndx, fee) {
            //alert("fee loop");
            growerOrderTotalCost += fee.Amount;

            //Make the Row
            var row = $("<ul>");
            var rowProperties = {
                "id": fee.Guid,
                "class": "supplier_subtotal_boxes"
            };
            row.prop(rowProperties);

            //Item for icon and description
            var li1 = $("<li>");
            var li1Properties = { "class": "right" };
            li1.prop(li1Properties);

            var span = "";
            var description = fee.FeeDescription || "";
            description = description.trim();
            if (description != "") {
                li1
                .off()
                .on({
                    mouseenter: function () {
                        eps_tools.HoverMessage($(this)).load(description).display(0);    //0 means display left of element
                    },
                    mouseleave: function () {
                        eps_tools.HoverMessage($(this)).remove();
                    }
                });

                 span = "<span class='supplier_order_fee_info_icons'>&nbsp;<img style='display:inline-block; vertical-align:text-bottom; width:20px; height:20px; margin: 0; padding:0;' src='/Images/icons/icon_information_circle_green_24.png' alt='' />&nbsp;</span>";
                 li1.html('').append(span);
            };
            row.append(li1);
            
            //Item for Fee Type
            var li2 = $("<li>");
            var li2Properties = { "class": "left"};
            li2.prop(li2Properties);
            li2.text(fee.GrowerOrderFeeTypeDescription || "");
            row.append(li2);
            
            //Item for Fee Amount
          //  alert(fee.Amount)
            var feeToDisplay = numeral(fee.Amount).format('$ 0,0.00');
            if (fee.GrowerOrderFeeStatusCode.toLowerCase() === "included" || fee.GrowerOrderFeeStatusCode.toLowerCase() === "tbd" || fee.GrowerOrderFeeStatusCode.toLowerCase() === "na") {
                feeToDisplay = fee.GrowerOrderFeeStatusCode;
            };

            var li3 = $("<li>");
            var li3Properties = { "class": "right" };
            li3.prop(li3Properties);
            li3.text(feeToDisplay || "");
            row.append(li3);

            feeDiv.append(row);
        });
        growerFeesDiv.replaceWith(feeDiv);
       
        $("." + viewModel.Config.DetailDisplay.orderTotalsGrid.totalTraysSpanId).text(numeral(growerOrderTotalTrays).format('0,0'));
        $("." + viewModel.Config.DetailDisplay.orderTotalsGrid.totalCuttingsSpanId).text(numeral(growerOrderTotalCuttings).format('0,0'));
        $("." + viewModel.Config.DetailDisplay.orderTotalsGrid.totalCuttingsCostSpanId).text(numeral(growerOrderTotalCost).format('$ 0,0.00'));
    };
    viewModel.FillCreditCardTable = function () {
        var display = $("#" + viewModel.Config.DetailDisplay.CreditCardDisplayId);
        var table = $("#" + viewModel.Config.DetailDisplay.CreditCardTableId);
        table.html("");

        var creditCards = model.DetailData.GrowerOrder.CreditCards || [];
        var invoiceSelected = viewModel.state.PaymentMethodInvoiceSelected();
        if (invoiceSelected) {
            //hide credit card display
            $("#" + viewModel.Config.DetailDisplay.CreditCardDisplayId).css("display", "none");
        } else if (creditCards.length == 0) {
            table.hide();
            $("#no_cards_on_file_display").show();
            display.show();

            //show credit card display
            $("#" + viewModel.Config.DetailDisplay.CreditCardDisplayId).css("display", "block");
           
            ////set Invoice as payment method
            viewModel.DisplayAddCardOnFileForm();

        } else {
            table.show();
            $("#no_cards_on_file_display").hide();

            //show credit card display
            $("#" + viewModel.Config.DetailDisplay.CreditCardDisplayId).css("display", "block");

            var selectionValues = { "First": "", "Default": "", "Selected": "" };
            $.each(model.DetailData.GrowerOrder.CreditCards, function (i, card) {
                //capture the code of the tag to be selected
                if (i == 0) { selectionValues.First = card.Guid; };
                if (card.IsSelected) { selectionValues.Selected = card.Guid; };
                if (card.IsDefault) { selectionValues.Default = card.Guid; };
              //  alert(card.Guid)
                //compose html
                var row = $("<ul>");
                var td0 = $("<li>");
                if (model.state.isReadOnly == false) {
                    var radio = $("<input />")
                        .prop({
                            "type": "radio",
                            "id": "credit_card_selection_" + i,
                            "name": "credit_card_radio_buttons_name",
                            "class": viewModel.Config.DetailDisplay.CreditCardRadioButtonsClass,
                            "value": card.Guid
                        });
                }
                else {
                    var radio = $("<input />")
                        .prop({
                            "type": "radio",
                            "id": "credit_card_selection_" + i,
                            "name": "credit_card_radio_buttons_name",
                            "class": viewModel.Config.DetailDisplay.CreditCardRadioButtonsClass,
                            "value": card.Guid,
                            "disabled": "disabled"
                    });
                }
                td0.append(radio);
                row.append(td0);

                var td1 = $("<li>");
                var cardNumber = $('<span />')
                    .text("Card: " + card.CardNumber);
                td1.append(cardNumber);
                row.append(td1);

                var td2 = $("<li>");
                if (model.state.isReadOnly == false) { 
                    var cardDescription = $('<input type="text" >').prop(
                        {
                            "maxlength": "35",
                            "value": card.CardDescription || "",
                            "class": viewModel.Config.DetailDisplay.CreditCardDescriptionsClass,
                        }).data({ "guid": card.Guid });
                }
                else {
                    var cardDescription = $('<input type="text" >').prop(
                            {
                                "maxlength": "35",
                                "value": card.CardDescription || "",
                                "class": viewModel.Config.DetailDisplay.CreditCardDescriptionsClass,
                                "disabled": "disabled",
                            }).data({ "guid": card.Guid });
                }
                td2.append(cardDescription);
                row.append(td2);

                var td3 = $("<li>");
                if (model.state.isReadOnly == false) {
                    var deleteLink = $("<a />")
                        .prop({ "class": viewModel.Config.DetailDisplay.CreditCardDeleteLinksClass })
                        .text("DELETE")
                        .on("click", function () {
                            var offset = $(this).offset();
                            viewModel.DisplayDeleteCardOnFileForm(card.Guid, { left: offset.left - $(window).scrollLeft(), top: offset.top - $(window).scrollTop() });
                        }
                    );
                }
                else { var deleteLink = $("<a />") }
                td3.append(deleteLink);
                row.append(td3);

                table.append(row);
            });
            viewModel.defineCardOnFileDescriptionInputEvents();

            var selectedGuid = "";
            if (selectionValues.Selected != null && selectionValues.Selected != "") {
                selectedGuid = selectionValues.Selected;
            } else {
                if (selectionValues.Default != null && selectionValues.Default != "") {
                    selectedGuid = selectionValues.Default;
                } else {
                    if (selectionValues.First != null && selectionValues.First != "") {
                        selectedGuid = selectionValues.First;
                    };
                };
            };
            if (typeof selectedGuid !== "undefined" && selectedGuid != null && selectedGuid.length == 36) {
                var radioButtons = $("." + viewModel.Config.DetailDisplay.CreditCardRadioButtonsClass);
                $("input:radio[value='" + selectedGuid + "']").prop({ "checked": "checked" });
            };
        };
    };
    viewModel.FillAddressTable = function (tableDiv) {
        tableDiv.empty();
        model.state.selectedAddressGuid = null;

        $.each(model.DetailData.GrowerOrder.ShipToAddresses, function (i, item) {
      //      alert(item.Guid);
            if (item.IsSelected) {
                model.state.selectedAddressGuid = item.Guid;
            } else if (model.state.selectedAddressGuid == null) {
                if (i == 0 || item.IsDefault) { model.state.selectedAddressGuid = item.Guid; };
            };

            var row = $("<ul style='list-style:none;'>");

            var cell1 = $("<li>");
            var radio = $("<input type=\"radio\" />");

            if (model.state.isReadOnly == false) {
                var radioProperties = {
                    "id": item.Guid,
                    "name": viewModel.Config.DetailDisplay.ShipToRadioButtonsName,
                    "class": viewModel.Config.DetailDisplay.ShipToRadioButtonsClass,
                    "value": item.Guid,
                };
            }
            else {
                var radioProperties = {
                    //"id": viewModel.Config.DetailDisplay.RadioSelectionShipToIdStem + i,
                    "id": item.Guid,
                    "name": viewModel.Config.DetailDisplay.ShipToRadioButtonsName,
                    "class": viewModel.Config.DetailDisplay.ShipToRadioButtonsClass,
                    "value": item.Guid,
                    "disabled": "disabled",
                };
            }

            radio.prop(radioProperties);
            cell1.append(radio);
            cell1.append("&nbsp;" + item.Name);
            row.append(cell1);

            var cell2 = $('<li>' + item.StreetAddress1 + '</li><li>' + item.StreetAddress2 + '</li><li>' + item.City + ", " + item.StateCode + " " + item.ZipCode + '</li><li>' + item.Country + '</li>');

            row.append($("<li />").append($('<ul style="list-style:none;" />').append(cell2)));

            var cell3 = $("<li></li>");

            var editLink = $("<a />");
            editLinkProperties = { "class": viewModel.Config.DetailDisplay.ShipToEditLinkClass };
            editLink.prop(editLinkProperties);
            editLink.text("EDIT");
            editLink.on("click", function () {
                //alert("Edit Address Guid: " + item.Guid);
                viewModel.DisplayEditShipToAddressForm(item.Guid);
            });
            cell3.append(editLink);
            row.append(cell3);
            tableDiv.append(row);
        });

        var buttonToBeSelected = $("#" + model.state.selectedAddressGuid)
        buttonToBeSelected.prop("checked", true);
    };
    viewModel.DisplayPromoName = function (theCode,callbacks) {
      //  alert("get name");
        model.getPromoName(theCode, callbacks);
    }
   
////////////////////// Utilities /////////////////////////
    viewModel.ProcessEventChain = function (callbacks) {

        if (typeof callbacks !== 'undefined' && callbacks != null && callbacks.length > 0) {
            var callback = callbacks.shift();
            callback(callbacks);
        }
    };
    viewModel.floatMessage = function (element, content, interval) {
        //floats message to right of element
        //if no element then does nothing
        if (typeof element !== 'undefined' && element != null && element.length > 0) {
            var position = element.offset();
            //ToDo: code to center floating message vertically with element
            //assume floating message is 35 px tall

            var top = Math.floor(position.top + ((element.height() - 35) / 2));
            var left = Math.floor(position.left + element.width() + 30);
            eps_tools.FloatingMessage(left, top, content, interval);
        };
    };
   
    
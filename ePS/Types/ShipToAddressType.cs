﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ePS.Types
{
    public class ShipToAddressType : AddressType
    {
        [DataMember]
        public Guid GrowerAddressGuid { get; set; }

        [DataMember]
        public Guid Guid { get { return GrowerAddressGuid; } }

        [DataMember]
        public string PhoneNumber { get; set; }
        
        [DataMember]
        public string SpecialInstructions { get; set; }

        [DataMember]
        public bool IsDefault { get; set; }

        [DataMember]
        public bool IsSelected { get; set; }
    }
}
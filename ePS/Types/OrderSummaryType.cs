using System;
using System.Runtime.Serialization;

namespace ePS.Types
{
    [DataContract]
    public class OrderSummaryType
    {
        [DataMember]
        public Guid OrderGuid { get; set; }

        [DataMember]
        public string OrderNo { get; set; }

        [DataMember]
        public string ShipWeekString { get; set; }

        [DataMember]
        public string ProgramCategoryName { get; set; }

        [DataMember]
        public string ProductFormName { get; set; }

        [DataMember]
        public int OrderQty { get; set; }

        [DataMember]
        public string CustomerPo { get; set; }

        [DataMember]
        public string OrderDescription { get; set; }

        [DataMember]
        public string CustomerCode { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string GrowerOrderStatusCode { get; set; }

        [DataMember]
        public string LowestSupplierOrderStatusCode { get; set; }

        [DataMember]
        public string LowestOrderLineStatusCode { get; set; }

        [DataMember]
        public PersonType PersonWhoPlacedOrder { get; set; }
    }
}
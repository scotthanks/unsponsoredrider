﻿using System;
using System.Runtime.Serialization;

namespace ePS.Types
{
    [DataContract]
    public class GrowerOrderType
    {
        [DataMember]
        public Guid OrderGuid { get; set; }

        [DataMember]
        public string ShipWeekCode { get; set; }

        [DataMember]
        public string OrderNo { get; set; }

        [DataMember]
        public string CustomerPoNo { get; set; }

        [DataMember]
        public string GrowerName { get; set; }

        [DataMember]
        public string GrowerCode { get; set; }

        [DataMember]
        public string GrowerPhone { get; set; }

        [DataMember]
        public string GrowerEmail { get; set; }

        [DataMember]
        public string GrowerAdditionalAccountingEmail { get; set; }
        
        [DataMember]
        public string GrowerAdditionalOrderEmail { get; set; }

        [DataMember]
        public bool GrowerAllowsSubstitutions { get; set; }

        [DataMember]
        public string OrderDescription { get; set; }

        [DataMember]
        public DateTime DateEntered { get; set; }

        //TODO: Sort this out (why dates don't deserialize).
        [DataMember]
        public string DateEnteredString { get; set; }

        [DataMember]
        public string OrderType { get; set; }

        [DataMember]
        public string ProgramTypeCode { get; set; }

        [DataMember]
        public string ProgramTypeName { get; set; }

        [DataMember]
        public string ProductFormCategoryCode { get; set; }

        [DataMember]
        public string ProductFormCategoryName { get; set; }

        [DataMember]
        public string GrowerOrderStatusCode { get; set; }

        [DataMember]
        public SelectListItemType[] PaymentTypeList { get; set; }

        [DataMember]
        public CreditCardType[] CreditCards { get; set; }

        [DataMember]
        public ShipToAddressType[] ShipToAddresses { get; set; }

        [DataMember]
        public SupplierOrderType[] SupplierOrders { get; set; }

        [DataMember]
        public LogType[] Logs { get; set; }

        [DataMember]
        public PersonType PersonWhoPlacedOrder { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ePS.Types
{
    [DataContract]
    public class SelectListItemType
    {
        [DataMember]
        public Guid Guid { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public bool IsDefault { get; set; }

        [DataMember]
        public bool IsSelected { get; set; }

        [DataMember]
        public string ParentPath { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ePS.Types
{
    [DataContract]
    public class SupplierOrderFeeType
    {

        [DataMember]
        public Guid Guid { get; set; }

        [DataMember]
        public Guid SupplierOrderGuid { get; set; }

        [DataMember]
        public String SupplierOrderFeeTypeCode { get; set; }

        [DataMember]
        public Decimal Amount { get; set; }

    }
}
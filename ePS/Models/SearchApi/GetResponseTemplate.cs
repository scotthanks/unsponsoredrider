﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using BusinessObjectsLibrary;
using BusinessObjectServices;

namespace ePS.Models.SearchApi
{
    public class GetResponseTemplate : ResponseBase
    {
        public GetResponseTemplate(Guid userGuid, GetRequestTemplate request)
            : base(userGuid, requiresAuthentication: false)
        {
            if (ValidateAuthenticationAndRequest(request))
            {

                //ToDo: Fetch the stuff from data

                var service = new ContentService();
                var template = service.GetSearchResultTemplateType(request.TemplateGuid);

                var result = new SearchResultTemplateType() { 
                    TemplateGuid = template.Guid,
                    Css = template.Css,
                    Html = template.Html,
                    Javascript = template.Javascript
                };

                ResultTemplate = result;

            }
        }

        public SearchResultTemplateType ResultTemplate { get; set; }

    }
}
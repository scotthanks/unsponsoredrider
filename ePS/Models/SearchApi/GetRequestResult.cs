﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
namespace ePS.Models.SearchApi
{
    public class GetRequestResult : RequestBase
    {
        public enum ScopeEnum
        {
            OpenSearch,
            CatalogPage,
            AvailabilityPage
        }


        public string Scope { get; set; }
        public string Term { get; set; }

        public override void Validate()
        {
            const int minimum_length = 3;
            const int maximum_length = 50;
            ScopeEnum requestType;

            bool pageIsValid = Enum.TryParse(Scope, ignoreCase: true, result: out requestType);
            if (!pageIsValid)
            {
                AddMessage(string.Format("Scope: {0} - not valid.", Scope));
            }

            Term = Term ?? string.Empty;
            Term = Term.Trim();
            bool stemIsValid = Term.Length >= minimum_length;
            if (!stemIsValid)
            {
                AddMessage(string.Format("Search not valid. Minimum search term: {1} characters.", Scope, minimum_length));
            }
            stemIsValid = Term.Length <= maximum_length;
            if (!stemIsValid)
            {
                AddMessage(string.Format("Search not valid. Maximum search term: {1} characters.", Scope, maximum_length));
            }
            
            if (Regex.IsMatch(Term, "[^0-9a-zA-Z.\\s]")==true) 
            
              {
                AddMessage("Search not valid. invalid character.");
            }

        }

    }
}
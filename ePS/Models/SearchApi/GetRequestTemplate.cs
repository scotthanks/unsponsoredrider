﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.SearchApi
{
    public class GetRequestTemplate : RequestBase
    {

        public Guid TemplateGuid { get; set; }
        
        public override void Validate()
        {
            bool guidIsEmpty = TemplateGuid == Guid.Empty;

            if (guidIsEmpty)
            {
                AddMessage(string.Format("Guid: {0} is empty.", TemplateGuid));
            }
        }


    }
}
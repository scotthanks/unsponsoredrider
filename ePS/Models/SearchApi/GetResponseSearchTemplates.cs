﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using BusinessObjectsLibrary;
using BusinessObjectServices;


namespace ePS.Models.SearchApi
{
    public class GetResponseSearchTemplates
    {
        public GetResponseSearchTemplates() { 
        
            var service = new ContentService();
            var templates = service.GetSearchResultTemplates();

            var list = new List<SearchResultTemplateType>();
            foreach (var template in templates) { 
            
                var result = new SearchResultTemplateType()
                {
                    TemplateGuid = template.Guid,
                    Css = template.Css,
                    Html = template.Html,
                    Javascript = template.Javascript
                };
                list.Add(result);
            };
            this.SearchResultTemplates = list;
        }

        public IEnumerable<SearchResultTemplateType> SearchResultTemplates { get; set; }
    }
}
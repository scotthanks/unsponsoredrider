﻿using AdminAppApiLibrary;
using DataObjectLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AdminAppOrderDetailApi
{
    public class AddOrderLineResponseModel : ResponseBase
    {
        public AddOrderLineResponseModel(Guid userGuid, AddOrderLineRequest request)
            : base(userGuid, requiresAuthentication: true)
        {
            Response = new AddOrderLineResponse();

            try
            {

                OrderLineTableService orderLineService = new OrderLineTableService();
                OrderLine orderLine = new OrderLine();

                orderLine.Guid = Guid.NewGuid();
                orderLine.DateDeactivated = null;
                orderLine.SupplierOrderGuid = request.SupplierOrderGuid;
                orderLine.ProductGuid = request.ProductGuid;
                orderLine.QtyOrdered = request.QtyOrdered;
                orderLine.QtyShipped = 0;
                orderLine.ActualPrice = request.Price;
                orderLine.OrderLineStatusLookupGuid = LookupTableValues.Code.OrderLineStatus.Ordered.Guid;
                orderLine.ChangeTypeLookupGuid = LookupTableValues.Code.ChangeType.NoChange.Guid;
                orderLine.DateEntered = DateTime.Now;

                orderLineService.Insert(orderLine);

                Response.Success = true;

            }
            catch (Exception ex)
            {
                Response.Success = false;
                Response.Message = ex.Message;
            }
        }

        public AddOrderLineResponse Response;
    }
}
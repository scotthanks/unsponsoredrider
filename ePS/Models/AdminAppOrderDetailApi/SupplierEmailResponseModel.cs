﻿using AdminAppApiLibrary;
using DataObjectLibrary;
using DataServiceLibrary;
using ePS.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AdminAppOrderDetailApi
{
    public class SupplierEmailResponseModel : ResponseBase
    {
        public SupplierEmailResponseModel(Guid userGuid, SendEmailRequest request)
            : base(userGuid, requiresAuthentication: false)
        {
            Response = new AdminAppResponseBase();
            try
            {
                SupplierOrderTableService orderService = new SupplierOrderTableService();
                SupplierOrder order = (SupplierOrder)orderService.GetByGuid(request.supplierOrderGuid);
                order.DateLastSenttoSupplier = DateTime.Now;
                orderService.Update(order);
                Response.Success = true;
                Response.Message = "";

            }
            catch (Exception ex)
            {

                Response.Success = false;
                Response.Message = ex.Message;
            }
        }

        public AdminAppResponseBase Response;
    }
}
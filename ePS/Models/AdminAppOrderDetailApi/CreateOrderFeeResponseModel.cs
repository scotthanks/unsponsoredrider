﻿using AdminAppApiLibrary;
using BusinessObjectServices;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectsLibrary;

namespace ePS.Models.AdminAppOrderDetailApi
{
    public class CreateOrderFeeResponseModel
    {
        public CreateOrderFeeResponseModel(Guid userGuid, CreateOrderFeeRequest request)
        {
            Response = new CreateOrderFeeResponse();
            if (userGuid == Guid.Empty)
            {
                Response.Success = false;
                Response.Message = "Not authorized";
                return;
            }
            var status = new StatusObject(userGuid, true);
            GrowerOrderFeeDataService feeService = new GrowerOrderFeeDataService(status);
            DataObjectLibrary.Lookup LookupValue = LookupTableService.SingletonInstance.TryGetByCode("Code/GrowerOrderFeeType", request.GrowerOrderFeeTypeCode);
            Guid feeTypeLookupGuid = LookupValue.Guid;
            RequestStatus requestStatus = feeService.CreateOrderFee(request.Guid, request.GrowerOrderGuid, request.Amount, feeTypeLookupGuid);
            Response.Success = requestStatus.Success;
            Response.Message = requestStatus.Message;
        }

        public CreateOrderFeeResponse Response;
    }
}
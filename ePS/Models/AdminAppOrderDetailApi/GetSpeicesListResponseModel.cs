﻿using AdminAppApiLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectsLibrary;
using BusinessObjectServices;
namespace ePS.Models.AdminAppOrderDetailApi
{
    public class GetSpeciesListResponseModel
    {
        public GetSpeciesListResponseModel(Guid userGuid, GetSpeciesListForSupplierOrderRequest request)
        {
           // SelectSpecies;
            Response = new GetSpeciesListForSupplierOrderResponse();
           
            if (userGuid == Guid.Empty)
            {
                Response.Success = false;
                Response.Message = "Not authorized";
                return;
            }
            var supplierOrderService = new SupplierOrderTableService();
            var supplierOrder = supplierOrderService.GetByGuid(request.SupplierOrderGuid);
            
            var listSupplierCode = new List<string>();
            listSupplierCode.Add( supplierOrder.Supplier.Code);
            string[] theStringArray = listSupplierCode.ToArray();
            
            var status = new StatusObject(userGuid, true);
            SpeciesService theService = new SpeciesService(status);
            var theList = theService.SelectSpecies("EPS", supplierOrder.GrowerOrder.ProgramType.Code, supplierOrder.GrowerOrder.ProductFormCategory.Code, theStringArray);
           
            Response.SpeciesList = theList.ToList();
            Response.Success = true;
            Response.Message = null;
        }
        
        public GetSpeciesListForSupplierOrderResponse Response;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.ProgramApi
{
    public class GetResponseModel : ResponseBase
    {

        public GetResponseModel(Guid userGuid, GetRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {

            var programService = new BusinessObjectServices.ProgramService();
            
            var programList = programService.GetAllPrograms();
            var newProgramList = new List<ePS.Types.ProgramType>();
            foreach (var program in programList)
            {

                var newItem = new ePS.Types.ProgramType
                {
                    ProgramGuid = program.ProgramGuid, 
                    Code = program.Code, 
                    Name = program.Name,
                    ProgramTypeGuid = program.ProgramTypeGuid,
                    SupplierGuid = program.SupplierGuid,

                };

                newProgramList.Add(newItem);
            }

            this.Programs = newProgramList;
        }

        public List<ePS.Types.ProgramType> Programs { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.LedgerApi
{
    public class ClosedTransaction
    {
        public string TransactionType { get; set; }
        public string TransactionId { get; set; }
        public decimal Amount { get; set; }
        //public decimal AmountPaid { get; set; }
        //public decimal Balance { get; set; }
        public string PaymentId { get; set; }
        public string OrderNumber { get; set; }
        public DateTime Date { get; set; }
    }

    public class PaymentsResponse : ResponseBase
    {
        //public decimal AccountBalance { get; set; }
        public List<ClosedTransaction> TransactionList { get; set; }

        public PaymentsResponse(Guid userGuid)
            : base(userGuid, requiresAuthentication: true)
        {
        }
    }
}
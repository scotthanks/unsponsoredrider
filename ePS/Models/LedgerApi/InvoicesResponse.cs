﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.LedgerApi
{
    public class OpenTransaction
    {
        public string TransactionType { get; set; }
        public string TransactionId { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountPaid { get; set; }
        public decimal Balance { get; set; }
        public DateTime Date { get; set; }
        public string OrderNumber { get; set; }
    }

    public class InvoicesResponse : ResponseBase
    {
        public decimal AccountBalance { get; set; }
        public List<OpenTransaction> TransactionList { get; set; }

        public InvoicesResponse(Guid userGuid)
            : base(userGuid, requiresAuthentication: true)
        {
        }
    }
}
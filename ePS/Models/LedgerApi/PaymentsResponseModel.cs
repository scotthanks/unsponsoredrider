﻿using BusinessObjectServices;
using BusinessObjectsLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.LedgerApi
{
    public class PaymentsResponseModel : ResponseBase
    {
        public PaymentsResponseModel(Guid userGuid)
            : base(userGuid, requiresAuthentication: true)
        {
            Response = new PaymentsResponse(userGuid);
            Response.TransactionList = new List<ClosedTransaction>();
            //Response.AccountBalance = 0;
            try
            {
                GrowerService growerService = new GrowerService();
                Grower grower = growerService.TryGetGrowerForUser(userGuid);

                LedgerTransactionService ledgerService = new LedgerTransactionService(grower.Guid, Guid.Empty, Guid.Empty, Guid.Empty);
                LedgerEntityResponse enityResponse = ledgerService.LedgerEntityRunningBalances(LookupTableValues.Code.GeneralLedgerAccount.AccountsRecievable.Guid);
                if (!enityResponse.Success)
                    throw new Exception("LedgerService.LedgerEntityBalances for Invoices failed.");

                foreach (var entity in enityResponse.EntityList)
                {
                    if (entity.Balance > 0)
                        continue;

                    ClosedTransaction trans = new ClosedTransaction();
                    trans.Amount = entity.IncreaseTotal;
                    trans.TransactionId = entity.InternalIdNumber;
                    trans.TransactionType = entity.TransactionType;
                    trans.Date = entity.TransactionDate;
                    //trans.PaymentId = entity.InternalIdNumber;
                    trans.PaymentId = entity.DecreaseExternalIdNumber;
                    trans.OrderNumber = GetOrderNumber(entity.ParentGuid);
                    Response.TransactionList.Add(trans);
                }
                enityResponse = ledgerService.LedgerEntityRunningBalances(LookupTableValues.Code.GeneralLedgerAccount.UnappliedPayments.Guid);
                if (!enityResponse.Success)
                    throw new Exception("LedgerService.LedgerEntityBalances for checks failed.");

                foreach (var entity in enityResponse.EntityList)
                {
                    if (entity.Balance > 0)
                        continue;
                    ClosedTransaction trans = new ClosedTransaction();
                    trans.Amount = -entity.IncreaseTotal;
                    trans.TransactionId = entity.ExternalIdNumber;
                    trans.TransactionType = entity.TransactionType;
                    trans.Date = entity.TransactionDate;
                    trans.PaymentId = entity.ExternalIdNumber;
                    trans.OrderNumber = GetOrderNumber(entity.ParentGuid);
                    Response.TransactionList.Add(trans);
                }

                enityResponse = ledgerService.LedgerEntityRunningBalances(LookupTableValues.Code.GeneralLedgerAccount.UnappliedCredits.Guid);
                if (!enityResponse.Success)
                    throw new Exception("LedgerService.LedgerEntityBalances for credits failed.");

                foreach (var entity in enityResponse.EntityList)
                {
                    if (entity.Balance > 0)
                        continue;
                    ClosedTransaction trans = new ClosedTransaction();
                    trans.Amount = -entity.IncreaseTotal;
                    trans.TransactionId = entity.InternalIdNumber;
                    trans.TransactionType = entity.TransactionType;
                    trans.Date = entity.TransactionDate;
                    trans.PaymentId = entity.InternalIdNumber;
                    trans.OrderNumber = GetOrderNumber(entity.ParentGuid);
                    Response.TransactionList.Add(trans);
                }

                enityResponse = ledgerService.LedgerEntityRunningBalances(LookupTableValues.Code.GeneralLedgerAccount.UnappliedCreditCardPayments.Guid);
                if (!enityResponse.Success)
                    throw new Exception("LedgerService.LedgerEntityBalances for credit cards failed.");

                foreach (var entity in enityResponse.EntityList)
                {
                    if (entity.Balance > 0)
                        continue;
                    ClosedTransaction trans = new ClosedTransaction();
                    trans.Amount = -entity.IncreaseTotal;
                    trans.TransactionId = entity.ExternalIdNumber;
                    trans.TransactionType = entity.TransactionType;
                    trans.Date = entity.TransactionDate;
                    trans.PaymentId = entity.InternalIdNumber;
                    trans.OrderNumber = GetOrderNumber(entity.ParentGuid);
                    Response.TransactionList.Add(trans);
                }

                Response.TransactionList = Response.TransactionList.OrderBy(x => x.Date).ToList();

                Response.Success = true;
            }
            catch (Exception ex)
            {
                Response.Success = false;
                Response.AddMessage(ex.Message);
            }
        }

        private string GetOrderNumber(Guid OrderGuid)
        {
            var status = new StatusObject(new Guid(), false);
            var x = new GrowerOrderDataService(status);
            var order = x.GetSummaryByGuid(OrderGuid);
            return order.OrderNo;
            //GrowerOrderTableService orderService = new GrowerOrderTableService();
            //List<DataObjectLibrary.GrowerOrder> orderList = (List<DataObjectLibrary.GrowerOrder>)orderService.GetAllActiveByGuid(OrderGuid);
            //if (orderList.Count > 0)
            //{
            //    return orderList[0].OrderNo;
            //}
            //return "";
        }

        public PaymentsResponse Response;    
    }
}
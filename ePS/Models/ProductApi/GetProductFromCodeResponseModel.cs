﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.ProductApi
{
    public class GetProductFromCodeResponseModel : ResponseBase
    {

            public GetProductFromCodeResponseModel(Guid userGuid, GetRequestModel request)
                : base(userGuid, requiresAuthentication: true)
            {
                var productService = new BusinessObjectServices.ProductService();
                var product = productService.GetProductFromCode(request.Code);

                var productList = new List<ePS.Types.ProductType>();

                var productItem = new ePS.Types.ProductType();
                productItem.ProductGuid = product.Guid;

                productList.Add(productItem);

                this.Products = productList;


            }

            public List<ePS.Types.ProductType> Products { get; set; }
    }
}
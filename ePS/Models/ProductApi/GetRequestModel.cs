﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.ProductApi
{
    public class GetRequestModel
    {

        public Guid ProgramGuid { get; set; }
        public String Code { get; set; }
    }
}
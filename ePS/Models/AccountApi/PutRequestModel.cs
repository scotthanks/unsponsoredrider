﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AccountApi
{
    public class PutRequestModel
    {
        public string CompanyName { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string ZipCode { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyTypeCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonEmail { get; set; }
        public string Password { get; set; }
        public string Password2 { get; set; }
        public string PersonPhone { get; set; }
        public string RoleCode { get; set; }
        public bool IsAuthorizedToPlaceOrders { get; set; }
        public bool SubscribeToNewsLetter { get; set; }
        public bool SubscribeToPromotions { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectsLibrary;
using BusinessObjectServices;
using Utility;
using System.IO;
 
namespace ePS.Models.AccountApi
{
    public class PutResponseModel
    {
        public List<FormErrorType> FormErrorList { get; set; }

        public bool Success
        {
            get { return !FormErrorList.Any(); }
        }

        public PutResponseModel(PutRequestModel request)
        {
            FormErrorList = new List<FormErrorType>();

            request.CompanyName = PreprocessStringField("CompanyName", request.CompanyName, isRequired: true);
            request.StreetAddress1 = PreprocessStringField("StreetAddress1", request.StreetAddress1, isRequired: true);
            request.StreetAddress2 = PreprocessStringField("StreetAddress2", request.StreetAddress2, isRequired: false);
            request.City = PreprocessStringField("City", request.City, isRequired: true);
            request.ZipCode = PreprocessStringField("ZipCode", request.ZipCode, isRequired: true);
            request.CompanyEmail = PreprocessStringField("CompanyEmail", request.CompanyEmail, isRequired: true);
            request.CompanyPhone = PreprocessStringField("CompanyPhone", request.CompanyPhone, isRequired: true);
            request.CompanyTypeCode = PreprocessStringField("CompanyTypeCode", request.CompanyTypeCode, isRequired: true);
            request.FirstName = PreprocessStringField("FirstName", request.FirstName, isRequired: true);
            request.LastName = PreprocessStringField("LastName", request.LastName, isRequired: true);
            request.PersonEmail = PreprocessStringField("Email", request.PersonEmail, isRequired: true);
            request.Password = PreprocessStringField("Password", request.Password, isRequired: true);
            request.Password2 = PreprocessStringField("Password2", request.Password, isRequired: true);
            request.PersonPhone = PreprocessStringField("PersonPhone", request.PersonPhone, isRequired: true);
            request.RoleCode = PreprocessStringField("RoleCode", request.RoleCode, isRequired: true);
            
            if (Success)
            {
                var emailRegEx =
                    new System.Text.RegularExpressions.Regex(
                        @"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");

                if (!emailRegEx.IsMatch(request.PersonEmail))
                {
                    FormErrorList.Add(new FormErrorType("The email address \"{0}\" is not a valid address.", "Email", request.PersonEmail));
                }
            }

            PersonService personService = new PersonService();

            if (Success)
            {
                if (personService.Exists(request.PersonEmail))
                {
                    FormErrorList.Add(new FormErrorType(personService.ExistsReasonMessage(request.PersonEmail), "Email", request.PersonEmail));
                }
            }

          

            if (Success)
            {
                string message;

                var returnedPerson = personService.Register
                    (
                        request.CompanyName,
                        request.StreetAddress1,
                        request.StreetAddress2,
                        request.City,
                        request.StateCode,
                        request.ZipCode,
                        request.CompanyEmail,
                        request.CompanyPhone,
                        request.CompanyTypeCode,
                        request.FirstName,
                        request.LastName,
                        request.PersonEmail,
                        request.Password,
                        request.PersonPhone,
                        request.RoleCode,
                        request.SubscribeToNewsLetter,
                        request.SubscribeToPromotions
                    );

                if (Success)
                {
                    var userProfileService = new UserProfileService();

                    bool success = userProfileService.CreateUser(request.PersonEmail, request.Password, returnedPerson.Grower.Guid, out message);

                    if (!success)
                    {
                        FormErrorList.Add(new FormErrorType(message, "Email", request.PersonEmail));
                    }
                    else
                    {
                        var userProfile = userProfileService.Get(request.PersonEmail);

                        returnedPerson.UserGuid = userProfile.UserGuid;

                        success = personService.SetUserGuid(returnedPerson.PersonGuid, userProfile.UserGuid);

                        if (!success)
                        {
                            FormErrorList.Add(new FormErrorType("Person could not be connected to the profile.", "Email"));

                           // UndoRegistrationAndRemoveProfile(personService, userProfileService, userProfile.UserName);
                        }
                        else
                        {
                            success = 
                                userProfileService.GrantRole
                                (
                                    userProfile.UserName,
                                    returnedPerson.IsAuthorizedToSeePrices,
                                    returnedPerson.IsAuthorizedToPlaceOrders,
                                    returnedPerson.IsAuthorizedToSeePrices
                                );

                            if (!success)
                            {
                                FormErrorList.Add(new FormErrorType("Unable to grant role.", "CompanyTypeCode"));
                               // UndoRegistrationAndRemoveProfile(personService, userProfileService, userProfile.UserName);   
                            }
                            if (1 == 2)
                            {
                                SendRegistrationEmail(success, request.PersonEmail, returnedPerson.IsAuthorizedToPlaceOrders);
                            }
                        }
                    }
                }
            }
        }

        

        private string PreprocessStringField(string fieldName, string field, bool isRequired)
        {
            string cleanField = field;

            if (cleanField == null)
            {
                cleanField = "";
            }

            cleanField = cleanField.Trim();

            if (isRequired && string.IsNullOrWhiteSpace(cleanField))
            {
                FormErrorList.Add(new FormErrorType("The field {0} is required", fieldName));
            }

            return cleanField;
        }
        private void SendRegistrationEmail(bool bSuccess, string sEmail, bool CanOrder)
        {
            string emailText = "Email text generation failed.";
            string emailSubject = "Email subject generation failed.";

            try
            {
                string emailHtmlPath = "";
                emailSubject = "Welcome to Green-Fuse";
                if (CanOrder)
                {
                   emailHtmlPath = HttpContext.Current.Server.MapPath("~/Views/CustomerEmail/WelcomeEmailCan.html");
                }
                else
                {
                    emailHtmlPath = HttpContext.Current.Server.MapPath("~/Views/CustomerEmail/WelcomeEmailCannotOrder.html");
                }
                emailText = File.ReadAllText(emailHtmlPath);
                bSuccess = true;
            }
            catch (Exception ex)
            {
                bSuccess = false;
                string errMsg = ex.Message;

            }

            if (sEmail.Substring(0, 4) == "test")
            {
                sEmail = new LookupService().GetConfigValue("SCOTT_EMAIL");
            }
            var emptyGuid = new Guid("00000000-0000-0000-0000-000000000000");
            var status = new StatusObject(emptyGuid, false);
            var emailService = new EmailQueueService(status);
            var bRet = emailService.AddEmailtoQueue(emailSubject, emailText, 0, "Registration","confirmations@green-fuse.com"  ,sEmail, "", "", emptyGuid, 0, 0, 0);


        }
    }
}
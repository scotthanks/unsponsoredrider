﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AccountApi
{
    public class FormErrorType
    {
        public FormErrorType(string message, string fieldName, string value = null)
        {
            FieldName = fieldName;
            Message = string.Format(message, fieldName, value);
        }

        public string FieldName { get; set; }
        public string Message { get; set; }
    }
}
﻿using System;
using Utility;

namespace ePS.Models.GrowerApi
{
    public class PutShipToAddressResponseModel : ResponseBase
    {
        public PutShipToAddressResponseModel(Guid userGuid, PutShipToAddressRequestModel shipToAddressRequest)
            :this( userGuid, shipToAddressRequest.GrowerShipToAddressGuid, shipToAddressRequest)
        {            
        }

        public PutShipToAddressResponseModel(Guid userGuid, Guid growerShipToAddressGuid, PutShipToAddressRequestModel shipToAddressRequest)
            : base(userGuid, requiresAuthentication: true)
        {
            shipToAddressRequest.GrowerShipToAddressGuid = growerShipToAddressGuid;

            if (ValidateAuthenticationAndRequest(shipToAddressRequest))
            {
                var growerBusinessService = new BusinessObjectServices.GrowerService();

                Success = growerBusinessService.UpdateShipToAddress
                (
                    userGuid: userGuid,
                    growerShipToAddressGuid: shipToAddressRequest.GrowerShipToAddressGuid,
                    addressName: shipToAddressRequest.Name,
                    streetAddress1: shipToAddressRequest.StreetAddress1,
                    streetAddress2: shipToAddressRequest.StreetAddress2,
                    city: shipToAddressRequest.City,
                    stateCode: shipToAddressRequest.StateCode,
                    zipCode: shipToAddressRequest.ZipCode,
                    phoneNumber: new PhoneNumber(shipToAddressRequest.PhoneNumber),
                    specialInstructions: shipToAddressRequest.SpecialInstructions,
                    sellerCustomerID: shipToAddressRequest.SellerCustomerID,
                    isDefault: false
                );

                if (!Success)
                {
                    AddMessage("The address was not updated.");
                }
            }
        }
    }
}
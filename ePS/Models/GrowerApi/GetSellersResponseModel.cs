﻿using System;
using System.Collections.Generic;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;


namespace ePS.Models.GrowerApi
{
    public class GetMenuResponseModel : ResponseBase
    {
        public Guid GrowerGuid { get; set; }
        public string MenuUL { get; set; }

        public GetMenuResponseModel(Guid userGuid, string email, string sellerCode, string isBeta)
            : base(userGuid, requiresAuthentication: true)
        {


            sellerCode = "GFB";
            var menuUL ="" ;
            StatusObject status = new StatusObject(userGuid,true);
            var oMenuService = new MenuService(status);
            var beta = false;
            if (isBeta == "1")
            {
                beta = true;
            }
            menuUL = oMenuService.GetMenuUL(email, sellerCode, beta);
            MenuUL = menuUL;
           
            Success = true;
        }
    
    }
}
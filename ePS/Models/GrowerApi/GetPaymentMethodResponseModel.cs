﻿using System;
using System.Collections.Generic;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;



namespace ePS.Models.GrowerApi
{
    public class GetPaymentMethodResponseModel : ResponseBase
    {
        public Guid DefaultPaymentMethodLookupGuid { get; set; }
        public List<SelectListItemType> PaymentTypeList { get; set; }

      

        public SelectListItemType SelectListItem { get; private set; }


        public GetPaymentMethodResponseModel(Guid userGuid)
            : base( userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndGuid(userGuid, "User Guid"))
            {
               

                var growerService = new GrowerService();
                Grower oGrower = growerService.TryGetGrowerForUser(userGuid);
                DefaultPaymentMethodLookupGuid = oGrower.DefaultPaymentType.Guid;

                var paymentTypeList = new List<SelectListItemType>();
                
               
                
                foreach (var paymentTypeBusinessObject in oGrower.DefaultPaymentType.SiblingList)
                {
                    var paymentType = new  SelectListItemType
                    {
                        Guid = paymentTypeBusinessObject.Guid,
                        Code = paymentTypeBusinessObject.Code,
                        Name = paymentTypeBusinessObject.Name,
                        DisplayName = paymentTypeBusinessObject.DisplayName,
                    };
                    if (paymentTypeBusinessObject.Guid == oGrower.DefaultPaymentType.Guid)
                        paymentType.IsDefault = true;

                  

                    paymentTypeList.Add(paymentType);
                }
                PaymentTypeList = paymentTypeList;
            }
            Success = true;
        }

   
    }
}

﻿using System;
using System.Collections.Generic;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;

namespace ePS.Models.GrowerApi
{
    public class GetCreditCardsResponseModel : ResponseBase
    {
        public Guid GrowerGuid { get; set; }
        public IEnumerable<CreditCardType> CreditCards { get; set; }

        public GetCreditCardsResponseModel(Guid userGuid)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndGuid(userGuid, "User Guid"))
            {
                var list = new List<CreditCardType>();
                var oGrowerService = new GrowerService();
                var oGrower = oGrowerService.TryGetGrowerForUser(userGuid);
                string sProfileID = oGrowerService.GetAuthorizationProfileId(oGrower.Guid);
                var theList = oGrowerService.GetCreditCards(userGuid, oGrower.Guid, new Guid(), new Guid());
                foreach (var oCreditCard in theList)
                {
                    list.Add(new CreditCardType()
                    {
                        Guid = oCreditCard.Guid,
                        CardDescription = oCreditCard.CardDescription,
                        CardType = oCreditCard.CardTypeCode,
                        CardTypeImageUrl = oCreditCard.CardTypeImageUrl,
                        CardNumber = oCreditCard.CardNumber ,
                        ExpirationDate = oCreditCard.ExpirationDate,
                        ProfileId = oCreditCard.PaymentProfileId,
                        IsDefault = oCreditCard.IsDefault,
                        IsSelected = oCreditCard.IsSelected
                    });
                }



               
                
                CreditCards = list;
            }
            else
            {

            };
            Success = true;
        }
    
    }
}
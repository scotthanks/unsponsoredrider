﻿using System;
using Utility;

namespace ePS.Models.GrowerApi
{
    public class PutShipToAddressDefaultResponseModel : ResponseBase
    {
       

        public PutShipToAddressDefaultResponseModel(Guid userGuid, PutShipToAddressDefaultRequestModel shipToAddressRequest)
            : base(userGuid, requiresAuthentication: true)
        {
           

            if (ValidateAuthenticationAndRequest(shipToAddressRequest))
            {
                var growerBusinessService = new BusinessObjectServices.GrowerService();

                Success = growerBusinessService.UpdateShipToAddressDefault
                (
                    userGuid: userGuid,
                    growerAddressGuid: shipToAddressRequest.AddressGuid
                   
                );

                if (!Success)
                {
                    AddMessage("The address was not updated.");
                }
            }
        }
    }
}
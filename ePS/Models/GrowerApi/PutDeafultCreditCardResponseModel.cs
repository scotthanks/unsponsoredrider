﻿using System;
using Utility;

namespace ePS.Models.GrowerApi
{
    public class PutDefaultCreditCardResponseModel : ResponseBase
    {


        public PutDefaultCreditCardResponseModel(Guid userGuid, Guid DefaultCreditCardGuid, PutDefaultCreditCardRequestModel DefaultCreditCardRequest)
            : base(userGuid, requiresAuthentication: true)
        {
            DefaultCreditCardRequest.UserGuid = userGuid;
            DefaultCreditCardRequest.DefaultCreditCardGuid = DefaultCreditCardGuid;

            if (ValidateAuthenticationAndRequest(DefaultCreditCardRequest))
            {
                var growerBusinessService = new BusinessObjectServices.GrowerService();

                Success = growerBusinessService.UpdateDefaultCreditCard
                (
                    userGuid: userGuid,
                    DefaultCreditCardGuid: DefaultCreditCardGuid
                );


                if (!Success)
                {
                    AddMessage("The default card was not updated.");
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using ePS.Types;

namespace ePS.Models.GrowerApi
{
    public class PutAllowSubstitutionsRequestModel : RequestBase
    {
        public Guid UserGuid { get; set; }
        public bool AllowSubstitutions { get; set; }
       

        public override void Validate()
        {
            if (UserGuid == Guid.Empty)
            {
                AddMessage("User Guid is required.");
            }

           
        }
    }
}
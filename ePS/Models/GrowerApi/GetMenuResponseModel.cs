﻿using System;
using System.Collections.Generic;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;


namespace ePS.Models.GrowerApi
{
    public class GetSellersResponseModel : ResponseBase
    {
        public Guid GrowerGuid { get; set; }
        public List<Seller> Sellers { get; set; }

        public GetSellersResponseModel(Guid userGuid,string email)
            : base(userGuid, requiresAuthentication: false)
        {
           
            
            var list = new List<Seller>();
            StatusObject status = new StatusObject(userGuid,true);
            var oGrowerService = new GrowerServiceNew(status);
            list = oGrowerService.GetSellers(email);
            Sellers = list;
           
            Success = true;
        }
    
    }
}
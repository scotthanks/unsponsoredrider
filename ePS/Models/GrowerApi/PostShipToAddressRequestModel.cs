﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;

namespace ePS.Models.GrowerApi
{
    public class PostShipToAddressRequestModel : RequestBase
    {
        public Guid GrowerGuid { get; set; }
        public string Name { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string SpecialInstructions { get; set; }
        public string SellerCustomerID { get; set; }

        public override void Validate()
        {
            if (GrowerGuid != Guid.Empty)
            {
                AddMessage("GrowerGuid cannot be specified.");
            }

            if (string.IsNullOrEmpty(Name))
            {
                AddMessage("Name is required.");
            }

            if (string.IsNullOrEmpty(StreetAddress1))
            {
                AddMessage("StreetAddress1 is required.");
            }

            if (string.IsNullOrEmpty(City))
            {
                AddMessage("City is required.");
            }

            if (string.IsNullOrEmpty(StateCode))
            {
                AddMessage("State is required.");
            }
            else
            {
                var stateService = new StateService();
                if (!stateService.IsValidStateCode(StateCode))
                {
                    AddMessage("The State code is not valid.");
                }   
            }

            if (string.IsNullOrEmpty(ZipCode))
            {
                AddMessage("Zip Code is required.");
            }

            var phoneNumber = new Utility.PhoneNumber(PhoneNumber);
            if (!phoneNumber.IsValid)
            {
                AddMessage("The phone number entered is not valid.");
            }
        }
    }
}
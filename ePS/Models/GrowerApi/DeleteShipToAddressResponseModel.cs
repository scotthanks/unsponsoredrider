﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.GrowerApi
{
    public class DeleteShipToAddressResponseModel : ResponseBase
    {
        public DeleteShipToAddressResponseModel(Guid userGuid, Guid growerShipToAddressGuid)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndGuid(growerShipToAddressGuid, "GrowerShipToGuid"))
            {
                var growerBusinessService = new BusinessObjectServices.GrowerService();

                Success = growerBusinessService.DeleteShipToAddress(userGuid, growerShipToAddressGuid);

                if (!Success)
                {
                    AddMessage("The address was not deleted.");
                }                
            }
        }
    }
}
﻿using System;
using Utility;

namespace ePS.Models.GrowerApi
{
    public class PutRequestLineOfCreditResponseModel : ResponseBase
    {


        public PutRequestLineOfCreditResponseModel(Guid userGuid, int RequestedLineOfCredit, PutRequestLineOfCreditRequestModel RequestLineOfCreditRequest)
            : base(userGuid, requiresAuthentication: true)
        {
            RequestLineOfCreditRequest.UserGuid = userGuid;
            RequestLineOfCreditRequest.RequestedLineOfCredit = RequestedLineOfCredit;

            if (ValidateAuthenticationAndRequest(RequestLineOfCreditRequest))
            {
                var growerBusinessService = new BusinessObjectServices.GrowerService();

                Success = growerBusinessService.RequestLineOfCredit
                (
                    userGuid: userGuid,
                    RequestedLineOfCreditAmount: RequestedLineOfCredit
                );
                RequestLineOfCreditRequest.UserGuid = userGuid;
                RequestLineOfCreditRequest.RequestedLineOfCredit = RequestedLineOfCredit;
                
                if (!Success)
                {
                    AddMessage("The request was not saved.");
                }
            }
            else
            { Success = true; }
        }
    }
}
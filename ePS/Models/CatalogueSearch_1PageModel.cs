﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using BusinessObjectsLibrary;
using BusinessObjectServices;

namespace ePS.Models
{
    public class CatalogueSearch_1PageModel
    {
        public CatalogueSearch_1PageModel(string productCategoryKey)
        {
            productCategoryKey = productCategoryKey ?? string.Empty;
            productCategoryKey = productCategoryKey.Trim();

            var userGuid = new Guid();
            var status = new StatusObject(userGuid, false);
            var service = new MenuService(status);

            var menuData = service.GetMenuData();

            this.Count = menuData.ProgramTypes.Count;

            var catalogList = new List<Catalogue>();

            foreach (var programType in menuData.ProgramTypes)
            {
                var catalog = new Catalogue()
                {
                    Guid = programType.Guid,
                    Key = programType.Code,
                    Name = programType.Name
                };

                if (programType.Code == productCategoryKey)
                {
                    this.SelectedCategory = catalog;
                }

                var activeProductForms = new List<Guid>();
                foreach (var productForm in programType.ActiveProductForms)
                {
                    activeProductForms.Add(productForm.Guid);
                }

                catalog.FormsAvailable = activeProductForms.ToArray();

                catalogList.Add(catalog);
            }

            this.CategoryCollection = catalogList.ToArray();

            this.ProductFormCollection = menuData.ProductForms.ToArray();
        }
        public Catalogue SelectedCategory { get; set; }
        public ProductFormCategory SelectedProductFormCategory { get; set; }

        public int Count { get; set; }
        public IEnumerable<Catalogue> CategoryCollection { get; set; }
        public IEnumerable<ProductFormCategory> ProductFormCollection { get; set; }  //Holds the ProductForms for Selected Catalogue
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.ShipWeekApi
{
    public class GetShipWeekResponseModel : ResponseBase
    {

        public GetShipWeekResponseModel(Guid userGuid, GetShipWeekRequestModel request)
                : base(userGuid, requiresAuthentication: true)
        {

            var shipWeekService = new BusinessObjectServices.ShipWeekService();
            var shipWeek = shipWeekService.GetShipWeek(request.ShipWeek, request.ShipYear);
           

            var list = new List<ePS.Types.ShipWeekType>();
            var item = new ePS.Types.ShipWeekType();
            item.ShipWeekGuid = shipWeek.Guid;
              
            list.Add(item);

            this.ShipWeeks = list;
            
        }

        public List<ePS.Types.ShipWeekType> ShipWeeks { get; set; }
    }
}
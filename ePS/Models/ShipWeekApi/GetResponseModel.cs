﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.ShipWeekApi
{
    public class GetResponseModel : ResponseBase
    {

        public GetResponseModel(Guid userGuid, GetRequestModel request)
            :base(userGuid, requiresAuthentication: true)
        {

            var shipWeekService = new BusinessObjectServices.ShipWeekService();
            var shipWeekList = shipWeekService.GetAllShipWeeks(request.ShipWeekBegin, request.ShipYearBegin, request.ShipWeekEnd, request.ShipYearEnd);

            var list = new List<ePS.Types.ShipWeekType>();
            foreach (var shipWeek in shipWeekList)
            {
                var item = new ePS.Types.ShipWeekType
                {
                    ShipWeekGuid = shipWeek.Guid,
                    Week = shipWeek.Week,
                    Year = shipWeek.Year
                    //Monday = shipWeek.MondayDate
                };

                list.Add(item);
            }

            this.ShipWeeks = list;

        }

        //public IEnumerable<ePS.Types.ShipWeekType> ShipWeeks { get; set; }
        public List<ePS.Types.ShipWeekType> ShipWeeks { get; set; }
    }
}
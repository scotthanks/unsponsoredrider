﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Web;
using System.Web.Http;
using System.Web.Mvc;


using Microsoft.WindowsAzure.Storage.Table;

using System.Configuration;
using ePS.Types;
using BusinessObjectsLibrary;
using BusinessObjectServices;

namespace ePS.Models
{
    public class LogUserAction
    {

        public void WriteToTable(HttpRequest request, HttpResponse response, string tableName)
        {
            
           
            
            try
            {
                // Create log Entity to store
                var logEntry = new LogUserActionType(request, response);
                var status = new StatusObject(logEntry.UserGuid,false);


                var pageHit = new PageHit();
                pageHit.AllowsCookies = logEntry.AllowsCookies;
                pageHit.BrowserName = logEntry.BrowserName;
                pageHit.BrowserPlatform = logEntry.BrowserPlatform;
                pageHit.BrowserVersion = logEntry.BrowserVersion;
                
                string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + request.ApplicationPath.TrimEnd('/') + "/";
                pageHit.Environment = baseUrl;
                pageHit.HttpMethod = logEntry.HttpMethod;
                pageHit.IPAddress = logEntry.UserHostAddress;
                pageHit.IsAuthenticated = logEntry.IsAuthenticated;
                pageHit.IsCrawler = logEntry.IsCrawler;
                pageHit.IsLocal = logEntry.IsLocal;
                pageHit.IsMobileDevice = logEntry.IsMobileDevice;
                pageHit.PartitionKey = logEntry.PartitionKey;
                pageHit.QueryString = logEntry.QueryString;
                pageHit.RawUrl = logEntry.RawUrl;
                pageHit.UrlReferrer = logEntry.UrlReferrer;
                pageHit.UserAgent = logEntry.UserAgent;
                pageHit.UserHostName = logEntry.UserHostName;
                pageHit.UserName = logEntry.UserName;



                var service = new PageHitService(status);
                service.AddPageHit(pageHit);

                //---------------------Old way to write to Table Storage
               // const string CONNECTION_STRING_NAME = "StorageConnectionString";
                //// Get Cloud Storage Account from Connection String in Web.Config
                //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings[CONNECTION_STRING_NAME].ConnectionString);

                //// Create the table client.
                //CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

                //// Instantiate Table
                //CloudTable table = tableClient.GetTableReference(tableName);
                
                //// Create the table if it doesn't exist.
                //table.CreateIfNotExists();

                //// Create the TableOperation that inserts the customer entity.
                //TableOperation insertOperation = TableOperation.Insert(logEntry);

                //// Execute the insert operation.
                //table.Execute(insertOperation);

            }
            catch (Exception e)
            {
                var s = e.Message;
            }

        }
        
        public class LogUserActionType: TableEntity {
            const string COOKIE_NAME_SESSION = "epsSession";

            public LogUserActionType(HttpRequest request, HttpResponse response) {

                this.UserName = string.Empty;       //not null
                if (request.IsAuthenticated)
                {
                    this.IsAuthenticated = true;
                    this.UserName = System.Web.HttpContext.Current.User.Identity.Name;

                    using (UsersContext db = new UsersContext())
                    {
                        UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == this.UserName.ToLower());
                        this.UserGuid = user.UserGuid;
                    }
                }

                HttpCookieCollection requestCookiesCollection = response.Cookies;
                HttpCookie stateCookie = requestCookiesCollection[COOKIE_NAME_SESSION] ?? null;
                var partitionKey = stateCookie.Value;
                if (partitionKey != null && partitionKey.Length == 36)
                {
                    this.PartitionKey = partitionKey;
                }
                else
                {
                    this.PartitionKey = Guid.NewGuid().ToString().ToLower();
                };

                this.RowKey = Guid.NewGuid().ToString().ToLower();
                //this.RowKey = DateTime.Now.ToString("HH:mm:ss tt");

                this.Url = request.Url.ToString();
                this.RawUrl = request.RawUrl;
                this.QueryString = request.QueryString.ToString();
                this.UrlReferrer = request.UrlReferrer.ToString();
                
                var browser = request.Browser;
                this.IsCrawler = browser.Crawler;
                this.AllowsCookies = browser.Cookies;
                this.BrowserName = browser.Browser;
                this.BrowserVersion = browser.MajorVersion;
                this.IsMobileDevice = browser.IsMobileDevice;
                this.BrowserPlatform = browser.Platform;

                this.IsLocal = request.IsLocal;
                this.HttpMethod = request.HttpMethod;
                this.UserHostAddress = request.UserHostAddress;
                this.UserAgent = request.UserAgent;
                this.UserHostName = request.UserHostName;
            }

            public bool IsAuthenticated { get; set; }
            public string UserName { get; set; }
            public Guid UserGuid { get; set; }

            public string Url { get; set; }             //Uri type doesnt write to table, so converted to string
            public string RawUrl { get; set; }
            public string QueryString { get; set; }
            public string UrlReferrer { get; set; }     //Uri type doesnt write to table, so converted to string
            public string UserAgent { get; set; }
            
            public bool IsCrawler { get; set; }
            public bool IsMobileDevice { get; set; }
            public bool AllowsCookies { get; set; }
            public string BrowserName { get; set; }
            public int BrowserVersion { get; set; }
            public string BrowserPlatform { get; set; }
            
            public bool IsLocal { get; set; }
            public string HttpMethod { get; set; }
            public string UserHostAddress { get; set; }
            public string UserHostName { get; set; }
        }

    }
}
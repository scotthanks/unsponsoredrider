﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models
{
    [Obsolete]
    public class ProductOrderApiOrderedItemType
    {
        public Guid productGuid { get; set; }
        public int OrderQuantity { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.CardChargeApi
{
    public class PutResponseModel
    {
        public PutResponseModel(PutRequestModel request)
        {
            var charge = new BusinessObjectServices.CardCharge();

            charge.CardNumber = request.CardNumber;
            charge.Expires = request.Expires;
            charge.Amount = request.Amount;
            charge.Description = request.Description;

            var service = new BusinessObjectServices.CardChargeService(charge);
            var response = service.Response;
            if (response != null) {
                this.Approved = response.Approved;
                this.Code = response.Code;
                this.Message = response.Message;
            }

    }

        public bool Approved { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }

    }
}
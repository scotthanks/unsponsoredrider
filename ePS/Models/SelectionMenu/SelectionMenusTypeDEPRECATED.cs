﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using BusinessObjectsLibrary;

namespace ePS.Models.SelectionMenu
{
    public class SelectionMenusTypeDEPRECATED
    {
        
        //public ProgramType ProgramType { get; set; }
        //public IEnumerable<ProductForm> ProductFormCategories { get; set; }


        public IEnumerable<SelectionCategoryType> Categories { get; set; }
        public IEnumerable<SelectionFormType> Forms { get; set; }



    }
}
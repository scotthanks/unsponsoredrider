﻿using System;
using System.Collections.Generic;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;



namespace ePS.Models.Grower2Api
{
    public class GetGrowerOrderSummariesResponseModel : ResponseBase
    {

        public int OrderCount { get; set; }
        public List<GrowerOrderSummary> OrderSummaries { get; set; }


        public GetGrowerOrderSummariesResponseModel(Guid userGuid, GetGrowerOrderGetSummariesRequestModel request)
            : base( userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndGuid(userGuid, "User Guid"))
            {
                string sellerCode = request.SellerCode;
                sellerCode = "GFB";
                string adjShipWeek = request.ShipWeekCode;
                if (request.ShipWeekCode.Length > 6)
                {
                    adjShipWeek = request.ShipWeekCode.Substring(3, 4) + request.ShipWeekCode.Substring(0, 2);
                }

                var service = new GrowerService();
                var oSummaries = service.GetGrowerOrderSummaries(userGuid, adjShipWeek, sellerCode);

                var oSumamryList = new List<GrowerOrderSummary>();

                OrderCount = oSummaries.Count;
                OrderSummaries = oSummaries;
                Success = true;
            }
        }

   
    }
}

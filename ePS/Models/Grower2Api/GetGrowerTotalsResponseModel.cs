﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;
using AdminAppApiLibrary;
using BusinessObjectsLibrary;

namespace ePS.Models.Grower2Api
{
    public class GetGrowerTotalsResponseModel : ResponseBase
    {
        public GetGrowerTotalsResponseModel(Guid userGuid, GetGrowerTotalsRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                int Carted = 0;
                int Uncarted = 0;        
                var service = new BusinessObjectServices.GrowerOrderService();



                if (userGuid != Guid.Empty)
                   {
                       //user found 
                       bool bRet = false;
                       bRet = service.GrowerGetTotals(userGuid, request.ProductFormCategoryCode, request.ProgramTypeCode, out Carted, out Uncarted);
                      
                       
                   }
                   else
                   {
                       //No user found
                       
                   }

                   this.Carted = Carted;
                   this.Uncarted = Uncarted;
                   this.Total = Carted + Uncarted;

                
                this.Success = true;
                            
                
            }
        }
        
        public int Carted { get; private set; }
        public int Uncarted { get; private set; }
        public int Total { get; private set; }
        
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;

namespace ePS.Models.Grower2Api
{
    public class GetGrowerOrderGetSummariesRequestModel : RequestBase
    {

        public string ShipWeekCode { get; set; }
        public string SellerCode { get; set; }
       

        public override void Validate()
        {
          

            if (string.IsNullOrEmpty(ShipWeekCode))
            {
                AddMessage("ShipWeekCode is required.");
            }
            if (string.IsNullOrEmpty(SellerCode))
            {
                AddMessage("SellerCode is required.");
            }

        }
    }
}
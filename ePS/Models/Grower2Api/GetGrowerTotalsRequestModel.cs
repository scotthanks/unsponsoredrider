﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataServiceLibrary;

namespace ePS.Models.Grower2Api
{
    public class GetGrowerTotalsRequestModel : RequestBase
    {
        public string ProgramTypeCode { get; set; }
        public string ProductFormCategoryCode { get; set; }
        
       

        public override void Validate()
        {
            
            if ( ProgramTypeCode == "" || ProductFormCategoryCode == "")
            {


                AddMessage("ProgramTypeCode, and ProductFormCategoryCode are required");
                
            };

           
        }
    }
}
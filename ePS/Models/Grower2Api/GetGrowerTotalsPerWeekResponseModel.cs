﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;
using AdminAppApiLibrary;
using BusinessObjectsLibrary;

namespace ePS.Models.Grower2Api
{
    public class GetGrowerTotalsPerWeekResponseModel : ResponseBase
    {
        public GetGrowerTotalsPerWeekResponseModel(Guid userGuid, GetGrowerTotalsRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                WeekSummary = new List<GrowerWeekSummary>();    
                var service = new BusinessObjectServices.GrowerOrderService();



                if (userGuid != Guid.Empty)
                   {
                       //user found 
                       WeekSummary = service.GrowerGetTotalsPerWeek(userGuid, request.ProductFormCategoryCode, request.ProgramTypeCode);       
                   }
                   else
                   {
                       //No user found
                       
                   }

                

                
                this.Success = true;
                            
                
            }
        }
        public List<GrowerWeekSummary> WeekSummary { get; private set; }
        
        
    }

}
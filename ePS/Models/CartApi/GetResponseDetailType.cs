﻿using ePS.Types;

namespace ePS.Models.CartApi
{
    public class GetResponseDetailType
    {
        public GrowerOrderType GrowerOrder { get; set; }
    }
}
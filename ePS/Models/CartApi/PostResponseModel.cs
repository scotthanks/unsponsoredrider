﻿using System;
using System.Collections.Generic;
using BusinessObjectServices;

namespace ePS.Models.CartApi
{
    public class PostResponseModel : ResponseBase
    {
        public PostResponseModel(Guid userGuid, PostRequestModel request)
            : base( userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                var growerOrderService = new GrowerOrderService();
                var growerOrderGuid = new Guid();
                var statusList = new List<OrderLineStatusType>();

                foreach (CartOrderLineUpdate orderLineUpdate in request.CartOrderLineUpdates)
                {
                    var service = new GrowerOrderService();
                    var returnData = service.OrderLineUpdate(userGuid, orderLineUpdate.Guid, orderLineUpdate.Quantity, 0, "Grower", out growerOrderGuid);
                    

                    statusList.Add
                        (
                            new OrderLineStatusType
                            {
                                OrderLineGuid = orderLineUpdate.Guid,
                                Quantity = returnData.Quantity,
                                Success = returnData.Success,
                                Message = returnData.Message
                            }
                        );
                    this.AddMessage( returnData.Message);
                }
                var x = this.Message;
                OrderLineStatuses = statusList.ToArray();
            }
        }

        public OrderLineStatusType[] OrderLineStatuses { get; set; }
    }
}
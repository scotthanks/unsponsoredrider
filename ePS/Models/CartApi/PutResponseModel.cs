﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;

namespace ePS.Models.CartApi
{
    public class PutResponseModel : ResponseBase
    {
        public PutResponseModel(Guid userGuid, Guid orderGuid)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndGuid(orderGuid, "OrderGuid"))
            {
                var service = new GrowerOrderService();

                //TODO: There is no request object and so no user code. What if this is called by the admin app? We will need a userCode then.
                var returnData = service.PlaceOrder(userGuid, userCode: null, growerOrderGuid: orderGuid);

                ProductsAddedToCart = returnData.OrderLinesChanged;
                PrecartOrderLinesDeleted = returnData.OrderLinesDeleted;

                OrderGuid = orderGuid;

                this.Success = ProductsAddedToCart > 0;                
            }
        }

        public Guid OrderGuid { get; set; }

        public int ProductsAddedToCart { get; set; }
        public int PrecartOrderLinesDeleted { get; set; }
    }
}
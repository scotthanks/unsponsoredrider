﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.CartApi
{
    public class GetRequestSummaryModel : RequestBase
    {
        //TODO: Get rid of this enum (replace with Lookup Codes).
        public enum RequestTypeEnum
        {
            SummaryData,
            //MyShoppingCartOrders,
            //MyPlacedOrders,
            //MyShippedOrders,
            //MyCancelledOrders,
            //MyOrders,
            AllShoppingCartOrders,
            AllPlacedOrders,
            AllShippedOrders,
            AllCancelledOrders,
            AllOrders,
        }

        public string RequestType { get; set; }

        public override void Validate()
        {
            RequestTypeEnum requestTypeEnum;

            if (!Enum.TryParse(RequestType, out requestTypeEnum))
            {
                AddMessage(string.Format( "RequestType {0} is not implemented.", RequestType));                
            }
        }
    }
}
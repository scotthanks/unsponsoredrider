﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;

namespace ePS.Models.CartApi
{
    public class GetResponseSummaryModel : ResponseBase
    {
        public enum RequestTypeEnum
        {
            SummaryData,
         //   MyShoppingCartOrders,
         //   MyPlacedOrders,
            //MyShippedOrders,
         //   MyCancelledOrders,
         //   MyOrders,
            AllShoppingCartOrders,
            AllPlacedOrders,
            AllShippedOrders,
            AllCancelledOrders,
            AllOrders,
        }

        public GetResponseSummaryModel(Guid userGuid, GetRequestSummaryModel request)
            : base( userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                //Retrieve Data from Data Server
                var orderSummaryService = new OrderSummaryService();
                List<OrderSummary> orderSummaryList;

                RequestTypeEnum requestType;
                bool valid = Enum.TryParse(request.RequestType, out requestType);
                if (!valid)
                {
                    throw new ApplicationException(string.Format("Request type {0} is not valid.", request.RequestType));
                }

                //if (requestType == RequestTypeEnum.SummaryData)
                //    requestType = RequestTypeEnum.MyShoppingCartOrders;

                switch (requestType)
                {
                    //case RequestTypeEnum.MyShoppingCartOrders:
                    //    orderSummaryList = orderSummaryService.GetMyShoppingCartOrders(userGuid);
                    //    break;
                    //case RequestTypeEnum.MyPlacedOrders:
                    //    orderSummaryList = orderSummaryService.GetMyOpenOrders(userGuid);
                    //    break;
                    //case RequestTypeEnum.MyShippedOrders:
                    //    orderSummaryList = orderSummaryService.GetMyShippedOrders(userGuid);
                    //    break;
                    //case RequestTypeEnum.MyCancelledOrders:
                    //    orderSummaryList = orderSummaryService.GetMyCancelledOrders(userGuid);
                    //    break;
                    //case RequestTypeEnum.MyOrders:
                    //    orderSummaryList = orderSummaryService.GetMyOrders(userGuid);
                    //    break;
                    case RequestTypeEnum.AllShoppingCartOrders:
                        orderSummaryList = orderSummaryService.GetAllShoppingCartOrders(userGuid);
                        break;
                    case RequestTypeEnum.AllPlacedOrders:
                        orderSummaryList = orderSummaryService.GetAllOpenOrders(userGuid);
                        break;
                    case RequestTypeEnum.AllShippedOrders:
                        orderSummaryList = orderSummaryService.GetAllShippedOrders(userGuid);
                        break;
                    case RequestTypeEnum.AllCancelledOrders:
                        orderSummaryList = orderSummaryService.GetAllCancelledOrders(userGuid);
                        break;
                    case RequestTypeEnum.AllOrders:
                        orderSummaryList = orderSummaryService.GetAllOrders(userGuid);
                        break;
                    default:
                        throw new ApplicationException(string.Format("Request type {0} is not implemented.", requestType.ToString()));
                        
                }

                //Populate List
                var cartSummaryList = new List<OrderSummaryType>();
                foreach (OrderSummary item in orderSummaryList)
                {
                    var summaryItem = new OrderSummaryType
                    {
                        OrderGuid = item.OrderGuid,
                        OrderNo = item.OrderNo,
                        ProductFormName = item.ProductFormCategory.Name,
                        ShipWeekString = item.ShipWeek.ShipWeekString,
                        ProgramCategoryName = item.ProgramTypeName,
                        OrderQty = item.OrderQty,
                        CustomerPo = item.CustomerPoNo,
                        PromotionalCode = item.PromotionalCode,
                        OrderDescription = item.OrderDescription,
                        CustomerCode = item.CustomerCode,
                        CustomerName = item.GrowerName,
                        PhoneNumber = item.PhoneNumber,
                        GrowerOrderStatusCode = item.GrowerOrderStatusCode,
                        LowestSupplierOrderStatusCode = item.LowestSupplierOrderStatusCode,
                        LowestOrderLineStatusCode = item.LowestOrderLineStatusCode,
                        PersonWhoPlacedOrder = new GetResponsePersonModel(item.PersonWhoPlacedOrder).PersonType,
                        InvoiceNo = item.InvoiceNo
                    };

                    cartSummaryList.Add(summaryItem);
                }

                //ToDo: Sort by ShipWeekString, ProductFormName

                //List<SomeClass>() a;
                //List<SomeClass> b = a.OrderBy(x => x.x).ThenBy(x => x.y).ToList();

                //List<GetResponseSummaryType> sortedCartSummaryList = cartSummaryList.OrderBy(x => x.ShipWeekString).ThenBy(x => x.ProgramCategoryName).ThenBy(x => x.ProductFormName).ToList();

                List<OrderSummaryType> sortedCartSummaryList = cartSummaryList.OrderBy(x => x.OrderNo).ToList();
                SummaryData = sortedCartSummaryList;

                Success = true;
            }
        }

        public IEnumerable<OrderSummaryType> SummaryData { get; set; }
    }
}
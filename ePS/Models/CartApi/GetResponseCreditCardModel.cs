﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;
using BusinessObjectsLibrary;

namespace ePS.Models.CartApi
{
    public class GetResponseCreditCardModel
    {
        public GetResponseCreditCardModel(BusinessObjectsLibrary.CreditCard creditCard)
        {
            CreditCard = new CreditCardType()
                {
                    Guid = creditCard.Guid,
                    CardDescription = creditCard.CardDescription,
                    CardType = creditCard.CardTypeCode,
                    CardTypeImageUrl = creditCard.CardTypeImageUrl,
                    CardNumber = creditCard.CardNumber,
                    ExpirationDate = creditCard.ExpirationDate,
                    ProfileId = creditCard.PaymentProfileId,
                    IsDefault = creditCard.IsDefault,
                };
        }

        public CreditCardType CreditCard { get; private set; }
    }
}
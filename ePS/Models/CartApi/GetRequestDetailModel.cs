﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataServiceLibrary;

namespace ePS.Models.CartApi
{
    public class GetRequestDetailModel : RequestBase
    {
        public Guid OrderGuid { get; set; }
        public string RequestType { get; set; }
      

        public override void Validate()
        {
            if (string.IsNullOrEmpty(RequestType))
            {
                AddMessage("RequestType is required.");
            }
            else
            {
                switch (RequestType)
                {
                    case "DetailData":
                        if (OrderGuid == Guid.Empty)
                        {
                            AddMessage("The OrderGuid is required.");
                        }
                        break;
                    case "SummaryData":
                        break;
                    default:
                        AddMessage(string.Format("RequestType \"{0}\" is not implemented.", RequestType));
                        break;
                }
            }

            //if (!string.IsNullOrEmpty(UserHandle))
            //{
            //    AddMessage("The UserHandle is no longer used.");
            //}
        }
    }
}
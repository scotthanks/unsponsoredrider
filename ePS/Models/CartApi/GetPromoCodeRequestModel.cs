﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;

namespace ePS.Models.CartApi
{
    public class GetPromoCodeRequestModel : RequestBase
    {

        public string PromoCode { get; set; }
        public string PromoName { get; set; }
       

        public override void Validate()
        {


            if (string.IsNullOrEmpty(PromoCode))
            {
                AddMessage("PromoCode is required.");
            }

        }
    }
}
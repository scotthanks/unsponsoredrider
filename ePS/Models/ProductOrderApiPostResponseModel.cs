﻿using System;

namespace ePS.Models
{
    public class ProductOrderApiPostResponseModel : ResponseBase
    {
        public ProductOrderApiPostResponseModel(Guid userGuid, ProductOrderApiPostRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                var orderService = new BusinessObjectServices.GrowerOrderService();

                var shipWeekService = new BusinessObjectServices.ShipWeekService();
                var shipWeek = shipWeekService.ParseShipWeekString(request.ShipWeekString);

                //TODO: Return availability data.
                //var availability = 
                orderService.UpdateOrderQuantity(userGuid, request.UserCode, shipWeek.Code, request.ProductGuid, request.OrderQty);

                Success = true;
            }        
        }
    }
}

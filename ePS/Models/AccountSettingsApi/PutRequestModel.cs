﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AccountSettingsApi
{
    public class PutRequestModel : RequestBase
    {
        public override void Validate()
        {
            if (string.IsNullOrEmpty(this.AccountSettings.CompanyName))
            {
                this.AddMessage("Company Name is required.");
                //ToDo: validate the data submitted 

            }
        }

        public AccountSettingsType AccountSettings { get; set; }

    }
}
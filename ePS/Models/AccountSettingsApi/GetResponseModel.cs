﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataServiceLibrary;
using BusinessObjectServices;
using BusinessObjectsLibrary;
namespace ePS.Models.AccountSettingsApi
{
    public class GetResponseModel : ResponseBase
    {
        public GetResponseModel(Guid userGuid, GetRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            if (!ValidateAuthenticationAndRequest(request)) return;
            
            //initialize Account Settings
            this.AccountSettings = new AccountSettingsType();

            try
            {
                var personService = new BusinessObjectServices.PersonService();
                Person oPerson = personService.GetPersonFromUser(userGuid);
                var growerService = new GrowerService();
                Grower oGrower = growerService.TryGetGrowerForUser(userGuid);

                //ToDo: Populate Account Settings
                AccountSettings.Guid = userGuid;
                AccountSettings.City = oGrower.Address.City;
                AccountSettings.CompanyEmail = oGrower.EMail ;
                AccountSettings.CompanyName = oGrower.GrowerName;
                AccountSettings.CompanyPhone = oGrower.PhoneNumber.FormattedPhoneNumber;
                AccountSettings.CompanyRole = "what is this?";
                var lookupService = new DataServiceLibrary.LookupTableService();  //Code/OrderLineStatus/Pending
                
                AccountSettings.CompanyType = lookupService.GetByGuid(oGrower.GrowerTypeLookupGuid).Code;
                AccountSettings.Country = oGrower.Address.State.Country.CountryCode;
                AccountSettings.FirstName = oPerson.FirstName;
                AccountSettings.IsAccountAdministrator = true; //--To Do
                AccountSettings.LastName = oPerson.LastName;
                AccountSettings.PersonalEmail = oPerson.Email;
                AccountSettings.PersonalPhone = oPerson.Phone.FormattedPhoneNumber;
                AccountSettings.PostalCode = oGrower.Address.ZipCode;
                AccountSettings.State = oGrower.Address.State.StateCode;
                AccountSettings.StreetAddress = oGrower.Address.StreetAddress1;
                AccountSettings.SubscribeToNewsLetter = true; //To do
                AccountSettings.SubscribeToPromotions = true; //To do
               


                this.Success = true;
            }
            catch (Exception ex)
            {
                this.Success = false;
                this.AddMessage(ex.Message);
            }
        }

        public AccountSettingsType AccountSettings { get; set; }
    }
}
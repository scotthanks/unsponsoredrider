﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AccountSettingsApi
{
    public class PostResponseModel : ResponseBase
    {
        public PostResponseModel(Guid userGuid, PostRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            if (!ValidateAuthenticationAndRequest(request)) return;
            

            //Note: Response Model should
            //  1. validate incoming data in request object, which may include modifying such as removing trailing spaces from a name
            //  2. Write incoming data
            //  3. Retrieve the updated data
            //  4. Return retrieved updated data

            


            this.AccountSettings = new AccountSettingsType();

            try
            {
                //ToDo: Call service(s) to write data

                //ToDo: Populate Account Settings
                AccountSettings.CompanyName = request.AccountSettings.CompanyName; //Something to show plumbing works

                this.Success = true;
            }
            catch (Exception ex)
            {
                this.Success = false;
                this.AddMessage(ex.Message);
            }
        }

        public AccountSettingsType AccountSettings { get; set; }

    }
}
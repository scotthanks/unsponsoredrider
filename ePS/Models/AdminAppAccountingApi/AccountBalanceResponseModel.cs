﻿using AdminAppApiLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AdminAppAccountingApi
{
    public class AccountBalanceResponseModel : ResponseBase
    {
        public AccountBalanceResponseModel(Guid userGuid, AccountBalanceRequest request)
            : base(userGuid, requiresAuthentication: true)
        {
            AccountTypes accountType = (AccountTypes)System.Enum.Parse(typeof(AccountTypes), request.AccountType);
            Response = new AccountBalanceResponse();
            LedgerTransactionService ledgerService = new LedgerTransactionService(request.GrowerGuid, Guid.Empty, Guid.Empty, Guid.Empty);
            Guid GLAccountLookupGuid = Guid.Empty;
            if (accountType == AccountTypes.AccountsRecievable)
                GLAccountLookupGuid = LookupTableValues.Code.GeneralLedgerAccount.AccountsRecievable.Guid;
            else if (accountType == AccountTypes.UnappliedPayments)
                GLAccountLookupGuid = LookupTableValues.Code.GeneralLedgerAccount.UnappliedPayments.Guid;
            else if (accountType == AccountTypes.UnappliedCredits)
                GLAccountLookupGuid = LookupTableValues.Code.GeneralLedgerAccount.UnappliedCredits.Guid;
            LedgerAccountBalance accountBalance = ledgerService.GetAccountBalance(GLAccountLookupGuid);
            Response.AccountBalance = accountBalance.AccountBalance;
            if(accountType == AccountTypes.AccountsRecievable)
                Response.AccountBalance = -Response.AccountBalance;
            Response.CreditTotal = accountBalance.CreditTotal;
            Response.DebitTotal = accountBalance.DebitTotal;
            Response.Success = accountBalance.Success;
            Response.Message = accountBalance.Message;
        }

        public AccountBalanceResponse Response;
    }
}
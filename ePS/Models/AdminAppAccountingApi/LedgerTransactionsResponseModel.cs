﻿using AdminAppApiLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AdminAppAccountingApi
{
    public class LedgerTransactionsResponseModel : ResponseBase
    {
        public LedgerTransactionsResponseModel(Guid userGuid, LedgerTransactionsRequest request)
            : base(userGuid, requiresAuthentication: true)
        {
            Response = new LedgerTransactionsResponse();
            try
            {
                Response.LedgerEntryList = new List<LedgerEntryType>();
                LedgerTransactionService ledgerService = new LedgerTransactionService(request.GrowerGuid, Guid.Empty, request.OrderGuid, Guid.Empty);

                LookupTableValues.CodeValues.GeneralLedgerAccountValues accountLookupService = new LookupTableValues.CodeValues.GeneralLedgerAccountValues();
                LookupTableValues.CodeValues.GeneralLedgerEntryTypeValues entryTypeLookupService = new LookupTableValues.CodeValues.GeneralLedgerEntryTypeValues();
                TransactionRequestType requestType = (TransactionRequestType)System.Enum.Parse(typeof(TransactionRequestType), request.TransactionRequestType);
                List<LedgerEntry> entryList = null;
                if (requestType == TransactionRequestType.InvoicePaymentsFromUnappliedPayments)
                {
                    ledgerService.AssetParentGuid = request.OrderGuid;
                    entryList = ledgerService.GetOrderTransactions(accountLookupService.AccountsRecievable.Guid, 
                        accountLookupService.UnappliedPayments.Guid,
                        entryTypeLookupService.Credit.Guid);
                }
                else if (requestType == TransactionRequestType.InvoicePaymentFromUnappliedCredits)
                {
                    ledgerService.AssetParentGuid = request.OrderGuid;
                    entryList = ledgerService.GetOrderTransactions(accountLookupService.AccountsRecievable.Guid,
                        accountLookupService.UnappliedCredits.Guid,
                        entryTypeLookupService.Credit.Guid);
                }

                else if (requestType == TransactionRequestType.UnappliedPaymentsFromGrowerCheck)
                {
                    // gets all unapplied payments for this grower not limited by parent guid
                    ledgerService.LiabilityParentGuid = Guid.Empty;
                    entryList = ledgerService.GetOrderTransactions(accountLookupService.Cash.Guid,
                        accountLookupService.UnappliedPayments.Guid, 
                        entryTypeLookupService.Credit.Guid);
                }
                else if (requestType == TransactionRequestType.UnappliedCreditsFromCreditMemo)
                {
                    // gets all unapplied credits for this grower not limited by parent guid
                    ledgerService.LiabilityParentGuid = Guid.Empty;
                    entryList = ledgerService.GetOrderTransactions(accountLookupService.Cash.Guid,
                        accountLookupService.UnappliedCredits.Guid,
                        entryTypeLookupService.Credit.Guid);
                }
                else
                    throw new Exception("TransactionRequestType not supported");

                if(entryList == null)
                    throw new Exception("ledgerService.GetOrderTransactions failed");

                foreach (var entry in entryList)
                {
                    LedgerEntryType payment = new LedgerEntryType();

                    payment.Amount = entry.Amount;
                    payment.TransactionDate = entry.TransactionDateString;
                    payment.TransactionGuid = entry.TransactionGuid;

                    payment.AssetExternalIdNumber = entry.AssetExternalIdNumber;
                    payment.AssetInternalIdNumber = entry.AssetInternalIdNumber;
                    payment.AssetParentGuid = entry.AssetParentGuid;

                    payment.LiablityLedgerExternalIdNumber = entry.LiablityLedgerExternalIdNumber;
                    payment.LiablityLedgerInternalIdNumber = entry.LiablityLedgerInternalIdNumber;
                    payment.LiablityParentGuid = entry.LiablityParentGuid;

                    Response.LedgerEntryList.Add(payment);
                }
                Response.Success = true;
            }
            catch (Exception ex)
            {
                Response.Success = false;
                Response.Message = ex.Message;
            }
        }

        public LedgerTransactionsResponse Response;
    }
}
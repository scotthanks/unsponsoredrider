﻿using AdminAppApiLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AdminAppAccountingApi
{
    public class SaveInvoiceDocumentResponseModel : ResponseBase
    {
        public SaveInvoiceDocumentResponseModel(Guid userGuid, SaveInvoiceDocumentRequest request)
            : base(userGuid, requiresAuthentication: true)
        {
            Response = new SaveInvoiceDocumentResponse();
            Response.Success = true;
        }

        public SaveInvoiceDocumentResponse Response { get; set; }
    }
}
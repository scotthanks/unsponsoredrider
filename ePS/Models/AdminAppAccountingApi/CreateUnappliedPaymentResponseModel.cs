﻿using AdminAppApiLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ePS.Models.AdminAppAccountingApi
{
    public class CreateUnappliedPaymentResponseModel : ResponseBase
    {
        public CreateUnappliedPaymentResponseModel(Guid userGuid, CreateUnappliedPaymentRequest request)
            : base(userGuid, requiresAuthentication: true)
        {
            Response = new CreateUnappliedPaymentResponse();
            try
            {
                LedgerTransactionService ledgerService = new LedgerTransactionService(request.GrowerGuid, Guid.Empty, Guid.Empty, Guid.Empty);
                Response.TransactionGuid = ledgerService.CheckPayment(request.Amount, request.UserCode, request.CheckNumber);
                Response.Success = Response.TransactionGuid != Guid.Empty;
                Response.TransactionDate = ledgerService.TransactionDateString;
            }
            catch (Exception ex)
            {
                Response.Success = false;
                Response.Message = ex.Message;
            }
        }

        public CreateUnappliedPaymentResponse Response;
    }
}
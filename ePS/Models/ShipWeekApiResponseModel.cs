﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using BusinessObjectsLibrary;
using BusinessObjectServices;

namespace ePS.Models
{
    public class ShipWeekApiResponseModel
    {
        public ShipWeekApiResponseModel(ShipWeekApiRequest requestData) {

            var shipWeekService = new ShipWeekService();
            string shipWeekString = requestData.ShipWeekString;
            this.SelectedShipWeek = shipWeekService.ParseShipWeekString(shipWeekString);
            
            ShipWeek firstWeekToDisplay = shipWeekService.OffsetByWeeks(this.SelectedShipWeek, -5);
            
            var list = new List<ShipWeek>();
            for (int i = 0; i < 30; i++) {
                ShipWeek shipWeek = shipWeekService.OffsetByWeeks(firstWeekToDisplay, i);
                list.Add(shipWeek);
            }
            this.DisplayShipWeeks = list;
        
        }
        public ShipWeek SelectedShipWeek { get; set; }
        public IEnumerable<ShipWeek> DisplayShipWeeks { get; set; }
    }
}
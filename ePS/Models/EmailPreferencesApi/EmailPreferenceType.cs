﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.EmailPreferencesApi
{
    public class EmailPreferenceType
    {

        public Guid Guid { get; set; }
        public bool IsAccountAdministrator { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public bool IsValid
        {
            get
            {
                if (string.IsNullOrEmpty(this.Name)) { return false; }
                if (string.IsNullOrEmpty(this.Email)) { return false; }
                return true;
            }
        }

    }
}
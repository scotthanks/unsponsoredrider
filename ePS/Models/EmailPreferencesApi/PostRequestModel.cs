﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.EmailPreferencesApi
{
    public class PostRequestModel : RequestBase
    {
        public override void Validate()
        {
            //ToDo: Validate the Guid Passed in - check to see that record exists
            if (this.EmailPreference.Guid == Guid.Empty)
            {
                this.AddMessage("Guid Supplied is not Valid");

            }


            //ToDo: validate the data submitted 
            if (string.IsNullOrEmpty(this.EmailPreference.Name))
            {
                this.AddMessage("Person name is required.");

            }
        }

        public EmailPreferenceType EmailPreference { get; set; }

    }
}
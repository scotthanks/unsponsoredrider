﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.EmailPreferencesApi
{
    public class PutResponseModel : ResponseBase
    {
        public PutResponseModel(Guid userGuid, PutRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            if (!ValidateAuthenticationAndRequest(request)) return;
            
            //Note: Response Model should
            //  1. validate incoming data in request object, which may include modifying such as removing trailing spaces from a name
            //  2. Write incoming data
            //  3. Retrieve the updated data
            //  4. Return retrieved updated data


            //initialize Account Settings
            this.EmailPreference = new EmailPreferenceType();

            try
            {
                //ToDo: Call service(s) to write data

                //ToDo: Populate Email Preference
                EmailPreference.Email = request.EmailPreference.Email; //show plumbing works

                this.Success = true;
            }
            catch (Exception ex)
            {
                this.Success = false;
                this.AddMessage(ex.Message);
            }
        }

        public EmailPreferenceType EmailPreference { get; set; }


    }
}
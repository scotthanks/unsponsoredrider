﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;
using BusinessObjectServices;

namespace ePS.Models.AvailabilityApi
{
    public class GetGrowerVolumeLevelsResponseModel : ResponseBase
    {

        public GetGrowerVolumeLevelsResponseModel(Guid userGuid):base(userGuid, requiresAuthentication: false)
        {
        }

        public GetGrowerVolumeLevelsResponseModel(Guid userGuid, GetGrowerVolumeLevelsRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
             
                //Get the GrowerName
                var personService = new PersonService();
                var person = personService.GetPersonFromUser(userGuid);
                var growerService = new GrowerService();
                var grower = growerService.GetByGuid(person.GrowerGuid);

                this.GrowerName = grower.GrowerName;
                
                //Get the volumes
                var growerVolumes = growerService.GetVolumeLevelsForGrower(userGuid, request.SupplierCodes);

                var list = new List<GrowerVolumeLevelType>();

                foreach (var growerVolume in growerVolumes)
                {
                   
                    list.Add(
                        new GrowerVolumeLevelType()
                        {
                            SupplierGuid = growerVolume.SupplierGuid,
                            SuppierCode = growerVolume.SupplierCode,
                            SupplierName = growerVolume.SupplierName,
                            VolumeLevel = growerVolume.LevelNumber,
                            RangeLow = growerVolume.RangeLow,
                            RangeHigh = growerVolume.RangeHigh,
                            RangeType = growerVolume.RangeType,
                            CurrentLevelType = growerVolume.CurrentLevelType,
                        });
                    
                }
                this.ResponseUrl = "";
                this.VolumeLevels = list;
                Success = true;

            }
        }

        public IEnumerable<GrowerVolumeLevelType> VolumeLevels { get; set; }
        public string GrowerName { get; set; }
        public string ResponseUrl { get; set; }

    }
}
﻿using AdminAppApiLibrary;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AvailabilityApi
{
    public class SupplierProductsModel : ResponseBase
    {
        public SupplierProductsModel(Guid userGuid, Guid supplierOrderGuid)
            : base(userGuid, requiresAuthentication: true)
        {
            Response = new SupplierProductsResponse();
            Response.SupplierProductsList = new List<SupplierProduct>();
            try
            {
                //Guid SupplierOrderGuid = new Guid("4fbea27f-3101-4025-b308-7fe0a391e4d3");

                SupplierOrderTableService supplierOrderService = new SupplierOrderTableService();
                DataObjectLibrary.SupplierOrder supplierOrder = supplierOrderService.GetByGuid(supplierOrderGuid);

                SupplierTableService supplierService = new SupplierTableService();
                DataObjectLibrary.Supplier supplier = supplierService.GetByGuid(supplierOrder.SupplierGuid);

                GrowerOrderTableService growerOrderService = new GrowerOrderTableService();
                DataObjectLibrary.GrowerOrder growerOrder = growerOrderService.GetByGuid(supplierOrder.GrowerOrderGuid);

                ProgramTypeTableService programTypeService = new ProgramTypeTableService();
                DataObjectLibrary.ProgramType programType = programTypeService.GetByGuid(growerOrder.ProgramTypeGuid);

                ProductFormCategoryTableService productFormService = new ProductFormCategoryTableService();
                DataObjectLibrary.ProductFormCategory productFormCategory = productFormService.GetByGuid(growerOrder.ProductFormCategoryGuid);

                ShipWeekTableService shipWeekService = new ShipWeekTableService();
                DataObjectLibrary.ShipWeek shipWeek = shipWeekService.GetByGuid(growerOrder.ShipWeekGuid);

                var status = new StatusObject(userGuid, true);
                AvailabilityService availService = new AvailabilityService(status);

                Availability availability = availService.GetAvailability(
                    userCode: null,
                    shipWeek: new ShipWeek(shipWeek),
                    weeksBefore: 0,
                    weeksAfter: 0,
                    includeCartData: false,
                    includePrices: true,
                    programTypeCode: programType.Code,
                    productFormCategoryCode: productFormCategory.Code,
                    supplierCodeList: new string[] { supplier.Code },
                    geneticOwnerCodeList: new string[] { "*" },
                    varietyCodeList: new string[] { "*" },
                    varietyMatchOptional: true);

                foreach (var prod in availability.ProductList)
                {
                    SupplierProduct supplierProd = new SupplierProduct();

                    supplierProd.Code = prod.Code;
                    supplierProd.ColorName = prod.ColorName;
                    supplierProd.GeneticOwnerName = prod.GeneticOwnerName;
                    supplierProd.Guid = prod.Guid;
                    supplierProd.HabitName = prod.HabitName;
                    supplierProd.IncludesDelivery = prod.IncludesDelivery;
                    supplierProd.IsOrganic = prod.IsOrganic;
                    supplierProd.LineItemMinumumQty = prod.LineItemMinumumQty;
                    supplierProd.Price = prod.Price;
                    supplierProd.ProductDescription = prod.ProductDescription;
                    supplierProd.ProductFormCategoryCode = prod.ProductFormCategoryCode;
                    supplierProd.ProductFormGuid = prod.ProductFormGuid;
                    supplierProd.ProductFormName = prod.ProductFormName;
                    supplierProd.ProductionLeadTimeWeeks = prod.ProductionLeadTimeWeeks;
                    supplierProd.ProgramGuid = prod.ProgramGuid;
                    supplierProd.ProgramTypeCode = prod.ProgramTypeCode;
                    supplierProd.SalesUnitQty = prod.SalesUnitQty;
                    supplierProd.SpeciesCode = prod.SpeciesCode;
                    supplierProd.SpeciesName = prod.SpeciesName;
                    supplierProd.SupplierCode = prod.SupplierCode;
                    supplierProd.SupplierName = prod.SupplierName;
                    supplierProd.SupplierProductDescription = prod.SupplierProductDescription;
                    supplierProd.SupplierProductIdentifier = prod.SupplierProductIdentifier;
                    supplierProd.VarietyCode = prod.VarietyCode;
                    supplierProd.VarietyName = prod.VarietyName;
                    supplierProd.VigorName = prod.VarietyName;

                    Response.SupplierProductsList.Add(supplierProd);
                }

                Response.Success = true;
            }
            catch (Exception ex)
            {
                Response.Success = false;
                Response.Message = ex.Message;
            }
        }

        public SupplierProductsResponse Response;
    }
}
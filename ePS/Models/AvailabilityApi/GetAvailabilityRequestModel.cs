﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AvailabilityApi
{
    public class GetAvailabilityRequestModel : RequestBase
    {
      
        public Guid UserGuid { get; set; }

        public string ProgramTypeCode { get; set; }
       
        public string Category { get { return ProgramTypeCode; } set { ProgramTypeCode = value; } }

        public string ProductFormCategoryCode { get; set; }
      
        public string Form { get { return ProductFormCategoryCode; } set { ProductFormCategoryCode = value; } }

        public string Suppliers { get; set; }
        public string Species { get; set; }

        public string ShipWeek { get; set; }
        public string OrganicOnly { get; set; }
        public string AvailableOnly { get; set; }
        public string SellerCode { get; set; }
       
        public override void Validate()
        {
            if (string.IsNullOrEmpty(ProgramTypeCode))
            {
                AddMessage("ProgramTypeCode is null.");
            }

            if (string.IsNullOrEmpty(ProductFormCategoryCode))
            {
                AddMessage("ProductFormCategoryCode is required.");
            }

            if (string.IsNullOrEmpty(ShipWeek))
            {
                AddMessage("ShipWeek is required.");
            }
        }
    }
}
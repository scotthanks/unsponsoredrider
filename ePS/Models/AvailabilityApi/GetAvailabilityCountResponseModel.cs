﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;


namespace ePS.Models.AvailabilityApi
{
    public class GetAvailabilityCountResponseModel : ResponseBase
    {
        public GetAvailabilityCountResponseModel(Guid userGuid)
            :base(userGuid, requiresAuthentication: false)
        {
        }

        public GetAvailabilityCountResponseModel(Guid userGuid, GetAvailabilityRequestModel availabilityRequest)
            : base(userGuid, requiresAuthentication: false)
        {
            if (ValidateAuthenticationAndRequest(availabilityRequest))
            {
                if(availabilityRequest.UserCode == null)
                {
                    availabilityRequest.UserCode = "Scott_ReadOnly";
                }
                

                var programTypeCode = availabilityRequest.ProgramTypeCode;
                var productFormCategoryCode = availabilityRequest.ProductFormCategoryCode;

                string suppliers = availabilityRequest.Suppliers ?? "";
                var supplierCodeArray = suppliers.Split(new char[] { ',' });

                string species = availabilityRequest.Species ?? "";
                var speciesCodeArray = species.Split(new char[] { ',' });

                var selectedShipWeekString = availabilityRequest.ShipWeek;
                var organicOnly = Convert.ToBoolean(availabilityRequest.OrganicOnly);
                var availableOnly = Convert.ToBoolean(availabilityRequest.AvailableOnly);
                string sellerCode = availabilityRequest.SellerCode ?? "";
                sellerCode = "GFB";

                var shipWeekService = new ShipWeekService();
                BusinessObjectsLibrary.ShipWeek selectedShipWeek = null;
                if (!string.IsNullOrEmpty(selectedShipWeekString))
                    selectedShipWeek = shipWeekService.ParseShipWeekCodeOrString(selectedShipWeekString);


                var status = new StatusObject(userGuid, true);
                var service = new AvailabilityService(status);
                var rowCount = 
                service.GetAvailabilityCount
                (
                    userCode: availabilityRequest.UserCode,
                    shipWeek: selectedShipWeek,
                    weeksBefore: 1,
                    weeksAfter: 2,
                    programTypeCode: availabilityRequest.ProgramTypeCode,
                    productFormCategoryCode: availabilityRequest.ProductFormCategoryCode,
                    supplierCodeList: supplierCodeArray,
                    geneticOwnerCodeList: null,
                    speciesCodeList: speciesCodeArray,
                    varietyCodeList: null,
                    organicOnly: organicOnly,
                    availableOnly: availableOnly,
                    sellerCode: sellerCode
                );

                this.RowDataCount = rowCount;

                

                Success = true;
            }
        }


        public long RowDataCount { get; set; }
       
    }
}
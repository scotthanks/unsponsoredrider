﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using BusinessObjectServices;

namespace ePS.Models.AvailabilityApi
{
    public class NextShipWeekCodesGet : ResponseBase
    {


        public List<ShipWeek2> ShipWeekCodes { get; set; }

        public NextShipWeekCodesGet(Guid userGuid)
           : base(userGuid, requiresAuthentication: false)
        {
            var status = new StatusObject(userGuid, false);
            try
            {
                
                var service = new ReportService(status);
                ShipWeekCodes = service.GetNextShipWeekCodes(52);


                status.StatusMessage = "Report Data Retrieved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }


    }

}
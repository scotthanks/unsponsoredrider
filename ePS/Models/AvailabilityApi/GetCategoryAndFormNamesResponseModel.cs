﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;
using BusinessObjectServices;

namespace ePS.Models.AvailabilityApi
{
    public class GetCategoryAndFormNamesResponseModel : ResponseBase
    {

        public GetCategoryAndFormNamesResponseModel(Guid userGuid):base(userGuid, requiresAuthentication: false)
        {
        }

        public GetCategoryAndFormNamesResponseModel(Guid userGuid, GetCategoryAndFormNamesRequestModel request)
            : base(userGuid, requiresAuthentication: false)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
             
               
                var catService = new ProgramTypeService ();
                var category = catService.GetByCode(request.CategoryCode);
                var formService = new ProductFormCategoryService();
                var form = formService.GetByCode(request.FormCode);

                this.FormName = form.Name ;
                this.CategoryName = category.Name;

                Success = true;

            }
        }

        public string FormName { get; set; }
        public string CategoryName { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Models.OrderApi;

namespace ePS.Models.AvailabilityApi
{
    public class PostResponseModel : ResponseBase
    {
        public PostResponseModel(Guid userGuid, PostRequestModel request, bool avail2)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                try
                {
                    var shipWeek = new BusinessObjectsLibrary.ShipWeek();
                    var service = new BusinessObjectServices.GrowerOrderService();

                    var shipWeekService = new BusinessObjectServices.ShipWeekService();
                    if (request.AddToOrder == true)
                    {
                        shipWeek = shipWeekService.ParseShipWeekString(request.ShipWeekString);
                        int itemsAdded = service.AddToOrder(userGuid, request.UserCode, new Guid(request.GrowerOrderGuid), shipWeek.Code, request.Category, request.Form);
                        AddMessage(string.Format("{0} products added to your Order.", itemsAdded));

                        string headerText = "ORDER UPDATE";
                        string bodyText = "We have received your order update. We will be in touch if there are any issues with making these changes.";
                        var emailer = new SendConfirmation();
                        Success = emailer.SendConfirmationEmail(userGuid, new Guid(request.GrowerOrderGuid), headerText, bodyText, 0, itemsAdded, 0);
                    }
                    else
                    {
                    
                        if (avail2==false)  //old way
                        {
                            shipWeek = shipWeekService.ParseShipWeekString(request.ShipWeekString);
                            if (request.AddToOrder == false)
                            {
                                int itemsAdded = service.AddToCart(userGuid, request.UserCode, shipWeek.Code, request.Category, request.Form);
                                AddMessage(string.Format("{0} products added to your cart.", itemsAdded));
                            }
                            else
                            {
                                int itemsAdded = service.AddToOrder(userGuid, request.UserCode, new Guid(request.GrowerOrderGuid), shipWeek.Code, request.Category, request.Form);
                                AddMessage(string.Format("{0} products added to your Order.", itemsAdded));

                                string headerText = "ORDER UPDATE";
                                string bodyText = "We have received your order update. We will be in touch if there are any issues with making these changes.";
                                var emailer = new SendConfirmation();
                                Success = emailer.SendConfirmationEmail(userGuid, new Guid(request.GrowerOrderGuid), headerText, bodyText, 0, itemsAdded, 0);
                            }
                        }
                        else  //new way
                        {
                            int itemsAdded = service.AddToCartAllWeeks(userGuid, request.UserCode, request.Category, request.Form);
                            AddMessage(string.Format("{0} products added to your cart.", itemsAdded));
                        }
                        Success = true;
                    }
                }
                catch (Exception e)
                {
                    AddMessage(e.Message);
                    Success = false;
                }
            }
        }

        public int Count { get; set; }

        [Obsolete]
        public string UserMessage { get { return Message; }}
    }
}
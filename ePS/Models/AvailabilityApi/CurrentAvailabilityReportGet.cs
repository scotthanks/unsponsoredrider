﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using BusinessObjectServices;
namespace ePS.Models.AvailabilityApi
{
    public class CurrentAvailabilityReportGet : ResponseBase
    {


        public List<AvailabilityItemAcrossWeeks> ReportData { get; set; }

        public CurrentAvailabilityReportGet(Guid userGuid)
            : base(userGuid, requiresAuthentication: false)
        {
            var status = new StatusObject(userGuid, false);
            try
            {

                var service = new ReportService(status);
                ReportData = service.GetCurrentAvailabilityReport();


                status.StatusMessage = "Report Data Retrieved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }


    }
}
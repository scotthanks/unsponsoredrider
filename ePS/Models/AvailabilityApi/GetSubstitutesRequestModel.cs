﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AvailabilityApi
{
    public class GetSubstitutesRequestModel : RequestBase
    {
        public Guid OrderLineGuid { get; set; }
        public string ShipWeek { get; set; }
        public string SpeciesCode { get; set; }

        public override void Validate()
        {
            if (OrderLineGuid == Guid.Empty)
            {
                AddMessage("OrderLineGuid is required.");
            }
        }
    }
}
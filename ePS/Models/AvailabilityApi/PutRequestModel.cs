﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;

namespace ePS.Models.AvailabilityApi
{
    public class PutRequestModel : RequestBase
    {
        

        public string ShipWeekString { get; set; }
        public Guid SessionGuid { get; set; }
        public string SellerCode { get; set; }
        public ProductAvailabilityDataType RowData { get; set; }

        public override void Validate()
        {
            if (string.IsNullOrEmpty(ShipWeekString))
            {
                AddMessage("ShipWeekString is required.");
            }

            if (RowData == null)
            {
                AddMessage("RowData is required.");
            }
            else
            {
                if (RowData.ProductGuid == Guid.Empty)
                {
                    AddMessage("RowData.ProductGuid is required.");
                }
                //Commented out by Scott on Nov 5.
                //if (RowData.Price == 0)
                //{
                //    AddMessage("RowData.Price is required.");
                //}
            }
        }
    }
}
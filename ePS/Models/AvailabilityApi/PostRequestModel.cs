﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AvailabilityApi
{
    public class PostRequestModel : RequestBase
    {

        public string GrowerOrderGuid { get; set; }

        public string Category { get; set; }
        public string Form { get; set; }
        public string ShipWeekString { get; set; }
        public bool AddToOrder { get; set; }

        public override void Validate()
        {
            if (string.IsNullOrEmpty(Category))
            {
                AddMessage("Category is required.");
            }

            if (string.IsNullOrEmpty(Form))
            {
                AddMessage("Form is required.");
            }

            if (string.IsNullOrEmpty(ShipWeekString))
            {
                AddMessage("ShipWeekString is required.");
            }
        }
    }

}
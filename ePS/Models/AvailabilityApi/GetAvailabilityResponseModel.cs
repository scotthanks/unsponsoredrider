﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;


namespace ePS.Models.AvailabilityApi
{
    public class GetAvailabilityResponseModel : ResponseBase
    {
        public GetAvailabilityResponseModel(Guid userGuid)
            :base(userGuid, requiresAuthentication: false)
        {
        }

        public GetAvailabilityResponseModel(Guid userGuid, GetAvailabilityRequestModel availabilityRequest)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(availabilityRequest))
            {
                var programTypeCode = availabilityRequest.ProgramTypeCode;
                var productFormCategoryCode = availabilityRequest.ProductFormCategoryCode;

                string suppliers = availabilityRequest.Suppliers ?? "";
                var supplierCodeArray = suppliers.Split(new char[] { ',' });

                string species = availabilityRequest.Species ?? "";
                var speciesCodeArray = species.Split(new char[] { ',' });

                var selectedShipWeekString = availabilityRequest.ShipWeek;
                var organicOnly = Convert.ToBoolean(availabilityRequest.OrganicOnly);

                var shipWeekService = new ShipWeekService();
                BusinessObjectsLibrary.ShipWeek selectedShipWeek = null;
                if (!string.IsNullOrEmpty(selectedShipWeekString))
                    selectedShipWeek = shipWeekService.ParseShipWeekCodeOrString(selectedShipWeekString);

                var userProfileService = new UserProfileService();
                ShowPriceData = userProfileService.UserHasPermissionToSeePrices(userGuid);
                AllowOrdering = userProfileService.UserHasPermissionToPlaceOrders(userGuid);

                var status = new StatusObject(userGuid, true);
                var service = new AvailabilityService(status);
                var availability =
                    service.GetAvailability
                    (
                        userCode: availabilityRequest.UserCode,
                        shipWeek: selectedShipWeek,
                        weeksBefore: 1,
                        weeksAfter: 3,
                        programTypeCode: availabilityRequest.ProgramTypeCode,
                        productFormCategoryCode: availabilityRequest.ProductFormCategoryCode,
                        supplierCodeList: supplierCodeArray,
                        geneticOwnerCodeList: null,
                        speciesCodeList: speciesCodeArray,
                        varietyCodeList: null,
                        varietyMatchOptional: true,
                        includeCartData: AllowOrdering,
                        includePrices: ShowPriceData,
                        organicOnly: organicOnly,
                        sellerCode: "GFB"
                    );

                this.ShipWeekHeadings = new string[availability.ShipWeekList.Count];
                int weekColumn = 0;
                availability.ShipWeekList.Sort();
                foreach (var shipweek in availability.ShipWeekList)
                {
                    this.ShipWeekHeadings[weekColumn] = shipweek.Week.ToString(CultureInfo.InvariantCulture);
                    weekColumn++;
                }

                var list = new List<ProductAvailabilityDataType>();
                
                foreach (var productData in availability.ProductList)
                {
                    var bIsPW = false;
                    var availabilityList = new int[productData.SelectedAvailability.Count];
                    var displayCodes = new string[productData.SelectedAvailability.Count];
                    int i = 0;
                    foreach (var quantityData in productData.SelectedAvailability)
                    {
                        availabilityList[i] = quantityData.Quantity;

                        string availabilityType = quantityData.AvailabilityType;
                        displayCodes[i] = availabilityType == "AVAIL" ? "" : availabilityType;

                        i++;
                    }
                    if (productData.GeneticOwnerName == "Proven Winners")
                        {  bIsPW = true;}
                    var type = new ProductAvailabilityDataType()
                    {
                        ProductGuid = productData.Guid,
                        Supplier = productData.SupplierName,
                        SpeciesCode = productData.SpeciesCode,
                        Species = productData.SpeciesName,
                        Product = productData.ProductDescription,
                        ProductCode = productData.Code,
                        VarietyCode = productData.VarietyCode,
                        Form = productData.ProductFormName,
                        Min = productData.LineItemMinumumQty,
                        Mult = productData.SalesUnitQty,
                        IsOrganic = productData.IsOrganic,
                        IsPW = bIsPW,
                        Price = productData.Price,
                        IncludesDelivery = productData.IncludesDelivery,
                        OrderQuantity = productData.OrderLine == null ? 0 : productData.OrderLine.QuantityOrdered,
                        IsCarted = productData.OrderLine != null && (productData.OrderLine.IsCarted != null && (bool)productData.OrderLine.IsCarted),
                        Availabilities = availabilityList,
                        DisplayCodes = displayCodes
                    };

                    if (type.OrderQuantity != 0)
                    {
                        type.OrderQuantity = type.OrderQuantity;
                    }

                    list.Add(type);
                }

                this.RowData = list;

                Success = true;
            }
        }

        [Obsolete("Use Messages instead.")]
        public string StatusMessage { get; set; }

        public string[] ShipWeekHeadings { get; set; }
        public IEnumerable<ProductAvailabilityDataType> RowData  { get; set; }
        public bool ShowPriceData { get; set; }
        public bool AllowOrdering { get; set; }
    }
}
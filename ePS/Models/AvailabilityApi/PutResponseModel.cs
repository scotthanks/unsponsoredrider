﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;

namespace ePS.Models.AvailabilityApi
{
    public class PutResponseModel : ResponseBase
    {
        public PutResponseModel(Guid userGuid, PutRequestModel request,bool avail2)
            : base( userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                bool success = false;

                this.RowData = request.RowData;

                var service = new BusinessObjectServices.GrowerOrderService();

                var shipweekService = new BusinessObjectServices.ShipWeekService();
                
                var shipweek = shipweekService.ParseShipWeekString(request.ShipWeekString);
                var qty = 0;
                if (avail2 == true)
                {
                    qty = request.RowData.OrderQuantities[request.RowData.WeekOffset];
                }
                else
                {
                    qty = request.RowData.OrderQuantity;
                }

                var availability =
                    service.UpdateCartQuantity
                    (
                        userGuid: userGuid,
                        userCode: request.UserCode,
                        shipWeekCode: shipweek.Code,
                        productGuid: request.RowData.ProductGuid,
                        quantity: qty,
                        price: request.RowData.Price,
                        getNewRowData: false,
                        sellerCode: "GFB"
                        
                    );
                if (1 == 1)
                {
                    var rowData = new ProductAvailabilityDataType();
                    this.RowData = rowData;
                    success = true;
                }
                else if (availability.ProductList.Count() == 1)
                {
                    try
                    {
                        var rowData = new ProductAvailabilityDataType();

                        var product = availability.ProductList[0];

                        rowData.ProductGuid = product.Guid;
                        rowData.Product = product.ProductDescription;
                        rowData.ProductCode = product.Code;
                        rowData.ProductCode = product.Code;
                        rowData.VarietyCode = product.VarietyCode;
                        rowData.SpeciesCode = product.SpeciesCode;
                        rowData.Species = product.SpeciesName;
                        rowData.Supplier = product.SupplierName;
                        rowData.Min = product.LineItemMinumumQty;
                        rowData.Mult = product.SalesUnitQty;
                        rowData.IsOrganic = product.IsOrganic;
                        rowData.Price = product.Price;
                        rowData.IncludesDelivery = product.IncludesDelivery;

                        if (product.OrderLine == null)
                        {
                            rowData.OrderQuantity = 0;
                            rowData.IsCarted = false;
                        }
                        else
                        {
                            rowData.OrderQuantity = product.OrderLine.QuantityOrdered;
                            rowData.IsCarted = product.OrderLine.IsCarted ?? false;
                        }

                        //TODO: Fix this; it relies on all selected availabilities being there, which they may not be.
                        int index = 0;
                        rowData.Availabilities = new int[product.SelectedAvailability.Count];
                        rowData.DisplayCodes = new string[product.SelectedAvailability.Count];
                        foreach (var selectedAvailability in product.SelectedAvailability)
                        {
                            rowData.Availabilities[index] = selectedAvailability.Quantity;
                            rowData.DisplayCodes[index] = selectedAvailability.AvailabilityType;
                            index++;
                        }

                        this.RowData = rowData;

                        success = true;
                    }
                    catch (Exception e)
                    {
                        AddMessage(e.Message);
                    }
                }

                this.Success = success;                
            }
        }

        [Obsolete]
        public string UserMessage 
        { 
            get { 
                return Message; }
        }

        public ProductAvailabilityDataType RowData { get; set; }
    }
}
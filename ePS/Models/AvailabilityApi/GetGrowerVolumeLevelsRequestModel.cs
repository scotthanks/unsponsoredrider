﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
 
namespace ePS.Models.AvailabilityApi
{
    public class GetGrowerVolumeLevelsRequestModel : RequestBase
    {
        public string SupplierCodes { get; set; }

        public override void Validate()
        {
            if (string.IsNullOrEmpty(SupplierCodes))
            {
                AddMessage("No Suppler Codes submitted.");
            }
        }

    }
}
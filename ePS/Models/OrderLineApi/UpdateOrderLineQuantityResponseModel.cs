﻿//using BusinessObjectServices;
//using ePS.Models.CartApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Models.OrderApi;
namespace ePS.Models.OrderLineApi
{
    public class UpdateOrderLineQuantityResponseModel : ResponseBase
    {
        public UpdateOrderLineQuantityResponseModel(Guid userGuid, UpdateOrderLineQuantityRequestModel request)
            : base( userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                bool isAdminRequest = !string.IsNullOrEmpty(request.UserCode);
                var growerOrderGuid = new Guid();
                var growerOrderService = new GrowerOrderService();
                int iCancelLine = 0;
                int iUpdateLine = 1;
                int originalQty = 0;
                int returnQty = 0;
                int updateQty = 0;
                var returnMessage = "";
                bool sendMail = true;
                var statusList = new List<OrderLineQuantityStatusType>();
               
               try{
                    foreach (UpdateOrderLineQuantityType orderLineUpdate in request.OrderLineUpdates)
                    {
                        var service = new GrowerOrderService();
                        var returnData = service.OrderLineUpdate(userGuid,orderLineUpdate.OrderLineGuid, orderLineUpdate.Quantity, 0, "Grower",out growerOrderGuid);

                        
                        originalQty = orderLineUpdate.OriginalQuantity;
                        updateQty = orderLineUpdate.Quantity - originalQty;
                        returnQty = returnData.Quantity;
                        returnMessage = returnData.Message;
                        AddMessage(returnMessage);
  //                      AddMessage(returnMessage + " " + CSingleTone.Instance.theCounter.ToString());
                        if (originalQty == returnQty)
                        {
                            AddMessage("Order Line not updated, insufficient availability");
                            returnMessage = "Order Line not updated, insufficient availability";
                            sendMail = false;

                        }
                        statusList.Add
                            (
                                new OrderLineQuantityStatusType
                                {
                                    OrderLineGuid = orderLineUpdate.OrderLineGuid,
                                    QuantityOnOrder = returnData.Quantity,
                                    Success = returnData.Success,
                                    Message = returnMessage
                                }
                            );
                       

                        if (orderLineUpdate.Quantity == 0)
                        {
                            iCancelLine = 1;
                            iUpdateLine = 0;
                        }

                       
                    }

                    OrderLineStatuses = statusList.ToArray();
                    
                    string headerText ="ORDER UPDATED";
                    string bodyText = "We have received your order update. We will be in touch if there are any issues with making these changes.";

                    Success = true;
                    if (sendMail == true)
                    {
                        var emailer = new SendConfirmation();
                        Success = emailer.SendConfirmationEmail(userGuid, growerOrderGuid, headerText, bodyText, iUpdateLine, 0, iCancelLine);
                    }
                    
                }
                catch (Exception ex)
                { 
                   
                    Success = false;
                    AddMessage("something is wrong:" + ex.Message );
                }
                finally
                {
                
                }
                
            }
        }

        public OrderLineQuantityStatusType[] OrderLineStatuses { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.OrderLineApi
{
    public class OrderLineQuantityStatusType
    {
        public Guid OrderLineGuid { get; set; }
        public bool Success { get; set; }
        public int QuantityOnOrder { get; set; }
        public string Message { get; set; }
    }
}
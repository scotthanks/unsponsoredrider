﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;

namespace ePS.Models.CardOnFile
{
    public class GetResponseModel
    {
        public GetResponseModel(Guid userGuid, GetRequestModel request)
        {
            Message = "";

            if (userGuid == Guid.Empty)
            {
                IsAuthenticated = false;
                Message = "You are not authenticated.";
            }
            else
            {
                IsAuthenticated = true;
                //This is silly, taking it out.
                //var cardOnFileService = new BusinessObjectServices.CardOnFileService();
                //ProfileIds = cardOnFileService.CustomerProfileIds();
                var profileslist = new List<BusinessObjectServices.CustomerProfile>();
                //foreach (var profileId in ProfileIds)
                //{

                //    var profile = cardOnFileService.RetrieveCustomerProfile(profileId);
                //    profileslist.Add(profile);
                //};
                this.Profiles = profileslist;

                var growerService = new GrowerService();
                var grower = growerService.TryGetGrowerForUser(userGuid);


                var growerOrderService = new GrowerOrderService();
                GrowerOrder growerOrder = null;
                if (request.GrowerOrderGuid != Guid.Empty)
                {
                    growerOrder = growerOrderService.GetGrowerOrderDetail( request.GrowerOrderGuid,false,false);
                }

                var creditcardList =
                    growerService.GetCreditCards
                        (
                            userGuid: userGuid,
                            growerGuid: grower.Guid,
                            growerCreditCardGuid: request.GrowerCreditCardGuid,
                            growerOrderGuid: request.GrowerOrderGuid
                        );

                var creditCardTypeList = new List<CreditCardType>();
                foreach (var creditCard in creditcardList)
                {
                    var creditCardType = new GetResponseCreditCardModel(creditCard).CreditCard;

                    if (growerOrder != null && creditCard.Guid == growerOrder.GrowerCreditCardGuid)
                        creditCardType.IsSelected = true;

                    creditCardTypeList.Add(creditCardType);
                }
                CreditCards = creditCardTypeList.ToArray();

                Success = true;
            }
        }

        public bool IsAuthenticated { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }

        // USE THIS ONE
        public IEnumerable<CreditCardType> CreditCards { get; set; }

        // NOT NEEDED
        public IEnumerable<BusinessObjectServices.CustomerProfile> Profiles { get; set; }
        public string[] ProfileIds { get; set; }
    }
}
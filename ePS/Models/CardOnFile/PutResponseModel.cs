﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
 
namespace ePS.Models.CardOnFile
{
    public class PutResponseModel
    {
        public PutResponseModel(Guid userGuid, PutRequestModel request)
        {
            if (userGuid != Guid.Empty)
            {
                var userProfileService = new UserProfileService();
                if (userProfileService.UserHasPermissionToPlaceOrders(userGuid))
                {
                    IsAuthenticated = true;

                    if (!request.IsValid)
                    {
                        Success = false;
                        Message = request.IsValidMessage;
                    }
                    else
                    {
                        var growerService = new GrowerService();
                        var grower = growerService.TryGetGrowerForUser(userGuid);
                        if (grower == null)
                        {
                            Success = false;
                            Message = "The current user is not attached to a Grower.";
                        }
                        else
                        {
                            string profileId = growerService.GetAuthorizationProfileId(grower.Guid);

                            var cardOnFileService = new BusinessObjectServices.CardOnFileService();
                            if (string.IsNullOrEmpty(profileId))
                            {
                                var profileResponse = cardOnFileService.CreateCustomerProfile(grower.Guid);

                                profileId = profileResponse.ProfileID;

                                if (string.IsNullOrEmpty(profileId))
                                {
                                    Success = false;
                                    Message = "A ProfileId could not be created.";
                                }
                                else
                                {
                                    growerService.SaveAuthorizationProfileId(grower, profileId);                                    
                                }
                            }

                            if (!string.IsNullOrEmpty(profileId))
                            {
                                var cardData = new BusinessObjectServices.CreditCardProfileRequest()
                                    {
                                        CardType = request.CardType,
                                        CardNumber = request.CardNumber,
                                        ExpirationDate = request.ExpirationDate,
                                        Code = request.Code,
                                        Description = request.Description,
                                        FirstName = request.FirstName,
                                        LastName = request.LastName,
                                        Company = request.Company,
                                        StreetAddress1 = request.StreetAddress1,
                                        StreetAddress2 = request.StreetAddress2,
                                        City = request.City,
                                        StateCode = request.StateCode,
                                        Country = request.Country,
                                        ZipCode = request.ZipCode,
                                        PhoneNumber = request.PhoneNumber,
                                        FaxNumber = request.FaxNumber
                                    };

                                int cardsOnFileCount = 0;
                                try
                                {
                                    var customerProfile = cardOnFileService.CreateCardProfile(profileId, cardData);

                                    if (customerProfile.Created == false)
                                    {
                                        Success = false;
                                        Message = "Card was not added to profile.";
                                    }
                                    else
                                    {
                                        ProfileID = customerProfile.ProfileID;
                                        Description = request.Description;
                                        CardNumber = customerProfile.CardJustAdded.CardNumber;
                                        CardType = customerProfile.CardJustAdded.CardTypeCode ?? request.CardType;
                                        CardExpiration = customerProfile.CardJustAdded.ExpirationDate;

                                        cardsOnFileCount = customerProfile.CardsOnFile.Count();
                                        Success = true;
                                        Message = "";
                                    }
                                }
                                catch (Exception e)
                                {
                                    Success = false;
                                    Message = e.Message;
                                }

                                if (Success == true)
                                {
                                    bool isDefaultCard = cardsOnFileCount == 1;
                                    CardGuid = cardOnFileService.SaveGrowerCreditCard
                                        (
                                            growerGuid: grower.Guid,
                                            cardProfileId: profileId,
                                            cardDescription: request.Description,
                                            maskedCardNumber: CardNumber,
                                            cardType: CardType,
                                            isDefaultCard: isDefaultCard
                                        );
                                }
                            }
                        }
                    }
                }
            }
        }

        public bool IsAuthenticated { get; set; }
        public bool Success { get; set; }

        public Guid CardGuid { get; set; }
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string CardExpiration { get; set; }
        public string Description { get; set; }

        public string Message { get; set; }

        // Not needed.
        public string ProfileID { get; set; }
    }
}
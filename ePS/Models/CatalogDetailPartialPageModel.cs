﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using BusinessObjectServices;
using BusinessObjectsLibrary;


namespace ePS.Models
{
    public class CatalogDetailPartialPageModel
    {
        public CatalogDetailPartialPageModel(string plantCategoryCode, string productFormCode, string supplierCodes, string varietyCode, string shipWeek){
            string seriesCultureLibraryLink = "";
            
            this.PlantCategoryCode = plantCategoryCode;
            this.ProductFormCode = productFormCode;
            this.SupplierCodes = supplierCodes;
            this.VarietyCode = varietyCode;
            this.ShipWeekString = shipWeek;

            var service = new VarietyService();
            var variety = service.GetVarietyByCode(this.VarietyCode);


            if (!variety.SeriesGuid.Equals(null))
            {
                var seriesService = new SeriesService();
                var series = seriesService.GetSeriesFromGuid(variety.SeriesGuid);
                this.SeriesDescriptionHtmlGuid = series.DescriptionHTMLGuid;
                seriesCultureLibraryLink = series.CultureLibraryLink;
            }

            
            
            this.SpeciesName  = variety.SpeciesName;
            this.VarietyName = variety.Name;
            this.BreederName = variety.GeneticOwnerName;

            this.ColorDescription = variety.ColorDescription;
            this.Color = variety.Color;
            this.Habit = variety.Habit;
            this.Vigor = variety.Vigor;
            this.DescriptionHtmlGuid = variety.DescriptionHtmlGuid;
            this.VarietyImageUrl = variety.FUllImageUrl;

            this.CultureLibraryUrl = Utility.StringStuffScott.Coalesce(variety.CultureLibraryURL , seriesCultureLibraryLink);
                      
            if( this.CultureLibraryUrl != "" )
            {
                this.CultureNotesLinkText = "Culture Notes";
            }

            this.PlantCategoryName = "TBD";
            this.ProductFormName = "TBD";

            

        
        }

        //Received Values
        public string PlantCategoryCode { get; set; }
        public string ProductFormCode { get; set; }
        public string SupplierCodes { get; set; }
        public string VarietyCode { get; set; }
        public string ShipWeekString { get; set; }

        //Retrieved Values
        public string PlantCategoryName { get; set; }
        public string ProductFormName { get; set; }
        public string SpeciesName { get; set; }
        public string VarietyName { get; set; }
        public string BreederName { get; set; }
        public string ColorDescription { get; set; }
        public string Color { get; set; }
        public string Habit { get; set; }
        public string Vigor { get; set; }
        public Guid? DescriptionHtmlGuid { get; set; }
        public string VarietyImageUrl { get; set; }
        public string CultureLibraryUrl { get; set; }
        public string CultureNotesLinkText { get; set; }
        

        public Catalogue SelectedCategory { get; set; }
        public ProductFormCategory SelectedProductFormCategory { get; set; }

        public string Species { get; set; }
        
        public int VarietyCount { get; set; }
        public IEnumerable<Variety> VarietyCollection { get; set; }
        public ShipWeek DefaultShipWeek { get; set; }
        public ShipWeek SelectedShipWeek { get; set; }

        public Guid? SeriesDescriptionHtmlGuid { get; set; }
    
    
    }


}
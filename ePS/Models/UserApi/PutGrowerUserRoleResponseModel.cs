﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using BusinessObjectsLibrary;

namespace ePS.Models.UserApi
{
    public class PutGrowerUserRoleResponseModel : ResponseBase
    {

        public PutGrowerUserRoleResponseModel(Guid userGuid, PutGrowerUserRoleRequestModel request)
          : base(userGuid, requiresAuthentication: true)
        {

            if (userGuid == request.GrowerUserGuid)
            {
                this.AddMessage("You cannot update your own account role");
                Success = false;
            }
            else
            {
                string message = "";


                BusinessObjectServices.UserProfileService service = new BusinessObjectServices.UserProfileService();
                BusinessObjectsLibrary.GrowerUser growerUser = new BusinessObjectsLibrary.GrowerUser();

                growerUser.SitePermission = request.SitePermission;
                growerUser.GrowerUserGuid = request.GrowerUserGuid;


                PersonService personService = new PersonService();
                var newPerson = personService.GetPersonFromUser(growerUser.GrowerUserGuid);
                growerUser.Email = newPerson.Email;

                var userPerson = personService.GetPersonFromUser(userGuid);

                string theTest = userPerson.Email;

                var newDBUser = service.GetUserProfile(growerUser.Email, out message);
                var newUserProfile = new BusinessObjectsLibrary.UserProfile(newDBUser.UserId, newDBUser.UserName, newDBUser.EmailAddress, newDBUser.UserGuid, newDBUser.GrowerGuid);
                theTest = newUserProfile.EmailAddress;

                bool isAdmin = false;
                bool isAuthorizedToPlaceOrders = false;
                bool isAuthorizedToSeePrices = true;

                if (growerUser.SitePermission == "CustomerAdministrator")
                {
                    isAdmin = true;
                    isAuthorizedToPlaceOrders = true;
                    isAuthorizedToSeePrices = true;
                }
                else if (growerUser.SitePermission == "CustomerOrderer")
                {
                    isAuthorizedToPlaceOrders = true;
                    isAuthorizedToSeePrices = true;
                }

                Success = service.GrantRole(growerUser.Email, isAdmin, isAuthorizedToPlaceOrders, isAuthorizedToSeePrices);

            }
                

           
        }
        

        public List<GrowerUser> growerUsers { get; set; }
    }
}
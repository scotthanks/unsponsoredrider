﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.UserApi
{
    public class PutGrowerUserDeleteRequestModel
    {

        public Guid GrowerUserGuid { get; set; }
        public string SitePermission { get; set; }       

    }
}
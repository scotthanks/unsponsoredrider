﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using BusinessObjectsLibrary;

namespace ePS.Models.UserApi
{
    public class PutGrowerUserDeleteResponseModel : ResponseBase
    {

        public PutGrowerUserDeleteResponseModel(Guid userGuid, PutGrowerUserDeleteRequestModel request)
          : base(userGuid, requiresAuthentication: true)
        {

             string message = "";

            if (userGuid == request.GrowerUserGuid)
            {
                message = "You cannot delete your own user";
                
            }
            else
            {
                PersonService personService = new PersonService();
                var bret = personService.Deactivate(request.GrowerUserGuid);
                if (bret == false)
                {
                    message = "Delete failed";
                    
                }
                
            }

            if (message != "")
            {
                AddMessage(message);
                Success = false;
            }
            else
            { Success = true; }
        }
                

           
        
        

        
    }
}
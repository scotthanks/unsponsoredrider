﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using Utility;
using System.IO;
namespace ePS.Models.UserApi
{
    public class PostGrowerUserResponseModel : ResponseBase
    {

        public PostGrowerUserResponseModel(Guid userGuid, PostGrowerUserRequestModel request)
          : base(userGuid, requiresAuthentication: true)
        {

            var bRet = false;

            string message  = "";

            BusinessObjectServices.UserProfileService service = new BusinessObjectServices.UserProfileService();
            BusinessObjectsLibrary.GrowerUser growerUser = new BusinessObjectsLibrary.GrowerUser();
            growerUser.FirstName = request.FirstName;
            growerUser.LastName = request.LastName;
            growerUser.Password = request.Password;
            growerUser.CompanyRole = request.CompanyRole;
            growerUser.Email = request.Email;
            growerUser.Phone = new Utility.PhoneNumber(request.PhoneNumber);
            growerUser.SitePermission = request.SitePermission;

            // Test Data
            //growerUser.Email = "scottp@mayflower.com";
            //growerUser.Phone = new Utility.PhoneNumber("8011088811");
            //growerUser.SitePermission = "Read Only";

            PersonService personService = new PersonService();
            var userPerson = personService.GetPersonFromUser(userGuid);

            growerUser.GrowerUserGuid = userPerson.GrowerGuid;

            var newDBUser = service.CreateGrowerUser(growerUser, out message);

            if (newDBUser.EmailAddress == growerUser.Email)
            {

                var newUserProfile = new BusinessObjectsLibrary.UserProfile(newDBUser.UserId, newDBUser.UserName, newDBUser.EmailAddress, newDBUser.UserGuid, newDBUser.GrowerGuid);

                var newPerson = personService.CreatePerson(growerUser, newUserProfile);

                if (newPerson.EMail == growerUser.Email)
                {

                    bool isAdmin = false;
                    bool isAuthorizedToPlaceOrders = false;
                    bool isAuthorizedToSeePrices = true;

                    if (growerUser.SitePermission == "CustomerAdministrator")
                    {
                        isAdmin = true;
                        isAuthorizedToPlaceOrders = true;
                        isAuthorizedToSeePrices = true;
                    }
                    else if (growerUser.SitePermission == "CustomerOrderer")
                    {
                        isAuthorizedToPlaceOrders = true;
                        isAuthorizedToSeePrices = true;
                    }

                    bRet = service.GrantRole(growerUser.Email, isAdmin, isAuthorizedToPlaceOrders, isAuthorizedToSeePrices);
                    if (bRet == false)
                    {
                        message = "Failed to grant role";
                    }
                    else {  
                        var canOrder = false;
                        if(growerUser.SitePermission == "CustomerAdministrator" || growerUser.SitePermission == "CustomerOrderer")
                        {
                            canOrder = true;
                        }
                        if (1 == 2) { 
                         SendInvitationEmail(bRet, growerUser.Email, canOrder);
                        }
                    }

                }
                else
                {
                    bRet = false;
                    message = "Failed to create person";
                }
            }
            else
            {
                bRet = false;
                message = "User not invited.<BR>This email address is already associated with an active account in orders.green-fuse.com";
            }

           



            if (message != "")
            {
                AddMessage(message);
            }
            Success = bRet;
        }
        private void SendInvitationEmail(bool bSuccess, string sEmail, bool CanOrder)
        {
            string emailText = "Email text generation failed.";
            string emailSubject = "Email subject generation failed.";



            try
            {
                string emailHtmlPath = "";
                emailSubject = "Welcome - You have been invited to Green-Fuse";
                if (CanOrder)
                {
                    emailHtmlPath = HttpContext.Current.Server.MapPath("~/Views/CustomerEmail/Invited-Email-CanPurchase.html");
                }
                else
                {
                    emailHtmlPath = HttpContext.Current.Server.MapPath("~/Views/CustomerEmail/Invited-Email-CanNOTPurchase.html");
                }
                emailText = File.ReadAllText(emailHtmlPath);
                bSuccess = true;
            }
            catch (Exception ex)
            {
                bSuccess = false;
                string errMsg = ex.Message;

            }
            var emptyGuid = new Guid("00000000-0000-0000-0000-000000000000");
            var status = new StatusObject(emptyGuid, false);
            var emailService = new EmailQueueService(status);
            var sFromEmail = new LookupService().GetConfigValue("ALMA_EMAIL");
            if (sEmail.Substring(0, 4) == "test")
            {
                sEmail = new LookupService().GetConfigValue("SCOTT_EMAIL");
            }
            
            var bRet = emailService.AddEmailtoQueue(emailSubject, emailText, 0, "Invite",sFromEmail, sEmail, "", "", emptyGuid, 0, 0, 0);



           
        }

    }
}
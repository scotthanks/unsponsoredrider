﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.OrderApi
{
    public class DeleteGrowerOrderLogRequestModel : RequestBase
    {
        public Guid GrowerOrderHistoryGuid { get; set; }
       

        public override void Validate()
        {
            if (GrowerOrderHistoryGuid == Guid.Empty)
            {
                AddMessage("GrowerOrderHistoryGuid is required.");
            }

           

        }
    }
}
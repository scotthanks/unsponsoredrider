﻿using System;

namespace ePS.Models.OrderApi
{
    public class GetGrowerOrderLogRequestModel : RequestBase
    {
        public Guid GrowerOrderGuid { get; set; }
        public bool IncludeInternalComments { get; set; }

        public override void Validate()
        {
            if (GrowerOrderGuid == Guid.Empty)
            {
                AddMessage("The GrowerOrderGuid is required.");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ePS.Models.OrderApi
{
    public class AddOrUpdateOrderLineRequestModel : RequestBase
    {
        public IEnumerable<OrderLineUpdate> OrderLineUpdates { get; set; }

        public override void Validate()
        {
            if (!OrderLineUpdates.Any())
            {
                AddMessage("There must be at least one update specified.");
            }
            else
            {
                int row = 0;
                foreach (var update in OrderLineUpdates)
                {
                    if (update.Guid == Guid.Empty)
                    {
                        AddMessage(string.Format("The Guid is required on each update. Row {0} has an empty guid.", row));
                    }

                    if (update.Quantity < 0)
                    {
                        AddMessage(string.Format("The quantity ({0}) on row {1} is less than 0.", update.Quantity, row));
                    }

                    row++;
                }
            }
        }
    }

    public class OrderLineUpdate
    {
        public Guid GrowerOrderGuid { get; set; } // Optional. Improves performance.
        public Guid Guid { get; set; }
        public int Quantity { get; set; }
        public int OriginalQuantity { get; set; } // Optional. Improves performance.
        public string ProductDescription { get; set; } // Optional. Improves performance.
    }
}
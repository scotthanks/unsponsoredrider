﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;
using AdminAppApiLibrary;

namespace ePS.Models.OrderApi
{
    public class GetResponseSupplierFeeModel : ResponseBase
    {
        public GetResponseSupplierFeeModel(Guid userGuid, GetRequestSupplierFeeModel request)
            : base(userGuid, requiresAuthentication: false)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                
                    var service = new BusinessObjectServices.GrowerOrderService();
                    var theSupplierOrderFees = service.GetSupplierOrderFees(request.SupplierOrderGuid);
                    var supplierOrderFees = new List<SupplierOrderFeeType>();
                    foreach (BusinessObjectsLibrary.SupplierOrderFee theFee in theSupplierOrderFees)
                    {
                        var newFee = new SupplierOrderFeeType();
                        newFee.Guid = theFee.Guid;
                        newFee.Amount = theFee.Amount;
                        newFee.FeeDescription = theFee.FeeDescription;
                        newFee.SupplierOrderFeeStatusCode = theFee.SupplierOrderFeeStatus.Code;
                        newFee.SupplierOrderFeeStatusDescription = theFee.SupplierOrderFeeStatus.Description;
                        newFee.SupplierOrderFeeTypeCode = theFee.SupplierOrderFeeType.Code;
                        newFee.SupplierOrderFeeTypeDescription = theFee.SupplierOrderFeeType.Description;
                        newFee.SupplierOrderGuid = theFee.SupplierOrderGuid;
                        
                        supplierOrderFees.Add(newFee);
                    }
               
                    
                
                    
                    this.SupplierOrderFees = supplierOrderFees;
                    this.Success = true;
                            
                
            }
        }

        public List<SupplierOrderFeeType> SupplierOrderFees { get; private set; }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;

namespace ePS.Models.OrderApi
{
    public class GetResponseSummaryModel : ResponseBase
    {
        public GetResponseSummaryModel(Guid userGuid, GetSummaryRequestModel summaryRequest)
            :base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(summaryRequest))
            {
                //Retrieve Data from Data Server
                var orderSummaryService = new OrderSummaryService();
                List<OrderSummary> orderSummaryList;

                GetSummaryRequestModel.RequestTypeEnum requestType;
                bool valid = Enum.TryParse(summaryRequest.RequestType, ignoreCase: true, result: out requestType);
                if (!valid)
                {
                    throw new ApplicationException(string.Format("Request type {0} is not valid.", summaryRequest.RequestType));
                }

                //if (requestType == GetSummaryRequestModel.RequestTypeEnum.SummaryData)
                //    requestType = GetSummaryRequestModel.RequestTypeEnum.MyOpenOrders;

                switch (requestType)
                {
                    //case GetSummaryRequestModel.RequestTypeEnum.MyShoppingCartOrders:
                    //    orderSummaryList = orderSummaryService.GetMyShoppingCartOrders(userGuid);
                    //    break;
                    //case GetSummaryRequestModel.RequestTypeEnum.MyOpenOrders:
                    //    orderSummaryList = orderSummaryService.GetMyOpenOrders(userGuid);
                    //    break;
                    //case GetSummaryRequestModel.RequestTypeEnum.MyShippedOrders:
                    //    orderSummaryList = orderSummaryService.GetMyShippedOrders(userGuid);
                    //    break;
                    //case GetSummaryRequestModel.RequestTypeEnum.MyCancelledOrders:
                    //    orderSummaryList = orderSummaryService.GetMyCancelledOrders(userGuid);
                    //    break;
                    //case GetSummaryRequestModel.RequestTypeEnum.MyOrders:
                    //    orderSummaryList = orderSummaryService.GetMyOrders(userGuid);
                    //    break;
                    case GetSummaryRequestModel.RequestTypeEnum.AllShoppingCartOrders:
                        orderSummaryList = orderSummaryService.GetAllShoppingCartOrders(userGuid);
                        break;
                    case GetSummaryRequestModel.RequestTypeEnum.AllOpenOrders:
                        orderSummaryList = orderSummaryService.GetAllOpenOrders(userGuid);
                        break;
                    case GetSummaryRequestModel.RequestTypeEnum.AllShippedOrders:
                        orderSummaryList = orderSummaryService.GetAllShippedOrders(userGuid);
                        break;
                    case GetSummaryRequestModel.RequestTypeEnum.AllCancelledOrders:
                        orderSummaryList = orderSummaryService.GetAllCancelledOrders(userGuid);
                        break;
                    case GetSummaryRequestModel.RequestTypeEnum.AllOrders:
                        orderSummaryList = orderSummaryService.GetAllOrders(userGuid);
                        break;
                    default:
                        throw new ApplicationException(string.Format("Request type {0} is not implemented.", requestType.ToString()));
                      
                }

                //Populate List
                var cartSummaryList = new List<OrderSummaryType>();
                foreach (OrderSummary item in orderSummaryList)
                {
                    var summaryItem = new OrderSummaryType
                    {
                        OrderGuid = item.OrderGuid,
                        OrderNo = item.OrderNo,
                        ProductFormName = item.ProductFormCategory.Name,
                        ShipWeekString = item.ShipWeek.ShipWeekString,
                        ProgramCategoryName = item.ProgramTypeName,
                        OrderQty = item.OrderQty,
                        CustomerPo = item.CustomerPoNo,
                        PromotionalCode = item.PromotionalCode,
                        OrderDescription = item.OrderDescription,
                        CustomerCode = item.CustomerCode,
                        CustomerName = item.GrowerName,
                        PhoneNumber = item.PhoneNumber,
                        GrowerOrderStatusCode = item.GrowerOrderStatusCode,
                        GrowerOrderStatusName = item.GrowerOrderStatusName,
                        LowestSupplierOrderStatusCode = item.LowestSupplierOrderStatusCode,
                        LowestOrderLineStatusCode = item.LowestOrderLineStatusCode,
                        PersonWhoPlacedOrder = new GetPersonResponseModel(item.PersonWhoPlacedOrder).PersonType,
                        InvoiceNo = item.InvoiceNo
                    };

                    cartSummaryList.Add(summaryItem);
                }

                //ToDo: Sort by ShipWeekString, ProductFormName

                //List<SomeClass>() a;
                //List<SomeClass> b = a.OrderBy(x => x.x).ThenBy(x => x.y).ToList();

                //List<GetResponseSummaryType> sortedCartSummaryList = cartSummaryList.OrderBy(x => x.ShipWeekString).ThenBy(x => x.ProgramCategoryName).ThenBy(x => x.ProductFormName).ToList();

                List<OrderSummaryType> sortedCartSummaryList = cartSummaryList.OrderBy(x => x.OrderNo).ToList();
                SummaryData = sortedCartSummaryList;

                
                
                
                

               
                PersonService personService = new PersonService();
                var userPerson = personService.GetPersonFromUser(userGuid);


                BusinessObjectServices.UserProfileService service = new BusinessObjectServices.UserProfileService();
             
               var message = "";
               var profile = service.GetUserProfile( userPerson.Email,out message);
               var currentRoleObj = service.GetUserRole(profile.UserId);
               if (currentRoleObj.RoleName == "CustomerAdministrator" || currentRoleObj.RoleName == "CustomerOrderer")
               { IsReadOnly = false; }
               else
               { IsReadOnly = true; }


                Success = true;
            }
        }

        public IEnumerable<OrderSummaryType> SummaryData { get; set; }
        public bool IsReadOnly{ get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectsLibrary;

namespace ePS.Models.OrderApi
{
    public class GetOrderHistoryRequestModel : RequestBase
    {
        public Guid OrderGuid { get; set; }
        public string RequestType { get; set; }
        public List<GrowerOrderHistoryEvent> eventList { get; set; }

        public override void Validate()
        {
            if (OrderGuid == Guid.Empty)
            {
                AddMessage("The OrderGuid is required.");
            }
        }
    }

   
}
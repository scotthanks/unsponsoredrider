﻿using System;
using DataServiceLibrary;

namespace ePS.Models.OrderApi
{
    public class PlaceOrderRequestModel : RequestBase
    {
        public Guid GrowerOrderGuid { get; set; }
        public string OrderTransitionTypeLookupCode { get; set; }
       
        public bool AgreeToTerms { get; set; }

        public override void Validate()
        {
            if (GrowerOrderGuid == Guid.Empty)
            {
                AddMessage("The GrowerOrderGuid, SupplierOrderGuid or OrderLineGuid is required.");
            }

            if (string.IsNullOrEmpty(OrderTransitionTypeLookupCode))
            {
                //TODO: Don't allow a null or empty code.
                OrderTransitionTypeLookupCode = LookupTableValues.Code.OrderTransitionType.PlaceOrder.Code;
                //AddMessage("OrderTransitionTypeLookupCode is required.");
            }

            var lookupBusinessService = new BusinessObjectServices.LookupService();

            if (!lookupBusinessService.IsValidOrderTransitionCode(OrderTransitionTypeLookupCode))
            {
                AddMessage(string.Format( "The OrderTransitionTypeLookupCode \"{0}\" is not valid.", OrderTransitionTypeLookupCode));
            }

            if (OrderTransitionTypeLookupCode == LookupTableValues.Code.OrderTransitionType.PlaceOrder.Code)
            {
                if (AgreeToTerms == false)
                {
                    AddMessage("You must agree to the Terms and Conditions.");
                }
            }
        }
    }
}
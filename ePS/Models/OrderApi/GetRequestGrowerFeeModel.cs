﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataServiceLibrary;

namespace ePS.Models.OrderApi
{
    public class GetRequestGrowerFeeModel : RequestBase
    {
        public Guid GrowerOrderGuid { get; set; }
        
       

        public override void Validate()
        {
            //ToDo: Scott, I added this code here.  You will want to build this out to validate the Supplier Order as well.
            if (this.GrowerOrderGuid == Guid.Empty)
            {
                AddMessage("GrowerOrderGuid is required.");
            };

           
        }
    }
}
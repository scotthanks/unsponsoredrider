﻿using BusinessObjectServices;
using ePS.Models.CartApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectsLibrary;

namespace ePS.Models.OrderApi
{
    public class AddOrUpdateOrderLineResponseModel : ResponseBase
    {
        public AddOrUpdateOrderLineResponseModel(Guid userGuid, AddOrUpdateOrderLineRequestModel request)
            : base( userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {

                var growerOrderGuid = new Guid();
                var growerOrderService = new GrowerOrderService();
               
                var statusList = new List<OrderLineStatusType>();

                foreach (OrderLineUpdate orderLineUpdate in request.OrderLineUpdates)
                {
                    var service = new GrowerOrderService();
                    var returnData = service.OrderLineUpdate(userGuid, orderLineUpdate.Guid, orderLineUpdate.Quantity, 0, "Supplier", out growerOrderGuid);
                   
                  
                    statusList.Add
                        (
                            new OrderLineStatusType
                            {
                                OrderLineGuid = orderLineUpdate.Guid,
                                Quantity = returnData.Quantity,
                                Success = returnData.Success,
                                Message = returnData.Message
                            }
                        );

                 

                }

                OrderLineStatuses = statusList.ToArray();

             


                Success = true;
            }
        }

        public OrderLineStatusType[] OrderLineStatuses { get; set; }
    }
}
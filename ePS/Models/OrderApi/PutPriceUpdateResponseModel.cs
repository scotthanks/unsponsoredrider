﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;

namespace ePS.Models.OrderApi
{
    public class PutPriceUpdateResponseModel : ResponseBase
    {
        public PutPriceUpdateResponseModel(Guid userGuid, PutPriceUpdateRequestModel request)
            :base(userGuid, requiresAuthentication: true)
        {
            PricesUpdated = 0;

            if (ValidateAuthenticationAndRequest(request))
            {
                var growerOrderService = new GrowerOrderService();
                if (request.OrderLineGuid == Guid.Empty)
                {
                    PricesUpdated = growerOrderService.UpdatePrices(userGuid,request.GrowerOrderGuid, runOnNewThread: false);
                }
                else
                {
                    PricesUpdated = growerOrderService.UpdatePrice(userGuid,request.GrowerOrderGuid, request.OrderLineGuid);
                }
                Success = true;
            }
        }

        public int PricesUpdated { get; set; }
    }
}
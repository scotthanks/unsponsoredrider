﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;

using BusinessObjectsLibrary;

namespace ePS.Models.OrderApi
{
    public class GetShipToAddressResponseModel
    {
        public GetShipToAddressResponseModel(BusinessObjectsLibrary.GrowerShipToAddress shipToAddress)
        {
            ShipToAddress = new ShipToAddressType()
                {
                    GrowerAddressGuid = shipToAddress.GrowerAddressGuid,
                    AddressGuid = shipToAddress.AddressGuid,
                    Name = shipToAddress.AddressName,
                    StreetAddress1 = shipToAddress.StreetAddress1,
                    StreetAddress2 = shipToAddress.StreetAddress2,
                    City = shipToAddress.City,
                    StateCode = shipToAddress.State.StateCode,
                    StateName = shipToAddress.State.StateName,
                    Country = shipToAddress.State.Country.CountryName,
                    CountryCode = shipToAddress.State.Country.CountryCode,
                    ZipCode = shipToAddress.ZipCode,
                    SpecialInstructions = shipToAddress.SpecialInstructions,
                    SellerCustomerID = shipToAddress.SellerCustomerID,
                    IsDefault = shipToAddress.IsDefault
                };
        }

        public ShipToAddressType ShipToAddress { get; private set; }
    }
}
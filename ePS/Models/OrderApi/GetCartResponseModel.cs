﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;

namespace ePS.Models.OrderApi
{
    public class GetCartResponseModel : ResponseBase
    {
        public GetCartResponseModel(Guid userGuid)
            :base(userGuid, requiresAuthentication: true)
        {
           
            var status = new StatusObject(userGuid,true);
            var cartService = new CartService(status);

            //Populate List
            var cartSummaryList = cartService.GetCartOrderSummaries();


         //   List<CartOrderSummary> sortedCartSummaryList = cartSummaryList.OrderBy(x => x.OrderID).ToList();
            SummaryData = cartSummaryList;

                
            //Get the Person read status
            PersonService personService = new PersonService();
            var userPerson = personService.GetPersonFromUser(userGuid);
            BusinessObjectServices.UserProfileService service = new BusinessObjectServices.UserProfileService();
             
            var message = "";
            var profile = service.GetUserProfile( userPerson.Email,out message);
            var currentRoleObj = service.GetUserRole(profile.UserId);
            if (currentRoleObj.RoleName == "CustomerAdministrator" || currentRoleObj.RoleName == "CustomerOrderer")
            { IsReadOnly = false; }
            else
            { IsReadOnly = true; }

            Success = true;
            
        }

        public IEnumerable<CartOrderSummary> SummaryData { get; set; }
        public bool IsReadOnly{ get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;
using System.IO;
using BusinessObjectServices;



namespace ePS.Models.OrderApi
{
    public class GetOrderPDFResponseModel : ResponseBase
    {
       // public MemoryStream OrderPDF { get; private set; }
        public string urlPDF;
        public GetOrderPDFResponseModel(Guid userGuid,Guid orderGuid)
            : base(userGuid, requiresAuthentication: true)
        {

            var service = new PDFService();
            //MemoryStream x = service.GetOrderPDF(orderGuid);
            string x = service.GetOrderPDF(orderGuid);




            this.urlPDF = x;

            Success = true;
            
            
        }
    }

  
    
}
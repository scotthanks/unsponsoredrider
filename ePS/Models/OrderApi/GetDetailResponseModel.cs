﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;

namespace ePS.Models.OrderApi
{
    public class GetDetailResponseModel : ResponseBase
    {
        public GrowerOrderType GrowerOrder { get; private set; }

        public GetDetailResponseModel(Guid userGuid, GetDetailRequestModel detailRequest)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(detailRequest))
            {
                var service = new BusinessObjectServices.GrowerOrderService();
                var growerOrder = service.GetGrowerOrderDetail(detailRequest.OrderGuid, includePriceData: false);

                if (growerOrder != null)
                {
                    var responseModel = new GetGrowerOrderResponseModel(growerOrder);

                    this.GrowerOrder = responseModel.GrowerOrder;

                    Success = true;
                }
            }
        }
    }

    public class GetResponseDetailModelV2 : ResponseBase
    {
        public GetResponseDetailModelV2(Guid userGuid, GetRequestDetailModelV2 request)
            :base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                var service = new BusinessObjectServices.GrowerOrderService();
                var growerOrder = service.GetGrowerOrderDetail(request.OrderGuid, includePriceData: false);

                var responseModel = new GetGrowerOrderResponseModel(growerOrder);
                this.GrowerOrder = responseModel.GrowerOrder;

                Success = true;
            }
        }

        public GrowerOrderType GrowerOrder { get; private set; }
    }
}
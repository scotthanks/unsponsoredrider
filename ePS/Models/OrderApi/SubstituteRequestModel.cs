﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.OrderApi
{
    public class SubstituteRequestModel : RequestBase
    {
        public Guid ProductGuid { get; set; }
        public Guid OrderLineGuid { get; set; }
        public string ShipWeek { get; set; }

        public override void Validate()
        {
            if (ProductGuid == Guid.Empty)
            {
                AddMessage("ProductGuid is required.");
            } 
            if (OrderLineGuid == Guid.Empty)
            {
                AddMessage("OrderLineGuid is required.");
            }
        }
    }
}
﻿using BusinessObjectsLibrary;
using ePS.Types;

namespace ePS.Models.OrderApi
{
    public class GetSelectListItemResponseModel
    {
        public GetSelectListItemResponseModel(Lookup lookup)
        {
            SelectListItem = new SelectListItemType
                {
                    Guid = lookup.Guid,
                    Code = lookup.Code,
                    Name = lookup.Name,
                    DisplayName = lookup.DisplayName,
                };
        }

        public SelectListItemType SelectListItem { get; private set; }

        public bool IsDefault
        {
            get { return SelectListItem.IsDefault; }
            set { SelectListItem.IsDefault = value; }
        }

        public bool IsSelected
        {
            get { return SelectListItem.IsSelected; }
            set { SelectListItem.IsSelected = value; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;

namespace ePS.Models.OrderApi
{
    public class GetOpenOrderSummary : ResponseBase
    {
        public GetOpenOrderSummary(Guid userGuid)
            :base(userGuid, requiresAuthentication: true)
        {
           
            //Retrieve Data from Data Server
            var status = new StatusObject(userGuid,true);
            var cartDataService = new DataServiceLibrary.CartDataService(status);
            List<OrderSummary2> orderSummaryList = cartDataService.GetOpenOrders();

     //       List<OrderSummary2> sortedorderSummaryList = orderSummaryList.OrderBy(x => x.OrderNo).ToList();
            SummaryData = orderSummaryList;

                               
            PersonService personService = new PersonService();
            var userPerson = personService.GetPersonFromUser(userGuid);


            BusinessObjectServices.UserProfileService service = new BusinessObjectServices.UserProfileService();
             
            var message = "";
            var profile = service.GetUserProfile( userPerson.Email,out message);
            var currentRoleObj = service.GetUserRole(profile.UserId);
            if (currentRoleObj.RoleName == "CustomerAdministrator" || currentRoleObj.RoleName == "CustomerOrderer")
            { IsReadOnly = false; }
            else
            { IsReadOnly = true; }


            Success = true;
        }
        

        public List<OrderSummary2> SummaryData { get; set; }
        public bool IsReadOnly{ get; set; }
    }
}
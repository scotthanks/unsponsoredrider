﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;
using AdminAppApiLibrary;
using BusinessObjectsLibrary;

namespace ePS.Models.OrderApi
{
    public class GetResponseGrowerOrderTotalsModel : ResponseBase
    {
        public GetResponseGrowerOrderTotalsModel(Guid userGuid, GetRequestGrowerOrderTotalsModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                int Carted = 0;
                int Uncarted = 0;
                Guid growerOrderGuid  = request.GrowerOrderGuid;
                   var service = new BusinessObjectServices.GrowerOrderService();
                   var oGrowerOrder = new GrowerOrder(); 

                   if (growerOrderGuid  == Guid.Empty )
                   {
                       //Check for order based on params
                    oGrowerOrder = service.GetGrowerOrder(userGuid,request.ShipWeekString,request.Category,request.Form);
                    growerOrderGuid = oGrowerOrder.OrderGuid;
                   }

                  
                   if (growerOrderGuid  !=  Guid.Empty)
                   {
                       //Order found so use it
                       bool bRet = false;
                       bRet = service.GetOrderTotals(growerOrderGuid, out Carted, out Uncarted);
                      
                       
                   }
                   else
                   {
                       //No Order started
                       
                   }

                   this.Carted = Carted;
                   this.Uncarted = Uncarted;
                   this.Total = Carted + Uncarted;

                
                this.Success = true;
                            
                
            }
        }
        
        public int Carted { get; private set; }
        public int Uncarted { get; private set; }
        public int Total { get; private set; }
        
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;
using ePSAdmin.Models.OrderDetailApi;
using BusinessObjectsLibrary;
using BusinessObjectServices;
using DataServiceLibrary;

namespace ePS.Models.OrderApi
{
    public class SendConfirmation
    {
        
        public bool SendConfirmationEmail(Guid userGuid,Guid growerOrderGuid, string headerText, string bodyText, int updateLines,int addLines,int cancelLines)
        {
    
            bool Success = false;
            string emailText = "Email text generation failed.";
            string emailSubject = "Email subject generation failed.";




            var service = new BusinessObjectServices.GrowerOrderService();
            var growerOrder = service.GetGrowerOrderDetail(growerOrderGuid, includePriceData: false);

            try
            {
                emailSubject = string.Format("Green Fuse Botanicals Order - {0}", growerOrder.OrderNo);  
                if (growerOrder.CustomerPoNo != "")
                {
                    emailSubject += "  , PO Number: " + growerOrder.CustomerPoNo;
                }


                string shipWeek = growerOrder.ShipWeek.ShipWeekString;
                var responseModel = new GetGrowerOrderResponseModel(growerOrder);
                // string emailHtmlPath = HttpContext.Current.Server.MapPath("~/Views/CustomerEmail/GrowerAckEmail.html");
                string emailHtmlPath = HttpContext.Current.Server.MapPath("~/Views/CustomerEmail/GrowerOrderEmail.html");

                EmailModel emailModel = new EmailModel(-1, shipWeek, emailHtmlPath, responseModel.GrowerOrder, new OrderStatusNameService(), headerText, bodyText);
                emailText = emailModel.ApplyTemplate();
                Success = true;
            }
            catch (Exception ex)
            {
                Success = false;
                string errMsg = ex.Message;
           
            }
            var iDelay = 5;
            var emailType = "Order Update";
            if(headerText == "ORDER RECEIVED")
            {
                emailType = "Order Received";
                iDelay = 0;
            }
          
            var status = new StatusObject(userGuid, false);
            var emailService = new EmailQueueService(status);
            var bRet = emailService.AddEmailtoQueue(emailSubject, emailText, iDelay, emailType, "confirmations@green-fuse.com", growerOrder.PersonWhoPlacedOrder.Email, growerOrder.Grower.AdditionalOrderEmail, "", growerOrderGuid, updateLines, addLines, cancelLines);


            return Success;
        }


       
    }
}
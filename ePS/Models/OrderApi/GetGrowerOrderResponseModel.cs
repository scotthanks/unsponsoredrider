﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using BusinessObjectsLibrary;
using ePS.Types;
using AdminAppApiLibrary;

namespace ePS.Models.OrderApi
{
    public class GetGrowerOrderResponseModel
    {
        public GetGrowerOrderResponseModel(GrowerOrder growerOrder)
        {
            GrowerOrder = new GrowerOrderType
                {
                    GrowerCode = growerOrder.Grower.GrowerCode,
                    GrowerName = growerOrder.Grower.GrowerName,
                    GrowerPhone = growerOrder.Grower.PhoneNumber.FormattedPhoneNumber,
                    GrowerEmail = growerOrder.Grower.EMail,
                    GrowerAdditionalAccountingEmail = growerOrder.Grower.AdditionalAccountingEmail,
                    GrowerAdditionalOrderEmail = growerOrder.Grower.AdditionalOrderEmail,
                    GrowerAllowsSubstitutions = growerOrder.Grower.AllowSubstitutions,

                    OrderGuid = growerOrder.OrderGuid,
                    OrderNo = growerOrder.OrderNo,
                    ShipWeekCode = growerOrder.ShipWeek.Code,
                    CustomerPoNo = growerOrder.CustomerPoNo,
                    PromotionalCode = growerOrder.PromotionalCode,
                    OrderDescription = growerOrder.OrderDescription,
                    //DateEntered = growerOrder.DateEntered,
                    DateEnteredString = growerOrder.DateEntered.ToString(CultureInfo.InvariantCulture),

                    OrderType = growerOrder.OrderType,
                    ProgramTypeCode = growerOrder.ProgramTypeCode,
                    ProgramTypeName = growerOrder.ProgramTypeName,
                    ProductFormCategoryCode = growerOrder.ProductFormCategoryCode,
                    ProductFormCategoryName = growerOrder.ProductFormCategoryName,
                    NavigationProgramTypeCode = growerOrder.NavigationProgramTypeCode,
                    NavigationProductFormCategoryCode = growerOrder.NavigationProductFormCategoryCode,

                    GrowerOrderStatusCode = growerOrder.GrowerOrderStatus.Code,
                    GrowerOrderStatusName = growerOrder.GrowerOrderStatus.Name,

                    PersonWhoPlacedOrder = new GetPersonResponseModel(growerOrder.PersonWhoPlacedOrder).PersonType,

                    SupplierOrders =
                        growerOrder.SupplierOrders.Select(
                            supplierOrder => new GetResponseSupplierOrderModel(supplierOrder).SupplierOrder).ToArray(),
                };

            var paymentTypeList = new List<SelectListItemType>();
            foreach (var paymentTypeBusinessObject in growerOrder.Grower.DefaultPaymentType.SiblingList)
            {
                var paymentType = new GetSelectListItemResponseModel(paymentTypeBusinessObject);

                if (paymentTypeBusinessObject.Guid == growerOrder.Grower.DefaultPaymentType.Guid)
                    paymentType.IsDefault = true;

                if (paymentTypeBusinessObject.Guid == growerOrder.PaymentType.Guid)
                    paymentType.IsSelected = true;

                paymentTypeList.Add(paymentType.SelectListItem);
            }
            GrowerOrder.PaymentTypeList = paymentTypeList.ToArray();

            var creditCardList = new List<CreditCardType>();
            foreach (var creditCardBusinessObject in growerOrder.Grower.CreditCardList)
            {
                var creditCard = new GetCreditCardResponseModel(creditCardBusinessObject).CreditCard;
                creditCardList.Add(creditCard);

                //TODO: There is no default credit card stored in the system.
                creditCard.IsDefault = false;

                if (creditCard.Guid == growerOrder.PaymentCreditCard.Guid)
                {
                    creditCard.IsSelected = true;
                }
            }
            GrowerOrder.CreditCards = creditCardList.ToArray();

            var shipToAddressList = new List<ShipToAddressType>();
            bool defaultShipToSelected = false;
            ShipToAddressType alternateDefaultShipTo = null;
            foreach (var shipToAddressBusinessObject in growerOrder.Grower.ShipToAddressList)
            {
                var shipToAddress = new GetShipToAddressResponseModel(shipToAddressBusinessObject).ShipToAddress;
                shipToAddressList.Add(shipToAddress);

                if (shipToAddress.IsDefault)
                {
                    defaultShipToSelected = true;
                }

                if (shipToAddress.AddressGuid == growerOrder.Grower.Address.AddressGuid)
                {
                    alternateDefaultShipTo = shipToAddress;
                }

                if (shipToAddress.GrowerAddressGuid == growerOrder.ShipToAddressGuid)
                {
                    shipToAddress.IsSelected = true;
                }
            }

            if (!defaultShipToSelected && alternateDefaultShipTo != null)
            {
                alternateDefaultShipTo.IsDefault = true;
            }

            GrowerOrder.ShipToAddresses = shipToAddressList.ToArray();

            var logList = new List<LogType>();
            foreach (var log in growerOrder.GrowerOrderHistoryEvents)
            {
                logList.Add(new LogType()
                {
                    Guid = log.Guid,
                    //LogTime = log.EventTime,
                    LogTimeString = log.EventTime.ToString("g", CultureInfo.CreateSpecificCulture("en-us")),
                    Text = log.Comment,
                    LogTypeLookupCode = log.LogTypeLookupCode,
                    IsInternal = log.IsInternal,
                    PersonName = log.PersonName,
                    UserCode = log.UserCode,
                });
            }
            GrowerOrder.Logs = logList.ToArray();

            GrowerOrder.GrowerOrderFeeList = new GrowerOrderFeeType[growerOrder.GrowerOrderFeeList.Count];
            int arryIndex = 0;
            foreach (var growerFee in growerOrder.GrowerOrderFeeList) 
            {
                GrowerOrderFeeType fee = new GrowerOrderFeeType();
                fee.GrowerOrderGuid = growerFee.GrowerOrderGuid;
                fee.Guid = growerFee.Guid;
                fee.Amount = growerFee.Amount;
                fee.FeeDescription = growerFee.FeeDescription;
                fee.GrowerOrderFeeStatusCode = growerFee.GrowerOrderFeeStatus.Code;
                fee.GrowerOrderFeeStatusDescription = growerFee.GrowerOrderFeeStatus.Description;
                fee.GrowerOrderFeeTypeCode = growerFee.GrowerOrderFeeType.Code;
                fee.GrowerOrderFeeTypeDescription = growerFee.GrowerOrderFeeType.Description;
                GrowerOrder.GrowerOrderFeeList[arryIndex++] = fee;
            }
        }

        public GrowerOrderType GrowerOrder { get; private set; }
    }
}
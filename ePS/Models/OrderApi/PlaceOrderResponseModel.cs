﻿using System;
using System.Collections.Generic;
using BusinessObjectServices;
using DataServiceLibrary;
using ePS.Types;
using System.Web;
using ePSAdmin.Models.OrderDetailApi;

namespace ePS.Models.OrderApi
{
    public class PlaceOrderResponseModel : ResponseBase
    {
        public PlaceOrderResponseModel(Guid userGuid, PlaceOrderRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            Success = true;
            //TODO: Remove this test once it is no longer needed.
            if (request.OrderTransitionTypeLookupCode.StartsWith(LookupTableValues.Code.OrderTransitionType.GrowerNotified.Code) && request.OrderTransitionTypeLookupCode != LookupTableValues.Code.OrderTransitionType.GrowerNotified.Code)
            {
                AddMessage(string.Format( "The OrderTransitionTypeLookupCode \"{0}\" is depricated. Use \"{1}\" instead.", request.OrderTransitionTypeLookupCode, LookupTableValues.Code.OrderTransitionType.GrowerNotified.Code));

                // Gib TBD request.OrderTransitionTypeLookupCode = LookupTableValues.Code.OrderTransitionType.GrowerNotified.Code;
            }

            if (ValidateAuthenticationAndRequest(request))
            {
                GrowerOrderGuid = request.GrowerOrderGuid;

                var growerOrderService = new GrowerOrderService();

                BusinessObjectServices.GrowerOrderService.ChangeStatusReturnValues result = growerOrderService.
                    ChangeOrderLineStatuses
                    (
                        userGuid,
                        request.UserCode,
                        request.OrderTransitionTypeLookupCode,
                        request.GrowerOrderGuid,
                        new Guid(),
                         new Guid()
                    );

                if (result.OrderLinesChanged <= 0)
                    Success = false;

               

                //TODO: This should be handled in the business logic, but it can't be because of the way it gets the email HTML from the site.
                if (request.OrderTransitionTypeLookupCode == LookupTableValues.Code.OrderTransitionType.PlaceOrder.Code)
                {
                    string headerText = "ORDER RECEIVED";
                    string bodyText = "Thank you for placing your order with Green Fuse Botanicals. We have received your order and will notify you when we have final confirmation from our farm.";
                    var emailer = new SendConfirmation();
                    Success = emailer.SendConfirmationEmail(userGuid,request.GrowerOrderGuid, headerText, bodyText,0,0,0);
                    
                                      
                    result = growerOrderService.ChangeOrderLineStatuses
                    (
                        userGuid,
                        request.UserCode,
                        "GrowerNotified",
                        request.GrowerOrderGuid,
                        new Guid(),
                        new Guid()
                    );
                    if (result.OrderLinesChanged <= 0)
                        Success = false;
                }
            }
        }

       

        public Guid GrowerOrderGuid { get; set; }

        //TODO: OrderLinesChanged is not returned by the procedure yet.
        public int OrderLinesChanged { get; set; }

        public int ProductsOrdered { get; set; }
        public int PrecartOrderLinesDeleted { get; set; }

        public SupplierOrderStatus[] SupplierOrderStatuses { get; set; }

        public class SupplierOrderStatus
        {
            public Guid SupplierOrderGuid { get; set; }
            public bool Success { get; set; }
            public string Message { get; set; }

            public OrderLineStatus[] OrderLineStatuses { get; set; }

            public class OrderLineStatus
            {
                public Guid OrderLineGuid { get; set; }
                public bool Success { get; set; }
                public string Message { get; set; }
                public int Quantity { get; set; }
            }
        }
    }
}
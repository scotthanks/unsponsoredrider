﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;

using BusinessObjectsLibrary;
using System.Globalization;

namespace ePS.Models.OrderApi
{
    public class GetOrderLineResponseModel
    {
        public GetOrderLineResponseModel(BusinessObjectsLibrary.OrderLine orderLine)
        {
            ////TODO: Implement these for real. (Dan).
            //bool isLockedForReduction = Utility.Random.RandomBool;
            //bool isLockedForAllChanges = Utility.Random.RandomBool;


            ////If it's locked for all changes, reduction is not valid either.
            //if (isLockedForAllChanges)
            //{
            //    isLockedForReduction = true;
            //}

            OrderLine = new OrderLineType()
                {
                    Guid = orderLine.Guid,
                    ProductFormName = orderLine.Product.ProductFormName,
                    ProductDescription = orderLine.Product.ProductDescription,
                    SpeciesCode = orderLine.Product.SpeciesCode,
                    SpeciesName = orderLine.Product.SpeciesName,
                    VarietyCode = orderLine.Product.VarietyCode,
                    VarietyName = orderLine.Product.VarietyName,
                    Multiple = orderLine.Product.SalesUnitQty,
                    Minumum = orderLine.Product.LineItemMinumumQty,
                    QuantityOrdered = orderLine.QuantityOrdered,
                    IsCarted = orderLine.IsCarted,
                    Price = orderLine.Price,
                    AvailableQty = orderLine.AvailableQty,
                    AvailabilityTypeCode = orderLine.AvailabilityTypeCode,
                    OrderLineStatusCode = orderLine.OrderLineStatus.Code,
                    OrderLineStatusName = orderLine.OrderLineStatus.Name,
                    IsLockedForReduction = orderLine.IsLockedForReduction,
                    IsLockedForAllChanges = orderLine.IsLockedForAllChanges,
                    QuantityShipped = orderLine.QuantityShipped,
                    DateEnteredString = orderLine.DateEnteredString,
                    DateChangedString = orderLine.DateChangedString,
                    DateQtyChangedString = orderLine.DateQtyChangedString,
                };

            var logList = new List<LogType>();
            foreach (var log in orderLine.GrowerOrderHistoryEvents)
            {
                logList.Add(new LogType()
                {
                    Guid = log.Guid,
                    //LogTime = log.EventTime,
                    LogTimeString = log.EventTime.ToString("g", CultureInfo.CreateSpecificCulture("en-us")),
                    Text = log.Comment,
                    LogTypeLookupCode = log.LogTypeLookupCode,
                    IsInternal = log.IsInternal,
                    PersonName = log.PersonName,
                    UserCode = log.UserCode
                });
            }
            OrderLine.Logs = logList.ToArray();
        }

        public OrderLineType OrderLine { get; set; }

        //TODO: Remove all of these.
        public Guid Guid { get; set; }
        public string ProductDescription { get; set; }
        public int Multiple { get; set; }
        public string SpeciesName { get; set; }
        public string VarietyName { get; set; }
        public int QuantityOrdered { get; set; }
        public bool? IsCarted { get; set; }
        public decimal Price { get; set; }
    }
}
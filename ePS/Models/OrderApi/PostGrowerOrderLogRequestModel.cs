﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.OrderApi
{
    public class PostGrowerOrderLogRequestModel : RequestBase
    {
        public Guid GrowerOrderGuid { get; set; }
        public string LogTypeLookupCode { get; set; }
        public string Text { get; set; }

        public override void Validate()
        {
            if (GrowerOrderGuid == Guid.Empty)
            {
                AddMessage("GrowerOrderGuid is required.");
            }

            if (string.IsNullOrEmpty(LogTypeLookupCode))
            {
                AddMessage("LogTypeLookupCode is required.");
            }
            else
            {
                var growerLogTypes = DataServiceLibrary.LookupTableValues.Logging.LogType.Grower;
                if (LogTypeLookupCode != growerLogTypes.CommentExternal.Code &&
                    LogTypeLookupCode != growerLogTypes.CommentInternal.Code)
                {
                    AddMessage("This LogTypeLookupCode is not supported.");
                }
            }

            if (string.IsNullOrEmpty(Text))
            {
                AddMessage("Text is required.");
            }
        }
    }
}
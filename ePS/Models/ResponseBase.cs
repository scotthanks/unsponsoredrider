﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models
{
    public class ResponseBase
    {
        private readonly bool _requiresAuthentication;
        public bool IsAuthenticated { get; set; }
        public bool Success { get; set; }

        private readonly List<string> _messageList;
 
        public ResponseBase(Guid userGuid, bool requiresAuthentication)
        {
            _requiresAuthentication = requiresAuthentication;

            _messageList = new List<string>();

            if (userGuid != Guid.Empty)
            {
                IsAuthenticated = true;
            }
        }

        public bool ValidateAuthenticationAndRequest(RequestBase request)
        {
            bool isValidAndAuthenticated = false;

            if (_requiresAuthentication && !IsAuthenticated)
            {
                AddMessage("You are not authorized for this request.");
            }
            else
            {
                request.Validate();

                if (request.IsValid)
                {
                    isValidAndAuthenticated = true;
                }
                else
                {
                    foreach (var message in request.Messages)
                    {
                        AddMessage(message);
                    }
                }
            }

            return isValidAndAuthenticated;
        }

        public bool ValidateAuthenticationAndGuid(Guid guid, string guidName)
        {
            bool isValidAndAuthenticated = false;

            if (_requiresAuthentication && !IsAuthenticated)
            {
                AddMessage("You are not authorized for this request.");
            }
            else
            {
                if (guid == Guid.Empty)
                {
                    AddMessage(string.Format("{0} is required.", guidName));
                }
                else
                {
                    isValidAndAuthenticated = true;
                }
            }

            return isValidAndAuthenticated;
        }

        public void AddMessage(string message)
        {
            _messageList.Add(message);
        }

        public string[] Messages
        {
            get { return _messageList.ToArray(); }
        }

        [System.Obsolete]
        public string Message
        {
            get
            {
                if (_messageList == null || _messageList.Count == 0)
                {
                    return "";
                }

                return _messageList[0];
            }
        }
    }
}
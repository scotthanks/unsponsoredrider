﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models
{
    public class ShipWeekApiRequest
    {
        public string PlantCategoryCode { get; set; }
        public string ProductFormCode { get; set; }
        public string SupplierCodes { get; set; }
        public string VarietyCode { get; set; }
        public string ShipWeekString { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;

namespace ePS.Models.SupplierOrderFeeApi
{
    public class PutRequestModel : RequestBase
    {
        public Guid SupplierOrderFeeGuid { get; set; }
        public decimal Amount { get; set; }

        public override void Validate()
        {
            if (SupplierOrderFeeGuid == Guid.Empty)
            {
                AddMessage("SupplierOrderFeeGuid is required.");
            }

            /// alow negative fee to be entered as a discount
            //if (Amount < 0)
            //{
            //    AddMessage("The Amount must be greater than 0.");
            //}
        }
    }
}
﻿using AdminAppApiLibrary;
using ePS.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.SupplierOrderFeeApi
{
    public class PutResponseModel : ResponseBase
    {
        public SupplierOrderFeeType SupplierOrderFeeType { get; set; }

        public PutResponseModel(Guid userGuid, PutRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                BusinessObjectServices.GrowerOrderService growerOrderService = new BusinessObjectServices.GrowerOrderService();
                var supplierFee = growerOrderService.UpdateSupplierFee(request.SupplierOrderFeeGuid, request.Amount);

                this.SupplierOrderFeeType = new SupplierOrderFeeType();
                SupplierOrderFeeType.Guid = supplierFee.Guid;
                SupplierOrderFeeType.SupplierOrderGuid = supplierFee.SupplierOrderGuid;
                SupplierOrderFeeType.SupplierOrderFeeTypeCode = supplierFee.SupplierOrderFeeType.Code;
                SupplierOrderFeeType.SupplierOrderFeeTypeDescription = supplierFee.SupplierOrderFeeType.Description;
                SupplierOrderFeeType.Amount = supplierFee.Amount;
                Success = true;                
            }
        }    
    }
}
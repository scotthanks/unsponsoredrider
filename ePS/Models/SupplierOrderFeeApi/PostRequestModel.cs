﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;

namespace ePS.Models.SupplierOrderFeeApi
{
    public class PostRequestModel : RequestBase
    {
        public Guid SupplierOrderGuid { get; set; }
        public String SupplierOrderFeeTypeCode { get; set; }
        public String SupplierOrderFeeStatusCode { get; set; }
        public Decimal Amount { get; set; }
        public String FeeDescription { get; set; }

        public override void Validate()
        {
            if (SupplierOrderGuid == Guid.Empty)
            {
                AddMessage("SupplierOrderGuid is required.");
            }

            if (string.IsNullOrEmpty(SupplierOrderFeeTypeCode))
            {
                AddMessage("SupplierOrderFeeTypeCode is required.");
            }

            if (Amount < 0)
            {
                AddMessage("The Amount must be greater than 0.");
            }
        }
    }
}
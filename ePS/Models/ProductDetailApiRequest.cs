﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models
{
    public class ProductDetailApiRequest
    {
        public string UserHandle { get; set; }
        public Guid UserGuid { get; set; }
        public string PlantCategoryCode { get; set; }
        public string ProductFormCode { get; set; }
        public string SupplierCodes { get; set; }
        public string VarietyCode { get; set; }
        public string ShipWeekString { get; set; }

    }
}
﻿using System.Collections.Generic;

namespace ePS.Models
{
    public abstract class RequestBase
    {
        private readonly List<string> _messageList;
        protected RequestBase()
        {
            _messageList = new List<string>();
        }
        public abstract void Validate();
        public void AddMessage(string message)
        {
            _messageList.Add(message);
        }

        public bool IsValid
        {
            get
            {
                return _messageList.Count == 0;
            }
        }
        public string[] Messages
        {
            get
            {
                return _messageList.ToArray();
            }
        }

        public string UserCode { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using BusinessObjectServices;

namespace ePS.Models
{
    public class CatalogSearchPageModel
    {
        public CatalogSearchPageModel(string categoryKey, string formKey)
        {
            
            var service = new CatalogueService();

            var category = service.Catalogue(categoryKey);
            this.CategoryKey = category.Key;
            this.CategoryName = category.Name;

            var form = service.ProductForm(formKey);
            this.FormKey = formKey;
            this.FormName = form.Name;
        
        }

        public string CategoryKey { get; set; }
        public string CategoryName { get; set; }
        public string FormKey { get; set; }
        public string FormName { get; set; }

    }
}
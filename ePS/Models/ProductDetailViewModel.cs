﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using BusinessObjectsLibrary;


namespace ePS.Models
{
    public class ProductDetailViewModel
    {
        public ProductDetailViewModel(string plantCategoryCode, string productFormCode, string supplierCodes, string varietyCode, string shipWeek)
        {
            this.PlantCategoryCode = plantCategoryCode;
            this.ProductFormCode = productFormCode;
            this.SupplierCodes = supplierCodes;
            this.VarietyCode = varietyCode;
            this.ShipWeekString = shipWeek;

            var service = new VarietyService();
            var variety = service.GetVarietyByCode(this.VarietyCode);

            this.SpeciesName  = variety.SpeciesName;
            this.VarietyName = variety.Name;
            this.BreederName = variety.GeneticOwnerName;

            this.ColorDescription = variety.ColorDescription;
            this.Color = variety.Color;
            this.Habit = variety.Habit;
            this.Vigor = variety.Vigor;
            this.DescriptionHtmlGuid = variety.DescriptionHtmlGuid;
            this.VarietyImageUrl = variety.FUllImageUrl;

            this.PlantCategoryName = "TBD";
            this.ProductFormName = "TBD";


        
        }

        //Received Values
        public string PlantCategoryCode { get; set; }
        public string ProductFormCode { get; set; }
        public string SupplierCodes { get; set; }
        public string VarietyCode { get; set; }
        public string ShipWeekString { get; set; }

        //Retrieved Values
        public string PlantCategoryName { get; set; }
        public string ProductFormName { get; set; }
        public string SpeciesName { get; set; }
        public string VarietyName { get; set; }
        public string BreederName { get; set; }
        public string ColorDescription { get; set; }
        public string Color { get; set; }
        public string Habit { get; set; }
        public string Vigor { get; set; }
        public Guid? DescriptionHtmlGuid { get; set; }
        public string VarietyImageUrl { get; set; }

        public Catalogue SelectedCategory { get; set; }
        public ProductFormCategory SelectedProductFormCategory { get; set; }

        public string Species { get; set; }
        
        public int VarietyCount { get; set; }
        public IEnumerable<Variety> VarietyCollection { get; set; }
        public ShipWeek DefaultShipWeek { get; set; }
        public ShipWeek SelectedShipWeek { get; set; }
        
        public ProductDetailViewModel(string productCategoryCode, string productFormCode, string supplierCodes, string varietyCode)
        {
            //ToDo: Change catalogueKey to categoryKey

            //ToDo: these should not just be passed through, should look up object
            this.VarietyCode = varietyCode;

            var service = new CatalogueService();

            productCategoryCode = productCategoryCode ?? string.Empty;
            productCategoryCode = productCategoryCode.Trim();
            if (productCategoryCode.Length > 0)
            {
                this.SelectedCategory = service.Catalogue(productCategoryCode);
            }
            else
            {
                this.SelectedCategory = new Catalogue();
            }
        
            productFormCode = productFormCode ?? string.Empty;
            productFormCode = productFormCode.Trim();
            if (productFormCode.Length > 0)
            {
                this.SelectedProductFormCategory = service.ProductForm(productFormCode);
            }
            else
            {
                this.SelectedProductFormCategory = new ProductFormCategory();
            }
            
            //ToDo: Code to set DefaultShipWeek
            this.DefaultShipWeek = service.ReturnShipWeek(6, 2013);
            this.SelectedShipWeek = service.ReturnShipWeek(6, 2013);
        }
    }
}
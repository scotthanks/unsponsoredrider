﻿using System;

namespace ePS.Models.CartUpdateApi
{
    // This request is for updating a single field of a GrowerOrder or a SupplierOrder.
    // To use it, supply three things:
    //  1. The FieldName of the field to update.
    //  2. The Guid of the GrowerOrder or SupplierOrder (depending on whether the field you are updating is on the GrowerOrder or SupplierOrder).
    //  3. The new value of the field, which could be a CustomerPoNo, an OrderDescription, a TagRatioLookupCode etc.

    public class CartOrderUpdateRequestModel
    {
        public string FieldName { get; set; }

        // Specify one or the other, but not both:
        public Guid GrowerOrderGuid { get; set; }
        public Guid SupplierOrderGuid { get; set; }

        // Enter a Guid for one and only one of the following GrowerOrder or SupplierOrder items to update:

        // Grower Order items.
        public string CustomerPoNo { get; set; }
        public string PromotionalCode { get; set; }
        public string OrderDescription { get; set; }
        public Guid ShipToAddressGuid { get; set; }
        public string PaymentTypeLookupCode { get; set; }
        public Guid CardOnFileGuid { get; set; }
        public bool AgreeToTerms { get; set; }

        // Supplier Order Items.
        public string TagRatioLookupCode { get; set; }
        public string ShipMethodLookupCode { get; set; }

        internal bool IsValid
        {
            get
            {
                FieldEnum field;

                bool isValid = Enum.TryParse(FieldName, out field);

                // Make sure that the right Guid (GrowerOrderGuid or SupplierOrderGuid is specified, but not both).
                if (isValid)
                {
                    if (OrderTypeToUpdate == OrderTypeToUpdateEnum.GrowerOrder)
                        isValid = GrowerOrderGuid != Guid.Empty && SupplierOrderGuid == Guid.Empty;
                    else
                        isValid = SupplierOrderGuid != Guid.Empty && GrowerOrderGuid == Guid.Empty;
                }

                // Make sure the item to be updated is not empty (unless that's allowed).
                if (isValid)
                {
                    switch (field)
                    {
                        case FieldEnum.CustomerPoNo:
                            // Allowed to be empty.
                            break;
                        case FieldEnum.PromotionalCode:
                            // Allowed to be empty.
                            break;
                        case FieldEnum.OrderDescription:
                            // Allowed to be empty.
                            break;
                        case FieldEnum.ShipToAddressGuid:
                            isValid = ShipToAddressGuid != Guid.Empty;
                            break;
                        case FieldEnum.PaymentTypeLookupCode:
                            isValid = !string.IsNullOrEmpty(PaymentTypeLookupCode);
                            break;
                        case FieldEnum.AgreeToTerms:
                            // Allowed to be empty.
                            break;
                        case FieldEnum.CardOnFileGuid:
                            isValid = CardOnFileGuid != Guid.Empty;
                            break;
                        case FieldEnum.TagRatioLookupCode:
                            isValid = !string.IsNullOrEmpty(TagRatioLookupCode);
                            break;
                        case FieldEnum.ShipMethodLookupCode:
                            isValid = !string.IsNullOrEmpty(ShipMethodLookupCode);
                            break;
                        default:
                            isValid = false;
                            break;
                    }

                    // Make sure only the right field is filled in.
                    if (isValid)
                    {
                        if (!string.IsNullOrEmpty(CustomerPoNo) && Field != FieldEnum.CustomerPoNo)
                            isValid = false;
                        if (!string.IsNullOrEmpty(PromotionalCode) && Field != FieldEnum.PromotionalCode)
                            isValid = false;

                        if (!string.IsNullOrEmpty(OrderDescription) && Field != FieldEnum.OrderDescription)
                            isValid = false;

                        if (ShipToAddressGuid != Guid.Empty && Field != FieldEnum.ShipToAddressGuid)
                            isValid = false;

                        if (!string.IsNullOrEmpty(PaymentTypeLookupCode) && Field != FieldEnum.PaymentTypeLookupCode)
                            isValid = false;

                        if (CardOnFileGuid != Guid.Empty && Field != FieldEnum.CardOnFileGuid)
                            isValid = false;

                        if (!string.IsNullOrEmpty(TagRatioLookupCode) && Field != FieldEnum.TagRatioLookupCode)
                            isValid = false;

                        if (!string.IsNullOrEmpty(ShipMethodLookupCode) && Field != FieldEnum.ShipMethodLookupCode)
                            isValid = false;
                    }
                }

                return isValid;
            }
        }

        internal enum FieldEnum
        {
            CustomerPoNo,
            PromotionalCode,
            OrderDescription,
            ShipToAddressGuid,
            PaymentTypeLookupCode,
            CardOnFileGuid,
            TagRatioLookupCode,
            ShipMethodLookupCode,
            AgreeToTerms
        }

        internal FieldEnum Field
        {
            get
            {
                FieldEnum field;
                Enum.TryParse(FieldName, out field);
                return field;
            }
        }

        internal enum OrderTypeToUpdateEnum
        {
            GrowerOrder,
            SupplierOrder
        }

        private OrderTypeToUpdateEnum OrderTypeToUpdate
        {
            get
            {
                OrderTypeToUpdateEnum orderTypeToUpdate = 0;

                switch (Field)
                {
                    // Grower Order Items.
                    case FieldEnum.CustomerPoNo:
                    case FieldEnum.PromotionalCode:
                    case FieldEnum.OrderDescription:
                    case FieldEnum.ShipToAddressGuid:
                    case FieldEnum.PaymentTypeLookupCode:
                    case FieldEnum.CardOnFileGuid:
                    case FieldEnum.AgreeToTerms:
                        orderTypeToUpdate = OrderTypeToUpdateEnum.GrowerOrder;
                        break;
                    // SupplierOrderItems.
                    case FieldEnum.TagRatioLookupCode:
                    case FieldEnum.ShipMethodLookupCode:
                        orderTypeToUpdate = OrderTypeToUpdateEnum.SupplierOrder;
                        break;
                    default:
                        throw new ApplicationException(string.Format("The type {0} is not supported.", orderTypeToUpdate.ToString()));
                        
                }

                return orderTypeToUpdate;
            }
        }
    }
}

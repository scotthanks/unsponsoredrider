﻿using System;
using System.Collections.Generic;
using BusinessObjectServices;
using DataServiceLibrary;

namespace ePS.Models.CartUpdateApi
{
    public class CartOrderUpdateResponseModel
    {
        public CartOrderUpdateResponseModel(Guid userGuid, CartOrderUpdateRequestModel request)
        {
            Message = "";
            //Response to Order Button on Cart Page

            if (userGuid == Guid.Empty)
            {
                IsAuthenticated = false;
                Message = "Not authenticated.";
            }
            else
            {
                IsAuthenticated = true;                    

                if (!request.IsValid)
                {
                    Success = false;
                    Message = "The request is not valid.";
                }
                else
                {
                    var growerOrderService = new GrowerOrderService();

                    switch (request.Field)
                    {
                        case CartOrderUpdateRequestModel.FieldEnum.CustomerPoNo:
                            Success = growerOrderService.UpdateCustomerPoNoOnOrder(request.GrowerOrderGuid, request.CustomerPoNo);
                            break;
                        case CartOrderUpdateRequestModel.FieldEnum.PromotionalCode:
                            Success = growerOrderService.UpdatePromotionalCodeOnOrder(userGuid,request.GrowerOrderGuid, request.PromotionalCode);
                            break;
                        case CartOrderUpdateRequestModel.FieldEnum.OrderDescription:
                            Success = growerOrderService.UpdateDescriptionOnOrder(request.GrowerOrderGuid, request.OrderDescription);
                            break;
                        case CartOrderUpdateRequestModel.FieldEnum.ShipToAddressGuid:
                            Success = growerOrderService.UpdateAddressOnOrder(request.GrowerOrderGuid, request.ShipToAddressGuid);
                            break;
                        case CartOrderUpdateRequestModel.FieldEnum.PaymentTypeLookupCode:
                            Success = growerOrderService.UpdatePaymentTypeOnOrder(request.GrowerOrderGuid, request.PaymentTypeLookupCode);
                            break;
                        case CartOrderUpdateRequestModel.FieldEnum.CardOnFileGuid:
                            Success = growerOrderService.UpdateCardOnOrder(request.GrowerOrderGuid, request.CardOnFileGuid);
                            break;
                        case CartOrderUpdateRequestModel.FieldEnum.AgreeToTerms:
                            Success = true;
                            if (!request.AgreeToTerms)
                            {
                               // Success = false;
                                Message = "You must agree to the Terms and Conditions.";                                
                            }
                            break;
                        case CartOrderUpdateRequestModel.FieldEnum.TagRatioLookupCode:
                            Success = growerOrderService.UpdateTagRatioLookupCode(userGuid,request.SupplierOrderGuid, request.TagRatioLookupCode);
                            break;
                        case CartOrderUpdateRequestModel.FieldEnum.ShipMethodLookupCode:
                            Success = growerOrderService.UpdateShipmentMethodLookupCode(userGuid,request.SupplierOrderGuid, request.ShipMethodLookupCode);
                            break;
                        default:
                            Success = false;
                            Message = string.Format("The field {0} is not supported.", request.Field.ToString());
                            break;
                    }
                }
            }
        }

        public bool IsAuthenticated { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using BusinessObjectsLibrary;
using BusinessObjectServices;

namespace ePS.Models.AvailabilityShipWeeksApi
{
    public class GetResponseModel : ResponseBase
    {
        public GetResponseModel(Guid userGuid, GetRequestModel request)
            : base(userGuid, requiresAuthentication: false)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                var service = new ShipWeekService();

             //   var shipWeekList = service.GetShipWeeks(userGuid, request.UserCode, service.FirstPossibleOrderShipWeek.Code, 52);
                //Changed to 78 weeks on Jan 20, 2015 from user meeting.
                var shipWeekList = service.GetShipWeeks(userGuid, request.UserCode, service.FirstPossibleOrderShipWeek.Code, 78);

                shipWeekList.Sort();

                this.ShipWeeksList = shipWeekList;

                if (string.IsNullOrEmpty(request.ShipWeek))
                {
                    var status = new StatusObject(userGuid, true);
                    var availabilityService = new AvailabilityService(status);
                    this.SelectedShipWeek = availabilityService.GetDefaultShipWeek(request.Category,request.Form);
                }
                else
                {
                    this.SelectedShipWeek = service.ParseShipWeekString(request.ShipWeek);
                }

                Success = true;
            }
        }

        public string StatusMessage { get; set; }
        public ShipWeek SelectedShipWeek { get; set; }
        public IEnumerable<ShipWeek> ShipWeeksList { get; set; }
    }
}
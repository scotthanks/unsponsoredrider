﻿
"use strict";

angular.module('catalogApp', ['ngTouch', 'ui.grid', 'ui.bootstrap', 'angularAwesomeSlider']);

angular.module('catalogApp').config(function($locationProvider) {
    //$locationProvider.html5Mode(true);      //.hashPrefix('!');
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
});

angular.module('catalogApp').run(function ($rootElement) {
    $rootElement.on('click', function(e) { e.stopPropagation(); });
});

angular.module('catalogApp').constant('EPS_API', {
    'CATEGORIES_GET': '/api/Catalog/Categories/',
    'PRODUCT_FORMS_GET': '/api/Catalog/ProductForms/',
    'SELECTIONS_GET': '/api/Catalog/Selections/',
    'SUPPLIERS_GET': '/api/Catalog/Suppliers/',
    'SPECIES_GET': '/api/Catalog/Species/',
    'VARIETIES_GET': '/api/Catalog/Varieties/',
    'PRODUCT_GET': '/api/Catalog/Variety/Detail/',

});


angular.module('catalogApp').controller('VarietyDetailController', function ($scope, $modalInstance, varietyDetailData) {

    $scope.varietyDetailData = varietyDetailData;

    //$scope.ok = function () {
    //    $modalInstance.close('value on close');
    //};

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});





//console.log('app.js loaded');

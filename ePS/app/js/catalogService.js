﻿
(function () {

    var catalogService = function($http, EPS_API, $rootScope, $q) {

        var self = this;

        //var groupProfitData = function (requestObj) {
        //    //console.log('requestObj: ', requestObj);
        //    return $http({
        //        url: GEOS_API.GROUP_REPORT_DATA,
        //        method: 'get',
        //        params: requestObj,
        //    }).then(function (response) {
        //        //console.log('reportData: ', response);
        //        return response.data;
        //    });
        //};

        //var groupProfitDataSummary = function (archiveYear, archiveMonth) {
        //    //console.log('groupProfitSummaryData: ', archiveYear, archiveMonth);
        //    return $http({
        //        url: GEOS_API.GROUP_REPORT_DATA_SUMMARY,
        //        method: 'get',
        //        params: {
        //            'archiveYear': archiveYear,
        //            'archiveMonth': archiveMonth
        //        },
        //    }).then(function (response) {
        //        //console.log('summaryData: ', response);
        //        return response.data;
        //    });
        //};

        //var groupProfitDataPayout = function (archiveYear, archiveMonth) {
        //    //console.log('groupProfitSummaryData: ', archiveYear, archiveMonth);
        //    return $http({
        //        url: GEOS_API.GROUP_REPORT_DATA_PAYOUT,
        //        method: 'get',
        //        params: {
        //            'archiveYear': archiveYear,
        //            'archiveMonth': archiveMonth
        //        },
        //    }).then(function (response) {
        //        //console.log('summaryData: ', response);
        //        return response.data;
        //    });
        //};

        //var uploadFile = function (fileName, file) {
        //    console.log('file=', file);
        //    var fd = new FormData();
        //    fd.append('FileName', fileName);
        //    fd.append('file', file, fileName);
        //    console.log('form data=', fd);
        //    return $http.post(GEOS_API.GROUP_REPORT_MAIL_PDF, fd, {
        //        timeout: 45000,
        //        transformRequest: angular.identity,
        //        headers: { 'Content-Type': undefined }
        //    }).then(function (response) {
        //        //console.log('reportData: ', response);
        //        return response;
        //    });
        //};

        //var latestArchiveDate = function () {
        //    //console.log('bingo');
        //    var reqObj = new dataRequestType(
        //        'LatestArchive',
        //        1,
        //        'Archive',
        //        '2015/12/31',
        //        '2015/12/31',
        //        null,
        //        null,
        //        null);
        //    //console.log(reqObj);

        //    //var deferred = $q.defer();
        //    //deferred.resolve(reqObj);

        //    //return deferred.promise;


        //    return groupProfitData(reqObj)
        //        .then(function (data) {
        //            //console.log('data=', data);
        //            return data;
        //        }
        //        ).then(
        //            function (data) {
        //                var date = new Date(data);
        //                var obj = {
        //                    date: date,
        //                    dateStr: data,
        //                    year: date.getUTCFullYear(),
        //                    month: date.getUTCMonth(),
        //                    day: date.getUTCDate()
        //                };
        //                //console.log('obj', obj);
        //                return obj;
        //            }
        //        );
        //};
        //var monthAbbr = function (date) {
        //    var arr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        //    var month = date.getMonth();
        //    return arr[month];
        //};

        //var categoriesGet = function() {
        //    //console.log('categoriesGet');
        //    return $http({
        //        url: EPS_API.CATEGORIES_GET,
        //        method: 'get',
        //        params: {},
        //    }).then(function(response) {
        //        //console.log('categoriesGet: ', response);
        //        return response.data;
        //    });
        //};
        //var productFormsGet = function(categoryCode) {
        //    //console.log('categoryCode: ', categoryCode);
        //    return $http({
        //        url: EPS_API.PRODUCT_FORMS_GET,
        //        method: 'get',
        //        params: { 'categoryCode': categoryCode },
        //    }).then(function(response) {
        //        //console.log('productFormsGet: ', response);
        //        return response.data;
        //    });
        //};

        ////NOTE: Variety detail calls the MVC controller to get back a partial page.


        //var suppliersGet = function(categoryCode, productFormCode) {
        //    var params = { 'categoryCode': categoryCode, 'productFormCode': productFormCode };
        //    //console.log(params);
        //    return $http({
        //        url: EPS_API.SUPPLIERS_GET,
        //        method: 'get',
        //        params: params,
        //    }).then(function(response) {
        //        //console.log('suppliersGet: ', response);
        //        return response.data;
        //    });
        //};

        //var speciesGet = function(categoryCode, productFormCode, supplierCodes) {
        //    var params = { 'categoryCode': categoryCode, 'productFormCode': productFormCode, 'supplierCodes': supplierCodes };
        //    //console.log('speciesGet', params);
        //    return $http({
        //        url: EPS_API.SPECIES_GET,
        //        method: 'get',
        //        params: params,
        //    }).then(function(response) {
        //        //console.log('speciesGet: ', response);
        //        return response.data;
        //    });
        //};

        //var varietiesGet = function(categoryCode, productFormCode, supplierCodes, speciesCodes) {
        //    var params = { 'categoryCode': categoryCode, 'productFormCode': productFormCode, 'supplierCodes': supplierCodes, 'speciesCodes': speciesCodes };
        //    //console.log('varietiesGet', params);
        //    return $http({
        //        url: EPS_API.VARIETIES_GET,
        //        method: 'get',
        //        params: params,
        //    }).then(function(response) {
        //        //console.log('varietiesGet: ', response);
        //        return response.data;
        //    });
        //};

        var dataGet = function (categoryCode, productFormCode) {
            var params = { 'categoryCode': categoryCode, 'productFormCode': productFormCode };
            //console.log(params);
            return $http({
                url: EPS_API.SELECTIONS_GET,
                method: 'get',
                params: params,
            }).then(function (response) {
                //console.log('suppliersGet: ', response);
                return response.data;
            });
        };

        var varietyDetailGet = function(varietyCode) {
            var params = { 'varietyCode': varietyCode };
            //console.log('varietyDetailGet params=', params);
            return $http({
                url: EPS_API.PRODUCT_GET,
                method: 'get',
                params: params,
            }).then(function(response) {
                //console.log('varietyDetailGet: ', response);
                return response.data;
            });
        };

        return {
            dataGet:dataGet,
            //categoriesGet: categoriesGet,
            //productFormsGet: productFormsGet,
            //suppliersGet: suppliersGet,
            //speciesGet: speciesGet,
            //varietiesGet: varietiesGet,
            varietyDetailGet: varietyDetailGet
        };
    };
    var module = angular.module('catalogApp');
    module.factory('catalogService', ['$http', 'EPS_API', '$rootScope', '$q', catalogService]);

}());


﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utility;
using FluentAssertions;

namespace UtilityTest
{
    [TestClass]
    public class PhoneNumberTests
    {
        [TestMethod]
        public void UtilityPhoneNumberTestSetNumerically()
        {
            PhoneNumber phoneNumber;

            phoneNumber = new PhoneNumber(123, 1234567, allowEmpty: true, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(123);
            phoneNumber.LocalPhoneNumber.Should().Be(1234567);
            phoneNumber.FormattedPhoneNumber.Should().Be("(123) 123-4567");

            phoneNumber = new PhoneNumber(0, 0, allowEmpty: true, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");

            phoneNumber = new PhoneNumber(123, 1234567, allowEmpty: false, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(123);
            phoneNumber.LocalPhoneNumber.Should().Be(1234567);
            phoneNumber.FormattedPhoneNumber.Should().Be("(123) 123-4567");

            phoneNumber = new PhoneNumber(0, 0, allowEmpty: false, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeFalse();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");

            phoneNumber = new PhoneNumber(123, 1234567, allowEmpty: false, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(123);
            phoneNumber.LocalPhoneNumber.Should().Be(1234567);
            phoneNumber.FormattedPhoneNumber.Should().Be("(123) 123-4567");

            phoneNumber = new PhoneNumber(0, 0, allowEmpty: false, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeFalse();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");

            phoneNumber = new PhoneNumber(0, 1, allowEmpty: false, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(1);
            phoneNumber.FormattedPhoneNumber.Should().Be("(000) 000-0001");

            phoneNumber = new PhoneNumber(1111, 0, allowEmpty: false, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeFalse();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");

            phoneNumber = new PhoneNumber(0, 11111111, allowEmpty: false, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeFalse();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");

            phoneNumber = new PhoneNumber(-1, 0, allowEmpty: true, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeFalse();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");

            phoneNumber = new PhoneNumber(0, -1, allowEmpty: true, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeFalse();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");

            phoneNumber = new PhoneNumber(-1, 0, allowEmpty: true, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeFalse();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");

            phoneNumber = new PhoneNumber(0, -1, allowEmpty: true, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeFalse();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");
        }

        [TestMethod]
        public void UtilityPhoneNumberTestSetWithString()
        {
            PhoneNumber phoneNumber;

            phoneNumber = new PhoneNumber("(123) 123-4567", allowEmpty: true, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(123);
            phoneNumber.LocalPhoneNumber.Should().Be(1234567);
            phoneNumber.FormattedPhoneNumber.Should().Be("(123) 123-4567");

            phoneNumber = new PhoneNumber("", allowEmpty: true, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");

            phoneNumber = new PhoneNumber("(000) 000-0000", allowEmpty: true, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");

            phoneNumber = new PhoneNumber("(001) 000-0000", allowEmpty: true, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(1);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("(001) 000-0000");

            phoneNumber = new PhoneNumber("(000) 001-0000", allowEmpty: true, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(10000);
            phoneNumber.FormattedPhoneNumber.Should().Be("(000) 001-0000");

            phoneNumber = new PhoneNumber("(000) 000-0001", allowEmpty: true, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(1);
            phoneNumber.FormattedPhoneNumber.Should().Be("(000) 000-0001");

            phoneNumber = new PhoneNumber("0000000001", allowEmpty: true, requireRegExMatch: false);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(1);
            phoneNumber.FormattedPhoneNumber.Should().Be("(000) 000-0001");

            phoneNumber = new PhoneNumber("0000000001", allowEmpty: true, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(1);
            phoneNumber.FormattedPhoneNumber.Should().Be("(000) 000-0001");

            phoneNumber = new PhoneNumber("123-123-1231", allowEmpty: true, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(123);
            phoneNumber.LocalPhoneNumber.Should().Be(1231231);
            phoneNumber.FormattedPhoneNumber.Should().Be("(123) 123-1231");

            phoneNumber = new PhoneNumber("123-1231234", allowEmpty: true, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(123);
            phoneNumber.LocalPhoneNumber.Should().Be(1231234);
            phoneNumber.FormattedPhoneNumber.Should().Be("(123) 123-1234");

            phoneNumber = new PhoneNumber("1231231234", allowEmpty: true, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(123);
            phoneNumber.LocalPhoneNumber.Should().Be(1231234);
            phoneNumber.FormattedPhoneNumber.Should().Be("(123) 123-1234");

            phoneNumber = new PhoneNumber("(123)123-1234", allowEmpty: true, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(123);
            phoneNumber.LocalPhoneNumber.Should().Be(1231234);
            phoneNumber.FormattedPhoneNumber.Should().Be("(123) 123-1234");

            phoneNumber = new PhoneNumber("123-123-1234", allowEmpty: true, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeTrue();
            phoneNumber.IsEmpty.Should().BeFalse();
            phoneNumber.AreaCode.Should().Be(123);
            phoneNumber.LocalPhoneNumber.Should().Be(1231234);
            phoneNumber.FormattedPhoneNumber.Should().Be("(123) 123-1234");

            phoneNumber = new PhoneNumber("-123-123-1234", allowEmpty: true, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeFalse();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");

            phoneNumber = new PhoneNumber("123--123-1234", allowEmpty: true, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeFalse();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");

            phoneNumber = new PhoneNumber("123-123-1234-", allowEmpty: true, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeFalse();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");

            phoneNumber = new PhoneNumber("123-123-1234x", allowEmpty: true, requireRegExMatch: true);
            phoneNumber.IsValid.Should().BeFalse();
            phoneNumber.IsEmpty.Should().BeTrue();
            phoneNumber.AreaCode.Should().Be(0);
            phoneNumber.LocalPhoneNumber.Should().Be(0);
            phoneNumber.FormattedPhoneNumber.Should().Be("");
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace UtilityTest
{
    [TestClass]
    public class RandomTest
    {
        [TestMethod]
        public void UtilityRandomIntTest()
        {
            var random = new Utility.Random();

            int zeroCount = 0;
            int fourCount = 0;
            for (int i = 0; i < 1000; i++)
            {
                int answer = random.GetRandomIntLessThan(5);

                System.Console.WriteLine( answer);

                answer.Should().BeGreaterOrEqualTo(0);
                answer.Should().BeLessOrEqualTo(4);

                if (answer == 0)
                    zeroCount++;

                if (answer == 4)
                    fourCount++;
            }

            System.Console.WriteLine( string.Format( "{0} zeros, {1} fours", zeroCount, fourCount));

            zeroCount.Should().BeGreaterThan(0);
            fourCount.Should().BeGreaterThan(0);

            int randomInt = Utility.Random.RandomInt;
            Utility.Random.RandomInt.Should().NotBe(randomInt);
        }

        [TestMethod]
        public void UtilityRandomBoolTest()
        {
            int trueCount = 0;
            int falseCount = 0;

            for (int i = 0; i < 1000; i++)
            {
                bool answer = Utility.Random.RandomBool;

                System.Console.WriteLine(answer);

                if (answer)
                {
                    trueCount++;
                }
                else
                {
                    falseCount++;
                }
            }

            trueCount.Should().BeGreaterThan(5);
            falseCount.Should().BeGreaterThan(5);
            (trueCount + falseCount).Should().Be(1000);

            System.Console.WriteLine(string.Format( "{0} trues, {1} falses", trueCount, falseCount));
        }
    }
}

﻿CREATE TABLE [dbo].[webpages_Roles] (
    [RoleId]   INT            IDENTITY (1, 1) NOT NULL,
    [RoleName] NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK__webpages__RoleID] PRIMARY KEY CLUSTERED ([RoleId] ASC),
    CONSTRAINT [UQ__webpages__RoleName] UNIQUE NONCLUSTERED ([RoleName] ASC)
);


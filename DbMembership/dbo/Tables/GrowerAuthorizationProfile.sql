﻿CREATE TABLE [dbo].[GrowerAuthorizationProfile] (
    [GrowerGuid]             UNIQUEIDENTIFIER NOT NULL,
    [AuthorizationProfileId] NCHAR (50)       NOT NULL,
    CONSTRAINT [PK__GrowerAuth] PRIMARY KEY CLUSTERED ([GrowerGuid] ASC)
);


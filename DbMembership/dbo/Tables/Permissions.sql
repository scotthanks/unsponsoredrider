﻿CREATE TABLE [dbo].[Permissions] (
    [PermissionId]   INT            IDENTITY (1, 1) NOT NULL,
    [PermissionName] NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK__PermissionsPermissionID] PRIMARY KEY CLUSTERED ([PermissionId] ASC),
    CONSTRAINT [UQ__PermissionsPermissionName] UNIQUE NONCLUSTERED ([PermissionName] ASC)
);


﻿create procedure dbo.RoleGrant
	@UserName AS NVARCHAR(56),
	@RoleName AS NVARCHAR(256),
	@Success AS BIT = NULL OUT
AS
	INSERT INTO webpages_UsersInRoles
	(
		UserProfile.UserId,
		webpages_UsersInRoles.RoleId
	)
	SELECT
		UserId,
		RoleId
	FROM UserProfile
		CROSS JOIN webpages_Roles
	WHERE
		UserName = @UserName AND
		RoleName = @RoleName

	IF @@ROWCOUNT = 1
		SET @Success = 1
	ELSE
		SET @Success = 0

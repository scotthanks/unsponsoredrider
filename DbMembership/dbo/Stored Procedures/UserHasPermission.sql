﻿
CREATE procedure [dbo].[UserHasPermission]
	@UserGuid AS UNIQUEIDENTIFIER,
	@PermissionName AS [nvarchar](256),
	@HasPermission AS BIT OUT
AS
	SELECT @HasPermission = COUNT(*)
	FROM PermissionsView
	WHERE
		PermissionsView.UserGuid = @UserGuid AND
		PermissionsView.PermissionName = @PermissionName


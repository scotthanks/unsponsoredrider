﻿using System;
using System.Collections.Generic;

using BusinessObjectsLibrary;

//added for hubspot code


namespace BusinessObjectServices
{
    public class PersonService
    {
        public bool Exists(string email)
        {
            string reasonMessage;

            return CheckIfExists(email, out reasonMessage);
        }

        public DataObjectLibrary.Person CreatePerson(BusinessObjectsLibrary.GrowerUser growerUser, BusinessObjectsLibrary.UserProfile user)
        {

            DataObjectLibrary.Person newPerson;
            
            DataObjectLibrary.Person person = new DataObjectLibrary.Person();
            person.FirstName = growerUser.FirstName;
            person.LastName = growerUser.LastName;
            person.EMail = growerUser.Email;
            person.GrowerGuid = growerUser.GrowerUserGuid;
            person.UserGuid = user.UserGuid;
            person.UserCode = growerUser.Email;
            person.UserId = user.UserId;
            person.PhoneAreaCode = growerUser.Phone.AreaCode;
            person.Phone = growerUser.Phone.LocalPhoneNumber;

            var dbLookupService = new DataServiceLibrary.LookupTableService();
            var personType = dbLookupService.TryGetByCode("Code/PersonType/",growerUser.CompanyRole, false);
            person.PersonTypeLookupGuid = personType.Guid;

            var dbPersonService = new DataServiceLibrary.PersonTableService();
            newPerson = dbPersonService.Insert(person);

            return newPerson;

        }

        public Person GetPersonFromUser(Guid userGuid)
        {

            var personService = new DataServiceLibrary.PersonTableService();
            DataObjectLibrary.Person dbPerson = personService.TryGetByField("UserGuid", userGuid.ToString(), "=", true);
            Person person = new Person(dbPerson);
            return person;

        }

        public bool Deactivate(Guid userGuid)
        {

            bool deactivated = false;

            var personService = new DataServiceLibrary.PersonTableService();
            DataObjectLibrary.Person dbPerson = personService.TryGetByField("UserGuid", userGuid.ToString(), "=", true);

            if (dbPerson != null)
            {
                var theDate = DateTime.Now;
                var newPerson = personService.UpdateField(dbPerson.Guid, "DateDeactivated", theDate);
                if (newPerson.DateDeactivated.ToString() == theDate.ToString())
               {
                   var a  = newPerson.DateDeactivated.ToString();
                   var b = theDate.ToString();
                    deactivated = true;
               }
            }

            return deactivated;

        }

        public string ExistsReasonMessage(string email)
        {
            string reasonMessage;

            bool exists = CheckIfExists(email, out reasonMessage);

            if (!exists)
            {
                reasonMessage = "This email does not already exist in the system.";
            }

            return reasonMessage;
        }

        private bool CheckIfExists(string email, out string reasonMessage)
        {
            var service = new DataServiceLibrary.PersonTableService();
            reasonMessage = "This user does not exist.";

            bool exists = service.TryGetByField(DataObjectLibrary.Person.ColumnEnum.EMail.ToString(), email, "LIKE", excludeInactive: false) != null;

            if (exists)
            {
                reasonMessage = "A Person already exists with the same email address.";
            }

            if (!exists)
            {
                var userProfileService = new DataServiceLibrary.UserProfileTableService();

                exists = userProfileService.Get(email) != null;

                if (exists)
                {
                    reasonMessage = "A UserProfile already exists with the same code.";
                }
            }

            return exists;
        }

        public BusinessObjectsLibrary.Person GetPersonData( Guid personGuid)
        {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var service = new DataServiceLibrary.PersonDataService(status);

            var personData = service.GetData(personGuid);
            return new BusinessObjectsLibrary.Person( personData.Person, personData.Grower);
        }

    //    private DataServiceLibrary.PersonRegisterProcedure.PersonRegisterParameters _lastRegisterParameters = null;

        public BusinessObjectsLibrary.Person Register
        (
           
            string companyName,
            string streetAddress1,
            string streetAddress2,
            string city,
            string stateCode,
            string zipCode,
            string companyEmail,
            string companyPhone,
            string companyTypeCode,
            string firstName,
            string lastName,
            string email,
            string password,
            string personPhone,
            string roleCode,
            bool subscribeToNewsLetter,
            bool subscribeToPromotions
        )
        {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var service = new DataServiceLibrary.PersonDataService(status);

            bool isAuthorizedToPlaceOrders = false || (companyTypeCode == "ProGH" || companyTypeCode == "Retail" || companyTypeCode ==  "Supplier" || companyTypeCode == "Farmer");
            bool isAuthorizedToSeePrices = false || (companyTypeCode == "ProGH" || companyTypeCode == "Retail" || companyTypeCode == "Supplier" || companyTypeCode == "Farmer");

            var personData = service.RegisterPerson
            (   companyName,
                streetAddress1,
                streetAddress2,
                city,
                stateCode,
                zipCode,
                companyEmail,
                companyPhone,
                companyTypeCode,
                firstName,
                lastName,
                email,
                password,
                personPhone,
                roleCode,
                isAuthorizedToPlaceOrders,
                subscribeToNewsLetter,
                subscribeToPromotions,
                false
            );

          //  _lastRegisterParameters = service.Parameters;
            
            //added to call hubspot
            //Took out for Green Fuse
            //int iPortalID = 426504; //This is the ID
            //string sFormGUID = "129b9dad-56e0-4055-b98f-2a6c6378fac3"; //This is the form guid for registration in Hubspot
            //bool bHubspot = SendRegistrationToHubSpot(iPortalID, sFormGUID,companyName, "USA", streetAddress1,city,stateCode,zipCode,companyTypeCode,firstName, lastName,personPhone, email,roleCode,subscribeToNewsLetter,subscribeToPromotions); 

            return new Person(personData.Person, personData.Grower, isAuthorizedToPlaceOrders, isAuthorizedToSeePrices);
        }

        

        public bool SetUserGuid(Guid personGuid, Guid userGuid)
        {
            var personDataService = new DataServiceLibrary.PersonTableService();

            var person = personDataService.UpdateField(personGuid, "UserGuid", userGuid);

            return person.UserGuid == userGuid;
        }

        public List<Person> GetGrowerPersons(Guid growerGuid)
        {
            List<Person> growerPersons = new List<Person>();

            var dbUserProfileService = new DataServiceLibrary.UserProfileTableService();

            var dbPersonService = new DataServiceLibrary.PersonTableService();
            var dbPersonList = dbPersonService.GetAllByField("GrowerGuid",growerGuid.ToString(),"=",true);

            foreach (var person in dbPersonList)
            {
                var newPerson = new Person(person);

                var userProfile = dbUserProfileService.GetByGuid(newPerson.UserGuid);

                var Role = dbUserProfileService.GetUserRole(userProfile.UserId);
                newPerson.UserRole = Role.RoleName;

                growerPersons.Add(newPerson);
            }

            return growerPersons;
        }


        //Took out for Green Fuse
     //   private bool SendRegistrationToHubSpot(int iPortalID,string sFormGUID,string sCompanyName,string sCountry,string sStreetAddress1,string sCity,string sStateCode,string sZipCode,string sCompanyTypeCode,string sFirstName,string sLastName,string sPersonPhone,string sEmail,string sRoleCode,bool bSubscribeToNewsLetter,bool bSubscribeToPromotions) 
     //   {
     //       try
     //       {
		   //     // Build dictionary of field names/values (must match the HS field names)
     //           //  	Hubspot Field — Registration Field
     //           //•	company — Company Name
     //           //•	country — Country
     //           //•	address — Street Address
     //           //•	city — City
     //           //•	state — State
     //           //•	zip — Zipcode
     //           //•	registered_type_c — Type of Company
     //           //•	firstname — First Name
     //           //•	lastname — Last Name
     //           //•	phone — Phone
     //           //•	email — Email
     //           //•	jobtitle — Company Role
     //           //•	green-fuse_emails_c — first check box
     //           //•	partner_promo_emails_c — second check box
     //           //•	emailoptout — last check box

     //           bool bUnsubscribe = false;
     //           if (bSubscribeToNewsLetter == false &&  bSubscribeToPromotions== false)
     //           {
     //               bUnsubscribe = true;
     //           }

     //           Dictionary < string, string > dictFormValues = new Dictionary < string, string > ();
     //           dictFormValues.Add("company", sCompanyName);
     //           dictFormValues.Add("country", sCountry);
     //           dictFormValues.Add("address", sStreetAddress1);
     //           dictFormValues.Add("city", sCity);
     //           dictFormValues.Add("state", sStateCode);
     //           dictFormValues.Add("zip", sZipCode);
     //           dictFormValues.Add("registered_type_c", sCompanyTypeCode);
     //           dictFormValues.Add("firstname", sFirstName);
     //           dictFormValues.Add("lastname", sLastName);
     //           dictFormValues.Add("phone", sPersonPhone);
     //           dictFormValues.Add("email", sEmail);
     //           dictFormValues.Add("jobtitle", sRoleCode);
     //           dictFormValues.Add("green-fuse_emails_c", bSubscribeToNewsLetter.ToString());
     //           dictFormValues.Add("partner_promo_emails_c", bSubscribeToPromotions.ToString());
     //           dictFormValues.Add("emailoptout", bUnsubscribe.ToString());


		   //     // Form Variables (from the HubSpot Form Edit Screen)
     //           int intPortalID = iPortalID; //place your portal ID here
     //           string strFormGUID = sFormGUID; //place your form guid here

		   //     // Tracking Code Variables
     //          string strHubSpotUTK = System.Web.HttpContext.Current.Request.Cookies["hubspotutk"].Value;
		   //    string strIpAddress = System.Web.HttpContext.Current.Request.UserHostAddress;

		   //     // Page Variables
		   //     string strPageTitle = "User Registered";
		   //     string strPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

		   //     // Do the post, returns true/false
		   //     string strError = "";
		   //     bool blnRet = Post_To_HubSpot_FormsAPI(intPortalID, strFormGUID, dictFormValues, strHubSpotUTK, strIpAddress, strPageTitle, strPageURL, ref strError);
		   //     if (blnRet) 
     //           {
			  //      return true;
		   //     }
     //           else 
     //           {
			  //      return false;
		   //     }
     //       }
     //       catch (Exception ex)
     //       {
     //           return false;
     //       }
	    //}

//Took out for Green Fuse
	    //private bool Post_To_HubSpot_FormsAPI(int intPortalID, string strFormGUID, Dictionary < string, string > dictFormValues, string strHubSpotUTK, string strIpAddress, string strPageTitle, string strPageURL, ref string strMessage)
     //   {
     //       try
     //       {

		   //     // Build Endpoint URL
		   //     string strEndpointURL = string.Format("https://forms.hubspot.com/uploads/form/v2/{0}/{1}", intPortalID, strFormGUID);

		   //     // Setup HS Context Object
		   //     Dictionary < string, string > hsContext = new Dictionary < string, string > ();
		   //    // hsContext.Add("hutk", strHubSpotUTK);
		   //     hsContext.Add("ipAddress", strIpAddress);
		   //     hsContext.Add("pageUrl", strPageURL);
		   //     hsContext.Add("pageName", strPageTitle);

		   //     // Serialize HS Context to JSON (string)
		   //     System.Web.Script.Serialization.JavaScriptSerializer json = new System.Web.Script.Serialization.JavaScriptSerializer();
		   //     string strHubSpotContextJSON = json.Serialize(hsContext);

		   //     // Create string with post data
		   //     string strPostData = "";

		   //     // Add dictionary values
		   //     foreach(var d in dictFormValues) {
     //              // strPostData += d.Key + "=" + Server.UrlEncode(d.Value) + "&";
     //               strPostData += d.Key + "=" + Uri.EscapeDataString(d.Value) + "&";
                   
		   //     }

		   //     // Append HS Context JSON
     //         //  strPostData += "hs_context=" + Server.UrlEncode(strHubSpotContextJSON);
     //           strPostData += "hs_context=" + Uri.EscapeDataString(strHubSpotContextJSON);

		   //     // Create web request object
		   //     System.Net.HttpWebRequest r = (System.Net.HttpWebRequest) System.Net.WebRequest.Create(strEndpointURL);

		   //     // Set headers for POST
		   //     r.Method = "POST";
		   //     r.ContentType = "application/x-www-form-urlencoded";
		   //     r.ContentLength = strPostData.Length;
		   //     r.KeepAlive = false;

		   //     // POST data to endpoint
		   //     using(System.IO.StreamWriter sw = new System.IO.StreamWriter(r.GetRequestStream())) {
			  //      try {
				 //       sw.Write(strPostData);
			  //      } catch (Exception ex) {
				 //       // POST Request Failed
				 //       strMessage = ex.Message;
				 //       return false;
			  //      }
		   //     }

		   //     return true; //POST Succeeded
     //       }
     //       catch (Exception ex)
     //       { 
     //           return false;
     //       }
	        
     //   }


    }
}

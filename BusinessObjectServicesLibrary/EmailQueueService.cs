﻿using System.Data.SqlClient;
using System.Web;
using BusinessObjectsLibrary;
using DataAccessLibrary;
using DataObjectLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Threading;
using GrowerOrderHistoryEvent = BusinessObjectsLibrary.GrowerOrderHistoryEvent;
//using GrowerOrder = BusinessObjectsLibrary.GrowerOrder;
using Lookup = DataObjectLibrary.Lookup;
//using OrderLine = DataObjectLibrary.OrderLine;
//using SupplierOrder = DataObjectLibrary.SupplierOrder;

namespace BusinessObjectServices
{   
    public class EmailQueueService : BusinessServiceBase
    {
        public EmailQueueService(StatusObject status)
            : base(status)
        {

        }


        public bool AddEmailtoQueue(string subject, string body, int delaySendMinutes, string emailType, string emailFrom,string emailTo, string emailCC, string emailBCC, Guid orderGuid, int updateCount, int addCount, int cancelCount)
        {

            var service = new EmailQueueDataService(Status);

            var bRet = service.AddEmail(subject, body, delaySendMinutes, emailType, emailFrom,emailTo, emailCC, emailBCC, orderGuid, updateCount, addCount, cancelCount);
        

            return bRet;
        }
    }
}
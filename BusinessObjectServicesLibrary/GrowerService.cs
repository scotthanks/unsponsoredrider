﻿using BusinessObjectsLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using Utility;

namespace BusinessObjectServices
{
    public class GrowerService
    {
        public bool ExistsSimilar(string name, string zip, PhoneNumber phoneNumber)
        {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var service = new GrowerDataService(status);

            return service.Exists(name, zip, phoneNumber.AreaCode, phoneNumber.LocalPhoneNumber);
        }

        public bool EmailExists(string email)
        {
            var service = new GrowerTableService();

            return service.TryGetByField(DataObjectLibrary.Grower.ColumnEnum.EMail.ToString(), email, "LIKE", excludeInactive: false) != null;
        }

        public class AddShipToAddressReturnData
        {
            public Guid GrowerAddressGuid { get; set; }
            public Guid AddressGuid { get; set; }
        }

        public AddShipToAddressReturnData AddShipToAddress(Guid userGuid, Guid growerGuid, GrowerShipToAddress shipToAddress)
        {
            var returnData = new AddShipToAddressReturnData();
            var status = new StatusObject(userGuid, true);
            var service = new GrowerAddressDataService(status);

            var dataLayerReturnData = service.AddAddress
                (
                    growerGuid,
                    shipToAddress.AddressName,
                    shipToAddress.StreetAddress1,
                    shipToAddress.StreetAddress2,
                    shipToAddress.City,
                    shipToAddress.State.StateCode,
                    shipToAddress.ZipCode,
                    shipToAddress.Phone,
                    shipToAddress.SpecialInstructions,
                    shipToAddress.SellerCustomerID,
                    shipToAddress.IsDefault
                );

            returnData.GrowerAddressGuid = dataLayerReturnData.GrowerAddressGuid;
            returnData.AddressGuid = dataLayerReturnData.AddressGuid;

            return returnData;
        }

        public bool UpdateShipToAddressDefault(Guid userGuid, Guid growerAddressGuid)
        {
            var service = new GrowerAddressTableService();
            var growerAddress = service.GetByGuid(growerAddressGuid);
            var status = new StatusObject(userGuid, true);
            var addressUpdateService = new GrowerAddressDataService(status);


            bool success = addressUpdateService.UpdateAddress
               (
                    userGuid: userGuid,
                    growerAddressGuid: growerAddressGuid,
                    name: growerAddress.Address.Name,
                    streetAddress1: growerAddress.Address.StreetAddress1,
                    streetAddress2: growerAddress.Address.StreetAddress2,
                    city: growerAddress.Address.City,
                    stateCode: growerAddress.Address.State.Code,
                    zipCode: growerAddress.Address.ZipCode,
                    phoneNumber: new PhoneNumber( growerAddress.PhoneAreaCode.ToString() + growerAddress.Phone.ToString()),
                    specialInstructions: growerAddress.SpecialInstructions,
                    sellerCustomerID: growerAddress.SellerCustomerID,
                    isDefault: true
                );

            return success;
            
        }
        
        public bool UpdateShipToAddress(Guid userGuid, Guid growerShipToAddressGuid, string addressName, string streetAddress1, string streetAddress2, string city, string stateCode, string zipCode, PhoneNumber phoneNumber, string specialInstructions, string sellerCustomerID,bool isDefault)
        {
            var status = new StatusObject(userGuid, true);
            var service = new GrowerAddressDataService(status);
           
            
            bool success = service.UpdateAddress
                (
                    userGuid: userGuid,
                    growerAddressGuid: growerShipToAddressGuid,
                    name: addressName,
                    streetAddress1: streetAddress1,
                    streetAddress2: streetAddress2,
                    city: city,
                    stateCode: stateCode,
                    zipCode: zipCode,
                    phoneNumber: phoneNumber,
                    specialInstructions: specialInstructions,
                    sellerCustomerID: sellerCustomerID,
                    isDefault: isDefault
                );

            return success;
        }
        public bool UpdateGrowerField(Guid userGuid, string theField ,string theValue)
        {
            var status = new StatusObject(userGuid, true);
            var service = new GrowerDataService(status);
            var growerDataRow = service.GetGrowerDataFromUserGuid(userGuid);

            var growerTableservice = new GrowerTableService();
            growerTableservice.UpdateField(growerDataRow.Guid, theField, theValue);

            bool success = true;

            return success;
        }
       
        public bool RequestLineOfCredit(Guid userGuid, int RequestedLineOfCreditAmount)
        {
            var status = new StatusObject(userGuid, true);
            var service = new GrowerDataService(status);
            var growerDataRow = service.GetGrowerDataFromUserGuid(userGuid);
            
            var RequestedLineOfCreditStatusLookupPending = LookupTableValues.Code.CreditAppStatus.Pending;
            var growerTableService = new GrowerTableService();
            var growerTableObject = growerTableService.GetByGuid(growerDataRow.Guid);
            growerTableObject.RequestedLineOfCredit = RequestedLineOfCreditAmount;
            growerTableObject.RequestedLineOfCreditStatusLookupGuid = RequestedLineOfCreditStatusLookupPending.Guid;
            growerTableService.Update(growerTableObject);

            bool success = true;
            return success;
        }
        public bool UpdateDefaultPaymentType(Guid userGuid, Guid DefaultPaymentTypeLookupGuid)
        {
            var status = new StatusObject(userGuid, true);
            var service = new GrowerDataService(status);
            var growerDataRow = service.GetGrowerDataFromUserGuid(userGuid);

            var growerTableservice = new GrowerTableService();
            growerTableservice.UpdateField(growerDataRow.Guid , "DefaultPaymentTypeLookupGuid", DefaultPaymentTypeLookupGuid);

            bool success = true;

            return success;
        }


        public bool UpdateCreditCard(Guid userGuid, string userCode, Guid creditCardGuid, string cardDescription,DateTime dateDeactivated)
        {

            var status = new StatusObject(userGuid, true);
            var service = new GrowerCreditCardDataService(status);
            bool bRet = service.UpdateCard(userCode, creditCardGuid, cardDescription, dateDeactivated);



            return bRet;
        }

        public bool UpdateDefaultCreditCard(Guid userGuid, Guid DefaultCreditCardGuid)
        {
            var status = new StatusObject(userGuid, true);
            var service = new GrowerDataService(status);
            var growerDataRow = service.GetGrowerDataFromUserGuid(userGuid);
       
            service.SetDefaultCreditCard(growerDataRow.Guid, DefaultCreditCardGuid);
           
            return true;
        }
        public bool DeleteShipToAddress(Guid userGuid, Guid growerShipToAddressGuid)
        {

            var status = new StatusObject(userGuid, true);
            var service = new GrowerAddressDataService(status);

            bool success = service.DeleteAddress(growerShipToAddressGuid);

            return success;
        }

        public List<GrowerVolume> GetVolumeLevelsForGrower(Guid userGuid,  String supplierCodeList)
        {

            var status = new StatusObject(userGuid, true);
            var service = new GrowerDataService(status);
            
            var dataLayerReturnData = service.GetVolumesForGrower(supplierCodeList);

            return GetVolumes(dataLayerReturnData);
        }

        private List<GrowerVolume> GetVolumes(GrowerDataService.ReturnData returnedData)
        {
            var growerVolumeList = returnedData.GrowerVolumeList;
            //var growerVolumeList = new List<GrowerVolume>();
            //foreach (var growerVolumeTableRow in returnedData.GrowerVolumeList)
            //{
            //    var growerVolume = new BusinessObjectsLibrary.GrowerVolume(growerVolumeTableRow);


            //    growerVolumeList.Add(growerVolume);
            //}

            return growerVolumeList;
        }

        public List<GrowerShipToAddress> GetShipToAddressesForGrower(Guid userGuid, Guid growerGuid)
        {
            var status = new StatusObject(userGuid, true);
            var service = new GrowerAddressDataService(status);

            var dataLayerReturnData = service.GetAddressesForGrower(userGuid, growerGuid);

            return GetShipToAddresses(dataLayerReturnData);
        }

        public List<GrowerShipToAddress> GetShipToAddress(Guid userGuid, Guid growerShipToAddressGuid)
        {
            var status = new StatusObject(userGuid, true);
            var service = new GrowerAddressDataService(status);

            var dataLayerReturnData = service.GetAddress(userGuid, growerShipToAddressGuid);

            return GetShipToAddresses(dataLayerReturnData);
        }



        public List<GrowerShipToAddress> GetShipToAddressesForGrowerOrder(Guid userGuid, Guid growerGuid, Guid growerOrderGuid)
        {
            var status = new StatusObject(userGuid, true);
            var service = new GrowerAddressDataService(status);

            var dataLayerReturnData = service.GetAddressesForGrowerOrder(userGuid, growerGuid, growerOrderGuid);

            return GetShipToAddresses(dataLayerReturnData);
        }



        private List<GrowerShipToAddress> GetShipToAddresses(GrowerAddressDataService.ReturnData returnedData)
        {
            var growerAddressList = new List<GrowerShipToAddress>();
            foreach (var shipToAddressTableRow in returnedData.GrowerAddressList)
            {
                var growerAddress = new BusinessObjectsLibrary.GrowerShipToAddress(shipToAddressTableRow);

                if
                (
                    returnedData.GrowerOrder != null &&
                    returnedData.GrowerOrder.ShipToAddressGuid == growerAddress.GrowerAddressGuid
                )
                {
                    growerAddress.IsSelected = true;
                }

                growerAddressList.Add(growerAddress);
            }

            return growerAddressList;
        }


        public Grower TryGetGrowerForUser(Guid userGuid)
        {
            var status = new StatusObject(userGuid, true);
            var service = new GrowerDataService(status);

            var growerDataRow = service.GetGrowerDataFromUserGuid(userGuid);

            var grower = new Grower(growerDataRow);
            grower.LoadShipToAddresses(growerDataRow.GrowerAddressList);
            grower.LoadCreditCards(growerDataRow.GrowerCreditCardList);
            
            return grower;
        }


        public string GetAuthorizationProfileId(Guid growerGuid)
        {
            var service = new UserProfileTableService();

            var authorizationProfileId = service.GetGrowerAuthorizationProfileId(growerGuid);

            return authorizationProfileId;
        }

        public void SaveAuthorizationProfileId(Grower grower, string authorizationProfileId)
        {
            var service = new UserProfileTableService();

            service.SaveGrowerAuthorizationProfile(grower.Guid, authorizationProfileId);
        }

        public void DeleteAuthorizationProfileId(Grower grower)
        {
            var service = new UserProfileTableService();

            service.DeleteGrowerAuthorizationProfile(grower.Guid);
        }

 

        public List<BusinessObjectsLibrary.CreditCard> GetCreditCards(Guid userGuid, Guid growerGuid, Guid growerCreditCardGuid, Guid growerOrderGuid)
        {
            var growerService = new DataServiceLibrary.GrowerTableService();
            var grower = growerService.GetByGuid(growerGuid);
            
            //var growerOrderService = new DataServiceLibrary.GrowerOrderTableService();
            var orderService = new GrowerOrderService();
           var growerOrder = new GrowerOrder();
       //     DataObjectLibrary.GrowerOrder growerOrderTableRow = null;
            if (growerOrderGuid != Guid.Empty)

            {
                growerOrder = orderService.GetGrowerOrderDetail(growerOrderGuid, false, false);
         //       growerOrderTableRow = growerOrderService.GetByGuid(growerOrderGuid);
            }

            var creditCardList = new List<BusinessObjectsLibrary.CreditCard>();
            foreach (var creditCardDataRow in grower.GrowerCreditCardList)
            {
                var creditCard = new BusinessObjectsLibrary.CreditCard(creditCardDataRow);
                if (creditCardDataRow.DateDeactivated == null)
                {
                    creditCardList.Add(creditCard);

                   // if (growerOrderTableRow != null && growerOrderTableRow.GrowerCreditCardGuid == growerOrderTableRow.Guid)
                    if (growerOrderGuid != Guid.Empty)
                    {
                        if (growerOrder.GrowerCreditCardGuid == creditCard.Guid) 
                        {
                            creditCard.IsSelected = true;

                        }
                        
                    }
                }
            }

            return creditCardList;
        }

        public Grower GetByGuid(Guid growerGuid)
        {
            var growerTableService = new GrowerTableService();
            return new Grower(growerTableService.GetByGuid(growerGuid));
        }


        public bool DeleteGrowerOrderLog(Guid userGuid,  Guid growerOrderHistoryGuid)
        {
            var deleteService = new GrowerOrderHistoryService();
            var bRet = deleteService.DeleteOrderHistroy( userGuid,growerOrderHistoryGuid);
            return bRet.Success;
            


        }
        public bool EditGrowerOrderLog(Guid userGuid, Guid growerOrderHistoryGuid,string theComment,bool IsInternal)
        {
            var deleteService = new GrowerOrderHistoryService();
            var bRet = deleteService.EditOrderHistroy(userGuid, growerOrderHistoryGuid, theComment, IsInternal);
            return bRet.Success;



        }

        public Guid AddGrowerOrderLog(Guid userGuid, string userCode, Guid growerOrderGuid, string logTypeLookupCode, string text)
        {
            Guid logTypeLookupGuid = Guid.Empty;

            var lookupTableService = new LookupTableService();
           
            //var gib - put back logTypeLookup = lookupTableService.GetByCode(LookupTableValues.Logging.LogType.LookupValue, logTypeLookupCode);

            var logTypeLookup = lookupTableService.GetByCode(LookupTableValues.Logging.LogType.Grower.LookupValue, logTypeLookupCode);

            PersonService personService = new PersonService();
            var userPerson = personService.GetPersonFromUser(userGuid);

            //var growerOrderHistoryTableService = new GrowerOrderHistoryTableService();
            //var theEvent = new DataObjectLibrary.GrowerOrderHistory();
            //theEvent.PersonGuid = userPerson.PersonGuid ;
            //theEvent.Comment = text;
            //theEvent.GrowerOrderGuid = growerOrderGuid;
            //theEvent.LogTypeLookup = logTypeLookup;
            //theEvent.LogTypeLookupGuid = logTypeLookup.Guid;
            //theEvent.EventDate = DateTime.Now;
            //var growerOrderHistory = growerOrderHistoryTableService.Insert
            //    ( theEvent);
            var bInternal = false;
            if (logTypeLookupCode == "CommentInternal")
            {
                bInternal = true; 
            }

            var status = new StatusObject(userGuid, true);
            var theProc = new GrowerOrderDataService(status);
             theProc.GrowerOrderHistoryAdd(userCode, growerOrderGuid, text, bInternal, logTypeLookup.Guid, userPerson.PersonGuid, DateTime.Now);

            return growerOrderGuid;  //just to return something to stop false error
          //  return growerOrderHistory.Guid;


        }

        public object GetGrowerOrderLogs(Guid growerOrderGuid)
        {
            var sqlServices = new DataAccessLibrary.SqlQueryServices();

            // Add logic here to get GrowerOrderLogs through a stored procedure.
            throw new NotImplementedException();
        }

        public List<GrowerOrderSummary> GetGrowerOrderSummaries(Guid userGuid, string shipWeekCode, string sellerCode)
        {

            sellerCode = "GFB";
            var status = new StatusObject(userGuid, true);
            var growerOrderDataService = new GrowerOrderDataService(status);
            var theSummaries = growerOrderDataService.GrowerOrderGetGrowerShipWeekOrders(shipWeekCode, sellerCode);
            var theOrderList = new List<GrowerOrderSummary>();
            foreach (GrowerOrderSummary theOrder in theSummaries)
            {
                var newSummary = new GrowerOrderSummary();
                newSummary.GrowerOrderGuid = theOrder.GrowerOrderGuid;
                newSummary.ShipWeekGuid = theOrder.ShipWeekGuid;
                newSummary.OrderNo = theOrder.OrderNo;
                newSummary.CustomerPoNo = theOrder.CustomerPoNo;
                newSummary.ShipToCity = theOrder.ShipToCity;
                newSummary.QtyOrdered = theOrder.QtyOrdered;
                
                theOrderList.Add(newSummary);
            }

            return theOrderList;

        }

        public string GetPromoName(Guid userGuid, string promoCode)
        {
           // var userguid = GetCurrentUserGuid();
            var status = new StatusObject(userGuid, true);
            var service = new CartDataService(status);
            var promoName = service.GetPromoName(promoCode);
            if (promoName == null) { promoName = "No active promotion found for this Promotion Code"; }
            return promoName;

        }

    }
}

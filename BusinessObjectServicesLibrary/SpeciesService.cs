﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataServiceLibrary;
using BusinessObjectsLibrary;

namespace BusinessObjectServices
{
    public class SpeciesService: BusinessServiceBase
    {

        public SpeciesService(StatusObject status)
            : base(status)
        { }
    
        public IEnumerable<Species> SelectSpecies(string sellerCode,string plantCategoryCode, string productFormCode, string[] supplierCodes)
        {
            sellerCode = "GFB";
            var procedure = new DataServiceLibrary.VarietyDataService(Status);

            var speciesTableObjectList = 
                procedure.GetSpecies
                (
                    programTypeCode: plantCategoryCode,
                    productFormCategoryCode: productFormCode,
                    supplierCodes: supplierCodes,
                    sellerCode: sellerCode
                );

            var speciesList = speciesTableObjectList.Select
                (
                    species => new Species() 
                    {
                        Code = species.Code, 
                        Name = species.Name
                    }
                ).ToList();

            speciesList = speciesList.OrderBy(o => o.Name).ToList();

            return speciesList;
        }


    }
}

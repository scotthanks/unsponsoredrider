﻿using System.Data.SqlClient;
using System.Web;
using BusinessObjectsLibrary;
using DataAccessLibrary;
using DataObjectLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Threading;
using GrowerOrderHistoryEvent = BusinessObjectsLibrary.GrowerOrderHistoryEvent;
//using GrowerOrder = BusinessObjectsLibrary.GrowerOrder;
using Lookup = DataObjectLibrary.Lookup;
//using OrderLine = DataObjectLibrary.OrderLine;
//using SupplierOrder = DataObjectLibrary.SupplierOrder;


namespace BusinessObjectServices
{
    public class ReportService : BusinessServiceBase
    {
        public ReportService(StatusObject status)
            : base(status)
        {

        }


        public List<ShipWeek2> GetNextShipWeekCodes(int weeks)
        {

            var service = new ReportDataService(Status);
            var list = service.GetNextShipWeekCodes(weeks);

            return list;
        }
        public List<AvailabilityItemAcrossWeeks> GetCurrentAvailabilityReport()
        {

            var service = new ReportDataService(Status);
            var list = service.GetCurrentAvailabilityReport();

            return list;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;
using DataServiceLibrary;
using BusinessObjectsLibrary;

namespace BusinessObjectServices
{
    public class MenuService : BusinessServiceBase
    {
        public MenuService(StatusObject status)
            : base(status)
        {

        }
    
        public MenuData GetMenuData()
        {
           
            var service = new DataServiceLibrary.ProgramDataService(Status);

            List<DataObjectLibrary.ProgramType> programTypeList;
            List<DataObjectLibrary.ProductFormCategory> productFormCategoryList;
            List<DataObjectLibrary.ProductFormCategoryProgramTypeCombinationView> combinationList;

            service.GetLists(out programTypeList, out productFormCategoryList, out combinationList);

            var menuData = new MenuData();

            var productFormDictionary = new Dictionary<Guid, BusinessObjectsLibrary.ProductFormCategory>();

            foreach (var productFormCategoryTableRow in productFormCategoryList)
            {
                var productForm = new BusinessObjectsLibrary.ProductFormCategory(productFormCategoryTableRow);

                menuData.ProductForms.Add(productForm); 
                productFormDictionary.Add(productFormCategoryTableRow.Guid, productForm);
            }

            foreach (var programTypeTableRow in programTypeList)
            {
                var programType = new BusinessObjectsLibrary.ProgramType(programTypeTableRow);

                bool hasProductForms = false;
                foreach (var combination in combinationList)
                {
                    if (combination.ProgramTypeGuid == programType.Guid)
                    {
                        hasProductForms = true;

                        BusinessObjectsLibrary.ProductFormCategory productFormCategory;

                        bool found = productFormDictionary.TryGetValue(combination.ProductFormCategoryGuid, out productFormCategory);
                        if (!found)
                        {
                            throw new ApplicationException(string.Format( "The product form {0} was not found in the dictionary.", combination.ProductFormCategoryCode));
                        }

                        programType.ActiveProductForms.Add(productFormCategory);
                    }
                }

                if (hasProductForms)
                {
                    menuData.ActiveProgramTypes.Add(programType);
                }

                menuData.ProgramTypes.Add(programType);

                menuData.ProgramTypes.Sort();
                menuData.ActiveProgramTypes.Sort();
                menuData.ProductForms.Sort();
            }

            return menuData;
        }

        public string GetMenuUL(string email,string sellerCode,bool isBeta)
        {
           // if (sellerCode == "" || sellerCode == null || sellerCode == "GFB" )
          //  {
                sellerCode = "GFB";
            //     return null;
               
          //  }
            

            var service = new MenuDataService(Status);
            var list = service.GetMenuData(email, sellerCode);
            var programType = "";
            
            //Main Nav UL
            var ul = "<ul>";
            
            //Availability Menu
            var bFirstType = true;
            ul += "<li class='has-submenu'><a href='#'>Availability</a>";
            ul += "<ul class='sub-menu'>";
            foreach (var item in list)
            {
                if (item.ProgramTypeName != programType)
                {
                    if (bFirstType == false)
                    {
                        //end the previous list, not needed on first
                        ul += "</ul>";
                        ul += "</li>";
                        
                    }
                    programType = item.ProgramTypeName;
                   
                    ul += "<li class='has-submenu'>";
                    ul += "<a href='#'>" + item.ProgramTypeName + "</a>";
                    ul += "<ul>";
                    bFirstType = false;
                }
                if (isBeta == true)
                {
                    ul += "<li class='selected_menu_item'><a href='/Availability2/Search?Category=" + item.ProgramTypeCode.Trim() + "&Form=" + item.ProductFormCategoryCode.Trim() + "'>" + item.ProductFormCategoryName + "</a></li>";

                }
                else
                {
                    ul += "<li class='selected_menu_item'><a href='/Availability/Search?Category=" + item.ProgramTypeCode.Trim() + "&Form=" + item.ProductFormCategoryCode.Trim() + "'>" + item.ProductFormCategoryName + "</a></li>";

                }
            }
            //end the last Program Type
            ul += "</ul>";
            //End the Availability list
            ul += "</li>";
            ul += "</ul>";
            
            
            //Catalog Menu
            bFirstType = true;
            programType = "";
            ul += "<li class='has-submenu'><a href='#'>Catalog</a>";
            ul += "<ul class='sub-menu'>";
            foreach (var item in list)
            {
                if (item.ProgramTypeName != programType)
                {
                    if (bFirstType == false)
                    {
                        //end the previous list, not needed on first
                        ul += "</ul>";
                        ul += "</li>";

                    }
                    programType = item.ProgramTypeName;

                    ul += "<li class='has-submenu'>";
                    ul += "<a href='#'>" + item.ProgramTypeName + "</a>";
                    ul += "<ul>";
                    bFirstType = false;
                }
                ul += "<li class='selected_menu_item'><a href='/Catalog/Search?Category=" + item.ProgramTypeCode.Trim() + "&Form=" + item.ProductFormCategoryCode.Trim() + "'>" + item.ProductFormCategoryName + "</a></li>";
            }
            //end the last Program Type
            ul += "</ul>";
            //End the Catalog list
            ul += "</li>";
            ul += "</ul>";
            


	        			   

            //Partners Menu
            ul += "<li><a href='/partners/'>Partners</a></li>";
            
            //BLOG Menu
            ul += "<li><a href='http://Blog.green-fuse.com'>Blog</a></li>";
        
            //About Us Menu        	        	
            ul += "<li><a href='/Home/about/'>About Us</a></li>";
            
            //Contact 
            ul += "<li><a href='http://green-fuse.com/contact'>Contact</a></li>";

            //End the Menu
            ul += "</ul>";          
            return ul;
        }

    }
}
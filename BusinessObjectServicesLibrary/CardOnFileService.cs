﻿using AuthorizeNet;
using BusinessObjectsLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Address = AuthorizeNet.Address;

namespace BusinessObjectServices
{

    public class CardOnFileService
    {
        private const string API_LOGIN_ID = "2HAk9yxjT6p";         //ToDo: Move this value to WebConfig, Table Storage, or Database
        private const string TRANSACTION_KEY = "97ar45699KUcZCRh";  //ToDo: Move this value to WebConfig, Table Storage, or Database

        public bool DeleteCustomerProfile(string profileId)
        {
            var success = true;
            try
            {
                //Connect to customer gateway
                CustomerGateway gate = new CustomerGateway(API_LOGIN_ID, TRANSACTION_KEY, ServiceMode.Live);
                var result = gate.DeleteCustomer(profileId); 
                //Customer cust = gate.GetCustomer(profileId);
                
            }
            catch
            {
                success = false;
            }
            finally {
            }
            return success;
        }
        
        public CustomerProfile RetrieveCustomerProfile(string profileId) {

            
            var customerProfile = new CustomerProfile();

            //Connect to customer gateway
            CustomerGateway gate = new CustomerGateway(API_LOGIN_ID, TRANSACTION_KEY, ServiceMode.Live);
            Customer cust = gate.GetCustomer(profileId);
            
            if(cust !=null)
            {

                var cards = new List<BusinessObjectsLibrary.CreditCard>();
                foreach(var paymentProfile in cust.PaymentProfiles)
                {
                    cards.Add
                        (
                            new CreditCard()
                                {
                                    CardTypeCode = paymentProfile.CardType,
                                    CardNumber = paymentProfile.CardNumber,
                                    ExpirationDate = paymentProfile.CardExpiration
                                }
                        );
                };

                customerProfile = new CustomerProfile()
                    {
                        Created = true,
                        ProfileID = cust.ProfileID,
                        EmailAddress = cust.Email,
                        Description = cust.Description,
                        CardsOnFile = cards
                    };
            }
            return customerProfile;
        }
       
        public string[] CustomerProfileIds() {
            //Connect to customer gateway
            CustomerGateway gate = new CustomerGateway(API_LOGIN_ID, TRANSACTION_KEY, ServiceMode.Live);


            var result = gate.GetCustomerIDs();

            return result;
        }

        public CustomerProfile CreateCustomerProfile(Guid growerGuid)
        {
            var response = new CustomerProfile();
            
            var growerService = new GrowerService();
            var grower = growerService.GetByGuid(growerGuid);
            string customerDescription = grower.Guid.ToString();
            string emailAddress = grower.EMail;

            CustomerGateway gate = new CustomerGateway(API_LOGIN_ID, TRANSACTION_KEY, ServiceMode.Live);

            var result = gate.CreateCustomer(emailAddress, customerDescription);
            if (result != null)
            {
                Customer cust = gate.GetCustomer(result.ProfileID);
                var cards = new List<BusinessObjectsLibrary.CreditCard>();
                foreach (PaymentProfile paymentProfile in cust.PaymentProfiles)
                {
                    cards.Add
                        (
                            new CreditCard()
                                {
                                    CardNumber = paymentProfile.CardNumber,
                                    CardTypeCode = paymentProfile.CardType,
                                    ExpirationDate = paymentProfile.CardExpiration
                                }
                        );
                };

                response = new CustomerProfile()
                {
                    Created = true,
                    ProfileID = result.ProfileID,
                    CardsOnFile = cards,
                    Description = result.Description
                };
            }

            return response;
        }

        public CustomerProfile CreateCardProfile(string profileId, CreditCardProfileRequest cardData)
        {
            var response = new CustomerProfile();

            CustomerGateway gate = new CustomerGateway(API_LOGIN_ID, TRANSACTION_KEY, ServiceMode.Live);

            int expirationMonth = Utility.CreditCard.GetExpirationMonth(cardData.ExpirationDate);
            int expirationYear = Utility.CreditCard.GetExpirationYear(cardData.ExpirationDate);

            var address = new Address()
                {
                    ID = "",
                    First = cardData.FirstName,
                    Last = cardData.LastName,
                    Company = cardData.Company,
                    Street = cardData.StreetAddress1 + " " + cardData.StreetAddress2,
                    City = cardData.City,
                    State = cardData.StateCode,
                    Country = cardData.Country,
                    Zip = cardData.ZipCode,
                    Phone = cardData.PhoneNumber,
                    Fax = cardData.FaxNumber
                };

            string cardProfileId = gate.AddCreditCard
                (
                    profileID: profileId,                    
                    cardNumber: cardData.CardNumber,
                    expirationMonth: expirationMonth,
                    expirationYear: expirationYear,
                    cardCode: cardData.Code,
                    billToAddress: address
                );

            if (!string.IsNullOrEmpty(cardProfileId))
            {
                response.ProfileID = cardProfileId;

                Customer cust = gate.GetCustomer(profileId);
                var cards = new List<CreditCard>();
                foreach (PaymentProfile paymentProfile in cust.PaymentProfiles)
                {
                    var card =
                        new CreditCard()
                            {
                                CardNumber = paymentProfile.CardNumber,
                                CardTypeCode = paymentProfile.CardType,
                                ExpirationDate = paymentProfile.CardExpiration
                            };

                    cards.Add(card);

                    if (paymentProfile.ProfileID == cardProfileId)
                    {
                        response.Created = true;
                        response.CardJustAdded = card;
                    }
                }

                response.Created = true;
                response.ProfileID = cust.ProfileID;
                response.CardsOnFile = cards;
                response.Description = cust.Description;
            }

            return response;
        }

        public CustomerProfile Response { get; set; }

        //ToDo: add private function to parse and validate Expires

        public Guid SaveGrowerCreditCard(Guid growerGuid, string cardProfileId, string cardDescription, string maskedCardNumber, string cardType, bool isDefaultCard)
        {
            var growerCreditCard = new DataObjectLibrary.GrowerCreditCard()
                {
                    Guid = Guid.NewGuid(),
                    GrowerGuid = growerGuid,
                    AuthorizeNetSerialNumber = cardProfileId,
                    CardDescription = cardDescription,
                    LastFourDigits = maskedCardNumber,
                    DefaultCard = isDefaultCard
                };

            var growerCreditCardService = new DataServiceLibrary.GrowerCreditCardTableService();
            var createdGrowerCreditCard = growerCreditCardService.Insert(growerCreditCard);

            return createdGrowerCreditCard.Guid;
        }
    }

    //TODO: Refactor these to the way other business objects are handled.

    public class CreditCardProfileRequest
    {
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string ExpirationDate { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
    }

    public class CustomerProfile
    {
        public bool Created { get; set; }
        public string ProfileID { get; set; }
        public string EmailAddress { get; set; }
        public string Description { get; set; }
        public IEnumerable<BusinessObjectsLibrary.CreditCard> CardsOnFile { get; set; }
        public BusinessObjectsLibrary.CreditCard CardJustAdded { get; set; }
    }

    //public class CreditCardType {
    //    public Guid Guid { get; set; }
    //    public string Type { get; set; }
    //    public string Number { get; set; }
    //    public string CardNumber { get; set; }
    //    public string CardDescription { get; set; } // dup
    //    public string CardType { get; set; }
    //    public string CardTypeImageUrl { get; set; }
    //    public string Code { get; set; }
    //    public string Expiration { get; set; }
    //    public bool IsDefault { get; set; }
    //    public bool IsSelected { get; set; }
    //}
}

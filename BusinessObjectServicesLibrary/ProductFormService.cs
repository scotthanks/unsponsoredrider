﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLibrary;
using DataObjectLibrary;
using DataServiceLibrary;

namespace BusinessObjectServices
{
    public class ProductFormService
    {
        public List<BusinessObjectsLibrary.ProductForm> GetProductForms(Guid? supplierGuid = null, Guid? productFormCategoryGuid = null, bool? active = null)
        {
            if (supplierGuid == Guid.Empty)
                supplierGuid = null;

            if (productFormCategoryGuid == Guid.Empty)
                productFormCategoryGuid = null;

            var productFormService = new DataServiceLibrary.ProductFormTableService();
            var productFormTableObjects = productFormService.GetAll();

            var productFormList = new List<BusinessObjectsLibrary.ProductForm>();
            foreach (var productFormTableRow in productFormTableObjects)
            {
                if (supplierGuid != null && supplierGuid != productFormTableRow.SupplierGuid)
                    continue;

                if (productFormCategoryGuid != null &&
                    productFormCategoryGuid != productFormTableRow.ProductFormCategoryGuid)
                    continue;

                if (active != null && active != productFormTableRow.IsActive)
                    continue;

                var productForm = new BusinessObjectsLibrary.ProductForm(productFormTableRow);
                BusinessObjectsLibrary.ProductFormCategory productFormCategory;

                productFormList.Add(productForm);
            }

            return productFormList;
        }
    }
}

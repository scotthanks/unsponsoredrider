﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLibrary;
using DataServiceLibrary;
using DataObjectLibrary;
using Product = BusinessObjectsLibrary.Product;
using ProductFormCategory = BusinessObjectsLibrary.ProductFormCategory;
using ProgramType = BusinessObjectsLibrary.ProgramType;
using ShipWeek = BusinessObjectsLibrary.ShipWeek;

namespace BusinessObjectServices
{
    public class AvailabilityService : BusinessServiceBase
    {
        public AvailabilityService(StatusObject status)
            : base(status)
        { }
    

        public bool IsValidCategoryCode(string code, out string name)
        {
            try
            {
                ProgramTypeService programTypeService = new ProgramTypeService();
                ProgramType programType = programTypeService.GetByCode(code);
                name = programType.Name;
                return true;
            }
            catch 
            {
                name = "";
                return false; 
            }
        }
        public bool IsValidFormCode(string code, out string name)
        {
            try
            {
                ProductFormCategoryService productFormCategoryService = new ProductFormCategoryService();
                ProductFormCategory productFormCategory = productFormCategoryService.GetByCode(code);
                name = productFormCategory.Name;
                return true;
            }
            catch
            {
                name = "";
                return false;
            }
        }
        
        
        public ShipWeek GetDefaultShipWeek(string programTypeCode,string productFormCategoryCode)
        {
            var productFormCategoryService = new ProductFormCategoryService();
            var productFormCategory = productFormCategoryService.GetByCode(productFormCategoryCode);
            
            var programTypeService = new ProgramTypeService ();
            var programType = programTypeService.GetByCode(programTypeCode);

            return GetDefaultShipWeek(programType, productFormCategory);
        }

        private ShipWeek GetDefaultShipWeek(ProgramType programType, ProductFormCategory productFormCategory)
        {
            if (productFormCategory == null)
            {
                throw new ArgumentNullException("productFormCategory");
            }
            if (programType == null)
            {
                throw new ArgumentNullException("programType");
            }
            return GetDefaultShipWeek(DateTime.Today, programType.Code,productFormCategory.AvailabilityWeekCalculationMethod);
        }

       public ShipWeek GetDefaultShipWeek(DateTime today,string programTypeCode, string availabilityWeekCalcMethod)
        {
            ShipWeek shipWeek = null;
            var shipWeekService = new ShipWeekService();
            shipWeek = shipWeekService.ReturnShipWeekTypeFromDate(today);

           
            if (availabilityWeekCalcMethod == LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeek.Code)
            {
                if (today.DayOfWeek == DayOfWeek.Monday || today.DayOfWeek == DayOfWeek.Tuesday)
                {
                    //shipWeek = shipWeek;
                }
                else
                {
                    shipWeek = shipWeekService.OffsetByWeeks(shipWeek, 1);
                }
            }
            else if (availabilityWeekCalcMethod == LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus1.Code)
            {
                if (today.DayOfWeek >= DayOfWeek.Monday && today.DayOfWeek <= DayOfWeek.Wednesday)
                {
                    shipWeek = shipWeekService.OffsetByWeeks(shipWeek, 1);
                }
                else
                {
                    shipWeek = shipWeekService.OffsetByWeeks(shipWeek, 2);
                }

            }
            else if (availabilityWeekCalcMethod == LookupTableValues.Code.AvailabilityWeekCalcMethod.CurrentWeekPlus3.Code)
            {
                shipWeek = shipWeekService.OffsetByWeeks(shipWeek, 3);
            }
            else if (availabilityWeekCalcMethod == LookupTableValues.Code.AvailabilityWeekCalcMethod.Week11Thru17Odd.Code)
            {
                
                if (programTypeCode == "POI")  //Poinsettia has different rule
                {
                    const int beginWeek = 39;
                    if (shipWeek.Week <= beginWeek)
                    {
                        shipWeek = shipWeekService.ShipWeek(beginWeek, shipWeek.MondayDate.Year);
                    }
                    else
                    {
                        shipWeek = shipWeekService.ShipWeek(beginWeek, shipWeek.MondayDate.Year + 1);
                    }
                }
                else
                {
                    const int beginWeek = 10; //shifted from 11 in year 2016  should probably add a new code
                    const int endWeek = 17;
                    if (shipWeek.Week <= endWeek)
                    {
                        shipWeek = shipWeekService.ShipWeek(GetOddWeekNumberBetweenWeekNumbers(shipWeek.Week, beginWeek, endWeek), shipWeek.MondayDate.AddDays(6).Year);
                    }
                    else
                    {
                        shipWeek = shipWeekService.ShipWeek(beginWeek, shipWeek.MondayDate.Year + 1);
                    }
                }
            }
           

            else
            {
                throw new ArgumentException(string.Format("The AvailabilityWeekCalcMethod {0} is not supported.", availabilityWeekCalcMethod), availabilityWeekCalcMethod);
            }

            return shipWeek;
        }

        public int GetOddWeekNumberBetweenWeekNumbers(int weekNumber, int beginWeekNumber, int endWeekNumber)
        {
            if (weekNumber < beginWeekNumber || weekNumber > endWeekNumber)
            {
                return beginWeekNumber;
            }
            else
            {
                return weekNumber/2*2 + 1;
            }
        }

        public Availability GetAvailability(string userCode, ShipWeek shipWeek, int weeksBefore, int weeksAfter, bool includeCartData, bool includePrices, string programTypeCode = null, string productFormCategoryCode = null, string[] supplierCodeList = null, string[] geneticOwnerCodeList = null, string[] speciesCodeList = null, string[] varietyCodeList = null, bool varietyMatchOptional = false, bool organicOnly = false,string sellerCode = "GFB")
        {
            sellerCode = "GFB";

            var service = new AvailabilityDataService(Status);

            var availabilityData = service.GetAvailability
            (
                userCode: userCode,
                shipWeekCode: shipWeek.Code, 
                weeksBefore: weeksBefore, 
                weeksAfter: weeksAfter, 
                programTypeCode: programTypeCode,
                productFormCategoryCode: productFormCategoryCode, 
                supplierCodes: supplierCodeList,
                geneticOwnerCodes: geneticOwnerCodeList,
                speciesCodes: speciesCodeList,
                varietyCodes: varietyCodeList,
                productGuid: Guid.Empty,
                varietyMatchOptional: varietyMatchOptional,
                includePossibleSubstitutes: false,
                organicOnly: organicOnly,
                sellerCode: sellerCode
            );

            var availability = 
                Availability
                (
                    availabilityData, 
                    includePrices: includePrices,
                    includeCartData: includeCartData, 
                    varietyCodeList: varietyCodeList,
                    sellerCode: sellerCode
                );

            return availability;
        }
        public long GetAvailabilityCount(string userCode, ShipWeek shipWeek, int weeksBefore, int weeksAfter, string programTypeCode = null, string productFormCategoryCode = null, string[] supplierCodeList = null, string[] geneticOwnerCodeList = null, string[] speciesCodeList = null, string[] varietyCodeList = null, bool organicOnly = false, bool availableOnly = false, string sellerCode = "GFB")
        {
            sellerCode = "GFB";
            var service = new AvailabilityDataService(Status);

            var theCount = service.GetAvailabilityCount
            (
                userCode: userCode,
                shipWeekCode: shipWeek.Code,
                weeksBefore: weeksBefore,
                weeksAfter: weeksAfter,
                programTypeCode: programTypeCode,
                productFormCategoryCode: productFormCategoryCode,
                supplierCodes: supplierCodeList,
                geneticOwnerCodes: geneticOwnerCodeList,
                speciesCodes: speciesCodeList,
                varietyCodes: varietyCodeList,
                productGuid: Guid.Empty,            
                organicOnly: organicOnly,
                availableOnly: availableOnly,
                sellerCode: sellerCode
            );



            return theCount;
        }


        public Availability GetAvailabilityByPage(string userCode, ShipWeek shipWeek, int weeksBefore, int weeksAfter, bool includeCartData, bool includePrices, string programTypeCode = null, string productFormCategoryCode = null, string[] supplierCodeList = null, string[] geneticOwnerCodeList = null, string[] speciesCodeList = null, string[] varietyCodeList = null, bool varietyMatchOptional = false, bool organicOnly = false, bool availableOnly = false, string sortOption = "Species", int pageSize = 50, int pageNumber = 1,string sellerCode = "GFB")
        {
            sellerCode = "GFB";
            var service = new AvailabilityDataService(Status);

            var availabilityData = service.GetAvailabilityByPage
            (
                userCode: userCode,
                shipWeekCode: shipWeek.Code,
                weeksBefore: weeksBefore,
                weeksAfter: weeksAfter,
                programTypeCode: programTypeCode,
                productFormCategoryCode: productFormCategoryCode,
                supplierCodes: supplierCodeList,
                geneticOwnerCodes: geneticOwnerCodeList,
                speciesCodes: speciesCodeList,
                varietyCodes: varietyCodeList,
                sortOption: sortOption,
                pageSize: pageSize,
                pageNumber: pageNumber,
                organicOnly: organicOnly,
                availableOnly: availableOnly,
                sellerCode: sellerCode
            );
            
            var availability =
                Availability
                (
                    availabilityData,
                    includePrices: includePrices,
                    includeCartData: includeCartData,
                    varietyCodeList: varietyCodeList,
                    sellerCode: sellerCode
                );

            return availability;
        }


        public Availability GetAvailability(string userCode, Guid growerGuid, ShipWeek shipWeek, Guid productGuid)
        {
            var sellerCode = "GFB";

            var service = new AvailabilityDataService(Status);
            var availabilityData = service.GetAvailability
            (
                userCode: userCode,
                shipWeekCode: shipWeek.Code,
                weeksBefore: 0,
                weeksAfter: 0,
                productGuid: productGuid,
                varietyMatchOptional: false,
                includePossibleSubstitutes: false
            );

            var availability =
                Availability
                (
                    availabilityData,
                    includePrices: true,
                    includeCartData: false,
                    sellerCode: sellerCode
                );

            return availability;
        }

        public Availability GetAvailabilityOfSubstitutes(string userCode, Guid orderLineGuid, string speciesCode, ShipWeek shipWeek = null)
        {
            var sellerCode = "GFB";
            var orderLineTableService = new OrderLineTableService();
            var orderLineTableObject = orderLineTableService.GetByGuid(orderLineGuid);

            if (shipWeek == null)
            {
                shipWeek = new ShipWeek(orderLineTableObject.SupplierOrder.GrowerOrder.ShipWeek);
            }
                        
            var service = new AvailabilityDataService(Status);
            //TODO: Make the procedure aware of whether we need cart or price data, so it doesn't send it, rather than us just ignoring it here.
            var availabilityData = service.GetAvailability
            (
                userCode: userCode,
                shipWeekCode: shipWeek.Code,
                weeksBefore: 0,
                weeksAfter: 0,
                programTypeCode: null,
                productFormCategoryCode: null,
                supplierCodes: null,
                geneticOwnerCodes: null,
                speciesCodes: new string[1] { speciesCode },
                //speciesCodes: null,
                varietyCodes: null,
                productGuid: orderLineTableObject.ProductGuid,
                varietyMatchOptional: false,
                includePossibleSubstitutes: true
            );

            var availabilityOfSubstitutes = AvailabilityOfSubstitutes(availabilityData, orderLineTableObject, sellerCode);

            return availabilityOfSubstitutes;
        }

        Availability AvailabilityOfSubstitutes(AvailabilityData availabilityData, DataObjectLibrary.OrderLine orderLineTableObject,string sellerCode)
        {
            sellerCode = "GFB";
            var availability = Availability(availabilityData, includePrices: true, includeCartData: false, sellerCode: sellerCode);

            var filteredProductList = new List<Product>();

            foreach (var product in availability.ProductList)
            {
                if
                (
                    product.Guid != orderLineTableObject.ProductGuid &&
                    product.HasAvailableQtyOfAtLeast(orderLineTableObject.QtyOrdered)
                )
                {
                    filteredProductList.Add(product);                 
                }
            }

            availability.ProductList = filteredProductList;

            return availability;
        }

        public Availability Availability(AvailabilityData availabilityData, bool includePrices, bool includeCartData, string[] varietyCodeList = null,string sellerCode = "GFB")
        {
            sellerCode = "GFB";
            var availability = new Availability();
            if(availabilityData == null)
            {
                return availability;
            }
            var productDictionary = new Dictionary<Guid, Product>();

            availability.ShipWeekList = new List<ShipWeek>();
            foreach (var shipWeekData in availabilityData.ShipWeekList)
            {
                availability.ShipWeekList.Add(new ShipWeek(shipWeekData));
            }
            availability.ShipWeekList.Sort();



            foreach (var reportedAvailability in availabilityData.ReportedAvailabilityList)
            {
                Product product;

                if (!productDictionary.ContainsKey(reportedAvailability.ProductGuid))
                {
                    product = new Product(reportedAvailability.Product);

                    if (varietyCodeList != null && varietyCodeList.Contains(reportedAvailability.Product.Variety.Code))
                    {
                        product.VarietyWasSelectedByUser = true;
                    }

                    productDictionary.Add(reportedAvailability.ProductGuid, product);
                }
                else
                {
                    productDictionary.TryGetValue(reportedAvailability.ProductGuid, out product);
                }

                if (product == null)
                {
                    throw new ApplicationException("Product should not be null.");
                }

                product.SelectedAvailability.Add(new AvailabilityShipWeek(reportedAvailability));
            }

            if (includeCartData)
            {
                foreach (var orderLine in availabilityData.PreCartOrderLineList)
                {
                    Product product = null;

                    if (productDictionary.ContainsKey(orderLine.ProductGuid))
                    {
                        productDictionary.TryGetValue(orderLine.ProductGuid, out product);
                    }

                    if (product == null)
                    {
                        throw new ApplicationException("Product should not be null.");
                    }

                    if (product.OrderLine != null)
                    {
                       // throw new ApplicationException("There should only be one pre-cart order line per product.");
                    }

                    product.OrderLine = new BusinessObjectsLibrary.OrderLine(orderLine) { IsCarted = false };
                    product.OrderLine.ShipWeekCode = orderLine.ShipWeekCode;
                    product.OrderLines.Add(product.OrderLine);
                }

                foreach (var orderLine in availabilityData.CartedOrderLineList)
                {
                    Product product = null;

                    if (productDictionary.ContainsKey(orderLine.ProductGuid))
                    {
                        productDictionary.TryGetValue(orderLine.ProductGuid, out product);
                    }

                    if (product == null)
                    {
                        throw new ApplicationException("Product should not be null.");
                    }

                    if (product.OrderLine != null)
                    {
                     //   throw new ApplicationException("There should only be one pre-cart or carted order line per product.");
                    }

                    product.OrderLine = new BusinessObjectsLibrary.OrderLine(orderLine) { IsCarted = true }; 
                    product.OrderLine.ShipWeekCode = orderLine.ShipWeekCode;
                    product.OrderLines.Add(product.OrderLine);
                }                
            }

            if (includePrices)
            {
                foreach (var productPrice in availabilityData.ProductPriceList)
                {
                    Product product = null;

                    if (productDictionary.ContainsKey(productPrice.ProductGuid))
                    {
                        productDictionary.TryGetValue(productPrice.ProductGuid, out product);
                    }

                    if (sellerCode == "GFB")
                    {
                        if (product == null)
                        {
                            throw new ApplicationException("Product should not be null.");
                        }

                        if (product.Price != 0)
                        {
                            //      throw new ApplicationException("There should only be one price line per product.");
                        }

                        if (productPrice.PriceGuid != null)
                        {
                            var priceCalculator = new PriceCalculator(productPrice.Price, productPrice.Product.Program.CurrentProgramSeason, DateTime.Today, availabilityData.ShipDate, productPrice.PriceGuid);

                            product.Price = priceCalculator.Price;
                        }
                    }
                    else 
                    {
                        product.Price = Convert.ToDecimal(productPrice.DummenPrice);
                    }

                    
                }
            }

            availability.ProductList = productDictionary.Values.ToList();

            availability.ProductList.Sort((IComparer<Product>)new Product.ProductComparer());
            foreach (var product in availability.ProductList)
            {
                product.SelectedAvailability.Sort();
            }

            return availability;
        }
    }
}

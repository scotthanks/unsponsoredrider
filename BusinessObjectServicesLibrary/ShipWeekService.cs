﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using DataServiceLibrary;

namespace BusinessObjectServices
{
    public class ShipWeekService
    {
        private const int SeedShipWeekIndex = 0;
        private const int SeedShipYearIndex = 1;
        private const int SeedDateYearIndex = 2;
        private const int SeedDateMonthIndex = 3;
        private const int SeedDateDayIndex = 4;
        private const int SeriesStartIndex = 5;

        public const char ShipWeekDelimiterChar = '|';

        private readonly int[] _shipWeekSequences = new[]
            {1, 2012, 2012, 1, 2, 52, 52, 52, 53, 52, 52, 52, 52, 53, 52, 52, 52};

        private int SeedWeek
        {
            get { return _shipWeekSequences[SeedShipWeekIndex]; }
        }

        private int SeedYear
        {
            get { return _shipWeekSequences[SeedShipYearIndex]; }
        }

        private DateTime SeedDate
        {
            get
            {
                return new DateTime(_shipWeekSequences[SeedDateYearIndex], _shipWeekSequences[SeedDateMonthIndex],
                                    _shipWeekSequences[SeedDateDayIndex]);
            }
        }

        private int FirstYearKnown
        {
            get { return SeedWeek; }
        }

        private int LastYearKnown
        {
            get { return SeedYear + Sequences.Length - 1; }
        }

        private DateTime FirstDateKnown
        {
            get { return SeedDate; }
        }

        private DateTime LastDateKnown
        {
            get
            {
                int totalDays = -(SeedWeek*7);
                foreach (int sequence in Sequences)
                {
                    totalDays += sequence*7;
                }
                return SeedDate.AddDays(totalDays);
            }
        }

        private int[] Sequences
        {
            get
            {
                var arr = new int[_shipWeekSequences.Length - SeriesStartIndex];
                Array.Copy(_shipWeekSequences, SeriesStartIndex, arr, 0, arr.Length);
                return arr;
            }
        }

        public int CurrentShipWeekNumber
        {
            get
            {
                DateTime now = DateTime.Today;

                int returnValue = SeedWeek + (((now - SeedDate).Days)/7); //The seed week plus all the weeks since
                foreach (int sequence in Sequences)
                {
                    if (returnValue > sequence)
                    {
                        returnValue = returnValue - sequence;
                    }
                    else
                    {
                        break;
                    }
                    ;
                }
                return returnValue;
            }
        }

        public ShipWeek FirstPossibleOrderShipWeek
        {
            get
            {
                ShipWeek shipWeek = ShipWeek(CurrentShipWeekNumber, DateTime.Today.Year);

                if (DateTime.Today.DayOfWeek != DayOfWeek.Monday && DateTime.Today.DayOfWeek != DayOfWeek.Tuesday)
                    shipWeek = OffsetByWeeks(shipWeek, 1);

                return shipWeek;
            }
        }

        private string _composeShipWeekString(int week, int year)
        {
            return string.Format("{0:D2}{1}{2}", week, ShipWeekDelimiterChar, year);
        }

        public DateTime ReturnMondayDate(DateTime date)
        {
            while (1 != (int) date.DayOfWeek)
            {
                date = date.AddDays(-1);
            }
            return date;
        }

        public DateTime CalculateMondayDate(int shipWeek, int shipYear)
        {
            int[] sequences = Sequences;
            if (shipWeek < 1 || shipWeek > 53 || shipYear < SeedYear || shipYear > LastYearKnown)
            {
                return new DateTime();
            }
            ;
            if (shipWeek == 53 && sequences[shipYear - SeedYear] != 53)
            {
                return new DateTime();
            }
            ;
            int seedWeek = _shipWeekSequences[SeedShipWeekIndex];
            DateTime mondayDate = SeedDate; //begin with seed
            int offsetDays = 0;
            for (int i = SeedYear; i < shipYear; i++)
            {
                //adjust for years
                offsetDays += (sequences[i - SeedYear]*7);
            }
            ;
            offsetDays += (shipWeek - seedWeek)*7; //adjust for weeks
            mondayDate = mondayDate.AddDays(offsetDays);
            return mondayDate;
        }

        public ShipWeek ShipWeek(int week, int year)
        {
            var shipWeek = new ShipWeek();
            // Use CalculateMondayData to determine if this is valid week and year combination, if not return empty
            shipWeek.MondayDate = CalculateMondayDate(week, year);
            if (shipWeek.MondayDate.Year < 2000)
            {
                return new ShipWeek();
            }
            ;
            shipWeek.Week = week;
            shipWeek.Year = year;
            shipWeek.ShipWeekString = _composeShipWeekString(shipWeek.Week, shipWeek.Year);
            return shipWeek;
        }

        public ShipWeek GetShipWeek(int week, int year)
        {
            var shipWeek = new ShipWeek();
            // Use CalculateMondayData to determine if this is valid week and year combination, if not return empty
            shipWeek.MondayDate = this.CalculateMondayDate(week, year);
            if (shipWeek.MondayDate.Year < 2000) { return new ShipWeek(); };

            var shipWeekService = new DataServiceLibrary.ShipWeekTableService();
            var dbShipWeek = shipWeekService.TryGetByField("MondayDate", shipWeek.MondayDate.ToShortDateString(), "=", false);

            shipWeek.Guid = dbShipWeek.Guid;
            shipWeek.Week = week;
            shipWeek.Year = year;
            shipWeek.ShipWeekString = _composeShipWeekString(shipWeek.Week, shipWeek.Year);
            return shipWeek;
        }

        public ShipWeek ReturnShipWeekTypeFromDate(DateTime date)
        {
            date = ReturnMondayDate(date);

            if (date < FirstDateKnown || date > LastDateKnown)
            {
                return new ShipWeek();
            }

            int daysDiff = (date - SeedDate).Days;
            int shipWeek = (daysDiff/7) + SeedWeek;
            int shipYear = SeedYear;

            foreach (int sequence in Sequences)
            {
                if (shipWeek <= sequence)
                {
                    break;
                }
                ;
                shipWeek -= sequence;
                shipYear++;
            }
            ShipWeek ShipWeek = ParseShipWeekString(_composeShipWeekString(shipWeek, shipYear));
            return ShipWeek;
        }

        public ShipWeek OffsetByWeeks(ShipWeek shipWeek, int offsetWeeks)
        {
            var newShipWeek = new ShipWeek();
            // Use CalculateMondayData to determine if this is valid week and year combination, if not return empty
            DateTime mondayDate = CalculateMondayDate(shipWeek.Week, shipWeek.Year);
            if (mondayDate.Year < 2000)
            {
                return newShipWeek;
            }
            ;

            mondayDate = mondayDate.AddDays(offsetWeeks*7);
            newShipWeek = ReturnShipWeekTypeFromDate(mondayDate);
            return newShipWeek;
        }

        public ShipWeek ParseShipWeekCodeOrString(string shipWeekCodeOrString)
        {
            if (string.IsNullOrEmpty(shipWeekCodeOrString))
            {
                throw new ArgumentNullException("shipWeekCodeOrString");
            }

            shipWeekCodeOrString = shipWeekCodeOrString.Trim();

            ShipWeek shipWeek = null;
            if (shipWeekCodeOrString.Length == 7)
            {
                shipWeek = ParseShipWeekString(shipWeekCodeOrString);
            }
            else if (shipWeekCodeOrString.Length == 6)
            {
                shipWeek = ParseShipWeekCode(shipWeekCodeOrString);
            }
            else
            {
                throw new ArgumentException(
                    string.Format(
                        "The shipWeekCodeOrString parameter \"{0}\" is not in a valid ship week code or string.",
                        shipWeekCodeOrString), "shipWeekCodeOrString");
            }

            return shipWeek;
        }

        public ShipWeek ParseShipWeekCode(string shipWeekCode)
        {
            ShipWeek shipWeek = null;

            if (string.IsNullOrEmpty(shipWeekCode))
            {
                throw new ArgumentNullException(shipWeekCode);
            }
            else if (shipWeekCode.Length != 6)
            {
                throw new ArgumentException("The length of the parameter must be 6.", "shipWeekCode");
            }
            else
            {
                int year;
                int.TryParse(shipWeekCode.Substring(0, 4), out year);

                int weekNumber;
                int.TryParse(shipWeekCode.Substring(4, 2), out weekNumber);

                if (year < 2013 || weekNumber == 0 || weekNumber > 54)
                {
                    throw new ArgumentException(
                        "The shipWeek code must be in the format YYYYWW where YYYY is the year and WW is a week number between 1 and 53");
                }

                shipWeek = ShipWeek(weekNumber, year);
            }

            return shipWeek;
        }

        public ShipWeek ParseShipWeekString(string shipWeekString)
        {
            //Note: ShipWeek String format = "##|####";
            //Note: This handles any single-character separator, eg "11/2012"

            const int EXPECTED_LENGTH = 7;

            if (shipWeekString == null) return new ShipWeek();

            if (shipWeekString.Length == (EXPECTED_LENGTH - 1))
            {
                //prepend a zero
                shipWeekString = string.Format("0{0}", shipWeekString);
            }
            ;
            if (shipWeekString.Length != EXPECTED_LENGTH) return new ShipWeek();

            var shipWeek = new ShipWeek();
            try
            {
                //this approach actually ignors current value of delimiter
                shipWeek.Week = Convert.ToInt32(shipWeekString.Substring(0, 2));
                shipWeek.Year = Convert.ToInt32(shipWeekString.Substring(3));
            }
            catch (FormatException fe)
            {
                return new ShipWeek();
            }
            catch (OverflowException oe)
            {
                return new ShipWeek();
            }

            shipWeek.MondayDate = CalculateMondayDate(shipWeek.Week, shipWeek.Year);
            if (shipWeek.MondayDate.Year < 2000)
            {
                return new ShipWeek();
            }

            shipWeek.ShipWeekString = _composeShipWeekString(shipWeek.Week, shipWeek.Year);

            return shipWeek;
        }

        public List<ShipWeek> GetShipWeeks(Guid userGuid, string userCode, string beginShipWeekCode, int numberOfWeeks)
        {
            var status = new StatusObject(userGuid, true);
            var shipWeekDataGetProcedure = new ShipWeekDataGetProcedure(status);

            List<DataObjectLibrary.ShipWeek> shipWeekTableObjectList = shipWeekDataGetProcedure.GetShipWeekData(
                userGuid, userCode, beginShipWeekCode, numberOfWeeks);

            var shipWeekList = new List<ShipWeek>();
            foreach (DataObjectLibrary.ShipWeek shipWeekTableRow in shipWeekTableObjectList)
            {
                var shipWeek = new ShipWeek(shipWeekTableRow);

                shipWeekList.Add(shipWeek);
            }

            return shipWeekList;
        }

        public List<ShipWeek> GetAllShipWeeks(int? shipWeekBegin, int? shipYearBegin, int shipWeekEnd, int shipYearEnd)
        {
            var shipWeekService = new ShipWeekTableService();
            IEnumerable<DataObjectLibrary.ShipWeek> shipWeeks = shipWeekService.GetAll();

            var shipWeekList = new List<ShipWeek>();

            foreach (DataObjectLibrary.ShipWeek shipWeek in shipWeeks)
            {
                if ((shipWeek.Year == shipYearBegin && shipWeek.Week >= shipWeekBegin) || shipWeek.Year > shipYearBegin)
                {
                    if ((shipWeek.Year == shipYearEnd && shipWeek.Week <= shipWeekEnd) || shipWeek.Year < shipYearEnd)
                    {
                        var oShipWeek = new ShipWeek(shipWeek);
                        shipWeekList.Add(oShipWeek);
                    }
                }
            }

            return shipWeekList;
        }
    }
}
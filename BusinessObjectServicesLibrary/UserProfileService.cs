﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using BusinessObjectsLibrary;
using WebMatrix.WebData;
using System.Web;
using DataServiceLibrary;

namespace BusinessObjectServices
{
    public class UserProfileService
    {
        public bool CreateUser(string email, string password, Guid growerGuid, out string message)
        {
            bool success;

            try
            {
                WebSecurity.CreateUserAndAccount(email, password);

                var userProfileDataService = new UserProfileTableService();
                var userProfileDataObject = userProfileDataService.Get(email);
                userProfileDataObject.EmailAddress = email;
                userProfileDataObject.GrowerGuid = growerGuid;
                userProfileDataObject = userProfileDataService.Update(userProfileDataObject);

                if (userProfileDataObject.EmailAddress == email && userProfileDataObject.GrowerGuid == growerGuid)
                {
                    WebSecurity.Login(email, password);

                    success = true;
                    message = "User created.";                    
                }
                else
                {
                    success = false;
                    message = "The user profile was not successfully set up.";
                }
            }
            catch (MembershipCreateUserException e)
            {
                success = false;
                message = GetMessageFromErrorCode(e.StatusCode);
            }

            return success;
        }

        public BusinessObjectsLibrary.UserProfile Get(string email)
        {
            var service = new UserProfileTableService();
            DataObjectLibrary.UserProfile up = service.Get(email);
            return new UserProfile(up.UserId,up.UserName,up.EmailAddress,up.UserGuid,up.GrowerGuid);
        }

        public bool Delete(string email)
        {
            bool success = false;

            var service = new UserProfileTableService();

            var userProfile = service.Get(email);

            if (userProfile != null)
            {
                success = service.Delete(userProfile);
            }

            return success;
        }

        public DataObjectLibrary.UserProfile CreateGrowerUser(GrowerUser growerUser, out string message)
        {

            var newUser = new DataObjectLibrary.UserProfile();
            message = "";

            try
            {
                WebSecurity.CreateUserAndAccount(growerUser.Email, growerUser.Password);

                var userProfileDataService = new UserProfileTableService();
                var user = userProfileDataService.Get(growerUser.Email);
                user.EmailAddress = growerUser.Email;
                user.GrowerGuid = growerUser.GrowerUserGuid;
                user = userProfileDataService.Update(user);

                if (user.EmailAddress == growerUser.Email && user.GrowerGuid == growerUser.GrowerUserGuid)
                {
                    newUser = user;
                }
            }
            catch (MembershipCreateUserException e)
            {
                message = GetMessageFromErrorCode(e.StatusCode);
            }


            return newUser;
        }



        public DataObjectLibrary.UserProfile GetUserProfile(string growerUseremail, out string message)
        {

            var newUser = new DataObjectLibrary.UserProfile();
            message = "";

            try
            {
               

                var userProfileDataService = new UserProfileTableService();
                var user = userProfileDataService.Get(growerUseremail);
               
                newUser = user;
               
            }
            catch (MembershipCreateUserException e)
            {
                message = GetMessageFromErrorCode(e.StatusCode);
            }


            return newUser;
        }



        public Role GetUserRole(int userId)
        {
            UserProfileTableService userTableService = new UserProfileTableService();
            DataObjectLibrary.Role dbRole = userTableService.GetUserRole(userId);

            Role userRole = new Role();
            userRole.RoleId = dbRole.RoleId;
            userRole.RoleName = dbRole.RoleName;

            return userRole;
        }

        public bool GrantRole(string email, bool isAdmin, bool isAuthorizedToPlaceOrders, bool isAuthorizedToSeePrices)
        {
            var message = "";
            var profile = GetUserProfile( email,out message);
            var currentRoleObj = GetUserRole(profile.UserId);

            DataObjectLibrary.UserProfile.RoleEnum currentRole = new DataObjectLibrary.UserProfile.RoleEnum();
            if (currentRoleObj.RoleName == "CustomerAdministrator")
            {currentRole =DataObjectLibrary.UserProfile.RoleEnum.CustomerAdministrator;}
            else if (currentRoleObj.RoleName == "CustomerOrderer")
            { currentRole = DataObjectLibrary.UserProfile.RoleEnum.CustomerOrderer; }
            else if (currentRoleObj.RoleName == "CustomerReadOnly")
            { currentRole = DataObjectLibrary.UserProfile.RoleEnum.CustomerReadOnly; }



            DataObjectLibrary.UserProfile.RoleEnum role;

            if (isAdmin && isAuthorizedToPlaceOrders && isAuthorizedToSeePrices)
            {
                role = DataObjectLibrary.UserProfile.RoleEnum.CustomerAdministrator;
            }
            else if (isAuthorizedToPlaceOrders && isAuthorizedToSeePrices)
            {
                role = DataObjectLibrary.UserProfile.RoleEnum.CustomerOrderer;
            }
            else if (isAuthorizedToSeePrices)
            {
                role = DataObjectLibrary.UserProfile.RoleEnum.CustomerReadOnly;
            }
            else
            {
                role = DataObjectLibrary.UserProfile.RoleEnum.CustomerReadOnlyNoPrice;
            }

            var service = new UserProfileTableService();
            var del = service.RevokeRole(profile.UserName, currentRole);
            return service.GrantRole(email, role);
        }

        public bool UserHasPermissionToSeePrices(Guid userGuid)
        {
            return UserHasPermission(userGuid, "SeePrices");
        }

        public bool UserHasPermissionToPlaceOrders(Guid userGuid)
        {
            return UserHasPermission(userGuid, "PlaceOrders");
        }

        private bool UserHasPermission(Guid userGuid, string permissionName)
        {
            var userProfileTableService = new UserProfileTableService();
            return userProfileTableService.UserHasPermission(userGuid, permissionName);
        }

        private static string GetMessageFromErrorCode(MembershipCreateStatus createStatus)
        {
            string message;
            string fieldName;

            ErrorCodeGetData(createStatus, out message, out fieldName);

            return message;
        }

        private static string GetFieldNameFromErrorCode(MembershipCreateStatus createStatus)
        {
            string message;
            string fieldName;

            ErrorCodeGetData(createStatus, out message, out fieldName);

            return fieldName;
        }

        private static void ErrorCodeGetData(MembershipCreateStatus createStatus, out string message, out string fieldName)
        {
            fieldName = "Unknown";

            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    message = "User name already exists. Please enter a different user name.";
                    fieldName = "UserCode";
                    break;

                case MembershipCreateStatus.DuplicateEmail:
                    message = "A user name for that e-mail address already exists. Please enter a different e-mail address.";
                    fieldName = "UserCode";
                    break;

                case MembershipCreateStatus.InvalidPassword:
                    message = "The password provided is invalid. Please enter a valid password value.";
                    fieldName = "Password";
                    break;

                case MembershipCreateStatus.InvalidEmail:
                    message = "The e-mail address provided is invalid. Please check the value and try again.";
                    fieldName = "UserCode";
                    break;

                case MembershipCreateStatus.InvalidAnswer:
                    message = "The password retrieval answer provided is invalid. Please check the value and try again.";
                    break;

                case MembershipCreateStatus.InvalidQuestion:
                    message = "The password retrieval question provided is invalid. Please check the value and try again.";
                    break;

                case MembershipCreateStatus.InvalidUserName:
                    message = "The user name provided is invalid. Please check the value and try again.";
                    fieldName = "UserCode";
                    break;

                case MembershipCreateStatus.ProviderError:
                    message = "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
                    break;

                case MembershipCreateStatus.UserRejected:
                    message = "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
                    break;

                default:
                    message = "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
                    break;
            }
        }
    }
}

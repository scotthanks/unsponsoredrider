﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectServices
{
    public class ProgramService
    {

        public List<BusinessObjectsLibrary.Program> GetAllPrograms()
        {

            var programService = new DataServiceLibrary.ProgramTableService();
            var programs = programService.GetAll();

            var programList = new List<BusinessObjectsLibrary.Program>();

            foreach (var program in programs)
            {

                var newProgram = new BusinessObjectsLibrary.Program();
                newProgram.ProgramGuid = program.Guid;
                newProgram.Code = program.Code;
                newProgram.Name = program.Name;
                newProgram.SupplierGuid = program.SupplierGuid;
                newProgram.ProgramTypeGuid = program.ProgramTypeGuid;
                programList.Add(newProgram);

            }

            return programList;

        }

        public List<BusinessObjectsLibrary.Program> GetSupplierPrograms(Guid supplierGuid)
        {

            var programService = new DataServiceLibrary.ProgramTableService();
            var programs = programService.GetAllByField("SupplierGuid", supplierGuid.ToString(), "=", false);

            var programList = new List<BusinessObjectsLibrary.Program>();

            foreach (var program in programs)
            {

                var newProgram = new BusinessObjectsLibrary.Program();
                newProgram.ProgramGuid = program.Guid;
                newProgram.Code = program.Code;
                newProgram.Name = program.Name;
                newProgram.SupplierGuid = program.SupplierGuid;
                newProgram.ProgramTypeGuid = program.ProgramTypeGuid;
                programList.Add(newProgram);

            }
            
         
            return programList;
        }
    }
}

﻿using System;
using System.Configuration;
using System.Globalization;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Web.UI.WebControls;
using BusinessObjectsLibrary;

using DataServiceLibrary;
using DataAccessLibrary;

using BusinessObjectsLibrary.Catalog;

namespace BusinessObjectServices
{
    public class CatalogService : BusinessServiceBase
    {
        public CatalogService(StatusObject status)
            : base(status)
        { }

        public SelectionsType SelectionsGet(string categoryCode, string productFormCode)
        {
            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var selections = new SelectionsType();
                var categoryList = new List<CategoryType>();
                var productFormList = new List<ProductFormType>();
                var supplierList = new List<SupplierType>();
                var speciesList = new List<SpeciesType>();
                var varietiesList = new List<VarietyType>();
                var speciesToSuppliersList = new List<SpeciesToSuppliersType>();
                
             
                var service = new CatalogDataService(Status);
                var reader = service.AllGet(connection, categoryCode, productFormCode);
               // Categories
                while (reader.Read())
                {
                    categoryList.Add(new CategoryType()
                    {
                        Guid = (Guid)reader["ProgramTypeGuid"],
                        Code = reader["ProgramTypeCode"].ToString(),
                        Name = reader["ProgramTypeName"].ToString()
                    });
                }
                selections.Categories = categoryList;

                // Product Forms
                reader.NextResult();
                while (reader.Read())
                {
                    productFormList.Add(new ProductFormType()
                    {
                        Guid = (Guid)reader["ProductFormCategoryGuid"],
                        Code = reader["ProductFormCategoryCode"].ToString(),
                        Name = reader["ProdcutFormCategoryName"].ToString(),
                        CategoryGuid = (Guid)reader["ProgramTypeGuid"]
                    });
                }
                selections.ProductForms = productFormList;

                // Suppliers
                reader.NextResult();
                while (reader.Read())
                {
                    supplierList.Add(new SupplierType()
                    {
                        Guid = (Guid)reader["Guid"],
                        Code = reader["Code"].ToString(),
                        Name = reader["Name"].ToString()
                    });
                }
                selections.Suppliers = supplierList;


                // Species
                reader.NextResult();
                while (reader.Read())
                {
                    speciesList.Add(new SpeciesType()
                    {
                        Guid = (Guid)reader["Guid"],
                        Code = reader["Code"].ToString(),
                        Name = reader["Name"].ToString()
                    });
                }
                selections.Species = speciesList;

                // Varieties
                reader.NextResult();
                while (reader.Read())
                {
                    //Scott Added back in Secure handling                    
                    string sVarURL = GetImageUrl(reader["ImageFileName"].ToString());
                    if (Status.IsSecureRequest)
                    {
                        sVarURL = sVarURL.Replace("http://", "https://");
                    }
                    
                    varietiesList.Add(new VarietyType()
                    {
                        Code = reader["VarietyCode"].ToString(),
                        Name = reader["VarietyName"].ToString(),
                        //GeneticOwnerCode = varietyTableObject.GeneticOwner.Code,
                        GeneticOwnerName = reader["GeneticOwnerName"].ToString(),
                        SpeciesCode = reader["SpeciesCode"].ToString(),
                        SpeciesGuid = new Guid( reader["SpeciesGuid"].ToString()),
                        // ImageUrl = thumbnailImageUrl,




                        ThumbnailImageUrl = sVarURL,
                        // FUllImageUrl = fullImageUrl,
                        // CultureLibraryUrl = varietyTableObject.CultureLibraryLink
                    });
                }
                selections.Varieties = varietiesList;


                // SpeciesToSuppliers
                reader.NextResult(); 
                while (reader.Read())
                {
                    speciesToSuppliersList.Add(new SpeciesToSuppliersType()
                    {

                        SpeciesCode = reader["SpeciesCode"].ToString(),
                        SupplierCode = reader["SupplierCode"].ToString()
                    });
                }
                selections.SpeciesToSuppliers = speciesToSuppliersList;
                
                Status.Success = true;
                Status.StatusMessage = "Selections Loaded.";
                return selections;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;
               
            }
            finally
            { connection.Close(); }
          
        }

        public IEnumerable<CategoryType> CategoriesGet()
        {
            
            try
            {
                var service = new CatalogDataService(Status);
                var list = service.CategoriesGet();
                return list;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;
               
            }
            finally
            { }
        }

        public IEnumerable<ProductFormType> ProductFormsGet(string categoryCode)
        {

            try
            {
                var service = new CatalogDataService(Status);
                var list = service.ProductFormsGet(categoryCode);
                return list;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;

            }
            finally
            { }
        }

        public IEnumerable<SupplierType> SuppliersGet(Guid userGuid, string categoryCode, string productFormCode)
        {

            try
            {
                var service = new CatalogDataService(Status);
                var list = service.SuppliersGet(userGuid, categoryCode, productFormCode);
                return list;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;

            }
            finally
            { }
        }

        
        public IEnumerable<SpeciesType> SpeciesGet(string categoryCode, string productFormCode, string supplierCodes)
        {
            try
            {
                var service = new CatalogDataService(Status);
                var list = service.SpeciesGet(categoryCode, productFormCode, supplierCodes);
                return list;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;

            }
            finally
            { }
        }
        public IEnumerable<SpeciesToSuppliersType> SpeciesSuppliersGet(string categoryCode, string productFormCode)
        {
            try
            {
                var service = new CatalogDataService(Status);
                var list = service.SpeciesSuppliersGet(categoryCode, productFormCode);
                return list;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;

            }
            finally
            { }
        }



        //public IEnumerable<BusinessObjectsLibrary.Species> VarietiesInSpeciesGet(string categoryCode, string productFormCode, string supplierCodes, string speciesCodes)
        //{
        //    try
        //    {
        //        var service = new CatalogDataService(Status);
        //        var list = service.VarietiesInSpeciesGet(categoryCode, productFormCode, supplierCodes, speciesCodes);
        //        return list;
        //    }
        //    catch (Exception ex)
        //    {
        //        var error = new ErrorObject()
        //        {
        //            ErrorNumber = ex.HResult,
        //            ErrorLocation = "CatalogService",
        //            ErrorMessageUser = ex.Message,
        //            ErrorMessageSystem = ex.Message,
        //            Error = ex.Source
        //        };
        //        AddError(error);
        //        Status.Success = false;
        //        return null;

        //    }
        //    finally
        //    { }
            
        //}

        public IEnumerable<VarietyType> VarietiesInSpeciesGetV2(string categoryCode,
            string productFormCode, string supplierCodes, string speciesCodes)
        {

            try
            {
                var service = new CatalogDataService(Status);
                var list = service.VarietiesInSpeciesGetV2(categoryCode, productFormCode, supplierCodes, speciesCodes);
                return list;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CatalogService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;

            }
            finally
            { }
        }

       

        public VarietyDetailType VarietyDetailGet(string varietyCode)
        {
            var product = new VarietyDetailType();
            var service = new BusinessObjectsLibrary.VarietyService();
            var variety = service.GetVarietyByCode(varietyCode);
            
            product.VarietyCode = varietyCode;
            
            string seriesCultureLibraryLink = "";
            if (!variety.SeriesGuid.Equals(null))
            {
                var seriesService = new SeriesService();
                var series = seriesService.GetSeriesFromGuid(variety.SeriesGuid);
                product.SeriesDescriptionHtmlGuid = series.DescriptionHTMLGuid;
                seriesCultureLibraryLink = series.CultureLibraryLink;
            }
            product.Code = variety.Code;
            product.Name = variety.Name;
            product.SpeciesName = variety.SpeciesName;
            product.VarietyName = variety.Name;
            product.BreederName = variety.GeneticOwnerName;

            product.ColorDescription = variety.ColorDescription;
            product.Color = variety.Color;
            product.Habit = variety.Habit;
            product.Vigor = variety.Vigor;
            product.DescriptionHtmlGuid = variety.DescriptionHtmlGuid;
            product.VarietyImageUrl = variety.FUllImageUrl;

            product.CultureLibraryUrl = Utility.StringStuffScott.Coalesce(variety.CultureLibraryURL, seriesCultureLibraryLink);

            if (product.CultureLibraryUrl != "")
            {
                product.CultureNotesLinkText = "Culture Notes";
            }

            Status.Success = true;
            Status.StatusMessage = "Product Loaded";
            return product;
        }



        private string GetImageUrl( string imageName)
        {

            string imageUrl = "";
            if (imageName == "")
            { 
                imageUrl = GetImageUrl("Unknown_Thumbnail", "JPG"); 
            }
            else
            {
                imageUrl = GetImageUrl(imageName, "JPG");
            }

            return imageUrl;
        }

        private string GetImageUrl( string imageName, string extension)
        {
            return string.Format("{0}/{1}.{2}", GetImagePath(), imageName, extension.ToLower());
        }

        private  string GetImagePath()
        {
            var imageContainerObject = ConfigurationManager.AppSettings["EpsStorageUrl"];
            if (imageContainerObject == null)
            {
                throw new ApplicationException(string.Format("There is no AppSetting with a name of {0} in the config file.", "EpsStorageUrl"));
            }

            return string.Format("{0}/{1}", imageContainerObject.ToString(CultureInfo.InvariantCulture), "CatalogImages".ToLower());
        }
    }
}

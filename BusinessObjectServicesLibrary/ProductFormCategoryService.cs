﻿using System.Collections.Generic;
using System.Linq;
using BusinessObjectsLibrary;
using DataServiceLibrary;

namespace BusinessObjectServices
{
    public class ProductFormCategoryService
    {
        public List<ProductFormCategory> GetAll()
        {
            var productFormCategoryTableService = new ProductFormCategoryTableService();

            return
                productFormCategoryTableService.GetAll()
                    .Select(
                        productFormCategoryDataRow =>
                        new ProductFormCategory(productFormCategoryDataRow))
                    .ToList();
        }

        public ProductFormCategory GetByCode(string productFormCategoryCode)
        {
            //TODO: This requires a round trip to the database. This could be avoided by caching.
            var productFormCategoryTableService = new ProductFormCategoryTableService();
            var productFormCategoryTableObject = productFormCategoryTableService.GetByCode(productFormCategoryCode, excludeInactive: true);
            return new ProductFormCategory(productFormCategoryTableObject);
        }
    }
}
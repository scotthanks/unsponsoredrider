﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BusinessObjectsLibrary;
using DataServiceLibrary;

using System.Data;
using System.Data.SqlClient;


namespace BusinessObjectServices
{
    public class ContentService
    {

       

        public string DynamicContent(Guid guid)
        {
            var returnString = string.Empty;
            try
            {
                var userGuid = new Guid();
                var status = new StatusObject(userGuid, true);
                var service = new DataServiceLibrary.ContentDataService(status);
                returnString = service.GetDynamicContentHtml(guid);
            }
            catch { }
            return returnString;
        }

        public ManagedDynamicContentType GetDynamicContentType(Guid Guid)
        {
            
            var dynamicContent = new ManagedDynamicContentType();
            
            using (SqlConnection connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                string sqlString = "SELECT TOP(1) * FROM DynamicContent WHERE IsLive = 1 AND Guid = @Guid";

                SqlCommand command = new SqlCommand(sqlString, connection);
                command.CommandType = CommandType.Text;
                command.Parameters.Add(new SqlParameter("@Guid", Guid));
                command.CommandTimeout = 5;

                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        dynamicContent.Guid = Guid;
                        dynamicContent.Html = reader["Html"].ToString();
                        dynamicContent.IsLoaded = true;
                    }
                }
                else
                {
                    dynamicContent.Guid = new Guid();
                    dynamicContent.Html = string.Empty;
                    dynamicContent.IsLoaded = false; ;
                }
                reader.Close();
            };

            return dynamicContent;
        }

        public IEnumerable<SearchResultTemplateType> GetSearchResultTemplates()
        {

            var list = new List<SearchResultTemplateType>();


            using (SqlConnection connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                string sqlString = "SELECT * FROM ResultTemplate WHERE IsLive = 1;";

                SqlCommand command = new SqlCommand(sqlString, connection);
                command.CommandType = CommandType.Text;
                command.CommandTimeout = 5;

                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var template = new SearchResultTemplateType();
                        template.Guid = Guid.Empty;
                        var ord = reader.GetOrdinal("Guid");
                        if (!reader.IsDBNull(ord)) { template.Guid = reader.GetGuid(ord); };

                        template.IsLive = reader.GetBoolean(reader.GetOrdinal("IsLive"));
                        template.Type = reader["Type"].ToString() ?? string.Empty;
                        template.Html = reader["Html"].ToString() ?? string.Empty;
                        template.Css = reader["Css"].ToString() ?? string.Empty;
                        template.Javascript = reader["Javascript"].ToString() ?? string.Empty;
                        template.Description = reader["Description"].ToString() ?? string.Empty;
                        template.Note = reader["Note"].ToString() ?? string.Empty;

                        template.IsLoaded = true;

                        list.Add(template);

                    }
                };
                //else
                //{
                //    template.IsLoaded = false; ;
                //}
                reader.Close();
            };


            return list;
        }

        public SearchResultTemplateType GetSearchResultTemplateType(Guid guid)
        {

            var template = new SearchResultTemplateType();

            using (SqlConnection connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                string sqlString = "SELECT TOP(1) * FROM ResultTemplate WHERE IsLive = 1 AND Guid = @Guid";

                SqlCommand command = new SqlCommand(sqlString, connection);
                command.CommandType = CommandType.Text;

                command.Parameters.Add(new SqlParameter("@Guid", guid));
                command.CommandTimeout = 5;

                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        //public Guid Guid { get; set; }
                        //public bool IsLive { get; set; }
                        //public string Type { get; set; }
                        //public string Html { get; set; }
                        //public string Css { get; set; }
                        //public string Javascript { get; set; }
                        //public string Description { get; set; }
                        //public string Note { get; set; }
                        //public bool IsLoaded { get; set; }



                        template.Guid = Guid.Empty;
                        var ord = reader.GetOrdinal("Guid");
                        if (!reader.IsDBNull(ord)) { template.Guid = reader.GetGuid(ord); };

                        template.IsLive = reader.GetBoolean(reader.GetOrdinal("IsLive"));

                        template.Type = reader["Type"].ToString() ?? string.Empty;
                        template.Html = reader["Html"].ToString() ?? string.Empty;
                        template.Css = reader["Css"].ToString() ?? string.Empty;
                        template.Javascript = reader["Javascript"].ToString() ?? string.Empty;
                        template.Description = reader["Description"].ToString() ?? string.Empty;
                        template.Note = reader["Note"].ToString() ?? string.Empty;




                        template.IsLoaded = true;
                    }
                }
                else
                {
                    template.IsLoaded = false; ;
                }
                reader.Close();
            };


            return template;
        }

        public ManagedPageContentType GetStaticContentType(string Path)
        {

            var staticContent = new ManagedPageContentType();

            Guid CssPageGuid = new Guid(); 

            //First Retrieve the Page Content for this Path
            using (SqlConnection connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                string sqlString = "SELECT TOP(1) * FROM PageContent WHERE IsLive = 1 AND Path = @Path";

                SqlCommand command = new SqlCommand(sqlString, connection);
                command.CommandType = CommandType.Text;

                command.Parameters.Add(new SqlParameter("@Path", Path));
                command.CommandTimeout = 50;

                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        staticContent.Title = reader["Title"].ToString() ?? string.Empty;
                        staticContent.Html = reader["Html"].ToString() ?? string.Empty;
                        staticContent.Css = reader["Css"].ToString() ?? string.Empty;
                        staticContent.Javascript = reader["Javascript"].ToString() ?? string.Empty;
                        staticContent.JavascriptOnReady = reader["JavascriptOnReady"].ToString() ?? string.Empty;



                        var ord = reader.GetOrdinal("CssPageGuid");
                        if (!reader.IsDBNull(ord))
                        {
                            CssPageGuid = reader.GetGuid(ord);
                        }

                        staticContent.IsLoaded = true;
                    }
                }
                else
                {
                    staticContent.IsLoaded = false; ;
                }
                reader.Close();
            };

            //ToDo: Validate CssPageGuid before using it
            if (staticContent.IsLoaded) {



                //Now we need to retrieve the CssPage itself
                using (SqlConnection connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
                {
                    connection.Open();
                    string sqlString = "SELECT TOP(1) * FROM CSSPages WHERE IsLive = 1 AND Guid = @Guid";

                    SqlCommand command = new SqlCommand(sqlString, connection);
                    command.CommandType = CommandType.Text;

                    command.Parameters.Add(new SqlParameter("@Guid", CssPageGuid));
                    command.CommandTimeout = 5;

                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            staticContent.CssPage = reader["Css"].ToString();
                        }
                    }
                    else
                    {
                        staticContent.CssPage = string.Empty;
                    }
                    reader.Close();
                }

            };

            return staticContent;





            //var service = new DataServiceLibrary.ContentService();
            //staticContent.Html = service.GetStaticContentHtml(path);


            ////////////////////////////////////////////////////////////////
            //This service is now deprecated
            
            //staticContent.PageCss = null;
            //staticContent.HeadCss = null;
            //ToDo: Rick I need to retrieve HeadCSS (a new column) from the same table
            //The Data Service appears to make a trip to get a single column called ContentHTML
            //I don't want to make multiple trips for this everytime a page is loaded.
            //There is another new column that is more complex.  it is a guid that will point elsewhere in the same table to get Page Level CSS
            //I need to populate StaticContentType.cs which has three properties and may have more over time.
            ////////////////////////////////////////////////////////////////

            //return staticContent;

        }


    }
}

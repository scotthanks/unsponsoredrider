﻿using System.Data.SqlClient;
using System.Web;
using BusinessObjectsLibrary;
using DataAccessLibrary;
using DataObjectLibrary;
using DataServiceLibrary;
using System;

using System.Collections.Generic;
using System.Threading;

using GrowerOrderHistoryEvent = BusinessObjectsLibrary.GrowerOrderHistoryEvent;
using GrowerOrder = BusinessObjectsLibrary.GrowerOrder;
using Lookup = DataObjectLibrary.Lookup;
using OrderLine = DataObjectLibrary.OrderLine;
using SupplierOrder = DataObjectLibrary.SupplierOrder;


namespace BusinessObjectServices
{
    public class GrowerOrderService
    {
        public static bool RoundTripToDatabaseHasOccurredFORDEBUGGINGPURPOSESONLY;

        public GrowerOrder GetGrowerOrderDetail(Guid growerOrderGuid, bool includePriceData,bool includePreCart = true)
        {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var dataService = new GrowerOrderDataService(status);

            var growerOrderData = dataService.GetGrowerOrderDetailData(growerOrderGuid, includePriceData);

            var growerOrder = new GrowerOrder(growerOrderData.GrowerOrder);

            growerOrder.GrowerOrderFeeList = new List<BusinessObjectsLibrary.GrowerOrderFee>();
            foreach (DataObjectLibrary.GrowerOrderFee growerOrderFee in growerOrderData.GrowerOrderFeeList)
            {
               
                var newFee = new BusinessObjectsLibrary.GrowerOrderFee(growerOrderFee);
                growerOrder.GrowerOrderFeeList.Add(newFee);

            }


            growerOrder.Grower.LoadShipToAddresses(growerOrderData.GrowerOrder.Grower.GrowerAddressList);
            growerOrder.Grower.LoadCreditCards(growerOrderData.GrowerOrder.Grower.GrowerCreditCardList);

            var priceCalculatorDictionary = new Dictionary<Guid, PriceCalculator>();

            foreach (ProductGrowerSeasonPriceView priceTableRow in growerOrderData.PriceList)
            {
                if (priceCalculatorDictionary.ContainsKey(priceTableRow.ProductGuid))
                {
                    throw new ApplicationException("There should only be one price per product.");
                }
                else
                {
                    priceCalculatorDictionary.Add(priceTableRow.ProductGuid, new PriceCalculator(priceTableRow.Price, priceTableRow.Product.Program.CurrentProgramSeason, growerOrderData.GrowerOrder.DateEntered, growerOrderData.GrowerOrder.ShipWeek.MondayDate, priceTableRow.PriceGuid));
                }
            }

            GetOrderHistory(growerOrderData.GrowerOrderHistoryList, growerOrder.GrowerOrderHistoryEvents);

            foreach (SupplierOrder supplierOrderTableObject in growerOrderData.GrowerOrder.SupplierOrderList)
            {
                var supplierOrder = new BusinessObjectsLibrary.SupplierOrder(supplierOrderTableObject);

                foreach (OrderLine orderLineTableObject in supplierOrderTableObject.OrderLineList)
                {
                    var orderLine = new BusinessObjectsLibrary.OrderLine(orderLineTableObject);

                    //ShipWeekService swService = new ShipWeekService();
                    //DateTime currentWeekMonday = swService.ReturnMondayDate(DateTime.Today);
                    DateTime orderShipWeekMonday = growerOrderData.GrowerOrder.ShipWeek.MondayDate;
                    int daysToSubtract = orderLine.Product.ProductionLeadTimeWeeks * 7;
                    DateTime productionWeekMonday = orderShipWeekMonday.AddDays(daysToSubtract * -1);
                    DateTime theFridayB4ShipWeekOfOrder = orderShipWeekMonday.AddDays(-3);
                    
                    if (orderLine.OrderLineStatus.Code == LookupTableValues.Code.OrderLineStatus.Shipped.Code 
                        || DateTime.Today >= theFridayB4ShipWeekOfOrder)
                    {
                        orderLine.IsLockedForAllChanges = true;
                    }

                    if (orderLine.IsLockedForAllChanges || DateTime.Today >= productionWeekMonday)
                    {
                        orderLine.IsLockedForReduction = true;
                    }




                    if (includePreCart == false)
                    {
                        // Skip any OrderLines that have a status less than Pending (i.e. not yet in the cart).
                        if
                            (
                                orderLine.OrderLineStatus.SortSequence <
                                LookupTableValues.Code.OrderLineStatus.Pending.SortSequence
                            )
                        {
                            continue;
                        }
                    }

                    PriceCalculator priceCalculator;
                    bool priceCalculatorFound = 
                        priceCalculatorDictionary.TryGetValue(orderLineTableObject.ProductGuid, out priceCalculator);

                    if (priceCalculatorFound)
                    {
                        orderLine.CurrentProductPriceGuid = priceCalculator.PriceGuid;
                        orderLine.CurrentProductPrice = priceCalculator.Price;
                        orderLine.CurrentProductCost = priceCalculator.Cost;

                        if (orderLine.PriceUsedGuid == null)
                        {
                            orderLine.PriceUsedGuid = orderLine.CurrentProductPriceGuid;
                            orderLine.Price = orderLine.CurrentProductPrice;
                            orderLine.Cost = orderLine.CurrentProductCost;
                            orderLine.PriceHasBeenSet = false;
                        }
                        else
                        {
                            orderLine.PriceHasBeenSet = true;
                        }
                    }
                   
                    orderLine.DateChangedString = orderLineTableObject.DateLastChanged.ToString();
                    orderLine.DateEnteredString = orderLineTableObject.DateEntered.ToString();
                    orderLine.DateQtyChangedString = orderLineTableObject.DateQtyLastChanged.ToString();
                    supplierOrder.OrderLines.Add(orderLine);

                   
                }

              

                // Only add the SupplierOrder to the GrowerOrder if there are order lines or at least EventLogs.
                if (supplierOrder.OrderLines.Count != 0 || supplierOrder.GrowerOrderHistoryEvents.Count != 0)
                {
                    growerOrder.SupplierOrders.Add(supplierOrder);
                }

                DataObjectLibrary.Program programDataRow = supplierOrderTableObject.Program;
                foreach (Lookup tagRatioDataRow in programDataRow.TagRatioLookupList)
                {
                    var tagRatioLookup = new BusinessObjectsLibrary.Lookup(tagRatioDataRow);
                    supplierOrder.TagRatioList.Add(tagRatioLookup);
                    if (programDataRow.TagRatioDefault != null &&
                        tagRatioDataRow.Guid == programDataRow.TagRatioDefault.Guid)
                    {
                        supplierOrder.TagRatioDefault = tagRatioLookup;
                    }
                }

                foreach (Lookup shipMethodDataRow in programDataRow.ShownShipMethodList)
                {
                    var shipMethodLookup = new BusinessObjectsLibrary.Lookup(shipMethodDataRow);
                    supplierOrder.ShipMethodList.Add(shipMethodLookup);
                    if (programDataRow.ShipMethodDefault != null &&
                        shipMethodDataRow.Guid == programDataRow.ShipMethodDefault.Guid)
                    {
                        supplierOrder.ShipMethodDefault = shipMethodLookup;
                    }
                }
            }

            return growerOrder;
        }

        private void GetOrderHistory(IEnumerable<DataObjectLibrary.GrowerOrderHistory> growerOrdeHistoryTableObjectList, List<BusinessObjectsLibrary.GrowerOrderHistoryEvent> destinationList)
        {
           
            foreach (var growerOrderHistoryTableObject in growerOrdeHistoryTableObjectList)
            {
                GrowerOrderHistoryEvent theGrowerOrderHistoryEvent = new GrowerOrderHistoryEvent(growerOrderHistoryTableObject);

                destinationList.Add(theGrowerOrderHistoryEvent);
            }
        }

        public List<BusinessObjectsLibrary.GrowerOrderHistoryEvent> GetGrowerOrderHistory(Guid growerOrderGuid, bool includeExternalOnly)
        {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var dataService = new GrowerOrderDataService(status);
            var theEventList = new List<BusinessObjectsLibrary.GrowerOrderHistoryEvent>();
            var theEventList2 = new List<BusinessObjectsLibrary.GrowerOrderHistoryEvent>();
           
            var growerOrdeHistoryData = dataService.GetGrowerOrderHistoryData(growerOrderGuid);
            GetOrderHistory(growerOrdeHistoryData.GrowerOrderHistoryList, theEventList);

            if (includeExternalOnly == true)
            {
                foreach (var oEvent in theEventList)
                {
                    if (oEvent.IsInternal == false)
                    {
                        theEventList2.Add(oEvent);
                    }
                }
            }
            else
            {
                theEventList2 = theEventList;
            }

            return theEventList2;

        }
        public Availability UpdateCartQuantity(Guid userGuid, string userCode, string shipWeekCode, Guid productGuid,
                                               int quantity, decimal price = 0, bool getNewRowData = true,string sellerCode = "GFB")
        {
            sellerCode = "GFB";
            return UpdateCartQuantity(userGuid, userCode, shipWeekCode, productGuid, quantity, price: price, isPlacedOrder: false, getNewRowData: getNewRowData,sellerCode: sellerCode);
        }

        [Obsolete("You cannot update a placed order quantity without providing the OrderLineGuid, use another overload.")]
        public Availability UpdateOrderQuantity(Guid userGuid, string userCode, string shipWeekCode, Guid productGuid,
                                       int quantity)
        {
            throw new NotImplementedException("You cannot update a placed order quantity without providing the OrderLineGuid.");
            //return UpdateCartQuantity(userGuid, userCode, shipWeekCode, productGuid, quantity, isPlacedOrder: true);
        }

        private Availability UpdateCartQuantity(Guid userGuid, string userCode, string shipWeekCode, Guid productGuid, int quantity, bool isPlacedOrder, decimal price = 0,
                                               string orderTypeLookupCode = "Order", bool getNewRowData = true,string sellerCode = "GFB")
        {
            var status = new StatusObject(userGuid, true);
            var service = new GrowerOrderDataService(status);
            sellerCode = "GFB";
            AvailabilityData availabilityData = service.UpdateCartQuantity(userCode, shipWeekCode, productGuid, quantity, price: price, getNewRowData: getNewRowData,sellerCode: sellerCode);

            if (getNewRowData == true)
            {
                var availabilityService = new AvailabilityService(status);
                var availability = availabilityService.Availability(availabilityData, includePrices: false, includeCartData: true);
                return availability;

            }
            else
            {
                var availability = new Availability();
                return availability;
            }

           

        }

        public class OrderLineUpdateResult
        {
            public int Quantity { get; set; }
            public bool Success { get; set; }
            public string Message { get; set; }
        }



        public OrderLineUpdateResult OrderLineUpdate(Guid userGuid, Guid orderLineGuid, int quantity, decimal price, string supplierOrGrower,out Guid growerOrderGuid)
        {
            var orderGuid = new Guid();
            var newQuantity = 0;  
            var status  = new StatusObject(userGuid,true);
            var service  = new OrderLineUpdateServiceNew(status);
            var bRet = service.OrderLineUpdate(orderLineGuid, quantity, price, supplierOrGrower, out newQuantity, out orderGuid);
            var orderLineUpdateResult = new OrderLineUpdateResult();
            orderLineUpdateResult.Quantity = newQuantity;
            orderLineUpdateResult.Message = status.StatusMessage;
            growerOrderGuid = orderGuid;
            var growerOrderService = new GrowerOrderDataService(status);
            growerOrderService.RecalculateFeesbyGrowerOrderGuid(growerOrderGuid, true);
            return orderLineUpdateResult;
                       
        }
     
        private static OrderLine GetOrderLineIfNull(OrderLine orderLine, Guid orderLineGuid, SqlTransaction transaction)
        {
            if (orderLine == null)
            {
                RoundTripToDatabaseHasOccurredFORDEBUGGINGPURPOSESONLY = true;

                var orderLineTableService = new OrderLineTableService();
                orderLine = orderLineTableService.GetByGuid(orderLineGuid, transaction);
            }

            return orderLine;
        }

        public BusinessObjectsLibrary.OrderLine GetOrderLineBOByGuid(Guid orderLineGuid, SqlTransaction transaction)
        {
            var service = new DataServiceLibrary.OrderLineTableService();
            var orderLineTableObject = service.TryGetByGuid(orderLineGuid, transaction);
            BusinessObjectsLibrary.OrderLine theOrderLine = null;
            if (orderLineTableObject != null)
            {
                theOrderLine = new BusinessObjectsLibrary.OrderLine(orderLineTableObject);
            }
            return theOrderLine;


        }
        public OrderLine GetOrderLineByGuid(Guid orderLineGuid)
        {

                var orderLineTableService = new OrderLineTableService();
                OrderLine orderLine = orderLineTableService.GetByGuid(orderLineGuid);

            return orderLine;
        }

        public void SubstituteProductOnOrderLine(Guid userGuid, string userCode, Guid orderLineGuid, Guid substituteProductGuid)
        {
            OrderLineTableService orderLineService = new OrderLineTableService();

            var orderLine = orderLineService.GetByGuid(orderLineGuid);

            if (orderLine.ProductGuid == substituteProductGuid)
            {
                throw new ArgumentException("The substituted product is the same as the original.", "substituteProductGuid");
            }

            

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();

                var transaction = connection.BeginTransaction();

                var originalProduct = orderLine.Product;
                orderLine.ChangeTypeLookup = LookupTableValues.Code.ChangeType.SupplierSubstitute;
                orderLine.DateLastChanged = DateTime.Now;
                orderLine.ProductGuid = substituteProductGuid;

                // Clear out the price information; it's no longer relevent and can only cause trouble.
                // After a substitution, the prices must be set on the order again.
                orderLine.ActualPrice = 0;
                orderLine.ActualCost = 0;
                orderLine.ActualPriceUsedGuid = null;

                orderLine = orderLineService.Update(orderLine, transaction);

                string logText =
                    string.Format
                    (
                        "{0} substituted with {1}.",
                        originalProduct.ProductDescriptionCalculated,
                        orderLine.Product.ProductDescriptionCalculated
                    );

                //This is broken
                //PersonService personService = new PersonService();
                //var userPerson = personService.GetPersonFromUser(userGuid);

                //var growerOrderHistoryTableService = new GrowerOrderHistoryTableService();
                //var theEvent = new DataObjectLibrary.GrowerOrderHistory();
                //theEvent.PersonGuid = userPerson.PersonGuid;
                //theEvent.Comment = logText;
                //theEvent.GrowerOrderGuid = orderLine.SupplierOrder.GrowerOrderGuid;
                //theEvent.LogTypeLookup = LookupTableValues.Logging.LogType.Grower.OrderLineSubstitute;
                //theEvent.LogTypeLookupGuid = LookupTableValues.Logging.LogType.Grower.OrderLineSubstitute.Guid;
                //theEvent.EventDate = DateTime.Now;
                //var growerOrderHistory = growerOrderHistoryTableService.Insert
                //    (theEvent);

               

                transaction.Commit();

                connection.Close();

                //reprice
                var iLinesUpdated = this.UpdatePrice(userGuid,orderLine.SupplierOrder.GrowerOrderGuid, orderLineGuid, false);
            }


        }

        public int AddToCart(Guid userGuid, string userCode, string shipWeekCode, string programTypeCode,
                             string productFormCategoryCode)
        {
            //This is what changes the order staus to "in cart"
            var service = new OrderLineChangeStatusProcedure();
            int itemsAdded = service.AddToCart(userGuid, userCode, shipWeekCode, programTypeCode, productFormCategoryCode);

            var status = new StatusObject(userGuid, true);
            var growerOrderService = new GrowerOrderDataService(status);
            growerOrderService.ResetProgramCategoryType(shipWeekCode, programTypeCode, productFormCategoryCode);

            //this call actually does the fees for Grower Order
            var supplierOrderService = new SupplierOrderService(status);
            supplierOrderService.RecalculateFees(userGuid, shipWeekCode, programTypeCode, productFormCategoryCode);

            return itemsAdded;
        }
        public int AddToCartAllWeeks(Guid userGuid, string userCode, string programTypeCode,
                            string productFormCategoryCode)
        {
            // Get all AddToCartAllWeeks with Add to Cart items
            var status = new StatusObject(userGuid, true);
            //This is what changes the order staus to "in cart"
            var service = new OrderLineChangeStatusProcedure();
            var supplierOrderService = new SupplierOrderService(status);
            var growerOrderService = new GrowerOrderDataService(status);
            int itemsAdded = service.AddToCartAllWeeks(userGuid, userCode, programTypeCode, productFormCategoryCode);
           
            //for each week with add to cart items.....
            var totalService = new BusinessObjectServices.GrowerOrderService();
            var weeks = totalService.GrowerGetTotalsPerWeek(userGuid, productFormCategoryCode,programTypeCode );
            foreach (var week in weeks)
                {
                    if (week.Carted > 0)
                    {
                        
                        
                        growerOrderService.ResetProgramCategoryType(week.ShipWeekCode, programTypeCode, productFormCategoryCode);

                        ////this call actually does the fees for Grower Order
                        supplierOrderService.RecalculateFees(userGuid, week.ShipWeekCode, programTypeCode, productFormCategoryCode);
                    }

                }
        

            return itemsAdded;
           // return 0;
        }
        public int AddToOrder(Guid userGuid, string userCode, Guid growerOrderGuid, string shipWeekCode, string programTypeCode,string productFormCategoryCode)
        {
            var status = new StatusObject(userGuid, true);
            var service = new GrowerOrderDataService(status);
            int itemsAdded = service.AddToOrder(userCode, growerOrderGuid,shipWeekCode );

            var growerOrderService = new GrowerOrderDataService(status);
            growerOrderService.ResetProgramCategoryType(shipWeekCode, programTypeCode, productFormCategoryCode);

            var supplierOrderService = new SupplierOrderService(status);
            supplierOrderService.RecalculateFees(userGuid, shipWeekCode, programTypeCode, productFormCategoryCode);

            return itemsAdded;
        }
        public  GrowerOrder GetGrowerOrder(Guid userGuid,string shipWeekCode, string programTypeCode,
                             string productFormCategoryCode)
        {
            string sWeekCode = shipWeekCode;
            if (shipWeekCode.Substring(2, 1) == "|")
            { 
            //Need to flip the week and year
                sWeekCode = shipWeekCode.Substring(3, 4) + "|" + shipWeekCode.Substring(0, 2);
            }
           var status = new StatusObject(userGuid, true);
           var growerOrderDataService = new GrowerOrderDataService(status);
           Guid growerOrderGuid = growerOrderDataService.GrowerOrderGetGuid( sWeekCode, programTypeCode, productFormCategoryCode, false);
           GrowerOrder growerOrder = new GrowerOrder();
           if ( growerOrderGuid != Guid.Empty)
           {
               growerOrder = GetGrowerOrderDetail(growerOrderGuid, false);
           }

          
           return growerOrder;

          
        }
       
        public bool GetOrderTotals(Guid growerOrderGuid, out int CartedQty, out int UncartedQty)
        {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);

            var growerOrderDataService = new GrowerOrderDataService(status);
            var theTotals = growerOrderDataService.GrowerOrderGetTotals(growerOrderGuid);
            CartedQty = 0;
            UncartedQty = 0;
            foreach (var oTotal in theTotals)
            {
                if (oTotal.Status == "Pending")
                { 
                    CartedQty = oTotal.Qty;
                }
                if (oTotal.Status == "PreCart")
                { 
                    UncartedQty = oTotal.Qty;
                }

            }
            
            return true;

        }
        public List<GrowerWeekSummary> GrowerGetTotalsPerWeek(Guid userGuid, string productFormCategoryCode, string programTypeCode)
        {
            var status = new StatusObject(userGuid, true);
            var list = new List<GrowerWeekSummary>();
            var growerOrderDataService = new GrowerOrderDataService(status);
            list = growerOrderDataService.GrowerGetTotalsPerWeek(userGuid, productFormCategoryCode, programTypeCode);
       
          

            return list;

        }

        public bool GrowerGetTotals(Guid userGuid,string productFormCategoryCode,string programTypeCode , out int CartedQty, out int UncartedQty)
        {
            var status = new StatusObject(userGuid, true);

            var growerOrderDataService = new GrowerOrderDataService(status);
            var theTotals = growerOrderDataService.GrowerGetTotals(userGuid, productFormCategoryCode, programTypeCode);
            CartedQty = 0;
            UncartedQty = 0;
            foreach (var oTotal in theTotals)
            {
                if (oTotal.Status == "Pending")
                {
                    CartedQty = oTotal.Qty;
                }
                if (oTotal.Status == "PreCart")
                {
                    UncartedQty = oTotal.Qty;
                }

            }

            return true;

        }

        public bool UpdateGrowerOrder(Guid userGuid, Guid growerOrderGuid, string customerPoNo, string orderDescription, Guid shipToAddressGuid, string paymentTypeLookupCode, Guid? growerCreditCardGuid, string promotionalCode)
        {
            var status = new StatusObject(userGuid, true);
            var service = new GrowerOrderDataService(status);

            bool success = service.UpdateOrder(userGuid, growerOrderGuid, customerPoNo, orderDescription, shipToAddressGuid, paymentTypeLookupCode, growerCreditCardGuid,promotionalCode);

            return success;
        }

        public bool UpdateSupplierOrder(Guid userGuid, Guid supplierOrderGuid, string tagRatioCode, string shipMethodCode)
        {
            var status = new StatusObject(userGuid, true);
            var service = new SupplierOrderService(status);

            bool success = service.UpdateOrder(supplierOrderGuid, tagRatioCode, shipMethodCode);

            return success;
        }

        public OrderLineChangeStatusProcedure.ReturnData PlaceOrder(Guid userGuid, string userCode, Guid growerOrderGuid, string emailText = "", string emailSubject = "", bool updatePricesOnNewThread = true)
        {
            var service = new OrderLineChangeStatusProcedure();

            OrderLineChangeStatusProcedure.ReturnData returnData = service.PlaceOrder(userGuid, userCode, growerOrderGuid);

               return returnData;
        }

        public OrderLineChangeStatusProcedure.ReturnData SetOrderLineStatus
            (
                SqlTransaction transaction,
                Guid userGuid, 
                string userCode,
                Guid orderLineGuid, 
                int quantity,
                bool isAdminRequest
            )
        {
            bool isCancel = quantity == 0;

            Lookup toStatusLookup;
            
            var orderLineStatuses = LookupTableValues.Code.OrderLineStatus;
            var supplierOrGrower = "Grower";
            if(isAdminRequest){
                supplierOrGrower = "Supplier";
            }
            if (isCancel)
            {
                if (isAdminRequest)
                {
                    toStatusLookup = orderLineStatuses.SupplierCancelled;
                }
                else
                {
                    toStatusLookup = orderLineStatuses.GrowerCancelled;
                }
            }
            else
            {
                if (isAdminRequest)
                {
                    // this now handled in SPROC
                    toStatusLookup = orderLineStatuses.SupplierEdit;
                }
                else
                {
                   // this now handled in SPROC
                    toStatusLookup = orderLineStatuses.GrowerEdit;
                }
            }
            if (toStatusLookup != orderLineStatuses.GrowerEdit && toStatusLookup != orderLineStatuses.SupplierEdit)
            {

                var service = new OrderLineChangeStatusProcedure();

                var procedureReturnData = service.ChangeOrderLineStatusesInTransaction
                        (
                            transaction,
                            userGuid,
                            userCode,
                            growerOrderGuid: Guid.Empty,
                            supplierOrderGuid: Guid.Empty,
                            orderLineGuid: orderLineGuid,
                            fromStatusLookup: null,
                            toStatusLookup: toStatusLookup,
                            supplierOrGrower: supplierOrGrower
                        );
                var returnData = new OrderLineChangeStatusProcedure.ReturnData
                {
                    OrderLinesChanged = procedureReturnData.OrderLinesChanged,
                    OrderLinesDeleted = procedureReturnData.OrderLinesDeleted
                };

                return returnData;
            }
            else
            {
                var returnData = new OrderLineChangeStatusProcedure.ReturnData
                {
                    OrderLinesChanged = 1,
                    OrderLinesDeleted = 0
                };
                return returnData;
            }
        }

        public int UpdatePrices(Guid userGuid,Guid growerOrderGuid, bool runOnNewThread = false)
        {
            return OrderLinePriceUpdateService.UpdatePrices(userGuid,growerOrderGuid, runOnNewThread);
        }
        public int UpdatePrice(Guid userGuid, Guid growerOrderGuid, Guid orderLineGuid, bool runOnNewThread = false)
        {
            return OrderLinePriceUpdateService.UpdatePrice(userGuid,growerOrderGuid, orderLineGuid);
        }
        

        public class ChangeStatusReturnValues
        {
            public string Message { get; set; }
            public int OrderLinesChanged { get; set; }
            public int OrderLinesDeleted { get; set; }
        }

        public ChangeStatusReturnValues ChangeOrderLineStatuses(Guid userGuid, string userCode, string orderTransitionTypeLookupCode, Guid growerOrderGuid,
                                     Guid supplierOrderGuid, Guid orderLineGuid)
        {
            string message = null;

            DataObjectLibrary.Lookup toStatusLookup = null;
            DataObjectLibrary.Lookup deleteStatusLookup = null;
            DataObjectLibrary.Lookup logTypeLookup = null;
            string logMessage = "";

            var transitionTypeLookups = LookupTableValues.Code.OrderTransitionType;
            var orderLineStatusLookups = LookupTableValues.Code.OrderLineStatus;
            var logTypeLookups = LookupTableValues.Logging.LogType;

            var fromStatusLookupList = new List<Lookup>();

            if (orderTransitionTypeLookupCode == transitionTypeLookups.PlaceOrder.Code)
            {
                if (supplierOrderGuid == Guid.Empty)
                    message = GetGuidNotAllowedMessage("SupplierOrderGuid", orderTransitionTypeLookupCode);

                //fromStatusLookup = orderLineStatusLookups.Pending;
                fromStatusLookupList.Add (orderLineStatusLookups.Pending);
                toStatusLookup = orderLineStatusLookups.Ordered;
                deleteStatusLookup = orderLineStatusLookups.PreCart;

                logTypeLookup = logTypeLookups.Grower.GrowerOrderPlace;
                logMessage = "Order placed.";
            }
            else if (orderTransitionTypeLookupCode == transitionTypeLookups.GrowerNotified.Code)
            {
                if (supplierOrderGuid == Guid.Empty)
                    message = GetGuidNotAllowedMessage("SupplierOrderGuid", orderTransitionTypeLookupCode);

               // fromStatusLookup = orderLineStatusLookups.Ordered;
                fromStatusLookupList.Add (orderLineStatusLookups.Ordered);
                toStatusLookup = orderLineStatusLookups.GrowerNotified;

                logTypeLookup = logTypeLookups.RowUpdate;
                logMessage = "Grower notified.";                
            }
            else if (orderTransitionTypeLookupCode == transitionTypeLookups.SupplierNotified.Code)
            {
                if (growerOrderGuid == Guid.Empty)
                    message = GetGuidNotAllowedMessage("GrowerOrderGuid", orderTransitionTypeLookupCode);

                //fromStatusLookup = orderLineStatusLookups.GrowerNotified;
                fromStatusLookupList.Add(orderLineStatusLookups.GrowerNotified);
                fromStatusLookupList.Add(orderLineStatusLookups.GrowerEdit);
                fromStatusLookupList.Add(orderLineStatusLookups.Ordered);
                toStatusLookup = orderLineStatusLookups.SupplierNotified;

                logTypeLookup = logTypeLookups.RowUpdate;
                logMessage = "Supplier notified.";                                
            }
            else if (orderTransitionTypeLookupCode == transitionTypeLookups.SupplierConfirmed.Code)
            {
                if (growerOrderGuid == Guid.Empty)
                    message = GetGuidNotAllowedMessage("GrowerOrderGuid", orderTransitionTypeLookupCode);

               // fromStatusLookup = orderLineStatusLookups.SupplierNotified;
                fromStatusLookupList.Add(orderLineStatusLookups.SupplierNotified);
                toStatusLookup = orderLineStatusLookups.SupplierConfirmed;

                logTypeLookup = logTypeLookups.RowUpdate;
                logMessage = "Supplier confirmed.";
            }
            else if (orderTransitionTypeLookupCode == transitionTypeLookups.GrowerNotifiedAfterSupplierConfirmed.Code)
            {
                if (growerOrderGuid == Guid.Empty)
                    message = GetGuidNotAllowedMessage("GrowerOrderGuid", orderTransitionTypeLookupCode);

             //   fromStatusLookup = orderLineStatusLookups.SupplierConfirmed;
                fromStatusLookupList.Add(orderLineStatusLookups.SupplierNotified);
                fromStatusLookupList.Add(orderLineStatusLookups.SupplierConfirmed);
                fromStatusLookupList.Add(orderLineStatusLookups.SupplierEdit);
                fromStatusLookupList.Add(orderLineStatusLookups.SupplierAdd);
                toStatusLookup = orderLineStatusLookups.GrowerNotifiedAfterSupplierConfirmed;

                logTypeLookup = logTypeLookups.RowUpdate;
                logMessage = "Grower notified after supplier confirmed.";
            }
            else if (orderTransitionTypeLookupCode == transitionTypeLookups.Shipped.Code)
            {
                if (growerOrderGuid == Guid.Empty)
                    message = GetGuidNotAllowedMessage("GrowerOrderGuid", orderTransitionTypeLookupCode);

              //  fromStatusLookup = orderLineStatusLookups.GrowerNotifiedAfterSupplierConfirmed;
                fromStatusLookupList.Add(orderLineStatusLookups.GrowerNotifiedAfterSupplierConfirmed);
                toStatusLookup = orderLineStatusLookups.Shipped;

                logTypeLookup = logTypeLookups.RowUpdate;
                logMessage = "Shipped.";
            }

            var orderLineChangeStatusProcedure = new OrderLineChangeStatusProcedure();
            var procedureReturnData = 
                orderLineChangeStatusProcedure.ChangeOrderLineStatuses
                (
                    userGuid, 
                    userCode, 
                    growerOrderGuid,
                    supplierOrderGuid, 
                    orderLineGuid,
                    fromStatusLookupList, 
                    toStatusLookup,
                    deleteStatusLookup, 
                    logTypeLookup,
                    logMessage
                );

            if (orderTransitionTypeLookupCode == LookupTableValues.Code.OrderTransitionType.PlaceOrder.Code)
            {
                if (procedureReturnData.OrderLinesChanged == 0)
                {
                    message = "No products have been ordered.";
                }
                else
                {
                    UpdatePrices(userGuid,growerOrderGuid, true);
                    UpdateAvailability(growerOrderGuid);                    
                }
            }

                     
            return new ChangeStatusReturnValues()
                {
                    Message = message,
                    OrderLinesChanged = procedureReturnData.OrderLinesChanged,
                    OrderLinesDeleted = procedureReturnData.OrderLinesDeleted
                };
        }

        private void UpdateAvailability(Guid growerOrderGuid)
        {
            var growerOrder = GetGrowerOrderDetail(growerOrderGuid, includePriceData: false);

            foreach (var supplierOrder in growerOrder.SupplierOrders)
            {
                ReportedAvailabilityService reportedAvailabilityService = new ReportedAvailabilityService();

                foreach (var orderLine in supplierOrder.OrderLines)
                {
                    Guid shipWeekGuid = growerOrder.ShipWeekGuid;
                    Guid productGuid = orderLine.Product.Guid;
                    Guid productFormGuid = orderLine.Product.ProductFormGuid;
                    int updateQty = orderLine.QuantityOrdered;

                    DataObjectLibrary.ReportedAvailability availabilityUpdated
                        = reportedAvailabilityService.UpdateSalesSinceDateReported(productGuid, productFormGuid, shipWeekGuid, updateQty);
                }
            }            
        }

        private string GetGuidNotAllowedMessage(string guidTypeName, string transactionTypeLookupCode)
        {
            return string.Format("{0} is not allowed for transaction type {1}", guidTypeName, transactionTypeLookupCode);
        }

        private Lookup GetCurrentGrowerOrderStatus(Guid growerOrderGuid)
        {
            var growerOrderTableService = new GrowerOrderTableService();

            var growerOrder = growerOrderTableService.GetByGuid(growerOrderGuid);

            return growerOrder.GrowerOrderStatusLookup;
        }

        private Lookup GetCurrentSupplierOrderStatus(Guid supplierOrderGuid)
        {
            var supplierOrderTableService = new SupplierOrderTableService();

            var supplierOrder = supplierOrderTableService.GetByGuid(supplierOrderGuid);

            return supplierOrder.SupplierOrderStatusLookup;
        }

        private Lookup GetCurrentOrderLineStatus(Guid orderLineGuid)
        {
            var orderLineTableService = new OrderLineTableService();

            var orderLine = orderLineTableService.GetByGuid(orderLineGuid);

            return orderLine.OrderLineStatusLookup;
        }

        [Obsolete("Use ChangeStatus() instead.")]
        public OrderLineChangeStatusProcedure.ReturnData GrowerNotified(Guid userGuid, Guid growerOrderGuid, Guid orderLineGuid)
        {
            var service = new OrderLineChangeStatusProcedure();

            OrderLineChangeStatusProcedure.ReturnData returnData = service.GrowerNotified(userGuid, growerOrderGuid, orderLineGuid);

            return returnData;
        }

        [Obsolete("Use ChangeStatus() instead.")]
        public OrderLineChangeStatusProcedure.ReturnData SupplierNotified(Guid userGuid, Guid supplierOrderGuid, Guid orderLineGuid)
        {
            var service = new OrderLineChangeStatusProcedure();

            OrderLineChangeStatusProcedure.ReturnData returnData = service.SupplierNotified(userGuid, supplierOrderGuid, orderLineGuid);

            return returnData;
        }

        [Obsolete("Use ChangeStatus() instead.")]
        public OrderLineChangeStatusProcedure.ReturnData SupplierConfirmed(Guid userGuid, Guid supplierOrderGuid, Guid orderLineGuid)
        {
            var service = new OrderLineChangeStatusProcedure();

            OrderLineChangeStatusProcedure.ReturnData returnData = service.SupplierConfirmed(userGuid, supplierOrderGuid, orderLineGuid);

            return returnData;
        }

        [Obsolete("Use ChangeStatus() instead.")]
        public OrderLineChangeStatusProcedure.ReturnData GrowerNotified(Guid userGuid, Guid growerOrderGuid, Guid supplierOrderGuid, Guid orderLineGuid)
        {
            var service = new OrderLineChangeStatusProcedure();

            OrderLineChangeStatusProcedure.ReturnData returnData = service.GrowerNotifiedAfterSupplierConfirmed(userGuid, growerOrderGuid, supplierOrderGuid, orderLineGuid);

            return returnData;
        }

        [Obsolete("Use ChangeStatus() instead.")]
        public OrderLineChangeStatusProcedure.ReturnData Shipped(Guid userGuid, Guid growerOrderGuid, Guid supplierOrderGuid, Guid orderLineGuid)

        {
            var service = new OrderLineChangeStatusProcedure();

            OrderLineChangeStatusProcedure.ReturnData returnData = service.Shipped(userGuid, growerOrderGuid, supplierOrderGuid, orderLineGuid);

            return returnData;
        }

        [Obsolete("Use ChangeStatus() instead.")]
        public OrderLineChangeStatusProcedure.ReturnData DeleteCartOrder(Guid userGuid, string userCode, Guid growerOrderGuid)
        {
            var service = new OrderLineChangeStatusProcedure();

            return service.DeleteCartOrder(userGuid, userCode, growerOrderGuid);
        }

        public OrderLineChangeStatusProcedure.ReturnData DeletePlacedOrderFORTESTINGPURPOSESONLY(Guid userGuid, string userCode,
                                                                           GrowerOrder growerOrder)
        {
            BusinessObjectsLibrary.Lookup minStatus = null;
            BusinessObjectsLibrary.Lookup maxStatus = null;

            if (growerOrder.SupplierOrders.Count == 0)
            {
                throw new ApplicationException("This GrowerOrder has no supplier orders. Are you sure it's a fully populated GrowerOrder object?");
            }

            foreach (var supplierOrder in growerOrder.SupplierOrders)
            {
                if (supplierOrder.OrderLines.Count == 0)
                {
                    throw new ApplicationException("A SupplierOrder on the GrowerOrder has no OrderLines. Are you sure it's a fully populated GrowerOrder object?");
                }

                foreach (var orderLine in supplierOrder.OrderLines)
                {
                    if (minStatus == null || orderLine.OrderLineStatus.SortSequence < minStatus.SortSequence)
                    {
                        minStatus = orderLine.OrderLineStatus;
                    }

                    if (maxStatus == null || orderLine.OrderLineStatus.SortSequence > maxStatus.SortSequence)
                    {
                        maxStatus = orderLine.OrderLineStatus;
                    }
                }
            }

            if (minStatus == null || maxStatus == null)
            {
                throw new ApplicationException(string.Format("The statuses on the GrowerOrder's SupplierOrder's OrderLines are null."));
            }

            if (minStatus != maxStatus)
            {
                throw new ApplicationException(
                    string.Format(
                        "To delete a GrowerOrder (rather than cancel it), all of the GrowerOrder's OrderLines' statuses need to be the same. In this case, the statuses are different. The minimum value is {0} and the maximum value is {1}.",
                        minStatus.Code, maxStatus.Code));
            }

            if (minStatus.Guid != LookupTableValues.Code.OrderLineStatus.Ordered.Guid && minStatus.Guid != LookupTableValues.Code.OrderLineStatus.GrowerNotified.Guid)
            {
                throw new ApplicationException(string.Format("To delete a GrowerOrder, (rather than cancel it), all of the GrowerOrder's OrderLines' statuses must be \"Ordered\" (or GrowerNotified). In this case, they are {0}.", minStatus.Code));
            }

            var orderLineChangeStatusProcedureService = new OrderLineChangeStatusProcedure();

            if (minStatus.Guid == LookupTableValues.Code.OrderLineStatus.GrowerNotified.Guid)
            {
                orderLineChangeStatusProcedureService.UnnotifyGrowerFORTESTINGPURPOSESONLY(userGuid, userCode, growerOrder.OrderGuid);
            }

            return orderLineChangeStatusProcedureService.DeletePlacedOrderFORTESTINGPURPOSESONLY(userGuid, userCode, growerOrder.OrderGuid);
        }

        public void CancelPlacedOrder(Guid userGuid, string userCode, Guid growerOrderGuid)
        {
            var growerOrderTableService = new GrowerOrderTableService();

            var growerOrder = growerOrderTableService.GetByGuid(growerOrderGuid);

            //TODO: Improve on this business logic and work out the mechanism for returning failure data to the caller. 
            if (growerOrder.GrowerOrderStatusLookup.SortSequence <
                LookupTableValues.Code.GrowerOrderStatus.AwaitingCreditApproval.SortSequence ||
                growerOrder.GrowerOrderStatusLookup.SortSequence >
                LookupTableValues.Code.GrowerOrderStatus.CreditApproved.SortSequence)
            {
                throw new ApplicationException("This order cannot be cancelled.");
            }

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();

                var transaction = connection.BeginTransaction();

                growerOrder.GrowerOrderStatusLookupGuid = LookupTableValues.Code.GrowerOrderStatus.Cancelled.Guid;
                growerOrder = growerOrderTableService.Update(growerOrder, transaction);

                //This is broken
                //PersonService personService = new PersonService();
                //var userPerson = personService.GetPersonFromUser(userGuid);

                //var growerOrderHistoryTableService = new GrowerOrderHistoryTableService();
                //var theEvent = new DataObjectLibrary.GrowerOrderHistory();
                //theEvent.PersonGuid = userPerson.PersonGuid;
                //theEvent.Comment = "Cancelled.";
                //theEvent.GrowerOrderGuid = growerOrderGuid;
                //theEvent.LogTypeLookup = LookupTableValues.Logging.LogType.Grower.GrowerOrderCancel;
                //theEvent.LogTypeLookupGuid = LookupTableValues.Logging.LogType.Grower.GrowerOrderCancel.Guid;
                //theEvent.EventDate = DateTime.Now;
                //var growerOrderHistory = growerOrderHistoryTableService.Insert
                //    (theEvent);

             

                transaction.Commit();
            }
        }

        public bool UpdateCustomerPoNoOnOrder(Guid growerOrderGuid, string poNo)
        {
            //value = value ?? "";

            var status = new StatusObject(new Guid(), true);
            poNo = poNo ?? "";
            var service = new GrowerOrderDataService(status);
            var bRet = service.UpdateCustomerPoNoOnOrder(growerOrderGuid, poNo);
            return bRet;
            //var growerOrderTableObject = UpdateGrowerOrderField(growerOrderGuid, "CustomerPoNo", value);
            //return growerOrderTableObject != null && growerOrderTableObject.CustomerPoNo == value;
        }

        public bool UpdatePromotionalCodeOnOrder(Guid userGuid,Guid growerOrderGuid, string promoCode)
        {
            var status = new StatusObject(userGuid, true);
            promoCode = promoCode ?? "";
            var service = new GrowerOrderDataService(status);
            var bRet = service.UpdatePromotionalCodeOnOrder(growerOrderGuid, promoCode);

            //var growerOrderTableObject = UpdateGrowerOrderField(growerOrderGuid, "PromotionalCode", value);
            //return growerOrderTableObject != null && growerOrderTableObject.PromotionalCode == value;

            var growerOrderService = new GrowerOrderDataService(status);
            growerOrderService.RecalculateFeesbyGrowerOrderGuid(growerOrderGuid,false);


           
            return bRet;
        }

        public bool UpdateDescriptionOnOrder(Guid growerOrderGuid, string description)
        {
            description = description ?? "";

            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var service = new GrowerOrderDataService(status);
            var bRet = service.UpdateDescriptionOnOrder(growerOrderGuid, description);
            return bRet;
            
            //var growerOrderTableObject = UpdateGrowerOrderField(growerOrderGuid, "Description", value);
            //return growerOrderTableObject != null && growerOrderTableObject.Description == value;
        }

        public bool UpdateAddressOnOrder(Guid growerOrderGuid, Guid addressGuid)
        {
            
            
            var userGuid = new Guid();
            var status = new StatusObject(userGuid,true);
            var service = new GrowerOrderDataService(status);
            var bRet = service.UpdateAddressOnOrder(growerOrderGuid, addressGuid);
            return bRet;

                
            //var growerOrderTableObject = UpdateGrowerOrderField(growerOrderGuid, "ShipToAddressGuid", value);
            //return growerOrderTableObject != null && growerOrderTableObject.ShipToAddressGuid == value;
        }

        public bool UpdatePaymentTypeOnOrder(Guid growerOrderGuid, string paymentType)
        {
            //var lookupService = LookupTableService.SingletonInstance;
            //var paymentTypeLookup = lookupService.GetByCode(LookupTableValues.Code.PaymentType.LookupValue, paymentType);

            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var service = new GrowerOrderDataService(status);
            var bRet = service.UpdatePaymentTypeOnOrder(growerOrderGuid, paymentType);
            return bRet;


            //var growerOrderTableObject = UpdateGrowerOrderField(growerOrderGuid, "PaymentTypeLookupGuid", paymentTypeLookup.Guid);
            //return growerOrderTableObject != null && growerOrderTableObject.PaymentTypeLookupGuid == paymentTypeLookup.Guid;
        }

        public bool UpdateCardOnOrder(Guid growerOrderGuid, Guid value)
        {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid,true);
            var service = new GrowerOrderDataService(status);
            var bRet = service.UpdateCardOnOrder(growerOrderGuid, value);
            return bRet;
            //var growerOrderTableObject = UpdateGrowerOrderField(growerOrderGuid, "GrowerCreditCardGuid", value);
            //return growerOrderTableObject != null && growerOrderTableObject.GrowerCreditCardGuid == value;
        }

        private DataObjectLibrary.GrowerOrder UpdateGrowerOrderField(Guid growerOrderGuid, string fieldName, object newValue)
        {
            var service = new DataServiceLibrary.GrowerOrderTableService();
            return service.UpdateField(growerOrderGuid, fieldName, newValue);
        }

        public bool UpdateTagRatioLookupCode(Guid userGuid,Guid supplierOrderGuid, string value)
        {
            var lookupService = LookupTableService.SingletonInstance;
            var tagRatioLookup = lookupService.GetByCode(LookupTableValues.Code.TagRatio.LookupValue, value);

            var supplierOrderTableObject = UpdateSupplierOrderField(supplierOrderGuid, "TagRatioLookupGuid", tagRatioLookup.Guid);
            
            var status = new StatusObject(userGuid, true);
            var supplierOrderService = new SupplierOrderService(status);
            supplierOrderService.RecalculateFeesbySupplierOrderGuid( supplierOrderGuid);
      
            return supplierOrderTableObject != null && supplierOrderTableObject.TagRatioLookupGuid == tagRatioLookup.Guid;
        }

        public bool UpdateShipmentMethodLookupCode(Guid userGuid,Guid supplierOrderGuid, string value)
        {
            var lookupService = LookupTableService.SingletonInstance;
            var shipMethod = lookupService.GetByCode(LookupTableValues.Code.ShipMethod.LookupValue, value);

            var supplierOrderTableObject = UpdateSupplierOrderField(supplierOrderGuid, "ShipMethodLookupGuid", shipMethod.Guid);
            var growerOrderGuid = supplierOrderTableObject.GrowerOrderGuid; 
            
           // var supplierOrderService = new SupplierOrderUpdateService();
           // supplierOrderService.RecalculateFeesbySupplierOrderGuid(new Guid(), supplierOrderGuid);
            var status = new StatusObject(userGuid, true);
            var growerOrderService = new GrowerOrderDataService(status);
            growerOrderService.RecalculateFeesbyGrowerOrderGuid( growerOrderGuid, true);
      
            
            return supplierOrderTableObject != null && supplierOrderTableObject.ShipMethodLookupGuid == shipMethod.Guid;
        }

        private DataObjectLibrary.SupplierOrder UpdateSupplierOrderField(Guid supplierOrderGuid, string updateTypeCode, object newValue)
        {
            var service = new SupplierOrderTableService();
            return service.UpdateField(supplierOrderGuid, updateTypeCode, newValue);
        }

      


        public BusinessObjectsLibrary.SupplierOrderFee AddSupplierFee(Guid supplierOrderGuid, String supplierOrderFeeTypeCode, Decimal amount, String feeDescription, string supplierOrderFeeStatusCode)
        {
            var lookupService = new DataServiceLibrary.LookupTableService();
            //var feeTypeLookup = lookupService.GetByCode(LookupTableValues.Code.SupplierOrderFeeType.LookupValue, supplierOrderFeeTypeCode);
            var feeTypeLookup = lookupService.GetByCode(LookupTableValues.Code.SupplierOrderFeeType.LookupValue, supplierOrderFeeTypeCode);
            var feeStatusLookup = lookupService.GetByCode(LookupTableValues.Code.SupplierOrderFeeStatus.LookupValue, supplierOrderFeeStatusCode);

            var supplierOrderFee = new DataObjectLibrary.SupplierOrderFee();
            supplierOrderFee.SupplierOrderGuid = supplierOrderGuid;
            supplierOrderFee.FeeTypeLookupGuid = feeTypeLookup.Guid;
            supplierOrderFee.FeeStatXLookupGuid = feeStatusLookup.Guid;
            supplierOrderFee.Amount = amount;
            supplierOrderFee.FeeDescription = feeDescription;
            supplierOrderFee.IsManual = true;
            var supplierOrderFeeService = new SupplierOrderFeeTableService();
            supplierOrderFee = supplierOrderFeeService.Insert(supplierOrderFee);

            return new BusinessObjectsLibrary.SupplierOrderFee(supplierOrderFee);
        }

        public BusinessObjectsLibrary.SupplierOrderFee UpdateSupplierFee(Guid supplierFeeGuid, Decimal amount)
        {
            var supplierOrderFeeService = new SupplierOrderFeeTableService();

            var supplierOrderFeeDataObject = supplierOrderFeeService.UpdateField(supplierFeeGuid, "Amount", amount);

            return new BusinessObjectsLibrary.SupplierOrderFee(supplierOrderFeeDataObject);
        }

        public void FullUpdateSupplierFee(Guid supplierFeeGuid, decimal amount, string FeeDescription, 
                    string supplierOrderFeeStatusCode, string supplierOrderFeeTypeCode)
        {
            var lookupService = new DataServiceLibrary.LookupTableService();
            var feeTypeLookup = lookupService.GetByCode(LookupTableValues.Code.SupplierOrderFeeType.LookupValue, supplierOrderFeeTypeCode);
            var feeStatusLookup = lookupService.GetByCode(LookupTableValues.Code.SupplierOrderFeeStatus.LookupValue, supplierOrderFeeStatusCode);

            var supplierOrderFeeService = new SupplierOrderFeeTableService();
            DataObjectLibrary.SupplierOrderFee fee = supplierOrderFeeService.GetByGuid(supplierFeeGuid);

            fee.Amount = amount;
            fee.FeeDescription = FeeDescription;
            fee.FeeStatXLookupGuid = feeStatusLookup.Guid;
            fee.FeeTypeLookupGuid = feeTypeLookup.Guid;

            supplierOrderFeeService.Update(fee);
        }

        public bool DeleteSupplierFee(Guid supplierFeeGuid)
        {
            var supplierOrderFeeService = new SupplierOrderFeeTableService();

            //TODO: Add a DeleteByGuid method to the DataServices so we don't have to create an object to do a delete.
            supplierOrderFeeService.Delete(new DataObjectLibrary.SupplierOrderFee() { Guid = supplierFeeGuid});

            return true;
        }

        public List<BusinessObjectsLibrary.SupplierOrderFee> GetSupplierOrderFees(Guid supplierOrderGuid)
        {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var supplierOrderGetFeesProcedure = new SupplierOrderService(status);
            var theFees = supplierOrderGetFeesProcedure.GetFees(supplierOrderGuid);
            var theFeeList = new List<BusinessObjectsLibrary.SupplierOrderFee>();
            foreach (DataObjectLibrary.SupplierOrderFee theFee in theFees)
            {
                var newFee = new BusinessObjectsLibrary.SupplierOrderFee(theFee);
                
                
                theFeeList.Add(newFee);
            }
           
            return theFeeList;
       
        }

        public List<BusinessObjectsLibrary.GrowerOrderFee> GetGrowerOrderFees(Guid userGuid, Guid growerOrderGuid)
        {
            
            var status = new StatusObject(userGuid, true);
            var growerOrderGetFeesProcedure = new GrowerOrderFeeDataService(status);
            var theFees = growerOrderGetFeesProcedure.GrowerOrderGetFees(growerOrderGuid);
            var theFeeList = new List<BusinessObjectsLibrary.GrowerOrderFee>();
            foreach (DataObjectLibrary.GrowerOrderFee theFee in theFees)
            {
                var newFee = new BusinessObjectsLibrary.GrowerOrderFee(theFee);
              


                theFeeList.Add(newFee);
            }

            return theFeeList;

        }

   
       
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataServiceLibrary;
using BusinessObjectsLibrary;

namespace BusinessObjectServices
{
    public class PageHitService: BusinessServiceBase
    {

        public PageHitService(StatusObject status)
            : base(status)
        { }
    
        public bool AddPageHit(PageHit pagehit)
        {

            var service = new PageHitDataService(Status);
            var bRet = service.AddPageHit(pagehit);

            return bRet;
        }


    }
}

﻿
using System.Web;
using System.Linq;
using System;
using System.Text;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using BusinessObjectsLibrary;
using HiQPdf;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;



namespace BusinessObjectServices
{
    public class PDFService
    {
        const string CONNECTION_STRING_NAME = "StorageConnectionString";
        const string CONTAINER_NAME = "confirmationpdf";
        const string CONTAINER_PATH = "https://epsstorage.blob.core.windows.net";
      //  public MemoryStream GetOrderPDF(Guid orderGuid)
        public string GetOrderPDF(Guid orderGuid)
        {

            var service = new GrowerOrderService();
            var growerOrder = service.GetGrowerOrderDetail(orderGuid, false,  false);
            var pdfStream = GetOrderConfirmationPdfStream(growerOrder);

            //Upload to the Azure cloud storage
            string thepath = ConfigurationManager.ConnectionStrings[CONNECTION_STRING_NAME].ConnectionString;
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(thepath);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(CONTAINER_NAME);
            var fileName = growerOrder.OrderNo + "_" + orderGuid + ".pdf";
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
            blockBlob.UploadFromStream(pdfStream);

            var url = CONTAINER_PATH + "/" + CONTAINER_NAME + "/" + fileName;
        

            return url;
        }
        private MemoryStream GetOrderConfirmationPdfStream(GrowerOrder growerOrder)
        {
            var model = GetConfirmationPdfModelFromGrowerOrder(growerOrder);

            var htmlBuilder = new HtmlOrderConfirmationBuilder();
            var html = htmlBuilder.Build(model, HttpContext.Current.Server.MapPath("~"));

           
            var result = HtmlToPdf(html, "");

            return new MemoryStream(result);
        }
        private OrderConfirmationModel GetConfirmationPdfModelFromGrowerOrder(GrowerOrder growerOrder)
        {
            var paymentMethod = growerOrder.PaymentType.Name;
            var shipToAddress = "to do";
            //var shipToAddress = growerOrder. .ShipToAddresses == null ? null :
            //    growerOrder.ShipToAddresses.FirstOrDefault(x => x.IsSelected);

            var result = new OrderConfirmationModel
            {
                OrderNumber = growerOrder.OrderNo,
                PurchaseOrderNumber = growerOrder.CustomerPoNo,
                CustomerName = growerOrder.Grower.GrowerName,
                OrderDescription = growerOrder.OrderDescription,
                WeekString = string.Format("{0}/{1}", growerOrder.ShipWeek.ShipWeekString.Substring(4, 2), growerOrder.ShipWeek.ShipWeekString.Substring(0, 4)),
                PaymentMethod = paymentMethod,
                ShipToName = "To Do",
                ShipToAddress = shipToAddress == null ? null
                    : new Address("To Do", "To Do", "TO Do", "TD", "To do")
            };

            foreach (var supplierOrder in growerOrder.SupplierOrders)
            {
                var supplierOrderNo = supplierOrder.SupplierOrderNo;
                if (supplierOrderNo == "unassigned")
                {
                    supplierOrderNo = "";
                }
                var tagRatio = supplierOrder.TagRatio.Name;
                var shipMethod = supplierOrder.ShipMethod.Name;
                var supplierTotal = 0;
                foreach (OrderLine orderLine in supplierOrder.OrderLines)
                {
                    if (orderLine.QuantityShipped > 0)
                    {
                        supplierTotal += orderLine.QuantityShipped;
                    }
                    else
                    {
                        supplierTotal += orderLine.QuantityOrdered;
                    }
                }
                var supplierSection = new SupplierSectionModel
                {
                    SupplierOrderNo = supplierOrderNo,
                    TagInfo = tagRatio,
                    ShipMethodName = shipMethod,
                    SupplierName = supplierOrder.Supplier.Name,
                    TotalCuttings = supplierTotal
                };

                foreach (OrderLine orderLine in supplierOrder.OrderLines)
                {
                    var qty = orderLine.QuantityOrdered;
                    if (orderLine.QuantityShipped > 0)
                    {
                        qty = orderLine.QuantityShipped;
                    }

                    var varietyShort = orderLine.Product.VarietyName;
                    if (varietyShort.Length >= 35)
                    {
                        varietyShort = varietyShort.Substring(0, 31) + "...";
                    }
                    supplierSection.LineItems.Add(new LineItemSectionModel
                    {
                        SpeciesName = orderLine.Product.SpeciesName,
                        VarietyName = varietyShort,
                        FormDescription = orderLine.Product.ProductFormName,
                        Quantity = qty,
                        UnitPrice = Convert.ToDouble(orderLine.Price)
                    });
                }

                foreach (var charge in supplierOrder.SupplierOrderFeeList)
                {
                    var chargeModel = new ChargeModel();

                    chargeModel.Description = charge.SupplierOrderFeeStatus.Code == "Estimated"
                        ? string.Format("Estimated {0}", charge.SupplierOrderFeeType.DisplayName)
                        : charge.SupplierOrderFeeType.DisplayName;

                    chargeModel.Charge = Convert.ToDouble(charge.Amount);

                    if (charge.SupplierOrderFeeStatus.Code!= "Estimated"
                        && charge.SupplierOrderFeeStatus.Code != "Actual") // i.e. "TBD" or "Incl."
                    {
                        chargeModel.AlternateChargeText = charge.SupplierOrderFeeStatus.Description;
                    }

                    supplierSection.Charges.Add(chargeModel);
                }

                result.SupplierSections.Add(supplierSection);
            }

            if (growerOrder.GrowerOrderFeeList != null && growerOrder.GrowerOrderFeeList.Count() > 0)
            {
                foreach (var fee in growerOrder.GrowerOrderFeeList)
                {
                    var growerOrderCharge = new ChargeModel();
                    growerOrderCharge.Description = fee.GrowerOrderFeeType.Description;
                    growerOrderCharge.Charge = Convert.ToDouble(fee.Amount);

                    result.GrowerOrderCharges.Add(growerOrderCharge);
                }
            }

            return result;
        }

     
        public class OrderConfirmationModel
        {
            public OrderConfirmationModel()
            {
                SupplierSections = new List<SupplierSectionModel>();
                GrowerOrderCharges = new List<ChargeModel>();
            }

            public string OrderNumber { get; set; }
            public string PurchaseOrderNumber { get; set; }
            public string CustomerName { get; set; }
            public string WeekString { get; set; }
            public string OrderDescription { get; set; }
            public string PaymentMethod { get; set; }
            public string ShipToName { get; set; }
            public Address ShipToAddress { get; set; }

            public List<SupplierSectionModel> SupplierSections { get; set; }

            public List<ChargeModel> GrowerOrderCharges { get; set; }

            public int TotalCuttings
            {
                get
                {
                    return SupplierSections.SelectMany(x => x.LineItems).Sum(x => x.Quantity);
                }
            }

            public double TotalCuttingCharge
            {
                get
                {
                    return Math.Round(SupplierSections.SelectMany(x => x.LineItems).Sum(x => x.TotalPrice), 2);
                }
            }

            public double TotalOrderCharge
            {
                get
                {
                    return Math.Round(
                        SupplierSections.SelectMany(x => x.Charges).Sum(x => x.Charge)
                        + GrowerOrderCharges.Sum(x => x.Charge)
                        + TotalCuttingCharge, 2);
                }
            }
        }

        public class Address
        {
            public Address(string streetLine1, string streetLine2, string city, string regionCode, string postalCode)
            {
                StreetLine1 = streetLine1;
                StreetLine2 = streetLine2;
                City = city;
                RegionCode = regionCode;
                PostalCode = postalCode;
            }

            public string StreetLine1 { get; private set; }
            public string StreetLine2 { get; private set; }
            public string City { get; private set; }
            public string RegionCode { get; private set; }
            public string PostalCode { get; private set; }

            public override string ToString()
            {
                var result = new StringBuilder();
                result.Append(string.Format("{0}<br>", StreetLine1));

                if (!string.IsNullOrEmpty(StreetLine2))
                    result.Append(string.Format("{0}<br>", StreetLine2));

                result.Append(string.Format("{0}, {1} {2}", City, RegionCode, PostalCode));

                return result.ToString();
            }
        }

        public class SupplierSectionModel
        {
            public SupplierSectionModel()
            {
                LineItems = new List<LineItemSectionModel>();
                Charges = new List<ChargeModel>();
            }

            public string SupplierName { get; set; }
            public string SupplierOrderNo { get; set; }
            public string TagInfo { get; set; }
            public string ShipMethodName { get; set; }
            public List<LineItemSectionModel> LineItems { get; set; }
            public int TotalCuttings { get; set; }
            public List<ChargeModel> Charges { get; set; }
        }

        public class ChargeModel
        {
            public ChargeModel()
            {
                AlternateChargeText = null;
            }

            public string Description { get; set; }
            public double Charge { get; set; }
            public string AlternateChargeText { get; set; }
        }

        public class LineItemSectionModel
        {
            public string SpeciesName { get; set; }
            public string VarietyName { get; set; }
            public string FormDescription { get; set; }
            public int Quantity { get; set; }
            public double UnitPrice { get; set; }
            public double TotalPrice
            {
                get
                {
                    return Quantity * UnitPrice;
                }
            }
        }

        public class HtmlOrderConfirmationBuilder
        {
            public static string GetBasePath()
            {
                return HttpContext.Current == null
                    ? AppDomain.CurrentDomain.BaseDirectory
                    : Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");
            }

            public string Build(OrderConfirmationModel model, string templatePath)
            {
                DateTime dt = DateTime.Now;
                string sDate = dt.ToShortDateString();

                var file = Path.Combine(GetBasePath(), "OrderConfirmationTemplate.html");
                var result = File.ReadAllText(file);
                result = result.Replace("#OrderNumber#", model.OrderNumber);
                result = result.Replace("#CurrentDate#", sDate);
                result = result.Replace("#PONumber#", model.PurchaseOrderNumber);
                result = result.Replace("#CustomerName#", model.CustomerName);
                result = result.Replace("#OrderDescription#", model.OrderDescription);
                result = result.Replace("#PaymentMethod#", model.PaymentMethod);
                result = result.Replace("#ShipToName#", model.ShipToName);
                result = result.Replace("#ShipToAddress#", model.ShipToAddress.ToString());
                result = result.Replace("#WeekString#", model.WeekString);

                var supplierNodeStartKey = "<!--Supplier Node Start-->";
                var supplierNodeEndKey = "<!--Supplier Node End-->";

                var supplierNodeStartPos = result.IndexOf(supplierNodeStartKey, StringComparison.Ordinal);
                var supplierNodeEndPos = result.IndexOf(supplierNodeEndKey, StringComparison.Ordinal);

                var supplierNodeTemplate = result.Substring(
                    supplierNodeStartPos,
                    supplierNodeEndPos + supplierNodeEndKey.Length - supplierNodeStartPos);

                var supplierSections = "";

                foreach (var supplierModel in model.SupplierSections)
                {
                    var supplierSection = supplierNodeTemplate.Replace("#SupplierName#", supplierModel.SupplierName);
                    supplierSection = supplierSection.Replace("#SupplierOrderNo#", supplierModel.SupplierOrderNo);
                    supplierSection = supplierSection.Replace("#TagInfo#", supplierModel.TagInfo);
                    supplierSection = supplierSection.Replace("#ShipMethodName#", supplierModel.ShipMethodName);

                    var speciesItems = "";
                    var varietyItems = "";
                    var formDescriptionItems = "";
                    var quantityItems = "";
                    var unitPriceItems = "";
                    var totalPriceItems = "";

                    foreach (var lineItem in supplierModel.LineItems)
                    {
                        if (speciesItems != "")
                        {
                            speciesItems += "<br>";
                            varietyItems += "<br>";
                            formDescriptionItems += "<br>";
                            quantityItems += "<br>";
                            unitPriceItems += "<br>";
                            totalPriceItems += "<br>";
                        }

                        speciesItems += lineItem.SpeciesName;
                        varietyItems += lineItem.VarietyName;
                        formDescriptionItems += lineItem.FormDescription;
                        quantityItems += lineItem.Quantity;
                        unitPriceItems += lineItem.UnitPrice;
                        totalPriceItems += Math.Round(lineItem.TotalPrice, 2).ToString("C");
                    }

                    supplierSection = supplierSection.Replace("#SpeciesName#", speciesItems);
                    supplierSection = supplierSection.Replace("#VarietyName#", varietyItems);
                    supplierSection = supplierSection.Replace("#FormDescription#", formDescriptionItems);
                    supplierSection = supplierSection.Replace("#Quantity#", quantityItems);
                    supplierSection = supplierSection.Replace("#UnitPrice#", unitPriceItems);
                    supplierSection = supplierSection.Replace("#TotalPrice#", totalPriceItems);

                    supplierSection = supplierSection.Replace("#TotalCuttings#", supplierModel.TotalCuttings.ToString());

                    var chargeList = "";

                    foreach (var charge in supplierModel.Charges)
                    {
                        if (charge.AlternateChargeText == null)
                            chargeList += string.Format("{0}: {1}<br>", charge.Description, Math.Round(charge.Charge, 2).ToString("C"));
                        else
                            chargeList += string.Format("{0}: {1}<br>", charge.Description, charge.AlternateChargeText);
                    }

                    supplierSection = supplierSection.Replace("#SupplierCharges#", chargeList);

                    supplierSections += supplierSection;
                }

                result = result.Replace(supplierNodeTemplate, supplierSections);

                var totalsSection = new StringBuilder();
                totalsSection.Append(string.Format(@"<strong>Total Cuttings:</strong> {0}<br>", model.TotalCuttings));
                totalsSection.Append(string.Format(@"<strong>Total Cutting Cost:</strong> {0}<br>", Math.Round(model.TotalCuttingCharge, 2).ToString("C")));

                foreach (var charge in model.GrowerOrderCharges)
                {
                    if (charge.AlternateChargeText == null)
                        totalsSection.Append(string.Format(@"<strong>{0}:</strong> {1}<br>", charge.Description, Math.Round(charge.Charge, 2).ToString("C")));
                    else
                        totalsSection.Append(string.Format(@"<strong>{0}:</strong> {1}<br>", charge.Description, charge.AlternateChargeText));
                }

                totalsSection.Append(string.Format(@"<strong>Total Order Cost:</strong> {0}<br>", Math.Round(model.TotalOrderCharge, 2).ToString("C")));

                result = result.Replace("#OrderTotals#", totalsSection.ToString());

                //result = result.Replace("#TotalCuttingsForOrder#", model.TotalCuttings.ToString());
                //result = result.Replace("#TotalCuttingCost#", Math.Round(model.TotalCuttingCharge, 2).ToString("C"));
                //result = result.Replace("#TotalOrderCost#", Math.Round(model.TotalOrderCharge, 2).ToString("C"));

                return result;
            }
        }
        public byte[] HtmlToPdf(string htmlText, string baseUrl)
        {
            var converter = new HtmlToPdf();
            converter.Document.PageSize = PdfPageSize.Letter;
            converter.Document.PageOrientation = PdfPageOrientation.Portrait;
            converter.Document.Margins = new PdfMargins(14);
            converter.SerialNumber = "zISlnZyo-qoClrr6t-vrX74vzs-/ez47P31-7P/94v3+-4vX19fU=";
            return converter.ConvertHtmlToMemory(htmlText, baseUrl);
        }
    }
}
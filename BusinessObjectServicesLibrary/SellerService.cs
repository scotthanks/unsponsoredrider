﻿using System.Data.SqlClient;
using System.Web;
using BusinessObjectsLibrary;
using DataAccessLibrary;
using DataObjectLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Threading;
using GrowerOrderHistoryEvent = BusinessObjectsLibrary.GrowerOrderHistoryEvent;
//using GrowerOrder = BusinessObjectsLibrary.GrowerOrder;
using Lookup = DataObjectLibrary.Lookup;
//using OrderLine = DataObjectLibrary.OrderLine;
//using SupplierOrder = DataObjectLibrary.SupplierOrder;

namespace BusinessObjectServices
{   
    public class SellerService : BusinessServiceBase
    {
        public SellerService(StatusObject status)
            : base(status)
        {

        }


        public string GetSellerForUser()
        {

            var service = new SellerDataService(Status);

            var seller = service.GetSellerForUser();


            return seller;
        }
    }
}
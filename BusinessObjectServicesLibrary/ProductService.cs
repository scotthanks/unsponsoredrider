﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLibrary;
using DataServiceLibrary;

namespace BusinessObjectServices
{
    public class ProductService
    {
        public List<BusinessObjectsLibrary.Product> GetAllProgramProducts(Guid programGuid)
        {

            var productService = new DataServiceLibrary.ProductTableService();
            //var products = productService.GetAll();

            var products = productService.GetAllByField("ProgramGuid", programGuid.ToString(), "=", false);

            var productList = new List<BusinessObjectsLibrary.Product>();

            foreach (var product in products)
            {

                var newProduct = new BusinessObjectsLibrary.Product();
                newProduct.Guid = product.Guid;
                newProduct.SupplierProductIdentifier = product.SupplierIdentifier;
                newProduct.SupplierProductDescription = product.SupplierDescription;
                newProduct.ProgramGuid = product.ProgramGuid;
                productList.Add(newProduct);
             

            }

            return productList;
        }

        public BusinessObjectsLibrary.Product GetProductFromCode(string programCode)
        {
            var productService = new DataServiceLibrary.ProductTableService();
            var product = productService.TryGetByCode(programCode,false);

            BusinessObjectsLibrary.Product newProduct = new BusinessObjectsLibrary.Product();

            newProduct.Guid = product.Guid;
            
            return newProduct;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class SupplierService
    {
        public IEnumerable<Supplier> SelectSuppliers(Guid userguid,string plantCategoryCode, string productFormCode, string sellerCode)
        {
            //PlantCategoryCode maps to ProgramType[code] (e.g. VA)
            //ProductFormCode maps ProductFormCategory[code] (e.g. RC)
            //Note: Table named Species has code = cal for Calibrachoa

            //plantCategoryCode = "VA";
            //productFormCode = "RC";
            sellerCode = "GFB";
            var status = new StatusObject(userguid, true);
            var procedure = new DataServiceLibrary.SupplierDataService(status);

            var supplierTableObjectList =
                procedure.GetSuppliers
                (
                    programTypeCode: plantCategoryCode,
                    productFormCategoryCode: productFormCode,
                    sellerCode: sellerCode
                );

            var supplierList = new List<Supplier>();
            foreach (var item in supplierTableObjectList)
            {
                supplierList.Add( new Supplier(item));
            }

            supplierList = supplierList.OrderBy(o => o.Name).ToList();

            return supplierList;
        }

    }
}

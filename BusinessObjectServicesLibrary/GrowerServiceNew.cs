﻿using System.Data.SqlClient;
using System.Web;
using BusinessObjectsLibrary;
using DataAccessLibrary;
using DataObjectLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Threading;
using GrowerOrderHistoryEvent = BusinessObjectsLibrary.GrowerOrderHistoryEvent;
//using GrowerOrder = BusinessObjectsLibrary.GrowerOrder;
using Lookup = DataObjectLibrary.Lookup;
//using OrderLine = DataObjectLibrary.OrderLine;
//using SupplierOrder = DataObjectLibrary.SupplierOrder;

namespace BusinessObjectServices
{   
    public class GrowerServiceNew : BusinessServiceBase
    {
        public GrowerServiceNew(StatusObject status)
            : base(status)
        {

        }


        public List<Seller> GetSellers(string email)
        {

            var service = new GrowerDataService(Status);
            List<Seller> list = service.GetSellers(email);
            return list;
        }
    }
}